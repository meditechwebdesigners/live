// make sure not to conflict with CMS's jQuery...
var $jq = jQuery.noConflict(); 

$jq(document).ready(function () {
  
  // Variables
  
  var searchvisible = 0;
  var stickyheader__classname = 'js-sticky';
  var nav__class = '.js-nav';
  var search__button__class = '.js__search__button';
  var search__container__class = '.js__search__container';
  var search__input__class = '.form__search__input';
  //var search__input__class = '.gsc-input';
  var slideout_menu__class = '.sb-toggle-left';
  var search__icon = '.nav__icon__search';
  var icon__search = 'fa-search';
  var icon__close = 'fa-times';

  
  // Sticky Navigation
  
  $jq(window).scroll(function () {    
    if ($jq(window).scrollTop() >= 100) {
      $jq(nav__class).addClass(stickyheader__classname);
    } else {
      var windowwidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
      if(windowwidth >= 1041){
        $jq(nav__class).removeClass(stickyheader__classname);
        $jq(search__container__class).hide();
        $jq(search__button__class)
        .find(search__icon)
        .removeClass(icon__close)
        .addClass(icon__search);
        searchvisible = 0;
      }
    }
  });
  
  // Search Bar Functionality
  
  function closeSearch() {
    $jq(search__container__class).slideUp(200);
    $jq(search__button__class)
      .find(search__icon)
      .removeClass(icon__close)
      .addClass(icon__search);
    searchvisible = 0;
    return false;
  }
  
  function openSearch() {
    $jq(search__button__class)
      .find(search__icon)
      .removeClass(icon__search)
      .addClass(icon__close);
		$jq(search__container__class).slideDown(200).find(search__input__class).get(0).focus();
    searchvisible = 1;
    return false;
  }
   
  // Check and see if the Search Box is open
  
  function toggleSearch() {
      if (searchvisible === 1) {
        closeSearch();
      } else if(searchvisible === 0) {
        openSearch();
      }
  }

  // Fires Toggle Search if Search Icon is clicked
  
  $jq(search__button__class).on('click touchstart', function(){
      toggleSearch();
      return false;
  });
  
  // Closes Search if open when clicking on Menu Icon
  
  $jq(slideout_menu__class).on('click touchstart', function(){
    if(searchvisible === 1){
      toggleSearch();
    }
    return false;
  });
  
  $jq(window).scroll();
  
});