    function addLoadEvent(func){
      var onoldload =window.onLoad;
      if (typeof window.onLoad != 'function'){
        window.onLoad = func;
      } else {
        window.onLoad = function(){
          oldonload();
          func();
        }
      }
    }
    addLoadEvent(
      //plugin function, place inside DOM ready function
      outdatedBrowser({
        // bgColor: '#006652',
        // color: '#ffffff',
        lowerThan: 'borderImage',
        languagePath: '/lang/en.html'
        })
      );
