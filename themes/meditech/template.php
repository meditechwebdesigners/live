<?php

// remove unwanted HEAD tags =================================================================
function meditech_html_head_alter(&$head_elements){
  // Remove Drupal generator meta tag.
  if( isset($head_elements['system_meta_generator']) ){
    unset($head_elements['system_meta_generator']);
  }
}


// remove unwanted CSS files ============================================================================
function meditech_css_alter(&$css){
  $exclude = array(
    'modules/comment/comment.css' => FALSE,
    'modules/node/node.css' => FALSE,
    'modules/search/search.css' => FALSE,
    /*'modules/system/system.base.css' => FALSE,*/ // needed for filter accordions to collapse correctly
    'modules/system/system.menus.css' => FALSE,
    'modules/system/system.messages.css' => FALSE,
    'modules/system/system.theme.css' => FALSE,
    'modules/taxonomy/taxonomy.css' => FALSE,
    'modules/user/user.css' => FALSE,
    'modules/shortcut/shortcut.css' => FALSE,
    'modules/field/theme/field.css' => FALSE,
    'sites/all/modules/calendar/css/calendar_multiday.css' => FALSE,
    'sites/all/modules/date/date_api/date.css' => FALSE,
    'sites/all/modules/date/date_popup/themes/datepicker.1.7.css' => FALSE,
    'sites/all/modules/picture/picture_wysiwyg.css' => FALSE,
    'sites/all/modules/views/css/views.css' => FALSE,
    'sites/all/modules/ckeditor/css/ckeditor.css' => FALSE,
    'sites/all/modules/ctools/css/ctools.css' => FALSE,
  );
  $css = array_diff_key($css, $exclude);
}



// remove 'leaf' class from menu li =============================================================
function meditech_menu_link(array $variables){
  // This is the part that removes or changes the 'leaf' class.
  if (!empty($variables['element']['#attributes']['class'])){
    foreach ($variables['element']['#attributes']['class'] as $key => $class){
      if ($class == 'leaf'){
        // To remove the 'leaf' class.
        unset($variables['element']['#attributes']['class'][$key]);
      }
    }
  }
  return theme_menu_link($variables);
}


// Add class to EHR Solutions drop down menu ============================================================
function meditech_menu_tree__menu_ehr_solutions_goals(&$variables){
  return '<ul class="menu dropdown-menu">' . $variables['tree'] . '</ul>';
}


// alter search input types to follow HTML5 properties ====================================================
function meditech_preprocess_search_block_form(&$vars){
  $vars['search_form'] = str_replace('type="text"', 'type="search"', $vars['search_form']);
}



// Remove the search form from search result page ===================================================
function meditech_form_search_form_alter(&$form, &$form_state){
  if ((arg(1) == 'node') && (arg(2) != NULL)){
    $form['#access'] = FALSE;
  }
}



// alter the default search box ======================================================================
// alter the user login...
function meditech_form_alter(&$form, &$form_state, $form_id){
  
  // search form...
  if ($form_id == 'search_block_form'){
    $form['search_block_form']['#title'] = t('Site Search'); // Change the text on the label element
    $form['search_block_form']['#title_display'] = 'invisible'; // Toggle label visibilty
    $form['search_block_form']['#size'] = 40;  // define size of the textfield
    // Prevent user from searching the default text
    //$form['#attributes']['onsubmit'] = "if(this.search_block_form.value=='Search meditech.com' || this.search_block_form.value==''){ alert('Please enter a search'); return false; }";
    // Alternative (HTML5) placeholder attribute instead of using the javascript
    $form['search_block_form']['#attributes']['placeholder'] = t('');
	$form['search_block_form']['#attributes']['class'][] = 'form__search__input';
    //$form['actions']['#attributes']['class'][] = 'element-invisible';
    $form['actions']['submit']['#attributes']['class'][] = 'form__search__button';
    $form['#attributes']['class'][] = 'form__search';
  }
  
  // user login form...
  if($form_id == 'user_login' ){
    $form['name']['#title_display'] = 'invisible';
	$form['name']['#attributes']['placeholder'] = t('User Name');
	$form['name']['#attributes']['onblur'] = "if (this.value == ''){this.value = 'User Name';}";
    $form['name']['#attributes']['onfocus'] = "if (this.value == 'User Name'){this.value = '';}";
	
	$form['pass']['#title_display'] = 'invisible';
	$form['pass']['#attributes']['placeholder'] = t('Password');
	$form['pass']['#attributes']['onblur'] = "if (this.value == ''){this.value = 'Password';}";
    $form['pass']['#attributes']['onfocus'] = "if (this.value == 'Password'){this.value = '';}";
	
	$form['#attributes']['onsubmit'] = "if(this.user_login.name.value=='User Name'){ alert('Please enter your user name'); return false; }";
	$form['#attributes']['onsubmit'] = "if(this.user_login.pass.value=='Password'){ alert('Please enter your password'); return false; }";
	
	$form['actions']['submit']['#attributes']['class'][] = 'btn';
  }
  
}



// style Communications Team contact form (webforms)...webform-client-form-1087 ===================================
function meditech_preprocess_webform_element(&$variables){
  $variables['element']['#wrapper_attributes']['class'][] = 'another-wrapper-class';
}



// add variables to the page template ======================================================================
function meditech_preprocess_page(&$variables){
  // set $node variable for use in page templates...
  if( $node = menu_get_object() ){
    $variables['node'] = $node;
    $variables['field_not_for_everyone'] = $node->field_not_for_everyone['und'][0]['value'];
  }
  // add form variable so module's form can be rendered in node template...
  if(!empty($variables['node']) && $variables['node']->type == 'job_listing'){
    $variables['node']->mtJobInquiryForm = drupal_get_form('mtJobInquiryForm_form');
  }  
}


// add Block Search Engines field access to HTML template ==================================================
function meditech_preprocess_html(&$vars){
  $node = menu_get_object();
  if ($node && isset($node->nid)){
    $node = node_load($node->nid);
    node_build_content($node);
    $vars['field_block_search_engines'] = $node->field_block_search_engines['und'][0]['value'];
    $vars['field_not_for_everyone'] = $node->field_not_for_everyone['und'][0]['value'];
  } 
}



// create custom login templates =============================================================================
function meditech_theme(){
  $items = array();
  $items['user_login'] = array(
    'render element' => 'form',
    'path' => drupal_get_path('theme', 'meditech') . '/templates',
    'template' => 'user-login',
  );
  $items['user_register_form'] = array(
    'render element' => 'form',
    'path' => drupal_get_path('theme', 'meditech') . '/templates',
    'template' => 'user-register-form',
  );
  $items['user_pass'] = array(
    'render element' => 'form',
    'path' => drupal_get_path('theme', 'meditech') . '/templates',
    'template' => 'user-pass',
  );
  return $items;
}





// pull up image thumbnail for webinar using $node variable =================================================
// called in 2 Views templates ==============================================================================
function get_webinar_image($node){
  // check the node's taxonomy terms to adjust content accordingly...
  $tags = field_view_field('node', $node, 'field_webinar_topic');
  $taxonomyTerms = array();
  foreach($tags["#items"] as $tag){
    $taxonomyTerms[] = $tag["taxonomy_term"]->name;
  }
  $webinar_topic = $taxonomyTerms[0];

  // get webinar image choice...  
  switch($webinar_topic){
    case 'Ambulatory':
      $webinar_image = 'MEDITECH-webinar--ambulatory-care.jpg';
      $crop = 'crop-center';
      break;
    case 'Business and Clinical Analytics':
      $webinar_image = 'MEDITECH-webinar--business-clinical-analytics.jpg';
      $crop = 'crop-center';
      break;
    case 'Critical Care':
      $webinar_image = 'MEDITECH-webinar--critical-care.jpg';
      $crop = 'crop-center';
      break;
    case 'Cybersecurity':
      $webinar_image = 'MEDITECH-webinar--cybersecurity.jpg';
      $crop = 'crop-center';
      break;
    case 'ED Walkthrough':
      $webinar_image = 'MEDITECH-webinar--emergency-department.jpg';
      $crop = 'crop-center';
      break;
    case 'Expanse':
      $webinar_image = 'MEDITECH-webinar--expanse.jpg';
      $crop = 'crop-center';
      break;
    case 'Interoperability':
      $webinar_image = 'MEDITECH-webinar--interoperability.jpg';
      $crop = 'crop-center';
      break;
    case 'Mobility':
      $webinar_image = 'MEDITECH-webinar--mobility.jpg';
      $crop = 'crop-center';
      break;
    case 'MaaS':
      $webinar_image = 'MEDITECH-webinar--maas.jpg';
      $crop = 'crop-center';
      break;
    case 'Nursing':
      $webinar_image = 'MEDITECH-webinar--nursing.jpg';
      $crop = 'crop-center';
      break;
    case 'Oncology':
      $webinar_image = 'MEDITECH-webinar--oncology.jpg';
      $crop = 'crop-center';
      break;
    case 'Patient Experience':
      $webinar_image = 'MEDITECH-webinar--patient-experience.jpg';
      $crop = 'crop-center';
      break;
    case 'Physicians':
      $webinar_image = 'MEDITECH-webinar--physicians.jpg';
      $crop = 'crop-center';
      break;
    case 'Population Health':
      $webinar_image = 'MEDITECH-webinar--population-health.jpg';
      $crop = 'crop-center';
      break;
    case 'Professional Services':
      $webinar_image = 'MEDITECH-webinar--professional-services.jpg';
      $crop = 'crop-center';
      break;
    case 'Quality and Surveillance':
      $webinar_image = 'MEDITECH-webinar--quality-and-surveillance.jpg';
      $crop = 'crop-center';
      break;
    case 'Revenue Cycle':
      $webinar_image = 'MEDITECH-webinar--revenue-cycle.jpg';
      $crop = 'crop-center';
      break;
    case 'Surgical Services':
      $webinar_image = 'MEDITECH-webinar--surgical-services.jpg';
      $crop = 'crop-center';
      break;
    case 'Advisory Board':
      $webinar_image = 'MEDITECH-webinar--advisory-board.jpg';
      $crop = 'crop-center';
      break;
    case 'Social Determinants':
      $webinar_image = 'MEDITECH-webinar--social-determinants.jpg';
      $crop = 'crop-center';
      break;
    case 'Labor and Delivery':
      $webinar_image = 'MEDITECH-webinar--labor-and-delivery.jpg';
      $crop = 'crop-center';
      break;
    case 'Human Resources':
      $webinar_image = 'MEDITECH-webinar--human-resources.jpg';
      $crop = 'crop-center';
      break;
    case 'Toolkits':
      $webinar_image = 'MEDITECH-webinar--toolkits.jpg';
      $crop = 'crop-center';
      break;
    case 'Virtual Visits':
      $webinar_image = 'MEDITECH-webinar--virtual-visits.jpg';
      $crop = 'crop-center';
      break;
    case 'MUSE':
      $webinar_image = 'MEDITECH-webinar--MUSE-webinar.jpg';
      $crop = 'crop-center';
      break;
    case 'Interlace Health':
      $webinar_image = 'MEDITECH-webinar--Interlace-health.jpg';
      $crop = 'crop-center';
      break;
    case 'Across the Continuum':
      $webinar_image = 'MEDITECH-webinar--across-the-continuum.jpg';
      $crop = 'crop-center';
      break;
    case 'Access':
      $webinar_image = 'MEDITECH-webinar--Access.jpg';
      $crop = 'crop-center';
      break;
    case 'COVID-19 Data':
      $webinar_image = 'MEDITECH-webinar--covid-19-data.jpg';
      $crop = 'crop-center';
      break;
    case 'COVID-19 Strategy':
      $webinar_image = 'MEDITECH-webinar--coviv-19-strategy.jpg';
      $crop = 'crop-center';
      break;
    case 'CHIME':
      $webinar_image = 'MEDITECH-webinar--CHIME.jpg';
      $crop = 'crop-center';
      break;
    case 'Microsoft':
      $webinar_image = 'MEDITECH-webinar--Microsoft-webinar.jpg';
      $crop = 'crop-center';
      break;
    case 'Genomics':
      $webinar_image = 'MEDITECH-webinar--genomics.jpg';
      $crop = 'crop-center';
      break;
    case 'Patient Connect':
      $webinar_image = 'MEDITECH-webinar--patient-connect.jpg';
      $crop = 'crop-center';
      break;
    case 'Virtual Assistant':
      $webinar_image = 'MEDITECH-webinar--virtual-assistant.jpg';
      $crop = 'crop-center';
      break;
    case 'Harnessing Data':
      $webinar_image = 'MEDITECH-webinar--harnessing-data.jpg';
      $crop = 'crop-center';
      break;
    case 'Rural Health':
      $webinar_image = 'MEDITECH-webinar--rural-health.jpg';
      $crop = 'crop-center';
      break;
  }  
  
  // if image is selected, define image url...
  if( isset($webinar_image) ){
    $webinar_image_url = 'https://ehr.meditech.com/sites/all/themes/meditech/images/events/webinars/'.$webinar_image;
  }
  // if image is not selected, set image and image url to default image...
  else{
    $webinar_image_url = 'https://ehr.meditech.com/sites/all/themes/meditech/images/events/webinars/MEDITECH-webinar.jpg';
  }
  // if crop setting is set, define crop orientation...
  if( !isset($crop) ){
    $crop = 'crop-center';
  }
  $webinar_setup = array('url' => $webinar_image_url, 'crop' => $crop);
  
  return $webinar_setup;
}




// display clickable news-tags...
function generate_news_tag_links($node){
  print '<!-- START template.php -->';
  // get taxonomy terms................................
  $tags = field_view_field('node', $node, 'field_news_tags'); 
  // 'field_news_tags' is the machine name of the field in the content type that contains the taxonomy
  // list taxonomy news term associated with article..........
  foreach($tags['#items'] as $tag){
    $term = taxonomy_term_load($tag['tid']);
    $term_name = $term->name;
    // check for country tag and use special link, otherwise use normal tag link...
    if($term_name == 'South Africa'){
      print '<li><a href="https://ehr.meditech.com/global/meditech-south-africa/meditech-south-africa-news">South Africa</a></li>';
    }
    else{
      $link = l($term_name, 'taxonomy/term/'.$tag['tid']);
      print '<li>'.$link.'</li>';
    }
  }
  print '<!-- END template.php -->';
}



/* FIELD COLLECTION FUNCTIONS for templates */


// FUNCTION TO PULL IN CONTENT FOR A FIELD COLLECTION ITEM...
// to use the function simply assign it to a variable
// then pass in the field collection machine name to the function
// then you'll use the variable when calling each sub-field within the template
// ex: $cta = field_collection_data($node, 'field_fc_cta_block');
function field_collection_data($node, $field_name){
  // get array specific to a certain field collection item...
  $field_collection = field_get_items('node', $node, $field_name);
  // get all content within that field collection...
  $fc_field_data = entity_load('field_collection_item', array($field_collection[0]['value']));
  // get field collection ID...
  $fc_field_ID = key($fc_field_data);
  // create variable and load it with field collection data...
  $var_with_data = $fc_field_data[$fc_field_ID];
  // return variable...
  return $var_with_data;
}


// FUNCTION TO PULL IN CONTENT FOR A FIELD COLLECTION ITEM with multiple instances...
// to use the function simply assign it to a variable
// then pass in the field collection machine name to the function and the number of instances allowed ($quantity)
// then you'll use the variable when calling an array for each group of sub-fields within the template
// ex: $quotes = multi_field_collection_data($node, 'field_fc_quote_1', 3);
function multi_field_collection_data($node, $field_name, $quantity){
  // get array specific to a certain field collection item...
  $field_collection = field_get_items('node', $node, $field_name); 
  // create array...
  $multi_field_collections = array();
  // loop through '$quantity' times...
  for($x=0; $x<$quantity; $x++){
    // get all content within that field collection...
    $fc_field_data = entity_load('field_collection_item', array($field_collection[$x]['value']));
    // get field collection ID...
    $fc_field_ID = key($fc_field_data);
    // create variable and load it with field collection data...
    $var_with_data = $fc_field_data[$fc_field_ID];
    // add data to array...
    $multi_field_collections[] = $var_with_data;
  }
  // return variable...
  return $multi_field_collections;
}


// FUNCTION TO PULL IN CONTENT FOR A FIELD COLLECTION ITEM with multiple instances...
// to use the function simply assign it to a variable
// then pass in the field collection machine name to the function and the number of instances allowed ($quantity)
// then you'll use the variable when calling an array for each group of sub-fields within the template
// ex: $content_sections = multi_field_collection_data_unrestricted($node, 'field_fc_head_ltext_1');
function multi_field_collection_data_unrestricted($node, $field_name){
  // get array specific to a certain field collection item...
  $field_collection = field_get_items('node', $node, $field_name); 
  // create array...
  $multi_field_collections = array();
  $quantity = count($field_collection);
  // loop through '$quantity' times...
  for($x=0; $x<$quantity; $x++){
    // get all content within that field collection...
    $fc_field_data = entity_load('field_collection_item', array($field_collection[$x]['value']));
    // get field collection ID...
    $fc_field_ID = key($fc_field_data);
    // create variable and load it with field collection data...
    $var_with_data = $fc_field_data[$fc_field_ID];
    // add data to array...
    $multi_field_collections[] = $var_with_data;
  }
  // return variable...
  return $multi_field_collections;
}




// HUBSPOT button code...
function hubspot_button($hs_button_code, $alt_text){
  $button_code = trim($hs_button_code);
  print '<div class="button--hubspot">';
  print '<!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-'.$button_code.'"><span class="hs-cta-node hs-cta-'.$button_code.'" id="hs-cta-'.$button_code.'">';
  print '<!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/'.$button_code.'"><img class="hs-cta-img" id="hs-cta-img-'.$button_code.'" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/'.$button_code.'.png" alt="'.$alt_text.'" /></a></span>';
  print '<script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>';
  print '<script type="text/javascript">';
  print 'hbspt.cta.load(2897117, "'.$button_code.'", {"useNewLoader":"true","region":"na1"});';
  print '</script>';
  print '</span>';
  print '<!-- end HubSpot Call-to-Action Code -->';
  print '</div>';
}



// generate CTA text for bottom of campaigns...
function cta_text($cta){
  print '<h2>'.$cta->field_header_1['und'][0]['value'].'</h2>';
  // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
  if( $cta->field_long_text_1['und'][0]['value'] != '' ){
    print '<div>'.$cta->field_long_text_1['und'][0]['value'].'</div>';
  }
}



// pick colors for case study blocks...
function bg_color($abbr){
  $colors = array(
    array('edsc', '#0075A8'),
    array('frl', '#087E68'),
    array('iqs', '#00BC6F'),
    array('int', '#3E4545'),
    array('pne', '#00BBB3'),
    array('pop', '#B24EC4')
    );
  for($c = 0; $c < count($colors); $c++){
    if($colors[$c][0] == $abbr){
      $color = $colors[$c][1];
    }
  }
  return $color;
}



// function to display the correct secondary nav menu based on content type...
function get_secondary_nav($type){
  switch($type){
    case 'about':
    case 'about_hardcoded':
    case 'about_page':
    case 'executive':
    case 'hotels': // should this move under Events???
    case 'location':
      $sec_nav = 'menu-sec-nav---about';
      break;
    case 'campaign':
    case 'campaign_care_coordination':
    case 'campaign_critical_care':
    case 'campaign_long_term_partnerships':
    case 'campaign_oncology':
    case 'campaign_physician_productivity':
    case 'campaign_revenue_cycle':
    case 'campaign_surgical_services':
    case 'campaign_transformative_tech':
    case 'ehr_solutions_inner':
    case 'ehr_solutions_main':
    case 'toolkit_page':
      $sec_nav = 'menu-sec-nav---solutions';
      break;
    case 'career_event': // career event page is NOT a content type
    case 'career_page':
    case 'careers':
    case 'careers_hardcoded':
    case 'job_listing':
      $sec_nav = 'menu-sec-nav---careers';
      break;
    case 'event':
    case 'event_himss':
    case 'events_simple_page': // is this an issue for Global???
    case 'events_trade_show': // trade show page is NOT a content type NOR is past events
    case 'himss':
      $sec_nav = 'menu-sec-nav---events';
      break;
    case 'home_page':
      $sec_nav = 'menu-sec-nav---main';
      break;
    case 'international_asia_pacific':
      $sec_nav = 'menu-sec-nav---asia-pacific';
      break;
    case 'international_south_africa':
      $sec_nav = 'menu-sec-nav---south-africa';
      break;
    case 'international_uk_ire':
      $sec_nav = 'menu-sec-nav---uk-ire';
      break;
    case 'event_international':
    case 'international_canada': // until it has its own section
    case 'international_cta':
    case 'new_international': // main global page
      $sec_nav = 'menu-sec-nav---global';
      break;
    case 'news_article': // main news page is NOT a content type yet
    case 'ebook':
      $sec_nav = 'menu-sec-nav---news';
      break;
    default: 
      $sec_nav = 'menu-sec-nav---news';
      // call-to-action, documentation, home-page, customer_materials_page_menu, public-materials-page-menu, regulatory, search, simple-page, style-guide, testing, etc.
  }
  return $sec_nav;
}




/**
 * make node fields available to RSS Views...
 */
function meditech_preprocess_views_view_row_rss(&$vars) {
  $view     = &$vars['view'];
  $options  = &$vars['options'];
  $item     = &$vars['row'];
  // Use the [id] of the returned results to determine the nid in [results]
  $result = &$vars['view']->result;
  $id   = &$vars['id'];
  $node   = node_load( $result[$id-1]->nid );
  $vars['node'] = $node;
}
?>