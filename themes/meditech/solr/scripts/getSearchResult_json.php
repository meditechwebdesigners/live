<?php
//set up error logging
error_reporting(E_ALL);
ini_set('display_errors', 1);

isset($_POST['SearchString']) ? $searchString = $_POST['SearchString'] : $searchString = "";
isset($_POST['Server']) ? $serverList = $_POST['Server'] : $serverList = array();
isset($_POST['Status']) ? $statusList = $_POST['Status'] : $statusList = array();
//ATWEB-6956 - Add restrict to WU Site to advanced search
isset($_POST['WuSite']) ? $wuSiteList = $_POST['WuSite'] : $wuSiteList = array();
isset($_POST['From']) ? $from = $_POST['From'] : $from = "";
isset($_POST['Thru']) ? $thru = $_POST['Thru'] : $thru = "";

//solr search core location
$standardServerArr = array( "home"=>"https://solr.meditech.com/solr/home/select?q=",
                            "customer"=>"https://solr.meditech.com/solr/customer/select?q=");                            
$ehrServerArr = array ( "ehr"=>"https://solr.meditech.com/solr/ehr/select?");
$hubspotServerArr = array ( "blog"=>"https://solr.meditech.com/solr/hubspot/select?q=");

//hold array result for each area
$standardServer = array();
$ehrServer = array();       
$hubspotServer = array();

//gzip content
ob_start("ob_gzhandler");                
header('Content-Type: application/json');

$standardServer = standardSolrSelect($searchString,$statusList,$serverList,$wuSiteList,$from,$thru);
$ehrServer = ehrSolrSelect($searchString,$statusList,$serverList,$from,$thru);
$hubspotServer = hubspotSolrSelect($searchString,$statusList,$serverList,$from,$thru);

//return the merge of all array result
$finalAnswer = json_encode(array_merge_recursive($standardServer,$ehrServer,$hubspotServer));
echo $finalAnswer;
ob_end_flush();

//in :1=searchString 2=Status (Publish, Edit) 3=List of User OID 4=From 5=Thru
//out:array(jsonResult)
function ehrSolrSelect($searchString,$statusList,$serverList,$from,$thru) {
    global $ehrServerArr;
    //minus the first and last character and remove all special character (except space and dot)
    $cleanAndDequote = preg_replace('/[^a-zA-Z0-9 \.]+/', '', substr($searchString,1,strlen($searchString)-2));
    //if the original string is surrounded by double quote, put the double quote back on the cleanAndDequote version else remove all special character (except space and dot)
    $searchString = preg_match('/^(["\']).+\1$/m', $searchString) ? "\"" . $cleanAndDequote . "\"" : preg_replace('/[^a-zA-Z0-9 \.]+/', '', $searchString);
    if ($searchString) {
        $searchString = (preg_match('/^(["\']).+\1$/m', $searchString) ? str_replace("*","",$searchString) : $searchString);   
    }
    else {
        $searchString="";
    }    
    
    $searchQuery  = "mm=1&ps=15&hl=true&json.nl=map&fl=id,entity_id,entity_type,bundle,bundle_name,label,ss_language,is_comment_count,ds_created,ds_changed,score,path,url,is_uid,tos_name&";
    $searchQuery .= "start=0&rows=100&f.content.hl.maxAlternateFieldLength=256&hl.snippets=3&q=" . urlencode($searchString) . "&spellcheck.q=". urlencode($searchString) ."&spellcheck=true&qf=content^40&qf=label^5.0&qf=tags_h2_h3^3.0&";
    $searchQuery .= "qf=tags_h4_h5_h6^2.0&qf=tags_inline^1.0&qf=taxonomy_names^2.0&qf=tos_content_extra^0.1&qf=tos_name^3.0&pf=content^2.0&hl.fl=content&hl.mergeContiguous=true&wt=json&";
    $searchQuery .= "f.content.hl.alternateField=teaser&indent=true&defType=dismax";
	// boost most recent 
	$bfParams     = "&bf=" . urlencode("recip(rord(ds_changed),1,1000,1000)^20.5");	
	// boost from 3 months ago to now
	$bqParams	  = "&bq=" . urlencode("ds_changed:[NOW-3MONTHS/DAY TO NOW/DAY+1DAY]^100.5"); 
	
    $fq2Arr = array();
    //get advanced search options
    foreach ($statusList as $st) {
        $status = $st["id"];        

        if ($from || $thru){
            $from = $from ? $from : "*";
            $thru = $thru ? $thru : "*";            
            array_push ($fq2Arr,$status . "Date:[" . $from . " TO " . $thru . "T23:59:59]");        
        }
    }
    
    //filer query string for date
    $fq2 = !empty($fq2Arr) ? implode(' OR ' , $fq2Arr) : "";
    
    $ch = curl_init();    
    curl_setopt($ch, CURLOPT_VERBOSE, FALSE);
 
	//SSL stuff
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
	//end SSL 
    
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-type: application/json'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);    
   
    $resultArr = array();   
    if (empty($serverList)) {
        foreach ($ehrServerArr as $val) {
            $url = $val . $searchQuery ."&fq=" . urlencode($fq2);           
            curl_setopt($ch, CURLOPT_URL, $url);
            array_push ($resultArr,curl_exec($ch));          
        }
      
    }
    else {
        foreach ($serverList as $se) {
            $server = $se["id"];
            if ($server == "ehr") {
                $url = $ehrServerArr[$server] . $searchQuery ."&fq=" . urlencode($fq2) . $bqParams . $bfParams;
                curl_setopt($ch, CURLOPT_URL, $url);
                array_push($resultArr,curl_exec($ch));
            }
        }        
    }         
    $result = array();
    foreach($resultArr as $val) {        
        $result = array_merge_recursive($result,json_decode($val,true));        
    }
    
    curl_close($ch);    
    return $result;                    
};

//in :1=searchString 2=Status (Publish, Edit) 3=List of User OID 4=From 5=Thru
//out:array(jsonResult)
function hubspotSolrSelect($searchString,$statusList,$serverList,$from,$thru) {
    global $hubspotServerArr;
    
	//minus the first and last character and remove all special character (except space and dot)
    $cleanAndDequote = preg_replace('/[^a-zA-Z0-9 \.]+/', '', substr($searchString,1,strlen($searchString)-2));	
	//if the original string is surrounded by double quote and not empty(after remove all special character), put the double quote back on the cleanAndDequote version else remove all special character (except space and dot)
	if ($cleanAndDequote) {		
		$searchString = preg_match('/^(["\']).+\1$/m', $searchString) ? "\"" . $cleanAndDequote . "\"" : preg_replace('/[^a-zA-Z0-9 \.]+/', '', $searchString);
    }
	//searchString is empty 
	else {
		$searchString= "";
	}
	
    if ($searchString) {
		$q  = $searchString;
	}
    else {
        $q="";
    }	

	// ranking query ---------------------------------
	// boost most recent 
	$bfParams     = "&bf=" . urlencode("recip(rord(publish_date),1,1000,1000)^135.5");	
	// use dismax 
	// When there fewer than 3 keywords, both of the keywords must be found in the document. 
	// When there are 3 - 6 keywords, 80% of the keywords must be found in the document. 
	// When there are more than 6 keywords, 50% of the keywords must be found in the document.
	$queryParams  = "&wt=json&indent=true&defType=dismax&mm=" . urlencode("2<80% 6<50%");
	// boost query field and the phase field
	$queryParams .= "&qf=" . urlencode("page_title^5 post_summary^5 published_url^2 post_body^0.5") . "&pf=" . urlencode("page_title^20 post_summary^20 published_url^8 post_body^2");
	$queryParams .= "&ps=5&spellcheck=true";
	//end ranking query -----------------------------	
	
    $fq2Arr = array();
    //get advanced search options
    foreach ($statusList as $st) {
        $status = $st["id"];        

        if ($from || $thru){
            $from = $from ? $from : "*";
            $thru = $thru ? $thru : "*";            
            array_push ($fq2Arr,$status . "Date:[" . $from . " TO " . $thru . "T23:59:59]");        
        }
    }
    //filer query string for date
    $fq2 = !empty($fq2Arr) ? implode(' OR ' , $fq2Arr) : "";
    
    $ch = curl_init();
    
    curl_setopt($ch, CURLOPT_VERBOSE, FALSE); 
	//SSL stuff
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
	//end SSL 
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-type: application/json'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);    
   
    $resultArr = array();   
    if (empty($serverList)) {
        foreach ($hubspotServerArr as $val) {
            $url = $val . urlencode($q) ."&fq=" . urlencode($fq2) . $queryParams . $bfParams;
            curl_setopt($ch, CURLOPT_URL, $url);
            array_push ($resultArr,curl_exec($ch));            
        }      
    }
    else {
        foreach ($serverList as $se) {
            $server = $se["id"];
            if ($server == "blog") {
                $url = $hubspotServerArr[$server] . urlencode($q) ."&fq=" . urlencode($fq2) . $queryParams . $bfParams;
                curl_setopt($ch, CURLOPT_URL, $url);
                array_push($resultArr,curl_exec($ch));
            }
        }        
    }         

    $result = array();
    foreach($resultArr as $val) {        
        $result = array_merge_recursive($result,json_decode($val,true));        
    }

    curl_close($ch);
    return $result;      
};

//in :1=searchString 2=Status (Publish, Edit) 3=List of User OID 4=From 5=Thru
//out:array(jsonResult)
function standardSolrSelect($searchString,$statusList,$serverList,$wuSiteList,$from,$thru) {
    global $standardServerArr;
    //minus the first and last character and remove all special character (except space and dot)
    $cleanAndDequote = preg_replace('/[^a-zA-Z0-9 \.]+/', '', substr($searchString,1,strlen($searchString)-2));   

	//if the original string is surrounded by double quote and not empty(after remove all special character), put the double quote back on the cleanAndDequote version else remove all special character (except space and dot)
	if ($cleanAndDequote) {		
		$searchString = preg_match('/^(["\']).+\1$/m', $searchString) ? "\"" . $cleanAndDequote . "\"" : preg_replace('/[^a-zA-Z0-9 \.]+/', '', $searchString);
    }
	//searchString is empty 
	else {
		$searchString= "";
	}
	
    if ($searchString) {
		$q  = $searchString;
	}
    else {
        $q="";
    }

    $fq2Arr = $fq3Arr = array();
    //get advanced search options
    foreach ($statusList as $st) {
        $status = $st["id"];        

        if ($from || $thru){
            $from = $from ? $from : "*";
            $thru = $thru ? $thru : "*";            
            array_push ($fq2Arr,$status . "Date:[" . $from . " TO " . $thru . "T23:59:59]");        
        }
    }
    //ATWEB-6956 - Add restrict to WU Site to advanced search
    foreach ($wuSiteList as $wu) {
        $wuSite = $wu["name"];
        array_push ($fq3Arr, "id:*\\\\live_en_d\\\\" . $wuSite . "\\\\*");
    }
    
    //filer query string for date
    $fq2 = !empty($fq2Arr) ? implode(' OR ' , $fq2Arr) : "";
    //filer query string for wu site
    $fq3 = !empty($fq3Arr) ? implode(' OR ' , $fq3Arr) : "";     
    
    $ch = curl_init();
    
    curl_setopt($ch, CURLOPT_VERBOSE, FALSE);
    //Debug code
    //curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
    //curl_setopt($ch, CURLOPT_STDERR,$debugfile = fopen("debug.log", "ab"));
    //fwrite($debugfile,date('YmdHis'). "\r\n");
 
	//SSL stuff
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
	//end SSL 
    
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-type: application/json'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);    
   
    $resultArr = array();
	// ranking query ---------------------------------
	// boost most recent 
	$bfParams     = "&bf=" . urlencode("recip(rord(wuPublishDate),1,1000,1000)^20.5");	
	// boost from 3 months ago to now
	$bqParams	  = "&bq=" . urlencode("wuPublishDate:[NOW-3MONTHS/DAY TO NOW/DAY+1DAY]^100.5"); 
	// use dismax 
	// When there fewer than 3 keywords, both of the keywords must be found in the document. 
	// When there are 3 - 6 keywords, 80% of the keywords must be found in the document. 
	// When there are more than 6 keywords, 50% of the keywords must be found in the document.
	$queryParams  = "&wt=json&indent=true&defType=dismax&mm=" . urlencode("2<80% 6<50%");
	// boost query field and the phase field
	$queryParams .= "&qf=" . urlencode("title^5 wuUserTitle^5 id^2 text^0.5") . "&pf=" . urlencode("title^20 wuUserTitle^20 id^8 text^2");
	$queryParams .= "&ps=5&spellcheck=true";
	//end ranking query -----------------------------
	
    if (empty($serverList)) {
        foreach ($standardServerArr as $val) {
            $url = $val . urlencode($q) ."&fq=" . urlencode($fq2) . "&fq=" . urlencode($fq3) . $queryParams . $bfParams . $bqParams; 
            curl_setopt($ch, CURLOPT_URL, $url);
            array_push ($resultArr,curl_exec($ch));            
        }      
    }
    else {
        foreach ($serverList as $se) {
            $server = $se["id"];
            if ($server == "home" || $server == "customer") {
                $url = $standardServerArr[$server] . urlencode($q) ."&fq=" . urlencode($fq2) . "&fq=" . urlencode($fq3) . $queryParams . $bfParams . $bqParams;
                curl_setopt($ch, CURLOPT_URL, $url);
                array_push($resultArr,curl_exec($ch));
            }
        }        
    }         

    $result = array();
    foreach($resultArr as $val) {        
        $result = array_merge_recursive($result,json_decode($val,true));        
    }

    curl_close($ch);

    //Debug code
    //fwrite($debugfile,"\r\n\r\n");
	//fclose($debugfile);
    
    return $result;
}