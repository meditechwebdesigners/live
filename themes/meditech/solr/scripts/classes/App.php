<?php 

// Application Class.
class App extends DB {

	// Connect to database using parent DB class.
	public function __construct() {
		parent::__construct();
	}
	
	// Get an application.
	public function get($id) {
	
		// App query.
		$appQuery = 'SELECT * ';
		$appQuery .= 'FROM apps ';
		$appQuery .= 'WHERE id=:id';
		$this->query($appQuery);
		$this->bind(':id',$id);
		$appResult = $this->single();
	
		// Teams query.
		$teamsQuery = 'SELECT teamsListID AS id, list.name AS name ';
		$teamsQuery .= 'FROM teams ';
		$teamsQuery .= 'INNER JOIN teamsList AS list ON list.id = teams.teamsListID ';
		$teamsQuery .= 'WHERE appID = :id';
		$this->query($teamsQuery);
		$this->bind(':id',$id);
		$teamsResult = $this->resultSet();	
	
		// Technical Contacts query.
		$contactsQuery = 'SELECT name.oid AS oid, name.nameValue AS name ';
		$contactsQuery .= 'FROM technicalContacts ';
		$contactsQuery .= 'INNER JOIN coredata_read.coreUser_name AS name ON technicalContacts.staffOID = name.oid ';
		$contactsQuery .= 'WHERE appID = :id AND name.nameType = "display"';
		$this->query($contactsQuery);
		$this->bind(':id',$id);
		$contactsResult = $this->resultSet();			
		
		// Used By query.
		$usedByQuery = 'SELECT usedByListID AS id, list.name AS name ';
		$usedByQuery .= 'FROM usedBy ';
		$usedByQuery .= 'INNER JOIN usedByList AS list ON list.id = usedBy.usedByListID ';
		$usedByQuery .= 'WHERE appID = :id';
		$this->query($usedByQuery);
		$this->bind(':id',$id);
		$usedByResult = $this->resultSet();		
	
		// Clusters query.
		$clustersQuery = 'SELECT clustersListID AS id, list.name AS name ';
		$clustersQuery .= 'FROM clusters ';
		$clustersQuery .= 'INNER JOIN clustersList AS list ON list.id = clusters.clustersListID ';
		$clustersQuery .= 'WHERE appID = :id';
		$this->query($clustersQuery);
		$this->bind(':id',$id);
		$clustersResult = $this->resultSet();
		
		// Physical/Other Servers query.
		$serversQuery = 'SELECT servers.serverID AS id, serversList.name, servers.serverType AS type ';
		$serversQuery .= 'FROM servers ';
		$serversQuery .= 'INNER JOIN serversList ON servers.serverID = serversList.id ';
		$serversQuery .= 'WHERE appID = :id AND serverType != "VM"';
		$this->query($serversQuery);
		$this->bind(':id',$id);
		$servers = $this->resultSet();
		
		// VMs query.
		$vms = '';
		$vmsQuery = 'SELECT serverID AS id ';
		$vmsQuery .= 'FROM servers ';
		$vmsQuery .= 'WHERE appID = :id AND serverType = "VM"';
		$this->query($vmsQuery);
		$this->bind(':id',$id);
		$vmIDs = $this->resultArray();
		
		$vms = array();
		if (count($vmIDs) > 0) {
		
			// Define variables.
			$vm = new VM();
				
			// Get VM names.
			$vmIDs = implode(',',$vmIDs);
			$vms = $vm->getNames($vmIDs);

		}
		
		// Combine servers and vms.
		$serversList = array_merge($servers,$vms);

		// Resources query.
		$resourcesQuery = 'SELECT resourcesListID AS id, list.name AS name ';
		$resourcesQuery .= 'FROM resources ';
		$resourcesQuery .= 'INNER JOIN resourcesList AS list ON list.id = resources.resourcesListID ';
		$resourcesQuery .= 'WHERE appID = :id';
		$this->query($resourcesQuery);
		$this->bind(':id',$id);
		$resourcesResult = $this->resultSet();	
		
		// Dependencies query.
		$dependenciesQuery = 'SELECT appDependencies.dependentOn AS id, apps.name AS name ';
		$dependenciesQuery .= 'FROM appDependencies ';
		$dependenciesQuery .= 'INNER JOIN apps ON appDependencies.dependentOn = apps.id ';
		$dependenciesQuery .= 'WHERE appID = :id';
		$this->query($dependenciesQuery);
		$this->bind(':id',$id);
		$dependenciesResult = $this->resultSet();		
	
		// Return matches.
		return array('app'=>$appResult,'teams'=>$teamsResult,'technicalContacts'=>$contactsResult,'usedBy'=>$usedByResult,'clusters'=>$clustersResult,'servers'=>$serversList,'resources'=>$resourcesResult,'dependencies'=>$dependenciesResult);	
	
	}
	
	// Get all apps for staff viewing.
	public function getListForStaff() {
	
		// Query database.
		$query = 'SELECT apps.name, description, url, userDoc ';
		$query .= 'FROM apps ';
		$query .= 'LEFT JOIN usedBy ON apps.id = usedBy.appID ';
		$query .= 'WHERE apps.status = "Active" AND usedBy.usedByListID IN (20,22) ';
		$query .= 'GROUP BY apps.id ';
		$query .= 'ORDER BY apps.name';
		$this->query($query);
		$result = $this->resultSet();
		
		// Return result.
		return $result;
	
	}
	
	// Get all apps for admin viewing.
	public function getListForAdmins() {
	
		// Query database.
		$query = 'SELECT apps.id, apps.name, apps.status, apps.description, ';
		$query .= 'GROUP_CONCAT(DISTINCT teamsList.name SEPARATOR " <br />") AS teams, ';	      
		$query .= 'apps.url, adminUrl, userDoc, technicalDoc ';
		$query .= 'FROM apps ';
		$query .= 'LEFT JOIN teams ON apps.id = teams.appID ';
		$query .= 'LEFT JOIN teamsList ON teams.teamsListID = teamsList.id ';
		$query .= 'WHERE apps.status != "Removed" ';
		$query .= 'GROUP BY apps.id ';
		$query .= 'ORDER BY apps.name';
		$this->query($query);
		$result = $this->resultSet();
		
		// Return result.
		return $result;
	
	}
	
	// Check for duplicate application name.
	public function checkForDuplicate($id,$name) {
	
		// Include id in where clause if it exists.
		$where = '';
		if ($id) { $where = 'AND id != :id'; }
	
		// Query database.
		$query = 'SELECT name ';
		$query .= 'FROM apps ';
		$query .= 'WHERE name = :name AND status != "Removed" '.$where;
		$this->query($query);
		$this->bind(':name',$name);
		if ($id) { $this->bind(':id',$id); }
		$this->execute();
		$rows = $this->rowCount();	
		
		// Return rows.
		return $rows;
	
	}
	
	// Check app prior to removal.
	public function checkRemoval($id,$name) {
	
		// Query database.
		$query = 'SELECT apps.name ';
		$query .= 'FROM appDependencies ';
		$query .= 'INNER JOIN apps ON appDependencies.appID = apps.id ';
		$query .= 'WHERE dependentOn = :id ';
		$query .= 'ORDER BY apps.name';
		$this->query($query);
		$this->bind(':id',$id);
		$result = $this->resultSet();
		
		// Return result.
		return array('id'=>$id,'name'=>$name,'entries'=>$result);	
	
	}	
	
	// Add an application.
	public function add($submittedBy,$timestamp,$name,$description,$status,$releaseDate,$technicalContacts,$teams,$usedBy,$clusters,$servers,$resources,$dependencies,$url,$adminUrl,$userDoc,$technicalDoc) {
	
		try {
		
			// Begin transaction.
			$this->begin();			
		
			// App query.
			$appQuery = 'INSERT INTO apps (timestamp,submittedBy,name,description,releaseDate,status,url,adminUrl,userDoc,technicalDoc) VALUES ';
			$appQuery .= '(:timestamp,:submittedBy,:name,:description,:releaseDate,:status,:url,:adminUrl,:userDoc,:technicalDoc)';
			$this->query($appQuery);
			$this->bind(':timestamp',$timestamp);
			$this->bind(':submittedBy',$submittedBy);	
			$this->bind(':name',$name);
			$this->bind(':description',$description);
			$this->bind(':releaseDate',$releaseDate);
			$this->bind(':status',$status);
			$this->bind(':url',$url);
			$this->bind(':adminUrl',$adminUrl);	
			$this->bind(':userDoc',$userDoc);
			$this->bind(':technicalDoc',$technicalDoc);
			$this->execute();
			$id = $this->lastInsertId();

			// Technical Contacts query.
			if ($id && count($technicalContacts) > 0) {

				$contactsQuery = 'INSERT INTO technicalContacts (staffOID,appID) VALUES ';
				$values = array_fill(0,count($technicalContacts),'(?,?)');
				$contactsQuery .= implode(',',$values);
				$this->query($contactsQuery);
				$i = 1;
				foreach($technicalContacts AS $contact) {
					$contactOID = $contact['oid'];
					$this->bind($i++,$contactOID);
					$this->bind($i++,$id);
				}
				$this->execute();
	
			}

			// Teams query.
			$teamNames = '';
			if ($id && count($teams) > 0) {

				$teamsQuery = 'INSERT INTO teams (teamsListID,appID) VALUES ';
				$values = array_fill(0,count($teams),'(?,?)');
				$teamsQuery .= implode(',',$values);
				$this->query($teamsQuery);
				$i = 1;
				foreach($teams AS $team) {
					$teamID = $team['id'];
					$teamName = $team['name'];
					$teamNames .= $teamName . ' <br />';				
					$this->bind($i++,$teamID);
					$this->bind($i++,$id);
				}
				$this->execute();
	
			}			
			
			// Used By query.
			if ($id && count($usedBy) > 0) {

				$usedByQuery = 'INSERT INTO usedBy (usedByListID,appID) VALUES ';
				$values = array_fill(0,count($usedBy),'(?,?)');
				$usedByQuery .= implode(',',$values);
				$this->query($usedByQuery);
				$i = 1;
				foreach($usedBy AS $group) {
					$groupID = $group['id'];			
					$this->bind($i++,$groupID);
					$this->bind($i++,$id);
				}
				$this->execute();
	
			}

			// Clusters query.
			if ($id && count($clusters) > 0) {

				$clustersQuery = 'INSERT INTO clusters (clustersListID,appID) VALUES ';
				$values = array_fill(0,count($clusters),'(?,?)');
				$clustersQuery .= implode(',',$values);
				$this->query($clustersQuery);
				$i = 1;
				foreach($clusters AS $cluster) {
					$clusterID = $cluster['id'];			
					$this->bind($i++,$clusterID);
					$this->bind($i++,$id);
				}
				$this->execute();
	
			}			
	
			// Servers query.
			if ($id && count($servers) > 0) {

				$serversQuery = 'INSERT INTO servers (serverID,serverType,appID) VALUES ';
				$values = array_fill(0,count($servers),'(?,?,?)');
				$serversQuery .= implode(',',$values);
				$this->query($serversQuery);
				$i = 1;
				foreach($servers AS $server) {
					$serverID = $server['id'];
					$serverType = $server['type'];
					$this->bind($i++,$serverID);
					$this->bind($i++,$serverType);
					$this->bind($i++,$id);				
				}
				$this->execute();
	
			}	
	
			// Resources query.
			if ($id && count($resources) > 0) {

				$resourcesQuery = 'INSERT INTO resources (resourcesListID,appID) VALUES ';
				$values = array_fill(0,count($resources),'(?,?)');
				$resourcesQuery .= implode(',',$values);
				$this->query($resourcesQuery);
				$i = 1;
				foreach($resources AS $resource) {
					$resourceID = $resource['id'];			
					$this->bind($i++,$resourceID);
					$this->bind($i++,$id);
				}
				$this->execute();
	
			}	
			
			// Dependencies insert query.
			if (count($dependencies) > 0) {
	
				$dependenciesInsertQuery = 'INSERT INTO appDependencies (appID,dependentOn) VALUES ';
				$values = array_fill(0,count($dependencies),'(?,?)');
				$dependenciesInsertQuery .= implode(',',$values);
				$this->query($dependenciesInsertQuery);
				$i = 1;
				foreach($dependencies AS $dependency) {
					$dependencyID = $dependency['id'];			
					$this->bind($i++,$id);
					$this->bind($i++,$dependencyID);
				}
				$this->execute();
	
			}			
		
			// Log entry.
			if ($id) {
		
				$logQuery = 'INSERT INTO log (timestamp,staffOID,appID,action) VALUES ';
				$logQuery .= '(:timestamp,:staffOID,:appID,"ADD")';
				$this->query($logQuery);
				$this->bind(':timestamp',$timestamp);
				$this->bind(':staffOID',$submittedBy);
				$this->bind(':appID',$id);
				$this->execute();
		
			}
		
			// If no issues with above queries, then commit the changes.
			$this->end();
			
			// Return data.
			return array('id'=>$id,'name'=>$name,'status'=>$status,'description'=>$description,'url'=>$url,'adminUrl'=>$adminUrl,'userDoc'=>$userDoc,'technicalDoc'=>$technicalDoc,'teams'=>$teamNames);			
		
		} catch(Exception $e) {
		
			// If any of the above transactions failed, roll back changes.
			$this->cancel();
			return array('error'=>'There was an issue adding the application.');
		
		}
	
	}

	// Edit an application.
	public function edit($submittedBy,$id,$timestamp,$name,$description,$status,$releaseDate,$technicalContacts,$teams,$usedBy,$clusters,$servers,$resources,$dependencies,$url,$adminUrl,$userDoc,$technicalDoc) {

		try {
		
			// Begin transaction.
			$this->begin();
			
			// App query.
			$appQuery = 'UPDATE apps ';
			$appQuery .= 'SET timestamp=:timestamp,submittedBy=:submittedBy,name=:name,description=:description,releaseDate=:releaseDate,status=:status,url=:url,adminUrl=:adminUrl,userDoc=:userDoc,technicalDoc=:technicalDoc ';
			$appQuery .= 'WHERE id=:id';
			$this->query($appQuery);
			$this->bind(':id',$id);
			$this->bind(':timestamp',$timestamp);
			$this->bind(':submittedBy',$submittedBy);	
			$this->bind(':name',$name);
			$this->bind(':description',$description);
			$this->bind(':releaseDate',$releaseDate);
			$this->bind(':status',$status);
			$this->bind(':url',$url);
			$this->bind(':adminUrl',$adminUrl);	
			$this->bind(':userDoc',$userDoc);
			$this->bind(':technicalDoc',$technicalDoc);	
			$this->execute();
	
			// Technical Contacts delete query.
			$contactsDeleteQuery = 'DELETE FROM technicalContacts ';
			$contactsDeleteQuery .= 'WHERE appID=:id';
			$this->query($contactsDeleteQuery);
			$this->bind(':id',$id);
			$this->execute();
	
			// Technical Contacts insert query.
			if (count($technicalContacts) > 0) {
	
				$contactsInsertQuery = 'INSERT INTO technicalContacts (staffOID,appID) VALUES ';
				$values = array_fill(0,count($technicalContacts),'(?,?)');
				$contactsInsertQuery .= implode(',',$values);
				$this->query($contactsInsertQuery);
				$i = 1;
				foreach($technicalContacts AS $contact) {
					$contactOID = $contact['oid'];				
					$this->bind($i++,$contactOID);
					$this->bind($i++,$id);
				}
				$this->execute();
	
			}

			// Teams delete query.
			$teamsDeleteQuery = 'DELETE FROM teams ';
			$teamsDeleteQuery .= 'WHERE appID=:id';
			$this->query($teamsDeleteQuery);
			$this->bind(':id',$id);
			$this->execute();
	
			// Teams insert query.
			$teamNames = '';
			if (count($teams) > 0) {
	
				$teamsInsertQuery = 'INSERT INTO teams (teamsListID,appID) VALUES ';
				$values = array_fill(0,count($teams),'(?,?)');
				$teamsInsertQuery .= implode(',',$values);
				$this->query($teamsInsertQuery);
				$i = 1;
				foreach($teams AS $team) {
					$teamID = $team['id'];
					$teamName = $team['name'];
					$teamNames .= $teamName . ' <br />';				
					$this->bind($i++,$teamID);
					$this->bind($i++,$id);
				}
				$this->execute();
	
			}			
			
			// Used By delete query.
			$usedByDeleteQuery = 'DELETE FROM usedBy ';
			$usedByDeleteQuery .= 'WHERE appID=:id';
			$this->query($usedByDeleteQuery);
			$this->bind(':id',$id);
			$this->execute();
	
			// Used By insert query.
			if (count($usedBy) > 0) {
	
				$usedByInsertQuery = 'INSERT INTO usedBy (usedByListID,appID) VALUES ';
				$values = array_fill(0,count($usedBy),'(?,?)');
				$usedByInsertQuery .= implode(',',$values);
				$this->query($usedByInsertQuery);
				$i = 1;
				foreach($usedBy AS $group) {
					$groupID = $group['id'];			
					$this->bind($i++,$groupID);
					$this->bind($i++,$id);
				}
				$this->execute();
	
			}

			// Clusters delete query.
			$clustersDeleteQuery = 'DELETE FROM clusters ';
			$clustersDeleteQuery .= 'WHERE appID=:id';
			$this->query($clustersDeleteQuery);
			$this->bind(':id',$id);
			$this->execute();
	
			// Clusters insert query.
			if (count($clusters) > 0) {
	
				$clustersInsertQuery = 'INSERT INTO clusters (clustersListID,appID) VALUES ';
				$values = array_fill(0,count($clusters),'(?,?)');
				$clustersInsertQuery .= implode(',',$values);
				$this->query($clustersInsertQuery);
				$i = 1;
				foreach($clusters AS $cluster) {
					$clusterID = $cluster['id'];			
					$this->bind($i++,$clusterID);
					$this->bind($i++,$id);
				}
				$this->execute();
	
			}			
	
			// Servers delete query.
			$serversDeleteQuery = 'DELETE FROM servers ';
			$serversDeleteQuery .= 'WHERE appID=:id';
			$this->query($serversDeleteQuery);
			$this->bind(':id',$id);
			$this->execute();
	
			// Servers insert query.
			if (count($servers) > 0) {
	
				$serversInsertQuery = 'INSERT INTO servers (serverID,serverType,appID) VALUES ';
				$values = array_fill(0,count($servers),'(?,?,?)');
				$serversInsertQuery .= implode(',',$values);
				$this->query($serversInsertQuery);
				$i = 1;
				foreach($servers AS $server) {
					$serverID = $server['id'];
					$serverType = $server['type'];				
					$this->bind($i++,$serverID);
					$this->bind($i++,$serverType);
					$this->bind($i++,$id);
				}
				$this->execute();
	
			}	
	
			// Resources delete query.
			$resourcesDeleteQuery = 'DELETE FROM resources ';
			$resourcesDeleteQuery .= 'WHERE appID=:id';
			$this->query($resourcesDeleteQuery);
			$this->bind(':id',$id);
			$this->execute();
	
			// Resources insert query.
			if (count($resources) > 0) {
	
				$resourcesInsertQuery = 'INSERT INTO resources (resourcesListID,appID) VALUES ';
				$values = array_fill(0,count($resources),'(?,?)');
				$resourcesInsertQuery .= implode(',',$values);
				$this->query($resourcesInsertQuery);
				$i = 1;
				foreach($resources AS $resource) {
					$resourceID = $resource['id'];				
					$this->bind($i++,$resourceID);
					$this->bind($i++,$id);
				}
				$this->execute();
	
			}	
			
			// Dependencies delete query.
			$dependenciesDeleteQuery = 'DELETE FROM appDependencies ';
			$dependenciesDeleteQuery .= 'WHERE appID=:id';
			$this->query($dependenciesDeleteQuery);
			$this->bind(':id',$id);
			$this->execute();
	
			// Dependencies insert query.
			if (count($dependencies) > 0) {
	
				$dependenciesInsertQuery = 'INSERT INTO appDependencies (appID,dependentOn) VALUES ';
				$values = array_fill(0,count($dependencies),'(?,?)');
				$dependenciesInsertQuery .= implode(',',$values);
				$this->query($dependenciesInsertQuery);
				$i = 1;
				foreach($dependencies AS $dependency) {
					$dependencyID = $dependency['id'];				
					$this->bind($i++,$id);
					$this->bind($i++,$dependencyID);
				}
				$this->execute();
	
			}			
		
			// Log entry.
			if ($id) {
		
				$logQuery = 'INSERT INTO log (timestamp,staffOID,appID,action) VALUES ';
				$logQuery .= '(:timestamp,:staffOID,:appID,"UPDATE")';
				$this->query($logQuery);
				$this->bind(':timestamp',$timestamp);
				$this->bind(':staffOID',$submittedBy);
				$this->bind(':appID',$id);
				$this->execute();
		
			}		
		
			// If no issues with above queries, then commit the changes.
			$this->end();
			
			// Return data.
			return array('id'=>$id,'name'=>$name,'status'=>$status,'description'=>$description,'url'=>$url,'adminUrl'=>$adminUrl,'userDoc'=>$userDoc,'technicalDoc'=>$technicalDoc,'teams'=>$teamNames);
		
		} catch(PDOException $e) {
			$this->cancel();
			return array('error'=>'There was an issue editing the application.');
		}	

	}
	
	// Remove an app and all its associations.
	public function remove($submittedBy,$timestamp,$id) {
	
		try {
		
			// Begin transaction.
			$this->begin();
			
			// Remove app from dependencies.
			$appQuery = 'DELETE FROM appDependencies ';
			$appQuery .= 'WHERE dependentOn = :id';
			$this->query($appQuery);
			$this->bind(':id',$id);
			$this->execute();			
			
			// Set status of app to removed.
			$deleteQuery = 'UPDATE apps ';
			$deleteQuery .= 'SET status = "Removed" ';
			$deleteQuery .= 'WHERE id = :id';
			$this->query($deleteQuery);
			$this->bind(':id',$id);
			$this->execute();
			
			// Log entry.
			if ($id) {
		
				$logQuery = 'INSERT INTO log (timestamp,staffOID,appID,action) VALUES ';
				$logQuery .= '(:timestamp,:staffOID,:appID,"REMOVE")';
				$this->query($logQuery);
				$this->bind(':timestamp',$timestamp);
				$this->bind(':staffOID',$submittedBy);
				$this->bind(':appID',$id);
				$this->execute();
		
			}			

			// If no issues with above queries, then commit the changes.
			$this->end();
			
			// Return data.
			return array('id'=>$id);			
		
		} catch(PDOException $e) {
			$this->cancel();
			return array('error'=>'There was an issue removing the application.');
		}		
	
	}	
	
	// Get staff for type ahead lookup.
	public function staffLookup($term) {
	
		// Query database.
		$query = 'SELECT master.oid AS id, LOWER(ename.nameValue) AS ename, name.nameValue AS text ';
		$query .= 'FROM coredata_read.coreUser_master AS master ';
		$query .= 'LEFT JOIN (SELECT oid,nameValue FROM coredata_read.coreUser_name WHERE nameType="alias") AS alias ON alias.oid = master.oid ';	
		$query .= 'LEFT JOIN (SELECT oid,nameValue FROM coredata_read.coreUser_name WHERE nameType="display") AS name ON name.oid = master.oid ';
		$query .= 'LEFT JOIN (SELECT oid,nameValue FROM coredata_read.coreUser_name WHERE nameType="ename") AS ename ON ename.oid = master.oid ';
		$query .= 'LEFT JOIN (SELECT oid,nameValue FROM coredata_read.coreUser_name WHERE nameType="sort") AS sort ON sort.oid = master.oid ';
		$query .= 'LEFT JOIN (SELECT oid,phoneValue FROM coredata_read.coreUser_phone WHERE phoneType="office") AS phone ON phone.oid = master.oid ';
		$query .= 'WHERE master.userStatus = "Active" AND (alias.nameValue LIKE :term OR name.nameValue LIKE :term OR ename.nameValue LIKE :term OR phone.phoneValue LIKE :term) ';
		$query .= 'ORDER BY sort.nameValue';
		$this->query($query);
		$this->bind(':term','%'.$term.'%');
		$result = $this->resultSet();

		// Return result.
		return $result;
	
	}
	
	// Get technical contact information.
	public function technicalContactInfo($oid) {
	
		// Query database.
		$query = 'SELECT image.imageLink AS photo, name.nameValue AS name, CONCAT(LOWER(ename.nameValue),"@meditech.com") AS email, userTitle AS title, userJob AS job, userDivision AS division, phone.phoneValue AS phone, building.name AS building, mailstop.name AS mailstop, status.useractivitystatus AS status, status.useractivitydetail AS statusDetail, status.useractivityenddate AS returnDate, status.useractivityalternatephone AS alternatePhone, superiorName.nameValue AS superiorName ';
		$query .= 'FROM coredata_read.coreUser_master AS master ';
		$query .= 'LEFT JOIN (SELECT oid,imageLink FROM coredata_read.coreUser_image) AS image ON image.oid = master.oid ';
		$query .= 'LEFT JOIN (SELECT oid,nameValue FROM coredata_read.coreUser_name WHERE nameType="display") AS name ON name.oid = master.oid ';
		$query .= 'LEFT JOIN (SELECT oid,nameValue FROM coredata_read.coreUser_name WHERE nameType="ename") AS ename ON ename.oid = master.oid ';
		$query .= 'LEFT JOIN (SELECT oid,nameValue FROM coredata_read.coreUser_name WHERE nameType="sort") AS sort ON sort.oid = master.oid ';
		$query .= 'LEFT JOIN (SELECT oid,nameValue FROM coredata_read.coreUser_name WHERE nameType="display") AS superiorName ON superiorName.oid = master.userSuperior ';
		$query .= 'LEFT JOIN (SELECT oid,phoneValue FROM coredata_read.coreUser_phone WHERE phoneType="office") AS phone ON phone.oid = master.oid ';
		$query .= 'LEFT JOIN (SELECT oid,placeBuilding,placeAddress FROM coredata_read.coreUser_place) AS location ON location.oid = master.oid ';
		$query .= 'LEFT JOIN (SELECT oid,name FROM coredata_read.corePlaces_main) AS building ON building.oid = location.placeBuilding ';
		$query .= 'LEFT JOIN (SELECT oid,name FROM coredata_read.corePlaces_main) AS mailstop ON mailstop.oid = location.placeAddress ';
		$query .= 'LEFT JOIN (SELECT oid,useractivitystatus,useractivitydetail,useractivityenddate,useractivityalternatephone FROM coredata_read.coreUser_userLocationLog) AS status ON status.oid = master.oid ';
		$query .= 'WHERE master.oid = :oid';
		$this->query($query);
		$this->bind(':oid',$oid);
		$result = $this->single();
		
		// Return result.
		return $result;
	
	}
	
	// Search apps.
	public function search($name,$status,$technicalContacts,$teams,$usedBy,$clusters,$servers,$resources,$dependencies) {

		// Define variables.
		$apps = array();			
		$vm = new VM();
		$where = '';
		$i = 1;	
		
		// Include status search.
		if ($status && strtoupper($status) == 'ALL') {
			$where .= 'apps.status IS NULL OR apps.status != "Removed" ';		
		} else {
			$where .= 'apps.status = ? ';
		}	

		// Include name search.
		if ($name) {
			$where .= 'AND apps.name LIKE ? ';
		}		
		
		// Include technical contacts search.
		if (count($technicalContacts) > 0) {
			$where .= 'AND (';
			$values = array_fill(0,count($technicalContacts),'technicalContactOIDs LIKE ?');
			$where .= implode(' OR ',$values);
			$where .= ') ';
		}
		
		// Include teams search.
		if (count($teams) > 0) {
			$where .= 'AND (';
			$values = array_fill(0,count($teams),'FIND_IN_SET(?,teamsListIDs) > 0');
			$where .= implode(' OR ',$values);
			$where .= ') ';
		}			
		
		// Include used by search.
		if (count($usedBy) > 0) {
			$where .= 'AND (';
			$values = array_fill(0,count($usedBy),'FIND_IN_SET(?,usedByListIDs) > 0');
			$where .= implode(' OR ',$values);
			$where .= ') ';
		}	

		// Include clusters search.
		if (count($clusters) > 0) {
			$where .= 'AND (';
			$values = array_fill(0,count($clusters),'FIND_IN_SET(?,clustersListIDs) > 0');
			$where .= implode(' OR ',$values);
			$where .= ') ';
		}		
		
		// Include servers search.
		if (count($servers) > 0) {
			$where .= 'AND (';
			$values = array_fill(0,count($servers),'FIND_IN_SET(?,serverIDs) > 0 OR FIND_IN_SET(?,clusterServerIDs) > 0');
			$where .= implode(' OR ',$values);
			$where .= ') ';
		}	

		// Include resources search.
		if (count($resources) > 0) {
			$where .= 'AND (';
			$values = array_fill(0,count($resources),'FIND_IN_SET(?,resourcesListIDs) > 0');
			$where .= implode(' OR ',$values);
			$where .= ') ';
		}	

		// Include dependencies search.
		if (count($dependencies) > 0) {
			$where .= 'AND (';
			$values = array_fill(0,count($dependencies),'FIND_IN_SET(?,dependencyIDs) > 0');
			$where .= implode(' OR ',$values);
			$where .= ') ';
		}		
	
		// App query.
		$appQuery = 'SELECT ';
		$appQuery .= 'apps.id, ';
		$appQuery .= 'apps.name, ';
		$appQuery .= 'apps.status, ';
		$appQuery .= 'teams, ';
		$appQuery .= 'usedBy, ';
		$appQuery .= 'clusters, ';
		$appQuery .= 'resources, ';
		$appQuery .= 'dependentOn ';
		$appQuery .= 'FROM ';
		$appQuery .= '(SELECT apps.id, apps.name, apps.status, ';
		$appQuery .= 'GROUP_CONCAT(DISTINCT technicalContacts.staffOID SEPARATOR "~") AS technicalContactOIDs, ';
		$appQuery .= 'GROUP_CONCAT(DISTINCT CONCAT(teamsListID," ") SEPARATOR ",") AS teamsListIDs, ';
		$appQuery .= 'GROUP_CONCAT(DISTINCT teamsList.name SEPARATOR " <br />") AS teams, ';
		$appQuery .= 'GROUP_CONCAT(DISTINCT CONCAT(usedByListID," ") SEPARATOR ",") AS usedByListIDs, ';
		$appQuery .= 'GROUP_CONCAT(DISTINCT usedByList.name SEPARATOR " <br />") AS usedBy, ';
		$appQuery .= 'GROUP_CONCAT(DISTINCT CONCAT(clustersListID," ") SEPARATOR ",") AS clustersListIDs, ';
		$appQuery .= 'GROUP_CONCAT(DISTINCT clustersList.name SEPARATOR " <br />") AS clusters, ';
		$appQuery .= 'GROUP_CONCAT(DISTINCT CONCAT(clusterServers.serverID," ") SEPARATOR ",") AS clusterServerIDs, ';
		$appQuery .= 'GROUP_CONCAT(DISTINCT CONCAT(servers.serverID," ") SEPARATOR ",") AS serverIDs, ';
		$appQuery .= 'GROUP_CONCAT(DISTINCT CONCAT(resourcesListID," ") SEPARATOR ",") AS resourcesListIDs, ';
		$appQuery .= 'GROUP_CONCAT(DISTINCT resourcesList.name SEPARATOR " <br />") AS resources, ';		
		$appQuery .= 'GROUP_CONCAT(DISTINCT CONCAT(appDependencies.dependentOn," ") SEPARATOR ",") AS dependencyIDs, ';
		$appQuery .= 'GROUP_CONCAT(DISTINCT appDependencyNames.name SEPARATOR " <br />") AS dependentOn ';		
		$appQuery .= 'FROM apps ';
		$appQuery .= 'LEFT JOIN technicalContacts ON apps.id = technicalContacts.appID ';
		$appQuery .= 'LEFT JOIN teams ON apps.id = teams.appID ';
		$appQuery .= 'LEFT JOIN teamsList ON teams.teamsListID = teamsList.id ';
		$appQuery .= 'LEFT JOIN usedBy ON apps.id = usedBy.appID ';
		$appQuery .= 'LEFT JOIN usedByList ON usedBy.usedByListID = usedByList.id ';		
		$appQuery .= 'LEFT JOIN clusters ON apps.id = clusters.appID ';
		$appQuery .= 'LEFT JOIN clustersList ON clusters.clustersListID = clustersList.id ';
		$appQuery .= 'LEFT JOIN clusterServers ON clustersList.id = clusterServers.clusterID ';
		$appQuery .= 'LEFT JOIN servers ON apps.id = servers.appID ';
		$appQuery .= 'LEFT JOIN resources ON apps.id = resources.appID ';
		$appQuery .= 'LEFT JOIN resourcesList ON resources.resourcesListID = resourcesList.id ';
		$appQuery .= 'LEFT JOIN appDependencies ON apps.id = appDependencies.appID ';
		$appQuery .= 'LEFT JOIN apps AS appDependencyNames ON appDependencies.dependentOn = appDependencyNames.id ';
		$appQuery .= 'GROUP BY apps.id) AS apps ';
		$appQuery .= 'WHERE ' . $where . ' ';
		$appQuery .= 'ORDER BY apps.name';
		$this->query($appQuery);

		// If a status was provided, bind the value.
		if ($status && strtoupper($status) != 'ALL') { 
			$this->bind($i++,$status); 
		}			
		
		// If a name was provided, bind the value.
		if ($name) { 
			$this->bind($i++,'%'.$name.'%'); 
		}		
		
		// If technical contacts were provided, bind the values.
		if (count($technicalContacts) > 0) {
			foreach($technicalContacts AS $technicalContact) {
				$oid = str_replace('stafflink\\','',$technicalContact['oid']);
				$this->bind($i++,'%'.$oid.'%');
			}		
		}
		
		// If teams were provided, bind the values.
		if (count($teams) > 0) {
			foreach($teams AS $team) {
				$id = $team['id'];
				$this->bind($i++,$id.' ');
			}		
		}		
	
		// If used by groups were provided, bind the values.
		if (count($usedBy) > 0) {
			foreach($usedBy AS $group) {
				$id = $group['id'];
				$this->bind($i++,$id.' ');
			}		
		}	
		
		// If clusters were provided, bind the values.
		if (count($clusters) > 0) {
			foreach($clusters AS $cluster) {
				$id = $cluster['id'];
				$this->bind($i++,$id.' ');
			}		
		}
		
		// If servers were provided, bind the values.
		if (count($servers) > 0) {
			foreach($servers AS $s) {
				$id = $s['id'];
				$this->bind($i++,$id.' ');
				$this->bind($i++,$id.' ');
			}		
		}
		
		// If resources were provided, bind the values.
		if (count($resources) > 0) {
			foreach($resources AS $resource) {
				$id = $resource['id'];
				$this->bind($i++,$id.' ');
			}		
		}	

		// If dependencies were provided, bind the values.
		if (count($dependencies) > 0) {
			foreach($dependencies AS $dependency) {
				$id = $dependency['id'];
				$this->bind($i++,$id.' ');
			}		
		}		

		// Get the query result.
		$appResult = $this->resultSet();

		if (count($appResult) > 0) {

			// Loop through apps.			
			foreach($appResult AS $app) {
				
				// Define variables.
				$appID = $app['id'];
				$technicalContactInfo = '';
				$vmInfo = '';
				
				// Technical contacts query.
				$technicalContactsQuery = 'SELECT name.oid AS oid, name.nameValue AS name ';
				$technicalContactsQuery .= 'FROM technicalContacts ';
				$technicalContactsQuery .= 'INNER JOIN coredata_read.coreUser_name AS name ON technicalContacts.staffOID = name.oid ';
				$technicalContactsQuery .= 'WHERE appID = :appID AND name.nameType = "display"';
				$this->query($technicalContactsQuery);
				$this->bind(':appID',$appID);
				$technicalContacts = $this->resultSet();
				
				// Build technical contacts info.
				if (count($technicalContacts) > 0) {
					foreach($technicalContacts AS $contact) {
						$contactOID = $contact['oid'];
						$contactName = $contact['name'];
						$technicalContactInfo .= '<span title="View Technical Contact Info" id="technicalContact'.$contactOID.'" class="fa fa-user fa-lg technicalContactInfo"></span> '.$contactName.'<br />';
					}
				}
				
				// Add technical contacts info to app.
				$app['technicalContacts'] = $technicalContactInfo;	

				// Physical/Other Servers query.
				$serverQuery = 'SELECT servers.serverID AS id, serversList.name, servers.serverType AS type ';
				$serverQuery .= 'FROM servers ';
				$serverQuery .= 'INNER JOIN serversList ON servers.serverID = serversList.id ';
				$serverQuery .= 'WHERE appID = :appID AND serverType != "VM"';
				$this->query($serverQuery);
				$this->bind(':appID',$appID);
				$servers = $this->resultSet();
				
				// Build server info.
				$serverInfo = '';
				if (count($servers) > 0) {
					foreach($servers AS $server) {
						$serverID = $server['id'];
						$serverName = $server['name'];
						$serverType = $server['type'];
						$serverInfo .= '<span title="View Server Info" id="server'.$serverID.'" data-type="'.$serverType.'" class="fa fa-server fa-lg serverInfo"></span> '.$serverName.'<br />';
					}					
				}
				
				// VM query.
				$vmQuery = 'SELECT serverID AS id ';
				$vmQuery .= 'FROM servers ';
				$vmQuery .= 'WHERE appID = :appID AND serverType = "VM"';
				$this->query($vmQuery);
				$this->bind(':appID',$appID);
				$vmIDs = $this->resultArray();
				
				// Get VM names.
				$vmIDs = implode(',',$vmIDs);
				$vmInfo = '';
				if ($vmIDs) {
					$names = $vm->getNames($vmIDs);
					foreach($names AS $name) {
						$vmID = $name['id'];
						$vmName = $name['name'];
						$vmInfo .= '<span title="View Server Info" id="server'.$vmID.'" data-type="VM" class="fa fa-server fa-lg serverInfo"></span> '.$vmName.'<br />';
					}
				}
				
				// Combine server/vm info and add it to app.
				$app['servers'] = $serverInfo . $vmInfo;
				
				// Build apps array.
				array_push($apps,$app);
				
			}		
		
		}
		
		// Return apps.
		return $apps;

	}
	
	// Get entries for type ahead lookup.
	public function lookup($id,$term) {
	
		// Query database.
		$query = 'SELECT id, name AS text ';
		$query .= 'FROM apps ';
		$query .= 'WHERE ';
		if ($id) { $query .= 'id != :id AND '; }
		$query .= 'status != "Inactive" AND status != "Removed" AND name LIKE :term ';		
		$query .= 'ORDER BY name';
		$this->query($query);
		if ($id) { $this->bind(':id',$id); }
		$this->bind(':term','%'.$term.'%');
		$result = $this->resultSet();

		// Return result.
		return $result;
	
	}

	// Get single row for the coreUser display name by OID
	public function getDisplayNameByOid($oid) {
	
		$query = 'SELECT oid,nameValue FROM coredata_read.coreUser_name where oid = :oid and nameType = "display"';
		$this->query($query);
		$this->bind(':oid',$oid);
		$result = $this->single();
		// Return result.
		return $result;
	
	}

	// Get batch result for the coreUser display name by List of OID
	public function getDisplayNameByOidBatch($listOid) {

        foreach ($listOid as $key => $value)
            $listOid[$key] = addslashes($value);            
        $comma_separated = implode("','", $listOid);
        $comma_separated = "'".$comma_separated."'";
        
		//$query = 'SELECT oid,nameValue FROM coredata_read.coreUser_name where oid IN (:list_oid) and nameType = "display"';
		$query = 'SELECT oid,nameValue FROM coredata_read.coreUser_name where oid IN (' . $comma_separated . ') and nameType = "display"';
        $this->query($query);
		$result = $this->resultSet();

		// Return result.                    
		return $result;
    
	}       

    //ATWEB-6956 - Add restrict to WU Site to advanced search
    // return WU sites search by sitename and wuversion
	public function wuSiteLookup($term,$server) {
        $comma_separated = implode("','", $server);
        $comma_separated = "'".$comma_separated."'";
		$query = 'SELECT idsites as id,sitename as text FROM webutility.sites where resource IN (' . $comma_separated . ') and wuversion = "LIVE" and sitename like "%' . $term .'%"';
        $this->query($query);
		$result = $this->resultSet();
		// Return result.                    
		return $result;
    
	}  

    //ATWEB-6956 - Add restrict to WU Site to advanced search
    // return WU sites search by idsites
	public function wuSiteLookupByOidBatch($listSite) {          
        $comma_separated = implode("','", $listSite);
        $comma_separated = "'".$comma_separated."'";
		$query = 'SELECT idsites as id,sitename as text FROM webutility.sites where idsites IN (' . $comma_separated . ') and wuversion = "LIVE"';
        $this->query($query);
		$result = $this->resultSet();
		// Return result.                    
		return $result;
    
	}     
}

?>