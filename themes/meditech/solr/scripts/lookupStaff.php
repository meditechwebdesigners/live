<?php 


// Define required files.
require_once 'autoload.php';
	
// Define variables.
$term = $_POST['term'];
$type = $_POST['type'];


// Get matches.
$app = new App();

if ($type == 'lookahead') {
    $results = $app->staffLookup($term);
}
else if ($type == 'oid') {
    $results = $app->getDisplayNameByOid($term);
}
else if ($type == 'list_oid') {
    $results = $app->getDisplayNameByOidBatch($term);
}
//gzip content
ob_start("ob_gzhandler");
header('Content-Type: application/json');
echo json_encode(array('results'=>$results));
ob_end_flush();

?>