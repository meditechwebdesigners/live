<!-- START simple-page--node-4214.php -->
<?php // This template is set up to control the display of the campaign: COVID-19
$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');
$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<div class="js__seo-tool__body-content">

    <style>
        .show-less {
            height: 500px;
        }

        #news .show-more-container {
            left: -5px;
        }

        .panel.side-menu {
            background: none;
            padding: 0 1em;
            margin-bottom: 1em;
        }

        .panel.side-menu h3 {
            margin-top: 0;
        }

        .panel.side-menu ul.menu {
            padding: 0;
            list-style-type: none;
        }

        .panel.side-menu ul.menu li {
            padding: .6em 0 .5em 0;
            margin-bottom: 0;
            border-bottom: 1px solid #ccc;
        }

        .panel.side-menu ul.menu li:last-child {
            border-bottom: none;
        }

        .panel.side-menu ul.menu li:hover {
            background: #F2F2F2;
        }

        .panel.side-menu ul.menu li a {
            padding-left: .5em;
        }

        .squish-list li {
            margin-bottom: .75em;
            line-height: 1.5em;
        }

        .hero-image-container {
            position: relative;
        }

        .hero-image-container img {
            border-radius: 0.5em;
            margin-top: 2em;
            max-width: 100%;
            z-index: 1;
        }

        .background-rectangle-left {
            content: "";
            position: absolute;
            border-radius: 7px;
            height: 108%;
            width: 95%;
            z-index: -1;
            top: 0;
            bottom: 0;
            left: -2em;
            right: 4em;
        }

        .space--left {
            padding-left: 2em;
        }

        @media all and (max-width: 50em) {
            .background-rectangle-left {
                display: none;
            }

            .space--left {
                padding-left: 0;
            }
        }

    </style>


    <div class="js__seo-tool__body-content">

        <!-- Block 1 -->
        <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/covid-19--illustrated-render-of-virus.jpg);">
            <div class="container__centered">

                <div class="container__one-half text--white" style="padding: 2em 1em;">
                    <h1 class="js__seo-tool__title" style="margin-top:0;"><?php print $title; ?></h1>
                    <a href="https://customer.meditech.com/en/d/covid19resources/homepage.htm" role="button" class="btn--orange" style="margin: 1em 0;">COVID-19 Resource Page for MEDITECH Customers</a>
                </div>

            </div>
        </div>
        <!-- End of Block 1 -->


        <!-- Announcements -->
        <div class="container">
            <div class="container__centered">

                <div id="announcements" class="container__two-thirds" style="margin-bottom:2em;">
                    <div class="show-less auto-margins" data-height="750" style="height:750px;">
                        <div>
                            <p>MEDITECH is committed to supporting our customers, partners, and staff through these demanding times. Check back often for our latest updates and strategies related to the COVID-19 pandemic, and learn how our customers are supporting their patients, staff, and community. MEDITECH customers can access our latest Guidance Documents from our <a href="https://customer.meditech.com/en/d/covid19resources/homepage.htm">Client Services COVID-19 Resource page</a>.</p>

                            <h2 style="margin-top:2em;">Announcements</h2>
                            <ul>
                                <li><a href="https://ehr.meditech.com/news/meditech-ranked-among-top-vendors-by-klas-for-covid-19-response">MEDITECH ranked among top vendors by KLAS for COVID-19 response</a> (4/8/21)</li>
                                <li><a href="https://ehr.meditech.com/news/vaccine-eligibility-checking-now-included-in-meditech-patient-portal">Vaccine eligibility checking now included in MEDITECH patient portal</a> (3/18/21)</li>
                                <li><a href="https://ehr.meditech.com/news/ten-ways-meditech-supports-the-covid-19-vaccine-rollout">Ten Ways MEDITECH Supports the COVID-19 Vaccine Rollout</a> (1/14/20)</li>
                                <li><a href="<?php print $url; ?>/news/meditech-quick-vaccination-solution-streamlines-covid-19-vaccination-process">MEDITECH Quick Vaccination solution streamlines COVID-19 vaccination process</a> (1/13/21)</li>
                                <li><a href="<?php print $url; ?>/news/meditech-participates-in-hhs-webinar-on-covid-19-hospital-data-reporting">MEDITECH participates in HHS webinar on COVID-19 hospital data reporting</a> (12/22/20)</li>
                                <li><a href="<?php print $url; ?>/news/meditech-adds-new-faqs-page-to-covid-19-vaccine-guidance">MEDITECH adds new FAQs page to COVID-19 vaccine guidance</a> (12/11/20)</li>
                                <li><a href="https://blog.meditech.com/managing-the-dual-threat-of-flu-and-covid-19-with-level-of-care-guidance">Managing the dual threat of flu and COVID-19 with level of care guidance</a> (10/20/20)</li>
                                <li><a href="https://blog.meditech.com/on-bostons-front-line-of-covid-19">On Boston's front line of COVID-19</a> (8/18/20)</li>
                                <li><a href="https://customer.meditech.com/en/d/covid19regulatory/homepage.htm">LAB Data Reporting (SQL) and ELR Guidance for HHS/CARES Act Now Available</a> (7/31/20)</li>
                                <li><a href="<?php print $url; ?>/news/meditech-releases-updated-decision-support-and-guidance-for-coronavirus-revised-070920">MEDITECH Releases Updated Decision Support and Guidance for Coronavirus</a> (7/9/20)</li>
                                <li><a href="<?php print $url; ?>/news/google-and-hca-announce-the-national-response-portal-for-tracking-disease-hot-spots">Google and HCA announce the National Response Portal for tracking disease hot spots</a> (6/1/20)</li>
                                <li><a href="<?php print $url; ?>/news/meditech-assists-customers-with-software-and-support-to-help-combat-covid-19">MEDITECH Assists Customers with Software and Support to Help Combat COVID-19</a> (5/19/20)</li>
                                <li><a href="<?php print $url; ?>/news/additional-funds-released-through-the-cares-act">Additional Funds Released Through the CARES Act</a> (4/24/20)</li>
                                <li><a href="<?php print $url; ?>/news/first-databank-offers-covid-19-temporary-facility-access-support">First Databank Offers COVID-19 Temporary Facility Access Support</a> (4/13/20)</li>
                                <li><a href="<?php print $url; ?>/news/medpower-collaborates-with-meditech-to-help-deliver-complimentary-covid-19-online-training">MedPower Collaborates with MEDITECH to Help Deliver Complimentary COVID-19 Online Training</a> (4/8/20)</li>
                                <li><a href="<?php print $url; ?>/news/meditech-offers-expanse-ambulatory-customers-its-scheduled-virtual-visits-functionality-to-0">MEDITECH Extends Virtual Visits Offer to All Customers</a> (3/27/20)</li>
                                <li><a href="<?php print $url; ?>/news/risk-assessments-help-pre-screen-home-care-patients-for-covid-19">Risk Assessments Help Pre-Screen Home Care Patients for COVID-19</a> (3/24/20)</li>
                            </ul>
                        </div>
                    </div>
                    <button class="show-more">Show More</button>
                </div>
                <div class="container__one-third">
                    <div class="panel side-menu">
                        <h3>COVID-19 Content</h3>
                        <ul class="menu">
                            <li><a href="<?php print $url; ?>/covid-19-webinars">COVID-19 Webinars</a></li>
                            <li><a href="https://blog.meditech.com/topic/coronavirus">Thought Leadership</a></li>
                            <li><a href="#solutions-strategies">Solutions &amp; Strategies</a></li>
                            <li><a href="#customer-experiences">Customer Experiences</a></li>
                            <li><a href="<?php print $url; ?>/meditech-covid-19-control-plan-tenants-vendors-visitors">Tenants, Vendors & Visitors</a></li>
                        </ul>
                    </div>
                    <div class="panel side-menu">
                        <h3>Industry Resources</h3>
                        <ul class="menu">
                            <li><a href="https://www.who.int/emergencies/diseases/novel-coronavirus-2019">WHO COVID-19 Outbreak Website</a></li>
                            <li><a href="https://www.cdc.gov/coronavirus/2019-nCoV/index.html">CDC COVID-19 Website</a></li>
                            <li><a href="https://ipac-canada.org/coronavirus-resources.php">ipac Canada</a></li>
                            <li><a href="https://chimecentral.org/category/media/covid-19/">CHIME</a></li>
                            <li><a href="<?php print $url; ?>/covid-19-industry-resources">Other COVID-19 Industry Resources</a></li>
                            <li><a href="https://customer.meditech.com/en/d/covid19resources/pages/covid19collaborativevendorresources.htm">Collaborative Vendor Solutions</a></li>
                        </ul>
                    </div>
                </div>

            </div>
            <div id="solutions-strategies"></div>
        </div>
        <!-- End of Announcements -->


        <!-- Block 3 -->
        <div class="container bg--blue-gradient">
            <div class="container__centered">

                <div class="container__one-half">
                    <h2>Leveraging MEDITECH's EHR During the COVID-19 Pandemic</h2>
                    <p>Leverage your EHR to help screen, isolate, treat, monitor, and report on patients with suspected or confirmed cases of COVID-19, as well as the guidance and services that MEDITECH provides.</p>
                    <a href="<?php print $url; ?>/leveraging-meditechs-ehr-during-the-covid-19-pandemic" class="btn--orange" style="margin-bottom:1em;">Learn How</a>
                </div>

                <div class="container__one-half space--left">
                    <h3>MEDITECH Solutions &amp; Strategies</h3>
                    <ul>
                        <li><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/standalonevirtualvisitrapidresponseplan.pdf">Expanded Roll Out Virtual Visits Rapid Response Plan</a></li>
                        <li><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/virtualvisitsrapidreponseimpplan.pdf">Ambulatory Virtual Visits Rapid Response Plan</a></li>
                        <li><a href="https://customer.meditech.com/en/d/covid19resources/pages/covid19faq.htm">Customer Insights From the Field (FAQs)</a></li>
                    </ul>
                </div>

            </div>

            <div class="container__centered" style="padding: 2em 10% 0; border-top: 1px solid white; margin-top: 2em;">
                <?php include('inc-virtual-care-minutes.php'); ?>
            </div>

            <div id="customer-experiences"></div>
        </div>
        <!-- End of Block 3  -->


        <!-- Customer Experiences -->
        <div class="container no-pad--bottom" style="margin-bottom:1em;">
            <div class="container__centered">

                <div class="container__one-half">
                    <h2>Customer Experiences</h2>
                    <p>Healthcare organizations across the world are undertaking new strategies to improve safety for clinicians and patients, as well as prevent further spread of COVID-19. Many MEDITECH customers have gone above and beyond during this crisis, to innovate and improve upon care delivery processes for their communities. Here are some of their stories.</p>
                </div>
                <div class="container__one-half space--left">
                    <div class="hero-image-container">
                        <img src="<?php print $url; ?>/sites/default/files/styles/main-article-image-breakpoints_theme_meditech_big-medium_2x/public/images/news/MEDITECH-Screening-Protocol-Coronavirus.jpg?itok=EarLclpM&timestamp=1583416253" alt="female doctor wearing protective face mask checking blood results on tablet computer">
                        <div class="background-rectangle-left" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/background-pattern--virus--orange.png); background-repeat: no-repeat; background-size: 90%;"></div>
                    </div>
                </div>

            </div>
            <div id="customer-strategies"></div>
        </div>
        <!-- End of Customer Experiences -->


        <!-- Customer Strategies -->
        <div class="container no-pad--top no-margin--top">
            <div class="container__centered">
                <h3>Customer Strategies</h3>
                <div>
                    <div class="container__one-half">
                        <ul class="squish-list">
                            <li>Customers significantly <a href="https://ehr.meditech.com/news/customers-significantly-reduce-covid-19-vaccine-documentation-time-with-meditechs-quick"><strong>reduce COVID-19 vaccine documentation time</strong></a> with MEDITECH's Quick Vaccination solution (4/9/21)</li>
                            <li>The Valley Hospital Uses MEDITECH's Surveillance Solution to <a href="MEDITECH ranked among top vendors by KLAS for COVID-19 response"><strong>Monitor and Manage COVID-19 Patients</strong></a> (4/7/21)</li>
                            <li>Med Center Health's <a href="https://ehr.meditech.com/news/med-center-healths-vaccination-process-supports-high-volume-and-eliminates-waste"><strong>vaccination process supports high volume and eliminates waste</strong></a> (2/24/21)</li>
                            <li><a href="https://blog.meditech.com/using-a-vaccine-workflow-in-the-fight-against-covid-19"><strong>Using a vaccine workflow in the fight against COVID-19</strong></a> at Northwestern Medical Center (2/19/21)</li>
                            <li><a href="https://blog.meditech.com/advancing-digital-health-solutions-beyond-the-pandemic"><strong>Advancing digital health solutions</strong></a> beyond the pandemic (1/5/21)</li>
                            <li>Kalispell Regional Healthcare becomes the <a href="https://ehr.meditech.com/news/kalispell-regional-healthcare-becomes-the-first-hospital-in-montana-to-offer-advanced-covid-19"><strong>first hospital in Montana to offer advanced COVID-19 testing</strong></a> (12/28/20)</li>
                            <li>Managing a crisis with <a href="https://blog.meditech.com/managing-a-crisis-with-covid-19-drive-thru-testing-at-lawrence-general-hospital"><strong>COVID-19 drive-thru testing</strong></a> at Lawrence General Hospital (12/8/20)</li>
                            <li>Thought Leader Podcast: <a href="https://blog.meditech.com/virtual-care-in-the-age-of-covid-19-and-beyond"><strong>Virtual Care</strong></a> in the Age of COVID-19 and Beyond (6/30/20)</li>
                            <li>How MEDITECH Customers Are <a href="<?php print $url; ?>/news/how-meditech-customers-are-providing-mental-health-support-during-the-covid-19-pandemic"><strong>Providing Mental Health Support</strong></a> during the COVID-19 Pandemic (6/12/20)</li>
                            <li>MEDITECH Customers Offer <a href="<?php print $url; ?>/news/meditech-customers-offer-convalescent-plasma-therapy"><strong>Convalescent Plasma Therapy</strong></a> (6/10/20)</li>
                            <li><a href="<?php print $url; ?>/news/remote-ehr-implementation-enables-meditech-organizations-to-go-live-amid-covid-19-pandemic"><strong>Remote EHR Implementation</strong></a> Enables MEDITECH Organizations to Go LIVE Amid COVID-19 Pandemic (6/9/20)</li>
                            <li>Newton Medical Center uncovers <a href="<?php print $url; ?>/news/newton-medical-center-uncovers-covid-19-insights-with-meditechs-bca-solution"><strong>COVID-19 insights with MEDITECH's BCA solution</strong></a> (6/5/20)</li>
                            <li>Reflections from the frontlines: <a href="https://blog.meditech.com/reflections-from-the-frontlines-combating-covid19-in-boston-and-nyc-0"><strong>combating COVID-19 in Boston and NYC</strong></a> (6/4/20)</li>
                            <li>Cayuga Health System's <a href="<?php print $url; ?>/news/cayuga-health-systems-covid-19-response-is-new-york-strong"><strong>COVID-19 response</strong></a> is 'New York Strong' (6/2/20)</li>
                        </ul>
                    </div>
                    <div class="container__one-half">
                        <ul class="squish-list">
                            <li>From a Safe Distance: <a href="<?php print $url; ?>/news/from-a-safe-distance-remote-strategies-for-addressing-patient-health-concerns-during-covid-19"><strong>Remote Strategies</strong></a> for Addressing Patient Health Concerns During COVID-19 (5/22/20)</li>
                            <li>Queensway Advances its Use of MEDITECH for <a href="<?php print $url; ?>/news/queensway-advances-its-use-of-meditech-for-covid-19-clinic"><strong>COVID-19 Clinic</strong></a> (5/21/20)</li>
                            <li>How MEDITECH Customers Worldwide are <a href="<?php print $url; ?>/news/how-meditech-customers-worldwide-are-meeting-the-challenges-of-the-covid-19-pandemic"><strong>Meeting the Challenges</strong></a> of the COVID-19 Pandemic (5/12/20)</li>
                            <li>Healthcare Organizations Take Steps to <a href="<?php print $url; ?>/news/healthcare-organizations-take-steps-to-increase-patient-capacity-during-covid-19"><strong>Increase Patient Capacity</strong></a> During COVID-19 (4/30/20)</li>
                            <li>Firelands Regional Health System <a href="<?php print $url; ?>/news/firelands-regional-health-system-monitors-covid-19-patients-with-meditechs-bca-solution"><strong>Monitors COVID-19 Patients with MEDITECH's BCA Solution</strong></a> (4/30/20)</li>
                            <li>Virtual care delivers on <a href="https://blog.meditech.com/virtual-care-delivers-on-top-priority-for-patient-experience-safety"><strong>top priority for patient experience—safety</strong></a> (4/29/20)</li>
                            <li>Healthcare Organizations <a href="<?php print $url; ?>/news/healthcare-organizations-team-up-to-fight-covid-19"><strong>Team Up To Fight COVID-19</strong></a> (4/28/20)</li>
                            <li>Holyoke Medical Center promotes <a href="<?php print $url; ?>/news/holyoke-medical-center-promotes-community-health-during-covid-19"><strong>community health during COVID-19</strong></a> (4/24/20)</li>
                            <li><a href="https://blog.meditech.com/how-virtual-visits-are-keeping-providers-and-patients-safe-at-citizens-memorial-healthcare"><strong>How virtual visits are keeping providers and patients safe</strong></a> at Citizen's Memorial Healthcare (4/10/20)</li>
                            <li>MEDITECH customers find new <a href="<?php print $url; ?>/news/meditech-customers-find-new-ways-to-address-ppe-shortages"><strong>ways to address PPE shortages</strong></a> (4/9/20)</li>
                            <li><a href="<?php print $url; ?>/news/virtual-visits-help-communities-get-medical-care-practice-social-distancing"><strong>Virtual Visits</strong></a> help communities get medical care, practice social distancing (4/9/20)</li>
                            <li>Healthcare organizations implement <a href="https://blog.meditech.com/healthcare-organizations-implement-remote-testing-in-light-of-covid-19-0"><strong>remote testing procedures</strong></a> (4/7/20)</li>
                            <li>CalvertHealth <a href="<?php print $url; ?>/news/calverthealth-monitors-coronavirus-cases-using-meditechs-bca-solution"><strong>monitors coronavirus cases</strong></a> using MEDITECH's BCA solution (3/23/20)</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="customers-in-the-news"></div>
        </div>
        <!-- End of Customer Strategies -->


        <!-- Customers in the News -->
        <div class="container bg--emerald">
            <div class="container__centered">
                <h3>MEDITECH Customers in the News</h3>
            </div>
            <div class="container__centered">
                <div class="container__one-half">
                    <ul class="squish-list">
                        <li><strong>Waging War Against COVID-19</strong> at Comanche County Medical Center (1/21)</li>
                        <li>HCA teams with AHRQ, Johns Hopkins to <a href="https://www.fiercehealthcare.com/hospitals/hca-forms-consortium-ahrq-universities-to-share-key-data-covid-19-to-improve-treatments"><strong>share key data</strong></a> on COVID-19 (1/27/21)</li>
                        <li>Phoebe reaches <a href="https://www.albanyherald.com/news/phoebe-reaches-10-000-vaccine-milestone-as-covid-surge-continues/article_d2e4a0ee-5a8a-11eb-9dca-6be8c5ee9429.html"><strong>10,000 vaccine milestone</strong></a> as COVID surge continues (1/19/21)</li>
                        <li>Avera <a href="https://www.avera.org/news-media/news/2020/home-care-covid-successes/"><strong>Reduces COVID-19 Hospitalizations</strong></a> Through Home Care (11/17/20)</li>
                        <li><a href="https://www.wbjournal.com/article/qa-fighting-covid-no-matter-what-at-milford-regional-medical-center"><strong>Q&amp;A: Fighting COVID</strong></a>, no matter what, at Milford Regional Medical Center (6/17/20)</li>
                        <li>SHINE hospitals launch <a href="https://barrie.ctvnews.ca/local-hospitals-launch-virtual-visit-platforms-to-treat-some-patients-from-home-1.4953917"><strong>virtual visit platforms</strong></a> to treat some patients from home (5/26/20)</li>
                        <li><a href="https://youtu.be/x821WV9KVGw"><strong>Massachusetts Governor Charlie Baker</strong></a> addresses state from new Lawrence General testing site. (5/22/20)</li>
                        <li>Here's how hospital staff at Humber River are <a href="https://globalnews.ca/news/6875117/coronavirus-intensive-care-unit-covid-19-treatment/"><strong>treating coronavirus patients</strong></a> (4/28/20)</li>
                        <li>Lake Superior State University nursing collaborates with War Memorial to create <a href="https://www.wlns.com/news/lake-superior-state-university-nursing-collaborates-to-create-acute-container-care-pods/"><strong>acute container care pods</strong></a> (4/23/20)</li>
                        <li>Coronavirus <a href="https://www.northjersey.com/story/news/coronavirus/2020/04/22/nj-hospitals-study-coronavirus-treatment-with-blood-plasma-from-survivors/5140061002/"><strong>survivors donate blood plasma</strong></a> for sick patients in hope antibodies fight the disease at Valley Hospital (4/22/20)</li>
                    </ul>
                </div>
                <div class="container__one-half">
                    <ul class="squish-list">
                        <li>Hebrew SeniorLife Gives Front-Line <a href="https://www.wbur.org/commonhealth/2020/04/20/hebrew-seniorlife-coronavirus-pay-raise-employees"><strong>Staff 'Appreciation Pay'</strong></a> During Pandemic (4/20/20)</li>
                        <li>Ontario Shores offering <a href="https://www.durhamradionews.com/archives/126040"><strong>mental health support</strong></a> for health care workers during COVID-19 (4/17/20)</li>
                        <li>Avera Virtual Visits <a href="https://www.drgnews.com/avera-virtual-visits-keeps-patients-and-providers-connected/"><strong>keeps patients and providers connected</strong></a> (4/9/20)</li>
                        <li>Morton Hospital in Taunton <a href="https://www.wbur.org/commonhealth/2020/04/05/morton-hospital-taunton-covid-19-coronavirus-center-pandemic"><strong>converting to COVID-19 center</strong></a> (4/5/20)</li>
                        <li>Holyoke Medical Center welcomes <a href="https://www.holyokehealth.com/news/holyoke-medical-center-welcomes-40-soldiers-home-residents/"><strong>40 soldier's home residents</strong></a> (4/3/20)</li>
                        <li>Mount Nittany Health launches <a href="http://www.statecollege.com/news/local-news/mount-nittany-health-launches-covid19-response-fund,1482925/"><strong>COVID-19 response fund</strong></a> (4/3/20)</li>
                        <li>Firelands teams up with area hospitals to <a href="https://sanduskyregister.com/news/178230/area-hospitals-team-up"><strong>battle coronavirus</strong></a> (3/30/20)</li>
                        <li>Berkshire Medical Center uses <a href="https://www.berkshireeagle.com/stories/berkshire-medical-center-uses-call-center-to-help-tackle-the-coronavirus,600906"><strong>call center</strong></a> to help tackle the coronavirus (3/26/20)</li>
                        <li>Steward's Carney Hospital is <a href="https://www.bizjournals.com/boston/news/2020/03/17/dorchesters-carney-hospital-is-nations-first.html"><strong>nation's first</strong></a> dedicated to coronavirus patients (3/17/20)</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- End of Customers in the News -->


        <!-- Block 5 -->
        <div class="container">
            <div id="news" class="container__centered">

                <div class="show-less auto-margins" data-height="500" style="height:500px;">
                    <?php 
        // adds 'import news article' Views block...
        // pass node id number as 3rd parameter for article to embed...
        print views_embed_view('import_news_article', 'block', '3241'); 
        ?>
                </div>
                <button class="show-more">Show More</button>

            </div>
        </div>
        <!-- End of Block 5  -->

    </div>
    <!-- end js__seo-tool__body-content -->


    <!-- LAST Block -->
    <div class="container">
        <div class="container__centered center">

            <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
            <h2>
                <?php print $cta->field_header_1['und'][0]['value']; ?>
            </h2>
            <?php } ?>

            <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
            <div>
                <?php print $cta->field_long_text_1['und'][0]['value']; ?>
            </div>
            <?php } ?>

            <div class="center" style="margin-top:2em;">
                <?php //hubspot_button($cta_code, "Join our Mailing List and Stay Connected with MEDITECH"); ?>
            </div>

            <div style="margin-top:1em;">
                <?php print $share_link_buttons; ?>
            </div>

        </div>
    </div>
    <!-- End of LAST Block -->

    <?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>
    <!-- END simple-page--node-4214.php -->
