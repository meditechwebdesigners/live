<?php if( user_is_logged_in() ){ ?>
<!-- START style-guide--node-2858.php Accordion -->

<!--Javascript & CSS reference for Prism-->
<script type="text/javascript" src="<?php print $url; ?>/sites/all/themes/meditech/js/prism.js"></script>

<link href="<?php print $url; ?>/sites/all/themes/meditech/css/prism.css" rel="stylesheet" />

<section class="container__centered">

	<h1 class="page__title">
		<?php print $title; ?>
	</h1>

	<div class="container__two-thirds">

		<p>Accordions are useful when you want to toggle between hiding and showing a large amount of content. It is best practice to not hide content at all but there are situations where this can be useful, especially for mobile. We typically use these on the <a href="https://ehr.meditech.com/events">Events Pages</a> but they can be also be repurposed as dropdown menus and more.</p>

		<div class="demo-ct">
			<!-- Accordion -->
			<div class="accordion">
				<ul class="accordion__list">

					<!-- Accordion One -->
					<li class="accordion__list__item">
						<a class="accordion__link" href="#">Accordion One
							<div class="accordion__list__control"></div>
						</a>
						<div class="accordion__dropdown">
							<h3>This is an accordion.</h3>
							<p>You can put stuff in here.</p>
							<a class="js__accordion__toggle accordion__close" href="#">
								<i class="fa fa-minus-circle"></i> Close Accordion</a>
						</div>
					</li>
					<!-- End Accordion One -->

					<!-- Accordion Two -->
					<li class="accordion__list__item">
						<a class="accordion__link" href="#">Accordion Two
							<div class="accordion__list__control"></div>
						</a>
						<div class="accordion__dropdown">
							<h3>This is also an accordion.</h3>
							<p>You can also put stuff in here.</p>
							<a class="js__accordion__toggle accordion__close" href="#">
								<i class="fa fa-minus-circle"></i> Close Accordion</a>
						</div>
					</li>
					<!-- End Accordion Two -->

				</ul>
			</div>
			<!-- End Accordion -->

		</div>

		<!-- Start Accordion Code -->
		<pre style="margin-bottom:2em;"><code class="language-html">&lt;!-- Start Accordion -->
&lt;div class="accordion">
  &lt;ul class="accordion__list">

    &lt;!-- Accordion One -->
    &lt;li class="accordion__list__item">
      &lt;a class="accordion__link" href="#">Accordion One
        &lt;div class="accordion__list__control">&lt;/div>&lt;/a>
      &lt;div class="accordion__dropdown">
        &lt;h3>This is an accordion.&lt;/h3>
        &lt;p>You can put stuff in here.&lt;/p>
        &lt;a class="js__accordion__toggle accordion__close" href="#">
          &lt;i class="fa fa-minus-circle">&lt;/i> Close Accordion&lt;/a>
      &lt;/div>
    &lt;/li>
    &lt;!-- End Accordion One -->

    &lt;!-- Accordion Two -->
    &lt;li class="accordion__list__item">
      &lt;a class="accordion__link" href="#">Accordion Two
        &lt;div class="accordion__list__control">&lt;/div>&lt;/a>
      &lt;div class="accordion__dropdown">
        &lt;h3>This is also an accordion.&lt;/h3>
        &lt;p>You can also put stuff in here.&lt;/p>
        &lt;a class="js__accordion__toggle accordion__close" href="#">
          &lt;i class="fa fa-minus-circle">&lt;/i> Close Accordion&lt;/a>
      &lt;/div>
    &lt;/li>
    &lt;!-- End Accordion Two -->

  &lt;/ul>
&lt;/div>
&lt;!-- End Accordion -->
</code></pre>
		<!-- End Accordion Code -->

	</div>

	<!-- SIDEBAR -->
	<aside class="container__one-third">

		<div class="sidebar__nav panel">
			<?php
        $styleBlock = module_invoke('menu', 'block_view', 'menu-style-guide-side-nav');
        print render($styleBlock['content']); 
        ?>
		</div>

	</aside>
	<!-- END SIDEBAR -->

</section>

<!-- END style-guide--node-2858.php Accordion -->
<?php } ?>
