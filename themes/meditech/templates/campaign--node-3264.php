<!-- START campaign--node-3264.php -->

<?php // This template is set up to control the display of the campaign: Surgical Services

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');
$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>


<div class="js__seo-tool__body-content">

    <style>
        video {
            max-width: 100%;
        }

        .demo {
            border-radius: 0.389em;
            box-shadow: 0px 0px 20px 1px rgb(120 120 120 / 20%);
            cursor: pointer;
        }

        .no-play-pause {
            pointer-events: none;
        }

        .bg--black-coconut-gradient {
            background: #3E4545;
            background: -webkit-linear-gradient(#4c5252, #181919);
            background: -o-linear-gradient(#4c5252, #181919);
            background: -moz-linear-gradient(#4c5252, #181919);
            background-image: linear-gradient(-150deg, #4c5252, #181919);
            color: #fff;
        }

        .quote-person {
            margin-left: 30%;
            padding-top: 1em;
        }

        .circle-icon {
            height: 100px;
            width: 100px;
            color: #fff;
            display: flex;
            justify-content: center;
            align-items: center;
            border-radius: 50%;
            margin: 0 auto;
            margin-bottom: 1em;
        }

        .container-pad {
            padding: 4em 0;
        }

        .card--uneven-bottom {
            align-self: flex-start;
        }

        .fake-link {
            transition: color 0.1s linear;
            color: #087E68;
            text-decoration: none;
            background: transparent;
        }

        .extra-margin--bottom {
            margin-bottom: 2em;
        }

        .linked-play-icon::after {
            display: inline-block;
            font-style: normal;
            font-variant: normal;
            text-rendering: auto;
            -webkit-font-smoothing: antialiased;
            font-family: "Font Awesome 5 Free";
            font-weight: 400;
            content: " \f144";
            padding-left: 0.25em;
        }

        @media all and (max-width: 50em) {
            .container-pad {
                padding: 2em 0;
            }
        }

    </style>


    <div class="js__seo-tool__body-content">

        <!-- Block 1 -->
        <div class="container container-pad">
            <div class="container__centered">
                <div class="container__one-half">
                    <div class="bg-pattern--container">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/physician-preparing-for-surgery-securing-mask.jpg" alt="Physician tying mask in preperation of surgery">
                        <div class="bg-pattern--blue-squares bg-pattern--left"></div>
                    </div>
                </div>
                <div class="container__one-half content--pad-left" style="margin-top:1em;">
                    <h1 class="text--white js__seo-tool__title" style="display:none;">MEDITECH Surgical Services</h1>
                    <h2>Boost productivity with MEDITECH's Surgical Services</h2>
                    <p>Keep your surgical department running like clockwork. MEDITECH Expanse Surgical Services helps staff to strengthen efficiency and coordination — opening the doors to safer, more productive operating rooms. Intuitive and comprehensive tools enable your staff to manage workflows and patient care more precisely for increased revenue.</p>
                    <div class="btn-holder--content__callout">
                        <?php hubspot_button($cta_code, ""); ?>
                    </div>
                </div>

            </div>
        </div>
        <!-- End of Block 1 -->


        <!-- Block 2 -->
        <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-gradient--bg.jpg);">
            <div class="container__centered">
                <div class="page__title--center text--white auto-margins">
                    <h2>Ramp up efficiency with fast and flexible scheduling</h2>
                    <p class="text--large extra-margin--bottom">Schedule cases and instantly book, move, cancel, hold, and reschedule cases. Make it your own with customization.</p>
                </div>
                <div class="card__wrapper">
                    <div class="container__one-fourth card card--uneven-bottom">
                        <div class="card__info">
                            <div class="circle-icon bg--dark-blue"> <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--drag-and-drop.svg" alt="Phone with cyclical money icon" style="max-width:55%;"></div>
                            <p class="text--large">Move procedure times and dates by either dragging and dropping, or cutting and pasting.</p>
                        </div>
                    </div>
                    <div class="container__one-fourth card card--uneven-bottom">
                        <div class="card__info">
                            <div class="circle-icon bg--dark-blue"> <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--surgeon.svg" alt="Phone with cyclical money icon" style="max-width:50%;"></div>
                            <p class="text--large">Link surgeon-specific preference cards automatically — including images — to procedures at the time of scheduling.</p>
                        </div>
                    </div>
                    <div class="container__one-fourth card card--uneven-bottom">
                        <div class="card__info">
                            <div class="circle-icon bg--dark-blue"> <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--schedule-check.svg" alt="Phone with cyclical money icon" style="max-width:45%;"></div>
                            <p class="text--large">Tailor scheduling grids to easily schedule procedures while incorporating multiple resources. Prevent overbooking resources with automated resource conflict checks.</p>
                        </div>
                    </div>
                    <div class="container__one-fourth card card--uneven-bottom">
                        <div class="card__info">
                            <div class="circle-icon bg--dark-blue"> <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--question-mark.svg" alt="Phone with cyclical money icon" style="max-width:25%;"></div>
                            <p class="text--large">Send questionnaires and procedure information to the patient portal automatically.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- End of Block 2 -->


        <!-- Block 3 -->
        <div class="container bg--white">
            <div class="container__centered">
                <h2 class="page__title--center extra-margin--bottom">Ensure your patients are fit for surgery</h2>
                <div class="container__one-third">
                    <p>Simplify your patient’s surgical journey with the Pre-Admission Testing (PAT) Desktop. A split-screen design minimizes appointment information, so you can easily organize the desktop in the way that suits you best. You can:</p>
                    <ul>
                        <li>Manage PAT visit status boards and pre-op call lists that auto-populate based on your procedure-specific workflows.
                        </li>
                        <li>Customize your PAT Desktop to display the clinical data your team needs to review — including procedure information, lab results, portal questionnaires, consent forms, PFSH, and documentation tools.</li>
                        <li>Easily consume patient portal questionnaire responses directly into your visit documentation.</li>
                    </ul>
                </div>
                <div class="container__two-thirds center">
                    <!-- Start hidden modal box -->
                    <div id="modal1" class="modal">
                        <a class="close-modal" href="javascript:void(0)">&times;</a>
                        <div class="modal-content">
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-screenshot--PAT--large.jpg">
                        </div>
                    </div>
                    <!-- End hidden modal box -->

                    <!-- Start modal trigger -->
                    <div class="open-modal" data-target="modal1" style="margin:0.5em 0;">
                        <div class="tablet--white">
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-screenshot--PAT--small.jpg" alt="MEDITECH Surgical Services Pre-Admission Testing Desktop Screenshot">
                        </div> <!-- Add modal trigger here -->
                        <div class="mag-bg">
                            <!-- Include if using image trigger -->
                            <i class="mag-icon fas fa-search-plus"></i>
                        </div>
                    </div>
                    <!-- End modal trigger -->
                </div>
            </div>
        </div>
        <!-- End of Block 3  -->


        <!-- Block 4 -->
        <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-gradient--bg.jpg);">
            <div class="container__centered text--white">
                <!-- Start hidden modal box -->
                <div id="modal21" class="modal">
                    <a class="close-modal" href="javascript:void(0)">&times;</a>
                    <div class="modal-content">
                        <video autoplay loop muted playsinline class="demo no-play-pause">
                            <source src="<?php print $url; ?>/sites/all/themes/meditech/videos/campaigns/MEDITECH-demo--SUR-Expanse-Handoff.mp4" type="video/mp4">
                        </video>
                    </div>
                </div>
                <!-- End hidden modal box -->
                <h2 class="page__title--center extra-margin--bottom">Track patients throughout the surgical setting</h2>
                <div class="container__two-thirds center">
                    <!-- Start modal trigger -->
                    <div class="open-modal" data-target="modal21" style="margin:0.5em 0;">
                        <div class="tablet--white">
                            <video autoplay loop muted playsinline class="demo no-play-pause">
                                <source src="<?php print $url; ?>/sites/all/themes/meditech/videos/campaigns/MEDITECH-demo--SUR-Expanse-Handoff.mp4" type="video/mp4">
                            </video>
                        </div> <!-- Add modal trigger here -->
                        <div class="mag-bg">
                            <!-- Include if using image trigger -->
                            <i class="mag-icon fas fa-search-plus"></i>
                        </div>
                    </div>
                    <!-- End modal trigger -->
                </div>
                <div class="container__one-third">
                    <p>Surgical Trackers monitor all perioperative information associated with a patient, in real time.</p>
                    <ul>
                        <li>Interact with clinical data and decision support directly from the trackers.</li>
                        <li>Obtain quality metrics, alerts, and clinical decision support in real time.</li>
                        <li>Manage patient flow with unlimited displays based on location or status.</li>
                        <li>View all relevant SBAR data in one place using an embedded Hand Off feature.</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- End of Block 4 -->



        <!-- Block 5 -->
        <div class="container bg--white">
            <div class="container__centered">
                <!-- Start hidden modal box -->
                <div id="modal3" class="modal">
                    <a class="close-modal" href="javascript:void(0)">&times;</a>
                    <div class="modal-content">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-screenshot--SUR-Expanse-Intraop--large.jpg">
                    </div>
                </div>
                <div id="modal4" class="modal">
                    <a class="close-modal" href="javascript:void(0)">&times;</a>
                    <div class="modal-content modal-content--vid">
                        <!-- Add "--vid" to class name -->
                        <iframe src="https://player.vimeo.com/video/574257928" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe> <!-- Add iframe content here -->
                    </div>
                </div>
                <!-- End hidden modal box -->
                <div class="page__title--center extra-margin--bottom">
                    <h2>Document the entire perioperative episode, from Pre-Op to PACU</h2>
                </div>
                <div class="container__one-half">
                    <h4>Procedure-specific document templates provide important clinical decision support and tools to quickly and effectively document a case.</h4>
                    <ul>
                        <li>Record details related to the procedure with ease, including surgical times, staff present, materials used, outcomes, etc. </li>
                        <li>Access the patient’s MAR, transfusion administration record, interprofessional care plans, nursing assessments, Time Outs, and interventions immediately, in one centralized location within Surgical Services. </li>
                        <li><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/anesthesiainterfacegeneral.pdf">Share critical documentation data</a> with other-vendor <span class="open-modal fake-link linked-play-icon" data-target="modal4">Anesthesia Information Management Systems</span>.</li>
                        <!--they wanted to link the second link to https://vimeo.com/574257928/7200729a46-->
                    </ul>
                </div>

                <div class="container__one-half center">
                    <!-- Start modal trigger -->
                    <div class="open-modal" data-target="modal3" style="margin:0.5em 0;">
                        <div class="tablet--white">
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-screenshot--SUR-Expanse-Intraop--small.jpg" alt="MEDITECH Surgical Services Scheduling screenshot">
                        </div><!-- Add modal trigger here -->
                        <div class="mag-bg">
                            <!-- Include if using image trigger -->
                            <i class="mag-icon fas fa-search-plus"></i>
                        </div>
                    </div>
                    <!-- End modal trigger -->
                </div>
            </div>
        </div>
        <!-- End of Block 5  -->



        <!-- Block 6 -->
        <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-gradient--bg.jpg);">
            <div class="container__centered">
                <div class="page__title--center text--white auto-margins extra-margin--bottom">
                    <h2>Maintain a healthy bottom line with materials management and revenue cycle integration</h2>
                </div>

                <div class="card__wrapper">
                    <div class="container__one-half card">
                        <figure>
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/card-image--surgeon-picking-medical-supplies-in-storage-room.jpg" alt="Impromptu meeting between medical professionals and a businesswoman in hospital hallway">
                        </figure>
                        <div class="card__info">
                            <h4>Monitor inventory levels and manage costs at all times to avoid inventory shortages and overages.</h4>
                            <ul>
                                <li>Use bar-code technology to manage inventories.</li>
                                <li>Track implants and explants with accuracy.</li>
                                <li>Streamline physician ordering with queued orders.</li>
                                <li>Decrement inventory levels in real time during surgical case documentation.</li>
                                <li>Create, manage, and print requisitions for more inventory items.</li>
                                <li>Pinpoint changes to inventory when items are lent to, or borrowed from, another location.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="container__one-half card">
                        <figure>
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/card-image--medical-supply-closet.jpg" alt="Impromptu meeting between medical professionals and a businesswoman in hospital hallway">
                        </figure>
                        <div class="card__info">
                            <h4>Increase revenue and manage cases with specialized Biller and Manager Desktops.</h4>
                            <ul>
                                <li>Minimize losses and prevent waste by automatically capturing charges to send to patient accounting.</li>
                                <li>Open, close, hold, and resume cases in minimal time with a dedicated Biller's Desktop.</li>
                                <li>Make more informed decisions with the Director/Manager Desktop by quickly accessing standard reports that drill down to a specific surgeon or procedure.</li>
                                <li>Generate specific data, break down statistics, perform cost analysis, and review block utilization.</li>
                                <li>Leverage additional features for analyzing custom data to perform strategic cost analysis.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Block 6  -->


        <!-- Block 7 -->
        <div class="container bg--white">
            <div class="container__centered">
                <div class="card__wrapper">

                    <div class="container__one-third card card--uneven-bottom">
                        <div class="card__info">
                            <div class="circle-icon bg--dark-blue"> <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--one-click-health.svg" alt="Phone with cyclical money icon" style="max-width:45%;"></div>
                            <p class="">"For anesthesia we are using iProcedures which interfaces directly into Expanse Surgical Services. Our CRNA's are thrilled to be using iPads that allow them to document real-time at the patient bedside, reducing errors, and saving valuable time."</p>
                            <p><strong>Deb Bergeron</strong>, RN/BS, Clinical Systems Analyst<br>Androscoggin Valley Hospital, part of North Country Healthcare, NH</p>
                        </div>
                    </div>

                    <div class="container__one-third card card--uneven-bottom">
                        <div class="card__info">
                            <div class="circle-icon bg--dark-blue"> <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--inventory-level-graph.svg" alt="Phone with cyclical money icon" style="max-width:60%;"></div>
                            <p class="">"Materials Management ensures that we not only have the appropriate supplies for each specific surgery, but that the billing and reordering for those items are done smoothly within the IntraOp documentation section. Additionally, the standard content statistics reporting within the Surgical Services is the envy of other nursing departments."</p>
                            <p><strong>Cristi Shindler</strong>, RN/BSN, Clinical Informatics Nurse Manager<br>
                                Whitman Hospital and Medical Center, WA</p>
                        </div>
                    </div>

                    <div class="container__one-third card card--uneven-bottom">
                        <div class="card__info">
                            <div class="circle-icon bg--dark-blue"> <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--collaborative-team.svg" alt="Phone with cyclical money icon" style="max-width:60%;"></div>
                            <p class="">"We were looking for a solution that supported our team’s collaborative style, and MEDITECH’s Surgical Services has done just that. Their comprehensive approach to scheduling, documentation, and tracking has helped to bridge all the moving parts of our operating room."</p>
                            <p><strong>Katie Boston-Leary</strong>, CNOR<br>
                                Union Hospital, MD</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>
    <!-- end js__seo-tool__body-content -->


    <!-- LAST Block -->
    <!--    <div class="container" style="padding:5.5em;">-->
    <div class="container background--cover text--white" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-gradient--bg.jpg);">
        <div class="container__centered center">

            <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
            <h2>
                <?php print $cta->field_header_1['und'][0]['value']; ?>
            </h2>
            <?php } ?>

            <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
            <div>
                <?php print $cta->field_long_text_1['und'][0]['value']; ?>
            </div>
            <?php } ?>

            <div class="center" style="margin-top:2em;">
                <?php hubspot_button($cta_code, "Join our Mailing List and Stay Connected with MEDITECH"); ?>
            </div>

            <div style="margin-top:1em;">
                <?php print $share_link_buttons; ?>
            </div>

        </div>
    </div>
    <!-- End of LAST Block -->

    <?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>
    <!-- END campaign--node-3264.php -->
