<?php 
// This template is set up to control the display of the 'INTERNATIONAL - ASIA PACIFIC' content type 
$url = $GLOBALS['base_url']; // grabs the site url
$node_id = $node->nid;
?>
<!-- start node--international-asia-pacific.tpl.php template -->

<?php
// set region variables for Asia Pacific...
$home_page = 'meditech-asia-pacific';
$international_region = 'Asia Pacific';
$side_nav = 'menu-side-menu---asia-pacific';
$country_graphic = 'asia-pacific/asia-pacific-graphic.png';
$sidebar_class = 'asia_pacific_sidebar_gae';
$customer_link = 'https://meditechinternational.atlassian.net/servicedesk/customer';

// set hero variables based on node id...
// ignore if page does not use the hero...

switch($node_id){
  case 2093: // MEDITECH Asia Pacific home page
    $hero = 'yes-main';
    $hero_background_image = 'asia-pacific/asia-pacific-sydney-opera-house-harbour-bridge-skyline.jpg'; 
    $text_class = '';
    break;
    
    case 2095: // MEDITECH Asia Pacific Community page
    $hero = 'yes';
    $hero_background_image = 'asia-pacific/asia-pacific-sydney-skyline.jpg';
    $text_class = 'text--white text-shadow--black';
    break; 
}

?>

<?php if( isset($hero) && $hero == 'yes-main' ){ // if hero belongs on page AND this is a "main" page, display the following... ?>
  <!-- HERO section -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/international/<?php print $hero_background_image; ?>); min-height:20em;">
    <div class="container__centered">
      <div class="container__one-half">&nbsp;</div>
      <div class="container__one-half container__centered">&nbsp;</div>
    </div>
  </div>
  <!-- End of HERO section -->
<?php } ?>


<?php if( isset($hero) && $hero == 'yes' ){ // if hero belongs on page AND this is NOT a "main" page, display the following... ?>
  <!-- HERO section -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/international/<?php print $hero_background_image; ?>);">
    <div class="container__centered">
      <h1 class="<?php print $text_class; ?>"><?php print $title; ?></h1>
    </div>
  </div>
  <!-- End of HERO section -->
<?php } ?>  
  

  <section class="container__centered">

    <div class="container__two-thirds">

    <?php if( isset($hero) && $hero == 'yes-main' ){  ?>
    <h1><?php print $title; ?></h1>
    <?php } ?>
    
      <?php
      // if image exists for node, then proceed to render DIV...
      if(!empty($content['field_main_image_international'])){
      ?>
        <figure class="news__article__img">
          <?php print render($content['field_main_image_international']); ?>
        </figure>
      <?php
      }
      ?>
     
      <?php if( !isset($hero) ){ // if no hero on page, display title here... ?>
      <h1 class="page__title js__seo-tool__title"><?php print $title; ?></h1>
      <?php } ?>
      
      
      <div class="js__seo-tool__body-content">
       
        <?php print render($content['field_body']); // main content if any ?>
        
        <?php
        // specific content for specific pages ======================================================================
        switch($node_id){
            
          case 2097: // ASIA PACIFIC EXECUTIVES page ======================================================================
          print '<div class="container no-pad">';
          print views_embed_view('executives_asia_pacific', 'block'); // adds 'Executives - Asia Pacific' Views block...
          print '</div>';
          break;
          
          case 2096: // ASIA PACIFIC NEWS page ======================================================================
          print views_embed_view('news_asia_pacific', 'block'); // adds 'News - Asia Pacific' Views block... 
          break;
            
          case 2106: // ASIA PACIFIC DIRECTIONS page ======================================================================
          print views_embed_view('locations_asia_pacific', 'block'); // adds 'Locations - Asia Pacific' Views block... 
          break;

          case 2109: // ASIA PACIFIC CONTACT page ========================================================================== 
          ?>
          <h2 class="page__title">Are you an existing customer?</h2>
          <p>If you're a MEDITECH Asia Pacific customer in need of assistance, please log in to our <a href="http://www.meditech.com.au/customer-login/customer-support-form/" target="_blank">Customer Service area</a> and enter a task, or call us anytime at one of our locations listed below.</p>

          <address class="container no-pad">
           <h2>MEDITECH Australia</h2>
            <div class="container__one-third">
              <h3><i class="fas fa-map-marker-alt meditech-green"></i> Address</h3>
              <p>Suite 1.05<br />
              32 Delhi Road<br />
              North Ryde NSW 2113<br /> 
              Australia</p>
            </div>
            <div class="container__one-third">
              <h3><i class="fas fa-phone meditech-green"></i> Call</h3>
              <p><strong>Main Office</strong><br><a class="phone-number" href="tel:+61-2-9901-6400">+61-2-9901-6400</a></p>
              <p><strong>Customer Services</strong><br><a class="phone-number" href="tel:1300-080-043">1300-080-043</a>
            </div>
            <div class="container__one-third">
              <h3><i class="fas fa-fax meditech-green"></i> Fax</h3>
              <p>+61-2-9439-6331</p>
            </div>
          </address>
         
          <address class="container no-pad">
            <h2>MEDITECH Singapore</h2>
            <div class="container__one-third">
              <h3><i class="fas fa-map-marker-alt meditech-green"></i> Address</h3>
              <p>8 Wilkie Rd #03-08<br />
              Wilkie Edge<br />
              Singapore 228095</p>
            </div>
            <div class="container__one-third">
              <h3><i class="fas fa-phone meditech-green"></i> Call</h3>
              <p><strong>Main Office</strong><br><a class="phone-number" href="tel:+65-9066-0341">+65-9066-0341</a></p><p><strong>Customer Services</strong><br><a class="phone-number" href="tel:+65-800-616-7085">+65-800-616-7085</a></p>
            </div>
          </address>
          
          <hr>
          
          <address class="container no-pad">
            <h2>Stay Connected with MEDITECH Asia Pacific</h2>
            <div class="container__one-third">
              <h3><a href="https://www.facebook.com/Meditech-Australia-360851333989795/" target="_blank"><i class="fab fa-facebook-square meditech-green"></i> Facebook</a></h3>
            </div>
            <div class="container__one-third">
              <h3><a href="https://twitter.com/au_meditech" target="_blank"><i class="fab fa-twitter-square meditech-green"></i> Twitter</a></h3>
            </div>
            <div class="container__one-third">
              <h3><a href="https://au.linkedin.com/in/meditech-australia-63715257/de" target="_blank"><i class="fab fa-linkedin meditech-green"></i> LinkedIn</a></h3>
            </div>
          </address>
          
          <hr>

          <!-- CONTACT FORM =============================== -->
          <h3><i class="fas fa-envelope meditech-green"></i> Send a message to MEDITECH Asia Pacific</h3>

          <?php
            $contactForm = module_invoke('webform', 'block_view', 'client-block-2128');
            print render($contactForm['content']);
          ?>
          <!-- End of Contact Form -->            
          <?php
          break;
            
                      
          case 2091: // ASIA PACIFIC EMPLOYEE CONTACT page ========================================================================== 
            if( isset($_GET['employee']) && isset($_GET['send_to']) && $_GET['employee'] != '' && $_GET['send_to'] != '' ){
              $employee = $_GET['employee'];
              ?>
              <!-- CONTACT FORM =============================== -->
              <h3><i class="fas fa-envelope meditech-green"></i> Send <?php print $employee; ?> a message</h3>
 
              <?php
                $contactForm = module_invoke('webform', 'block_view', 'client-block-2092');
                print render($contactForm['content']);
              ?>
              <!-- End of Contact Form -->  
            <?php
            }
            else{
            ?>
            
              <p class="page__title">We're sorry but there is a problem with this form or the link you clicked. Please try contacting us through <a href="http://www.meditech.com.au/contact/" target="_blank">our main contact form</a>.</p>
            
            <?php
            }

            break;
            
            
          case 2414: // ASIA PACIFIC CUSTOMER SERVICE CONTACT page ========================================================================== 
            
            $contactForm = module_invoke('webform', 'block_view', 'client-block-2155');
            print render($contactForm['content']);
           
            break;
            
        } // END node ID switch
        ?>
        
        
      </div><!-- End .js__seo-tool__body-content -->
      <?php // SEO tool for internal use...
        if(node_access('update',$node)){
          print '<!-- SEO Tool is added to this div -->';
          print '<div class="container no-pad--top js__seo-tool"></div>';
        } 
      ?>
      
    </div>
    
    <!-- RIGHT SIDE NAV MENU -->
    
    <style>
      .sidebar__nav ul.menu ul.menu { padding-left: 1.5em; margin-top: 0; }
      .sidebar__nav ul.menu ul.menu li { padding: .4em 0 .5em 0; font-size: .9em; }
      .sidebar__nav ul.menu ul.menu li.last { padding-bottom: 0; }
      
      @media (max-width: 800px){
        .sidebar__nav ul.menu ul.menu li { font-size: 1em; }
      }
    </style>

    <aside class="container__one-third">
    
     <div class="panel">
     
       <?php if($node_id != 2093){ // if not MEDITECH Australia home page, display menu header... ?>
          <div class="center">
            <h3 style="font-size:1.25em;"><a href="<?php print $url; ?>/global/<?php print $home_page; ?>" title="Go to the <?php print $international_region; ?> home page">MEDITECH <?php print $international_region; ?></a></h3>
          </div>
        <?php } ?>

        <div class="sidebar__nav <?php print $sidebar_class; ?>" style="float:left; width:100%;">
          <?php
            // adds appropriate international menu block...
            $international_menu = module_invoke('menu', 'block_view', $side_nav);
            print render($international_menu['content']); 
          ?>
        </div>

        <p style="text-align:center;"><a href="<?php print $customer_link; ?>" target="_blank" class="btn--orange asia_pacific_customers_gae">MEDITECH <?php print $international_region; ?><br />Customer Service</a></p>
        
      </div>
      
      <?php if( !empty( views_get_view_result('next_big_event_ap', 'block') ) || !empty( views_get_view_result('upcoming_asia_pacific_trade_shows', 'block') ) ){ ?>
      <div class="panel <?php print $sidebar_class; ?>">

        <?php print views_embed_view('next_big_event_ap', 'block'); // adds 'Next Big US Event - International' Views block... ?>

        <?php if( !empty( views_get_view_result('upcoming_asia_pacific_trade_shows', 'block') ) ){ ?>
          <h3>Upcoming Trade Shows & Conferences</h3>
          <?php print views_embed_view('upcoming_asia_pacific_trade_shows', 'block'); // adds 'Upcoming UK IRE Trade Shows' Views block... ?>
        <?php } ?>

      </div>
      <?php } ?>      
      
    </aside>
  
  </section>
  
<!-- end node--international-asia-pacific.tpl.php template -->