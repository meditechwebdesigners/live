<!-- START campaign--node-2382.php -->
<?php // This template is set up to control the display of the BRIDGING THE GAP content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>
<style>
    .block1 {
        float: left;
        display: block;
        width: 28%;
    }

    .block2 {
        float: left;
        display: block;
        width: 7%;
        font-size: 4em;
        margin-top: 1em;
    }

    @media (max-width: 50em) {
        .block1 {
            width: 100%;
        }
        .block2 {
            width: 100%;
            margin: 0em;
        }
    }

    .packery-grid-item {
        float: left;
        width: 33.333%;
        height: 200px;
        padding: 0.5em 0 0 0;
        border-right: 5px solid #FFFFFF;
        border-bottom: 5px solid #FFFFFF;
    }

    @media screen and (max-width: 600px) {
        .packery-grid-item,
        .packery-grid-item--width2 {
            width: 100%;
        }
    }

    .packery-grid-item--height2 {
        height: 400px;
    }

    .packery-grid-item--bg {
        position: relative;
        padding: 0;
        margin: 0;
        height: 100%;
        overflow: hidden;
    }

    .packery-grid-item--info {
        position: absolute;
        top: auto;
        bottom: 0;
        left: 0;
        width: 100%;
        padding: 1em;
        background: rgba(0, 0, 0, 0.65);
    }

    .packery-grid-item--info--title {
        font-weight: bold;
        line-height: 1.25em;
        margin-bottom: 0;
        color: white;
    }

</style>
<!-- Hero -->
<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/two-middle-age-adults-riding-bicycles-in-park.jpg);">
    <div class="container__centered">
        <div class="container__one-half">
            &nbsp;
        </div>
        <div class="container__one-half transparent-overlay text--white">
            <h1>
                Bridge the gaps in care.
            </h1>
            <p>
                Your clinicians spend less than a few hours a year with each patient. But what happens when they leave the walls of your healthcare organization? MEDITECH Expanse bridges the gaps in care that could contribute to chronic diseases — resulting in ED visits and readmissions. From patient engagement strategies that encourage healthier lifestyles, to integrated workflows that increase collaboration across care settings, Expanse keeps you connected throughout all stages of the patient's journey.
            </p>
            
            <div class="center" style="margin-top:2em;">
              <?php hubspot_button($cta_code, "Download The Value-Based Care eBook"); ?>
            </div>
          
        </div>
    </div>
</div>
<!--End of Hero-->
<!--Block 2-->
<div class="container bg--green-gradient">
    <div class="container__centered text--white">
        <div class="page__title--center">
            <div class="container no-pad">
                <h2>
                    Breaking down silos across the care continuum.
                </h2>
                <p>
                    Extend care beyond the hospital and practice. Share information across all settings to get a complete view of your patients' needs.
                </p>
            </div>
            <div class="container no-pad--top">
                <div class="container__one-third transparent-overlay">
                    <img src="/sites/all/themes/meditech/images/campaigns/star-of-life-icon.png" alt="Star of life icon" style="max-width:100px; margin: 0.5em;">
                    <p>
                        <a href="https://ehr.meditech.com/ehr-solutions/web-ambulatory">Urgent Care</a> - Keep track of patients' minor ailments and medication changes that occur outside of regular office hours.
                    </p>
                </div>
                <div class="container__one-third transparent-overlay">
                    <img src="/sites/all/themes/meditech/images/campaigns/ambulance-icon.png" alt="Ambulance Icon" style="max-width:100px; margin: 0.5em;">
                    <p>
                        <a href="https://ehr.meditech.com/ehr-solutions/meditech-ed">Emergency Department</a> - Leverage information and documentation in one cohesive chart that facilitates care delivery inside and outside the ED.
                    </p>
                </div>
                <div class="container__one-third transparent-overlay">
                    <img src="/sites/all/themes/meditech/images/campaigns/brain-in-head-icon.png" alt="Brain Icon" style="max-width:100px; margin: 0.5em;">
                    <p>
                        <a href="https://ehr.meditech.com/ehr-solutions/behavioral-health-hospitals-health-systems">Behavioral Health</a> - Focus on patients' mental and physical health with tools to collaborate on intervention and support.
                    </p>
                </div>
            </div>
            <div class="container no-pad">
                <div class="container__one-third transparent-overlay">
                    <img src="/sites/all/themes/meditech/images/campaigns/patient-using-walking-bars.png" alt="Patient Using Walking Bars Icon" style="max-width:100px; margin: 0.5em;">
                    <p>
                        <a href="https://ehr.meditech.com/ehr-solutions/behavioral-health-post-acute-services">Rehabilitation</a> - Facilitate care team collaboration around patients' goals for positive patient outcomes. Embedded IRF-PAI documentation ensures accurate reimbursement.
                    </p>
                </div>
                <div class="container__one-third transparent-overlay">
                    <img src="/sites/all/themes/meditech/images/campaigns/Nurse-helping-patient-icon.png" alt="Nuse Helping Patient Icon" style="max-width:100px; margin: 0.5em;">
                    <p>
                        <a href="https://ehr.meditech.com/ehr-solutions/long-term-care-hospitals-health-systems">Long-Term Care</a> - Ensure smoother transitions of care for patients and residents using our single, integrated EHR.
                    </p>
                </div>
                <div class="container__one-third transparent-overlay">
                    <img src="/sites/all/themes/meditech/images/campaigns/house-with-health-cross-icon.png" alt="House with medical cross icon" style="max-width:100px; margin: 0.5em;">
                    <p>
                        <a href="https://ehr.meditech.com/ehr-solutions/meditech-home-care">Home Care</a> - Support patients at every stage of life, in the comfort of home, with three integrated options — Home Health, Hospice, and Telehealth.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End of Block 2-->
<!--Block 3-->
<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/dark-grey-background-with-faint-lines--brightened.jpg);">
    <div class="container__centered text--white">
        <div class="page__title">
            <div class="auto-margins">
                <h2>
                    Build clinically integrated networks with an interoperable EHR.
                </h2>
                <p>
                    Your patients don't always receive care in your network. Standards-based interoperability benefits everyone and helps you connect across the continuum of care, HIEs, and federal and local public health agencies with ease. Exchange information through C-CDAs, APIs, FHIR, HL7, and Direct Messaging using MEDITECH's Expanse.
                </p>
                <p>
                    To help you fill in the missing pieces, MEDITECH is collaborating with <a href="https://www.meditech.com/productbriefs/flyers/Arcadia_Flyer.pdf">Arcadia.io</a> to bring forward information from across the continuum, including claims data and other-vendor EHR data.
                </p>
                <p>
                    <a href="https://blog.meditech.com/a-new-way-to-drive-interoperability-and-improve-patient-experience">Learn how Signature Health and Beth Israel Deaconess Medical Center</a> are leveraging interoperability to improve care by making information readily available between their organizations.
                </p>
            </div>
        </div>
    </div>
</div>
<!--End of Block 3-->
<!--Block 4-->
<div class="container bg--white">
    <div class="container__centered">
        <div class="page__title">
            <div class="container no-pad">
                <div class="container__three-fourths">
                    <h2>
                        Empower patients to take charge of their health.
                    </h2>
                    <p>
                        No one plays a more crucial role in their health and well-being than patients themselves. Keep them engaged with MEDITECH's Patient and Consumer Health Portal, where they can view and trend their data from personal health devices to pursue individual wellness goals. Patients can also book appointments, update their information, and communicate securely with providers.
                    </p>
                    <p>
                        Learn how <a href="https://home.meditech.com/en/d/casestudies/otherfiles/ontarioshoresportalcasestudy.pdf">Ontario Shores</a> uses MEDITECH's Patient and Consumer Health Portal to meet their engagement goals.
                    </p>
                </div>
                <div class="container__one-fourth">
                    &nbsp;
                </div>
            </div>
            <div class="container no-pad">
                <h3>
                    See our Patient Engagement solutions in action.
                </h3>
                <div class="text--white">
                    <div class="packery-grid-item packery-grid-item--height2" style="background: brown url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/atheltic-person-using-MHealth-app.jpg) center top no-repeat;">
                        <div class="packery-grid-item--bg">
                            <div class="packery-grid-item--info">
                                <p class="packery-grid-item--info--title">The <a href="https://vimeo.com/230982426">MHealth Mobile app</a> provides convenient patient/proxy access to our patient portal.</p>
                            </div>
                        </div>
                    </div>
                    <div class="packery-grid-item packery-grid-item--height2" style="background: blue url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/runner-checking-wearable.jpg) center top no-repeat;">
                        <div class="packery-grid-item--bg">
                            <div class="packery-grid-item--info">
                                <p class="packery-grid-item--info--title"><a href="https://ehr.meditech.com/news/get-extra-mileage-from-your-health-wearables-using-meditech-s-patient-portal">Device Integration</a> puts patients at the center of their care, using the devices they already have.</p>
                            </div>
                        </div>
                    </div>
                    <div class="packery-grid-item packery-grid-item--height2 packery-grid-item--height2 sticky" style="background: red url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/patient-and-doctor-engaging-in-virtual-visit.jpg) center top no-repeat;">
                        <div class="packery-grid-item--bg">
                            <div class="packery-grid-item--info">
                                <p class="packery-grid-item--info--title"><a href="https://vimeo.com/meditechehr/review/256641270/8a1de02d75">Virtual Visits</a> offer patients a convenient, remote interaction with their providers.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End of Block 4-->
<!--Block 5 Video-->
<div class="content__callout">
    <div class="content__callout__media">
        <div class="content__callout__image-wrapper">
            <div class="video js__video" data-video-id="200671200">
                <figure class="video__overlay">
                    <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/video-overlay_dr-fletcher.jpg" alt="Dr Fletcher Patient Populations Video Covershot">
                </figure>
                <a class="video__play-btn" href="https://vimeo.com/200671200?&autoplay=1"></a>
                <div class="video__container"></div>
            </div>
        </div>
    </div>
    <div class="content__callout__content">
        <div class="content__callout__body">
            <div class="content__callout__body__text">
                <h2>
                    Understand where your patient populations are coming from.
                </h2>
                <p>
                    Know who you're accountable for, and help them to manage their health. Use MEDITECH's Patient Registries as the basis of a care management program that enables you to keep healthy patients well, and intervene before minor conditions spiral out of control. These actionable registries populate with real-time patient data from across care settings, including remote monitoring devices and fitness trackers. 
                </p>
            </div>
        </div>
    </div>
</div>
<!--End of Block 5-->
<!--Block 6-->
<div class="container bg--green-gradient">
    <div class="container__centered text--white">
        <div class="page__title auto-margins">
            <h2>
                Get a complete picture of patient risk and utilization.
            </h2>
            <p>
                Our collaboration with <a href="https://www.meditech.com/productbriefs/flyers/Arcadia_Flyer.pdf">Arcadia.io</a> embeds Arcadia-supplied data elements into your care management workflow, extending your population health initiatives.
            </p>
            <div class="container no-pad--top">
                <div class="container__centered">
                    <div class="transparent-overlay block1">
                        <h4>
                            Arcadia-Supplied Data
                        </h4>
                        <ul>
                            <li>
                                Claims data
                            </li>
                            <li>
                                Other-vendor EHR data
                            </li>
                            <li>
                                Arcadia-derived elements
                            </li>
                        </ul>
                    </div>
                    <p class="block2 center">+</p>
                    <div class="transparent-overlay block1">
                        <h4>
                            Advanced Analytics
                        </h4>
                        <ul>
                            <li>
                                Risk stratification
                            </li>
                            <li>
                                Utilization of resources
                            </li>
                            <li>
                                Costs
                            </li>
                            <li>
                                Impact on achieving quality outcomes
                            </li>
                        </ul>
                    </div>
                    <p class="block2 center">=</p>
                    <div class="transparent-overlay block1">
                        <h4>
                            Results
                        </h4>
                        <p>
                            More effective management of patient populations — the healthy, the chronically ill, and everyone in-between
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End of Block 6-->
<!--Block 7-->
<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Doctor-shaking-hands-with-discharged-patient.jpg);">
    <div class="container__centered text--white">
        <div class="page__title">
            <div class="container__two-thirds  transparent-overlay">
                <h2>
                    Keep patients on track.
                </h2>
                <p>
                    Case managers, advocate for your patients to receive the care they need. MEDITECH's Case Management solution combines clinical and administrative functions to track patient compliance, document care delivered to patient populations, and plan for discharge.
                </p>
                <p class="no-pad--bottom">
                    Our Case Management solution includes:
                </p>
                <ul>
                    <li>
                        Easy access to multidisciplinary plans of care and clinically significant documentation
                    </li>
                    <li>
                        Follow-up call routine
                    </li>
                    <li>
                        Program and service tracking
                    </li>
                    <li>
                        Remote monitoring capabilities for personal health trackers and durable medical equipment.
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--End of Block 7-->
<!--Block 8-->
<div class="container bg--blue-gradient">
    <div class="container__centered text--white">
        <div class="page__title auto-margins">
            <h2>
                Scale the social determinants that stand in your patients' way.
            </h2>
            <p>
                Extend care to include the community and social services your patients need to achieve a healthier lifestyle. Southwestern Vermont Medical Center has designed a new model of transitional care that supports individuals facing difficulties post-discharge, and enables them to take charge of their health. <a href="https://info.meditech.com/video-webinar-recording-social-determinants-0?hsCtaTracking=bb2bf4e3-d445-4f9f-a6cc-1b4854b8c26a%7Ccc2f1953-0301-4e74-8c70-a6fe258787f8">Watch our on-demand webinar: Social Determinants of Health.</a>
            </p>
            <div class="transparent-overlay">
                <p>
                    Working with a team of community partners, SVMC was able to:
                </p>
                <ul>
                    <li>
                        Reduce ED visits by 18.3% post-transitional-care nurse intervention
                    </li>
                    <li>
                        Reduce inpatient/observation visits by 54.4% post-transitional-care nurse intervention
                    </li>
                    <li>
                        Reduce ED visits by 44% for patients dealing with mental health or substance abuse issues
                    </li>
                    <li>
                        Reduce average A1C by 15.7% post-integrated diabetes education
                    </li>
                    <li>
                        Record a 0% readmission rate three months post-graduation from pulmonary rehab program.
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--End of Block 8-->

<!--TEST COMMENT-->

<!-- Block 9 Quote -->
<div class="container bg--white">
    <div class="container__centered">
        <div class="auto-margins">
            <p>
                The National Health Service (NHS) in England has recognized a number of digitally mature hospitals as <a href="https://www.england.nhs.uk/digitaltechnology/info-revolution/exemplars/">Global igital Exemplars (GDEs)</a> — organizations designated to lead the way in helping to close the gaps in both health and social care settings.
            </p>
            <p>
                City Hospitals Sunderland NHS Foundation Trust is one such organization.
            </p>
        </div>

        <br>

        <figure class="container__one-fourth center">
            <img class="quote__content__img" src="https://ehr.meditech.com/sites/all/themes/meditech/images/quote.svg" onerror="this.src='https://ehr.meditech.com/sites/all/themes/meditech/images/quote.svg';this.onerror=null;" alt="quote bubble graphic">
        </figure>
        <div class="container__three-fourths">
            <div class="quote__content__text text--large">
                <p>We are very happy with the sophistication of our MEDITECH solution. By optimising what we already have, we can continue on our journey of mitigating clinical risk, improving the quality of our services we provide to our patients, and becoming more efficient through the use of the MEDITECH solution. We can also better serve our population outside of the hospital walls.</p>
            </div>
            <p class="text--large no-margin--bottom">Andy Hart, Director of Information, Management and Technology</p>
            <p>City Hospitals Sunderland NHS Foundation Trust</p>
        </div>
    </div>
</div>
<!-- End Block 9 -->
<!--Block 10 CTA Block-->
<div class="container bg--black-coconut" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/dark-grey-triangle-pattern.png);">
    <div class="container__centered" style="text-align: center;">
        
      <?php cta_text($cta); ?>
        
      <div class="center" style="margin-top:2em;">
        <?php hubspot_button($cta_code, "Download The Value-Based Care eBook"); ?>
      </div>
        
      <div style="margin-top:1em;">
        <?php print $share_link_buttons; ?>
      </div>
      
    </div>
</div>
<!--End Block 10-->
<!-- END campaign--node-2382.php -->
