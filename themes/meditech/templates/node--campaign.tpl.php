<?php // This template is set up to control the display of the CAMPAIGN content type

  $url = $GLOBALS['base_url']; // grabs the site url

?>
<!-- start node--campaign.php template -->
 
<?php
$node_id = $node->nid;
include('campaign--node-'.$node_id.'.php');
?>


<script>
  ready(function(){
    
    var h2s = document.querySelectorAll('h2');
    forEach(h2s,function(item){
      // grab text between H2 tags and remove any quote marks...
      var text = item.innerHTML.replace(/['’"“”]+/g, '');
      // grab first letter of each word in string...
      var abbr = text.split(' ').map( function(item){ return item[0] } ).join('');
      // set H2 IDs with abbr...
      item.id = abbr;
      //console.log('A:' + abbr);
    });

  });
</script>
<!-- end node--campaign.tpl.php template -->