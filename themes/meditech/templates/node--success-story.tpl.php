<!-- start node--success-story.tpl.php template -->

<?php // This template is set up to control the display of the 'Success Story' content type... 

  $url = $GLOBALS['base_url']; // grabs the site url
?>

<style>
	.header-micro {
		margin: 0 0 0.75em 0;
		font-weight: normal;
		font-size: 1.15em;
	}

	.bg--dark-green {
		background-color: #073036;
	}
	
	blockquote {
		font-weight: bold;
	}

</style>

<section class="container__centered">
	<div class="container__two-thirds">

		<article>
			<!-- Subtitle (institution name) -->
			<p class="header-micro montserrat js__seo-tool__title"><?php print render($content['field_sub_header_1']); ?></p>

			<!-- Header -->
			<h1 class="header-two js__seo-tool__title"><?php print render($content['field_header_1']); ?></h1>

			<!-- Main banner image -->
			<figure class="body-img">
				<?php // break down to find image file name and alt tag to recreate HTML without width and height attributes...
                $image = $content['field_image']; // add image 
                $image_filename = $image[0]['#item']['filename'];
                $image_alt = $image[0]['#item']['alt'];
                print '<img src="'.$url.'/sites/default/files/images/success/'.$image_filename.'" alt="'.$image_alt.'"/>';
                ?>
			</figure>

			<!-- Main Content -->
			<?php print render($content['field_body']); ?>

		</article>
	</div>


	<!-- SIDEBAR -->
	<aside class="container__one-third">

		<div class="panel bg--dark-green text--white">
			<h3 class="text--meditech-green">AT A GLANCE</h3>
			<?php print render($content['field_long_text_1']); ?>
			<figure>
				<?php // break down to find image file name and alt tag to recreate HTML without width and height attributes...
                $image = $content['field_image_2']; // add image 
                $image_filename = $image[0]['#item']['filename'];
                $image_alt = $image[0]['#item']['alt'];
                print '<img src="'.$url.'/sites/default/files/images/success/'.$image_filename.'" alt="'.$image_alt.'"/>';
                ?>
			</figure>
			<hr style="border-bottom: 1px solid #00bc6f;">
			<?php print render($content['field_long_text_2']); ?>
		</div>

	</aside>
	<!-- END SIDEBAR -->

</section>

<!-- end node--success-story.tpl.php template -->
