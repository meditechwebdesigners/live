<!-- start events-simple-page--node-1761.php -->
<?php // IF is 'Webinars' page ================================================================================== ?>

<section class="container__centered">
<div class="container__two-thirds">

  <h1 class="page__title js__seo-tool__title"><?php print $title; ?></h1>

  <div class="js__seo-tool__body-content">

    <?php print views_embed_view('webinars_page', 'block'); // adds 'webinars page' Views block... ?>

  </div>

</div><!-- END container__two-thirds -->

<!-- SIDEBAR -->
<aside class="container__one-third panel">
  <div class="sidebar__nav">
    <?php
      $messnBlock = module_invoke('menu', 'block_view', 'menu-events-section-side-nav');
      print render($messnBlock['content']); 
    ?>
  </div>
</aside>
<!-- END SIDEBAR -->    
</section>
<!-- end events-simple-page--node-1761.php -->