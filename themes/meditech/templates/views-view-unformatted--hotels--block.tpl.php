<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<!-- START views-view-unformatted--hotels--block.tpl.php -->
<?php if (!empty($title)): ?>

<?php
  switch($title){
    case 'Canton, Lowder Brook, and Westwood':
      $anchorName = '<a name="Canton"></a><a name="Westwood"></a><a name="Lowder Brook"></a>';
    break;
    case 'Foxborough':
      $anchorName = '<a name="Foxborough"></a>';
    break;
    case 'Southcoast':
      $anchorName = '<a name="Fall River"></a>';
    break;
    case 'Atlanta':
      $anchorName = '<a name="Atlanta"></a>';
    break;
    case 'Minnesota':
      $anchorName = '<a name="Eden Prairie"></a><a name="Minnetonka"></a>';
    break;
  }
?>
   
  <?php print $anchorName; ?>
  <h2><?php print $title; ?></h2>

<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <?php print $row; ?>
<?php endforeach; ?>
<!-- END views-view-unformatted--hotels--block.tpl.php -->