<!-- START campaign--node-2353.php -->
<?php // This template is set up to control the display of the MEDITECH ED campaign
$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
    @media all and (max-width: 1000px) {
        .remove {
            display: none;
        }

        .expand {
            width: 100%;
        }
    }

</style>

<div class="js__seo-tool__body-content">

    <!-- Hero -->
    <div class="container background--cover" style="background-image: url(<?php $url; ?>/sites/all/themes/meditech/images/campaigns/nurse-on-tablet-people-in-waiting-room.jpg);">
        <div class="container__centered">

            <div class="container__one-fourth remove">&nbsp;</div>
            <div class="container__one-half expand transparent-overlay--white">
                <h1 style="display:none;" class="js__seo-tool__title">MEDITECH ED</h1>
                <h2 style="margin-top:0;">Increase your emergency department efficiency.</h2>
                <p>In the Emergency Room, saving lives means making the right decisions quickly and safely. With MEDITECH Expanse, you'll have all the information you need to provide patients with prompt and targeted treatment. One complete patient record across ED, acute, and ambulatory environments gives you access to the full patient story &mdash; with intuitive mobile and touch-based technology that will improve your efficiency, as well as your patient's experience.</p>

                <div class="center" style="margin-top:2em;">
                    <?php hubspot_button($cta_code, "Download The Nurse Navigator Case Study"); ?>
                </div>

            </div>
            <div class="container__one-fourth remove">&nbsp;</div>

        </div>
    </div>
    <!-- End of Hero -->


    <!-- Block 2 -->
    <div id="modal1" class="modal">
        <a class="close-modal" href="javascript:void(0)">×</a>
        <div class="modal-content">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-ED-patient-tracker-screen-with-sepsis-alert--large.jpg" alt="MEDITECH ED patient tracker screen shot">
        </div>
    </div>
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>);">
        <div class="container__centered">
            <h2>Quicker input + smoother output = better throughput.</h2>
            <p>In the fast-paced ED, your care team needs an EHR that will help them stay ahead of the curve. With MEDITECH Expanse, you can:</p>
            <div class="container__one-third">
                <ul>
                    <li>Drive efficiency with chief complaints and expert- and evidence-based protocols to generate suggested orders and documentation templates.</li>
                    <li>Use fully tailorable and expandable patient trackers to manage multiple patient lists, monitor wait times, sign up for patients, and easily complete shift hand-offs.</li>
                    <li>Document faster through voice recognition, branching logic, quick text, and other intuitive features.</li>
                    <li>Easily review, sign, and place orders, just like an online shopping cart.</li>
                    <li>Get notified at the first sign of trouble and take action sooner, with predictive analytics and alerts embedded in the physician and care team workflows.</li>
                </ul>
            </div>
            <div class="container__two-thirds">
                <div class="open-modal" data-target="modal1">
                    <div class="tablet--white">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-ED-patient-tracker-screen-with-sepsis-alert.jpg" alt="MEDITECH ED patient tracker screen shot">
                    </div>
                    <div class="mag-bg">
                        <i class="mag-icon fas fa-search-plus"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Block 2 -->


    <!-- Block 3 -->
    <div id="modal2" class="modal">
        <a class="close-modal" href="javascript:void(0)">×</a>
        <div class="modal-content">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-ED-patient-chart--large.jpg" alt="MEDITECH ED chart screen shot">
        </div>
    </div>
    <div class="container background--cover hide__bg-image--mobile" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/X-background-white-left.png); background-color:#d8d2c4;">
        <div class="container__centered">
            <div class="container__two-thirds">
                <div class="open-modal" data-target="modal2">
                    <div class="tablet--white">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-ED-patient-chart.jpg" alt="MEDITECH ED chart screen shot">
                    </div>
                    <div class="mag-bg">
                        <i class="mag-icon fas fa-search-plus"></i>
                    </div>
                </div>
            </div>
            <div class="container__one-third">
                <h2>Designed for the way you work.</h2>
                <p>No more wasting time hunting down the information you need. With MEDITECH Expanse, you have a personalized experience, seeing everything you want to see and nothing you don't. So you can work the way you do best &mdash; quickly and efficiently.</p>
                <p>Select from a library of over 100 general widgets and specialty choices to define your preferred patient summary layout. Tailor your own fixed reference panel, so you never lose sight of key details as you place orders, document care, and navigate the chart. Add new items to this panel on the fly, and even tailor your homepage with personal preferences to better prioritize your workload and notifications.</p>
            </div>
        </div>
    </div>
    <!-- End Block 3 -->


    <!-- Block 4 -->
    <div class="container bg--white">
        <div class="container__centered">
            <div class="remove" style="width:18%; float:left;">&nbsp;</div>
            <div class="container__two-thirds center expand">
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/quotes-blue.png" alt="quotes graphic">
                <p style="font-size:1.5em;">"With the implementation of [MEDITECH] ED, we gained direct links to patient histories, rapid trending of lab data, embedded voice recognition options in the record from any device, intuitive data entry points, and narrative formatting of the record for excellent readability. By making an EHR more intuitive and workflow friendly, patients get their care faster."</p>
                <p><strong><a href="https://blog.meditech.com/author/louis-dandurand-md-northwestern-medical-center">Louis Dandurand, MD</a></strong>
                    <br />Emergency Physician, Northwestern Medical Center</p>
            </div>
        </div>
    </div>
    <!-- End Block 4 -->


    <!-- Block 5 -->
    <div class="container bg--dark-blue">
        <div class="container__centered text--white">
            <h2>See the complete patient story.</h2>
            <p>With Expanse, you'll have an integrated, single patient record at your fingertips. That means when a patient comes in, you get their complete story - so you can deliver care that's appropriate for the person, and not just the problem. Benefits include:</p>
            <div class="container__centered" style="margin-top:2em;">
                <div class="container__one-half">
                    <div class="container__one-half center expand" style="margin-bottom:2em;">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon-sharing-data.png" alt="image" style="width:40%;">
                        <p>Shared problem, allergy, and medication lists across care settings</p>
                    </div>
                    <div class="container__one-half center expand" style="margin-bottom:2em;">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon-medical-history.png" alt="image" style="width:40%;">
                        <p>Instant access to a patient's complete medical history</p>
                    </div>
                </div>
                <div class="container__one-half">
                    <div class="container__one-half center expand" style="margin-bottom:2em;">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon-data-retrieval.png" alt="image" style="width:40%;">
                        <p>Immediate retrieval of patient data from other care networks through discrete C-CDAs and medication claims history</p>
                    </div>
                    <div class="container__one-half center expand" style="margin-bottom:2em;">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon-data-exchange.png" alt="image" style="width:40%;">
                        <p>Smooth transfer of documentation and orders upon admission to an acute bed</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Block 5 -->

    <!-- Virtual Assistant Block -->
    <div class="container no-pad">
        <div class="gl-container">
            <div class="container__one-half gl-text-pad">
                <h2>"OK, MEDITECH." <span style="font-weight: 400;">Make my life easier.</span></h2>
                <p>The most intuitive EHR for physicians just got easier. Now physicians can promptly find what they’re looking for in their EHR simply by asking. Powered by the same advanced AI you use on your personal devices, Expanse Virtual Assistant works just like you’d expect it to — lowering your cognitive load and allowing you to focus on your patients, not your devices.</p>
                <div style="margin-top: 1.5em;">
                    <?php hubspot_button('fca506cd-8f0c-4f7e-9b94-6026b7d3d4ce', "Learn More"); ?>
                </div>
            </div>
            <div class="container__one-half flex-order--reverse background--cover" style="background-image: url( <?php print $url; ?>/sites/all/themes/meditech/images/campaigns/doctor-using-virtual-assistant-speaking-with-patients.jpg); min-height:20em;">
            </div>
        </div>
    </div>
    <!-- Virtual Assistant Block -->

    <!-- Block 6 -->
    <div class="content__callout border-none">
        <div class="content__callout__media">
            <div class="content__callout__image-wrapper">
                <div class="video js__video" data-video-id="260991775">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-ED-physicians--video-overlay.jpg" alt="MEDITECH ED Video cover shot">
                    </figure>
                    <a class="video__play-btn" href="https://vimeo.com/260991775"></a>
                    <div class="video__container"></div>
                </div>
            </div>
        </div>
        <div class="content__callout__content no-l-padding">
            <div class="content__callout__body">
                <div class="content__callout__body__text">
                    <h2>Improve the patient experience.</h2>
                    <p>An EHR that brings all patient information to the forefront improves the experience of patients as well as providers. Don't turn your back on patients; document alongside them so they can better understand their conditions and treatments. See how MEDITECH's ED is already making a difference at Firelands Regional Medical Center.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- End Block 6 -->


    <!-- Block 7 -->
    <div class="container background--cover hide__bg-image--tablet" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/female-physician-with-female-elderly-patient.jpg);">
        <div class="container__centered">
            <h2>Make care coordination easier, automatically. </h2>
            <div class="container__one-half">
                <p>Prepare for discharge — the minute your patient steps into the ED. With Expanse, information recorded throughout the patient's ED visit automatically populates their multidisciplinary discharge plan — shared by physicians, nurses, and other clinicians. That means all appropriate orders, referrals, and instructions are in place before a patient's transition to the next level of care.</p>
                <p>Visit, discharge, and educational materials are automatically accessible via patient portals, while documentation is pushed out to connected network care providers for easy follow-ups. Practices using MEDITECH also receive notification of their patients' ED visits through event-driven registries, so they can schedule timely follow up visits.</p>
            </div>
        </div>
    </div>
    <!-- End Block 7 -->


    <!-- Block 8 -->
    <div id="modal3" class="modal">
        <a class="close-modal" href="javascript:void(0)">×</a>
        <div class="modal-content">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-ED-throughput-BCA-screenshot--large.png" alt="MEDITECH ED screen shot">
        </div>
    </div>
    <div class="container background--cover hide__bg-image--mobile" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/X-background-white-left.png); background-color:#00a094;">
        <div class="container__centered">
            <div class="container__two-thirds">
                <div class="open-modal" data-target="modal3">
                    <div class="tablet--white">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-ED-throughput-BCA-screenshot.png" alt="MEDITECH ED screen shot">
                    </div>
                    <div class="mag-bg">
                        <i class="mag-icon fas fa-search-plus"></i>
                    </div>
                </div>
            </div>
            <div class="container__one-third text--white">
                <h2>Show results, with dynamic reports. </h2>
                <p>Do more with your data. Measure ED throughput, volumes, operational time of day, and much more, using our web-based Business and Clinical Analytics. MEDITECH Expanse offers a diverse portfolio of reports and dashboards for analyzing data across your organization.</p>
            </div>
        </div>
    </div>
    <!-- End Block 8 -->

    <!-- New Efficiency Dashboard Block -->

    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/back-angle-runner-on-road4.png);">
        <div class="container__centered">
            <div class="container__one-half transparent-overlay text--white">
                <h2>Full speed ahead</h2>
                <p>Empower your physicians to get the most from their EHR with the Expanse Efficiency Dashboard. With detailed user metrics and real-time reporting, the dashboard offers actionable insights for adjusting workflows and personalizing the EHR experience to each provider’s needs. The result is happier physicians who get their work done on time and focus on what’s most important to them &mdash; their patients. Identify opportunities for targeted training, EHR personalization, and individualized support, so you can ensure confident, proficient, satisfied users. </p>
            </div>
            <div class="container__one-half">
                &nbsp;
            </div>

        </div>
    </div>

    <!-- End Efficiency Dashboard Block -->

</div>
<!-- end js__seo-tool__body-content -->


<!-- Block 9 - CTA Block -->
<div class="container">
    <div class="container__centered" style="text-align: center;">

        <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
        <h2>
            <?php print $cta->field_header_1['und'][0]['value']; ?>
        </h2>
        <?php } ?>

        <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
        <div>
            <?php print $cta->field_long_text_1['und'][0]['value']; ?>
        </div>
        <?php } ?>

        <div class="center" style="margin-top:2em;">
            <?php hubspot_button($cta_code, "Download the Halifax Health Mobility Case Study"); ?>
        </div>

        <div style="margin-top:1em;">
            <?php print $share_link_buttons; ?>
        </div>

    </div>
</div>
<!-- End Block 9 -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>
<!-- END campaign--node-2353.php -->
