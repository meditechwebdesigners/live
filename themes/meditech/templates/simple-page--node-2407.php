<!-- start simple-page--node-2407.tpl.php template -- PRIVACY POLICY PAGE -->

    <div class="container container__centered">

      <div class="container__two-thirds">
        <h1><?php print $title; ?></h1>
        <?php print render($content['field_body']); ?>
      </div>

      <div class="container__one-third panel" style="margin-right:0;">
        <div class="sidebar__nav">
          <?php
            $policyMenu = module_invoke('menu', 'block_view', 'menu-privacy-policy-menu');
            print render($policyMenu['content']); 
          ?>
        </div>
      </div>

    </div>
    
<!-- end simple-page--node-2407.tpl.php template -->    