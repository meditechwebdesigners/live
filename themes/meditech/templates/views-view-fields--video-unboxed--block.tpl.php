<?php // This template is for each row of the Views block: VIDEO UNBOXED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ ....................... 
  $url = $GLOBALS['base_url']; // grabs the site url
?>
<!-- start views-view-fields--video-unboxed--block.tpl.php template -->
  <div class="video video--full-width js__video" data-video-id="<?php print $fields['field_video_url']->content; ?>">
    <figure class="video__overlay">
      <?php // if the overlay image was added by user, then show overlay image, otherwise show default overlay image...
        if( !empty($fields['field_video_overlay_image']->content) ){
          print $fields['field_video_overlay_image']->content; 
        }
        else{
        ?>
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/video-overlay.jpg" alt="Video Covershot">
      <?php } ?>
    </figure>
    <a href="https://vimeo.com/<?php print $fields['field_video_url']->content; ?>?&autoplay=1" class="video__play-btn"></a>
    <div class="video__container"></div>
  </div>
  <?php // add Edit Video link...
    if( user_is_logged_in() ){ 
      print '<div style="display:block; text-align:right;"><span style="font-size:12px;">'; print l( t('Edit Video'),'node/'. $fields['nid']->content .'/edit' ); print "</span></div>"; 
    } 
  ?>  
<!-- end views-view-fields--video-unboxed--block.tpl.php template -->