<!-- START campaign--node-3297.php -->

<?php // This template is set up to control the display of the campaign: Google Resources
$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');
$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<div class="js__seo-tool__body-content">

    <div class="js__seo-tool__body-content">


        <div class="container container__centered">
            <div class="container__three-fourths">
                <h1 class="js__seo-tool__title" style="margin-top:0;"><?php print $title; ?></h1>
                <h2>Tomorrow's Possibilities. Today.</h2>
                <p>MEDITECH and Google Cloud are partnering to fuel the future of healthcare, by giving clinicians the tools they need to best serve their patients and communities.</p>
            </div>
            <div class="container__one-fourth">

            </div>
        </div>

        <style>
            .video2 {
                position: relative;
                padding-bottom: 37%;
                /* 16:9 */
                height: 0;
            }

            .video2 iframe {
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
            }

            @media (max-width: 800px) {
                .video2 {
                    padding-bottom: 50%;
                }
            }

        </style>
        <!-- Block 1 -->
        <div class="container bg--black-coconut" style="background-color:black;">
            <div class="container__centered">
                <div class="container__two-thirds video2">
                    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/RLX6w1h-_Qg?rel=0" title="YouTube video player" frameborder="0" allow="encrypted-media;" allowfullscreen></iframe>
                </div>
                <div class="container__one-third center" style="padding-top:2em;">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Google-Partner-Award-badge-2022.png" alt="Google Partner Award healthcare badge 2022" style="width:50%;"><br>
                    <a href="https://ehr.meditech.com/news/meditech-wins-google-cloud-industry-solution-partner-of-the-year-award-for-healthcare"><strong>MEDITECH</strong> wins Google Cloud Industry Solution Partner of the Year Award for Healthcare</a>
                </div>
            </div>
        </div>
        <div class="container container__centered center auto-margins">
            <h2>With solutions like <a href="https://ehr.meditech.com/ehr-solutions/cloud-platform">MEDITECH Cloud Platform</a> and <a href="https://ehr.meditech.com/ehr-solutions/meditech-as-a-service-maas">MEDITECH as a Service (MaaS)</a>, healthcare organizations can achieve:</h2>
        </div>
        <!-- End of Block 1 -->


        <!-- Block 2 MEDITECH and Google Cloud -->
        <div class="container no-pad">
            <div class="container__centered">

                <div class="container__one-fourth shadow-box center" style="background: rgb(79,202,178);
background: linear-gradient(0deg, rgba(79,202,178,0.08) 0%, rgba(79,202,151,0.08) 28%, rgba(255,255,255,1) 67%);">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Mobility-google--icon.svg" alt="mobility icon" style="height: 100px;">
                    <h3>Mobility</h3>
                    <p>Uninterrupted, smart device-enabled access to secure patient data enables providers to deliver care on the go.</p>
                </div>

                <div class="container__one-fourth shadow-box center" style="background: rgb(79,202,178);
background: linear-gradient(0deg, rgba(79,202,178,0.08) 0%, rgba(79,202,151,0.08) 28%, rgba(255,255,255,1) 67%);">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Productivity-google--icon.svg" alt="Productivity icon" style="height: 100px;">
                    <h3>Productivity</h3>
                    <p>Advanced clinical decision support and customizable workflows reduce the time clinicians spend searching patient charts.</p>

                </div>

                <div class="container__one-fourth shadow-box center" style="background: rgb(79,202,178);
background: linear-gradient(0deg, rgba(79,202,178,0.08) 0%, rgba(79,202,151,0.08) 28%, rgba(255,255,255,1) 67%);">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Affordability-google--icon.svg" alt="Affordability icon" style="height:100px;">
                    <h3>Affordability</h3>
                    <p>Financial management features ensure quick, convenient transactions while helping to cut inventory costs.</p>
                </div>

                <div class="container__one-fourth shadow-box center" style="background: rgb(79,202,178);
background: linear-gradient(0deg, rgba(79,202,178,0.08) 0%, rgba(79,202,151,0.08) 28%, rgba(255,255,255,1) 67%);">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Security-google--icon.svg" alt="Security Icon" style="height:100px;">
                    <h3>Security</h3>
                    <p>Built on Google Cloud's highly secure infrastructure to protect systems, data, and users.</p>
                </div>

            </div>
        </div>
        <!-- Block 2 MEDITECH and Google Cloud -->


        <!-- Customer Strategies -->

        <div class="container__centered" style="padding-top: 1.5em;">
            <h2>EHR Solutions Powered by the Cloud</h2>
        </div>


        <div class="container no-pad">

            <div class="container__centered">
                <h3>MaaS</h3>
                <p>MEDITECH's cost-effective, scalable, cloud-based EHR solution for organizations of any size or specialty.<br>
                    <a href="https://ehr.meditech.com/ehr-solutions/meditech-as-a-service-maas">MEDITECH as a Service: Get More, Faster</a>
                </p>
            </div>

            <div class="container no-pad">
                <div class="container__centered">

                    <h3>MEDITECH Cloud Platform</h3>
                    <p>MEDITECH's growing suite of cloud-native solutions, built on Google Cloud Platform.
                        Introducing MEDITECH Cloud Platform [<a href="https://www.youtube.com/watch?v=wA7AogUNo1c">video</a>] + [<a href="https://home.meditech.com/en/d/mktcontent/otherfiles/cloudplatform.pdf" target="_blank">flyer</a>]</p>
                    <ul>
                        <li><a href="https://www.youtube.com/watch?v=C4q7SdagzN4">High Availability SnapShot: Because Care Can't Stop</a> [video]</li>
                        <li><a href="https://www.youtube.com/watch?v=PQT0u-D-fMI">Expanse Patient Connect: Communicate at the Speed of Life</a> [video]</li>
                        <li><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/virtualcareflyer.pdf" target="_blank">Virtual Care: Stay Connected, No Matter What</a> [PDF]</li>
                        <li><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/expansenow.pdf" target="_blank">Expanse Now: Balance Amid Burnout</a> [PDF]</li>
                    </ul>

                </div>
            </div>

            <div class="container no-pad">
                <div class="container__centered">

                    <h3>Google Healthcare & Life Sciences</h3>
                    <p>Cloud-based solutions that empower healthcare organizations to innovate and transform care delivery.</p>
                    <ul>
                        <li><a href="https://meditech-podcast.simplecast.com/episodes/giving-clinicians-superpowers-with-cloud-solutions">Giving Clinicians Superpowers with Cloud Solutions</a> [podcast]</li>
                        <li><a href="https://cloud.google.com/why-google-cloud/">Why Google Cloud</a> [web page]</li>
                        <li><a href="https://workspace.google.com/industries/healthcare/">Google Workspace for Healthcare</a> [web page]</li>
                        <li><a href="https://workspace.google.com/learn-more/cloud-compliance-healthcare.html">Top 5 proven strategies for healthcare in the cloud</a> [article]</li>
                        <li><a href="https://cloudonair.withgoogle.com/events/hcls-secure-cloud-webinars">Healthcare-grade secure cloud</a> [On-demand webinar]</li>
                        <li><a href="https://cloudonair.withgoogle.com/events/covid-future-of-virtual-care">The Future of Virtual Care</a> [On-demand webinar]</li>
                    </ul>

                </div>
            </div>

            <div class="container no-pad">
                <div class="container__centered">

                    <h3>Benefits of MEDITECH and GCP</h3>
                    <ul>
                        <li><a href="https://cloud.google.com/blog/transform/cloud-breathes-new-life-into-electronic-health-records">How a game-changing app in the cloud signals new life for Electronic Health Records</a> [Blog]</li>
                        <li><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/googleflyerorganization.pdf">How MEDITECH and Google Cloud support your organization</a> [PDF]</li>
                        <li><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/googleflyerremarks.pdf">What sets MEDITECH and Google Cloud apart</a> [PDF]</li>
                        <li><a href="https://blog.meditech.com/how-google-and-meditech-are-charting-a-new-course-in-digital-healthcare">How Google and MEDITECH are charting a new course in digital healthcare</a> [blog]</li>
                        <li><a href="https://drive.google.com/file/d/1_D9iagpXrKSKNFWzfi_VD-kZo7kohgPw/view" target="_blank">Why MEDITECH and Google Cloud</a> [PDF]</li>
                        <li><a href="https://cloudonair.withgoogle.com/events/hcls-caregiver-agility-webinars?talk=talk6">Leading the Digital Transformation in Healthcare</a> [Google podcast] + [<a href="https://services.google.com/fh/files/misc/meditech_summary.pdf" target="_blank">executive summary</a>]</li>
                        <li><a href="https://blog.meditech.com/how-meditech-and-google-deliver-frustration-free-access-to-the-cloud">How MEDITECH and Google deliver frustration-free access to the cloud</a> [blog]</li>
                        <li><a href="https://blog.meditech.com/5-blogs-on-why-the-skys-the-limit-with-cloud-technology">5 blogs on why the sky's the limit with cloud technology</a> [blog]</li>
                        <li><a href="https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy5idXp6c3Byb3V0LmNvbS8xMDkzMDI4LnJzcw/episode/QnV6enNwcm91dC04MTAzNDU0?sa=X&ved=0CAUQkfYCahcKEwiIxJzZ87LvAhUAAAAAHQAAAAAQAg">Managing Medical Data with MEDITECH's VP of Advanced Technology Scott Radner</a> [podcast]</li>
                        <li><a href="https://www.onixnet.com/health-life-sciences/meditech-interview">How Onix and Google Cloud Help MEDITECH Ensure Secure Access to Critical Healthcare Information</a> [case study]</li>
                        <li><a href="https://blog.meditech.com/how-cloud-infrastructure-supports-our-business-and-customers-during-the-covid-19-pandemic">How cloud infrastructure supports our business and customers during the COVID-19 pandemic</a> [blog]</li>
                    </ul>

                </div>
            </div>

            <div class="container no-pad">
                <div class="container__centered">

                    <h3>The MEDITECH-Google Partnership</h3>
                    <ul>
                        <li><a href="https://ehr.meditech.com/news/meditech-wins-google-cloud-industry-solution-partner-of-the-year-award-for-healthcare">MEDITECH wins Google Cloud Industry Solution Partner of the Year Award for Healthcare</a> [press release]</li>
                        <li><a href="https://ehr.meditech.com/news/meditech-announces-collaboration-with-google-cloud-to-bring-ehrs-to-the-public-cloud">MEDITECH Announces Collaboration with Google Cloud to Bring EHRs to the Public Cloud</a> [press release]</li>
                        <li><a href="https://ehr.meditech.com/news/meditech-selects-google-cloud-to-power-ehr-solutions">MEDITECH selects Google Cloud to power EHR solutions</a> [press release]</li>
                        <li><a href="https://blog.meditech.com/why-meditechs-transition-to-google-cloud-is-the-beginning-of-a-bigger-movement-ceo-howard-messing">Why MEDITECH's transition to the cloud is the ‘beginning of a bigger movement': CEO Howard Messing</a> [blog]</li>
                        <li><a href="https://blog.meditech.com/helping-organizations-focus-on-care-not-it-discussing-meditechs-cloud-journey">Helping organizations focus on care, not IT: Discussing MEDITECH's cloud journey</a> [blog]</li>
                    </ul>

                </div>
            </div>

        </div>
        <!-- End of Customer Strategies -->

    </div>

</div>
<!-- end js__seo-tool__body-content -->


<!-- LAST Block -->
<div class="container">
    <div class="container__centered center">

        <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
        <h2>
            <?php print $cta->field_header_1['und'][0]['value']; ?>
        </h2>
        <?php } ?>

        <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
        <div>
            <?php print $cta->field_long_text_1['und'][0]['value']; ?>
        </div>
        <?php } ?>

        <div class="center" style="margin-top:2em;">
            <?php //hubspot_button($cta_code, "Join our Mailing List and Stay Connected with MEDITECH"); ?>
        </div>

        <div style="margin-top:1em;">
            <?php print $share_link_buttons; ?>
        </div>

    </div>
</div>
<!-- End of LAST Block -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>

<!-- Add this to the bottom of the template -->

<!-- END campaign--node-3297.php -->
