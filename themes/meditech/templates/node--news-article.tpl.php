<?php
// This template is set up to control the display of the 'news-article' content type 
// as well as the 'news-tags' taxonomy dynamic pages.
// 
// SECTION 1 describes the structure of the content when it's being displayed as an individual news article page.
// 
// SECTION 2 describes the structure of the content when it's being displayed in the main News page 
// OR as a list on a taxonomy page.


// get current URL info to determine which layout to render...
$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$currentURLarray = explode('/', $currentURL);
$urlArrayCountMinusThree = count($currentURLarray) -3;
$urlArrayCountMinusTwo = count($currentURLarray) -2;
$urlArrayCountMinusOne = count($currentURLarray) -1;
$lastURLsegment = $currentURLarray[$urlArrayCountMinusOne];
$secondToLastURLsegment = $currentURLarray[$urlArrayCountMinusTwo];
$thirdToLastURLsegment = $currentURLarray[$urlArrayCountMinusThree];

include('inc-share-buttons.php');

// [SECTION 1] if a NEWS ARTICLE node is being shown then render the following =============================================================
// ex: ../news/article-name

if($secondToLastURLsegment == 'news' || $thirdToLastURLsegment == 'news' || $secondToLastURLsegment == "node" || $thirdToLastURLsegment == "node"){
?>
  <!-- start node--news-article.tpl.php template SECTION 1 -->

  <?php
  // check first to see if field collection exists...
  $field_collection = field_get_items('node', $node, 'field_fc_button_1');
  // FUNCTION TO PULL IN CONTENT FOR A FIELD COLLECTION ITEM...
  if( $field_collection != '' && !empty($field_collection) ){
    $button_1 = field_collection_data($node, 'field_fc_button_1');
  }  
  ?>

  <!-- NEWS ARTICLE -->
  <section class="container__centered">

    <div class="container__two-thirds">
     
      <article class="no-pad--top">
      
      <?php 
        // if video exists for node, then proceed to render Video DIV, otherwise look for main image...
        // get value from field_video to pass to View (if video exists)...
        $video = render($content['field_video']);
        $main_image = render($content['field_news_article_main_image']);
        $photo_credit = render($content['field_photo_credit']);
  
        if(!empty($video)){ // if the page has a video...
          
          print '<!-- VIDEO -->';
          // remove apostrophes from titles to prevent View from breaking...
          $video_filtered = str_replace("&#039;", "'", $video);
          // adds 'video' Views block...
          print views_embed_view('video_unboxed', 'block', $video_filtered);
          print '<!-- END VIDEO -->';
          
        }
        // if image exists for node, then proceed to render DIV...
        else {
          
          if(!empty($main_image)){
          ?>
            <figure class="news__article__img">
              <?php print render($content['field_news_article_main_image']); ?>
              <?php if(!empty($photo_credit)){ ?>
              <p><cite><?php print render($content['field_photo_credit']); ?></cite></p>
              <?php } else { ?>
              <p>&nbsp;</p>
              <?php } ?>
            </figure>
          <?php
          }
        
        }
        ?>
        
        <h1 class="page__title js__seo-tool__title"><?php print $title; ?></h1>

        <?php if(!empty($content['field_subtitle'])){ ?>
          <p class="italic js__seo-tool__body-content"><?php print render($content['field_subtitle']); ?></p>
        <?php } ?>

        <div class="inline__text__wrapper">
        <?php if($node->published_at){ ?>
          <p><span class="news__article__callout"><time datetime="<?php print date('Y-m-d', $node->published_at); ?>" itemprop="datePublished"><?php print date('F j, Y', $node->published_at); ?></time></span>
        <?php } else { // for previewing purposes... ?>
          <p><span class="news__article__callout"><em>Unpublished: No Date</em></span>
        <?php } ?>
          &nbsp;|&nbsp;</p>
          <ul class="news__article__filters tag_link_news_article_gae">
            <?php generate_news_tag_links($node); ?>
          </ul>
        </div>

        <div class="js__seo-tool__body-content">
          <?php 
          if(!empty($content['field_body'])){
            print render($content['field_body']); // main article content 
          }
          else{
            print render($content['field_summary']); // summary content 
          } 
          
          // check to see if this is or has an EXTERNAL LINK ARTICLE or not...
          $external_source = field_view_field('node', $node, 'field_source_name');
          // article is external...
          if(!empty($external_source)){
            print '<p>Read the entire article: <a href="'.render($content['field_source_url']).'" title="external link" target="_blank">';
            print render($content['field_source_name']).'</a></p>';
          }
          ?>
        </div>

        <div>
          <?php 
          if($node->nid != 3295){ // block 1 news article from showing share buttons
            print $share_link_buttons; 
          }
          ?>
        </div>
        
        
        <?php            
        // check to see if button exists...
        if( isset($button_1) ){
          // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
          if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
            $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
          ?>
            <div class="button--hubspot news_article_cta_gae" style="border-top:1px solid #999; padding-top:1em; margin:1em 0;">
              <?php 
              if(!empty($content['field_text_1'])){
                print '<p><strong>'.render($content['field_text_1']).'</strong></p>'; 
              }
              ?>
              <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
            </div>
          <?php }else{ ?>
            <div class="news_article_cta_gae" style="border-top:1px solid #999; padding-top:1em; margin:1em 0;">
              <?php 
              if(!empty($content['field_text_1'])){
                print '<p><strong>'.render($content['field_text_1']).'</strong></p>'; 
              }
              ?>
              <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
            </div>
          <?php 
          } 
        }
        ?>

      
      </article>

      <?php // SEO Tool for internal use...
      if( user_is_logged_in() ){
        print '<!-- SEO Tool is added to this DIV -->';
        print '<div class="container js__seo-tool"></div>';
      } 
      ?>

    </div>

    <!-- SIDEBAR -->
    <aside class="container__one-third">
      <?php print views_embed_view('news_related_articles', 'block'); // adds 'news - related articles' Views block... ?>
      
      <div class="panel">
        <div class="sidebar__nav news_sidebar_gae">
          <?php
            $mnsBlock = module_invoke('menu', 'block_view', 'menu-news-sidebar');
            print render($mnsBlock['content']); 
          ?>
        </div>
      </div>
      
    </aside>
    <!-- END SIDEBAR -->

  </section><!-- END SECTION -->
  
  
  <!-- Google Structured Data -->
  <?php // get video overlay if video or image NOT a video page...
  if(!empty($video)){
    // get video data...
    $video_data = field_get_items('node', $node, 'field_video');
    $image_url = $video_data[0]['entity']->field_video_overlay_image['und'][0]['filename'];
  }
  else{
    $image_url = file_create_url($node->field_news_article_main_image['und'][0]['uri']);
  }
  ?>
  <script type="application/ld+json">
    {
      "@context" : "https://schema.org/",
      "@type" : "NewsArticle",
      "headline" : "<?php print $title; ?>",
      "datePublished" : "<?php print date('Y-m-d', $node->published_at); ?>",
      "dateModified" : "<?php print date('Y-m-d', $node->changed); ?>",
      "image" : [ "<?php print $image_url; ?>" ],
      "author" : {
        "@type" : "Organization",
        "name" : "MEDITECH"
      },
      "publisher" : {
        "@type" : "Organization",
        "name" : "MEDITECH",
        "logo" : {
          "@type" : "ImageObject",
          "name" : "MEDITECHlogo",
          "width" : "600",
          "height" : "60",
          "url" : "https://ehr.meditech.com/sites/all/themes/meditech/images/MEDITECH-logo--structured-data-version.jpg"
        }
      },
      "mainEntityOfPage" : "<?php print $currentURL; ?>"
    }
  </script> 
  
  <script>
    document.getElementsByTagName("h1")[0].innerHTML = 
      document.getElementsByTagName("h1")[0].innerHTML
      .replace(/((?!<sup>\s*))&reg;((?!\s*<\/sup>))/gi, '<sup>&reg;</sup>')
      .replace(/((?!<sup>\s*))®((?!\s*<\/sup>))/gi, '<sup>&reg;</sup>')
    ;
  </script>  

<!-- END node--news-article.tpl.php template SECTION 1 -->




<?php
}
else{
  // [SECTION 2] if the NEWS-TAGS taxonomy page is being shown then render the following =================================================================
  // ex: ../news-tags/physician
?>

  <!-- start node--news-article.tpl.php template SECTION 2 -->
  <?php 
  // get node URL from node ID...
  $nodeURL = url('node/'. $node->nid); 
  // check node's taxonomy news terms...
  $news_tags = field_view_field('node', $node, 'field_news_tags'); 
  // 'field_news_tags' is the machine name of the field in the content type that contains the taxonomy terms

  // list taxonomy news term associated with article...
  $tag_array = array();
  foreach($news_tags['#items'] as $news_tag){
    $term = taxonomy_term_load($news_tag['tid']);
    $term_name = $term->name;
    $tag_array[] = $term_name;
  }  

  // if taxonomy has the VIDEOS tag and an actual video is attached to the node, then show image thumbnail...
  if( in_array('Videos', $tag_array) && !empty($node->field_video) ){
      
    // use node to look for existance of video overlay image...
    foreach($node->field_video['und'] as $v){
      $video_id = $v['target_id'];
    }
    $video_node = node_load($video_id);    
    $video_overlay_uri = $video_node->field_video_overlay_image['und'][0]['uri'];
    $video_overlay_uri_array = explode('/', $video_overlay_uri);
    $video_overlay_image = end($video_overlay_uri_array);
    ?>
    <figure class="container no-pad">
      <div class="container__one-third">
        <?php
        // if video overlay image exists, use supplied image...
        if($video_overlay_image != ''){

          // get crop orientation option to set proper class name for image...
          $video_image_orientation_array = $video_node->field_image_1_crop_orientation;
          $video_image_orientation_id = $video_image_orientation_array['und'][0]['tid'];
          $video_image_orientation_term = taxonomy_term_load($video_image_orientation_id);
          $video_image_orientation_term_name = $video_image_orientation_term->name;

          if($video_image_orientation_term_name == 'Center Image'){
            $crop = 'crop-center';
          }
          elseif($video_image_orientation_term_name == 'Show Right Side of Image'){
            $crop = 'crop-right';
          }
          else{
            $crop = 'crop-left';
          }
          $video_overlay_image_url = 'https://ehr.meditech.com/sites/default/files/styles/video-overlay-breakpoints_theme_meditech_big-medium_2x/public/images/video/'.$video_overlay_image;

        }
        else{

          $video_overlay_image_url = 'https://ehr.meditech.com/sites/all/themes/meditech/images/news/main-news-hero_video-background.png';
          $crop = 'crop-left';

        }
        ?>
      <div class="square-img-cropper <?php print $crop; ?>">
        <a class="tag_link_news_article_gae" href="<?php print $nodeURL; ?>"><img src="<?php print $video_overlay_image_url; ?>" style="width:auto;" /></a>
      </div>
    </div>
    <figcaption class="container__two-thirds" style="margin-right:0;">
      <h3 class="header-four no-margin"><a class="tag_link_news_article_gae" href="<?php print $nodeURL; ?>"><?php print $title; ?></a></h3>
      <div class="inline__text__wrapper">
        <p><span class="snippet__card__text--callout"><time datetime="<?php print date('Y-m-d', $node->published_at); ?>" itemprop="datePublished"><?php print date("F j, Y", $node->published_at); ?></time></span>&nbsp;&mdash;&nbsp;</p>
        <?php
        $summary = field_view_field('node', $node, 'field_summary');
        print render($summary); 
        ?>
      </div>
    </figcaption>
    <div style="float:left; width:100%;">
      <ul class="news__article__filters tag_link_news_article_gae" style="float:left; width:100%;">
        <?php generate_news_tag_links($node); ?>
      </ul>
    </div>
  </figure>

  <hr>
  <?php
  }
  // no VIDEOS tag, don't show a thumbnail...
  else{
  ?>
    <div>

      <h3><a class="taxonomy_result_link_gae" href="<?php print $nodeURL; ?>"><?php print $title; ?></a></h3>

      <div class="inline__text__wrapper">
        <p><span class="snippet__card__text--callout"><time datetime="<?php print date('Y-m-d', $node->published_at); ?>" itemprop="datePublished"><?php print date("F j, Y", $node->published_at); ?></time></span>&nbsp;&mdash;&nbsp;</p>
        <?php
        $summary = field_view_field('node', $node, 'field_summary');
        print render($summary); 
        ?>
      </div>

      <ul class="news__article__filters tag_link_news_article_gae">
        <?php generate_news_tag_links($node); ?>
      </ul>

      <hr>

    </div>
  <?php
  }
  ?>
  
<!-- END node--news-article.tpl.php template SECTION 2 -->
<?php
}
?>