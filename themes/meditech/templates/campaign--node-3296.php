<!-- START campaign--node-3296.php -->
<?php // This template is set up to control the display of the NEW campaign Care Coordination content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');
$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>


<div class="js__seo-tool__body-content">

    <style>
        .button-vertical-adjustment {
            margin-top: 8em;
        }

        @media all and (max-width: 50em) {
            .content__callout__content {
                padding-left: 6%;
                color: #3e4545;
                padding-top: 0em;
            }

            .video {
                max-width: 100%;
            }
        }

        .video--shadow {
            border-radius: 6px;
            box-shadow: 13px 13px 40px rgba(0, 0, 0, 0.3);
        }


        @media (max-width: 800px) {
            .button-vertical-adjustment {
                margin-top: 1em;
            }
        }

        .spacing {
            margin-bottom: 1em;
        }

        @media (max-width: 800px) {
            .spacing {
                margin-bottom: 0;
            }
            .transparent-overlay {
                margin-bottom: 1em;
            }
        }

    </style>

    <!-- Block 1 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/hospital-settings-green.jpg);">
        <div class="container__centered">
            <div class="container__two-thirds transparent-overlay text--white">
                <h1 class="text--white js__seo-tool__title" style="margin-top:0;">
                    MEDITECH's Care Coordination Connects Every Point of Patient Care
                </h1>
                <h2 class="text--white">
                    Coordinate care with the power of one EHR.
                </h2>
                <p>Healthcare involves many different settings and providers, but in the end, it all comes down to a single patient. With <a href="https://ehr.meditech.com/expanse">MEDITECH Expanse</a>, you'll have one system to navigate all the complex twists and turns—so you can effectively treat the whole patient, wherever they may be in their journey.</p>
            </div>
            <div class="container__one-third center">
                <div class="btn-holder--content__callout button-vertical-adjustment">
                    <div class="center">
                        <?php hubspot_button($cta_code, "Sign Up For The Care Across The Continuum Webinar"); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Block 1 -->

    <!-- Block 2 - Video Block -->
    <div class="content__callout border-none" style="background-color: #E6E9EE !important;">
        <div class="content__callout__media">
            <div class="content__callout__image-wrapper" style="padding:3em !important;">
                <div class="video js__video video--shadow" data-video-id="280779080">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--care-coordination-2018.jpg" alt="Give clinicians the freedom to roam video image">
                    </figure>
                    <a class="video__play-btn" href="https://vimeo.com/263515474"></a>
                    <div class="video__container"></div>
                </div>
            </div>
        </div>
        <div class="content__callout__content">
            <div class="content__callout__body">
                <div class="content__callout__body__text">
                    <h2>Give clinicians the freedom to roam.</h2>
                    <p>Just a quick tap or swipe is all it takes for you to get the latest info about your patient, from anywhere in the healthcare continuum. Use secure messaging and texting to stay connected with your teams, and closely monitor your patient's ups and downs with our multidisciplinary care plans, status boards, and <a href="https://ehr.meditech.com/ehr-solutions/meditech-surveillance">surveillance tools</a>—all from one central location.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- End Block 2 -->

    <!-- Block 3 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/doctor-smiling-with-stethescope.jpg);">
        <div class="container__centered">
            <div class="container__two-thirds transparent-overlay text--white">
                <h2>
                    A smooth transition, every time.
                </h2>
                <p>With MEDITECH's multidisciplinary discharge routines and case management tools, you'll ensure continuous care as patients safely transition to other settings—by verifying that the appropriate appointments, referrals, education, medications, equipment, and support teams are all in place. Use our call management functionality for post-discharge follow up and documentation, to <a href="https://ehr.meditech.com/ehr-solutions/bridging-the-gaps-in-care">ensure that your patients stay on the right track</a> and avoid readmissions.</p>

            </div>
        </div>
    </div>
    <!-- End Block 3 -->

    <!-- Block 4 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/pharmacy-medicine-shelves-darkened.jpg);">
        <div class="container__centered text--white">
            <h2>
                See interoperability in action.
            </h2>
            <p>When patients are seen outside your network, MEDITECH's <a href="https://ehr.meditech.com/ehr-solutions/meditech-interoperability">interoperable</a> EHR fills the gaps by arming providers with the most up-to-date information they need to address healthcare's toughest challenges. For example, you can improve care for patients struggling with chronic pain and intervene with any drug-seeking behaviors by using MEDITECH to:</p>
            <div class="container no-pad text--white spacing">
                <div class="container__one-half transparent-overlay">
                    <p class="no-margin">Meet DEA regulations for e-Prescribing Controlled Substances (EPCS).</p>
                </div>
                <div class="container__one-half transparent-overlay">
                    <p class="no-margin">Pull in discrete problems (e.g., addiction) and medications from outside your network, as well as CCDs.</p>
                </div>
            </div>
            <div class="container no-pad text--white">
                <div class="container__one-half transparent-overlay">
                    <p class="no-margin">Identify controlled substance prescriptions and reconcile those filled elsewhere.</p>
                </div>
                <div class="container__one-half transparent-overlay">
                    <p class="no-margin">View controlled substance and care plan history simultaneously across the continuum.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- End Block 4 -->

    <!-- Block 5 -->
    <div id="modal1" class="modal">
        <a class="close-modal" href="javascript:void(0)">&times;</a>
        <div class="modal-content">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-screenshot--patient-registries.png" alt="MEDITECH Care Coordination tablet screenshot">
        </div>
    </div>
    <div class="container bg--white">
        <div class="container__centered">
            <div class="container__one-third">
                <h2>
                    Keep up with your patient between visits.
                </h2>
                <p>Easily keep track of the patient as he/she moves from one setting to the next, with <a href="https://www.meditech.com/productbriefs/flyers/PatientRegistriesOverview.pdf">actionable event and disease registries</a>. See where they've been and where they're going—whether it's the hospital, a long term care facility, or home—and do all your post-discharge follow ups from just one screen. Staying in touch with patients will help them to avoid unnecessary ER visits/readmissions, while improving your bottom line.</p>

            </div>
            <div class="container__two-thirds">
                <div class="open-modal" data-target="modal1">
                   <div class="tablet--white">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-screenshot--patient-registries.png" alt="MEDITECH Care Coordination tablet screenshot">
                  </div>
                    <div class="mag-bg">
                        <i class="mag-icon fas fa-search-plus"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Block 5 -->

    <!-- Block 6 - Video Block -->
    <div class="content__callout border-none" style="background-color: #E6E9EE !important;">
        <div class="content__callout__media">
            <div class="content__callout__image-wrapper" style="padding:3em !important;">
                <div class="video js__video video--shadow" data-video-id="280779080">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--care-coordination-campaign-patient-portal.jpg" alt="Get your patient on board and engaged video image">
                    </figure>
                    <a class="video__play-btn" href="https://vimeo.com/155984195"></a>
                    <div class="video__container"></div>
                </div>
            </div>
        </div>
        <div class="content__callout__content">
            <div class="content__callout__body">
                <div class="content__callout__body__text">
                    <h2>Get your patient on board and engaged.</h2>
                    <p>The patient is the most important caregiver of all. Keep them satisfied and on the path to wellness by giving them tools that are as useful as the ones their providers have. Enable better self care with our <a href="https://ehr.meditech.com/ehr-solutions/patient-engagement">Patient and Consumer Health Portal</a>, which includes access to test results, discharge instructions, appointment schedules, and online bill pay. In addition, <a href="https://www.meditech.com/productbriefs/flyers/TelehealthApproach.pdf">your patient can track his/her daily activity</a> with our wearable device integration through Validic.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- End Block 6 -->

    <!-- Block 7 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/female-patient-female-doctor-tablet.jpg);">
        <div class="container__centered">
            <div class="container__two-thirds transparent-overlay text--white">
                <h2>
                    Keep your patient safe at home.
                </h2>
                <p>Help your patient to stay healthier in his/her own home. With MEDITECH, your <a href="https://ehr.meditech.com/ehr-solutions/meditech-home-care">home care</a> staff has access to everything they need to assist—a complete Electronic Health Record, shared MPI, streamlined documentation, and comprehensive financial and communication capabilities. Using mobile devices, clinicians can easily access patient records, document data, and tailor care plans on the fly.</p>
            </div>
        </div>
    </div>
    <!-- End Block 7 -->

    <!-- Block 8 -->
    <div class="container bg--white">
        <div class="container__centered">
            <h2>
                Deliver responsive care for end-of-life.
            </h2>
            <p>With MEDITECH's&nbsp;<a href="https://ehr.meditech.com/ehr-solutions/hospice">Hospice</a> solution, you can give your patient comprehensive, personalized end-of-life care according to his/her wishes. Interdisciplinary care plans capture team members' contributions and formulate a unique patient summary. Hospice-specific assessments and charts keep care consistent, while still giving you the flexibility to accommodate individual beneficiary and family needs.</p>
        </div>
    </div>
    <!-- End Block 8 -->

    <!-- Block 9 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/doctor-nurse-patient-darkened.jpg);">
        <div class="container__centered">
            <div class="container__one-third">
                <div class="transparent-overlay">
                    <div class="text--meditech-green" style="text-align:center; padding-bottom:1em;"><i class="far fa-comments fa-3x"></i></div>
                    <div class="text--white">
                        <p>"Most of our providers live 25 miles away in Olympia—but no matter where they are, with MEDITECH they'll be able to check on their admitted patients, which greatly enhances our care coordination model and goals."</p>
                        <p class="text--large no-margin--bottom">
                            Will Callicoat, CFO
                        </p>
                        <p>
                            Summit Pacific Medical Center (Elma, WA)
                        </p>
                    </div>
                </div>
            </div>
            <div class="container__one-third">
                <div class="transparent-overlay">
                    <div class="text--meditech-green" style="text-align:center; padding-bottom:1em;"><i class="far fa-comments fa-3x"></i></div>
                    <div class="text--white">
                        <p>"MEDITECH's integrated EHR will allow our patients to have a seamless transition of care across the continuum since ARH goes from clinics, to inpatient services, and along to home care service lines. That seamless transition of care will make it easier for our patients and registration, and for their information to be consistent all the way across the continuum during that transition."</p>
                        <p class="text--large no-margin--bottom">
                            Ellen Wright, CNO
                        </p>
                        <p>
                            Appalachian Regional Healthcare (Lexington, KY)
                        </p>
                    </div>
                </div>
            </div>
            <div class="container__one-third">
                <div class="transparent-overlay">
                    <div class="text--meditech-green" style="text-align:center; padding-bottom:1em;"><i class="far fa-comments fa-3x"></i></div>
                    <div class="text--white">
                        <p>"By connecting the dots to integrate services such as community physicians and hospitals, we can make sure that patients are going where they need to be. Even though the patient's family helps to drive the care, we need to ensure that an integrated EHR like MEDITECH's is in place for us to direct that care."</p>
                        <p class="text--large no-margin--bottom">
                            Cydney Teal, MD, CMO
                        </p>
                        <p>
                            Union Hospital of Cecil County (Elkton, MD)
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Block 9 -->

</div>
<!-- end js__seo-tool__body-content -->

<!-- Block 10 -->
<div class="container bg--black-coconut">
    <div class="container__centered center">

        <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
        <h2>
            <?php print $cta->field_header_1['und'][0]['value']; ?>
        </h2>
        <?php } ?>

        <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
        <div>
            <?php print $cta->field_long_text_1['und'][0]['value']; ?>
        </div>
        <?php } ?>

        <div class="center" style="margin-top:2em;">
            <?php hubspot_button($cta_code, "Sign Up For The Care Across The Continuum Webinar"); ?>
        </div>

        <div style="margin-top:1em;">
            <?php print $share_link_buttons; ?>
        </div>

    </div>
</div>
<!-- End Block 10 -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>

<!-- END campaign--node-3296.php -->
