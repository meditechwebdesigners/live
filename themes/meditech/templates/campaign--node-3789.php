<!-- START campaign--node-3789.php -->

<?php // This template is set up to control the display of the campaign: Revenue Cycle 2021 update

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');
$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>


<style>
	.quote-box {
		padding: 3em;
		background-color: rgba(3, 3, 30, .5);
		border-left: 5px solid #10BAB3;
		margin-bottom: 2.35765%;
		border-radius: 7px;
		box-shadow: 0px 0px 20px 1px rgba(0, 0, 0, .2);
	}

	.text--small {
		line-height: 1.5;
	}

	.corner-border {
		border-radius: 7px;
	}

	.quote-text {
		width: 100%;
		padding: 1em 3em .5em 3em;
		font-style: italic;
		font-size: 1.4em;
		color: white;
		text-align: left;
	}

	.quote-text p {
		line-height: 1.35em;
	}

	.quote-element {
		padding: 0 2em;
	}

	.quote-person {
		margin-left: 50%;
		padding-top: 1em;
	}

	.quote-person p {
		line-height: 1.35em;
	}

	.contentButtonsContainer p,
	.contentButtonsContainer ul {
		display: inline-block;
	}

	ul.contentButtons {
		margin-left: .5em;
	}

	ul.contentButtons li {
		display: inline-block;
	}

	ul.contentButtons li a.selected {
		font-weight: bold;
	}

	.contentBox {
		padding-right: 1em;
		height: 350px;
		background-size: 80%;
	}

	.contentBox h3 {
		margin-top: 2em;
	}

	ul.contentButtons li a:after {
		content: " | ";
		position: relative;
		padding-left: .5em;
		padding-right: .5em;
		right: 0;
	}

	ul.contentButtons li:last-child a:after {
		content: "";
	}

	.squish-list li {
		margin-bottom: .5em;
	}

	@media all and (max-width: 1150px) {
		.container__one-third.contentButtonsContainer {
			width: 100%;
		}

		.container__two-thirds.contentContainerContent {
			width: 100%;
		}

		.contentBox {
			padding-right: 1em;
			height: 300px;
			background-size: 60%;
		}
	}

	@media all and (max-width: 900px) {
		.gl-container .container__one-fifth {
			width: 100%;
		}
	}

	@media all and (max-width: 660px) {
		ul.contentButtons {
			margin-left: 0;
		}
	}

</style>


<div class="js__seo-tool__body-content">

	<h1 style="display:none;" class="js__seo-tool__title">MEDITECH Revenue Cycle</h1>

	<!-- Block 1 -->
	<div class="container no-pad">
		<div class="gl-container">
			<div class="container__one-half background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Revenue-cycle-block-one-receptionist-at-desk.jpg); min-height:20em; background-position: top;">
			</div>
			<div class="container__one-half gl-text-pad text--white bg--blue-gradient">
				<h2 class="header-one">A strong revenue cycle begins and ends with a satisfied patient</h2>
				<p>The best healthcare is patient-centered. And your organization’s revenue cycle should be too. Your community wants consumer-friendly, accurate billing statements as well as simple online tools to settle their co-payments and balances. With MEDITECH’s integrated Revenue Cycle for Acute, Ambulatory, and Long Term Care, they will get it &mdash; while you get a solid foundation for long term financial health.</p>
				<div class="btn-holder--content__callout">
					<?php hubspot_button($cta_code, ""); ?>
				</div>
			</div>
		</div>
	</div>
	<!-- End of Block 1 -->

	<!-- Block 2 -->
	<div class="container bg--white">
		<div class="container__centered">

			<div id="contentContainer">

				<h2>Consolidate your Revenue Cycle under one roof</h2>
				<p>MEDITECH's Expanse Revenue Cycle was built with front-to-back integration in mind, giving you a more holistic patient story with every claim. And whether patients are visiting the practice or presenting at the ED, MEDITECH ensures that the demographic, insurance, and clinical data needed for timely billing and collections is efficiently captured prior to and/or at the time of service.</p>

				<div class="contentButtonsContainer list-style--none">
					<p>Select an office environment for more details:</p>
					<ul class="contentButtons">
						<li class="b01"><a href="#b01">Patient Access/Front Office </a></li>
						<li class="b02"><a href="#b02">Middle Office</a></li>
						<li class="b03"><a href="#b03">Back Office</a></li>
					</ul>
				</div>

				<div class="container no-pad contentContainerContent">

					<div class="contentBoxCover" style="position:relative;">
						<figure>
							<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/illustration--medical-environments-2021-update.svg" alt="medical office graphic">
						</figure>
						<a href="#b01" class="contentButtonAlso">
							<div style="position:absolute; top:0; left:10%; width:30%; height:40%;">&nbsp;</div>
						</a>
						<a href="#b02" class="contentButtonAlso">
							<div style="position:absolute; top:45%; left:30%; width:30%; height:40%;">&nbsp;</div>
						</a>
						<a href="#b03" class="contentButtonAlso">
							<div style="position:absolute; top:20%; left:60%; width:30%; height:40%;">&nbsp;</div>
						</a>
					</div>

					<div class="contentBox" id="b01">
						<div class="container__one-third">
							<h3>Patient Access/Front Office</h3>
							<ul class="squish-list">
								<li>Scheduling</li>
								<li>Preregistration</li>
								<Li>Contactless check-in</Li>
								<li>Registration</li>
								<li>Insurance verification</li>
								<li>Authorizations</li>
								<li>Patient co-pay collect</li>
								<li>Financial counseling</li>
							</ul>
						</div>
						<div class="container__two-thirds">
							<figure>
								<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/illustration--waiting-room-2021-update.svg" alt="medical waiting room graphic">
							</figure>
						</div>
					</div>

					<div class="contentBox" id="b02">
						<div class="container__one-third">
							<h3>Middle Office</h3>
							<ul class="squish-list">
								<li>Clinical documentation</li>
								<li>Transcription coding</li>
								<li>Case coordination</li>
								<li>Records storage</li>
								<li>Release of information</li>
								<li>Medical records</li>
								<li>Charge capture</li>
							</ul>
						</div>
						<div class="container__two-thirds">
							<figure>
								<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/illustration--doctors-office-2021-update.svg" alt="medical waiting room graphic">
							</figure>
						</div>
					</div>

					<div class="contentBox" id="b03">
						<div class="container__one-third">
							<h3>Back Office</h3>
							<ul class="squish-list">
								<li>Claims checking and error resolution</li>
								<li>Claims submission</li>
								<li>Payment processing and posting</li>
								<li>Denial management</li>
								<li>A/R follow-up and appeals</li>
								<li>Patient statements</li>
								<li>Contract compliance</li>
							</ul>
						</div>
						<div class="container__two-thirds">
							<figure>
								<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/illustration--back-office-2021-update.svg" alt="medical waiting room graphic">
							</figure>
						</div>
					</div>

				</div>

			</div>

			<script language="JavaScript">
				$jq(document).ready(function($jq) {
					var contentBoxes = $jq('#contentContainer .contentBox');
					contentBoxes.css('display', 'none');

					$jq('#contentContainer ul.contentButtons a').click(function() {
						$jq('#contentContainer .contentBoxCover').css('display', 'none');
						contentBoxes.css('display', 'none');
						contentBoxes.filter(this.hash).css('display', 'block');
						$jq('#contentContainer ul.contentButtons li a').removeClass('selected');
						$jq(this).addClass('selected');
						return false;
					});

					$jq('.contentBoxCover a').click(function() {
						$jq('#contentContainer .contentBoxCover').css('display', 'none');
						contentBoxes.css('display', 'none');
						contentBoxes.filter(this.hash).css('display', 'block');
						$jq('#contentContainer ul.contentButtons li a').removeClass('selected');
						var l = $jq(this).attr('href');
						var lClass = l.substring(1);
						$jq('#contentContainer ul.contentButtons li.' + lClass + ' a').addClass('selected');
						return false;
					});
				});

			</script>

		</div>
	</div>
	<!-- End of Block 2 -->

	<!-- Block 3 -->
	<div class="container bg--light-gray">
		<div class="container__centered">

			<h2>A centralized business office at the heart of healthy profit margins</h2>
			<p>Stay financially fit and achieve your revenue goals. End-to-end integration beginning at the first points of patient contact — spanning acute, ambulatory, and long-term-care settings — can minimize lost charges, reduce claim rejections, and improve employee productivity.</p>

			<div class="container" style="padding:2em 0 0;">

				<div class="container__one-half">
					<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Rev-cycle-man-looking-at-tablet-outside-building--block-6.jpg" alt="woman on computer tablet" class="corner-border">
					<h3 class="center">A seamless experience for patients</h3>
					<ul>
						<li>Provide a single, accurate, and timely billing statement combining ambulatory and acute charges.</li>
						<li>Aid customers with a "one call" environment for billing assistance across all care settings.</li>
						<li>Enable patients to actively participate in their own care through a centralized Patient Portal to book appointments, update demographics, view account balances, and pay bills online.</li>
					</ul>
				</div>

				<div class="container__one-half">
					<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Rev-cycle-woman-on-desktop--block-6.jpg" alt="woman on computer tablet" class="corner-border">
					<h3 class="center">A unified workflow for staff</h3>
					<ul>
						<li>Respond to patient or guarantor questions using accounting support in a central, intuitively designed location — no toggling between care settings.</li>
						<li>Use roles-based, prioritized worklists for maximum efficiency.</li>
						<li>Trim A/R days with centralized and decentralized billing, conflict checks, and denial management.</li>
						<li>View consolidated revenue reports to manage the overall financial health of your organization.</li>
					</ul>
				</div>

			</div>

		</div>
	</div>
	<!-- Close Block 3 -->

	<!-- Block 4 -->
	<div class="container">
		<div class="container__centered text--black-coconut">

			<h2>Denial prevention, beginning with first contact</h2>
			<p>Get fully reimbursed for the care you provide. Fight costly denials with embedded, front-end denial prevention workflows, a comprehensive appeals process, and the data to monitor trends.</p>

			<div class="gl-container center" style="background-color: transparent;">
				<div class="container__one-fifth" style="padding:1em;">
					<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon-patient-in-sling--rev-cycle.svg" alt="patient graphic" style="width:50%; max-width:300px;">
					<p>Collect complete and accurate patient and insurance data <em>before</em> services are rendered.</p>
				</div>

				<div class="container__one-fifth" style="padding:1em;">
					<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon-headset--rev-cycle.svg" alt="headset graphic" style="width:50%; max-width:300px;">
					<p>Avoid claim denials. Upon first points of patient contact, verify insurance, receive authorization alerts, and run medical necessity checks.</p>
				</div>

				<div class="container__one-fifth" style="padding:1em;">
					<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon-laptop-with-check-mark--rev-cycle.svg" alt="laptop graphic" style="width:50%; max-width:300px;">
					<p>Pre-screen claims and provide billers with a prioritized, exceptions-based worklist of accounts that could lead to a denial.</p>
				</div>

				<div class="container__one-fifth" style="padding:1em;">
					<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon-list-on-tablet--rev-cycle.svg" alt="tablet graphic" style="width:50%; max-width:300px;">
					<p>Manage and track the appeals process of denials with actionable worklists.</p>
				</div>

				<div class="container__one-fifth" style="padding:1em;">
					<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon-bar-chart--rev-cycle.svg" alt="bar chart graphic" style="width:50%; max-width:300px;">
					<p>Monitor and analyze denial trends, as well as the success rate of your appeals, with interactive denial management reports.</p>
				</div>
			</div>

		</div>
	</div>
	<!-- End of Block 4 -->



	<!-- START UPDATED KLAS (Block 5) -->
	<style>
		.flex-cont {
			display: flex;
			justify-content: space-between;
			align-items: center;
			background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Nurse-Smiling-MEDITECH-KLAS-Awards--bg.jpg);
			min-height: 450px;
			background-position: left;
		}

		.flex-image:nth-child(2) {
			margin: 0 1em;
		}

		@media all and (max-width: 550px) {
			.flex-cont {
				flex-wrap: wrap;
				background-image: none;
				justify-content: center;
			}

			.flex-image:nth-child(2) {
				margin: 0;
			}
		}

	</style>
	<div class="container bg--blue-gradient">
		<div class="container__centered">
			<div class="auto-margins center" style="margin-bottom: 2em;">
				<h2>Expanse earns top marks for Revenue Cycle performance</h2>
				<p>MEDITECH Expanse was ranked #1 in the 2022 Best in KLAS: Software &amp; Services report for Patient Accounting &amp; Patient Management (Community Hospital) for the third straight year.</p>
			</div>

			<div class="container__one-third center">
				<img style="width:300px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-KLAS-Award-2022--Patient-Accounting-and-Patient-Management.png" alt="MEDITECH KLAS Award 2022 - Patient Accounting and Patient Management">
			</div>

			<div class="container__two-thirds quote-box">
				<p class="italic">“We were very proud of our transition onto Expanse Patient Accounting. We were dropping claims a few days after go-live. We were very happy that there was no interruption. I have been through EMR transitions where there was oftentimes an interruption with the revenue cycle, but we did not experience one interruption with our transition to Expanse Patient Accounting.”
				</p>
				<p class="bold no-margin--bottom">— KLAS Research, CIO, February 2022</p>
			</div>
		</div>

		<div class="container__centered">
			<div class="container quote-box" style="width: 100%;">
				<p class="italic">"MEDITECH’s Financial Status Desktop lets users at all levels easily break down and drill into their financial data in many different ways. Following Go-LIVE, it helped our managers proactively monitor their revenue to ensure charges were flowing steadily for their areas. Managers and administration have come to rely on it for monitoring their daily and monthly performance metrics."</p>
				<p class="bold no-margin--bottom">— Carol Huesman, CIO Major Hospital (Shelbyville, IN)
				</p>
			</div>
			<div class="container quote-box" style="width: 100%;">
				<p class="italic">"By empowering patients to directly book their own COVID-19 vaccinations in our MEDITECH patient portal, we’ve saved our scheduling staff countless hours, freed up our phone lines, and provided our patients with the convenience of scheduling a time that works best for them."</p>
				<p class="bold no-margin--bottom">— Clark Averill, Director of IT, St. Luke’s (Duluth, MN)
				</p>
			</div>
		</div>

	</div>
	<!-- END UPDATED KLAS (Block 5) -->


	<!-- Start Block 6 -->
	<div class="container bg--white">
		<div class="container__centered">

			<h2>The lower, the better</h2>
			<div style="padding-bottom:2em;">
				<p>Intuitive workflows with system-wide integration can substantially reduce A/R days to boost your organization's overall fiscal health.</p>
			</div>

			<div class="card__wrapper">

				<div class="container__one-third card">
					<figure>
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Hilo-Medical-Center.jpg">
					</figure>
					<div class="card__info">
						<h3 class="header-four">Hilo Medical Center</h3>
						<p><a href="https://info.meditech.com/white-paper-innovation-in-action">Hilo Medical Center</a> reduced hospital A/R days by 53 percent using MEDITECH's Revenue Cycle solution. The result? A $10 million hard ROI in the inpatient setting.</p>
					</div>
				</div>

				<div class="container__one-third card">
					<figure>
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Summit-Pacific-Medical.jpg">
					</figure>
					<div class="card__info">
						<h3 class="header-four">Summit Pacific Medical Center</h3>
						<p><a href="https://info.meditech.com/summit-pacific-increases-reimbursement-clinic-volumes-with-meditech-analytics-solution-0">Summit Pacific Medical Center</a> achieved 13 fewer A/R days and an 8 percent increase in daily clinic volumes.</p>
					</div>
				</div>

				<div class="container__one-third card">
					<figure>
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Anderson-Medical-Center.jpg">
					</figure>
					<div class="card__info">
						<h3 class="header-four">Anderson Regional Health System</h3>
						<p><a href="https://info.meditech.com/case-study-anderson-regional-cuts-ar-days-by-50-using-meditechs-6.1-revenue-cycle-solution?hsCtaTracking=9cccde46-20ec-42dc-91ce-9a1113f1bee0%7Cd2339c5d-73d2-4f3c-ad82-1408ff9853c8">Anderson Regional Health System</a> saw a 50 percent reduction in A/R days and reduced lost revenue by 90 percent by overhauling its Revenue Cycle process.</p>
					</div>
				</div>

			</div>

		</div>
	</div>
	<!-- End Block 6 -->

	<!-- Block 7 -->
	<!-- Start hidden modal box -->
	<div id="modal1" class="modal">
		<a class="close-modal" href="javascript:void(0)">&times;</a>
		<div class="modal-content">
			<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Revenue-Cycle-screenshot--Large.jpg">
		</div>
	</div>
	<!-- End hidden modal box -->

	<div class="container bg--light bg--light-gray">
		<div class="container__centered">
			<div class="container__one-half">
				<h2>Realize big ideas with big data</h2>
				<div>
					<p>Your data is the key. Unlock its power with Revenue Cycle solutions that help you gain insight through data analytics and make the most informed decisions for your organization.</p>
					<ul>
						<li>Use <a href="https://ehr.meditech.com/ehr-solutions/meditech-business-clinical-analytics">Business and Clinical Analytics (BCA)</a> — a web-based visualization tool — to increase efficiency, measure progress, and improve performance.</li>
						<li>Arm your C-suite with analytics dashboards to drill down into the Revenue Cycle. Or, view combined Revenue Cycle data with clinical and operational insights for a broader perspective.</li>
						<li>Inform your value-based reimbursement strategy with a <a href="https://ehr.meditech.com/ehr-solutions/meditech-population-health">holistic view of your patient population</a>.</li>
					</ul>
				</div>
			</div>
			<div class="container__one-half">
				<!-- Start modal trigger -->
				<div class="open-modal" data-target="modal1">
					<div class="tablet--white">
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Revenue-Cycle-screenshot--Small.jpg">
					</div><!-- Add modal trigger here -->
					<div class="mag-bg">
						<!-- Include if using image trigger -->
						<i class="mag-icon fas fa-search-plus"></i>
					</div>
				</div>
				<!-- End modal trigger -->
			</div>
		</div>
	</div>
	<!-- End of Block 7 -->

</div>
<!-- end js__seo-tool__body-content -->

<!-- LAST Block -->
<div class="container bg--blue-gradient" style="padding:5.5em;">
	<div class="container__centered center text--white">

		<?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
		<h2>
			<?php print $cta->field_header_1['und'][0]['value']; ?>
		</h2>
		<?php } ?>

		<?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
		<div>
			<?php print $cta->field_long_text_1['und'][0]['value']; ?>
		</div>
		<?php } ?>

		<div class="center" style="margin-top:2em;">
			<?php hubspot_button($cta_code, "View Enrollment Details"); ?>
		</div>

		<div style="margin-top:1em;">
			<?php print $share_link_buttons; ?>
		</div>

	</div>
</div>
<!-- End of LAST Block -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>


<!-- END campaign--node-3789.php -->
