<?php // This template is for each row of the Views block: CASE STUDIES 3-COLUMN ....................... ?>
<!-- start views-view-fields--case-studies-3-column--block.tpl.php template -->
<?php    
// get node ID...
$nid = $fields['nid']->content;
$node = node_load($nid);

// display state if there is one...
$topicTerms = field_view_field('node', $node, 'field_case_study_topics'); 
if(!empty($topicTerms)){
  foreach($topicTerms["#items"] as $topicTerm){
    $topicAbbr = $topicTerm["taxonomy_term"]->description;
    $topicTerm = $topicTerm["taxonomy_term"]->name;
  }
}
else{
  $topicAbbr = '';
  $topicTerm = '';
}
?>
<div class="container__thirds card <?php print strip_tags(trim($topicAbbr)); ?>" style="border-width:4px; border-style:solid; padding-bottom:0;">
   <div class="container__content">
    <div class="topic-text"><?php print $topicTerm; ?></div>
    <p class="bold" style="margin-bottom:0; line-height:1.15em;"><?php print $fields['title']->content; ?></p>
    <p style="font-size:.8em; margin:0 0 .6em 0; font-style:italic;">Published on <?php print $fields['published_at']->content; ?>
    <?php // add Edit Video link...
      if( user_is_logged_in() ){ 
        print ' <span style="font-size:12px;">'; print l( t('Edit This'),'node/'. $fields['nid']->content .'/edit' ); print "</span>"; 
      } 
    ?></p>
    <a class="btn--orange cta_link_case_studies_page_gae" href="<?php print $fields['field_text_1']->content; ?>" style="margin:1em;">Learn more</a>
  </div>
</div>
<!-- end views-view-fields--case-studies-3-column--block.tpl.php template -->