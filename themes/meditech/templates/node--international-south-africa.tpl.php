<?php 
// This template is set up to control the display of the 'INTERNATIONAL - SOUTH AFRICA' content type 
$url = $GLOBALS['base_url']; // grabs the site url
$node_id = $node->nid;
?>
<!-- start node--international-south-africa.tpl.php template -->

<?php
// set region variables for South Africa...
$home_page = 'meditech-south-africa';
$international_region = 'South Africa';
$side_nav = 'menu-side-menu---south-africa';
$country_graphic = 'south-africa/south-africa-graphic.png';
$sidebar_class = 'south_africa_sidebar_gae';
$customer_link = 'https://meditechinternational.atlassian.net/servicedesk/customer';

// set hero variables based on node id...
// ignore if page does not use the hero...
switch($node_id){
  case 1926: // MEDITECH South Africa home page
    $hero = 'yes-main';
    $hero_background_image = 'south-africa/johannesburg-south-africa-skyline-nelson-mandela-bridge.jpg'; 
    $text_class = '';
    break;
  case 1933: // MEDITECH South Africa B-BBEE page
    $hero = 'yes';
    $hero_background_image = 'south-africa/group-of-diverse-people-against-bright-window.jpg';
    $text_class = 'text--white text-shadow--black';
    break;
  case 1928: // MEDITECH South Africa Community page
    $hero = 'yes';
    $hero_background_image = 'south-africa/johannesburg-south-africa-skyline.jpg';
    $text_class = 'text--white text-shadow--black';
    break; 
  case 1932: // MEDITECH South Africa Awards page
    $hero = 'yes';
    $hero_background_image = 'south-africa/applauding-hands.jpg';
    $text_class = 'text--white';
    break; 
}
?>

<?php if( isset($hero) && $hero == 'yes-main' ){ // if hero belongs on page AND this is a "main" page, display the following... ?>
<!-- HERO section -->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/international/<?php print $hero_background_image; ?>); min-height:20em;">
  <div class="container__centered">
    <div class="container__one-half">&nbsp;</div>
    <div class="container__one-half container__centered" style="position:relative;">
      <style>
        .anniversary-logo {
          position: absolute;
          right: 50px;
          width: 165px;
          height: 300px;
          background-color: RGBA(255, 255, 255, 0.8);
          z-index: 2;
          top: -3em;
        }

        .anniversary-logo img {
          height: auto;
          padding: 1em;
          position: relative;
          top: 5.5em;
        }

        @media (max-width: 800px) {
          .anniversary-logo {
            position: absolute;
            right: 50px;
            width: 165px;
            height: 200px;
            background-color: white;
            z-index: 2;
            top: -4.75em;
          }

          .anniversary-logo img {
            height: auto;
            padding: 1em;
            position: relative;
            top: 0;
          }
        }

      </style>
      <div class="anniversary-logo">
        <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/international/south-africa/MEDITECH-South-Africa-40th-logo.png" width="400" height="497" alt="MEDITECH South Africa: 40 years supporting healthcare and improving outcomes">
      </div>
    </div>
  </div>
</div>
<!-- End of HERO section -->
<?php } ?>


<?php if( isset($hero) && $hero == 'yes' ){ // if hero belongs on page AND this is NOT a "main" page, display the following... ?>
<!-- HERO section -->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/international/<?php print $hero_background_image; ?>);">
  <div class="container__centered">
    <h1 class="<?php print $text_class; ?>"><?php print $title; ?></h1>
  </div>
</div>
<!-- End of HERO section -->
<?php } ?>

<section class="container__centered">

  <div class="container__two-thirds">

    <?php if( isset($hero) && $hero == 'yes-main' ){  ?>
    <h1><?php print $title; ?></h1>
    <?php } ?>

    <?php
      // if image exists for node, then proceed to render DIV...
      if(!empty($content['field_main_image_international'])){
      ?>
    <figure class="news__article__img">
      <?php print render($content['field_main_image_international']); ?>
    </figure>
    <?php
      }
      ?>

    <?php if( !isset($hero) ){ // if no hero on page, display title here... ?>
    <h1 class="page__title js__seo-tool__title"><?php print $title; ?></h1>
    <?php } ?>


    <div class="js__seo-tool__body-content">

      <?php print render($content['field_body']); // main content if any ?>

      <?php
        // specific content for specific pages ======================================================================
        switch($node_id){
            
          case 1934: // SOUTH AFRICA EXECUTIVES page
            print '<div class="container no-pad">';
            print views_embed_view('executives_south_africa', 'block'); // adds 'Executives - South Africa' Views block...
            print '</div>';
            break;
            
          case 1931: // SOUTH AFRICA DIRECTIONS page ======================================================================
            print views_embed_view('locations_south_africa', 'block'); // adds 'Locations - South Africa' Views block... 
            break;
            
          case 1927: // SOUTH AFRICA NEWS page ======================================================================
            print views_embed_view('news_south_africa', 'block'); // adds 'News - South Africa' Views block... 
            break;
            
          case 1929: // SOUTH AFRICA CONTACT page ========================================================================== 
        ?>
      <h2 class="page__title">Are you an existing customer?</h2>
      <p>If you're a customer in need of assistance, please log in to our <a href="https://meditechinternational.atlassian.net/servicedesk/customer/portals" target="_blank">Customer Service area</a> and enter a task, or call us anytime at <a class="phone-number" href="tel:+27-11-805-1631">+27-11-805-1631</a>.</p>

      <address class="container no-pad">
        <div class="container__one-third">
          <h3><i class="fas fa-map-marker-alt meditech-green"></i> Address</h3>
          <p>MEDITECH South Africa<br />
            Thandanani Office Park<br />
            Cnr Invicta Ave & Matuka Close<br />
            Midrand<br />
            South Africa</p>
        </div>
        <div class="container__one-third">
          <h3><i class="fas fa-phone meditech-green"></i> Call</h3>
          <p><a class="phone-number" href="tel:+27-11-805-1631">+27-11-805-1631</a></p>
        </div>
        <div class="container__one-third">
          <h3><i class="fas fa-fax meditech-green"></i> Fax</h3>
          <p>+27-11-805-1624</p>
        </div>
      </address>

      <address class="container no-pad">
        <div class="container__one-third">
          <h3><a href="https://www.facebook.com/MEDITECH-SA-317306641687653" target="_blank"><i class="fab fa-facebook-square meditech-green"></i> Facebook</a></h3>
        </div>
        <div class="container__one-third">
          <h3><a href="https://twitter.com/MEDITECHSA" target="_blank"><i class="fab fa-twitter-square meditech-green"></i> Twitter</a></h3>
        </div>
        <div class="container__one-third">
          <h3><a href="https://za.linkedin.com/in/meditech-south-africa-a5340453" target="_blank"><i class="fab fa-linkedin meditech-green"></i> LinkedIn</a></h3>
        </div>
      </address>

      <!-- CONTACT FORM =============================== -->
      <h3><i class="fas fa-envelope meditech-green"></i>&nbsp; Send us a message</h3>

      <?php
              $contactForm = module_invoke('webform', 'block_view', 'client-block-1910');
              print render($contactForm['content']);
            ?>
      <!-- End of Contact Form -->
      <?php
            break;
            
          case 2033: // SOUTH AFRICA EMPLOYEE CONTACT page ========================================================================== 
            if( isset($_GET['employee']) && isset($_GET['send_to']) && $_GET['employee'] != '' && $_GET['send_to'] != '' ){
              $employee = $_GET['employee'];
              ?>
      <!-- CONTACT FORM =============================== -->
      <h3><i class="fas fa-envelope meditech-green"></i>&nbsp; Send <?php print $employee; ?> a message</h3>/
      <?php
                $contactForm = module_invoke('webform', 'block_view', 'client-block-2032');
                print render($contactForm['content']);
              ?>
      <!-- End of Contact Form -->
      <?php
            }
            else{
            ?>

      <p class="page__title">We're sorry but there is a problem with this form or the link you clicked. Please try contacting us through <a href="https://ehr.meditech.com/global/meditech-south-africa/contact-meditech-south-africa">our main contact form</a>.</p>

      <?php
            }

            break;
            
        } // END node ID switch
        ?>


    </div><!-- End .js__seo-tool__body-content -->
    <?php // SEO tool for internal use...
        if(node_access('update',$node)){
          print '<!-- SEO Tool is added to this div -->';
          print '<div class="container no-pad--top js__seo-tool"></div>';
        } 
      ?>

  </div>

  <!-- RIGHT SIDE NAV MENU -->

  <style>
    .sidebar__nav ul.menu ul.menu {
      padding-left: 1.5em;
      margin-top: 0;
    }

    .sidebar__nav ul.menu ul.menu li {
      padding: .4em 0 .5em 0;
      font-size: .9em;
    }

    .sidebar__nav ul.menu ul.menu li.last {
      padding-bottom: 0;
    }

    @media (max-width: 800px) {
      .sidebar__nav ul.menu ul.menu li {
        font-size: 1em;
      }
    }

  </style>


  <aside class="container__one-third">

    <div class="panel">

      <?php if($node_id != 1926){ // if not MEDITECH South Africa home page, display menu header... ?>
      <div class="center">
          <h3 style="font-size:1.25em;"><a href="<?php print $url; ?>/global/<?php print $home_page; ?>" title="Go to the <?php print $international_region; ?> home page">MEDITECH <?php print $international_region; ?></a></h3>
      </div>
      <?php } ?>

      <div class="sidebar__nav <?php print $sidebar_class; ?>" style="float:left; width:100%;">
        <?php
          // adds appropriate international menu block...
          $international_menu = module_invoke('menu', 'block_view', $side_nav);
          print render($international_menu['content']); 
        ?>
      </div>

      <p style="text-align:center;"><a href="<?php print $customer_link; ?>" target="_blank" class="btn--orange south_africa_customers_gae">MEDITECH <?php print $international_region; ?><br />Customer Service</a></p>

      <?php if( !empty( views_get_view_result('next_big_event_sa', 'block') ) || !empty( views_get_view_result('upcoming_south_africa_trade_shows', 'block') ) ){ ?>
      <div class="panel <?php print $sidebar_class; ?>">

        <?php print views_embed_view('next_big_event_sa', 'block'); // adds 'Next Big US Event - International' Views block... ?>

        <?php if( !empty( views_get_view_result('upcoming_south_africa_trade_shows', 'block') ) ){ ?>
        <h3>Upcoming Trade Shows & Conferences</h3>
        <?php print views_embed_view('upcoming_south_africa_trade_shows', 'block'); // adds 'Upcoming UK IRE Trade Shows' Views block... ?>
        <?php } ?>

      </div>
      <?php } ?>

    </div>

    <?php if($node_id != 1926){ // if not MEDITECH South Africa home page, display menu header... ?>
    <div class="center">
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/international/south-africa/MEDITECH-South-Africa-40th-logo.png" style="width: 60%;">
    </div>
    <?php } ?>

  </aside>


</section>

<!-- end node--international-south-africa.tpl.php template -->
