<?php  // This template is for each row of the Views block: EVENTS - NEXT BIG EVENT ....................... 

  $url = $GLOBALS['base_url']; // grabs the site url
  // get node ID...
  $nid = $fields['nid']->content;
  $node = node_load($nid);
?>  
<!-- start views-view-fields--events-next-big-event--block.tpl.php template -->
<div class="content__callout" style="min-height:500px;">
  <?php 
    if( !empty($fields['field_event_image']->content) ){      
      $imageData = field_get_items('node', $node, 'field_event_image');
      $imageURI = $imageData[0]['uri'];
      $imageFileURL = explode('/', $imageURI);
      $imageFileName = $imageFileURL[4];
    }
  ?>        
  <div class="content__callout__media content__callout__bg__img--green" style="background-image: url(<?php print $url.'/sites/default/files/images/events/'.$imageFileName; ?>);">
  </div>

  <div class="content__callout__content--green">
      <h2><?php print $fields['title']->content; ?></h2>
      <h3>
        <?php 
          print $fields['field_location_city']->content;
          if($fields['field_location_state']->content){
            $stateTerms = field_view_field('node', $node, 'field_location_state'); 
            if(!empty($stateTerms)){
              foreach($stateTerms["#items"] as $sTerm){
                $eventState = ', '.$sTerm["taxonomy_term"]->description;
              }
            }
            else{
              $eventState = '';
            }
            print $eventState; 
          }
          if($fields['field_location_country']->content){
            if($fields['field_location_country']->content != 'United States' && $fields['field_location_country']->content != 'Canada'){
              print ', '.$fields['field_location_country']->content;
            }
          }
          if($fields['field_location_city']->content == '' && $fields['field_location_state']->content == '' && $fields['field_location_country']->content == ''){
            print " | ";
          }
        ?>
<br>
      <?php 
        $eventDate = field_get_items('node', $node, 'field_event_date');

        // get first date data...
        $date1 = date_create($eventDate[0]['value']);
        $day1 = date_format($date1, 'jS');
        $month1 = date_format($date1, 'F');

        // get second date data...
        $date2 = date_create($eventDate[0]['value2']);
        $day2 = date_format($date2, 'jS');
        $month2 = date_format($date2, 'F');

        print $month1.' '.$day1;
        // if start date and end date are not in the same month...
        if($month1 != $month2){
            print " - ".$month2.' '.$day2;
        }
        // else if dates are in the same month...
        elseif($month1 == $month2 && $day1 != $day2){
            print " - ".$day2;
        }
        // else one day event...
        else{
        }
      ?>
      </h3>
    <?php print $fields['field_summary']->content; ?>

    <div class="btn-holder--content__callout">
      <?php 
      // for annual HIMSS page...
      // create regular event page with image, date, location, etc.
      // change button URL to main HIMSS page...
      /*
      if($nid == 2242){
        $url_path = 'https://ehr.meditech.com/events/meditech-at-himss18';
      }
      else{
        $url_path = $fields['path']->content;
      }
      */
      $url_path = $fields['path']->content;
      ?>
      <a class="btn--orange events_hero_link_gae" href="<?php print $url_path; ?>">Read More</a>
    </div>

  </div>
</div>
<!-- end views-view-fields--events-next-big-event--block.tpl.php template -->