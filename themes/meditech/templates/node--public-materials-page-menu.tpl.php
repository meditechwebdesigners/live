<?php // This template is set up to control the display of the PUBLIC MATERIALS PAGE WITH MENU content type

  $url = $GLOBALS['base_url']; // grabs the site url

?>
<!-- start node--public-materials-page-menu.tpl.php template -->
 
<?php
$node_id = $node->nid;
include('public-materials-page-menu--node-'.$node_id.'.php');
?>


<script>
  // wait for page to load...
  window.addEventListener('load', function(){
    // check URL of page to see if it qualifies...
    var currentURL = window.location.href;
    if( currentURL.indexOf('?mtid=') > -1 ){
      // get URL parameter...
      var param = currentURL.indexOf('?mtid=');
      var id = currentURL.substr(param + 6, 4);
      // get all links on page...
      var pageLinks = document.querySelectorAll('a');
      pageLinks.forEach(function(l){
        // add URL parameter to all links...
        var href = l.getAttribute('href');
        l.setAttribute('href', href + '?mtid=' + id);
      });
    }
  });
</script>        
<!-- end node--public-materials-page-menu.tpl.php template -->


<?php 
/*
https://www.drupal.org/docs/7/theming/template-theme-hook-suggestions

Because Drupal limits how templates can be named and creating "theme suggestions" require the use of complex preprocess functions,
I came up with this include procedure that allows us to name our templates using the content type and a node ID.

Technically you could name them anything and add a node ID but that wouldn't be very organized.

The possibility of using this base template as a wrapper for each of the included templates is also possible. 
We could have something appear above and below each of these nodes without having to use the page.tpl.php.
*/
?>