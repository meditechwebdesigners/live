<!-- start simple-page--node-1086.tpl.php template -- MEDIA COMMUNICATIONS PAGE -->

<!-- Hero -->
<div class="content__callout">
  <div class="content__callout__media content__callout__bg__img--green" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/MEDITECH-Communications-Team_hero.jpg);">
    <!-- Title -->
    <div class="content__callout__title-wrapper--green">
      <h1 class="content__callout__title js__seo-tool__title">MEDITECH Communications Team</h1>
    </div>
  </div>
  <div class="content__callout__content--green js__seo-tool__body-content">
    <p>You can find helpful background information in our <a href="https://home.meditech.com/en/d/mktcontent/otherfiles/meditechhighlights.pdf" title="corporate infographic">Corporate Infographic</a>, while our <a href="<?php print $url; ?>/news" title="MEDITECH News">News page</a> contains the latest updates on MEDITECH, our solutions, and customers. Be sure to also check our <a href="https://ehr.meditech.com/podcast-index">Podcast channel</a>. </p>
  </div>
</div>
<!-- Hero -->

<section class="container__centered">
  <div class="container__two-thirds">

    <div class="container no-pad--bottom">

      <div class="container no-pad js__seo-tool__body-content">

        <!-- Jason Patnode -->
        <figure class="info__card">
          <div class="info__card__media__img">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/jpatnode.png">
          </div>
          <figcaption class="center">
            <h3 class="header-five no-margin">Jason Patnode</h3>
            <p>Manager of Content, Branding and Digital Marketing, Public Relations<br>
              <a href="https://twitter.com/jpatnode" target="_blank" title="@JPatnode">@JPatnode</a>
            </p>
          </figcaption>
        </figure>

        <!-- Christina Noel -->
        <figure class="info__card">
          <div class="info__card__media__img">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/ChristinaNoel.png">
          </div>
          <figcaption class="center">
            <h3 class="header-five no-margin">Christina Noel</h3>
            <p>Social Media Coordinator<br>
              <a href="https://twitter.com/Christina_Noel" target="_blank" title="@Christina_Noel">@Christina_Noel</a>
            </p>
          </figcaption>
        </figure>

        <!-- Robin Montville -->
        <figure class="info__card">
          <div class="info__card__media__img">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/RobinMontville.png">
          </div>
          <figcaption class="center">
            <h3 class="header-five no-margin">Robin Montville</h3>
            <p>Public and Media Relations Manager<br>
              <a href="https://twitter.com/MontvilleRobin" target="_blank" title="@MontvilleRobin">@MontvilleRobin</a>
            </p>
          </figcaption>
        </figure>

        <!-- Rose McCarthy -->
        <figure class="info__card">
          <div class="info__card__media__img">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/RoseMcCarthy.png">
          </div>
          <figcaption class="center">
            <h3 class="header-five no-margin">Rose McCarthy</h3>
            <p>Public and Media Relations Manager<br>
              <a href="https://twitter.com/MeditechRose" target="_blank" title="@MeditechRose">@MeditechRose</a>
            </p>
          </figcaption>
        </figure>

      </div><!-- END container -->

      <!-- CONTACT FORM -->

      <h3><i class="fas fa-envelope meditech-green"></i>&nbsp; Send us a message</h3>

      <?php
        $formContent = module_invoke('webform', 'block_view', 'client-block-1087');
        print render($formContent['content']);
      ?>
    </div>

  </div><!-- END container__two-thirds -->


  <!-- SIDEBAR -->
  <aside class="container__one-third panel communications_sidebar_gae">

    <div class="sidebar__nav js__seo-tool__body-content">
      <ul class="menu">
        <li class=""><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/meditechhighlights.pdf" title="MEDITECH corporate infographic">Corporate Infographic</a></li>
        <li class=""><a href="<?php print $url; ?>/news-tags/press-releases">MEDITECH Press Releases</a></li>
        <li class=""><a href="<?php print $url; ?>/news">MEDITECH News</a></li>
      </ul>
      <br>
      <p class="bold">Boilerplate</p>
      <p>MEDITECH empowers healthcare organizations around the world to expand their vision of what’s possible with <a href="https://vimeo.com/meditechehr/review/502762364/b34c70dfcd">Expanse</a>, the intuitive EHR setting new standards for usability, efficiency, and personalization. Our agile innovation is advancing the productivity of busy clinicians in 23 countries, driving positive clinical outcomes and patient experiences. MEDITECH solutions span every care setting, from acute centers and ambulatory practices, to home health and hospice, long-term care and behavioral health facilities, outpatient services, patients’ homes and beyond. Expand your possibilities. See why KLAS rates MEDITECH Expanse the #1 EHR in three categories and the #2 Overall Software Suite. Visit ehr.meditech.com, find MEDITECH Podcasts on your favorite platform, and follow us on <a href="https://twitter.com/MEDITECH">Twitter</a>, <a href="https://www.facebook.com/MeditechEHR?ref=br_tf">Facebook</a>, and <a href="https://www.linkedin.com/company/meditech">LinkedIn</a>.</p>
      <p><em>*Intended for use by media only, unless given permission otherwise.</em></p>
    </div>

    <a class="twitter-timeline" data-lang="en" data-width="360" data-height="500" data-dnt="true" data-theme="light" data-link-color="#ff610a" href="https://twitter.com/MEDITECH">Tweets by MEDITECH</a>
    <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

  </aside>

</section>

<!-- end simple-page--node-1086.tpl.php -->
