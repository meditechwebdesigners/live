<?php
// This template is set up to control the display of the 'MPS page' content type
  $url = $GLOBALS['base_url']; // grabs the site url

$button_1 = field_collection_data($node, 'field_fc_button_1');
?>

<!-- start node--mps-page.tpl.php template -->

<section class="container__centered">

    <div class="container__two-thirds">
        <div class="js__seo-tool__body-content">

            <h1 class="page__title js__seo-tool__title">
                <?php print $title; ?>
            </h1>

            <figure class="news__article__img">
                <?php // break down to find image file name and alt tag to recreate HTML without width and height attributes...
                $image = $content['field_image']; // add image 
                $image_filename = $image[0]['#item']['filename'];
                $image_alt = $image[0]['#item']['alt'];
                print '<img src="'.$url.'/sites/default/files/images/mps/'.$image_filename.'" alt="'.$image_alt.'"/>';
                ?>
            </figure>

            <?php print render($content['field_body']); // *** MAIN CONTENT *** ?>


        </div>
        <!-- End .js__seo-tool__body-content -->

        <?php // SEO tool for internal use...
        if(node_access('update',$node)){
          print '<!-- SEO Tool is added to this div -->';
          print '<div class="container no-pad--top js__seo-tool"></div>';
        } 
      ?>
    </div>

    <aside>
        <div class="container__one-third no-pad">


            <?php 
           // use same function as in campaigns to pull field collection data where they have unlimited quantity...
           $toolkit_icons = multi_field_collection_data_unrestricted($node, 'field__fc_toolkit_icons_unl'); 
           ?>

            <?php if( !empty($toolkit_icons[0]) ){ // check to see if field collection is empty before creating HTML ?>
            <dl style="list-style-type:none; display:flex; flex-wrap:wrap; align-items:center; justify-content:flex-start;">
                <?php 
             // count how many instances of the field collection there are...
             $num = count($toolkit_icons);
             // loop through each instance...
             for($ti=0; $ti<$num; $ti++){
               // grab the taxonomy data to know what icon to use...
               $taxonomy_term = $toolkit_icons[$ti]->field_landing_page_type['und'][0]['taxonomy_term'];
               // grab the actual term they chose from that taxonomy...
               $term_name = $taxonomy_term->name;
               
               // generate different HTML based on which term they chose...
               switch($term_name){
                 case 'Case Study':
                    $icon_html = '<dt style="flex-basis: 20%; margin-bottom: 1em;"><img alt="Case Study Icon" src="https://ehr.meditech.com/sites/all/themes/meditech/images/ehr-solutions/icon--case-study.png"></dt><dd style="flex-basis: 80%; margin-bottom: 1em;">';
                    break;
                 case 'Video':
                    $icon_html = '<dt style="flex-basis: 20%; margin-bottom: 1em;"><img alt="Video Icon" src="https://ehr.meditech.com/sites/all/themes/meditech/images/ehr-solutions/icon--video.png"></dt><dd style="flex-basis: 80%; margin-bottom: 1em;">'; 
                    break;
                 case 'Podcast':
                    $icon_html = '<dt style="flex-basis: 20%; margin-bottom: 1em;"><img alt="Podcast Icon" src="https://ehr.meditech.com/sites/all/themes/meditech/images/ehr-solutions/icon--podcast.png"></dt><dd style="flex-basis: 80%; margin-bottom: 1em;">'; 
                    break;
                 case 'Blog':
                    $icon_html = '<dt style="flex-basis: 20%; margin-bottom: 1em;"><img alt="Blog Icon" src="https://ehr.meditech.com/sites/all/themes/meditech/images/ehr-solutions/icon--blog.png"></dt><dd style="flex-basis: 80%; margin-bottom: 1em;">'; 
                    break;
                 case 'Live Webinar':
                 case 'On Demand Webinar':
                    $icon_html = '<dt style="flex-basis: 20%; margin-bottom: 1em;"><img alt="Webinar Icon" src="https://ehr.meditech.com/sites/all/themes/meditech/images/ehr-solutions/icon--webinar.png"></dt><dd style="flex-basis: 80%; margin-bottom: 1em;">'; 
                    break;
                 default:
                    $icon_html = '<dt style="flex-basis: 20%; margin-bottom: 1em;"><img alt="Case Study Icon" src="https://ehr.meditech.com/sites/all/themes/meditech/images/ehr-solutions/icon--case-study.png"></dt><dd style="flex-basis: 80%; margin-bottom: 1em;">';
                    break;
               }
               print $icon_html;
               
               // generate text link...
               print '<a href="'.$toolkit_icons[$ti]->field_link_url_1['und'][0]['value'].'">'.$toolkit_icons[$ti]->field_link_text_1['und'][0]['value'].'</a>';
               // close the second DD tag...
               print '</dd>';
             } // end of loop 
             ?>
            </dl>
            <?php } // if field collection is empty, do nothing ?>
        </div>
    </aside>

    <aside class="container__one-third panel">
        <div class="sidebar__nav">
            <ul class="menu">
              <li class="first"><a href="<?php print $url; ?>/meditech-professional-services">MEDITECH Professional Services</a></li>
                <li><a href="<?php print $url; ?>/mps/implementation">Implementation</a></li>
                <li><a href="<?php print $url; ?>/mps/ehr-toolkits-and-quality">EHR Toolkits and Quality</a></li>
                <li><a href="<?php print $url; ?>/mps/physician-advisory-services">Physician Advisory Services</a></li>
                <li><a href="<?php print $url; ?>/mps/business-and-clinical-analytics-and-data-repository">Business and Clinical Analytics and<br>
                Data Repository</a></li>
                <li class="last"><a href="<?php print $url; ?>/mps/interoperability">Interoperability</a></li>
            </ul>
        </div>
        <div class="sidebar__nav">
            <div class="center">
                <a href="https://home.meditech.com/webforms/contact.asp?rcpt=professionalservices|meditech|com&rname=professionalservices" class="btn--orange">Contact MEDITECH Professional Services</a>
            </div>
        </div>
    </aside>


</section>

<!-- end node--mps-page.tpl.php template -->