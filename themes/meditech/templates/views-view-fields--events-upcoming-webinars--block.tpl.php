<?php // This template is for each row of the Views block:  UPCOMING WEBINARS ....................... 

// get node ID...
$nid = $fields['nid']->content;
$node = node_load($nid);

// get_webinar_image function is in the template.php file
?>
<!-- start views-view-fields--events-upcoming-webinars--block.tpl.php template -->
<figure class="container no-pad">
  <div class="container__one-third">
    <?php $webinar_image = get_webinar_image($node); ?>
    <div class="square-img-cropper <?php print $webinar_image['crop']; ?>">
      <img src="<?php print $webinar_image['url']; ?>" alt="webinar thumbnail">
    </div>
  </div>
  <figcaption class="container__two-thirds">
  
    <h3 class="header-four no-margin"><a class="webinars_live_link_gae" href="<?php print $fields['field_text_1']->content; ?>"><?php print $fields['title']->content; ?></a></h3>
    
    <?php // temp fix...
    if($nid == '3369'){
      print '<h5 class="no-margin--bottom">Date and Time TBD</h5>';
    }
    else{
    ?>
      <h5 class="no-margin--bottom"><?php print $fields['field_date_and_time']->content.', '.$fields['field_date_and_time_1']->content; ?> (Eastern) 
      <?php 
      if( !empty($fields['field_duration']->content) ){ 
        print ' | '.$fields['field_duration']->content; 
      }
      ?></h5>
    <?php 
    } 
    ?>
      
    <p><?php print $fields['field_summary']->content; ?></p>
  </figcaption>
</figure>

<?php 
if( user_is_logged_in() ){ 
  print '<p style="text-align:right; font-size:12px;"><a href="https://ehr.meditech.com/node/'.$nid.'/edit">Edit this content</a></p>';
}
?>
<hr>
<!-- end views-view-fields--events-upcoming-webinars--block.tpl.php template -->