<!-- start simple-page--node-4188.tpl.php template -- New Content PAGE -->
<div class="container container__centered">
  <h1><?php print $title; ?></h1>

  <div>
    <div class="container__two-thirds">
      <?php print views_embed_view('new_or_updated_content', 'block_1'); // adds Views block... ?>
    </div>
    <!-- END container__two-thirds -->

    <?php include('inc-news-sidebar.php'); ?>
  </div>
</div>
<!-- END simple-page--node-4188.tpl.php template -->    