<!-- start views-view-fields--news-asia-pacific--block.tpl.php template -->
<?php 
// This template is for each row of the Views block: NEWS-ASIA-PACIFIC ....................... 

// get node ID...
$nid = $fields['nid']->content;
$node = node_load($nid);
// get node URL from node ID...
$nodeURL = url('node/'. $nid); 

?>
<div>

  <h3><a class="news_main_link_gae" href="<?php print $nodeURL; ?>"><?php print $fields['title']->content; ?></a></h3>
  <div class="inline__text__wrapper">
    <p><span class="snippet__card__text--callout"><time datetime="<?php print date('Y-m-d', $node->published_at); ?>" itemprop="datePublished"><?php print date("F j, Y", $node->published_at); ?></time></span>&nbsp;&mdash;&nbsp;</p>
    <?php
    $summary = field_view_field('node', $node, 'field_summary');
    print render($summary); 
    ?>
  </div>

  <ul class="news__article__filters tag_link_news_main_gae">
    <?php generate_news_tag_links($node); ?>
  </ul>
  
  <hr>

</div>
<!-- end views-view-fields--news-asia-pacific--block.tpl.php template -->