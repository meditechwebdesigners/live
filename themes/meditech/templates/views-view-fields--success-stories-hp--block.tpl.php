<!-- start  views-view-fields--success-stories-hp--block.tpl.php template -->
<div class="container__one-third card">
  <figure>
        <?php
        if( !empty($fields['field_image']->content) ){ 
          // get image data...
          $image_data = $fields['field_image']->content;
          // example data result: <img typeof="foaf:Image" src="https://at_drudev.meditech.com/drupal/sites/default/files/images/campaigns/<FILENAME>.jpg" width="800" height="250" alt="" />
          $image_HTML_array = explode('/', $image_data);
          $image_HTML_ending = $image_HTML_array[8];
          $first_quotes = strpos($image_HTML_ending, '"');
          $image_filename = substr($image_HTML_ending, 0, $first_quotes);
          if( strpos($image_HTML_ending, 'alt=') > -1 ){
            $alt_pos = strpos($image_HTML_ending, 'alt=');
            $alt_text = substr($image_HTML_ending, $alt_pos, -1);
          }
          else{
            $alt_text = 'alt=""';
          }
        }  
        ?>
        <img src="https://ehr.meditech.com/sites/default/files/images/home/<?php print $image_filename.'" '.$alt_text; ?>>
  </figure>
  <div class="card__info">
    <p><a href="<?php print $fields['field_link_url_1']->content; ?>" class="success-story-gae"><?php print $fields['title']->content; ?></a></p>
  </div>
</div>
<!-- end  views-view-fields--success-stories-hp--block.tpl.php template -->