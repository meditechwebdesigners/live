<!-- START about--node-4163.tpl.php -->
<?php
$url = $GLOBALS['base_url']; // grabs the site url
?>

  <style>
    .reverse-grid {
      display: block;
      flex-direction: row;
    }

    .bg--polar-gray {
      background-color: #f2f8fc;
    }

    @media screen and (max-width: 800px) {
      .reverse-grid {
        display: flex;
        flex-direction: column-reverse;
      }
      /** IE fix for flex-direction height issue **/
      .reverse-grid-fix {
        min-height: 100px;
      }
    }

  </style>

  <!-- Block 1 -->
  <div class="container">
    <div class="container__centered reverse-grid">

      <div class="container__one-third">
        <h2>A Dedicated Community</h2>
          <p>For MEDITECH, being part of a community has many meanings. We think environmentally. We engage in the arts and sciences in local higher education and support local businesses. From the blood drives we hold regularly in our facilities to the financial and technical support we provide to our "neighbors" in Puerto Rico, MEDITECH strives to be a responsible member of the global community.</p>
      </div>
      <div class="container__two-thirds reverse-grid-fix center"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/dedicated-community-animation.gif" alt="A dedicated MEDITECH community"></div>
    </div>
  </div>
  <!-- End Block 1 -->

  <!-- Block 2 -->
  <div class="container bg--polar-gray">
    <div class="container__centered">

      <div class="container__one-half center"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/stem-education-and-innovation-animation.gif" alt="STEM education & innovation"></div>
      <div class="container__one-half">
        <h2>STEM Education & Innovation</h2>
          <p>MEDITECH works closely with local middle and high schools educating on what we do and STEM careers as a whole. The goal of this program is to expose children, at a relatively young age, to the science, technology, engineering, and math fields with the hope of piquing their interest in these fields.</p>

          <p>MEDITECH partners with the Northeastern University Nurse Innovation and Entrepreneurship Advisory Board, bringing leaders of healthcare, electronic health records, and digital applications together with nurses and nursing students to explore new solutions to clinical issues.</p>
      </div>
    </div>
  </div>
  <!-- End Block 2 -->

  <!-- Block 3 -->
  <div class="container">
    <div class="container__centered reverse-grid">

      <div class="container__one-half">
        <h2>Charitable Causes</h2>    
      <p>During the holidays, many MEDITECH employees participate in an adopt-a-family program, donating gifts to local families in need, which they then hand deliver. MEDITECH is also active in supporting disaster relief efforts where our customers are most affected, recently supporting United for Puerto Rico.</p>

          <p>If your organization is interested in developing a relationship with MEDITECH, please review our <a href="https://ehr.meditech.com/about/community-contributions-application-guidelines">application guidelines</a>.</p>
      </div>
      <div class="container__one-half reverse-grid-fix center"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/charitable-causes-animation.gif" alt="Charitable causes"></div>
    </div>
  </div>
  <!-- End Block 3 -->

  <!-- Block 4 -->
  <div class="container bg--polar-gray">
    <div class="container__centered">

      <div class="container__two-thirds center"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/military-animation.gif" alt="MEDITECH Military Employment"></div>
      <div class="container__one-third">
          <h2>Military</h2>
          <p><a href="https://ehr.meditech.com/careers/veterans">MEDITECH employs veterans</a> and active-duty military, often recruiting directly on bases. The use of forward-focused technology is a passion of both enterprises.</p>
      </div>
    </div>
  </div>
  <!-- End Block 4 -->

  <!-- Block 5 -->
  <div class="container">
    <div class="container__centered reverse-grid">

      <div class="container__one-third">
        <h2>Diversity</h2>
        <p>At MEDITECH we believe there is strength in a <a href="https://ehr.meditech.com/diversity-at-meditech">diverse workforce</a>, powered by the contributions of people from different backgrounds, perspectives, and life experiences. We are committed to fostering a work environment and culture in which all of our staff members can reach their fullest potential.</p>
        <p>Our core principles as an organization are best embodied in our Code of Ethical Conduct.</p>
      </div>
      <div class="container__two-thirds reverse-grid-fix center"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/community--diversity-at-MEDITECH.png" alt=""></div>
    </div>
  </div>
  <!-- End Block 5 -->

  <!-- Block 6 -->
  <div class="container bg--polar-gray">
    <div class="container__centered">

      <div class="container__one-half center"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/college-career-animation.gif" alt="College & Career"></div>
      <div class="container__one-half">
          <h2>College & Career</h2>
      <p>MEDITECH is a company that appreciates and promotes higher learning, developing relationships over the years with local institutions like Bridgewater State University and Northeastern University.</p>

          <p>At local <a href="https://ehr.meditech.com/careers/college-to-career">college career centers</a>, our recruiting staff often holds resume critiques and mock interviews—dedicating their time to help students start their careers, even if it isn’t here at MEDITECH. We know that the only way to further expand our industry is through educating the next generation of technology professionals.</p>
      </div>
    </div>
  </div>
  <!-- End Block 6 -->

  <!-- Block 7 -->
  <div class="container">
    <div class="container__centered reverse-grid">

      <div class="container__one-half">
          <h2>Water & Power</h2>
      <p>In addition to auto-flush mechanisms in all bathrooms, we are adding low consumption toilets and sinks as we renovate our facilities. Rain sensors and time programs control the irrigation system to avoid unneeded watering.</p>

          <p>MEDITECH is converting lighting sources to LEDs and other high efficiency bulbs and ballasts. We also use occupancy and motion sensors to reduce the need for electricity.</p>
      </div>
      <div class="container__one-half reverse-grid-fix center"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/water-power-animation.gif" alt="Water & Power"></div>
    </div>
  </div>
  <!-- End Block 7 -->

  <!-- Block 8 -->
  <div class="container bg--polar-gray">
    <div class="container__centered">

      <div class="container__one-half center"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/innovative-climate-control-animation.gif" alt="Innovative Climate Control"></div>
      <div class="container__one-half">
          <h2>Innovative Climate Control</h2>
        <p>Automatic temperature controls, energy management software, and digital controllers keep our HVAC processes energy-efficient.</p>

            <p>The ponds at MEDITECH's Canton facility are part of a system where a series of wells extract water from the earth, providing environmentally- and financially-conscious heating, cooling, and irrigation to the entire campus. Once the extracted water is used, it is then automatically returned to the earth through irrigation or a series of leeching fields.</p>
      </div>
    </div>
  </div>
  <!-- End Block 8 -->
  
  <!-- Block 9 -->
<div class="container">
  <div class="container__centered reverse-grid">

      <div class="container__one-third">
          <h2>Recycling</h2>
          <p>We have drastically reduced our paper usage, and our cafeterias offer recycled paper cups and recycled paper napkins. We recycle paper, metal, wood, and all other recyclable materials. We also donate old equipment to local charities.</p>
    </div>
   <div class="container__two-thirds reverse-grid-fix center"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/recycle-animation.gif" alt="Recycling"></div>

  </div>
</div>
<!-- End Block 9 -->

<!-- Block 10 -->
<div class="container bg--polar-gray">
  <div class="container__centered">

   <div class="container__two-thirds center"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/art-program-animation.gif" alt="Art Program"></div>
    <div class="container__one-third">
        <h2>Art Program</h2>
        <p>Our company believes in supporting the arts, not just through charity work, but by participating in the art community. Every wall in our buildings is covered in works from local artists. Whether sculptural, paint, mixed media, or other format, the variety of works on display at all our locations promotes local artists, and creates an environment of creative thinking in our offices.</p>
    </div>

  </div>
</div>
<!-- End Block 10 -->

<!-- Block 11 -->
<div class="container background--cover">
  <div class="container__centered reverse-grid">

    <div class="container__one-third">
        <h2>Company Picnic</h2>
        <p>An annual tradition since the 1970s, thousands of MEDITECHers enjoy fun and food, provided in part by local law enforcement and vendors in our communities.</p>
    </div>
    <div class="container__two-thirds reverse-grid-fix center"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/company-picnic-animation.gif" alt="Company Picnic"></div>

  </div>
</div>
<!-- End Block 11 -->

<!-- END about--node-4163.tpl.php -->