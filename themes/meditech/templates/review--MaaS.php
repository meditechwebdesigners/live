<!-- START review--MaaS.php -->
<?php // This template is set up to control the display of the NEW MaaS campaign

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
  .two-columns { columns: 2; }
  
  .corner-border {
    border-radius: 7px;
  }
  
  .vendor-logo { margin-bottom: 1.5em; }
  .vendor-logo a img { max-height: 70px; width: auto; }
  
  .transparent-overlay { padding: 1em 2em; background-color: rgba(0, 0, 0, 0.8); }
  
  .fa-check-square {
    padding-right: 0.75em;
  }

  @media all and (max-width: 55em) {
    .two-columns { columns: 1; }
  }
</style>


<div class="js__seo-tool__body-content">

  <h1 style="display:none;" class="js__seo-tool__title">MEDITECH as a Service (MaaS)</h1>

  <!-- Block 1 -->
  <div class="container no-pad">
     <div class="gl-container">
        <div class="container__one-half gl-text-pad bg--green-gradient">
        <h2 class="header-one" style="margin-top:2em;">Get more, faster with MaaS.</h2>
        <p>You shouldn't have to choose between cost and quality when selecting an EHR. MEDITECH as a Service (MaaS) is a cost-effective and scalable EHR solution for organizations of any size or specialty. MaaS is MEDITECH's cloud-hosted option for data storage, transfer, and recovery, and leverages the latest security measures to keep patient records safe.</p>
        <div style="margin-top: 1.5em;">
          <?php hubspot_button($cta_code, "Read the MaaS ebook"); ?>
        </div>
      </div>
      <div class="container__one-half background--cover" style="background-image: url( <?php print $url; ?>/sites/all/themes/meditech/images/campaigns/business-man-tablet-cloud-technology-graphics.jpg); min-height:350px;">&nbsp;</div>
    </div>
  </div>
  <!-- End Block 1 -->


  <!-- Block 2 -->
  <div class="container" style="padding: 4em 0;">
    <div class="container__centered auto-margins">
      <h2 class="header-one no-margin--top">Low stress and no sacrifices: Welcome to the <i>full</i> Expanse.</h2>
      <p>Expanse invites you to see healthcare through a whole new lens with mobile <a href="https://blog.meditech.com/using-the-cloud-to-untether-medicine-from-wired-tech">tools that untether your care</a> and help you navigate the digital frontier with confidence. MaaS includes:</p>
      <ul class="fa-ul two-columns" style="margin-top: 2em;">
        <li><span class="fa-li"><i class="far fa-check-square text--orange" aria-hidden="true"></i></span>MEDITECH Expanse</li>
        <li><span class="fa-li"><i class="far fa-check-square text--orange" aria-hidden="true"></i></span>Web-based physician and nurse tools</li>
        <li><span class="fa-li"><i class="far fa-check-square text--orange" aria-hidden="true"></i></span>Interoperability initiatives</li>
        <li><span class="fa-li"><i class="far fa-check-square text--orange" aria-hidden="true"></i></span>Administrative, financial, and clinical applications</li>
        <li><span class="fa-li"><i class="far fa-check-square text--orange" aria-hidden="true"></i></span>Revenue Cycle</li>
        <li><span class="fa-li"><i class="far fa-check-square text--orange" aria-hidden="true"></i></span>Standard content</li>
        <li><span class="fa-li"><i class="far fa-check-square text--orange" aria-hidden="true"></i></span>Clinical decision support tools</li>
        <li><span class="fa-li"><i class="far fa-check-square text--orange" aria-hidden="true"></i></span>Browser access</li>
      </ul>
    </div>
  </div>
  <!-- End Block 2 -->


  <!-- Block 3 -->
  <div class="container bg--blue-gradient">
    <div class="container__centered">
      <div class="auto-margins">
        <h2 class="header-one">Expand your MaaS investment.</h2>
        <p>MaaS gives you the foundation and flexibility you need to scale up with added solutions, and pave the way for future sustainability. Grow your MaaS EHR to fit your needs, with MEDITECH solutions including:</p>
      </div>

      <div class="container auto-margins" style="padding:2em 0 0;">

        <div class="container__one-half">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/chemotherapy-patient-receiving-treatment.jpg" alt="cancer patient with nurse" class="corner-border">
          <h3>Expanse Oncology</h3>
          <p>Give cancer patients the best care possible, from diagnosis to survivorship. MEDITECH's specialty-driven <a href="https://ehr.meditech.com/ehr-solutions/meditech-oncology">Expanse Oncology solution</a> provides a modern, tailored, and mobile experience to help your team deliver efficient, whole person care for better outcomes.</p>
        </div>

        <div class="container__one-half">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/doctor-using-virtual-assistant-speaking-with-patients.jpg" alt="female doctor with patients" class="corner-border">
          <h3>Ambulatory</h3>
          <p>MEDITECH's integrated solution supports <a href="https://ehr.meditech.com/ehr-solutions/meditech-ambulatory">your ambulatory needs</a> and unites it with acute care to give your providers the complete patient story. Whether home health, behavioral health, long-term care, virtual care, physician practice, or beyond, we've got you covered.</p>
        </div>

      </div>
    </div>
  </div>
  <!-- End Block 3 -->  
  
  
  <!-- Block 4 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/speeding-by-bright-colors.jpg);">
    <div class="container__centered no-pad">
      <div class="container__two-thirds" style="padding:2em;">
        <div class="transparent-overlay text--white corner-border">
          <h2 class="header-one">Get up and running with an expedited go-LIVE.</h2>
          <p>By using evidence-based best practices and streamlined implementation techniques, MEDITECH will help your organization go LIVE with the latest version of MEDITECH Expanse in an expedited time frame. Here's how we make it happen:</p>
          <ul>
            <li>MaaS is delivered as a consolidated agreement, including software licenses, implementation, and service for MEDITECH and third-party vendors</li>
            <li>Data center is hosted remotely, so there's less strain on your technical resources</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- End Block 4 -->


  <!-- Block 5 -->
  <div class="container" style="padding: 6em 0;">
    <div class="container__centered">
      <div class="container__one-half">
        <h2 class="header-one no-margin--top">Secure, sustainable services at a predictable cost.</h2>
        <p>MaaS is a MEDITECH cloud-hosted solution with high levels of physical and network security. We take care of your EHR so you can take care of your patients.</p>
        <p>Healthcare organizations are choosing MaaS for numerous reasons, including:</p>
      </div>
      <div class="container__one-half">
        <ul class="fa-ul">
          <li><span class="fa-li"><i class="far fa-check-square text--meditech-green" aria-hidden="true"></i></span>Simple subscription</li>
          <li><span class="fa-li"><i class="far fa-check-square text--meditech-green" aria-hidden="true"></i></span>Minimal capital outlay</li>
          <li><span class="fa-li"><i class="far fa-check-square text--meditech-green" aria-hidden="true"></i></span>Streamlined contracting</li>
          <li><span class="fa-li"><i class="far fa-check-square text--meditech-green" aria-hidden="true"></i></span>MEDITECH cloud hosted</li>
          <li><span class="fa-li"><i class="far fa-check-square text--meditech-green" aria-hidden="true"></i></span>Focused implementation for an expedited go-LIVE</li>
          <li><span class="fa-li"><i class="far fa-check-square text--meditech-green" aria-hidden="true"></i></span>System maintenance and network security handled by experts</li>
          <li><span class="fa-li"><i class="far fa-check-square text--meditech-green" aria-hidden="true"></i></span>Fully interoperable and scalable</li>
        </ul>
      </div>
    </div>
  </div>
  <!-- End Block 5 -->

  <!-- Block 6 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dark-blurred-female-physician-using-tablet.jpg);">
    <article class="container__centered text--white center auto-margins">
      <figure>
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Matt-Walker-CEO-WilliamBee.png" alt=" Matt Walker headshot" style="width:150px;">
      </figure>
      <div class="text--large italic" style="font-size:1.4em; padding-top: .75em;">
        <p>"We chose MEDITECH's Expanse MaaS to achieve one fully integrated system. The EHR will enable all of our care teams to work off a single chart across our clinics and our hospital, as well as our associated ancillary services. This solution will ultimately lead to better quality and continuity of care for our patients."</p>
      </div>
      <p class="no-margin--bottom text--large bold text--meditech-green">Matt Walker, CEO</p>
      <p>William Bee Ririe Hospital</p>
    </article>
  </div>
  <!--End Block 6 -->

  <!-- Block 7 -->
  <div class="container no-pad" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/maas-cloud-vendors-bg.jpg); background-repeat:repeat-x; background-position: bottom;">
    <div class="container__centered text--white auto-margins center" style="padding-top: 3em; padding-bottom: 5em;">
      <h2 class="header-one">Who else is in the MaaS cloud?</h2>
      <p>Serving the technological needs of an enterprise takes more than one solution. MaaS incorporates features from multiple third-party vendors.</p>
    </div>
  </div>
  <div class="container__centered center"> 
    <div class="container">  
      <?php
        $vendor_ids = array(
          array('Access', '3436'),
          array('DrFirst', '1180'),
          array('FDB', '1083'),
          array('Forward', '1082'),
          array('IBM', '3530'),
          array('IMO', '1084'),
          array('Stedman', '1187'),
          array('Zynx', '1188'),
        );
        print '<div class="container" style="padding: 1em 0;">';
        for($v=0; $v<3; $v++){ // loop through first 3 vendors
          print '<div class="container__one-third vendor-logo">';
          print views_embed_view('vendor_logos', 'block', $vendor_ids[$v][1]); // 2nd item in each array
          print '</div>';
        }
        print '</div>';

        print '<div class="container" style="padding: 1em 0;">';
        for($v=3; $v<6; $v++){ // loop through next 3 vendors
          print '<div class="container__one-third vendor-logo">';
          print views_embed_view('vendor_logos', 'block', $vendor_ids[$v][1]); // 2nd item in each array
          print '</div>';
        }
        print '</div>';

        print '<div class="container" style="padding: 1em 0;">';
        for($v=6; $v<8; $v++){ // loop through last 2 vendors
          print '<div class="container__one-half vendor-logo">';
          print views_embed_view('vendor_logos', 'block', $vendor_ids[$v][1]); // 2nd item in each array
          print '</div>';
        }
        print '</div>';

      ?>
    </div>
  </div>
  <!-- End Block 7 -->

  <!--Block 8 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dark-blurred-colleagues-meeting-at-table.jpg);">
    <article class="container__centered text--white center auto-margins">
      <figure>
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/howard-messing-headshot-circle.png" alt="Howard Messing headshot" style="width:150px;">
      </figure>
      <div class="text--large italic" style="font-size:1.4em; padding-top: .75em;">
        <p>"This is the future of healthcare. We've been retooling MEDITECH for the new healthcare paradigm, reshaping our company to meet the needs of today's market and today's customers. It began with <a href="<?php print $url; ?>/expanse">Expanse</a>, one of the first web-based solutions, released as one of the first full-scale platforms of the post-Meaningful Use era. Now, our collaboration with Google Cloud and our commitment to the public cloud further expands the productivity and agility of our customers."</p>
      </div>
      <p class="no-margin--bottom text--large bold text--meditech-green">Howard Messing, CEO</p>
      <p>MEDITECH</p>
      <p style="margin-top: 2em;">Read more about MEDITECH's <a href="<?php print $url; ?>/news/meditech-announces-collaboration-with-google-cloud-to-bring-ehrs-to-the-public-cloud">Collaboration with Google Cloud</a> on our News page.</p>
    </article>
  </div>
  <!--End Block 8 -->

</div>
<!-- end js__seo-tool__body-content -->

<!-- Block 9 - CTA Block -->
<div class="container bg--white">
  <div class="container__centered" style="text-align: center;">

    <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
    <h2>
      <?php print $cta->field_header_1['und'][0]['value']; ?>
    </h2>
    <?php } ?>

    <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
    <div>
      <?php print $cta->field_long_text_1['und'][0]['value']; ?>
    </div>
    <?php } ?>

    <div class="center" style="margin-top:2em;">
      <?php hubspot_button($cta_code, "Read the MaaS ebook"); ?>
    </div>

    <div style="margin-top:1em;">
      <?php print $share_link_buttons; ?>
    </div>

  </div>
</div>
<!-- End Block 9 -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>