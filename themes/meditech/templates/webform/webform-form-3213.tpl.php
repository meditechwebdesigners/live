<?php
/**
 * @file
 * Customize the display of a complete webform.
 *
 * This file may be renamed "webform-form-[nid].tpl.php" to target a specific
 * webform on your site. Or you can leave it "webform-form.tpl.php" to affect
 * all webforms on your site.
 *
 * Available variables:
 * - $form: The complete form array.
 * - $nid: The node ID of the Webform.
 *
 * The $form array contains two main pieces:
 * - $form['submitted']: The main content of the user-created form.
 * - $form['details']: Internal information stored by Webform.
 *
 * If a preview is enabled, these keys will be available on the preview page:
 * - $form['preview_message']: The preview message renderable.
 * - $form['preview']: A renderable representing the entire submission preview.
 */
?>
<!-- START webform-form-3213.tpl.php CCPA Data Request FORM -->

<?php
  // Print out the progress bar at the top of the page
  print drupal_render($form['progressbar']);

  // Print out the preview message if on the preview page.
  if (isset($form['preview_message'])) {
    print '<div class="messages warning">';
    print drupal_render($form['preview_message']);
    print '</div>';
  }
?>

<div>
  <h2>Section 1: Requestor Information</h2>
  <?php print drupal_render($form['submitted']['requestors_full_name']); ?>
  <?php print drupal_render($form['submitted']['requestors_mailing_address']); ?>
  <?php print drupal_render($form['submitted']['requestors_telephone_number']); ?>
  <?php print drupal_render($form['submitted']['requestors_email_address']); ?>
  
  <?php print drupal_render($form['submitted']['are_you_the_consumer']); ?>
  <p style="margin:1em;"><em>*To use an authorized agent, you must provide the agent with written authorization. To the extent we are unable to verify the identity of the person submitting this form, we may request additional information from the person making the submission.</em></p>
  
  <h2>Section 2: Consumer/Data Subject Information</h2>
  <?php print drupal_render($form['submitted']['consumers_full_name']); ?>
  <?php print drupal_render($form['submitted']['consumers_state_of_residence']); ?>
  <?php print drupal_render($form['submitted']['consumers_email_address']); ?>
  <?php print drupal_render($form['submitted']['what_has_been_the_consumers_affiliation_to_meditech']); ?>
  <?php print drupal_render($form['submitted']['name_of_your_primary_contact_at_meditech_if_any']); ?>
  
  <h2>Section 3: Specify Request(s)**</h2> 
  <p>Check all that apply.</p>
  <?php print drupal_render($form['submitted']['request_type']); ?>
  
  <p style="margin:1em; float:left; width:100%;"><em>MEDITECH does not sell your personal information so no request is necessary.</em></p>
  
  <p style="margin:1em;"><em><strong>**Request for online activity information</strong><br>
  If your request applies to online activity information that MEDITECH may have collected through cookies or similar technologies, you must make your request from the browser or device that you have previously used to access MEDITECH's websites or apps. This allows us to read any identifier that we have assigned to your browser or device.</em></p>
  
  <h2>Section 4: Declaration</h2> 

  <p>By submitting this form, I hereby certify that the information entered into this form is complete, accurate, and up-to-date, and that I am the consumer who is the subject of the request or have been authorized by that consumer to act on his/her behalf, as indicated above. I understand that it may be necessary for MEDITECH to verify the identity of the consumer and/or authorized agent for this request, and additional information may be requested for this purpose. I also may be asked to provide further clarifying information regarding the information or action I am requesting from MEDITECH.</p>

  <p>MEDITECH reserves the right to refuse requests, in part or in whole, to the extent permitted by law, if we are unable to verify your identity, or if we cannot verify your authority to act on behalf of another person.</p>

  <p>Please note that if the information you request reveals details directly or indirectly about another person, it may be excluded from any data access response sent to you.</p>

  <?php print drupal_render($form['submitted']['i_understand_and_accept']); ?>
</div>

<?php
  // Print out the main part of the form.
  // Feel free to break this up and move the pieces within the array.
  print drupal_render($form['submitted']);

  // Always print out the entire $form. This renders the remaining pieces of the
  // form that haven't yet been rendered above (buttons, hidden elements, etc).
  print drupal_render_children($form);

print '<!-- END webform-form-3213.tpl.php -->';

// see template.php for other edits like submit button