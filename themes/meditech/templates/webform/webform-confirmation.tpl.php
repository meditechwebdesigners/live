<?php
/**
 * @file
 * Customize confirmation screen after successful submission.
 *
 * This file may be renamed "webform-confirmation-[nid].tpl.php" to target a
 * specific webform e-mail on your site. Or you can leave it
 * "webform-confirmation.tpl.php" to affect all webform confirmations on your
 * site.
 *
 * Available variables:
 * - $node: The node object for this webform.
 * - $progressbar: The progress bar 100% filled (if configured). This may not
 *   print out anything if a progress bar is not enabled for this node.
 * - $confirmation_message: The confirmation message input by the webform
 *   author.
 * - $sid: The unique submission ID of this submission.
 * - $url: The URL of the form (or for in-block confirmations, the same page).
 */

$main_url = $GLOBALS['base_url']; // grabs the site url
  
?>
<!-- start webform-confirmation.tpl.php template -->

  <section class="container__centered">
	
    <?php print $progressbar; ?>

    <div class="webform-confirmation">
      <?php 
      
        if($confirmation_message){
          print $confirmation_message;
          print '<p>&nbsp;</p>';
          print '<p>&nbsp;</p>';
          print '<p>&nbsp;</p>';
          print '<p>&nbsp;</p>';
          print '<p>&nbsp;</p>';
          print '<p>&nbsp;</p>';
          print '<p>&nbsp;</p>';
        }
      
        elseif( $node->nid == 1609 ){
        ?>
          <h2 class="center">How MEDITECH’s Web EHR Improves Physician Productivity</h2>
          <div class="video js__video" data-video-id="190746060" style="margin-top: 2em; margin-bottom: 2em;">
            <figure class="video__overlay">
              <img src="<?php print $main_url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--Web-EHR-Improves-Physician-Productivity.jpg" alt="Video Covershot" style="width:100%;">
            </figure>
            <a class="video__play-btn video_gae" href="https://vimeo.com/190746060"></a>
            <div class="video__container"></div>
          </div>
          <div class="center">
            <h3>Momentum for our latest platform is growing fast. <br/> Get a taste for <a href="https://ehr.meditech.com/ehr-solutions/6-1-success">how our customers are winning with 6.1</a>. (And so will you!)</h3>
          </div>
        <?php
          
        }
        else{
          print '<p>'.t('Thank you, your submission has been received.').'</p>';
          print '<p>&nbsp;</p>';
          print '<p>&nbsp;</p>';
          print '<p>&nbsp;</p>';
          print '<p>&nbsp;</p>';
          print '<p>&nbsp;</p>';
          print '<p>&nbsp;</p>';
          print '<p>&nbsp;</p>';
        }
      ?>
    </div>

  </section>
  
<!-- end webform-confirmation.tpl.php template -->