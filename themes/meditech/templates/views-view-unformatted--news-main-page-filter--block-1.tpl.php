<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<!-- START views-view-unformatted--news-main-page-filter--block-1.tpl.php -->
<div class="auto-margins">

 <?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <?php print $row; ?>
<?php endforeach; ?>

</div>
<style>
div.item-list {
  max-width: 50em;
  margin-left: auto;
  margin-right: auto;
}
ul.pager {
  margin-bottom: 4em; margin-left: auto; margin-right: auto;
}
</style>
<!-- END views-view-unformatted--news-main-page-filter--block-1.tpl.php -->