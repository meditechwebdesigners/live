<!-- start customer-materials-page-menu--node-2820.php template Labor and Delivery Announcement page -->
<?php
$content_sections = multi_field_collection_data_unrestricted($node, 'field_fc_head_ltext_1');
$buttons = multi_field_collection_data_unrestricted($node, 'field_fc_button_unl_1');
$side_menu = multi_field_collection_data_unrestricted($node, 'field_fc_text_link_unl_1');
?>

  <!-- HERO section -->
  <div class="container no-pad background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/customer/nurse-handing-baby-to-patient--background.jpg);">
    <div class="container__centered bg-overlay--white">
      <div class="container__one-half" style="padding:4em 1em 4em 0em;">
        <h1>
          <?php print $title; ?>
        </h1>
        <?php if( !empty($content['field_sub_header_1']) ){ ?>
        <h2>
          <?php print render($content['field_sub_header_1']); ?>
        </h2>
        <?php } ?>
      </div>
    </div>
  </div>
  <!-- End of HERO section -->

  <section class="container__centered">

    <div class="container__two-thirds">

    <?php

    if( isset($content_sections) && !empty($content_sections) ){
      $number_of_content_sections = count($content_sections);
      for($cs=0; $cs<$number_of_content_sections; $cs++){
        print '<h2>';
        print $content_sections[$cs]->field_header_1['und'][0]['value']; 
        print '</h2>';
        print '<div>';
        print $content_sections[$cs]->field_long_text_1['und'][0]['value']; 
        print '</div>';
      }
    }
/*
    if( isset($buttons) && !empty($buttons) ){
      $number_of_buttons = count($buttons);
      for($b=0; $b<$number_of_buttons; $b++){
        if( !empty($buttons[$b]->field_hubspot_embed_code_1['und'][0]['value']) && $buttons[$b]->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
          $button_code = $buttons[$b]->field_hubspot_embed_code_1['und'][0]['value']; 
          print '<div class="center" style="margin-top:2em;">';
          hubspot_button($button_code, "button");
          print '</div>';
        }
        else{
          print '<a href="';
          print $buttons[$b]->field_button_url_1['und'][0]['value'];
          print '" class="btn--orange">';
          print $buttons[$b]->field_button_text_1['und'][0]['value'];
          print '</a>';
        }
      }
    }
*/    

    ?>

    </div>

    <aside class="container__one-third <?php 
    if( isset($side_menu) && $side_menu[0] != '' ){ 
      print 'panel';
    } 
  ?>">

      <?php
    if( isset($side_menu) && $side_menu[0] != '' ){
      print '<div class="sidebar__nav solutions_sidebar_gae">';
      print '<ul class="menu">';
      $number_of_menu_links = count($side_menu);
      for($ml=0; $ml<$number_of_menu_links; $ml++){
        print '<li><a href="';
        print $side_menu[$ml]->field_link_url_1['und'][0]['value'];
        print '"';
        if($side_menu[$ml]->field_external_link['und'][0]['value'] == 1){
          print ' target="_blank"';
        }
        print '>';
        print $side_menu[$ml]->field_link_text_1['und'][0]['value']; 
        print '</a></li>';
      }
      print '</ul>';
      print '</div>';
    }
    ?>

    </aside>

  </section>
  <!-- end customer-materials-page-menu--node-2820.php template -->
