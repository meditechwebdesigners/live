<?php
/**
 * @file
 * Default theme implementation for displaying a single search result.
 *
 * This template renders a single search result and is collected into
 * search-results.tpl.php. This and the parent template are
 * dependent to one another sharing the markup for definition lists.
 *
 * Available variables:
 * - $url: URL of the result.
 * - $title: Title of the result.
 * - $snippet: A small preview of the result. Does not apply to user searches.
 * - $info: String of all the meta information ready for print. Does not apply
 *   to user searches.
 * - $info_split: Contains same data as $info, split into a keyed array.
 * - $module: The machine-readable name of the module (tab) being searched, such
 *   as "node" or "user".
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Default keys within $info_split:
 * - $info_split['module']: The module that implemented the search query.
 * - $info_split['user']: Author of the node linked to users profile. Depends
 *   on permission.
 * - $info_split['date']: Last update of the node. Short formatted.
 * - $info_split['comment']: Number of comments output as "% comments", %
 *   being the count. Depends on comment.module.
 *
 * Other variables:
 * - $classes_array: Array of HTML class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $title_attributes_array: Array of HTML attributes for the title. It is
 *   flattened into a string within the variable $title_attributes.
 * - $content_attributes_array: Array of HTML attributes for the content. It is
 *   flattened into a string within the variable $content_attributes.
 *
 * Since $info_split is keyed, a direct print of the item is possible.
 * This array does not apply to user searches so it is recommended to check
 * for its existence before printing. The default keys of 'type', 'user' and
 * 'date' always exist for node searches. Modules may provide other data.
 * @code
 *   <?php if (isset($info_split['comment'])): ?>
 *     <span class="info-comment">
 *       <?php print $info_split['comment']; ?>
 *     </span>
 *   <?php endif; ?>
 * @endcode
 *
 * To check for all available data within $info_split, use the code below.
 * @code
 *   <?php print '<pre>'. check_plain(print_r($info_split, 1)) .'</pre>'; ?>
 * @endcode
 *
 * @see template_preprocess()
 * @see template_preprocess_search_result()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<!-- START search-result.tpl.php -->
<div>
  <?php // get node ID...
    $nid = $variables['result']['node']->nid;
    $node = node_load($nid);

    $nodeType = $node->type;

    switch($nodeType):
      case 'ehr_solutions_inner':
      case 'ehr_solutions_main':
      case 'regulatory':
          $contentType = 'EHR Solutions';
      break;
      case 'executive':
      case 'about':
      case 'location':
      case 'quote':
          $contentType = 'About MEDITECH';
      break;
      case 'news_article':
          $contentType = 'News';
      break;
      case 'job_listing':
      case 'career_event':
      case 'careers':
          $contentType = 'Careers';
      break;
      case 'event':
      case 'events_trade_show':
      case 'events_simple_page':
          $contentType = 'Events';
      break;
      case 'video':
          $contentType = 'Video';
      break;
      default:
          $contentType = '';
      break;
    endswitch; 

  ?>
  <?php print render($title_prefix); ?>
  <h3><a class="search_result_link_gae" href="<?php print $url; ?>"><?php print $title; ?></a> <span style="font-size:.6em; font-weight:normal;"><?php print $contentType; ?></span></h3>
  
  <?php print render($title_suffix); ?>
  
  <div class="inline__text__wrapper">
    <?php if($nodeType == 'news_article' || $nodeType == 'job_listing'){ ?>
      <p><span class="bold"><time datetime="<?php print date('Y-m-d', $node->published_at); ?>" itemprop="datePublished"><?php print date("F j, Y", $node->published_at); ?></time></span>&nbsp;&mdash;&nbsp;</p>
    <?php } ?>

    <p><?php print $snippet; ?></p>

    <hr>

  </div>
  
</div><!-- END snippet__card -->
<!-- END search-result.tpl.php -->