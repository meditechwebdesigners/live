<!-- inc-virtual-care-minutes.php -->
 <div class="center">
  <h2 class="no-margin--top">Over 3 Million Minutes</h2>
  <p>Healthcare organizations have used MEDITECH's Virtual Visits to provide their patients with over 3 million minutes of virtual care since the start of the COVID-19 pandemic&mdash;March 15th - December 31st.</p>
</div>
<!-- inc-virtual-care-minutes.php -->