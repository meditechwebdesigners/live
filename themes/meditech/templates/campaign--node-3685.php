<!-- START campaign--node-3685.php -->
<?php // This template is set up to control the display of the Expanse Patient Connect Campaign

$url = $GLOBALS['base_url']; // grabs the site url

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
	.content__callout,
	.content__callout__content {
		background-color: transparent;
		color: #fff;
	}

	.card__info {
		padding: 1em 2em;
	}

	.gl-text-pad {
		padding: 5em !important;
	}

	@media all and (max-width: 50em) {
		.gl-text-pad {
			padding: 2em !important;
		}

		.card figure img {
			height: 225px;
		}
	}

</style>

<div class="js__seo-tool__body-content">

	<h1 style="display:none;" class="js__seo-tool__title">Expanse Patient Connect</h1>


	<!-- START Block 1 -->
	<div class="bg--purple-gradient">
		<div class="content__callout">
			<div class="content__callout__media">
				<div class="content__callout__image-wrapper">
					<div class="video js__video" data-video-id="544527596">
						<figure class="video__overlay">
							<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--woman-texting-on-walk.jpg" alt="Woman texting on walk - video covershot">
						</figure>
						<a class="video__play-btn" href="https://vimeo.com/544527596"></a>
						<div class="video__container"></div>
					</div>
				</div>
			</div>
			<div class="content__callout__content">
				<div class="content__callout__body">
					<h2>Communicate at the speed of life with Expanse Patient Connect</h2>
					<p>With Expanse Patient Connect, our latest Cloud Platform service, you can build a community of engaged, loyal consumers — while also increasing your organizational efficiency via a modern, web-based solution.</p>
					<p>You can “close the last mile” to patients by connecting with them, wherever they are and through their preferred channel. And improve your bottom line by making your staff more efficient, as well as reducing appointment no-shows.</p>
					<div style="margin-top:2em;">
						<?php hubspot_button($cta_code, "Sign Up For The Expanse Summer Showcase"); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END Block 1 -->


	<!-- START Block 2-->
	<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/bg--organic-shapes-green-blue.svg);">
		<div class="container__centered">
			<div class="auto-margins center" style="margin-bottom:2em;">
				<h2>Improve the healthcare experience</h2>
				<p>Patients and healthcare organizations alike benefit from timely two-way communication. Here’s what Expanse Patient Connect can do for you and your patients:</p>
			</div>
			<div class="card__wrapper">
				<div class="container__one-half card" style="padding: 2em;">
					<div class="center">
						<img style="width:125px; margin-bottom:1em;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--patient-hand-texting.svg" alt="Icon of a person's hand holding a phone with a messaging screen shown">
						<h3>For Patients</h3>
					</div>
					<ul class="fa-ul">
						<li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Convenient technology they're already accustomed to</li>
						<li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>No app or internet access needed to respond back</li>
						<li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>No time spent on hold confirming appointments</li>
						<li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Quick launch into the patient portal to complete pre-visit forms</li>
					</ul>
				</div>
				<div class="container__one-half card" style="padding: 2em;">
					<div class="center">
						<img style="width:125px; margin-bottom:1em;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--organization-sending-messages.svg" alt="Icon of an administrative person sending out a message to the patient">
						<h3>For Healthcare Organizations</h3>
					</div>
					<ul class="fa-ul">
						<li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Reduce phone tag with patients</li>
						<li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Confirm appointments and identify cancellations sooner to fill open slots</li>
						<li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Confirmations and cancellations automatically flow back into Expanse, saving time</li>
						<li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Free up staff to focus on more high value tasks</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- END Block 2 -->


	<!-- START Block 3 -->
	<div class="container no-pad">
		<div class="gl-container">
			<div class="container__one-half background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/patient-using-questionnaire.jpg); min-height:450px; background-position: top;"></div>
			<div class="container__one-half gl-text-pad bg--purple-gradient">
				<h2>Drive a deeper connection to patient health and wellness </h2>
				<p>Expanse Patient Connect can help your organization to drive portal adoption and use by:</p>
				<ul class="fa-ul">
					<li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Providing an easy launch to the portal in the appointment reminder text, including links that feed patient intake/appointment workflows in day-of-appointment messages.</li>
					<li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Reminding patients to check the portal for results or post-visit instructions in follow-up messages.</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- END Block 3 -->


	<!-- START Block 4 -->
	<div class="container no-pad">
		<div class="gl-container">
			<div class="container__one-half gl-text-pad bg--purple-gradient">
				<h2>Healthier patients for a healthier bottom line</h2>
				<p>As healthcare organizations make the move from volume to value, engaging the right patients at the right time and in the right way is a key component of success.</p>
				<p>With Expanse Patient Connect, you can reach specific members of your patient population through their preferred channel, and close care gaps, manage chronic diseases, and promote wellness with actionable messages to pre-selected patients.</p>
			</div>
			<div class="container__one-half background--cover flex-order--reverse" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/busy-park-with-senior-man-messaging.jpg); background-position: right; min-height:450px;"></div>
		</div>
	</div>
	<!-- END Block 4 -->


	<!-- START Block 5-->
	<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/bg--organic-shapes-green-blue-reversed.svg);">
		<div class="container__centered">
			<div class="center" style="margin-bottom:2em;">
				<h2>Confirm your ROI</h2>
				<p>Patients value convenient communication. Expanse Patient Connect ensures that value is reciprocal by:</p>
			</div>
			<div class="card__wrapper">
				<div class="container__one-third card">
					<figure>
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/man-texting-walking-down-street.jpg" alt="Man walking down the street looking at his phone">
					</figure>
					<div class="card__info">
						<p>Engaging with patients through their preferred channel (text, email, or phone) to improve patient retention and acquisition.</p>
					</div>
				</div>
				<div class="container__one-third card">
					<figure>
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/happy-administrator-updating-appointments.jpg" alt="Female administrator updating patients information">
					</figure>
					<div class="card__info">
						<p>Prompting patients to confirm or cancel appointments in advance of their visit, thereby allowing open time slots to be identified and filled.</p>
					</div>
				</div>
				<div class="container__one-third card">
					<figure>
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/doctor-talking-with-elderly-patient.jpg" alt="Male administrator speaking with patients">
					</figure>
					<div class="card__info">
						<p>Automating routine communications, which frees up staff and phone lines for high quality patient interactions.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END Block 5 -->

	<!-- START Block 6 -->
	<div class="container bg--purple-gradient">
		<div class="container__centered center">
			<?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
			<h2>
				<?php print $cta->field_header_1['und'][0]['value']; ?>
			</h2>
			<?php } ?>

			<?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
			<div>
				<?php print $cta->field_long_text_1['und'][0]['value']; ?>
			</div>
			<?php } ?>
			<div class="center" style="margin-top:2em;">
				<?php hubspot_button($cta_code, "Sign Up For The Expanse Summer Showcase"); ?>
			</div>
			<div style="margin-top:1em;">
				<?php print $share_link_buttons; ?>
			</div>
		</div>
	</div>
	<!-- END Block 6 -->


</div>
<!-- end js__seo-tool__body-content -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>

<!-- END campaign--node-3685.php -->