<?php // This template is for each row of the Views block: LOCATIONS - CONTACT PAGE ....................... ?>
<!-- start views-view-fields--locations-contact-page--block.tpl.php template -->
<div class="sidebar--nav--list--item">
  <a href="<?php print $fields['field_directions_url']->content; ?>" target="_blank" class="sidebar__nav__link--external directions_gae"><?php print $fields['title']->content; ?></a>
</div>
<!-- end views-view-fields--locations-contact-page--block.tpl.php template -->