<!-- START events-simple-page--node-4217.php -->

<?php // This template is set up to control the display of the MEDITECH at HIMSS22 content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
	/*	Universal styles */
	.pad-adjust {
		padding: 5em 0;
	}

	.bg-gradient {
		background-color: #02071b;
		background-image: linear-gradient(0, #1a1a2b 20%, #02071b 100%);
	}

	.bg--white-dots {
		background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/events/white-dot.png);
		background-repeat: repeat;
	}


	.btn--outline {
		background-color: transparent;
		border-width: 1px !important;
		-webkit-transition: all 400ms ease-in-out;
	}


	.gradient-box:before {
		content: "";
		position: absolute;
		border-radius: 7px;
		left: 0px;
		top: 0px;
		right: 0px;
		bottom: 0px;
		background: #1a1a2b;
		z-index: 2;
	}

	.gradient-box:after {
		content: "";
		position: absolute;
		border-radius: 7px;
		left: -1px;
		top: -1px;
		right: -1px;
		bottom: -1px;
		background: -webkit-gradient(linear, left top, right top, from(#00bc6f), color-stop(#c471ed), to(#00BC6F));
		background: -webkit-linear-gradient(left, #00bc6f, #c471ed, #00BC6F);
		background: -o-linear-gradient(left, #00bc6f, #c471ed, #00BC6F);
		background: linear-gradient(to right, #00bc6f, #c471ed, #00BC6F);
		z-index: 1;
	}

	/*	Block 1 - background video */
	.bg-vid--wrap {
		position: relative;
		overflow: hidden;
		width: 100%;
		height: 675px;
		background: url(<?php print $url; ?>/sites/all/themes/meditech/images/events/twisting-shapes--bg.jpg) no-repeat top center/cover;
	}

	video {
		min-width: 100%;
		z-index: 1;
		height: 700px;
		object-fit: cover;
	}

	.bg-vid--overlay {
		width: 100%;
		position: absolute;
		padding: 8em 0;
		top: 0;
		background: linear-gradient(111deg, rgb(51 31 81 / 50%) 0%, rgb(38 5 132 / 75%) 55%, rgba(0, 0, 0, 0) 100%);
		z-index: 2;
	}

	/*	Block 2 */
	.box-item {
		padding: 1.5em;
		width: 33.3%;
		float: left;
	}

	.box-item p:first-child {
		font-family: "montserrat", Verdana, sans-serif;
		font-weight: bold;
		margin-bottom: 0.5em;
	}

	.gradient-line--hr {
		width: 1100px;
		height: 1px;
	}

	.horizontal-menu {
		display: flex;
		justify-content: center;
	}

	a.h-menu-item {
		padding: 1em 1.25em;
		transition: 0.3s ease;
		text-decoration: none;
		border-bottom: 0;
	}

	.h-menu-seperator {
		border-right: 1px solid #756484;
		margin: 1.25em 0;
	}

	a.h-menu-item:hover {
		background-color: #24083e;
		color: #fff;
		border-bottom: 0;
		transition: 0.3s ease;
	}

	.mobile-menu a {
		color: #fff;
		font-size: 1.25em;
	}

	.accordion__dropdown {
		border: none;
		background-color: #301548;
		border-top: 1px solid #756484;
	}

	.accordion__dropdown ul {
		list-style-type: none;
		text-align: center;
		padding-left: 0;
		margin: 0;
	}

	.accordion__dropdown ul li {
		margin: 1em 0;
	}

	.accordion__dropdown li {
		padding: .25em 0;
	}

	.accordion__link {
		padding: 1.2em 1.2em 1.2em 40px;
	}

	/*	Block 3 */
	.shadow-box.w-headshot-outline {
		padding: 4em 2em 2em 2em;
		position: relative;
		margin-top: 50px;
		box-shadow: none;
		background-color: transparent;
		margin-bottom: 3em;
		min-height: 330px;
	}

	.headshot {
		position: absolute;
		top: -50px;
		left: 50%;
		transform: translate(-50%);
		z-index: 3;
	}

	.headshot img {
		width: 100px;
		height: 100px;
		border-radius: 50%;
	}

	.quote-text {
		z-index: 3;
		position: relative;
	}

	.w-headshot-outline:before {
		content: "";
		position: absolute;
		border-radius: 7px;
		left: 0px;
		top: 0px;
		right: 0px;
		bottom: 0px;
		background: #0f1124;
		z-index: 2;
	}

	.w-headshot-outline:after {
		content: "";
		position: absolute;
		border-radius: 7px;
		left: -1px;
		top: -1px;
		right: -1px;
		bottom: -1px;
		background: -webkit-gradient(linear, left top, right top, from(#00bc6f), color-stop(#c471ed), to(#00BC6F));
		background: -webkit-linear-gradient(left, #00bc6f, #c471ed, #00BC6F);
		background: -o-linear-gradient(left, #00bc6f, #c471ed, #00BC6F);
		background: linear-gradient(to right, #00bc6f, #c471ed, #00BC6F);
		z-index: 1;
	}

	.headshot.tweet {
		top: unset;
		bottom: -15px;
	}

	.fa-twitter:before {
		content: "\f099";
	}

	a.tweet-btn {
		height: 20px;
		padding: 5px 12px 5px 12px;
		background-color: #1d9bf0;
		color: #fff;
		border-radius: 50px;
		font-size: 12pt;
		white-space: nowrap;
		border-bottom: none;
		-webkit-transition: background 0.25s ease-in-out;
		-moz-transition: background 0.25s ease-in-out;
		transition: background 0.25s ease-in-out;

	}

	a:hover.tweet-btn {
		color: #fff;
		background-color: #0c7abf;
		border-bottom: none;
		-webkit-transition: background 0.25s ease-in-out;
		-moz-transition: background 0.25s ease-in-out;
		transition: background 0.25s ease-in-out;
	}

	a:focus-visible.tweet-btn {
		outline: 2px solid #fff;
	}

	a.tweet-btn[target="_blank"]:after {
		content: '';
	}

	/*	Block 4 */
	.fw-pad2 {
		padding: 1em 5em 2em 5em;
		max-width: 1800px;
		margin: 0 auto;
	}

	.gl-container {
		background: transparent;
		border-radius: 7px;
	}
	
	.header-micro {
		margin-top: 0;
	}

	.grid-item-1 .header-micro,
	.grid-item-2 .header-micro,
	.grid-item-3 .header-micro {
		line-height: 1.5;
	}

	.grid-layout,
	.grid-2x2 {
		width: 100%;
		display: grid;
		grid-template-columns: repeat(6, 1fr);
		gap: 1.5em;
	}

	.grid-layout>div,
	.grid-2x2>div {
		border-radius: 7px;
		background-color: #26263C;
		padding: 2em;
	}

	.grid-item-1 {
		grid-column: 1 / span 3;
		grid-row: 1;
	}

	.grid-item-2 {
		grid-column: 4 / span 3;
		grid-row: 1;
	}

	.grid-item-3 {
		grid-column: 1 / span 6;
		grid-row: 2 / span 2;
	}

	.grid-2x2 div:nth-child(1) {
		grid-column: 1 / span 3;
		grid-row: 1;
	}

	.grid-2x2 div:nth-child(2) {
		grid-column: 4 / span 3;
		grid-row: 1;
	}

	.grid-2x2 div:nth-child(3) {
		grid-column: 1 / span 3;
		grid-row: 2;
	}

	.grid-2x2 div:nth-child(4) {
		grid-column: 4 / span 3;
		grid-row: 2;
	}


	/*	Block 6 */
	.vendors .shadow-box {
		margin-bottom: 1.5em;
		min-height: 145px;
		box-shadow: none;
	}

	/*	Media Queries */
	@media all and (max-width: 1135px) {
		.bg-vid--overlay {
			padding: 8em 4em;
		}

		.bg-vid--overlay .container__one-half {
			width: 100%;
		}

		.bg-vid--wrap {
			height: 600px;
		}
	}

	@media all and (max-width: 990px) {
		a.h-menu-item {
			font-size: 85%;
			padding: 1em
		}
	}


	@media all and (max-width: 800px) {
		.bg-vid--overlay {
			padding: 4em;
		}

		.bg-vid--wrap {
			height: 355px;
		}

		.horizontal-menu {
			display: none;
		}

		.shadow-box.w-headshot-outline {
			padding: 5em 2em 2em 2em;
			min-height: 0;
		}

		.vendors .shadow-box {
			margin-bottom: 1.5em;
			min-height: 130px;
		}

		.grid-item-1 {
			grid-column: 1 / span 6;
			grid-row: 1;
		}

		.grid-item-2 {
			grid-column: 1 / span 6;
			grid-row: 2;
		}

		.grid-item-3 {
			grid-column: 1 / span 6;
			grid-row: 3;
		}

		.grid-2x2 div:nth-child(1) {
			grid-column: 1 / span 9;
			grid-row: 1;
		}

		.grid-2x2 div:nth-child(2) {
			grid-column: 1 / span 9;
			grid-row: 2;
		}

		.grid-2x2 div:nth-child(3) {
			grid-column: 1 / span 9;
			grid-row: 3;
		}

		.grid-2x2 div:nth-child(4) {
			grid-column: 1 / span 9;
			grid-row: 4;
		}
	}

	@media all and (min-width: 800px) {
		.dropdown-menu {
			display: none;
		}
	}

	@media all and (max-width: 700px) {
		.bg-vid--wrap video {
			display: none;
		}

		.bg-vid--wrap {
			height: auto;
			background-position: left;
		}

		.bg-vid--overlay {
			background: linear-gradient(111deg, rgb(51 31 81 / 100%) 0%, rgb(38 5 132 / 75%) 55%, rgba(0, 0, 0, .75) 100%);
			position: relative;
			padding: 2em;
		}

		.box-item {
			width: 100%;
		}

		.gradient-line--hr {
			width: 50%;
		}

		.fw-pad2 {
			padding: 1em 2em;
		}
	}

</style>

<div class="js__seo-tool__body-content">

	<!-- Block 1 -->
	<div class="container no-pad bg-vid--wrap">
		<video src="<?php print $url; ?>/sites/all/themes/meditech/images/events/twisting-shapes--bg.mp4" loop muted autoplay>
		</video>
		<div class="bg-vid--overlay">
			<div class="container__centered text--white">
				<div class="container__one-half">
					<h1 class="header-xl js__seo-tool__title no-margin--bottom">MEDITECH at HIMSS22</h1>
					<h2 class="header-one no-margin--top">Transformation in motion.</h2>
					<p>Change is happening in healthcare, and we’re in the thick of it. To adjust to the ebbs and flows, we need to facilitate better business practices, reduce clinician burden, and most importantly, improve patient outcomes. With MEDITECH Expanse, you have a trusted partner by your side to help propel you forward, no matter where you are in your journey toward digital transformation.</p>
					<p><a href="https://himss22.mapyourshow.com/8_0/exhibitor/exhibitor-details.cfm?exhid=107608" target="_blank"><strong>Visit us in booth #3311</strong></a> at #HIMSS22 and let’s achieve more together.</p>
				</div>
			</div>
		</div>
	</div>
	<!-- END Block 1 -->

	<!-- Horizontal Navigation Bar -->
	<div class="no-print" style="background-color:#301548;">
		<div class="container__centered no-pad">
			<div class="horizontal-menu text--white">
				<a class="h-menu-item" href="#ApEfpc">A Personalized EHR</a>
				<div class="h-menu-seperator"></div>
				<a class="h-menu-item" href="#NSatET">The Expanse Theatre</a>
				<div class="h-menu-seperator"></div>
				<a class="h-menu-item" href="#E=bp">Boundless Possibilities</a>
				<div class="h-menu-seperator"></div>
				<a class="h-menu-item" href="#Lfeoatt">Transform Together</a>
				<div class="h-menu-seperator"></div>
				<a class="h-menu-item" href="#MCotHMS">The Main Stage</a>
			</div>
		</div>
	</div>
	<!-- End Horizontal Navigation Bar -->


	<!-- Start Mobile Navigation Bar -->
	<div class="no-print" style="background-color: #301548;">
		<div class="container__centered no-pad">
			<div class="accordion" style="margin-top: 0;">
				<ul class="accordion__list dropdown-menu">
					<li class="accordion__list__item" style="margin-bottom:0; background-color: #301548; text-align:center;">
						<a class="accordion__link" href="#" style="color: #fff; font-size:1.5em; font-weight: 600;">HIMSS22 Menu<div class="accordion__list__control"></div></a>
						<div class="accordion__dropdown">
							<ul class="mobile-menu">
								<li><a href="#ApEfpc">A Personalized EHR</a></li>
								<li><a href="#NSatET">The Expanse Theatre</a></li>
								<li><a href="#E=bp">Boundless Possibilities</a></li>
								<li><a href="#Lfeoatt">Transform Together</a></li>
								<li><a href="#MCotHMS">The Main Stage</a></li>
							</ul>
							<a style="color: #fff;" class="js__accordion__toggle accordion__close" href="#"><i class="fa fa-minus-circle"></i>&nbsp; Close Menu</a>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- End Mobile Navigation Bar -->


	<!-- START Background Gradient -->
	<div class="bg-gradient">


		<!-- Block 2 -->
		<div class="container pad-adjust">
			<div class="container__centered center text--white">
				<div class="auto-margins" style="margin-bottom: 1em;">
					<h2>A personalized EHR for personalized care.</h2>
					<p>No two healthcare networks, clinicians, or patients are the same. See MEDITECH <a href="<?php print $url; ?>/ehr-solutions/meditech-expanse">Expanse</a> in action and discover how our EHR can evolve with the needs of your organization, your providers, and your community.</p>
					<div style="margin-top:1.5em;">
						<?php hubspot_button('e6d834f0-61c2-419a-b0cf-61dea48d2cb2', "Connect With Us At HIMSS"); ?>
					</div>
				</div>
				<div class="container no-pad">
					<div class="box-item">
						<p><a href="<?php print $url; ?>/ehr-solutions/expanse-for-physicians">Expanse for Physicians</a></p>
						<p class="no-margin--bottom">Focus on your patients, not your EHR.</p>
					</div>
					<div class="box-item">
						<p><a href="<?php print $url; ?>/ehr-solutions/meditech-nursing">Expanse Point of Care</a></p>
						<p class="no-margin--bottom">Keep your patients safe, even when you’re on the move.</p>
					</div>
					<div class="box-item">
						<p><a href="<?php print $url; ?>/ehr-solutions/expanse-patient-connect">Expanse Patient Connect</a></p>
						<p class="no-margin--bottom">Meet patients where they are.</p>
					</div>
				</div>
				<img class="gradient-line--hr" src="<?php print $url; ?>/sites/all/themes/meditech/images/events/GettyImages-1278560410.gif" alt="Horizontal rule">
				<div class="container no-pad">
					<div class="box-item">
						<p><a href="<?php print $url; ?>/ehr-solutions/meditech-interoperability">Traverse</a></p>
						<p class="no-margin--bottom">Put people back at the center of healthcare.</p>
					</div>
					<div class="box-item">
						<p><a href="<?php print $url; ?>/ehr-solutions/care-compass">Expanse Care Compass</a></p>
						<p class="no-margin--bottom">Proactively manage your patient populations.</p>
					</div>
					<div class="box-item">
						<p><a href="<?php print $url; ?>/ehr-solutions/meditech-business-clinical-analytics">Business and Clinical Analytics</a></p>
						<p class="no-margin--bottom">Manage big data for organizational success.</p>
					</div>
				</div>
				<img class="gradient-line--hr" style="transform: scaleX(-1);" src="<?php print $url; ?>/sites/all/themes/meditech/images/events/GettyImages-1278560410.gif" alt="Horizontal rule">
				<div class="container no-pad">

					<div class="box-item">
						<p><a href="<?php print $url; ?>/ehr-solutions/expanse-ambulatory">Expanse Ambulatory</a></p>
						<p class="no-margin--bottom">Position your practice for the future.</p>
					</div>
					<div class="box-item">
						<p><a href="<?php print $url; ?>/ehr-solutions/meditech-professional-services">Professional Services</a></p>
						<p class="no-margin--bottom">Bring your EHR to its fullest potential.</p>
					</div>
					<div class="box-item">
						<p><a href="<?php print $url; ?>/ehr-solutions/meditech-genomics">Expanse Genomics</a></p>
						<p class="no-margin--bottom">The future of medicine. Right now.</p>
					</div>
				</div>
			</div>
		</div>
		<!-- END Block 2 -->


		<!-- START Block 3 -->
		<div class="container text--white pad-adjust">
			<div class="container__centered">
				<div class="center">
					<h2>Now Showing at the Expanse Theater</h2>
					<p>Catch a preview of MEDITECH's latest Expanse EHR solutions. No registration required.</p>
				</div>
				<div class="container grid-layout">
					<div class="grid-item-1">
						<p class="header-micro">Tuesday, March 15 | 1:00 p.m.<br> Wednesday, March 16 | 1:00 P.M.</p>
						<h3>A New View of Ambulatory Care with Expanse </h3>
						<p>Join us for a high-level look at our uniquely intuitive and efficient Ambulatory EHR. Expanse combines the top-rated EHR for clinician usability and personalization with a powerful, integrated practice management platform. See how <a href="<?php print $url; ?>/ehr-solutions/expanse-ambulatory">Expanse Ambulatory</a> lets you get back to practicing medicine.</p>
						<p class="bold no-margin">Presenter:</p>
						<ul style="margin-left:1em;">
							<li>Sandra Greene, Marketing Solutions Manager, MEDITECH</li>
						</ul>
					</div>
					<div class="grid-item-2">
						<p class="header-micro">Tuesday, March 15 | 1:30 p.m.<br> Wednesday, March 16 | 1:30 P.M.</p>
						<h3>Expanse Care Compass: Helping Clinicians See What Matters Most About their Patient Population</h3>
						<p>MEDITECH's <a href="https://ehr.meditech.com/ehr-solutions/care-compass">Expanse Care Compass</a> takes care management to the next level, providing an integrated solution tailored to the role of the care manager. Learn how Care Compass consolidates the powerful tools your care managers need to access and operationalize the information they demand. We'll take a tour through our new Expanse offering and explore how this will enable care managers to effectively manage and care for their patient population(s).</p>
						<p class="bold no-margin">Presenter:</p>
						<ul style="margin-left:1em;">
							<li>Christine Silva, Director, Strategy, MEDITECH</li>
						</ul>
					</div>
					<div class="grid-item-3">
						<p class="header-micro">Tuesday, March 15 | 2:00 p.m.<br> Wednesday, March 16 | 11:30 A.M.</p>
						<h3>Reimagining Precision Medicine with Expanse Genomics</h3>
						<p>Imagine a world where discrete genomic data is completely interoperable and genetic mutations drive clinician decisions at the point of care. MEDITECH's cutting-edge Genomics solution is the first of its kind. Come see how <a href="https://ehr.meditech.com/ehr-solutions/meditech-genomics">Expanse Genomics</a> is transforming the practice of healthcare in every setting.</p>
						<p class="bold no-margin">Presenters:</p>
						<ul style="margin-left:1em;">
							<li>Marsha Fearing, MD, MPH, MMSc, Biochemical Genetics Specialist, MEDITECH</li>
							<li>Jennifer Ford, Product Manager, Strategy, MEDITECH</li>
							<li>Anna Dover, Director, Product Management, First Databank
								<p style="margin-left:1em; margin-top:1em;"><em>Anna Dover is the Director of Product Management at FDB. She has a doctorate in pharmacy (PharmD), is a board-certified pharmacotherapy specialist (BCPS), and completed a pharmacogenomics certificate program through the American Society of Health-System Pharmacists (ASHP).</em></p>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- END Block 3 -->


		<!-- START Block 4 -->
		<div class="container pad-adjust bg--white-dots">
			<div class="container__centered text--white">
				<div class="auto-margins center" style="margin-bottom: 2em;">
					<h2>Expanse = boundless possibilities.</h2>
					<p>Learn from our clinician experts as they share first hand how Expanse improves mobility, usability, and efficiency for their staff and patients. Visit our booth to hear their stories.</p>
				</div>
				<div class="container no-pad">
					<div class="container__one-third">
						<div class="shadow-box w-headshot-outline">
							<div class="headshot">
								<img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/Joe-Farr.png" alt="Joe Farr head shot">
							</div>
							<div class="quote-text">
								<p class="italic text--large">"Expanse puts all the right nursing tools at my fingertips."</p>
								<p class="bold no-margin--bottom">Joe Farr, RN</p>
								<p class="no-margin--bottom">Clinical Applications Coordinator</p>
								<p>Kings Daughters Medical Center</p>
							</div>
							<div class="headshot tweet">
								<a href="https://twitter.com/intent/tweet?url=https%3A%2F%2Fehr.meditech.com%2Fevents%2Fmeditech-at-himss22&via=MEDITECH&text=%22Expanse%20puts%20all%20the%20right%20nursing%20tools%20at%20my%20fingertips.%22%20Joe%20Farr%2C%20RN%2C%20Clinical%20Applications%20Coordinator%2C%20Kings%20Daughters%20Medical%20Center&hashtags=HIMSS22" class="tweet-btn" target="_blank"><i class="fab fa-twitter"></i>&nbsp; Tweet this</a>
							</div>
						</div>
					</div>
					<div class="container__one-third">
						<div class="shadow-box w-headshot-outline">
							<div class="headshot">
								<img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/Priscilla-Frase.png" alt="Dr. Priscilla Frase head shot">
							</div>
							<div class="quote-text">
								<p class="italic text--large">“Expanse helps us evolve in the ways we need to for our patients.”</p>
								<p class="bold no-margin--bottom">Priscilla Frase, MD</p>
								<p class="no-margin--bottom">Hospitalist / CMIO</p>
								<p>Ozarks Healthcare</p>
							</div>
							<div class="headshot tweet">
								<a href="https://twitter.com/intent/tweet?url=https%3A%2F%2Fehr.meditech.com%2Fevents%2Fmeditech-at-himss22%20%23HIMSS22&via=MEDITECH&text=%22Expanse%20helps%20us%20evolve%20in%20the%20ways%20we%20need%20to%20for%20our%20patients.%22%20Priscilla%20Frase%2C%20MD%2C%20Hospitalist%20/%20CMIO%2C%20Ozarks%20Healthcare&hashtags=HIMSS22" class="tweet-btn" target="_blank"><i class="fab fa-twitter"></i>&nbsp; Tweet this</a>
							</div>
						</div>
					</div>
					<div class="container__one-third">
						<div class="shadow-box w-headshot-outline">
							<div class="headshot">
								<img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/Donnie-Dykes.png" alt="Donnie Dykes head shot">
							</div>
							<div class="quote-text">
								<p class="italic text--large">"Expanse has allowed me to navigate the expanse of patient care easily and efficiently."</p>
								<p class="bold no-margin--bottom">Donnie Dykes, RN</p>
								<p class="no-margin--bottom">Manager, Inpatient Applications</p>
								<p>Woman's Hospital</p>
							</div>

							<div class="headshot tweet">
								<a href="https://twitter.com/intent/tweet?url=https%3A%2F%2Fehr.meditech.com%2Fevents%2Fmeditech-at-himss22&via=MEDITECH&text=%22Expanse%20has%20allowed%20me%20to%20navigate%20the%20expanse%20of%20patient%20care%20easily%20and%20efficiently.%22%20Donnie%20Dykes%2C%20RN%2C%20Manager%2C%20Inpatient%20Applications%2C%20Woman%27s%20Hospital&hashtags=HIMSS22" class="tweet-btn" target="_blank"><i class="fab fa-twitter"></i>&nbsp; Tweet this</a>
							</div>

						</div>
					</div>
				</div>
				<div class="container no-pad">
					<div class="container__one-third">
						<div class="shadow-box w-headshot-outline">
							<div class="headshot">
								<img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/Andy-Burchett.png" alt="Andy Burchett head shot">
							</div>
							<div class="quote-text">
								<p class="italic text--large">“We strive to remove friction from our daily routines. Expanse enables us by adapting to our preferences, our patients, and our optimal workflows.”</p>
								<p class="bold no-margin--bottom">Andy Burchett, DO</p>
								<p class="no-margin--bottom">CMIO</p>
								<p>Avera Health</p>
							</div>
							<div class="headshot tweet">
								<a href="https://twitter.com/intent/tweet?url=https%3A%2F%2Fehr.meditech.com%2Fevents%2Fmeditech-at-himss22&via=MEDITECH&text=%22We%20strive%20to%20remove%20friction%20from%20our%20daily%20routines.%20Expanse%20enables%20us%20by%20adapting%20to%20our%20preferences%2C%20our%20patients%2C%20and%20our%20optimal%20workflows.%22%20Andy%20Burchett%2C%20DO%2C%20CMIO%2C%20Avera%20Health&hashtags=HIMSS22" class="tweet-btn" target="_blank"><i class="fab fa-twitter"></i>&nbsp; Tweet this</a>
							</div>
						</div>
					</div>
					<div class="container__one-third">
						<div class="shadow-box w-headshot-outline">
							<div class="headshot">
								<img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/Daniel-Neves.png" alt="Dan Neves head shot">
							</div>
							<div class="quote-text">
								<p class="italic text--large">"As a clinical informaticist, Expanse has allowed lots of flexibility to customize and improve our documentation according to our hospital's policy, especially around the ever-changing COVID era."</p>
								<p class="bold no-margin--bottom">Dan Neves, RN</p>
								<p class="no-margin--bottom">Clinical Informaticist</p>
								<p>Lawrence General Hospital</p>
							</div>
							<div class="headshot tweet">
								<a href="https://twitter.com/intent/tweet?url=https%3A%2F%2Fehr.meditech.com%2Fevents%2Fmeditech-at-himss22&via=MEDITECH&text=%22Expanse%20provides%20the%20flexibility%20to%20customize%20and%20improve%20our%20documentation%20according%20to%20our%20hospital%27s%20policy%2C%20especially%20around%20COVID.%22%20-Dan%20Neves%2C%20RN%2C%20Clinical%20Informaticist%2C%20Lawrence%20General%20Hospital&hashtags=HIMSS22" class="tweet-btn" target="_blank"><i class="fab fa-twitter"></i>&nbsp; Tweet this</a>
							</div>
						</div>
					</div>
					<div class="container__one-third">
						<div class="shadow-box w-headshot-outline">
							<div class="headshot">
								<img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/John-Pelzel.png" alt="John Pelzel head shot">
							</div>
							<div class="quote-text">
								<p class="italic text--large">"MEDITECH has helped me with increased efficiency in chart review and information gathering, allowing me more time to focus on patient care and interactions."</p>
								<p class="bold no-margin--bottom">John Pelzel, MD </p>
								<p class="no-margin--bottom">Family Practice Physician</p>
								<p>Sleepy Eye Medical Center</p>
							</div>
							<div class="headshot tweet">
								<a href="https://twitter.com/intent/tweet?url=https%3A%2F%2Fehr.meditech.com%2Fevents%2Fmeditech-at-himss22&via=MEDITECH&text=%22MEDITECH%20has%20helped%20me%20with%20increased%20efficiency%20in%20chart%20review%20and%20information%20gathering%2C%20allowing%20me%20more%20time%20to%20focus%20on%20patient%20care%20and%20interactions.%22%20John%20Pelzel%2C%20MD%2C%20Family%20Practice%20Physician%2C%20Sleepy%20Eye%20Medical%20Center&hashtags=HIMSS22" class="tweet-btn" target="_blank"><i class="fab fa-twitter"></i>&nbsp; Tweet this</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END Block 4 -->


		<!-- START Block 5 -->
		<div class="container text--white pad-adjust">
			<div class="container__centered">
				<div class="auto-margins center">
					<h2>Learn from each other and transform together.</h2>
					<p>We’re excited to take part in two HIMSS Global Conference events that highlight our ongoing commitment to collaboration.</p>
				</div>
			</div>
			<div class="container fw-pad2">
				<div class="gl-container">
					<div class="container__two-thirds bg--white-dots gl-text-pad" style="background-color: #1d2956;">
						<p class="header-micro">Interoperability Showcase</p>
						<h2>Connected care is better care.</h2>
						<p>With MEDITECH's <a href="<?php print $url; ?>/ehr-solutions/meditech-interoperability">Traverse interoperability solution</a>, you have all of the components needed to exchange information the way you want — with one touch, one view, and one step — following the patient story wherever it leads.</p>
						<p>Join us for this year’s <a href="https://www.himss.org/global-conference/exhibition-specialty-pavilions/himss-interoperability-showcase" target="_blank">Interoperability Showcase</a>, as we work alongside other CommonWell Health Alliance participants to demonstrate the power of connected care and the impact of competitive systems to improve clinical outcomes, quality, and patient experience.</p>
						<p class="bold">March 15-17 | Booth 8240</p>
					</div>
					<div class="container__one-third background--cover flex-order--reverse" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/events/connected-care-following-patient-journey.jpg); min-height:15em;"></div>
				</div>
			</div>
			<div class="container fw-pad2">
				<div class="gl-container">
					<div class="container__one-third background--cover flex-order--reverse" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/events/business-people-at-reception.jpg); min-height:15em;"></div>
					<div class="container__two-thirds bg--white-dots gl-text-pad" style="background-color: #301548;">
						<p class="header-micro">CMIO Roundtable</p>
						<h2>Developing a forward-looking mobility strategy.</h2>
						<p>We’re excited to sponsor the <a href="https://www.himss.org/global-conference/session-himss-cmio-roundtable-0" target="_blank">CMIO Roundtable</a>, a unique opportunity for clinician leaders to share their understanding of how technology can play an important role as part of an integrated care team.</p>
						<p>Join your peers for this interactive session featuring Dr. Andy Burchett, CMIO, Avera Health, Dr. Eve Cunningham, CMO Strategic Partnerships at Providence Health, and Dr. Jim Jirjis, Chief Health Information Officer, HCA Healthcare, and discover how they’re leveraging mobility and accelerating their journey toward digital transformation.</p>
						<p class="bold">Tuesday, March 15 | 4:15 p.m. | W340A</p>
					</div>
				</div>
			</div>
		</div>
		<!-- END Block 5 -->


		<!-- START Block 6 -->
		<div class="container text--white">
			<div class="container__centered">
				<div class="center">
					<h2>MEDITECH Customers on the HIMSS22 Main Stage</h2>
					<p>See who will be sharing their strategies with the HIMSS community.</p>
				</div>
				<div class="container no-pad--bottom grid-2x2">
					<div>
						<p class="header-micro">Monday, March 14 | 11:15 a.m. | W311A</p>
						<h3><a href="https://www.himss.org/global-conference/session-balancing-documentation-burden-need-discrete-data-elements">Balancing Documentation Burden with the Need for Discrete Data Elements</a></h3>
						<ul>
							<li>Jane Englebright, PhD, RN, FAAN, Senior Vice President & Chief Nurse Executive, HCA Healthcare</li>
						</ul>
						<p><em>Note: This session is part of the <a href="https://www.himss.org/global-conference/program-nursing-informatics-symposium">Nursing Informatics Symposium</a> and requires an additional fee and registration.</em></p>
					</div>
					<div>
						<p class="header-micro">Wednesday, March 16 | 2:30 p.m. | W414A</p>
						<h3><a href="https://www.himss.org/global-conference/session-harnessing-data-advance-quality-care">Harnessing Data to Advance Quality of Care</a></h3>
						<ul>
							<li>Jackie Rice, BSN, RN, Vice President and CIO, Frederick Health</li>
							<li>Robert Wack, MD, Chief Medical Information Officer, Frederick Memorial Hospital</li>
						</ul>
					</div>
					<div>
						<p class="header-micro">Thursday, March 17 | 8:30 a.m. | W206A</p>
						<h3><a href="https://www.himss.org/global-conference/session-remote-patient-monitoring-reaching-out-and-breaking-through-0">Remote Patient Monitoring: Reaching Out and Breaking Through</a></h3>
						<ul>
							<li>Andy Burchett, DO, Chief Medical Information Officer, Chair of Family Medicine, Avera Health</li>
						</ul>
					</div>
					<div>
						<p class="header-micro">Thursday, March 17 | 1:00 p.m. | W206A</p>
						<h3><a href="https://www.himss.org/global-conference/session-helping-physicians-prepare-genomics-revolution">Helping Physicians Prepare for the Genomics Revolution</a></h3>
						<ul>
							<li>Marsha Fearing, MD, MPH, MMSc, Clinical Medical and Biochemical Genetics Physician Consultant, Harvard Medical School</li>
						</ul>
					</div>
				</div>

			</div>
		</div>
		<!-- END Block 6 -->


		<!-- START Bock 7 -->
		<div class="container text--white">
			<div class="container__centered center">
				<div style="margin-bottom:2em;">
					<?php print $share_link_buttons; ?>
				</div>
			</div>
		</div>
		<!-- END Block 7 -->


		<!-- END Background Gradient -->
	</div>

</div>
<!-- end js__seo-tool__body-content -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>

<!-- END events-simple-page--node-4217.php MEDITECH at HIMSS22 -->
