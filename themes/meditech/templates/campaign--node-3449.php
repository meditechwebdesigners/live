<!-- START campaign--node-3449.php -->
<?php // This template is set up to control the display of the Expanse content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
	.headshot-circular {
		margin: 0 auto;
		max-height: 262px;
		width: 100%;
		border-radius: 50%;
	}

	section,
	article {
		padding-top: 2em;
		padding-bottom: 2em;
		max-width: 64em;
		margin-left: auto;
		margin-right: auto;
	}

	.fa-check-square:before {
		content: "\f14a";
		color: rgb(0, 0, 0);
	}

	.fa-ul {
		list-style-type: none;
		margin-left: 2.5em;
		padding-left: 0;
	}

	.fa-star:before {
		content: "\f005";
		color: rgb(255, 255, 255);
	}





	@media all and (max-width: 50em) {

		.pie-chart--max-width {
			max-width: 25em;
		}
	}

	@media all and (max-width: 800px) {
		.container__one-fourth {
			margin-right: 2%;
			width: 48%;
			margin-top: 2em;
		}
	}

	@media all and (max-width: 500px) {
		.container__one-fourth {
			margin-right: 0;
			width: 100%;
			margin-top: 2em;
		}
	}

</style>


<div class="js__seo-tool__body-content">

	<!-- Block 1 -->

	<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/physician-high-fiving-child-diverse-expanse-success.jpg);">
		<div class="container__centered">
			<div class="container center">
				<h1 class="js__seo-tool__title" style="display:none;">MEDITECH Expanse means limitless possibilities</h1>
				<img style="padding:0,2em,2em,2em; margin-bottom: 2em;" src="<?php print $url; ?>/sites/all/themes/meditech/images/MEDITECH-expanse-logo--green-white.svg" alt="MEDITECH Expanse Logo">
			</div>
			<div class="container text--white center auto-margins" style="background-color: rgba(0,0,0,.7); max-width: 40em; padding-bottom: 1em; padding-left: 2em; padding-right: 2em; border-radius:.5em; ">
				<p>
					Healthcare technology is all about possibilities. How can we be healthier, happier, and more connected in a world that is so unpredictable?
				</p>
				<p>
					MEDITECH Expanse is the EHR for a changing world. It evolves along with the needs of your organization, your providers, and your community. So you can facilitate better business practices. And reduce the burdens on your clinicians. And most importantly, improve patient outcomes. No matter your size or budget, your goals are possible with the right technology partner at your side.
				</p>

				<div class="center" style="margin-top:2em;">
					<?php hubspot_button($cta_code, "Spark Video"); ?>
				</div>

			</div>

		</div>
	</div>
	<!-- End of Block 1 -->

	<!-- Start of VIdeo Block -->

	<div class="content__callout">
		<div class="content__callout__media">
			<div class="content__callout__image-wrapper">
				<div class="video js__video" data-video-id="490908808">
					<figure class="video__overlay">
						<img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/dandelion-possibilities--EXP.jpg" alt="MEDITECH Possibilities - Video Covershot">
					</figure>
					<a class="video__play-btn" href="https://vimeo.com/meditechehr/review/490908808/db2c16c946"></a>
					<div class="video__container"></div>
				</div>
			</div>
		</div>
		<div class="content__callout__content">
			<div class="content__callout__body">
				<div class="content__callout__body__text">
					<h2>MEDITECH Expanse means limitless possibilities</h2>
					<p>With Expanse, people are at the center of care. So even when healthcare doesn’t go in a straight line, the needs of clinicians and patients are always met.</p>
				</div>
			</div>
		</div>
	</div>

	<!-- End of Video Block -->


	<!-- Block 2 -->

	<div class="container bg--white">
		<div class="container__centered">

			<div class="center auto-margins" style="margin-bottom: 1em;">
				<figure style="padding-bottom: 1em;">
					<img class="headshot-circular" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Helen-Waters--headshot.jpg" style="width:150px;" alt="Helen Waters Headshot">
				</figure>
				<div class="text--large italic">
					<p>"Healthcare is different now, just as the world itself is different. And technology will be the key to serving the needs of patients and providers, as we undergo these transformational changes in work and living. At MEDITECH, we will continue to champion solutions that improve the accessibility of both data and care itself."</p>
				</div>
				<div>
					<p><span class="text--large bold text--meditech-green"> Helen Waters</span><br>
						Executive Vice President & COO, MEDITECH</p>
				</div>

			</div>

		</div>

	</div>

	<!-- End of Block 2 -->


	<!-- Start of Block 3 -->

	<div class="container no-pad">
		<div class="gl-container">

			<div class="container__one-half background--cover flex-order--reverse" style="background-image: url( https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/older-man-on-laptop-talking-to-doctor.jpg); min-height:550px; background-position: left;">
			</div>

			<div class="container__one-half gl-text-pad text--white bg--green-gradient">
				<h2>Embracing innovation and transformation</h2>

				<p>Digital transformation has taken healthcare out of medical buildings and into real life. Free your organization, your clinicians, and your patients from cookie-cutter approaches to care, and offer a mobile, affordable, personalized experience for everyone. </p>

				<p>With MEDITECH’s <a href="https://ehr.meditech.com/ehr-solutions/cloud-platform">Cloud Platform</a>, organizations of any size can extend and enhance their Expanse EHR to include multiple specialties, connect remote sites, and protect patient data. Maintain easy and convenient communication with your community (even at a distance) with <a href="https://ehr.meditech.com/ehr-solutions/virtual-care">Expanse Virtual Care</a> and our <a href="https://ehr.meditech.com/ehr-solutions/patient-engagement">Patient and Consumer Health Portal</a>. And give your providers the data they need during unexpected downtime, with <a href="https://ehr.meditech.com/ehr-solutions/digital-transformation">High Availability SnapShot</a>.</p>
			</div>


		</div>
	</div>

	<!-- End of Block 3 -->

	<!-- Block 4  -->
	<div class="container bg--white">
		<div class="container__centered text--black-coconut">

			<div style="margin-bottom:2em;">
				<h2>Keeping up with physicians and nurses</h2>
				<p>Physicians and nurses each have unique roles, but they must work together to coordinate safe, prompt care delivery. Equally robust EHR tools keep them untethered from clunky computers and moving in sync - while they can still see the information they need, exactly the way they want to see it. Give people the sense of connection they need, without the hassle.</p>
			</div>

			<div>
				<div class="container__two-thirds text--black-coconut">
					<div style="margin-top: 2em;">
						<h6 class="text--black-coconut"><strong>For Physicians</strong></h6>
						<ul class="fa-ul">
							<li><span class="fa-li"><i class="fas fa-check-square" aria-hidden="true"></i></span>
								Look your patients in the eye again, with <a href="https://ehr.meditech.com/ehr-solutions/virtual-assistant">Expanse Virtual Assistant</a>. This progressive technology responds to simple verbal commands by retrieving the information you’re looking for &mdash; without you having to type, point, click, or even touch your device.
							</li>
							<li><span class="fa-li"><i class="fas fa-check-square" aria-hidden="true"></i></span>
								<a href="https://ehr.meditech.com/ehr-solutions/expanse-for-physicians">Expanse Now</a> and our other <a href="https://www.youtube.com/watch?v=nrzC1Teszjs" target="_blank">physician solutions</a> provide secure, remote access to your Expanse EHR, using intuitive mobile device conventions and voice commands on your smartphone or tablet. With these options, you can remotely manage routine tasks, while communicating and coordinating care from the palm of your hand.
							</li>
						</ul>
					</div>
				</div>

				<div class="container__one-third mobile-smaller-image tablet-half center">

					<!-- Hidden Modal right -->

					<div id="modal24" class="modal">
						<a class="close-modal" href="javascript:void(0)">×</a>
						<div class="modal-content">
							<img src="https://ehr.meditech.com//sites/all/themes/meditech/images/campaigns/results-expanse-now-2--large-EXP.jpg" alt="Screenshot of Expanse Now Lab Results">
						</div>
					</div>

					<!-- End of Hidden Modal -->

					<!-- Start modal right trigger -->

					<div class="open-modal" data-target="modal24">
						<div class="phone--black">
							<img src="https://ehr.meditech.com//sites/all/themes/meditech/images/campaigns/results-expanse-now-2--small-EXP.jpg" alt="Screenshot of Expanse Now Lab Results">
						</div>
						<!-- Add modal trigger here -->
						<div class="mag-bg">
							<!-- Include if using image trigger -->
							<i class="mag-icon fas fa-search-plus"></i>
						</div>
					</div>

					<!-- End modal trigger -->
				</div>

			</div>

			<div style="margin-bottom: 2em">
				<div class="container__one-third mobile-smaller-image tablet-half center">

					<!-- Hidden Modal right -->

					<div id="modal29" class="modal">
						<a class="close-modal" href="javascript:void(0)">×</a>
						<div class="modal-content">
							<img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/screenshot-patient-data--large-EXP.jpg" alt="MEDITECH's Web Point of Care software - patient data">
						</div>
					</div>

					<!-- End of Hidden Modal -->

					<!-- Start modal left trigger -->

					<div class="open-modal" data-target="modal29">
						<div class="phone--black">
							<img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/screenshot-patient-data--small-EXP.jpg" alt="MEDITECH's Web Point of Care software - patient data">
						</div>
						<!-- Add modal trigger here -->
						<div class="mag-bg">
							<!-- Include if using image trigger -->
							<i class="mag-icon fas fa-search-plus"></i>
						</div>
					</div>

					<!-- End modal trigger -->
				</div>

				<div class="container__two-thirds text--black-coconut" style="margin-top: 2em;">
					<h6 class="text--black-coconut"><strong>For Nurses</strong></h6>
					<ul class="fa-ul">
						<li><span class="fa-li"><i class="fas fa-check-square" aria-hidden="true"></i></span>
							<a href="https://ehr.meditech.com/ehr-solutions/meditech-nursing">Expanse Point of Care software</a> gives you the flexibility to perform the most common interventions whenever and wherever they're needed, through a <a href="https://info.forwardadvantage.com/create-a-secure-clinical-mobile-device-workflow-for-meditech-expanse">device</a> that fits in your pocket. In addition, <a href="https://ehr.meditech.com/ehr-solutions/meditech-nursing">Expanse Patient Care</a> gives you everything you need to stay mobile with a tablet. These are technologies that you already know and love, as well as a win-win for you and your patients.
						</li>
					</ul>
				</div>
			</div>


		</div>
	</div>
	<!-- End of Block 4  -->


	<!-- Block 5 -->
	<div class="container no-pad">
		<div class="gl-container">
			<div class="container__one-half gl-text-pad text--white bg--green-gradient">
				<h2>Seeing the big picture with interoperability and population health</h2>
				<p>To truly connect with patients, you must also connect to their data. Having information at your fingertips means you can take action quickly before small issues become big ones.</p>
				<ul class="fa-ul">
					<li><span class="fa-li"><i class="fas fa-star" aria-hidden="true"></i></span>
						<p>Free-flowing information with <a href="https://ehr.meditech.com/ehr-solutions/meditech-interoperability">Traverse interoperability</a> means easy access to your patients’ medical histories, with just <strong>one touch, one view, and one step</strong>. MEDITECH also extends beyond local affiliations and regional HIEs to connect all practitioners using <a href="https://www.commonwellalliance.org/">CommonWell Health Alliance®</a> and a bridge to Carequality, which supports the exchange of member records with all major EHR vendors.</p>
					</li>
					<li><span class="fa-li"><i class="fas fa-star" aria-hidden="true"></i></span>
						<p><a href="https://ehr.meditech.com/ehr-solutions/meditech-population-health">Expanse Population Health</a> adds big picture clarity for tracking and delivering timely interventions to your at-risk populations. You'll have the functionality to support individual patients and help them manage health risks at every stage of life.</p>
					</li>
				</ul>
			</div>
			<div class="container__one-half background--cover flex-order--reverse" style="background-image: url( https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Asian-business-man-looking-at-phone-in-crowd.jpg); min-height:550px; background-position: center;">
			</div>
		</div>
	</div>
	<!-- End of Block 5 -->



	<!-- START of Block 6 - UPDATED KLAS BLOCK -->
	<style>
		.flex-cont {
			display: flex;
			justify-content: space-between;
			align-items: center;
			background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Nurse-Smiling-MEDITECH-KLAS-Awards--bg.jpg);
			min-height: 450px;
			background-position: left;
		}

		.gl-text-pad .button--hubspot {
			margin-bottom: 0;
		}

		.flex-image:nth-child(2) {
			margin: 0 1em;
		}

		@media all and (max-width: 550px) {
			.flex-cont {
				flex-wrap: wrap;
				background-image: none;
				justify-content: center;
			}

			.flex-image:nth-child(2) {
				margin: 0;
			}
		}

	</style>
	<div class="container no-pad">
		<div class="gl-container">

			<div class="container__one-half background--cover gl-text-pad flex-cont" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/subtle-light-shining-dark-bg.jpg);">
				<div class="flex-image" style="width: 250px;"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-KLAS-Award-2022--Acute-Care-EMR.png" alt="MEDITECH KLAS Award 2022 - Acute Care EMR"></div>
				<div class="flex-image" style="width: 250px;"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-KLAS-Award-2022--Patient-Accounting-and-Patient-Management.png" alt="MEDITECH KLAS Award 2022 - Patient Accounting and Patient Management"></div>
				<div class="flex-image" style="width: 250px;"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-KLAS-Award-2022--Home-Health.png" alt="MEDITECH KLAS Award 2022 - Home Health EHR"></div>
			</div>

			<div class="container__one-half bg--blue-gradient gl-text-pad">
				<h2>MEDITECH Expanse Earns Top KLAS Honors</h2>
				<p>MEDITECH Expanse ranked first in the 2022 Best in KLAS: Software &amp; Services report in three market segments, including Acute Care EMR (Community Hospital), Patient Accounting &amp; Patient Management (Community Hospital), and Home Health EHR (small 1-200 average daily census). This is the second straight year MEDITECH earned this recognition in all three segments. MEDITECH Expanse was also top performer for Overall Software Suite, Ambulatory EMR (>75 physicians), and Acute Care EMR (Large/IDN).</p>
				<p>The Best in KLAS designation is awarded only in those software and services market segments that have the broadest operational and clinical impact on healthcare organizations. This is the eighth consecutive year MEDITECH has been recognized by the global healthcare research firm.</p>
				<div style="margin-top:2em;">
					<?php hubspot_button('e23b73bd-6b14-4e22-a961-180ac2a22ef1', "Learn More"); ?>
				</div>
			</div>

		</div>
	</div>
	<!-- END of Block 6 - UPDATED KLAS BLOCK -->



	<!-- Block 7 -->

	<div class="container">
		<div class="container__centered" style="margin-top: 2em; margin-bottom: 2em;">

			<h2>Balancing quality with long-term sustainability</h2>
			<p>If you know what’s going on with your business, you can be there for patients now and in the future. Track your improvement efforts and ensure that the financial side of your organization is just as healthy as the clinical side.</p>

			<div class="flex-order--container" style="margin-top: 3em; margin-bottom: 3em;">
				<div class="container__one-half content--pad-right">
					<h3>Business and Clinical Analytics</h3>
					<p>Managing big data is critical to the health of your organization and your patients. Increase efficiency and measure progress with <a href="https://ehr.meditech.com/ehr-solutions/meditech-business-clinical-analytics">Business and Clinical Analytics.</a> Customizable analytics dashboards enable you to turn your clinical, operational, and financial data into a concrete plan of action. When the right information is at your fingertips, <a href="https://ehr.meditech.com/ehr-solutions/ehr-value-and-sustainability">maximizing revenues and controlling costs</a> is within your reach.</p>
				</div>

				<div class="container__one-half flex-order--reverse">
					<div class="bg-pattern--container">
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/physician-and-cio-on-laptop-in-meeting.jpg" alt="doctor speaking with analyst">
						<div class="bg-pattern--green-dots bg-pattern--right"></div>
					</div>
				</div>
			</div>

			<div>

				<div class="container__one-half">
					<div class="bg-pattern--container">
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/male-patient-checking-in-at-reception-desk--EXP-Success.jpg" alt="woman on computer tablet">
						<div class="bg-pattern--green-dots bg-pattern--left"></div>
					</div>
				</div>

				<div class="container__one-half content--pad-left">
					<h3>Revenue Cycle</h3>
					<p>
						With <a href="https://ehr.meditech.com/ehr-solutions/meditechs-revenue-cycle">MEDITECH's Revenue Cycle</a>, you can automate your collection processes to get bills out quickly and maximize your reimbursement potential in all care settings. The improved cash flow will enable you to make sensible business decisions and investments - so you can attract and retain patients, pursue the latest technologies, upgrade equipment, and hire enough staff to keep it all humming along.
					</p>
				</div>
			</div>

		</div>
	</div>

	<!-- End of Block 7  -->

	<!-- Start of block 8 -->
	<div class="container bg--green-gradient">
		<div class="container__centered">
			<div class="container no-pad">
				<div class="center auto-margins">
					<h2>
						Stop small problems from becoming big ones.
					</h2>
					<p>
						Early intervention can save lives. Expanse empowers providers to detect subtle changes in a patient's condition, and take action before they become harder to manage. For example, using <a href="https://ehr.meditech.com/ehr-solutions/meditech-ehr-excellence-toolkits">MEDITECH's EHR Toolkits</a> (e.g. Sepsis Management, CAUTI reduction) in tandem with the Quality and Surveillance solution, our customers have experienced the following results:
					</p>
				</div>
			</div>
			<div class="container no-pad">
				<div class="gl-container" style="background-color: inherit;">
					<div class="container__one-third center">
						<img class="pie-chart--max-width" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/pie-chart-81--white-with-text.svg" alt="Pie chart showing 81%">
						<p>
							<strong>81%</strong> increase in Sep-1 core measure compliance by <a href="https://info.meditech.com/case-study-hilo-medical-center-improves-sep-1-compliance-by-34-percent-using-meditechs-expanse-ehr?">Hilo Medical Center</a>
							(Hilo, HI), from 42% to 76%
						</p>
					</div>
					<div class="container__one-third center">
						<img class="pie-chart--max-width" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/pie-chart-65--white-with-text.svg" alt="Pie chart showing 65%">
						<p>
							<strong>65%</strong> improvement in Sepsis mortality rates and <strong>150%</strong> improvement in Sep-1 core measure compliance for <a href="https://info.meditech.com/case-study-frederick-memorial-hospital-reduces-sepsis-mortality-rate-by-65-percent-with-meditech-0-0">Frederick Memorial Hospital</a> (Frederick, MD)
						</p>
					</div>
					<div class="container__one-third center">
						<img class="pie-chart--max-width" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/pie-chart-45--white-with-text.svg" alt="Pie chart showing 45%">
						<p>
							<strong>45%</strong> decrease in CAUTI cases and a <strong>35%</strong> reduction in indwelling catheter days for <a href="https://info.meditech.com/case-study-rcch-healthcare-partners-uses-meditech-toolkit-and-surveillance-solution-to-combat-cauti-0">RCCH HealthCare Partners</a> (Brentwood, TN)
						</p>
					</div>
				</div>

			</div>
		</div>
	</div>
	<!-- End of block 8 -->

	<!-- Start of block 9 -->

	<div class="container background--cover" style="background-image:url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/bg--organic-shapes-3.svg);">
		<div class="container__centered center" style="text-align: left;">
			<div class="auto-margins" style="text-align: center; margin-bottom:2em;">
				<h2>The proof is in the numbers.</h2>
				<p>
					According to a <a href="https://www.meditech.com/productbriefs/flyers/navinhafftyqualitystudy.pdf">recent study</a> by Navin Haffty and Associates, a greater percentage of Expanse hospitals received positive Value-Based Purchasing adjustments compared to those using other leading vendor EHRs. And these programs are having an impact.
				</p>
			</div>

			<div class="container__one-third shadow-box">
				<div class="center">
					<img style="max-width: 100px;" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/mobile--icon-MT-green.svg" alt="mobile icon">
				</div>
				<p><a href="https://ehr.meditech.com/news/improving-the-patient-experience-at-kings-daughters-medical-center-video">King’s Daughters Medical Center</a> (Brookhaven, MS) uses MEDITECH's mobile solutions to deliver more efficient care, reduce wait times, improve as well as increase the number of patients being treated.</p>
			</div>

			<div class="container__one-third shadow-box">
				<div class="center">
					<img style="max-width: 100px;" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/analytics--icon-MT-green.svg" alt="Analytics icon">
				</div>
				<p>
					Deborah Heart and Lung Center (Brown Mills, NJ) uses MEDITECH's Business and Clinical Analytics to target specific improvement areas. The organization <a href="https://info.meditech.com/case-study-deborah-heart-and-lung-optimizes-patient-throughput-with-meditech-businsess-and-clinical-analytics">increased discharges by 10 a.m. sixfold</a>, thereby improving patient throughput and satisfaction.
				</p>
			</div>

			<div class="container__one-third shadow-box">
				<div class="center">
					<img style="max-width: 100px;" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/diabetes--icon-MT-green.svg" alt="diabetes icon">
				</div>
				<p>
					See how <a href="https://cdn2.hubspot.net/hubfs/2897117/WPs,%20Case%20Studies,%20Special%20Reports/CustomerLeaders_GoldenValley_BCA.pdf">Golden Valley Memorial Healthcare</a> (Clinton, MO) saved its Health Information Management (HIM) staff approximately 140 hours per month.
				</p>
			</div>

		</div>

	</div>

	<!-- End of Block 9 -->



</div>
<!-- end js__seo-tool__body-content -->

<!-- Call to Action -->
<div class="container bg--black-coconut text--white">
	<div class="container__centered center">

		<?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
		<h2 class="text--white">
			<?php print $cta->field_header_1['und'][0]['value']; ?>
		</h2>
		<?php } ?>

		<?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
		<div>
			<?php print $cta->field_long_text_1['und'][0]['value']; ?>
		</div>
		<?php } ?>

		<div class="center" style="margin-top:2em;">
			<?php hubspot_button($cta_code, "Download the Innovators Booklet"); ?>
		</div>

		<div style="margin-top:1em;">
			<?php print $share_link_buttons; ?>
		</div>

	</div>
</div>
<!-- End Call to Action -->


<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>
<!-- END campaign--node-3449.php -->
