<!-- START campaign--node-2735.php -->

<?php // This template is set up to control the display of the MEDITECH Interoperability content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
    .circle-icon {
        height: 75px;
        width: 75px;
        color: #fff;
        display: flex;
        justify-content: center;
        align-items: center;
        border-radius: 50%;
        margin: 0 auto;
        margin-bottom: 1em;
    }

    .headshot-border {
        max-width: 150px;
        max-height: 150px;
        border: 6px solid #e6e9ee;
        border-radius: 50%;
        margin-left: auto;
        margin-right: auto;
        margin-bottom: 1em;
    }

    .paragraph-margin {
        margin-left: -2em;
        margin-right: 2em;
    }

    @media screen and (min-width: 50em) and (max-width: 56.250em) {
        .gl-container h2 {
            font-size: 1.2em;
            line-height: 1.40625em;
        }
    }

    @media screen and (max-width: 50em) {
        .paragraph-margin {
            margin: .5em 0em 0em 0em;
            text-align: center;
        }
    }

    .small_popup_video {
        width: 100%;
        border: 4px solid #e6e9ee;
        border-radius: 50%;
    }

    .flip-horizontal--bg,
    .flip-horizontal--content {
        -moz-transform: scaleX(-1);
        -o-transform: scaleX(-1);
        -webkit-transform: scaleX(-1);
        transform: scaleX(-1);
        filter: FlipH;
        -ms-filter: "FlipH";
    }

    .flip-horizontal--content {
        margin-left: 30%;
    }

    .margin-top {
        margin-top: 3.5em;
    }

    @media all and (max-width: 50em) {

        .flip-horizontal--bg,
        .flip-horizontal--content {
            -moz-transform: none;
            -o-transform: none;
            -webkit-transform: none;
            transform: none;
            filter: none;
            -ms-filter: none;
        }

        .flip-horizontal--content {
            margin-left: 0;
        }

        .margin-top {
            margin-top: inherit;
        }
    }

</style>

<div class="js__seo-tool__body-content">

    <!-- Start Block 1 -->
    <div class="container no-pad background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/doctor-talking-to-couple-wth-tablet-in-office.jpg); background-position: right;">
        <div class="container__centered bg-overlay--white" style="padding:2em;">
            <div class="container__two-thirds transparent-overlay--white" style="background-color:rgba(223, 219, 201, 0.8); padding: 1em 2em;">
                <h1 class="js__seo-tool__title">Stay connected with Traverse, MEDITECH's interoperability solution.</h1>
                <p>Interoperability isn't really about clicks, or C-CDAs, or interfaces, or data. It's about people and making their lives better. MEDITECH's Traverse interoperability solution, available with our Expanse EHR, puts people back at the center of healthcare, by bridging the gaps that can happen during care transitions.</p>

                <div style="margin-top:1em; margin-bottom:2em;">
                    <?php hubspot_button($cta_code, "Download The Union Hospital Interoperability Case Study"); ?>
                </div>

            </div>
        </div>
    </div>
    <!-- End Block 1 -->


    <!-- Start Block 2 -->
    <div class="container bg--blue-gradient gl-container np-pad--bottom">
        <div class="container__one-half">
            <div class="video js__video" data-video-id="359079478">
                <figure class="video__overlay">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-Traverse.jpg" alt="MEDITECH Traverse - Video Covershot">
                </figure>
                <a class="video__play-btn" href="https://vimeo.com/359079478"></a>
                <div class="video__container"></div>
            </div>
            <div style="padding:1em 4em;">
                <h2>What it means to traverse the healthcare landscape.</h2>
                <p>Healthcare is always moving and changing. Traverse is an interoperability solution that keeps clinicians and patients connected, no matter where the flow takes them.</p>
            </div>
        </div>
        <div class="container__one-half">
            <div class="video js__video" data-video-id="370872507">
                <figure class="video__overlay">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/CIO-Jeannette-Currie--video-overlay.jpg" alt="CIO Jeannette Currie - Video Covershot">
                </figure>
                <a class="video__play-btn" href="https://vimeo.com/370872507"></a>
                <div class="video__container"></div>
            </div>
            <div style="padding:1em 4em;">
                <h2>Beth Israel Lahey Health connects with One Touch</h2>
                <p>CIO Jeannette Currie talks about Beth Israel Lahey Health's interoperability strategy with Expanse, and how their information sharing improves the patient and physician experience. Additionally, check out our case study highlighting how <a href="https://info.meditech.com/hubfs/WPs,%20Case%20Studies,%20Special%20Reports/CustomerSuccess_BethIsrael_One_Touch.pdf">MEDITECH's One Touch</a> keeps their clinicians and patients connected.</p>
            </div>
        </div>
    </div>
    <!-- End Block 2 -->


    <!-- Start Block 3 -->
    <div class="container">
        <div class="container__centered">
            <div class="auto-margins center text--dark-blue">
                <h2>Interoperability in three acts.</h2>
                <p>With Traverse, you can coordinate care in your community with:</p>
            </div>
            <div class="container" style="padding: 1.5em 0em 1.5em 0em;">

                <div class="gl-container">
                    <div class="container__one-third bg--dark-blue">
                        <p class="center"><span style="font-size:2.5em;">One View</span><br>
                            Share Clinical Data</p>
                        <hr />
                        <ul>
                            <li>Patient Summaries</li>
                            <li>Referral Notes</li>
                            <li>Discharge Summaries</li>
                        </ul>
                    </div>
                    <div class="container__one-third bg--light-gray text--dark-blue">
                        <p class="center"><span style="font-size:2.5em;">One Touch</span><br>
                            Record Access</p>
                        <hr style="border-bottom:1px solid #0075a8;" />
                        <ul>
                            <li>Secure EHR-to-EHR link</li>
                            <li>Embedded directly into clinical workflow</li>
                            <li>NO searching, NO signing on</li>
                        </ul>
                    </div>
                    <div class="container__one-third bg--dark-blue">
                        <p class="center"><span style="font-size:2.5em;">One Step</span><br>
                            Exchange Orders and Results with acute hospitals</p>
                        <hr />
                        <ul>
                            <li>Receive orders from clinicians throughout your community</li>
                            <li>Results link back to the originating order</li>
                            <li>Patient Record Smart Searches</li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- End Block 3 -->


    <!-- Start Block 4 -->
    <?php if( user_is_logged_in() ){ ?>
    <div class="container bg--blue-gradient">
        <div class="container__centered auto-margins">
            <div class="container no-pad">
                <h2>Traverse Exchange Canada bridges communication across communities.</h2>
                <p>Traverse Exchange Canada is a new cloud-based interoperability solution that enables the free flow of health information from multiple participating organizations and sends it to MEDITECH’s EHR and other EHRs that comply with interoperability standards. The solution provides clinicians with a more holistic view of their patients’ care. It will be available via a software as a service subscription model beginning in Ontario, with the intent to expand throughout Canada.</p>
            </div>
            <div class="container no-pad">
                <div class="container__one-half">
                    <ul class="fa-ul">
                        <li><span class="fa-li"><i class="fas fa-angle-double-right text--white" aria-hidden="true"></i></span>Cloud-based technology seamlessly connects participating organizations to each other and to Ontario Health using MEDITECH’s record locator system.</li>
                        <li><span class="fa-li"><i class="fas fa-angle-double-right text--white" aria-hidden="true"></i></span>Once patients opt in, providers receive a comprehensive picture of their record directly within the MEDITECH EHR — regardless of where care was provided.</li>
                        <li><span class="fa-li"><i class="fas fa-angle-double-right text--white" aria-hidden="true"></i></span>Data is located beyond hospital walls, across jurisdictional or federal systems from a variety of sources.</li>
                        <li><span class="fa-li"><i class="fas fa-angle-double-right text--white" aria-hidden="true"></i></span>Data can be viewed or consumed within the EHR as discrete data.</li>
                    </ul>
                </div>
                <div class="container__one-half">
                    <img style="border-radius: 0.389em; margin-top:0.5em; max-width:450px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-Traverse-Exchange-Canada--IG.jpg" alt="MEDITECH Traverse Exchange Canada promo image">
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <!-- End Block 4 -->


    <!-- Start Block 5 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blurred-hospital-lobby.jpg);">
        <div class="container__centered">
            <div class="gl-container">
                <div class="container__one-third bg--green-gradient">
                    <h2>Interoperability is free-flowing information.</h2>
                    <p>Neither geographical boundaries nor healthcare organization affiliation should hinder a clinician's access to patient records. That's why, with Traverse, we extend interoperability efforts beyond local affiliations and regional HIEs to all connected practitioners using <a href="https://www.commonwellalliance.org/" target="_blank">CommonWell Health Alliance&reg;</a>. And to extend this reach even further, CommonWell Health Alliance provides the opportunity to connect with Carequality, supporting the exchange of member records with all major EHR vendors. To learn more about how MEDITECH is leveraging CommonWell to help our customers move forward with interoperability using FHIR technology, watch our AVP of Marketing Christine Parent’s most recent interview with <a href="https://www.youtube.com/watch?v=3Kj6ZuUUZKY" target="_blank">CommonWell TV</a>.</p>
                </div>
                <div class="container__two-thirds center">
                    <div class="headshot-border">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/brent-headshot.png" alt="Brent head shot">
                    </div>
                    <h3>Why does this matter? Consider Brent.</h3>
                    <p style="margin-bottom:1em;">Brent is playing hockey and blacks out after hitting his head on the ice. He is admitted to a local emergency department. Since both the ED and Brent's PCP connect to the CommonWell network, his care team can:</p>
                    <div>
                        <div class="container__one-third">
                            <div class="circle-icon bg--emerald">
                                <i class="fas fa-prescription fa-2x"></i>
                            </div>
                            <p>Access Brent's medical history, including medications, problems, and allergies.</p>
                        </div>
                        <div class="container__one-third">
                            <div class="circle-icon bg--emerald">
                                <i class="fas fa-bolt fa-2x"></i>
                            </div>
                            <p>Leverage <a href="https://ehr.meditech.com/ehr-solutions/commonwell-connected">CommonWell's connection</a> to Carequality, to access shared injury history from a local academic facility.</p>
                        </div>
                        <div class="container__one-third">
                            <div class="circle-icon bg--emerald">
                                <i class="far fa-hand-point-up fa-2x"></i>
                            </div>
                            <p>Review past treatment and test results from within their workflow, so they immediately know how best to address his symptoms.</p>
                        </div>
                    </div>
                    <p>Since Brent's PCP has access to his complete medical record, all information about this health episode will be at the physician's fingertips when Brent returns for a check-up.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- End Block 5 -->


    <!-- Start Block 6 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/CHF-patient-with-cardiologist.jpg); background-position: top;">
        <div class="container__centered">
            <div class="gl-container">
                <div class="container__one-third  bg--blue-gradient">
                    <h2>Interoperability improves the patient experience.</h2>
                    <p>Patients want the freedom to choose their own specialist, the convenience of an easily accessible medical history, and the ability to engage with providers regularly. But high quality care requires more than just a strong specialist. It requires real-time patient information to make informed care decisions. From local and regional connections to enrollment in nationwide data exchange services, Traverse works with our PGHD solutions to keep patients and their records connected.</p>
                </div>
                <div class="container__two-thirds center">
                    <div class="headshot-border">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/diane-headshot.png" alt="Diane head shot">
                    </div>
                    <h3 style="margin-bottom:1em;">Consider Diane, a 55-year-old CHF patient, who requests a referral to a local cardiologist her friend recommended. MEDITECH's Expanse EHR provides:</h3>
                    <div class="container__one-third">
                        <h3>Freedom</h3>
                        <p>Diane's PCP and specialist share information through the local health information exchange (HIE), including her medication list, problem list, and visit notes.</p>
                    </div>
                    <div class="container__one-third">
                        <h3>Convenience</h3>
                        <p>Diane doesn't have to recall all her medical details or have tests duplicated.</p>
                    </div>
                    <div class="container__one-third">
                        <h3>Engagement</h3>
                        <p>Diane's PCP can monitor weight and blood pressure readings that Diane enters using the MHealth Mobile app. She'll intervene if she detects potential risks, such as rapid weight gain attributed to fluid retention.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Block 6 -->


    <!-- Start Block 7 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/patient-on-gurney.jpg);">
        <div class="container__centered">
            <div class="fw-gl-container">
                <div class="container bg--green-gradient">
                    <div class="center" style="margin-bottom:1em;">
                        <h2>Interoperability is efficient for clinicians.</h2>
                        <p>Timely treatment requires timely information, especially in the ED, where it can prove life saving.</p>
                    </div>
                    <div class="container__one-third">
                        <div class="headshot-border">
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/carlos-headshot.png" alt="Carlos head shot">
                        </div>
                    </div>
                    <div class="container__two-thirds">
                        <p class="paragraph-margin" style="margin-top:.5em;">Take Carlos. Carlos is rendered unconscious following a motor vehicle accident and is life flighted to a Level II trauma center. With only his driver's license available and no prior record of care, physicians normally would be unaware that Carlos is diabetic and is allergic to iodine. But with Traverse, they can:</p>
                    </div>
                </div>
                <div class="container center">
                    <div class="container__one-fourth">
                        <div class="circle-icon bg--emerald">
                            <i class="far fa-hand-point-up fa-2x"></i>
                        </div>
                        <p>Review Carlos' information in other systems through MEDITECH's secure One Touch EHR-to-EHR connection embedded in their workflow; no need to search for the patient or sign onto another system.</p>
                    </div>
                    <div class="container__one-fourth">
                        <div class="circle-icon bg--emerald">
                            <i class="fas fa-bolt fa-2x"></i>
                        </div>
                        <p>Quickly access Carlos' external medical information through a medical summary widget embedded in the clinician's workflow.</p>
                    </div>
                    <div class="container__one-fourth">
                        <div class="circle-icon bg--emerald">
                            <i class="far fa-address-card fa-2x"></i>
                        </div>
                        <p>Navigate a personalized view of outside medical summaries.</p>
                    </div>
                    <div class="container__one-fourth">
                        <div class="circle-icon bg--emerald">
                            <i class="fas fa-prescription fa-2x"></i>
                        </div>
                        <p>Select medications, allergies, problems, and immunizations to add to the patient's record in Expanse.</p>
                    </div>
                </div>
                <div class="container center" style="padding: 0em 2em 1em 2em;">
                    <h4>Through Traverse, interoperability speaks up for Carlos by sharing his complete story with clinicians, when he is unable to do so himself.</h4>
                </div>
            </div>
        </div>
    </div>
    <!-- End Block 7 -->


    <!-- Start Block 98 -->
    <!-- Start popup video -->
    <!-- Start hidden modal box -->
    <div id="modal1" class="modal">
        <a class="close-modal" href="javascript:void(0)">&times;</a>
        <div class="modal-content" style="z-index:10001;">
            <iframe src="https://player.vimeo.com/video/311922713" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
        </div>
    </div>
    <!-- End hidden modal box -->

    <div class="container no-pad background--cover flip-horizontal--bg" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/male-nurse-hands-on-tablet--blurred-background.jpg);">
        <div style="background-color: rgba(0, 0, 0, 0.5); padding:2em 0;">
            <div class="container__centered">

                <div class="container__two-thirds text--white flip-horizontal--content" style="padding: 2em 0;">

                    <h2> Interoperability is simply better care, made easier.</h2>
                    <p>Hear from Dr. Harris as well as representatives from the Valley Hospital and Signature Healthcare as they share how they're using MEDITECH Traverse to improve care delivery.</p>

                    <div class="container__centered">
                        <figure class="container__one-fourth center">

                            <!-- Start modal trigger -->
                            <div class="open-modal" data-target="modal1">
                                <img class="small_popup_video" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dr-louis-harris.png" alt="Dr. Louis Harris">
                                <div class="vid-bg">
                                    <!-- Include if using image trigger -->
                                    <i class="mag-icon fas fa-play"></i>
                                </div>
                            </div>
                            <!-- End modal trigger -->

                        </figure>

                        <div class="container__three-fourths">
                            <div>
                                <p style="font-size: 1em;">"Being able to see where patients have been, what their experience was, what their plans were, what tests they were supposed to have...within our own system is really beneficial."</p>
                                <p><i>Dr. Louis Harris, CMIO, Citizens Memorial</i></p>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <!-- End popup video -->
    <!-- End Block 8 -->


    <!-- Start Block 9 -->
    <div class="container bg--blue-gradient">
        <div class="container__centered">
            <div class="container__one-third center">
                <div id="modal2" class="modal">
                    <a class="close-modal" href="javascript:void(0)">×</a>
                    <div class="modal-content">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/iPhoneX-AllRecords--screen-shot--2019.png" alt="FHIR APIs Expand Consumer Access on iPhone">
                    </div>
                </div>
                <div class="open-modal" data-target="modal2">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/iPhoneX-AllRecords--screen-shot--2019.png" alt="FHIR APIs Expand Consumer Access on iPhone" style="width: 250px;">
                    <div class="mag-bg">
                        <i class="mag-icon fas fa-search-plus"></i>
                    </div>
                </div>
            </div>

            <div class="container__two-thirds">
                <h2>FHIR<sup>&reg;</sup> APIs expand consumer access on iPhone<sup>&reg;</sup></h2>
                <p>MEDITECH is giving consumers access to their health data through Health Records on iPhone.</p>
                <p>Using FHIR standard APIs, the Health app connects to MEDITECH's patient portal and pulls forward new data as soon as it's available.</p>
                <p>Information is aggregated across multiple participating organizations and combined with PGHD, to provide consumers with one convenient record that:</p>
                <ul style="margin-top:1.25em;">
                    <li>Includes key data elements, such as allergies, medications, vitals, lab results, conditions, procedures, and immunizations.</li>
                    <li>Helps fill information gaps for providers who aren't familiar with the patient, or who aren't connected through interoperability.</li>
                    <li>Supports secure and convenient access using the same credentials as the patient portal.</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- End Block 9 -->


    <!-- Start Block 10 -->
    <div class="container">
        <div class="container__centered">
            <div class="auto-margins center">
                <h2>Interoperability is happening today.</h2>
                <p>Interoperability isn't some far-away goal; with Traverse, MEDITECH users are delivering interoperable, efficient, safe care to patients right now. For example, Union Hospital of Cecil County is connecting to both the Maryland (CRISP) and Delaware (DHIN) HIEs, and <a href="https://info.meditech.com/case-study-union-hospital-improves-care-efficiency-with-multiple-health-information-exchange-strategy-0">producing meaningful results</a> for clinicians, including:</p>
            </div>
            <div class="container center" style="padding: 1.5em 0em 1.5em 0em;">
                <div class="container__one-third">
                    <div class="circle-icon">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--schedule.png" alt="Schedule icon">
                    </div>
                    <p>Saving physicians from one hour of unnecessary paperwork per day.</p>
                </div>
                <div class="container__one-third">
                    <div class="circle-icon">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--care-alerts.png" alt="Care alerts icon">
                    </div>
                    <p>Delivering pertinent patient information to users at a glance, through care alerts.</p>
                </div>
                <div class="container__one-third">
                    <div class="circle-icon">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--patient-portal.png" alt="Patient Portal icon">
                    </div>
                    <p>Embedding HIE portal access into clinical workflows, for a real-time view of any patient's regional data.</p>
                </div>
            </div>
            <div class="container center no-pad">
                <div class="container__one-third">
                    <div class="circle-icon">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--communication.png" alt="Communication icon">
                    </div>
                    <p>More effective clinical communication through secure messaging.</p>
                </div>
                <div class="container__one-third">
                    <div class="circle-icon">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--analytics.png" alt="Analytics icon">
                    </div>
                    <p>Comparing readmission rates with other organizations and setting achievable goals using real-time analytics.</p>
                </div>
                <div class="container__one-third">
                    <div class="circle-icon">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--workflow.png" alt="Workflow icon">
                    </div>
                    <p>Quicker, more informed decision-making on patient transfers.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- End Block 10 -->


    <!-- Start Block 11 -->
    <div class="container background--cover text--white" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/greenfield-background-digital-components.jpg); background-position:center top;">
        <div class="container__centered">
            <div class="auto-margins">
                <p>APIs and vendor collaboration play a vital role in fueling innovation, increasing EHR value, and laying the groundwork for better healthcare by ensuring data can be shared appropriately regardless of where patients receive care. MEDITECH Greenfield is expanding to include two new collaborative environments:</p>
            </div>
            <div class="auto-margins">
                <div class="container__one-half">
                    <div class="center no-target-icon" style="padding:1em;">
                        <img style="width:60%;" src="https://ehr.meditech.com/sites/all/themes/meditech/images/MEDITECH-Greenfield-Alliance-logo--white-knockout.svg" alt="MEDITECH Greenfield Alliance logo">
                    </div>
                    <p>The <a href="https://ehr.meditech.com/ehr-solutions/greenfield-alliance">Greenfield Alliance</a> is for partner organizations with proven, successful, and interoperable products which are aligned with MEDITECH and our solutions.</p>
                </div>
                <div class="container__one-half">
                    <div class="center no-target-icon" style="padding:1em;">
                        <img style="width:60%;" src="https://ehr.meditech.com/sites/all/themes/meditech/images/MEDITECH-Greenfield-Workspace-logo--white-knockout.svg" alt="MEDITECH Greenfield Workspace logo">
                    </div>
                    <p>The <a href="https://ehr.meditech.com/ehr-solutions/greenfield-workspace">Greenfield Workspace</a> is an open app development environment where MEDITECH Expanse customers and third-party developers can create mobile apps using RESTful FHIR APIs.</p>
                </div>
            </div>

        </div>
    </div>
    <!-- End Block 11 -->

</div>
<!-- end js__seo-tool__body-content -->


<!-- Start Block 12 -->
<div class="container auto-margins">
    <div class="container__centered center">

        <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
        <h2>
            <?php print $cta->field_header_1['und'][0]['value']; ?>
        </h2>
        <?php } ?>

        <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
        <div>
            <?php print $cta->field_long_text_1['und'][0]['value']; ?>
        </div>
        <?php } ?>

        <div class="btn-holder--content__callout center" style="margin-bottom:1.5em;">
            <div class="center" style="margin-top:2em;">
                <?php hubspot_button($cta_code, "Register for our Clinical Informatics Symposium"); ?>
            </div>
        </div>

        <div style="margin-top:1em;">
            <?php print $share_link_buttons; ?>
        </div>

    </div>
</div>
<!-- End Block 12 -->


<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>
<!-- END campaign--node-2735.php -->
