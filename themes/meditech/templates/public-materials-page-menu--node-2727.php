<!-- start public-materials-page-menu--node-2727.php Expanse Materials template -->
<?php
$url = $GLOBALS['base_url']; // grabs the site url  

$content_sections = multi_field_collection_data_unrestricted($node, 'field_fc_head_ltext_1');
$buttons = multi_field_collection_data_unrestricted($node, 'field_fc_button_unl_1');
$side_menu = multi_field_collection_data_unrestricted($node, 'field_fc_text_link_unl_1');
?>

<style> 
  a.anchor { display: block; position: relative; top: -195px; visibility: hidden;	} 
  .transparent-overlay { margin-bottom: 2em; }
  .transparent-overlay > div.center { color:#ffffff; padding-bottom:.5em; }
  
  .thumbnails p { width: 33%; float: left; }
  .thumbnails a { border-bottom: 0; }
  .thumbnails a[href$=".pdf"]:after, .thumbnails a[target="_blank"]:after { content: ""; }
</style>



<!-- Main Content -->
<div class="container__centered" style="padding-top:2em; margin-bottom:1em;">
  
  <h1><?php print $title; ?></h1>

  <div class="container__two-thirds">
    <?php
    if( isset($content_sections) && !empty($content_sections) ){
      $number_of_content_sections = count($content_sections);
      for($cs=0; $cs<$number_of_content_sections; $cs++){
        if($cs==2){
          print '<div class="container thumbnails no-pad">';
        }
        else{
          print '<div class="container no-pad">';
        }
          print '<h2>';
          print $content_sections[$cs]->field_header_1['und'][0]['value']; 
          print '</h2>';
          print $content_sections[$cs]->field_long_text_1['und'][0]['value']; 
        print '</div>';
      }
    }
    ?>
    
    <div class="video js__video" data-video-id="502762364" style="margin: 2em;">
      <figure class="video__overlay">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--MEDITECH-The-best-care-Humanly-possible.jpg" alt="MEDITECH: The best care. Humanly possible.">
      </figure>
      <a class="video__play-btn video_gae" href="https://vimeo.com/502762364"></a>
      <div class="video__container">
      </div>
    </div>
  </div>
  
  	
  <!--
  <aside class="container__one-third panel">
    <?php
    if( isset($side_menu) && !empty($side_menu) ){
      print '<div class="sidebar__nav solutions_sidebar_gae">';
      print '<ul class="menu">';
      $number_of_menu_links = count($side_menu);
      for($ml=0; $ml<$number_of_menu_links; $ml++){
        print '<li><a href="';
        print $side_menu[$ml]->field_link_url_1['und'][0]['value']; 
        print '">';
        print $side_menu[$ml]->field_link_text_1['und'][0]['value']; 
        print '</a></li>';
      }
      print '</ul>';
      print '</div>';
    }
    ?>
  </aside>
  -->
  
</div>
<!-- end public-materials-page-menu--node-2727.php Expanse Materials template -->