<!-- start node--home-page.tpl.php template -->
<?php // This template is set up to control the display of the HOME PAGE content type
$url = $GLOBALS['base_url']; // grabs the site url
?>


<style>
  .event-logo img {
    width: 70%;
    margin-top: 0.5em;
    margin-bottom: 1em;
  }

  /* <!-- Outlined Button --> */
  .btn--outline {
    background-color: transparent;
    color: #3E4545 !important;
    border-width: 4px !important;
    -webkit-transition: all 400ms ease-in-out;
  }

  .btn--orange:hover {
    -webkit-transition: all 400ms ease-in-out;

  }

  .btn--outline .button--hubspot a {
    font-size: 0.89em !important;
    height: auto !important;
    line-height: normal !important;
    outline: 0 !important;
    padding: 1em 1.5em !important;
    display: inline-block !important;
    font-weight: 700 !important;
    cursor: pointer !important;
    color: #3E4545 !important;
    text-align: center !important;
    -webkit-transition: all 400ms ease-in-out !important;
    -moz-transition: all 400ms ease-in-out !important;
    transition: all 400ms ease-in-out !important;
    border: 4px solid #ff8300 !important;
    background-color: transparent !important;
  }

  .btn--outline .button--hubspot a:hover {
    color: #fff !important;
    -webkit-transition: all 400ms ease-in-out !important;
    -moz-transition: all 400ms ease-in-out !important;
    transition: all 400ms ease-in-out !important;
    background: #ff610a !important;
    border: 4px solid #ff610a !important;
  }

  .btn--outline .button--hubspot a:visited {
    color: #3E4545 !important;
  }

  /* <!-- Contact Sales Button --> */
  #contact-sales-btn {
    position: fixed;
    right: 20px;
    bottom: 20px;
    width: 190px;
    border: 6px solid #00BC6F;
    box-shadow: 3px 3px 20px rgba(0, 0, 0, 0.3);
    z-index: 10;
  }

  .contact-title {
    background: #00BC6F;
    padding: 5px;
    color: #fff;
    font-size: 21px;
    font-weight: bold;
    cursor: pointer;
  }

  .contact-title a {
    color: #fff;
  }

  .contact-title a:hover {
    color: #fff;
  }

  .info-window {
    background-color: #fff;
    padding: 10px;
    font-size: 18px;
    display: none;
    text-align: center;
  }

  .float--x-container {
    float: right;
    z-index: 1001;
    padding-right: 3px;
  }

  .float--x {
    color: #fff;
  }

  @media all and (max-width: 50em) {
    #contact-sales-btn {
      display: none;
    }

    .card figure img {
      height: 225px;
    }
  }

  /* Expanse */
  .shadow-box--var {
    padding: 2em;
    color: #3e4545;
    background-color: rgba(255, 255, 255, 0.8);
    border-radius: 7px;
    box-shadow: 0px 0px 20px 1px rgba(120, 120, 120, .2);
    max-width: 500px;
    position: absolute;
    right: 18px;
    top: 50%;
    transform: translateY(-50%);
  }

  .shadow-box {
    padding: 2em 1.9em;
  }

  .container--bg-img {
    max-width: 900px;
    border-radius: 7px;
  }

  .container--rel {
    position: relative;
  }

  @media all and (max-width: 950px) {
    .shadow-box--var {
      max-width: 100%;
      position: relative;
      margin: 0 auto;
      right: 0;
      transform: translateY(0%);
      width: 100%;
      margin-top: 2.35765%;
    }

    .container--bg-img {
      display: none;
    }

    .container--rel {
      margin-bottom: 0;
    }
  }

  /* Digital Transformation Video */
  .fw-pad2 {
    padding: 3em 5em;
    max-width: 1750px;
    margin: 0 auto;
  }

  .container-rel {
    position: relative;
  }

  @media (max-width: 1050px) {
    .container-rel {
      margin: 3em 0;
    }
  }

  @media (max-width: 1000px) {
    .container-rel {
      margin: 0 0 !important;
    }
  }

  @media (max-width: 900px) {
    .container-rel {
      margin: 0 0 !important;
    }

    .fw-pad2 {
      padding: 3em 1em;
    }
  }

  /* Campaigns */

  /* Used to center one-third blocks to prevent squish on tablet*/
  @media (max-width: 1000px) {
    .center-tablet {
      text-align: center !important;
      display: block;
      width: 100%;
    }
  }

  /* KLAS */
  .award-img {
    width: 250px;
    margin-bottom: 1em;
  }

  /* end campaign */
  @media (max-width: 800px) {
    .center-mobile {
      text-align: center;
    }

    .margin-mobile {
      margin-top: 1.5em;
    }
  }
  
  .event-text { position: absolute; top: 0; right: 0; background-color: #f0fafb; width: 60%; padding: 2em 6em; }
  .event-image { position: absolute; top: 85px; left: 0; width: 50%; }
  @media (max-width: 1000px) {
    .event-text { padding: 2em 5em; }
  }
  @media (max-width: 800px) {
    .event-text { position: absolute; top: 0; right: 0; background-color: #f0fafb; width: 100%; padding: 2em; }
    .event-image { display: none; }
  }
  
</style>

<!-- Announcements/Alerts -->
<div style="background-color:#fdffd2; padding:1em 0;">
  <div class="container__centered center">
    <p class="no-margin--bottom text--small text--black-coconut">Learn how MEDITECH is <a href="https://ehr.meditech.com/ehr-solutions/covid-19-workflows">guiding customers</a> through the COVID-19 pandemic, or access our <a href="https://customer.meditech.com/en/d/covid19resources/homepage.htm">Customer Resource Page</a>.</p>
  </div>
</div>



<!-- Start Contact Sales Button -->
<div id="contact-sales-btn">
  <div class="float--x-container"><a href="#!" class="hide_sales_gae" onclick="hide('contact-sales-btn')"><i class="fas fa-times fa-lg float--x"></i></a></div>
  <div class="contact-title">
    <a href="#!" class="sales_button_gae"><i class="fa fa-phone fa-md fa-flip-horizontal" style="margin-right:5px;"></i> Get Started</a>
  </div>
  <div class="info-window">
    <p style="margin-bottom:0px;">Learn more about Expanse:<br> <span style="font-weight:bold;">781-774-7700</span></p>
  </div>
</div>
<script>
  // make sure not to conflict with CMS's jQuery...
  var $jq = jQuery.noConflict();
  $jq(document).ready(function() {
    $jq(".contact-title").click(function() {
      $jq(".info-window").toggle("fast");
    });
  });

  function hide(target) {
    document.getElementById(target).style.display = "none";
  }

</script>
<!-- End Contact Sales Button -->


<div class="js__seo-tool__body-content">

  <!-- Start Homepage Content -->
  <h1 style="display:none;" class="js__seo-tool__title">MEDITECH EHR Software Company</h1>


  <!-- Start Background Image -->
  <div class="background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/home/organic-shapes-2--bg-small.jpg);">


    <!-- Start Block 1 (Expanse Success) -->
    <div class="container__centered container--rel" style="padding:4em 1em;">
      <img class="container--bg-img" src="<?php print $url; ?>/sites/all/themes/meditech/images/home/black-male-doctor-with-black-child-patient.jpg" alt="black male doctor with black child patient and mother">
      <div class="shadow-box--var" style="padding:1em 2em;">
        <img style="padding: 1em 1em 0 1em;" src="<?php print $url; ?>/sites/all/themes/meditech/images/MEDITECH-expanse-logo--green-grey.svg" alt="MEDITECH Expanse Logo">
        <p>Healthcare technology is all about possibilities; how we can be healthier, happier, and more connected during an unpredictable time. Find out why MEDITECH Expanse is the EHR for a changing world.</p>
        <div class="btn-holder--content__callout center" style="margin-top:2em;">
          <?php hubspot_button('33997285-e326-4093-ac3e-d72779e0a5f1', "Explore Expanse"); ?>
        </div>
      </div>
    </div>
    <!-- End Block 1 (Expanse Success) -->


    <!-- Start Block 2 (Case Study) -->
    <div id="customers" class="container" style="margin: 3em 0;">
      <div class="container__centered">

        <h2 class="center" style="padding-bottom: 2em;">Our customers are improving outcomes with real results</h2>

        <div class="container__one-third shadow-box center-mobile">
          <div class="container__two-thirds">
            <p class="no-margin"><span style="font-size: 1.5em"><strong>100+ Hours</strong></span><br>
              Saved by nurses over 6 months<br><a href="https://info.meditech.com/en/kings-daughters-medical-center-gives-back-100-plus-hours-to-nurses-with-meditech-expanse-patient-care">King's Daughters Medical Center</a></p>
          </div>
          <div class="container__one-third margin-mobile">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/home/icon--purple-up-arrow--hours.svg" alt="hours up arrow icon" style="height: 100px; width:100%;">
          </div>
        </div>

        <div class="container__one-third shadow-box center-mobile">
          <div style="width:100%; float:left;">
            <div class="container__two-thirds">
              <p class="no-margin"><span style="font-size: 1.5em"><strong>140 Hours</strong></span><br>
                Saved by staff per month <br><a href="https://cdn2.hubspot.net/hubfs/2897117/WPs,%20Case%20Studies,%20Special%20Reports/CustomerLeaders_GoldenValley_BCA.pdf">Golden Valley Memorial Healthcare & MEDITECH Professional Services</a></p>
            </div>
            <div class="container__one-third margin-mobile">
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/home/icon--green-down-arrow--hours.svg" alt="hours down arrow icon" style="height:100px; width:100%;">
            </div>
          </div>
          <div style="width:100%; float:left; position:relative;">
            <a href="<?php print $url; ?>/ehr-solutions/meditech-professional-services"></a>
          </div>
        </div>

        <div class="container__one-third shadow-box center-mobile">
          <div class="container__two-thirds">
            <p class="no-margin"><span style="font-size:1.5em"><strong>8% Increase</strong></span><br>
              in clinic volumes &amp; 13 fewer A/R Days <br><a href="https://info.meditech.com/summit-pacific-increases-reimbursement-clinic-volumes-with-meditech-analytics-solution-0">Summit Pacific Medical Center</a></p>
          </div>
          <div class="container__one-third margin-mobile">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/home/icon--purple-up-arrow--percent.svg" alt="percent up arrow icon" style="height: 100px; width:100%;">
          </div>
        </div>

      </div>

      <div class="btn-holder--content__callout center" style="margin-top:2em;">
        <a href="<?php print $url; ?>/case-studies" class="btn--orange btn--outline">Read our case studies</a>
      </div>

    </div>
    <!-- End Block 2 (Case Study) -->


  </div><!-- End Background Image -->

  <!-- Start Block 3 (MEDITECH LIVE) -->
  <div class="container__centered center auto-margins" style="margin-bottom: 2em; margin-top: 6em;">
    <h2>It's time to turn those results into large-scale transformational change.
      Join us at MEDITECH LIVE.</h2>
  </div>

  <div class="container__centered" style="margin-bottom: 4em;">
    <div style="position:relative; width:100%; height:500px;">

      <div class="event-text center">

        <div class="event-logo">
          <img src="<?php print $websiteURL; ?>/sites/all/themes/meditech/images/events/MEDITECH-Live22-logo.svg" alt="MEDITECH LIVE 2022 logo">
        </div>
        <div>
          <p class="bold">We’re CALLING ALL CHANGEMAKERS for MEDITECH LIVE 22</p>
          <p>September 20th - 22nd | In-person | Foxborough, MA</p>
          <div class="btn-holder--content__callout btn--outline" style="margin-top:2em; text-align:unset;">
            <a href="<?php print $url; ?>/events/meditech-live-22" class="btn--orange btn--outline">Join Us</a>
          </div>
        </div>

      </div>

      <div class="event-image">
        <figure>
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/home/MEDITECH-LIVE-22--homepage--test.png" alt="Video covershot - MEDITECH Expanse">
        </figure>
      </div>

    </div>
  </div>
  <!-- End Block 3 (MEDITECH LIVE) -->

  <!-- Start Block 4 (Success Stories) -->
  <div id="expanse" class="container" style="margin: 5em 0 3em 0">
    <div class="container__centered">
      <div class="center" style="padding-bottom: 2em;">
        <h2>Here's where Expanse is making a difference</h2>
      </div>
      <?php print views_embed_view('success_stories_hp', 'block'); // adds 'Success Stories - Home page' Views block... ?>
      <div class="center">
        <div class="btn-holder--content__callout btn--outline" style="margin-top:2em; text-align:unset;">
          <?php hubspot_button('af8fa1bf-3269-4e9a-a8da-e92e570f7dc1', "See Our Customers' Achievements"); ?>
        </div>
      </div>
    </div>
  </div>
  <!-- End Block 4 (Success Stories) -->


  <!-- Start Block 5 (MEDITECH Campaigns) -->
  <div id="campaigns" class="container fw-pad2">
    <div class="gl-container" style="border-radius: 7px;">
      <div class="gl-text-pad" style="background-color: #f0fafb;">
        <h2 class="center" style="margin-bottom:2em;">Let's revolutionize care on your terms</h2>
        <div class="container__one-third center-tablet" style="margin-bottom:2em;">
          <h4>Expanse Ambulatory</h4>
          <p>Whether you’re an independent physician practice or part of an organization using Expanse in other care settings, Expanse Ambulatory is the most intuitive and efficient EHR and practice management solution available.</p>
          <div class="btn-holder--content__callout">
            <div class="center-tablet" style="text-align:left; margin-top:2em;">
              <a href="<?php print $url; ?>/ehr-solutions/expanse-ambulatory" class="btn--orange btn--outline">Expanse Ambulatory</a>
            </div>
          </div>
        </div>
        <div class="container__one-third center-tablet" style="margin-bottom:2em;">
          <h4>Flexible, Cloud-Hosted Option</h4>
          <p>Focus resources on your core business, and spend less energy managing servers and having to pay for unused hardware space. Lower your overhead through flexible subscription pricing.</p>
          <div class="btn-holder--content__callout">
            <div class="center-tablet" style="text-align:left; margin-top:2em;">
              <a href="<?php print $url; ?>/ehr-solutions/meditech-and-google-cloud" class="btn--orange btn--outline">MEDITECH and Google Cloud</a>
            </div>
          </div>
        </div>
        <div class="container__one-third center-tablet" style="margin-bottom:2em;">
          <h4>Professional Services</h4>
          <p>Try our more personalized approach to your Expanse implementation, to maximize your ROI.</p>
          <div class="btn-holder--content__callout">
            <div class="center-tablet" style="text-align:left; margin-top:2em;">
              <a href="<?php print $url; ?>/ehr-solutions/meditech-professional-services" class="btn--orange btn--outline">Contact Professional Services</a>
            </div>
          </div>
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/logo--MEDITECH-Professional-Services--gray-and-green.png" alt="" title="MEDITECH Professional Services" style="width:70%;">
        </div>
      </div>
    </div>
  </div>
  <!-- End Block 5 (MEDITECH Campaigns) -->

  <!-- Start New Block 6 (KLAS Awards) -->
  <!-- Start Background Image -->
  <div class="background--cover hide__bg-image--tablet" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/home/organic-shapes--bg-small.jpg);">

    <div class="container center" style="padding: 5em 0;">

      <div class="container__centered">
        <h2>MEDITECH Recognized as Best in KLAS for Eighth Consecutive Year</h2>
        <p class="italic" style="margin-bottom: 2em;">Expanse ranks #1 in three key markets segments</p>
        <div class="container__one-third">
          <img class="award-img" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-KLAS-Award-2022--Acute-Care-EMR.png" alt="Best in KLAS 2022 award - Acute Care EMR (community hospital) for MEDITECH Expanse">
          <p class="bold no-margin--bottom">Acute Care EMR</p>
          <p>Community Hospital</p>
        </div>
        <div class="container__one-third">
          <img class="award-img" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-KLAS-Award-2022--Patient-Accounting-and-Patient-Management.png" alt="Best in KLAS 2022 award - Patient Accounting & Patient Management (community hospital) for MEDITECH Expanse">
          <p class="bold no-margin--bottom">Patient Accounting &amp;
            Patient Management</p>
          <p>Community Hospital</p>
        </div>
        <div class="container__one-third">
          <img class="award-img" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-KLAS-Award-2022--Home-Health.png" alt="Best in KLAS 2022 award - Home Health (small) for MEDITECH Expanse">
          <p class="bold no-margin--bottom">Home Health EHR</p>
          <p>1-200 average daily census</p>
        </div>
      </div>

      <div class="container__centered" style="margin-top:2em;">
        <p class="italic">For the second consecutive year, MEDITECH is a top performer for Overall Software Suite, Ambulatory EMR (>75 physicians), and Acute Care EMR (Large/IDN). Learn more about our KLAS recognition and how MEDITECH was among the leaders in market share growth for 2022.</p>
        <div class="btn-holder--content__callout" style="margin-top:1.5em;">
          <a href="<?php print $url; ?>/about/meditech-awards" class="btn--orange btn--outline">Learn More</a>
        </div>
      </div>

    </div>
    <!-- End New Block 6 (KLAS Awards) -->

  </div><!-- End of BG image-->

</div>



<?php // SEO tool for internal use...
if( user_is_logged_in() ){
  print '<!-- SEO Tool is added to this div -->';
  print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
}
?>

<!-- end node--home-page.tpl.php template -->
