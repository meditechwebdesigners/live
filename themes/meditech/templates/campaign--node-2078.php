<!-- START campaign--node-2078.php -->
<?php // This template is set up to control the display of the TOOLKIT / SEPSIS CAMPAIGN

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<head>
    <link type="text/css" rel="stylesheet" href="https://ehr.meditech.com/sites/default/files/css/css_lQaZfjVpwP_oGNqdtWCSpJT1EMqXdMiU84ekLLxQnc4.css" media="all" />
    <link type="text/css" rel="stylesheet" href="https://ehr.meditech.com/sites/default/files/css/css_k3iUQDF8-kZtzliL_l12rbZ6lfqLO2kwHAqa4uhscWM.css" media="all" />
    <link type="text/css" rel="stylesheet" href="https://ehr.meditech.com/sites/default/files/css/css_RDEzBX_kRnOFSXRD_8t5IS_LJAqSLATt68n1h2oV3D.css" media="all" />
</head>

<style>
    h5.new-toolkit:before {

        content: "NEW!";
        font-size: 11px;
        padding-left: 140px;
        position: absolute;
        color: #00BC6F;
        float: right;
    }

    /*
    h5.new-toolkit:after {

        content: "NEW!";
        font-size: 11px;
        padding-left: 10px;

        color: #00BC6F;

    }
*/

    .toolkit--wrapper {
        display: flex;
        flex-wrap: wrap;
        flex-direction: row;
        justify-content: space-evenly;
    }

    .toolkit--item {
        margin: 1.5em;
    }

    .quote--headshot {
        border-radius: 7em;
        width: 10em;
    }

    .headshot-border {
        max-width: 150px;
        max-height: 150px;
        border: 6px solid #00BC6F;
        border-radius: 50%;
        margin-left: auto;
        margin-right: auto;
    }

    @media all and (max-width: 71.250em) {
        .toolkit--item {
            margin: 1em;
        }
    }


    @media all and (max-width: 62.375em) {
        .toolkit--item {
            margin: 0.7em;
        }

        /*
h5.new-toolkit:before {
padding-left: 140px;
}
*/
    }

    @media all and (max-width: 58.375em) {
        .toolkit--item {
            margin: 1.2em;
        }
    }

    @media all and (max-width: 54.375em) {
        .toolkit--item {
            margin: 1.1em;
        }



    }

    @media all and (max-width: 50em) {
        .container__one-fourth {
            float: left;
            display: block;
            margin-right: 2.35765%;
            width: 100%
        }

        .container__one-fourth:last-child {
            margin-right: 0
        }

        h5.new-toolkit:before {
            padding-left: 105px;
        }


    }

</style>



<div class="js__seo-tool__body-content">

    <!-- Block 1 -->
    <div class="container no-pad">
        <div class="gl-container bg--white">
            <div class="container__one-half background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/female-doctor-shows-tablet-to-female-patient-in-hospital-bed.jpg); min-height:550px; background-position: center;">
            </div>
            <div class="container__one-half gl-text-pad bg--white">
                <h1 class="js__seo-tool__title">
                    MEDITECH EHR Excellence Toolkits
                </h1>
                <h2>
                    Get more from your EHR in less time, with fewer resources.
                </h2>
                <p>
                    Why reinvent the wheel? MEDITECH’s EHR Excellence Toolkits recommend optimal MEDITECH EHR workflows and system setup to improve patient safety and outcomes. Designed using MEDITECH Expanse, our evidence-based toolkits are built and maintained in collaboration with staff physicians, clinicians, and customers, to get you up and running fast.
                </p>


                <div class="center" style="margin-top:2em;">
                    <?php hubspot_button($cta_code, "Watch The AMS Toolkit Webinar Recording"); ?>
                </div>

            </div>
        </div>
    </div>
    <!-- End of Block 1 -->

    <!-- Block 2 - Video -->
    <!--
    <div class="content__callout border-none" style="background-color: #E6E9EE !important;">
        <div class="content__callout__media">
            <div class="content__callout__image-wrapper" style="padding:3em !important;">
                <div class="video js__video video--shadow" data-video-id="367252293">
                    <figure class="video__overlay">
                        <img src="< ?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--Med-Center-Health-Reduces-Sepsis-Mortality.jpg" alt="Med Center Health Reduces Sepsis Mortality Video Overlay Image">
                    </figure>
                    <a class="video__play-btn video_gae" href="https://vimeo.com/367252293"></a>
                    <div class="video__container"></div>
                </div>
            </div>
        </div>
        <div class="content__callout__content">
            <div class="content__callout__body">
                <div class="content__callout__body__text">
                    <h2>
                        These statistics speak for themselves.
                    </h2>
                    <p>
                        Discover how Med Center Health has been able to implement <a href="https://ehr.meditech.com/ehr-solutions/ehr-toolkits/meditechs-sepsis-management-toolkit">MEDITECH’s Sepsis Management Toolkit</a> to substantially <a href="https://blog.meditech.com/5-vital-ehr-features-to-help-reduce-sepsis">reduce sepsis mortality</a> and readmission rates.
                    </p>
                </div>
            </div>
        </div>
    </div>
-->
    <!-- End Block 2 -->

    <!-- Block 3 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-gradient--bg.jpg);">
        <div class="container__centered center text--white">
            <h2>Tackle top healthcare priorities.</h2>
            <p>
                Our <a href="https://customer.meditech.com/en/d/ehrexcellence/homepage.htm" target="_blank">Toolkit Library</a> includes guidance for these high-priority conditions:
            </p>
            <div class="toolkit--wrapper">
                <div class="toolkit--item">
                    <img style="height: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/antimicrobial--toolkit-icon.svg" alt="Antimicrobial icon">
                    <h5><a href="https://ehr.meditech.com/ehr-solutions/ehr-toolkits/meditechs-antimicrobial-stewardship-toolkit" rel="noreferrer noopener">Antimicrobial
                            <br>Stewardship</a></h5>
                </div>
                <div class="toolkit--item">
                    <img style="height: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/cauti--toolkit-icon.svg" alt="urinary tract icon">
                    <h5><a href="https://ehr.meditech.com/ehr-solutions/ehr-toolkits/meditechs-cauti-prevention-toolkit" rel="noreferrer noopener">CAUTI
                            <br>Prevention</a></h5>
                </div>
                <div class="toolkit--item">
                    <img style="height: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Depression-Screening-and-Suicide-Prevention--toolkit-icon.svg" alt="Depression Screening & Suicide Prevention icon">
                    <h5><a href="https://ehr.meditech.com/ehr-solutions/ehr-toolkits/meditechs-depression-screening-and-suicide-prevention-toolkit" rel="noreferrer noopener">Depression Screening
                            <br>&amp; Suicide Prevention</a></h5>
                </div>
                <div class="toolkit--item">
                    <img style="height: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/diabetes--toolkit-icon.svg" alt="Diabetes-icon">
                    <h5><a href="https://ehr.meditech.com/ehr-solutions/ehr-toolkits/meditechs-diabetes-management-toolkit" rel="noreferrer noopener">Diabetes Prevention
                            <br>&amp; Management</a></h5>
                </div>
                <div class="toolkit--item">
                    <img style="height: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/fall-risk--toolkit-icon.svg" alt="person falling icon">
                    <h5><a href="https://ehr.meditech.com/ehr-solutions/ehr-toolkits/meditechs-fall-risk-management-toolkit" rel="noreferrer noopener">Fall Risk
                            <br>Management</a></h5>
                </div>
                <div class="toolkit--item">
                    <img style="height: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--heart-failure-management-toolkit.svg" alt="heart failure management icon">
                    <h5><a href="https://ehr.meditech.com/ehr-solutions/ehr-toolkits/meditechs-heart-failure-management-toolkit" rel="noreferrer noopener">Heart Failure
                            <br> Management</a></h5>
                </div>
                <div class="toolkit--item">
                    <img style="height: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--hypertension-toolkit.svg" alt="Hypertension Management icon">
                    <h5><a href="https://ehr.meditech.com/ehr-solutions/ehr-toolkits/meditechs-hypertension-management-toolkit">Hypertension
                            <br>Management</a></h5>
                </div>
                <div class="toolkit--item">
                    <img style="height: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/maternity--toolkit-icon.svg" alt="OB Hemorrhage icon">
                    <h5 class="new-toolkit"><a href="https://ehr.meditech.com/ehr-solutions/ehr-toolkits/obstetric-hemorrhage-management-toolkit" rel="noreferrer noopener">OB Hemorrhage
                            <br>Management</a></h5>
                </div>
                <div class="toolkit--item">
                    <img style="height: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/opioid--toolkit-icon.svg" alt="Opioid-icon">
                    <h5><a href="https://ehr.meditech.com/ehr-solutions/ehr-toolkits/meditechs-opioid-stewardship-toolkit" rel="noreferrer noopener">Opioid
                            <br>Stewardship</a></h5>
                </div>
                <div class="toolkit--item">
                    <img style="height: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/sepsis--toolkit-icon.svg" alt="thermometer icon">
                    <h5><a href="https://ehr.meditech.com/ehr-solutions/ehr-toolkits/meditechs-sepsis-management-toolkit" rel="noreferrer noopener">Sepsis
                            <br> Management</a></h5>
                </div>
                <div class="toolkit--item">
                    <img style="height: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--telemetry-toolkit.svg" alt="telemetry monitor icon">
                    <h5><a href="https://ehr.meditech.com/ehr-solutions/ehr-toolkits/meditechs-telemetry-appropriateness-toolkit" rel="noreferrer noopener">Telemetry
                            <br> Appropriateness</a></h5>
                </div>
            </div>
        </div>
    </div>
    <!--End of Block 3 -->

    <!-- Block 4 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/background--blurry-dark-hospital-shot-1.jpg); background-position: top;">
        <div class="container__centered text--white">

            <div class="center">
                <h2>Accelerate clinical quality improvement with MEDITECH Professional Services.</h2>
            </div>

            <div class="container center no-pad--top">
                <div class="gl-container">
                    <div class="container__one-half bg--green-gradient left">
                        <p>
                            MEDITECH EHR Excellence Toolkits are available as self-service, or through <a href="https://ehr.meditech.com/ehr-solutions/meditech-professional-services?hsCtaTracking=85e37c7f-690b-488f-b3ed-4e6b69e8ff11%7Cca64cef9-ae25-453e-aac1-e825f1ea0476">MEDITECH Professional Services</a>.
                        </p>
                        <p>
                            Get help from the experts who design our EHR Excellence Toolkits. <a href="https://ehr.meditech.com/ehr-solutions/meditech-professional-services?hsCtaTracking=85e37c7f-690b-488f-b3ed-4e6b69e8ff11%7Cca64cef9-ae25-453e-aac1-e825f1ea0476">MPS</a> will:
                        </p>
                        <!--                       style="color: #3e4545!important;"-->
                        <ul>
                            <li>
                                Accelerate clinical quality improvement by transforming your current workflow.
                            </li>
                            <li>
                                Facilitate the people, process, and build aspects of a complex quality project.
                            </li>
                            <li>
                                Equip your organization with a team of expert and clinically credentialed resources from the MEDITECH toolkit development division.
                            </li>
                            <li>
                                Support you through a full toolkit implementation.
                            </li>
                            <li>
                                Provide analytics development and support.
                            </li>
                        </ul>
                    </div>
                    <div class="container__one-half bg--white left">
                        <div class="headshot-border">
                            <img class="quote--headshot" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/headshot--William-Dailey.jpg">
                        </div>
                        <div class="italic" style="padding-top: .75em;">
                            <p>
                                "We hired the toolkit experts from MEDITECH’s Professional Services team to help us implement the sepsis toolkit. In addition to completing the toolkit build, they taught us how to embed Golden Valley Memorial Healthcare protocols into our workflows, and how to personalize and optimize Expanse to meet our needs. We saw the shift right away: staff started to become more aware of sepsis, and we are already seeing an increase in our core measure compliance. We are now empowered to take the principles we learned and apply them to future quality initiatives."
                            </p>
                        </div>
                        <p class="no-margin--bottom">
                            William Dailey, MD, CMIO
                        </p>
                        <p>
                            Golden Valley Memorial Healthcare
                        </p>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <!-- END BLOCK 4 -->

    <!-- Block 5 - Video 
    <div class="content__callout border-none" style="background-color: #E6E9EE !important;">
        <div class="content__callout__media">
            <div class="content__callout__image-wrapper" style="padding:3em !important;">
                <div class="video js__video video--shadow" data-video-id="348892173">
                    <figure class="video__overlay">
                        <img src="< ?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--Generic-Toolkit.jpg" alt="Generic Toolkit">
                    </figure>
                    <a class="video__play-btn video_gae" href="https://vimeo.com/348892173"></a>
                    <div class="video__container"></div>
                </div>
            </div>
        </div>
        <div class="content__callout__content">
            <div class="content__callout__body">
                <div class="content__callout__body__text">
                    <h2>
                        What are MEDITECH’s EHR Excellence Toolkits?
                    </h2>
                    <p>
                        Drs. Bryan Bagdasian and Joy Chesnut, physician advisors at MEDITECH, explain how toolkit components such as embedded standard content, advanced clinical decision support, real-time surveillance, and flowsheets streamline workflows and help keep patients safer.
                    </p>
                </div>
            </div>
        </div>
    </div>
     End Block 5 -->

    <!-- Block 6 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-gradient--bg.jpg);">
        <div class="container__centered center text--white">

            <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
            <h2 class="text--white">
                <?php print $cta->field_header_1['und'][0]['value']; ?>
            </h2>
            <?php } ?>

            <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
            <div class="text--white">
                <?php print $cta->field_long_text_1['und'][0]['value']; ?>
            </div>
            <?php } ?>

            <div class="center" style="margin-top:2em;">
                <?php hubspot_button($cta_code, "Watch The Antimicrobial Stewardship Toolkit Webinar Recording"); ?>
            </div>

            <div style="margin-top:1em;">
                <?php print $share_link_buttons; ?>
            </div>

        </div>
    </div>
    <!-- Close Block 6 -->
</div>
<!-- end js__seo-tool__body-content -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>
<!-- END campaign--node-2078.php -->
