<!-- start about--node-4167.php template -->
<?php // This template controls the display of the CUSTOMER LEADERS page
$url = $GLOBALS['base_url']; // grabs the site url
include('inc-share-buttons.php');
?>

<style>
	.share-icons img {
		max-width: 3.5%
	}

	/*	Calendar Styles */
	.cal-left {
		text-align: center;
		border-radius: 7px 0 0 7px;
	}

	.cal-left p {
		margin: .5em 0;
	}

	.cal-right {
		border-radius: 0 7px 7px 0;
		border-top: 1px solid #E6E9EE;
		border-right: 1px solid #E6E9EE;
		border-bottom: 1px solid #E6E9EE;
		background: rgba(248, 249, 253, 1);
	}

	.cal-right h4 {
		margin: .5em 0;
		padding-left: 2em;
	}

	.cal-right a {
		border-bottom: 1px solid #c8ced9;
	}

	.calendar .gl-container>div {
		padding: 1.25em;
		margin-bottom: 1.5em;
	}

	/* <!-- New Styles --> */
	.quote-box {
		padding: 3em;
		background-color: rgba(3, 3, 30, .5);
		border-left: 5px solid #00bc6f;
		margin-bottom: 2.35765%;
		border-radius: 7px;
		box-shadow: 0px 0px 20px 1px rgb(0 0 0 / 20%);
	}

	.shadow-box-outline {
		padding: 4em 2em 2em 2em;
		position: relative;
		box-shadow: none;
		border: 1px solid #c8ced9;
		border-radius: 7px;
		background-color: rgba(3, 3, 30, .3);
		margin-top: 50px;
	}

	.headshot {
		position: absolute;
		top: -50px;
		left: 50%;
		transform: translate(-50%);
	}

	.headshot img {
		width: 100px;
		height: 100px;
		border-radius: 50%;
	}

	.green-border {
		border-left: 7px solid #38b676;
		padding-left: 12px;
		padding-top: 2px;
		display: block;
	}

</style>

<div class="js__seo-tool__body-content">


	<!-- START Block 1 -->
	<div class="container no-pad">
		<div class="gl-container">
			<div class="container__one-half gl-text-pad bg--blue-gradient">
				<p class="header-micro">WHAT'S YOUR STORY?</p>
				<h2>Join our community of customer leaders</h2>

				<p>The <strong>MEDITECH community is mighty</strong>, and each of our customers is unique. We learn so much from our partnerships with healthcare organizations &mdash; and we want to share their achievements with the rest of the world!</p>
				<p>Get the stories about <strong>real practitioners, real patients, and real results</strong> that will help you on your clinical transformation journey. And share the hard work you are doing to help inspire others, <a href="<?php print $url; ?>/meditech-communications-team">by getting in touch with us</a>. We will help you to get the word out through one of our communication channels.</p>
				
				<div class="button--hubspot" style="margin-top:2em;"><!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-fbf32c3a-325a-4ad3-aac5-0410bdd3fcc7"><span class="hs-cta-node hs-cta-fbf32c3a-325a-4ad3-aac5-0410bdd3fcc7" id="hs-cta-fbf32c3a-325a-4ad3-aac5-0410bdd3fcc7" style="visibility: visible;" data-hs-drop="true"><a id="cta_button_2897117_04afc067-efcd-4e3f-9918-0bc02fbfbb02" class="cta_button " href="https://info.meditech.com/cs/c/?cta_guid=04afc067-efcd-4e3f-9918-0bc02fbfbb02&amp;signature=AAH58kH6RjL5oVm7kQKcrj-C9RRFtEV1xA&amp;placement_guid=fbf32c3a-325a-4ad3-aac5-0410bdd3fcc7&amp;click=cb4b40cb-3453-4f52-b3c6-c9885a356736&amp;hsutk=791af6c68cd6c39c492e0ba9e8ee9b91&amp;canon=https%3A%2F%2Fehr.meditech.com%2Fabout%2Fcustomer-leaders&amp;utm_referrer=https%3A%2F%2Fehr.meditech.com%2Fnode%2F4089%2Fedit%3Fdestination%3Dadmin%2Fcontent&amp;portal_id=2897117&amp;redirect_url=APefjpFbmMSNr823is9XemCQBfR0XxKyVqIKxZGmdd_onvCEpKputdJb0Il7MFPM5-0pq0Fo7KIR-G22XW-wg5eYlGaApGJpQfl6R7d83c34cvJIoa92N5BigxUhisMSX71ErgwxUaTIBDIunw6r5EvEl7z6BfVtohWFDxdtrs58yj_ivSK3yly4DobYCdtu476S2dCfJY50Y7db71lwmlrOKnnLRZIO4oKpM6XvhzQOUxwjDyoEPgZbS0I_sIXDi0MAAPU6FCOg" style="" cta_dest_link="https://info.meditech.com/customer_success_stories" title="Share Your Story">Share Your Story<strong><br></strong></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript">hbspt.cta.load(2897117, "fbf32c3a-325a-4ad3-aac5-0410bdd3fcc7", {});</script></span><!-- end HubSpot Call-to-Action Code --></div>
          
			</div>
			<div class="container__one-half background--cover flex-order--reverse" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Community-of-people-customer-engagement--block1.jpg); min-height:350px;"></div>
		</div>
	</div>
	<!-- END Block 1 -->


	<!-- Start Block 2 (Podcasts) -->
	<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/bg--organic-shapes-green-blue-reversed.svg); background-position: top;">
		<div class="container__centered">
			<div class="container no-pad--top">
				<div class="container__one-fourth"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/podcasts/MEDITECH-Podcast--Apple--Resized-Web.png" alt="MEDITECH Podcasts logo"></div>
				<div class="container__three-fourths">
					<p class="header-micro">PODCASTS</p>
					<h2>Hear from important industry voices</h2>
					<p>Get the real scoop, straight from healthcare’s most innovative thought leaders, on <a href="<?php print $url; ?>/podcast-index">MEDITECH Podcasts</a>. With topics like <a href="https://meditech-podcast.simplecast.com/episodes/giving-clinicians-superpowers-with-cloud-solutions">Giving Clinicians Superpowers with Cloud Solutions</a> presented in a short, easily digestible format, you can quickly get personal perspectives on the challenges we all face.
					</p>
					<div class="share-icons no-target-icon">
						<p style="float:left; margin-right:0.5em;">Subscribe to the podcast:</p>
						<a href="https://podcasts.apple.com/us/podcast/meditech-podcast/id1566866668" style="border-bottom: none;" target="_blank" rel="noreferrer noopener"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/podcasts/apple-podcast--icon.png" alt="Apple Podcast Logo"></a>
						<a href="https://www.google.com/podcasts?feed=aHR0cHM6Ly9mZWVkcy5zaW1wbGVjYXN0LmNvbS9RTFYxZE93Ng%3D%3D" style="border-bottom: none;" target="_blank" rel="noreferrer noopener"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/podcasts/google-podcast--icon.png" alt="Google Podcast Logo"></a>
						<a href="https://open.spotify.com/show/7q7wbf2Q6J1y5IGItYrBxR" style="border-bottom: none;" target="_blank" rel="noreferrer noopener"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/podcasts/spotify--icon.png" alt="Spotify Logo"></a>
						<a href="https://meditech-podcast.simplecast.com/" style="border-bottom: none;" target="_blank" rel="noreferrer noopener"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/podcasts/simplecast--icon.png" alt="Simplecast Logo"></a>
					</div>
				</div>
			</div>

			<div class="container__one-half shadow-box center-mobile bg--gray">
				<p class="bold">Supporting Mental Health</p>
				<div>
					<iframe height="52px" width="100%" frameborder="no" scrolling="no" seamless src="https://player.simplecast.com/3cbeafb7-f04a-44d0-8b27-5b665d6618b7?dark=false"></iframe>
				</div>
				<p class="italic">
					“It doesn't matter what your diagnosis is, you are a human being first. How do we support you, provide treatment with you, and really empower you?”
				</p>
				<p class="quote-person bold">
					Sanaz Riahi, RN, PhD, VP <br>
					Ontario Shores Centre for Mental Health Sciences
				</p>
			</div>
			<div class="container__one-half shadow-box center-mobile bg--gray">
				<p class="bold">Prioritizing Patient Safety &amp; Equity</p>
				<div>
					<iframe height="52px" width="100%" frameborder="no" scrolling="no" seamless src="https://player.simplecast.com/34e50047-8d39-4719-8a3a-8a8acaa9e3b0?dark=false"></iframe>
				</div>
				<p class="italic">
					“Better care often exists in islands, little points of light that are only available to people who are in the location of an innovative practice. The challenge is how to make that true for everyone.”
				</p>
				<p class="quote-person bold">
					Jennifer Zelmer, PhD <br>
					President and CEO of Healthcare Excellence Canada
				</p>
			</div>
		</div>
	</div>
	<!-- End Block 2 (Podcasts) -->


	<!-- START Block 3 (Success Stories) -->
	<div class="container no-pad">
		<div class="gl-container">
			<div class="container__one-half background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Coworkers-celebrating-success-customer-engagement--block3.jpg); min-height:350px;">
			</div>
			<div class="container__one-half gl-text-pad bg--blue-gradient">
				<p class="header-micro">NEWS ARTICLES AND PRESS RELEASES</p>
				<h2>Recognize successes, large and small</h2>
				<p>Good healthcare strategies don’t have to be complicated or expensive to be impactful. <a href="<?php print $url; ?>/news">Read our news articles and press releases</a> to learn how MEDITECH customers (of all types and sizes) are investing in more innovative technologies and processes.</p>

				<div class="quote-box" style="padding:2em; margin-top: 2em;">
					<p class="italic">"MEDITECH is a longtime partner of <a href="<?php print $url; ?>/news/hca-healthcare-invests-in-meditech-expanse-in-new-hampshire"> HCA Healthcare </a>, and we are excited to continue working together through their next-generation software capabilities designed to better support our hospital teams and continue to improve care."
					</p>
					<p class="bold no-margin--bottom">
						— Marty Paslick, CIO, HCA Healthcare
					</p>
				</div>
			</div>
		</div>
	</div>
	<!-- END Block 3 (Success Stories) -->


	<!-- Start Block 4 (Case Studies) -->
	<div id="customers" class="container" style="margin: 3em 0;">
		<div class="container__centered">
			<div class="center" style="padding-bottom: 2em;">
				<p class="header-micro" style="display:inline;">CASE STUDIES</p>
				<h2>Use data to tell the whole story</h2>
				<p>Our <a href="<?php print $url; ?>/case-studies">Case Studies</a> use data to illustrate how our customers are making the journey from healthcare challenges to solution execution to real <span class="bold">results</span>. Learn what approaches your peers are taking, and how they can help inform new strategies at your organization.</p>
			</div>

			<div class="container__one-third shadow-box center-mobile">
				<div style="width:100%; float:left;">
					<div class="container__two-thirds">
						<p>
							<span class="bold"><a href="https://f.hubspotusercontent10.net/hubfs/2897117/Case_Studies/Newman_Regional_Customer_Success_Story.pdf">Newman Regional Health</a></span> Uses MEDITECH to Transform Sepsis Treatment
						</p>
					</div>
					<div class="container__one-third margin-mobile">
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--disease.svg" alt="hours down arrow icon" style="height:100px; width:100%;">
					</div>
				</div>
				<div style="width:100%; float:left; position:relative;">
					<a href="<?php print $url; ?>/ehr-solutions/meditech-professional-services"></a>
				</div>
			</div>

			<div class="container__one-third shadow-box center-mobile">
				<div class="container__two-thirds">
					<p><span class="bold"><a href="https://f.hubspotusercontent10.net/hubfs/2897117/Case_Studies/Lawrence_General_Customer_Success_Story.pdf">Lawrence General Hospital</a></span> Increases Physician Efficiency With MEDITECH's BCA Solution</p>
				</div>
				<div class="container__one-third margin-mobile">
					<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--physician-efficiency.svg" alt="percent up arrow icon" style="height: 100px; width:100%;">
				</div>
			</div>

			<div class="container__one-third shadow-box center-mobile">
				<div class="container__two-thirds">
					<p><span class="bold"><a href="https://info.meditech.com/en/kings-daughters-medical-center-gives-back-100-plus-hours-to-nurses-with-meditech-expanse-patient-care"> King's Daughters Medical Center</a></span> Saved 100+ hours by nurses over 6 months</p>
				</div>
				<div class="container__one-third margin-mobile">
					<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--hrs-given-back.svg" alt="hours up arrow icon" style="height: 100px; width:100%;">
				</div>
			</div>
		</div>
	</div>
	<!-- End Block 4 (Case Studies) -->


	<!-- Block 5 (Social Media) -->
	<div class="container bg--blue-gradient">
		<div class="container__centered">
			<div class="container__one-half" style="margin-top: 2em;">
				<p class="header-micro">SOCIAL MEDIA</p>
				<h2>Nurture social connections</h2>
				<p>Some of our most important communities (especially during the pandemic) use technology to transcend physical and geographical boundaries. Social media has made it possible for healthcare leaders everywhere to exchange new ideas and information - and even make connections that we wouldn’t have access to anywhere else.</p>
				<div class="share-icons no-target-icon" style="margin-top: 2em;">
					<p class="text--large" style="float: left; margin-right:0.5em;">Connect with us on social media!</p>
					<p>
						<a href="https://www.instagram.com/meditechehr/" style="border-bottom: none;"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/instagram-brands.svg" alt="instagram icon"></a>
						<a href="https://www.facebook.com/MeditechEHR" style="border-bottom: none;" target="_blank" rel="noreferrer noopener"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/facebook-brands.svg" alt="facebook icon"></a>
						<a href="https://twitter.com/MEDITECH" style="border-bottom: none;"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/twitter-brands.svg" alt="twitter icon">
						</a>
						<a href="https://www.linkedin.com/company/meditech/mycompany/verification/" style="border-bottom: none;"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/linkedin-brands.svg" alt="linkedin icon"></a>
					</p>
				</div>
			</div>

			<div class="container__one-half">

				<blockquote class="twitter-tweet">
					<p lang="en" dir="ltr">Thank you to my friends at Meditech!</p>&mdash; Julia Hanigsberg (@Hanigsberg) <a href="https://twitter.com/Hanigsberg/status/1463293457569624068?ref_src=twsrc%5Etfw">November 23, 2021</a>
				</blockquote>
				<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
			</div>
		</div>
	</div>
	<!-- END Block 5 -->


	<!-- Block 6 (Blog) -->
	<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/bg--organic-shapes-green-blue-reversed.svg); background-position: bottom; margin-top: 4em;">
		<div class="container__centered">
			<div class="container__one-half">
				<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Meditech-blog-illustration-customer-engagement.svg" style="margin-top: -5em;" class="blog people" alt="people reading the MEDITECH blog">
			</div>
			<div class="container__one-half shadow-box">
				<p class="header-micro" style="margin-top: 1em;">BLOG</p>
				<h2>Access unique perspectives</h2>
				<p>The award-winning <a href="https://blog.meditech.com/">MEDITECH Blog</a> provides a space for industry leaders to share their perspectives on delivering better care through clinical transformation. Readers can curate the content based on their own specialty and interests.</p>
				<p>Make sure to check out how <a href="https://blog.meditech.com/how-google-and-meditech-are-charting-a-new-course-in-digital-healthcare">Google and MEDITECH</a> are charting a new course in digital healthcare.</p>
			</div>
		</div>
	</div>
	<!-- END Block 6 (Blog) -->


	<!-- START Block 7 (MEDITECH Events)-->
	<div class="container calendar bg--white">
		<div class="container__centered">
			<div class="center" style="margin-bottom: 3em;">
				<p class="header-micro" style="display:inline;">MEDITECH EVENTS</p>
				<h2>Move forward together</h2>
				<p>Virtual, hybrid, and in-person events are bringing inspiration (as well as greater flexibility) to our customers across the world. By connecting as leaders, practitioners, and informaticists at the human level, we can join forces to discover the best solutions to our shared challenges.</p>
			</div>
			<div>
				<p class="bold text--large center" style="margin-bottom: 1.5em;">
					Don't forget to add these upcoming 2022 <a href="<?php print $url; ?>/events">events</a> to your calendar!
				</p>
			</div>
			<div class="gl-container">
				<div class="container__one-fourth cal-left bg--blue-gradient text--white">
					<p class="header-four bold">June 15 & 16, 2022</p>
				</div>
				<div class="container__three-fourths cal-right">
					<h4><a href="https://ehr.meditech.com/events/2022-clinical-informatics-symposium">Clinical Informatics Symposium</a> &mdash; Held Virtually</h4>
				</div>
			</div>
			<div class="gl-container">
				<div class="container__one-fourth cal-left bg--blue-gradient text--white">
					<p class="header-four bold" style="margin-top: 1.15em;">September 20-22, 2022</p>
				</div>
				<div class="container__three-fourths cal-right">
					<h4><a href="https://ehr.meditech.com/events/meditech-live">MEDITECH LIVE: Building Connections that Drive Change</a> &mdash; Held in our Foxborough Conference Center</h4>
				</div>
			</div>
		</div>
	</div>
	<!-- End Block 7 (MEDITECH Events) -->
	
	
  <!-- Block 9 CTA -->
  <div class="container bg--purple-gradient">
    <div class="container__centered center">

      <h2>Stories like these are what make healthcare work.</h2>
        <p>Tell us what you've done, and together we can amplify those successes.</p>
			<div class="button--hubspot"><!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-fbf32c3a-325a-4ad3-aac5-0410bdd3fcc7"><span class="hs-cta-node hs-cta-fbf32c3a-325a-4ad3-aac5-0410bdd3fcc7" id="hs-cta-fbf32c3a-325a-4ad3-aac5-0410bdd3fcc7" style="visibility: visible;" data-hs-drop="true"><a id="cta_button_2897117_04afc067-efcd-4e3f-9918-0bc02fbfbb02" class="cta_button " href="https://info.meditech.com/cs/c/?cta_guid=04afc067-efcd-4e3f-9918-0bc02fbfbb02&amp;signature=AAH58kH6RjL5oVm7kQKcrj-C9RRFtEV1xA&amp;placement_guid=fbf32c3a-325a-4ad3-aac5-0410bdd3fcc7&amp;click=cb4b40cb-3453-4f52-b3c6-c9885a356736&amp;hsutk=791af6c68cd6c39c492e0ba9e8ee9b91&amp;canon=https%3A%2F%2Fehr.meditech.com%2Fabout%2Fcustomer-leaders&amp;utm_referrer=https%3A%2F%2Fehr.meditech.com%2Fnode%2F4089%2Fedit%3Fdestination%3Dadmin%2Fcontent&amp;portal_id=2897117&amp;redirect_url=APefjpFbmMSNr823is9XemCQBfR0XxKyVqIKxZGmdd_onvCEpKputdJb0Il7MFPM5-0pq0Fo7KIR-G22XW-wg5eYlGaApGJpQfl6R7d83c34cvJIoa92N5BigxUhisMSX71ErgwxUaTIBDIunw6r5EvEl7z6BfVtohWFDxdtrs58yj_ivSK3yly4DobYCdtu476S2dCfJY50Y7db71lwmlrOKnnLRZIO4oKpM6XvhzQOUxwjDyoEPgZbS0I_sIXDi0MAAPU6FCOg" style="" cta_dest_link="https://info.meditech.com/customer_success_stories" title="Share Your Story">Share Your Story<strong><br></strong></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript">hbspt.cta.load(2897117, "fbf32c3a-325a-4ad3-aac5-0410bdd3fcc7", {});</script></span><!-- end HubSpot Call-to-Action Code --></div>

      <div class="center" style="margin-top:1em;">
        <?php print $share_link_buttons; ?>
      </div>

    </div>
  </div>
  <!-- End of Block 9 -->

</div>


<?php // SEO tool for internal use...
if( user_is_logged_in() ){
  print '<!-- SEO Tool is added to this div -->';
  print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
}
?>

<!-- end about--node-4167.php template -->
