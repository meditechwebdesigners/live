<?php // This template is set up to control the display of the 'Job Listing' content type 

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');
?>
<!-- start node--job-listing.tpl.php template -->

    <?php
      // look up node's taxonomy terms...

      // JOB CATEGORY...
      $categoryTags = field_view_field('node', $node, 'field_job_category'); 
      // 'field_job_category' is the machine name of the field in the content type that contains the taxonomy
      foreach($categoryTags['#items'] as $catTag){
        $catTerm = taxonomy_term_load($catTag['tid']);
        $category = $catTerm->name;
      }

      // STATE...
      $stateTags = field_view_field('node', $node, 'field_job_location'); 
      // 'field_meditech_location' is the machine name of the field in the content type that contains the taxonomy
      foreach($stateTags['#items'] as $sTag){
        $stateTerm = taxonomy_term_load($sTag['tid']);
        $state = $stateTerm->name;
      }

      // FACILITIES...
      $facilityTags = field_view_field('node', $node, 'field_job_facilities'); 
      // 'field_job_facilities' is the machine name of the field in the content type that contains the taxonomy
      $facilityLocations = array();
      foreach($facilityTags['#items'] as $facTag){
        $facTerm = taxonomy_term_load($facTag['tid']);
        $facilityLocations[] = $facTerm->name;
      }
    ?>

<?php 
// get publication date...
if($node->published_at){
  $published_date = date('Y-m-d', $node->published_at);
}
else { // default to current date...
  $published_date = date('Y-m-d');
}

// get state abbreviation...
switch($state){
  case 'Massachusetts, US':
    $abbr = 'MA';
    break;
  case 'Georgia, US':
    $abbr = 'GA';
    break;
  case 'Minnesota, US':
    $abbr = 'MN';
    break;
  case 'South Carolina, US':
    $abbr = 'SC';
    break;
  case 'Texas, US':
    $abbr = 'TX';
    break;
  case 'South Africa':
    $abbr = 'ZA';
    break;
  case 'Singapore':
    $abbr = 'SG';
    break;
  case 'Australia':
    $abbr = 'AU';
    break;
  case 'United Kingdom':
    $abbr = 'UK';
    break;
  default:
    $abbr = '';
}
?>
<!-- Google Structured Data -->
<script type="application/ld+json">
 {
  "@context" : "https://schema.org/",
  "@type" : "JobPosting",
  "title" : "<?php print $title; ?>",
  <?php 
  $job_description = str_replace("’", "'", strip_tags( render($content['field_job_description']) ) ); 
  $job_description = substr($job_description, 0, 300);
  ?>
  "description" : "<?php print $job_description; ?>",
  "datePosted" : "<?php print $published_date; ?>",
  "hiringOrganization" : {
    "@type" : "Organization",
    "name" : "Medical Information Technology, Inc.",
    "sameAs" : "https://ehr.meditech.com",
    "logo" : "https://ehr.meditech.com/sites/all/themes/meditech/images/meditech-logo_google-structured-data.png"
  },
  "jobLocation": {
    "@type": "Place",
    "address": [
      <?php 
      $n = 1;
      $number_of_facilities = count($facilityLocations);
      foreach($facilityLocations as $facility){ 
        // get location address and zip...
        switch($facility){
          case 'Canton':
            $address = '7 Blue Hill River Road';
            $zip = '02021';
            $country = 'USA';
            break;
          case 'Foxborough':
            $address = 'One Constitution Way';
            $zip = '02035';
            $country = 'USA';
            break;
          case 'Framingham':
            $address = '550 Cochituate Road';
            $zip = '01701';
            $country = 'USA';
            break;
          case 'Westwood (Lowder Brook)':
            $address = '100 Lowder Brook Drive';
            $zip = '02090';
            $country = 'USA';
            break;
          case 'Fall River':
            $address = '101 Martine Street';
            $zip = '02723';
            $country = 'USA';
            break;
          case 'Waltham':
            $address = '21 Hickory Drive';
            $zip = '02451';
            $country = 'USA';
            break;
          case 'Westwood':
            $address = 'MEDITECH Circle';
            $zip = '02090';
            $country = 'USA';
            break;
          case 'Atlanta':
            $address = 'One Northside 75';
            $zip = '30318';
            $country = 'USA';
            break;
          case 'Minnetonka':
            $address = '5130 County Road 101';
            $zip = '55345';
            $country = 'USA';
            break;
          case 'Liverpool':
            $address = '5130 County Road 101';
            $zip = '';
            $country = 'GBR';
            break;
          case 'Midrand':
            $address = 'Thandanani Office Park, Cnr Invicta Ave & Matuka Close';
            $zip = '';
            $country = 'ZAF';
            break;
          case 'North Ryde NSW 2113':
            $address = 'Suite 1.05, 32 Delhi Road';
            $zip = '';
            $country = 'AUS';
            break;
          case 'Wilkie Edge 228095':
            $address = '8 Wilkie Rd #03-08, Wilkie Edge';
            $zip = '228095';
            $country = 'SGP';
            break;
          default:
            $address = '';
            $zip = '';
        }
      ?>
        {
          "@type": "PostalAddress",
          "streetAddress": "<?php print $address; ?>",
          "addressLocality": "<?php print $facility; ?>",
          "addressRegion": "<?php print $abbr; ?>",
          <?php if($zip != ''){ ?>
            "postalCode": "<?php print $zip; ?>",
          <?php } ?>
          "addressCountry": "<?php print $country; ?>"
        }
      <?php 
        if($n < $number_of_facilities){
          print ',';
        }
      $n++;
      } 
      ?>
    ]
  }
}
</script>    

    <section class="container__centered">
	  <div class="container__two-thirds">
	  
		<h1 class="page__title js__seo-tool__title"><?php print $title; ?></h1>
     
        <div class="js__seo-tool__body-content">
      
          <p><?php print $category; ?> | <?php print $state; ?></p>

          <h5>Description</h5>
          <?php print render($content['field_job_description']); ?>

          <h5>What You Should Have</h5>
          <?php print render($content['field_job_requirements']); ?>

          <?php if( !empty($facilityLocations) ){ ?>
            <h5>This opportunity is available at our following location(s):</h5>
            <ul class="snippet__card__filters">
              <?php
              // adds Facility locations...
              foreach($facilityLocations as $fLocation){
                print "<li>".$fLocation."</li>";
              }
              ?>
            </ul>
          <?php } ?>

          <?php print render($content['field_job_shift']); ?>
          
          <div>
            <?php print $share_link_buttons; ?>
          </div>
        
        </div><!-- End .js__seo-tool__body-content -->
        

        
        <div style="clear:left; padding-top:30px">
         
        <?php
          // if a Minnesota job, then show apply button to take them to external site...
          if($state == 'Minnesota, US'){
        ?>
         
          <div class="btn-holder--content__callout">
            <a class="btn--orange" href="https://home.meditech.com/careers/survey.php?jobcode=<?php print render($content['field_job_code']); ?>">Submit Your Resume</a>
          </div>
          
        <?php
          }
          /*
          elseif($state == 'Georgia, US'){ // Georgia jobs use the Atlanta form which sends submissions to an Atlanta email...
        ?>
        
          <h2>Submit Your Resume</h2>

          <?php
            $jobForm = module_invoke('webform', 'block_view', 'client-block-2435');
            print render($jobForm['content']); 
          ?>
          
          <script>
            // make sure not to conflict with CMS's jQuery...
            var $jq = jQuery.noConflict();

            $jq(document).ready(function(){
              $jq('input[name="submitted[job_category]"]').attr('value', '<?php print $category; ?>');
              $jq('input[name="submitted[job_location]"]').attr('value', '<?php print $state; ?>');
              $jq('.webform-component-file').css('margin-top', '1em');
              $jq('#edit-submitted-resume-upload-button').css('display', 'none');
            });
            
            $jq('#webform-client-form-2435').submit(function(event){
              if( $jq('#edit-submitted-resume-upload').val() === '' ){
                $jq('#edit-submitted-resume-ajax-wrapper').append('<p style="color:red;">Resume must be uploaded.</p>');
                event.preventDefault();
              }
            });
          </script>
        
        <?php
          }
          */
          elseif($state == 'United Kingdom'){ // United Kingdom jobs use the UK form which sends submissions to an UK email...
        ?>
        
          <h2>Submit Your Resume</h2>
 
          <?php
            $jobForm = module_invoke('webform', 'block_view', 'client-block-3555');
            print render($jobForm['content']); 
          ?>
          
          <script>
            // make sure not to conflict with CMS's jQuery...
            var $jq = jQuery.noConflict();

            $jq(document).ready(function(){
              $jq('input[name="submitted[job_category]"]').attr('value', '<?php print $category; ?>');
              $jq('input[name="submitted[job_location]"]').attr('value', '<?php print $state; ?>');
              $jq('.webform-component-file').css('margin-top', '1em');
              $jq('#edit-submitted-resume-upload-button').css('display', 'none');
            });
            
            $jq('#webform-client-form-2435').submit(function(event){
              if( $jq('#edit-submitted-resume-upload').val() === '' ){
                $jq('#edit-submitted-resume-ajax-wrapper').append('<p style="color:red;">Resume must be uploaded.</p>');
                event.preventDefault();
              }
            });
          </script>
        
        <?php
          }
          else {
            // all Mass jobs go to this form and sent to a Mass email address...
        ?>
          <h2>Submit Your Resume</h2>

          <?php
            $jobForm = module_invoke('webform', 'block_view', 'client-block-2434');
            print render($jobForm['content']); 
          ?>
          
          <script>
            // make sure not to conflict with CMS's jQuery...
            var $jq = jQuery.noConflict();

            $jq(document).ready(function(){
              $jq('input[name="submitted[job_category]"]').attr('value', '<?php print $category; ?>');
              $jq('input[name="submitted[job_location]"]').attr('value', '<?php print $state; ?>');
              $jq('.webform-component-file').css('margin-top', '1em');
              $jq('#edit-submitted-resume-upload-button').css('display', 'none');
            });
            
            $jq('#webform-client-form-2434').submit(function(event){
              if( $jq('#edit-submitted-resume-upload').val() === '' ){
                $jq('#edit-submitted-resume-ajax-wrapper').append('<p style="color:red;">Resume must be uploaded.</p>');
                event.preventDefault();
              }
            });
          </script>

        <?php
          }
        ?>
        
        <?php
        if( $state == 'Minnesota, US' || $state == 'Georgia, US' || $state == 'Texas, US' || $state == 'Massachusetts, US' || $state == 'South Carolina, US'  ){
            print "<script type='text/javascript' src='https://click.appcast.io/pixels/generic1-15067.js?ent=84'></script>";   
        }
        ?>
        
        </div>

      </div>

      <?php // add CAREERS SECTION SIDE NAV ===================================================================== ?>
      <aside class="container__one-third panel">
        <div class="sidebar__nav">
          <?php
            $massnBlock = module_invoke('menu', 'block_view', 'menu-careers-section-side-nav');
            print render($massnBlock['content']); 
          ?>
        </div>        
      </aside>    

    </section>

    <?php
    // if video exists for node, then proceed to render DIV ======================================================
    // get value from field_video to pass to View (if video exists)...
    $video = render($content['field_video']);
    if( $state == 'Minnesota, US' && empty($video)  ){
      print '<!-- VIDEO -->';
      print '<div class="js__seo-tool__body-content">';
      $video_minn = 'Working at MEDITECH Minnesota';
      print views_embed_view('video_boxed', 'block', $video_minn); // adds 'video boxed' Views block...
      print '</div>';
      print '<!-- END VIDEO -->';
    }
    elseif( $state == 'Minnesota, US' && !empty($video) ){
      print '<!-- VIDEO -->';
      print '<div class="js__seo-tool__body-content">';
      // remove apostrophes from titles to prevent View from breaking...
      $video_filtered = str_replace("&#039;", "'", $video);
      // adds 'video' Views block...
      print views_embed_view('video_boxed', 'block', $video_filtered);
      print '</div>';
      print '<!-- END VIDEO -->';
    }
    elseif( !empty($video) ){
      print '<!-- VIDEO -->';
      print '<div class="js__seo-tool__body-content">';
      // remove apostrophes from titles to prevent View from breaking...
      $video_filtered = str_replace("&#039;", "'", $video);
      // adds 'video' Views block...
      print views_embed_view('video_boxed', 'block', $video_filtered);
      print '</div>';
      print '<!-- END VIDEO -->';
    }
    else{
      // no video
    }
    ?>

    <?php // SEO tool for internal use...
      if(node_access('update',$node)){
        print '<!-- SEO Tool is added to this div -->';
        print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds js__seo-tool"></div></div>';
      } 
    ?>
<!-- end node--job-listing.tpl.php template -->