<!-- START campaign--node-2398.php -- AMBULATORY -->
<?php
$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
	.header-micro {
		margin-top: 0;
	}
	
	.transparent-overlay--white {
		padding: 1em 2em;
	}

	@media all and (max-width: 30em) {
		.transparent-overlay--white {
			margin-bottom: 1em;
		}
	}

</style>

<div class="js__seo-tool__body-content">

	<!-- Block 1 -->
	<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/expanse--girl-with-binoculars-in-mountains.jpg);">
		<div class="container__centered">
			<div class="container__one-third">&nbsp;</div>
			<div class="container__two-thirds text--white">
				<div style="margin:5em 0;" class="transparent-overlay--xp">
					<h1 class="js__seo-tool__title" style="display:none;">MEDITECH Ambulatory</h1>
					<p class="header-micro">EXPANSE AMBULATORY CARE</p>
					<h2>Expand your view</h2>
					<p>Expand your view of what an EHR can be with Expanse Ambulatory, a new EHR for a new era. Expanse takes full advantage of mobile technology, untethering you from your desktop so you can tap and swipe through your charts more efficiently. Why shouldn't your EHR be as easy to use as your favorite apps?</p>
					<div class="center" style="margin-top:2em;">
						<?php hubspot_button($cta_code, "Download the Patient Registries White Paper"); ?>
					</div>

				</div>
			</div>
		</div>
	</div>
	<!-- End Block 1 -->


	<!-- Block 2 -->
	<div id="modal1" class="modal">
		<a class="close-modal" href="javascript:void(0)">&times;</a>
		<div class="modal-content">
			<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-ambulatory_home-screen--large.jpg" alt="MEDITECH Ambulatory home screen">
		</div>
	</div>

	<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/african-american-physician-with-patient.jpg);">
		<div class="container__centered">
			<div class="container__two-thirds">
				<p class="header-micro">PATIENT-CENTERED</p>
				<h2>Practice medicine (not computer science)</h2>
				<p>Stop spending more than half your work day interacting with the EHR and put your focus where it belongs — on patients. With its mobile, flexible, and intuitive design, MEDITECH's Ambulatory solution won't slow you down or get between you and your patients. They'll be happier. You'll be happier. It's time to <a href="https://ehr.meditech.com/ehr-solutions/meditechs-physician-productivity">get back to practicing medicine</a>.</p>
				<div class="open-modal tablet--white" data-target="modal1">
					<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-ambulatory_home-screen.jpg" alt="MEDITECH Ambulatory home screen">
					<div class="mag-bg">
						<i class="mag-icon fas fa-search-plus"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Block 2 -->


	<!-- Block 3 -->
	<div class="container background--cover hide__bg-image--mobile" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/female-physician-tablet-office-window.jpg); background-color:#E5E4C4; padding: 6em 0;">
		<div class="container__centered">
			<div class="container__one-third">&nbsp;</div>
			<div class="container__two-thirds">
				<p class="header-micro">SPECIALTY CARE</p>
				<h2>Your speciality is our specialty</h2>
				<p>We worked closely with a wide range of physicians to create customizable templates and standard content for over 40 specialties. With Expanse Ambulatory you get hundreds of templates, order sets, flowsheets, and customizable widgets that can be used right out of the box, or adjusted to your personal work style.</p>
			</div>
		</div>
	</div>
	<!-- End Block 3 -->


	<!-- Block 4 -->
	<!--
    <div id="modal2" class="modal">
        <a class="close-modal" href="javascript:void(0)">×</a>
        <div class="modal-content">
            <img src="< ?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-ambulatory_scheduling-grid--large.jpg" alt="MEDITECH Ambulatory scheduling screen">
        </div>
    </div>
    <div class="container bg--purple-gradient">
        <div class="container__centered">
            <div class="container__one-third">
                <h2>The power of one</h2>
                <p>We bring it all together for your ambulatory practice: One EHR across all care settings, ensuring a complete picture of health for each of your patients. A single schedule, combining your appointments across settings, rounding schedule, meetings, and more — all in one place. And a single patient bill for all care, wherever it occurred. Add to this accelerated eligibility checking, streamlined referral management, and advanced revenue cycle tools, and your practice will be running at peak efficiency.</p>
            </div>
            <div class="container__two-thirds">
                <div class="open-modal" data-target="modal2">
                    <div class="tablet--white">
                        <img src="< ?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-ambulatory_scheduling-grid.jpg" alt="MEDITECH Ambulatory scheduling screen">
                    </div>
                    <div class="mag-bg">
                        <i class="mag-icon fas fa-search-plus"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
-->
	<!--
    <div class="container background--cover hide__bg-image--mobile" style="background-image: url(< ?php print $url; ?>/sites/all/themes/meditech/images/campaigns/female-physician-tablet-office-window.jpg); background-color:#E5E4C4; padding: 6em 0;">
        <div class="container__centered">
            <div class="container__one-third">&nbsp;</div>
            <div class="container__two-thirds">
                <h2>A new EHR for a new era</h2>
                <p>Experience what a modern EHR should be: intuitive, fast, and easy to use. Expanse takes full advantage of mobile technology, untethering you from your desktop so you can tap and swipe through your charts more efficiently. Why shouldn't your EHR be as easy to use as your favorite apps?</p>

            </div>
        </div>
    </div>
-->
	<!-- End Block 4 -->


	<!-- Block 4 Virtual Assistant Block -->
	<div class="container no-pad">
		<div class="gl-container">
			<div class="container__one-half gl-text-pad bg--purple-gradient text--white">
				<p class="header-micro">VIRTUAL ASSISTANT</p>
				<h2>"OK, MEDITECH." <span style="font-weight: 400;">Make my life easier.</span></h2>
				<p>The most intuitive EHR for physicians just got easier. Now physicians can promptly find what they’re looking for in their EHR simply by asking. Powered by the same advanced AI you use on your personal devices, Expanse Virtual Assistant works just like you’d expect it to — lowering your cognitive load and allowing you to focus on your patients, not your devices.</p>
				<div style="margin-top: 1.5em;">
					<?php hubspot_button('fca506cd-8f0c-4f7e-9b94-6026b7d3d4ce', "Learn More"); ?>
				</div>
			</div>
			<div class="container__one-half background--cover flex-order--reverse" style="background-image: url( <?php print $url; ?>/sites/all/themes/meditech/images/campaigns/doctor-using-virtual-assistant-speaking-with-patients.jpg); min-height:350px;">
			</div>
		</div>
	</div>
	<!-- Virtual Assistant Block -->


	<!-- Block 5 New Expanse Now Block -->

	<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/about/bg--organic-shapes-green-right-2.svg);">
		<div class="container__centered">
			<div class="container__one-third">
				<div class="expanse-logo">
					<img src="https://ehr.meditech.com/sites/all/themes/meditech/images/expanse-now-logo.png" alt="MEDITECH Expanse logo" style="margin-top: 3em;">
				</div>
			</div>
			<div class="container__two-thirds">
				<p class="header-micro">PHYSICIAN APP</p>
				<h2>Stay connected with Expanse Now</h2>
				<p>No matter how efficient your EHR is, we know there are times you need to access patient information outside of the office. That’s why we created Expanse Now, our physician mobility app for Android and iOS smartphones. Securely access your Expanse EHR wherever you are, using intuitive mobile device conventions and voice commands. Remotely manage routine tasks from the palm of your hand, and ensure the highest levels of care coordination and communication with your patients and care teams. <a href="https://ehr.meditech.com/ehr-solutions/ehr-mobility">Learn more</a>.</p>

			</div>
		</div>
	</div>
	<!-- End of Expanse Now Block -->

	<!-- Block 6 -->
	<div class="content__callout border-none">
		<div class="content__callout__media">
			<div class="content__callout__image-wrapper">
				<div class="video js__video" data-video-id="239126049">
					<figure class="video__overlay">
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/meditech-web-ambulatory-video-still.jpg" alt="Doctors talk about MEDITECH Expanse Ambulatory Video Covershot">
					</figure>
					<a class="video__play-btn" href="https://vimeo.com/239126049"></a>
					<div class="video__container"></div>
				</div>
			</div>
		</div>
		<div class="content__callout__content">
			<div class="content__callout__body">
				<div class="content__callout__body__text">
					<p class="header-micro">PERSPECTIVES</p>
					<h2>What docs are saying about our Ambulatory product</h2>
					<p>Hear about the features practicing physicians love in our Ambulatory solution — from personal widgets, to mobility, to the unsurpassed level of integration it offers.</p>
				</div>
			</div>
		</div>
	</div>
	<!-- End Block 6 -->


	<!-- Block 7 -->
	<div class="container no-pad">
		<div class="gl-container">
			<div class="container__one-half gl-text-pad bg--purple-gradient text--white">
				<p class="header-micro">CARE COORDINATION</p>
				<h2>Go team!</h2>
				<p>Whether you're a physician, nurse, PA, NP, or any other member of the care team, you need an ambulatory solution that supports a team approach. Expanse improves communication and care coordination, with secure messaging and texting, multidisciplinary care plans, and dynamic patient registries for tracking patients as they cross settings. A unified patient record with robust tools for care coordination ensures that your team is always working from the same playbook.</p>
			</div>
			<div class="container__one-half background--cover flex-order--reverse" style="background-image: url( <?php print $url; ?>/sites/all/themes/meditech/images/campaigns/clinician-team-group-shot.jpg); min-height:350px; background-position: top;">
			</div>
		</div>
	</div>

	<!--
    <div class="container background--cover hide__bg-image--mobile" style="background-image: url(< ?php print $url; ?>/sites/all/themes/meditech/images/campaigns/clinician-team-group-shot.jpg); background-color:#1FB2AC;">
        <div class="container__centered">
            <div class="container__one-third text--white" style="padding:3em 0;">
                <p class="header-micro">CARE COORDINATION</p>
                <h2>Go team!</h2>
                <p>Whether you're a physician, nurse, PA, NP, or any other member of the care team, you need an ambulatory solution that supports a team approach. Expanse improves communication and care coordination, with secure messaging and texting, multidisciplinary care plans, and dynamic patient registries for tracking patients as they cross settings. A unified patient record with robust tools for care coordination ensures that your team is always working from the same playbook.</p>
            </div>
        </div>
    </div>
-->
	<!-- End Block 7 -->


	<!-- Block 7 
    <div id="modal3" class="modal">
        <a class="close-modal" href="javascript:void(0)">×</a>
        <div class="modal-content">
            <img src="< ?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-ambulatory-registries--large.jpg" alt="MEDITECH Ambulatory Registry screen">
        </div>
    </div>
    <div class="container background--cover" style="background-image: url(< ?php print $url; ?>/sites/all/themes/meditech/images/campaigns/foot-traffic--beige.jpg);">
        <div class="container__centered">
            <div class="container__one-third">
                <div class="transparent-overlay--white">
                    <h2>A higher level of care</h2>
                    <p>With our Ambulatory solution, you can do more than just serve the patients you see that day. Take your care up a level and positively impact entire populations with dynamic patient registries &mdash; responsive browser-based lists that are updated the moment data in the EHR changes. Sort patients by common characteristics, quickly identify problem areas, and intervene before minor issues become serious. It's the tool you need to keep your healthy patients well and manage your chronically ill patients more effectively.</p>
                </div>
            </div>
            <div class="container__two-thirds center">
                <div class="open-modal" data-target="modal3">
                    <div class="tablet--white">
                        <img src="< ?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-ambulatory-registries.jpg" alt="MEDITECH Ambulatory Registry Screen shot">
                    </div>
                    <div class="mag-bg">
                        <i class="mag-icon fas fa-search-plus"></i>
                    </div>
                </div>
                <p style="margin-top:1em;">See the power of Patient Registries in our <a href="https://info.meditech.com/meditechs-building-a-foundation-for-population-health-patient-registries-white-paper">white paper: Building a Foundation for Population Health</a>.</p>
            </div>
        </div>
    </div>
   End Block 7 -->


	<!-- Block 8 -->
	<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/about/bg--organic-shapes-green-full-width.svg);">
		<div class="container__centered">
			<div class="auto-margins center">
				<p class="header-micro" style="display: inline;">PERSONALIZATION</p>
				<h2>Make it yours</h2>
				<p>In healthcare, one size rarely fits all. You need a personalized EHR designed around your unique workflows and practice patterns, with your own widgets, preferences, and shortcuts built in. Our Ambulatory solution includes out-of-the-box content across more than 40 specialities, including documentation templates, protocols, order sets, and much more — all based on the most current clinical evidence. You take it from there and make it your own. The result: lower stress, higher efficiency, <a href="https://blog.meditech.com/how-to-reduce-burnout-with-a-physician-driven-ehr-design">less burnout</a>. That's better for you and your patients.</p>
			</div>
			<div class="container__one-third center">
				<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-ambulatory_widget-patient.jpg" alt="MEDITECH Ambulatory patient widget">
			</div>
			<div class="container__one-third center">
				<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-ambulatory_widget-cardiovascular-risk.jpg" alt="MEDITECH Ambulatory cardiovascular risk widget">
			</div>
			<div class="container__one-third center">
				<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-ambulatory_widget-pfsh.jpg" alt="MEDITECH Ambulatory PFSH widget">
			</div>
		</div>
	</div>
	<!-- End Block 8 -->


	<!-- Block 9 -->
	<div id="modal4" class="modal">
		<a class="close-modal" href="javascript:void(0)">×</a>
		<div class="modal-content">
			<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-ambulatory-registries--large.jpg" alt="MEDITECH Ambulatory Registry screen">
		</div>
	</div>
	<div class="container bg--purple-gradient">
		<div class="container__centered">
			<div class="container__one-third">
				<p class="header-micro">CARE MANAGEMENT</p>
				<h2>Guide your patients to healthier outcomes</h2>
				<p>Identify and meet the needs of your populations while closing care gaps with Expanse Care Compass, our integrated, web-based solution for care managers. Whether patients are managing chronic conditions or working to maintain wellness, you can help them to reach their goals — improving patient outcomes, reducing costs, and driving value-based reimbursement.<br><a href="<?php print $url; ?>/ehr-solutions/care-compass">Learn More about Expanse Care Compass</a></p>
			</div>
			<div class="container__two-thirds">
				<div class="open-modal" data-target="modal4">
					<div class="tablet--white">
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-ambulatory-registries.jpg" alt="MEDITECH Ambulatory Registry Screen shot">
					</div>
					<div class="mag-bg">
						<i class="mag-icon fas fa-search-plus"></i>
					</div>
				</div>
				<p style="margin-top:1em;">See the power of Patient Registries, included in Expanse Care Compass, in our <a href="<?php print $url; ?>/meditechs-building-a-foundation-for-population-health-patient-registries-white-paper">white paper: Building a Foundation for Population Health</a>.</p>
			</div>
		</div>
	</div>
	<!-- End Block 9 -->


	<!-- Block 10 -->
	<div id="modal5" class="modal">
		<a class="close-modal" href="javascript:void(0)">×</a>
		<div class="modal-content">
			<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-patient-portal-screen-shot--glucose--large.png" alt="MEDITECH mobile patient portal Screenshot">
		</div>
	</div>
	<div class="container background--cover hide__bg-image--mobile" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/older-couple-on-couch-with-laptop.jpg); background-color:#864A0E;">
		<div class="container__centered">
			<div class="container__two-thirds">
				<div class="container__one-half transparent-overlay--xp text--white">
					<p class="header-micro">PATIENT EXPERIENCE</p>
					<h2>Be more engaging</h2>
					<p>Engaged patients become active partners in their own care. Encourage them with smart tools like our <a href="https://ehr.meditech.com/ehr-solutions/patient-portal">Patient and Consumer Health Portal</a> and <a href="https://ehr.meditech.com/news/mhealth-meditech-in-the-app-store">MHealth app</a>, which enable patients to easily schedule appointments, pay bills, print immunization records, and securely communicate with their providers. They can even connect their fitness devices and apps. You can also offer your patients the ultimate convenience: care wherever they are, via our secure virtual visit technology.</p>
				</div>
				<div class="container__one-half center">
					<div class="open-modal" data-target="modal5">
						<div class="phone--white">
							<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-patient-portal-screen-shot--glucose.png" alt="MEDITECH mobile patient portal Screenshot">
							<div class="mag-bg">
								<i class="mag-icon fas fa-search-plus"></i>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Block 10 -->



	<!-- Block 11 New Efficiency Dashboard Block -->
	<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/back-angle-runner-on-road4.png);">
		<div class="container__centered">
			<div class="container__one-half transparent-overlay--xp text--white">
				<p class="header-micro" style="display: inline;">EFFICIENCY</p>
				<h2>Full speed ahead</h2>
				<p>Empower your physicians to get the most from their EHR with the Expanse Efficiency Dashboard. With detailed user metrics and real-time reporting, the dashboard offers actionable insights for adjusting workflows and personalizing the EHR experience to each provider’s needs. The result is happier physicians who get their work done on time and focus on what’s most important to them &mdash; their patients. Identify opportunities for targeted training, EHR personalization, and individualized support, so you can ensure confident, proficient, satisfied users. </p>
			</div>
			<div class="container__one-half">
				&nbsp;
			</div>
		</div>
	</div>
	<!-- End Efficiency Dashboard Block -->
	
	<!-- Block 12 New Practice Management block-->
	<div class="container bg--purple-gradient">
		<div class="container__centered text--white">

			<div class="center auto-margins">
				<p class="header-micro" style="display: inline;">EXPANSE EVERYWHERE</p>
				<h2>For those who integrate</h2>
				<p>Practices associated with health systems using Expanse across the enterprise reap additional benefits from MEDITECH’s industry-leading integration.</p>
			</div>

			<div class="container center no-pad--top">
				<div class="gl-container bg--emerald">

					<div class="container__one-third bg--meditech-green">
						<img style="width: 40%;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Complete-Picture--Icon.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Complete-Picture--Icon.png';this.onerror=null;" alt="Complete Picture icon">
						<h3>One Record</h3>
						<h4>across the enterprise</h4>
						<p>including physician practices, hospitals and acute facilities, home care, long term care, and beyond</p>
					</div>
					<div class="container__one-third bg--white">
						<img style="width: 40%;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/schedule--icon.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/schedule--icon.png';this.onerror=null;" alt="Schedule on Computer icon">
						<h3>One Schedule</h3>
						<h4>across the enterprise</h4>
						<p>including provider appointments, rounding schedules, meetings, and more</p>
					</div>
					<div class="container__one-third">
						<img style="width: 40%;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/statement--icon.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/statement--icon.png';this.onerror=null;" alt="Complete Picture icon">
						<h3>One Bill</h3>
						<h4>for patient care</h4>
						<p>anywhere in the enterprise combined on one patient-friendly statement</p>
					</div>

				</div>
			</div>

		</div>
	</div>
	<!--End of New Practice Management block-->
</div>
<!-- end js__seo-tool__body-content -->

<!-- Block 8 - CTA Block -->
<div class="container bg--white">
	<div class="container__centered center">

		<?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
		<h2>
			<?php print $cta->field_header_1['und'][0]['value']; ?>
		</h2>
		<?php } ?>

		<?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
		<div>
			<?php print $cta->field_long_text_1['und'][0]['value']; ?>
		</div>
		<?php } ?>

		<div class="center" style="margin-top:2em;">
			<?php hubspot_button($cta_code, "Sign up for the Ambulatory Webinar"); ?>
		</div>

		<div style="margin-top:1em;">
			<?php print $share_link_buttons; ?>
		</div>

	</div>
</div>
<!-- End Block 8 -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>

<!-- END campaign--node-2398.php -->
