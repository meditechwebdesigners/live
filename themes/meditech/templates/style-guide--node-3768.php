<?php if( user_is_logged_in() ){ ?>
<!-- START style-guide--node-3768.php Buttons  -->

<!--Javascript & CSS reference for Prism-->
<script type="text/javascript" src="<?php print $url; ?>/sites/all/themes/meditech/js/prism.js"></script>

<link href="<?php print $url; ?>/sites/all/themes/meditech/css/prism.css" rel="stylesheet" />

<style>
	/*	Buttons */
	.btn--dark-orange,
	.btn--dark-orange-rd,
	.btn--dark-green,
	.btn--dark-green-rd,
	.btn--dark-purple,
	.btn--light-purple,
	.btn--light-purple-rd,
	.btn--green-outline-light,
	.btn--green-outline-rd-light,
	.btn--green-outline-dark,
	.btn--green-outline-rd-dark,
	.btn--orange-outline-light,
	.btn--orange-outline-dark,
	.btn--orange-outline-rd-light,
	.btn--orange-outline-rd-dark,
	.btn--light-gray,
	.btn--light-gray-rd {
		transition: all 300ms ease-in-out;
	}

	a.btn--dark-orange,
	a.btn--dark-green,
	a.btn--dark-purple,
	a.btn--light-purple,
	a.btn--green-outline-light,
	a.btn--green-outline-dark,
	a.btn--orange-outline-light,
	a.btn--orange-outline-dark,
	a.btn--light-gray {
		border-radius: 30px;
		border: none;
		font-size: 16px;
		font-weight: 700;
		line-height: 1.5;
		font-family: "source-sans-pro", helvetica, arial, sans-serif;
		display: inline-block;
		text-align: center;
		padding: .85em 1.5em;
		transition: all 300ms ease-in-out;
	}

	a.btn--light-purple-rd,
	a.btn--dark-green-rd,
	a.btn--dark-orange-rd,
	a.btn--light-gray-rd,
	a.btn--orange-outline-rd-light,
	a.btn--orange-outline-rd-dark,
	a.btn--green-outline-rd-light,
	a.btn--green-outline-rd-dark {
		border-radius: 7px;
		border: none;
		font-size: 16px;
		font-weight: 700;
		line-height: 1.5;
		font-family: "source-sans-pro", helvetica, arial, sans-serif;
		display: inline-block;
		text-align: center;
		padding: 1em 1.5em;
		transition: all 300ms ease-in-out;
	}

	a.btn--dark-orange{
		background-color: #dd6720;
		color: #fff;
	}
	
	a.btn--dark-orange-rd {
		background-color: #FF9370;
		color: #3e4545;
	}
	
	.btn--dark-orange-rd:hover {
		color: #3e4545 !important;
		border-bottom: none !important;
		background-color: #e88667;
		outline: none;
	}

	.btn--dark-orange:hover {
		color: #fff !important;
		border-bottom: none !important;
		background-color: #c85d1c;
		outline: none;
	}

	a.btn--dark-purple {
		background-color: #5d21d0;
		color: #fff;
	}

	.btn--dark-purple:hover {
		color: #fff !important;
		border-bottom: none !important;
		background-color: #521db9;
		outline: none;
	}

	a.btn--light-gray,
	a.btn--light-gray-rd {
		background-color: #F3F5F9; 
		color:#612BF7;
	}
	
	a.btn--light-gray:hover,
	a.btn--light-gray-rd:hover {
		background-color: #e6e9ee; 
	}
	
	a.btn--light-purple,
	a.btn--light-purple-rd {
		background-color: #612BF7;
		color: #fff;
	}

	.btn--light-purple:hover,
	.btn--light-purple-rd:hover {
		color: #fff !important;
		border-bottom: none !important;
		background-color: #5726df;
		outline: none;
	}

	a.btn--dark-green,
	a.btn--dark-green-rd {
		background-color: #087E68;
		color: #fff;
	}

	.btn--dark-green:hover,
	.btn--dark-green-rd:hover {
		color: #fff !important;
		background-color: #066a57;
		border-bottom: none !important;
		outline: none;
	}

	a.btn--green-outline-light {
		color: #3e4545;
		border: 3px solid #00bc6f;
	}
	
	a.btn--green-outline-rd-light {
		color: #087E68;
		border: 3px solid #087E68;
	}
	
	.btn--green-outline-rd-light:hover,
	.btn--green-outline-light:hover {
		border: 3px solid #3e4545;
		color: #3e4545;
		transition: all 300ms ease-in-out;
	}

	a.btn--green-outline-dark {
		border: 3px solid #00bc6f;
		color: #fff;
	}
	
	a.btn--green-outline-rd-dark {
		border: 3px solid #087E68;
		color: #fff;
	}

	a.btn--orange-outline-light,
	a.btn--orange-outline-rd-light {
		border: 3px solid #FF8300;
		color: #3e4545;
	}

	a.btn--orange-outline-dark,
	a.btn--orange-outline-rd-dark {
		border: 3px solid #FF8300;
		color: #fff;
	}

	.btn--green-outline-rd-light:hover,
	.btn--orange-outline-rd-light:hover {
		border: 3px solid #3e4545;
		transition: all 300ms ease-in-out;
	}

	.btn--green-outline-dark:hover,
	.btn--green-outline-rd-dark:hover,
	.btn--orange-outline-rd-dark:hover {
		border: 3px solid #fff !important;
		color: #fff !important;
		transition: all 300ms ease-in-out;
	}

	.btn--orange-outline-light:hover {
		border: 3px solid #3e4545;
		transition: all 400ms ease-in-out;
	}

	.btn--orange-outline-dark:hover {
		border: 3px solid #fff !important;
		color: #fff !important;
		transition: all 400ms ease-in-out;
	}

</style>

<section class="container__centered">

	<h1 class="page__title">
		<?php print $title; ?>
	</h1>

	<div class="container">
		<h2>Character Limits</h2>

		<div class="container no-pad">
			<div class="container__one-third bg--light-gray" style="padding: 1em; margin-top: 2em;">
				<p class="bold">One Third Limit: 37</p>
				<a href="#" role="button" class="btn--dark-orange">The quick brown fox jumps over the la</a>
			</div>
		</div>

		<div class="container no-pad">
			<div class="container__one-half bg--light-gray" style="padding: 1em; margin-top: 2em;">
				<p class="bold">One Half Limit: 63</p>
				<a href="#" role="button" class="btn--dark-orange">The quick brown fox jumps over the lazy dog the quick brown fox</a>
			</div>
		</div>

		<div class="container no-pad">
			<div class="container__one-half bg--light-gray" style="padding: 1em; margin-top: 2em;">
				<p class="bold">Reccomended: 38</p>
				<p>This is the most amount of characters that will stay on a single line up until the 800px breakpoint (in a half container with 1em padding).</p>
				<a href="#" role="button" class="btn--dark-orange">The quick brown fox jumps over the laz</a>
			</div>
		</div>

	</div>

	<h2>New Button Styles</h2>

	<div class="container no-pad--bottom center">
		<div class="container__one-half" style="margin-bottom: 2em; padding: 2em;">
			<p class="bold">Dark Orange Pill - #DD6720</p>
			<a href="#" role="button" class="btn--dark-orange">Sign up for the Expanse Oncology Webinar</a>
		</div>
		<div class="container__one-half bg--black-coconut" style="padding: 2em; margin-bottom: 2em;">
			<p class="bold">Dark Orange Pill - #DD6720</p>
			<a href="#" role="button" class="btn--dark-orange">Sign up for the Expanse Oncology Webinar</a>
		</div>
	</div>
	
	<div class="container no-pad--bottom center">
		<div class="container__one-half" style="margin-bottom: 2em; padding: 2em;">
			<p class="bold">Dark Orange Rounded - #DD6720</p>
			<a href="#" role="button" class="btn--dark-orange-rd">Sign up for the Expanse Oncology Webinar</a>
		</div>
		<div class="container__one-half bg--black-coconut" style="padding: 2em; margin-bottom: 2em;">
			<p class="bold">Dark Orange Rounded - #DD6720</p>
			<a href="#" role="button" class="btn--dark-orange-rd">Sign up for the Expanse Oncology Webinar</a>
		</div>
	</div>
	
	<hr>

	<div class="container no-pad--bottom center">
		<div class="container__one-half" style="margin-bottom: 2em; padding: 2em;">
			<p class="bold">Dark Orange Outline Pill - #DD6720</p>
			<a href="#" role="button" class="btn--orange-outline-light">Sign up for the Expanse Oncology Webinar</a>
		</div>
		<div class="container__one-half bg--black-coconut" style="padding: 2em; margin-bottom: 2em;">
			<p class="bold">Dark Orange Outline Pill - #DD6720</p>
			<a href="#" role="button" class="btn--orange-outline-dark">Sign up for the Expanse Oncology Webinar</a>
		</div>
	</div>

	<div class="container no-pad--bottom center">
		<div class="container__one-half" style="margin-bottom: 2em; padding: 2em;">
			<p class="bold">Dark Orange Outline Rounded - #DD6720</p>
			<a href="#" role="button" class="btn--orange-outline-rd-light">Sign up for the Expanse Oncology Webinar</a>
		</div>
		<div class="container__one-half bg--black-coconut" style="padding: 2em; margin-bottom: 2em;">
			<p class="bold">Dark Orange Outline Rounded - #DD6720</p>
			<a href="#" role="button" class="btn--orange-outline-rd-dark">Sign up for the Expanse Oncology Webinar</a>
		</div>
	</div>

	<hr>


	<div class="container no-pad--bottom center">
		<div class="container__one-half" style="margin-bottom: 2em; padding: 2em;">
			<p class="bold">Emerald Pill - #087E68</p>
			<a href="#" role="button" class="btn--dark-green">Sign up for the Expanse Oncology Webinar</a>
		</div>
		<div class="container__one-half bg--black-coconut" style="padding: 2em; margin-bottom: 2em;">
			<p class="bold">Emerald Pill - #087E68</p>
			<a href="#" role="button" class="btn--dark-green">Sign up for the Expanse Oncology Webinar</a>
		</div>
	</div>
	
	<div class="container no-pad--bottom center">
		<div class="container__one-half" style="margin-bottom: 2em; padding: 2em;">
			<p class="bold">Emerald - Rounded - #087E68</p>
			<a href="#" role="button" class="btn--dark-green-rd">Sign up for the Expanse Oncology Webinar</a>
		</div>
		<div class="container__one-half bg--black-coconut" style="padding: 2em; margin-bottom: 2em;">
			<p class="bold">Emerald - Rounded - #087E68</p>
			<a href="#" role="button" class="btn--dark-green-rd">Sign up for the Expanse Oncology Webinar</a>
		</div>
	</div>
	
	<hr>

	<div class="container no-pad--bottom center">
		<div class="container__one-half" style="margin-bottom: 2em; padding: 2em;">
			<p class="bold">MT Green Outline Pill - #00BC6F</p>
			<a href="#" role="button" class="btn--green-outline-light">Sign up for the Expanse Oncology Webinar</a>
		</div>
		<div class="container__one-half bg--black-coconut" style="padding: 2em; margin-bottom: 2em;">
			<p class="bold">MT Green Outline Pill - #00BC6F</p>
			<a href="#" role="button" class="btn--green-outline-dark">Sign up for the Expanse Oncology Webinar</a>
		</div>
	</div>

	<div class="container no-pad--bottom center">
		<div class="container__one-half" style="margin-bottom: 2em; padding: 2em;">
			<p class="bold">Emerald Outline Rounded - #087E68</p>
			<a href="#" role="button" class="btn--green-outline-rd-light">Sign up for the Expanse Oncology Webinar</a>
		</div>
		<div class="container__one-half bg--black-coconut" style="padding: 2em; margin-bottom: 2em;">
			<p class="bold">Emerald Outline Rounded - #087E68</p>
			<a href="#" role="button" class="btn--green-outline-rd-dark">Sign up for the Expanse Oncology Webinar</a>
		</div>
	</div>
	
	<hr>
	
	<div class="container no-pad--bottom center">
		<div class="container__one-half" style="margin-bottom: 2em; padding: 2em;">
			<p class="bold">Purple Pill - #5D21D0</p>
			<a href="#" role="button" class="btn--dark-purple">Sign up for the Expanse Oncology Webinar</a>
		</div>
		<div class="container__one-half bg--black-coconut" style="padding: 2em; margin-bottom: 2em;">
			<p class="bold">Purple Pill - #5D21D0</p>
			<a href="#" role="button" class="btn--dark-purple">Sign up for the Expanse Oncology Webinar</a>
		</div>
	</div>
	
	<hr>
	
	<div class="container no-pad--bottom center">
		<div class="container__one-half" style="margin-bottom: 2em; padding: 2em;">
			<p class="bold">Bluish Purple Pill - #612BF7</p>
			<a href="#" role="button" class="btn--light-purple">Sign up for the Expanse Oncology Webinar</a>
		</div>
		<div class="container__one-half bg--black-coconut" style="padding: 2em; margin-bottom: 2em;">
			<p class="bold">Bluish Purple Pill - #612BF7</p>
			<a href="#" role="button" class="btn--light-purple">Sign up for the Expanse Oncology Webinar</a>
		</div>
	</div>

	<div class="container no-pad--bottom center">
		<div class="container__one-half" style="margin-bottom: 2em; padding: 2em;">
			<p class="bold">Bluish Purple Rounded - #612BF7</p>
			<a href="#" role="button" class="btn--light-purple-rd">Sign up for the Expanse Oncology Webinar</a>
		</div>
		<div class="container__one-half bg--black-coconut" style="padding: 2em; margin-bottom: 2em;">
			<p class="bold">Bluish Purple Rounded - #612BF7</p>
			<a href="#" role="button" class="btn--light-purple-rd">Sign up for the Expanse Oncology Webinar</a>
		</div>
	</div>

	<hr>
	
	<div class="container no-pad--bottom center">
		<div class="container__one-half" style="margin-bottom: 2em; padding: 2em;">
			<a href="#" role="button" class="btn--light-gray-rd" style="margin-right:1em;">Cancel</a>
			
			<a href="#" role="button" class="btn--light-purple-rd">Submit</a>
		</div>
		
		<div class="container__one-half" style="margin-bottom: 2em; padding: 2em;">
			<a href="#" role="button" class="btn--light-gray" style="margin-right:1em;">Cancel</a>
			
			<a href="#" role="button" class="btn--light-purple">Submit</a>
		</div>
	</div>
	
	<div class="container no-pad--bottom center">
		<div class="container__one-half" style="margin-bottom: 2em; padding: 2em;">
			<a href="#" role="button" class="btn--green-outline-rd-light" style="border: 3px solid #087E68; color:#087E68; padding: .85em 1.5em; margin-right:1em;">Cancel</a>
			
			<a href="#" role="button" class="btn--dark-green-rd">Submit</a>
		</div>
		
		<div class="container__one-half" style="margin-bottom: 2em; padding: 2em;">
			<a href="#" role="button" class="btn--green-outline-light" style="border: 3px solid #087E68; color:#087E68; padding: .7em 1.5em; margin-right:1em;">Cancel</a>
			
			<a href="#" role="button" class="btn--dark-green">Submit</a>
		</div>
	</div>


	<!-- SIDEBAR -->
	<!--
	<aside class="container__one-third">

		<div class="sidebar__nav panel">
			<?php
        $styleBlock = module_invoke('menu', 'block_view', 'menu-style-guide-side-nav');
        print render($styleBlock['content']); 
        ?>
		</div>

	</aside>
-->
	<!-- END SIDEBAR -->

</section>

<!-- END style-guide--node-3768.php Buttons -->
<?php } ?>
