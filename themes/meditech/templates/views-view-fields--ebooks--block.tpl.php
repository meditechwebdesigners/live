<!-- start views-view-fields--ebooks--block.tpl.php template -->
<?php 
// This template is for each row of the Views block: EBOOKS PAGE ....................... 
// Get the nid
$nid = $fields['nid']->content;
?>

<figure class="container no-pad">
  <div class="container__one-third">
  <?php
    if( !empty($fields['field_image']->content) ){ 
      // get image data...
      $image_data = $fields['field_image']->content;
      // example data result: <img typeof="foaf:Image" src="https://at_drudev.meditech.com/drupal/sites/default/files/images/campaigns/<FILENAME>.jpg" width="800" height="250" />
      $image_HTML_array = explode('/', $image_data);
      // grab this part: MEDITECH-surgical-services_grid-short.jpg" width="800" height="250" ...
      // oddly it removed the < and />, but there is a trailing space...
      $image_HTML_ending = $image_HTML_array[8];
      // find the first set of quotes...
      $first_quotes = strpos($image_HTML_ending, '"');
      // remove everything from the first set of quotes back...
      $image_filename = substr($image_HTML_ending, 0, $first_quotes);

      print '<a class="ebooks_link_gae" href="'.$fields['path']->content.'"><img src="https://ehr.meditech.com/sites/default/files/images/ebook/'.$image_filename.'" alt="ebook cover image"></a>';
    }
    else{
      print '<a class="ebooks_link_gae" href="'.$fields['path']->content.'"><img src="https://ehr.meditech.com/sites/all/themes/meditech/images/news/MEDITECH-ebook--thumbnail.jpg" alt="white paper cover image"></a>';
    }
    ?>
  </div>
  <figcaption class="container__two-thirds">
    <h3 class="header-four no-margin"><a class="ebooks_link_gae" href="<?php print $fields['path']->content; ?>"><?php print $fields['title']->content; ?></a></h3>
    <p><?php print $fields['field_summary']->content; ?></p>
  </figcaption>
</figure>

<?php 
if( user_is_logged_in() ){ 
  print '<p style="text-align:right; font-size:12px;"><a href="https://ehr.meditech.com/node/'.$nid.'/edit">Edit this content</a></p>';
}
?>
<hr>
<!-- end views-view-fields--ebooks--block.tpl.php template -->