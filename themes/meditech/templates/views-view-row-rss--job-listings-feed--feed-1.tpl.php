<?php
/**
* @file
* Default view template to display a item in an RSS feed.
*
* @ingroup views_templates
*/

// Facility...
$facilityTag = $node->field_job_facilities; 
$facID = $facilityTag[und][0]['tid'];
$facility = taxonomy_term_load($facID);
$facility_name = $facility->name;

// Location name...
$locationTag = $node->field_job_location; 
$locID = $locationTag[und][0]['tid'];
$location = taxonomy_term_load($locID);
$location_name = $location->name;
$location_name = substr($location_name, 0, -4);

// Location description...
$location_desc = trim( strip_tags($location->description) );

// zip codes...
switch($location_desc){
case 'MA':
$zip = '02021';
break;
case 'MN':
$zip = '55305';
break;
case 'GA':
$zip = '30301';
break;
case 'TX':
$zip = '75230';
break;
default:
$zip = '';
}

// Category name...
$categoryTag = $node->field_job_category;
$catID = $categoryTag[und][0]['tid'];
$category = taxonomy_term_load($catID);
$category_name = $category->name;
$category_name = str_replace('&', 'and', $category_name);

// Shift...
$shiftTag = $node->field_job_shift;
$shiftID = $shiftTag[und][0]['tid'];
$shift = taxonomy_term_load($shiftID);
$shift_name = $shift->name;

// Job Description...
$jdTag = $node->field_job_description;
$jd = $jdTag[und][0]['value'];
$jd = str_replace('&nbsp;', ' ', $jd);
$jd = str_replace('&amp;', 'and', $jd);
$jd = str_replace('&quot;', '', $jd);

// Job Requirements...
$jrTag = $node->field_job_requirements;
$jr = $jrTag[und][0]['value'];
$jr = str_replace('&nbsp;', ' ', $jr);
$jr = str_replace('&amp;', 'and', $jr);
$jr = str_replace('&quot;', '', $jr);

// Date...
$date = date('m/d/Y', $node->published_at);
?>
<job>
<title><?php print $title; ?></title>
<url><?php print $link; ?></url>
<location><?php print $facility_name; ?>, <?php print $location_desc; ?></location>
<city><?php print $facility_name; ?></city>
<state><?php print $location_name; ?></state>
<country>United States</country>
<zip_code><?php print $zip; ?></zip_code>
<company>Medical Information Technology, Inc. (MEDITECH)</company>
<req_number><?php print $node->nid; ?></req_number>
<category><?php print $category_name; ?></category>
<job_type><?php print $shift_name; ?></job_type>
<posted_at><?php print_r($date); ?></posted_at>
<description><p><strong>Job Description</strong></p><?php print $jd; ?></description>
<qualifications><p><strong>Job Requirements</strong></p><?php print $jr; ?></qualifications>
</job>
