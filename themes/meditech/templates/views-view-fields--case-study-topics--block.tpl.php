<?php // This template is for each row of the Views block: CASE STUDY TOPICS \\\\\\\\\\\\\\\\\\\\\\\\ ....................... 
  $url = $GLOBALS['base_url']; // grabs the site url
?>

<!-- start views-view-fields--case-study-topics--block.tpl.php template -->
<?php 
$name = $fields['name']->content; 
$value = strip_tags(trim($fields['description']->content));
?>
<div class="topic checkbox <?php print $value; ?>">
  <input id="<?php print $name; ?>" type="checkbox" name="topics" value="<?php print $value; ?>"> <label for="<?php print $name; ?>" style="font-size:.9em;"><?php print $name; ?></label>
</div>
<!-- end views-view-fields--case-study-topics--block.tpl.php template -->