<?php if( user_is_logged_in() ){ ?>
<!-- START style-guide--node-3672.php Article Cards -->

<!--Javascript & CSS reference for Prism-->
<script type="text/javascript" src="<?php print $url; ?>/sites/all/themes/meditech/js/prism.js"></script>

<link href="<?php print $url; ?>/sites/all/themes/meditech/css/prism.css" rel="stylesheet" />

<style>

</style>

<section class="container__centered">

	<h1 class="page__title">
		<?php print $title; ?>
	</h1>

	<div class="container__two-thirds">

		<p>Our card containers are useful in scenarios where you may want an image along with some text in a clean looking box with a drop shadow. We pair them up with our <a href="<?php print $url; ?>/style-guide/grid">grid</a> containers for consistency across the site. There is also an option to add a date tab for news articles or events.</p>

		<p>The basic HTML structure is listed below.

		<ul>
			<li>Create a wrapping div with the class <code class="language-html">.card__wrapper</code>.</li>
			<li>Add the containers inside along with the <code class="language-html">.card</code> class on each one.</li>
			<li>Inside the containers, add the <code class="language-html">figure</code> and <code class="language-html">img</code> elements.</li>
			<li>If you require a date tab, add it above the <code class="language-html">img</code> element with the classes <code class="language-html">.card__date</code> and <code class="language-html">.text--small</code>. This will appear positioned over the image.</li>
			<li>Below that, add the <code class="language-html">.card__info</code> div with your heading and paragraph text.</li>
		</ul>

		<!-- Start Card HTML Structure Code -->
		<pre style="border-radius: 6px;"><code class="language-html">&lt;!-- Start Card HTML Structure -->
&lt;div class="card__wrapper">
	&lt;div class="container__one-third card">
		&lt;figure>
			&lt;div class="card__date text--small">Date&lt;/div>
			&lt;img src="img/your-image.jpg">
		&lt;/figure>
		&lt;div class="card__info">
			&lt;h4>Your header here&lt;/h4>
			&lt;p>Your text here&lt;/p>
		&lt;/div> 		
	&lt;/div>
&lt;/div>
&lt;!-- End Card HTML Structure -->
</code></pre>
		<!-- End Card HTML Structure Code -->

		<h2>Card Container Examples</h2>

		<div class="card__wrapper">

			<div class="container no-pad card">
				<figure>
					<div class="card__date text--small">April 14, 2049</div>
					<img src="<?php print $url; ?>/sites/all/themes/meditech/images/styleguide/style-guide-background-accents-image.jpg" style="object-position: top;">
				</figure>
				<div class="card__info">
					<h4>Full Width Card Container</h4>
					<p>Example paragraph text.</p>
				</div>
			</div>

		</div>

		<div class="card__wrapper">

			<div class="container__one-half card">
				<figure>
					<div class="card__date text--small">April 14, 2049</div>
					<img src="<?php print $url; ?>/sites/all/themes/meditech/images/styleguide/style-guide-background-accents-image.jpg">
				</figure>
				<div class="card__info">
					<h4>One Half Card Containers</h4>
					<p>Example paragraph text.</p>
				</div>
			</div>

			<div class="container__one-half card">
				<figure>
					<div class="card__date text--small">April 14, 2049</div>
					<img src="<?php print $url; ?>/sites/all/themes/meditech/images/styleguide/style-guide-background-accents-image.jpg">
				</figure>
				<div class="card__info">
					<h4>One Half Card Containers</h4>
					<p>Example paragraph text.</p>
				</div>
			</div>
			
		</div>
		
		<div class="card__wrapper">

			<div class="container__one-third card">
				<figure>
					<div class="card__date text--small">April 14, 2049</div>
					<img src="<?php print $url; ?>/sites/all/themes/meditech/images/styleguide/style-guide-background-accents-image.jpg">
				</figure>
				<div class="card__info">
					<h4>One Third Card Containers</h4>
					<p>Example paragraph text.</p>
				</div>
			</div>

			<div class="container__one-third card">
				<figure>
					<div class="card__date text--small">April 14, 2049</div>
					<img src="<?php print $url; ?>/sites/all/themes/meditech/images/styleguide/style-guide-background-accents-image.jpg">
				</figure>
				<div class="card__info">
					<h4>One Third Card Containers</h4>
					<p>Example paragraph text.</p>
				</div>
			</div>

			<div class="container__one-third card">
				<figure>
					<div class="card__date text--small">April 14, 2049</div>
					<img src="<?php print $url; ?>/sites/all/themes/meditech/images/styleguide/style-guide-background-accents-image.jpg">
				</figure>
				<div class="card__info">
					<h4>One Third Card Containers</h4>
					<p>Example paragraph text.</p>
				</div>
			</div>

		</div>

		<p><span class="italic"><strong>Note:</strong> By default, the images in the </span><code class="language-html">.container</code><span class="italic"> and </span><code class="language-html">.container__one-half</code><span class="italic"> classes are set to 225px height to avoid narrow cropping. The images in all other container classes are set to 175px height. These images will be automatically cropped to fit the width of the containers so make sure you have them positioned properly (object-position CSS property can be utilized here if needed).</span></p>

	</div>

	<!-- SIDEBAR -->
	<aside class="container__one-third">

		<div class="sidebar__nav panel">
			<?php
        $styleBlock = module_invoke('menu', 'block_view', 'menu-style-guide-side-nav');
        print render($styleBlock['content']); 
        ?>
		</div>

	</aside>
	<!-- END SIDEBAR -->

</section>

<!-- END style-guide--node-3549.php3672.php Article Cards -->
<?php } ?>
