<!-- START inc-covid19-sidebar.php -->
<style>
  .squish-list li { margin-bottom: .75em; line-height: 1.5em; }
</style>

<div class="squish-list">

  <a href="https://customer.meditech.com/en/d/covid19resources/homepage.htm" role="button" class="btn--orange" style="margin: 1em 0;">COVID-19 Resource Page for MEDITECH Customers</a>

  <h3>COVID-19 Content</h3>
  <ul>
    <li><a href="https://ehr.meditech.com/covid-19-resources">COVID-19 Public Resource Page</a></li>
    <li><a href="https://ehr.meditech.com/covid-19-webinars">COVID-19 Webinars</a></li>
    <li><a href="https://blog.meditech.com/topic/coronavirus">Thought Leadership</a></li>
    <li><a href="https://ehr.meditech.com/covid-19-resources#customer-experiences">Customer Experiences</a></li>
    <li><a href="https://ehr.meditech.com/covid-19-resources#solutions-strategies">Solutions &amp; Strategies</a></li> 
    <li><a href="https://ehr.meditech.com/covid-19-resources#announcements">Announcements</a></li>
    <li><a href="https://ehr.meditech.com/meditech-covid-19-control-plan-tenants-vendors-visitors">Tenants, Vendors & Visitors</a></li>
  </ul>

  <h3>Industry Resources</h3>
  <ul>
    <li><a href="https://www.who.int/emergencies/diseases/novel-coronavirus-2019" target="_blank">WHO COVID-19 Outbreak Website</a></li>
    <li><a href="https://www.cdc.gov/coronavirus/2019-nCoV/index.html" target="_blank">CDC COVID-19 Website</a></li>
    <li><a href="https://ipac-canada.org/coronavirus-resources.php" target="_blank">ipac Canada</a></li>
    <li><a href="https://chimecentral.org/category/media/covid-19/" target="_blank">CHIME</a></li>
    <li><a href="https://ehr.meditech.com/covid-19-industry-resources">Other COVID-19 Industry Resources</a></li>
    <li><a href="https://customer.meditech.com/en/d/covid19resources/pages/covid19collaborativevendorresources.htm">Collaborative Vendor Solutions</a></li>
  </ul>

</div>

<!-- END inc-covid19-sidebar.php -->