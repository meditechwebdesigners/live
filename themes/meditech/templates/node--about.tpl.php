<?php // This template is set up to control the display of the 'about' content type 

$url = $GLOBALS['base_url']; // grabs the site url
$node_id = $node->nid; // grab node ID of current page

// list of about page nodes that have specific templates
// this list must be updated with any new templates created...
$special_templates = array(4162, 4163, 4165, 4166, 4167, 4215);

// if current page's node id is in the above array, a different template will be used..
if( in_array($node_id, $special_templates) === true ){
  include('about--node-'.$node_id.'.php');
} 
else{
?>
<!-- start node--about.tpl.php template -->
    
  
  <?php
  /* 
  * ***************************************************************************************************
  * check to see which page is the current page, then load the proper Views or content for that page...
  * ***************************************************************************************************
  */
  ?>

  <section class="container__centered">
	<div class="container__two-thirds">

    <h1 class="page__title js__seo-tool__title"><?php print $title; ?></h1>
    
    <div class="js__seo-tool__body-content">
      <?php print render($content['field_body']); // render BODY field if it exists... ?>
    </div>
    
    
    <?php // SEO tool for internal use...
      if($node_id != 251 && $node_id != 239 && $node_id != 242 && node_access('update',$node)){
        print '<!-- SEO Tool is added to this div -->';
        print '<div class="container no-pad--top js__seo-tool"></div>';
      } 
    ?>    
    
    <?php
    // content for the main EXECUTIVES page ======================================================================
    if($node_id == 242){
    ?>     
      <div class="container no-pad">
        <?php print views_embed_view('executives', 'block'); // adds 'Executives' Views block... ?>
      </div><!-- END container -->
    <?php
    }
    ?>

    <?php
    // content for the main DIRECTIONS page ======================================================================
    if($node_id == 239){
      print views_embed_view('locations_directions_page', 'block'); // adds 'locations - directions page' Views block... 
    }
    ?>
    
    <?php
    // content for the main AREA HOTELS page ======================================================================
    if($node_id == 251){
    ?>
      <?php print views_embed_view('hotels', 'block'); // adds 'Hotels' Views block... ?>
    <?php
    }
    ?>

    </div><!-- END container__two-thirds -->

    <?php
    // add ABOUT SIDE NAV ===========================================================================
    ?>
    <aside class="container__one-third panel">
      <div class="sidebar__nav about_sidebar_gae">
        <?php
          $massnBlock = module_invoke('menu', 'block_view', 'menu-about-section-side-nav');
          print render($massnBlock['content']); 
        ?>
      </div>
    </aside>
  
  </section>
  
  <?php // VIDEO SECTION =================================================================
    // get value from field_video to pass to View (if video exists)...
    $video = render($content['field_video']);
    if(!empty($video)){ // if the page has a video...
      print '<!-- VIDEO -->';
      // remove apostrophes from titles to prevent View from breaking...
      $video_filtered = str_replace("&#039;", "'", $video);
      // adds 'video' Views block...
      print views_embed_view('video_boxed', 'block', $video_filtered);
      print '<!-- END VIDEO -->';
    }
  ?>  
  
  
<?php
} // END if NOT special templates
?>
<!-- end node--about.tpl.php template -->