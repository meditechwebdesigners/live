<!-- START campaign--node-3008.php -->
<?php // This template is set up to control the display of the MEDITECH Professional Services campaign

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<script src="<?php print $url; ?>/sites/all/themes/meditech/js/splide.min.js"></script>

<style>
  /* <!-- Outlined Button --> */
  .btn--outline {
    background-color: transparent;
    color: #3E4545 !important;
    border-width: 4px !important;
    -webkit-transition: all 400ms ease-in-out;
  }

  .btn--orange:hover {
    -webkit-transition: all 400ms ease-in-out;

  }

  .btn--outline .button--hubspot a {
    font-size: 0.89em !important;
    height: auto !important;
    line-height: normal !important;
    outline: 0 !important;
    padding: 1em 1.5em !important;
    display: inline-block !important;
    font-weight: 700 !important;
    cursor: pointer !important;
    color: #3E4545 !important;
    text-align: center !important;
    -webkit-transition: all 400ms ease-in-out !important;
    -moz-transition: all 400ms ease-in-out !important;
    transition: all 400ms ease-in-out !important;
    border: 4px solid #ff8300 !important;
    background-color: transparent !important;
  }

  .btn--outline .button--hubspot a:hover {
    color: #fff !important;
    -webkit-transition: all 400ms ease-in-out !important;
    -moz-transition: all 400ms ease-in-out !important;
    transition: all 400ms ease-in-out !important;
    background: #ff610a !important;
    border: 4px solid #ff610a !important;
  }

  .btn--outline .button--hubspot a:visited {
    color: #3E4545 !important;
  }

  /*  Splide Carousel Styles block 5*/

  .container__centered--extra-pad {
    padding-top: 2em;
  }

  #block5-slider .card {
    box-shadow: none;
    background-color: transparent;
    align-self: flex-start;
  }

  #block5-slider .card__info {
    padding: 2em;
  }

  .splide__pagination {
    text-align: center;
    padding-top: 2em;
  }

  .splide__slide:focus-visible {
    border: 2px solid #fff !important;
  }

  .headshot-container {
    display: flex;
    align-items: center;
    margin-top: 1.5em;
  }

  .headshot {
    max-width: 100px;
    max-height: 100px;
    flex-grow: 1;
    margin-right: 1.5em;
  }

  .quote-name {
    flex-grow: 2;
  }

  .quote-name p {
    margin-bottom: 0;
  }

  .sub-quote {
    border: 1px solid #c8ced9;
    border-radius: 7px;
    padding: 2em;
  }

  /*  Splide Carousel Styles block 7*/
  #primary-slider .card {
    box-shadow: none;
    border: 1px solid #c8ced9;
    background-color: transparent;
    align-self: flex-start;
  }

  #primary-slider .card__info {
    padding: 2em;
  }

  .splide__pagination {
    text-align: center;
  }

  .splide__slide:focus-visible {
    border: 2px solid #fff !important;
  }

  .headshot-container {
    display: flex;
    align-items: center;
    margin-top: 1.5em;
  }

  .headshot {
    max-width: 90px;
    max-height: 90px;
    flex-grow: 1;
    margin-right: 1.5em;
  }

  .quote-name {
    flex-grow: 2;
  }

  .quote-name p {
    margin-bottom: 0;
  }


  /* Quotes */

  .quote-box {
    padding: 3em;
    background-color: rgba(3, 3, 30, .5);
    border-left: 5px solid #00bc6f;
    margin-bottom: 2.35765%;
    border-radius: 7px;
    box-shadow: 0px 0px 20px 1px rgba(0, 0, 0, .2);
  }

  .circle-icon {
    height: 125px;
    width: 125px;
    color: #fff;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 50%;
    margin: 0 auto;
    margin-bottom: 1.5em;
  }

  .white-card {
    padding: 2em;
    text-align: center;
  }

  .li-adjust li {
    margin-bottom: .5em;
    text-align: left;
  }

  .gl-container {
    background-color: rgba(255, 255, 255, .92);
    border-radius: 6px;
  }

  @media all and (max-width: 545px) {
    .circle-icon {
      width: 110px;
      height: 110px;
    }
  }

</style>


<div class="js__seo-tool__body-content">


  <!-- Block 1 -->
  <div class="gl-container">

    <div class="container__one-half gl-text-pad bg--purple-gradient">
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/logo--MEDITECH-Professional-Services--white-and-green.png" alt="MEDITECH Professional Services logo" style="width:50%;">

      <div class="text--white">
        <h2 class="text--white">
          Reach your goals faster with MEDITECH Professional Services
        </h2>
        <p>Don’t let time constraints or limited resources restrict the positive impact you can make on your patients and your community. MEDITECH Professional Services can help elevate your outcomes, improve peak performance, and drive decisions through data insights. Collaborate with a team of experts to help you <a href="https://blog.meditech.com/how-jump-starting-quality-improvement-efforts-can-lead-to-transformational-change">improve quality</a> and maximize ROI using advanced system functionality.</p>
      </div>

      <div style="margin-top: 2em;">

        <div class="button--hubspot">
          <a href="https://meditech.us3.list-manage.com/subscribe?u=0ac5c60e03668ee7629106fe8&id=3a4a14fd93">Sign Up For Elevate, MEDITECH Professional Services’ Insights</a>
        </div>
      </div>

    </div>

    <div class="container__one-half background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/smiling-collaborating-physicians--MPS.jpg); min-height: 23em;">

    </div>

  </div>
  <!-- End of Block 1 -->


  <!-- Start Block 2 (Video) -->
  <div class="content__callout">
    <div class="content__callout__media">
      <div class="content__callout__image-wrapper">
        <div class="video js__video" data-video-id="666114920">
          <figure class="video__overlay">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--mps-physician-helping-patient.jpg" alt="MEDITECH Professional Services - Video Covershot">
          </figure>
          <a class="video__play-btn" href="https://vimeo.com/666114920"></a>
          <div class="video__container"></div>
        </div>
      </div>
    </div>
    <div class="content__callout__content">
      <div class="content__callout__body">
        <div class="content__callout__body__text">
          <h2>Optimize your Expanse EHR with experienced, practicing physicians and system experts</h2>
          <p>MPS provides expert resources who know our software inside and out. They’ll partner with you to determine how to best tailor Expanse according to your workflows.</p>
        </div>
      </div>
    </div>
  </div>
  <!-- End Block 2 (Video)-->


  <!-- Start Background Image -->
  <div class="background--cover hide__bg-image--mobile" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/organic-shapes-mps--bg-block-3.jpg);">


    <!-- Start Block 3 (Case Study) -->
    <div class="container">
      <div class="container__centered center">
        <div class="auto-margins" style="margin-bottom:2em;">
          <h2>Elevate your outcomes</h2>
          <p>Collaborate with MPS to boost your outcomes and attain your goals faster, so you can improve the quality of care across your community.</p>
        </div>

        <div class="container shadow-box bg--box1" style="text-align:left;">
          <p class="text--large">Kingman Regional Medical Center</p>
          
          <div class="container__one-half">
            <p><a href="https://f.hubspotusercontent10.net/hubfs/2897117/Case_Studies/SuccessStory_KingmanHumana.pdf">Used wellness and diabetes registries</a> in tandem with a new nurse navigator program over eight months to:
          </p>
            <div>
              <ul class="fa-ul">
                <li><span class="fa-li text--dark-blue"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span><b>Increase gap closures</b> across all conditions by 23%.</li>
                <li><span class="fa-li text--dark-blue"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span><b>Double</b> patient outreach.</li>
                <li><span class="fa-li text--dark-blue"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span><b>Close gaps</b> for diabetes HbA1c control, <b>which increased from 17% to 69%</b> after follow-up.</li>
              </ul>
            </div>
          </div>
          <div class="container__one-half">
            <p>After earning <b><a href="https://ehr.meditech.com/news/kingman-regional-wins-health-it-innovation-award">Health Current’s 2021 Health IT Innovation Award</a></b>, KRMC is keeping the momentum going by working with MPS to:
            </p>
            <div>
              <ul class="fa-ul">
                <li><span class="fa-li text--dark-blue"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span><b>Incorporate</b> remote monitoring into registries.</li>
                <li><span class="fa-li text--dark-blue"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span><b>Engineer a strategy</b> for achieving Digital Health Most Wired.</li>
              </ul>
            </div>
          </div>
        </div>

        <div class="container no-pad--top" style="text-align:left;">
          <div class="container__one-half shadow-box">
            <p class="text--large">Mount Nittany Health</p>
            <p>Created a <a href="https://ehr.meditech.com/ehr-solutions/meditech-population-health">diabetes registry</a> to help <b>lower HbA1c for patients at risk of diabetes</b>. The registry enables staff to identify and follow up with patients who either have an A1c > 9.0% or haven’t had a recent A1c test. Some patients lowered their A1c by as many as 4 points.</p>
          </div>
          <div class="container__one-half shadow-box">
            <p class="text--large">Tidelands Health</p>
            <p>Implemented <a href="https://ehr.meditech.com/ehr-solutions/meditech-ehr-excellence-toolkits?hsCtaTracking=0945b164-5e50-484e-bf28-9e3e28f15c00%7C6a449f6c-8529-41cf-91b0-936189d5abbf">MEDITECH’s Telemetry Appropriateness Toolkit</a> and within 30 days <b><a href="https://ehr.meditech.com/news/tidelands-health-sees-52-drop-in-inappropriate-telemetry-orders">reduced unnecessary ECG orders by 52%</a></b>. Staff also established process improvements to reduce nursing time, as well as improve provider and patient satisfaction.</p>
          </div>
        </div>
      </div>
    </div>
    <!-- End Block 3 (Case Study) -->

  </div> <!-- End first bg image -->


  <!-- Block 4 -->
  <div class="container" style="background-color: rgba(1, 108, 145, .07);">
    <div class="container__centered">

      <div class="center" style="padding-bottom:2em;">
        <h2>Improve peak performance</h2>
        <p>Accelerate your journey to excellence with the right partner and technology to transform care delivery.</p>
      </div>

      <div>
        <div class="container__one-half">
          <p style="padding-bottom:2em;"><b>Frederick Health</b> leveraged MPS CMIO Advisory Support to standardize Expanse across care settings, establish best practices, and personalize provider workflows. Today, MPS is helping them incorporate precision medicine into <a href="https://ehr.meditech.com/ehr-solutions/meditech-population-health">Population Health</a> through MEDITECH Expanse <a href="https://ehr.meditech.com/ehr-solutions/meditech-genomics?hsCtaTracking=6665f033-2f04-43c2-bed2-22c11d042721%7Cd0fb337d-192b-49de-86e0-69a7b48f883f">Genomics</a>.</p>
          <div class="shadow-box">
            <p class="italic">“I greatly appreciate the valuable assistance provided by MEDITECH Physician Advisory Professional Services. <b>Our engagement significantly accelerated a steep learning curve. In particular, our physician advisor was extremely knowledgeable and flexible in tailoring his support to our evolving needs.</b>”</p>
            <div class="headshot-container">
              <div class="headshot">
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/robert-wack.png" style="max-width:90px; max-height:90px;" alt="Robert Wack">
              </div>
              <p><b>Robert Wack, MD</b><br>
                Chief Medical Information Officer<br>
                Medical Director, Continuum of Care<br>
                Frederick Health<br>
                Frederick, MD
              </p>
            </div>
          </div>
        </div>

        <div class="container__one-half">
          <p style="padding-bottom: 2em;"><b>Southern Ohio Medical Center</b> partnered with MPS to <a href="https://blog.meditech.com/serving-your-community-with-a-quality-strategy">develop an acute care toolkit</a> related to opioid prescribing to improve both post operative and general prescribing habits for their providers after implementing <a href="https://ehr.meditech.com/ehr-solutions/meditech-ehr-excellence-toolkits?hsCtaTracking=0945b164-5e50-484e-bf28-9e3e28f15c00%7C6a449f6c-8529-41cf-91b0-936189d5abbf">MEDITECH’s Opioid Stewardship Toolkit</a>.</p>
          <div class="shadow-box">
            <p class="italic">“It was important for us to introduce Expanse’s Opioid Stewardship Toolkit, which has helped us address our post operative prescribing habits. Now, <b>we are</b> taking this initiative a step further and <b>working with MEDITECH Professional Services on a custom dashboard that can identify better ways of managing pain in the ambulatory and acute care settings.</b>”</p>
            <div class="headshot-container">
              <div class="headshot">
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/valerie-decamp-gray-bg.png" style="max-width:90px; max-height:90px;" alt="Valerie DeCamp" style="border-color: darkgray;
                    border-width: 1px;
                    border-style: solid;">
              </div>
              <p><b>Valerie DeCamp, DNP, RN, A-GNP-C, NE-BC </b><br>
                VP of Clinical Integration, Chief Quality Officer<br>
                Southern Ohio Medical Center<br>
                Portsmouth, OH
              </p>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
  <!-- END Block 4 -->


  <!-- Start Background Image -->
  <div class="background--cover hide__bg-image--mobile" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/organic-shapes-mps--bg-block-5.jpg);">


    <!-- START Block 5 quotes -->
    <div class="container">
      <div class="container__centered">
        <div id="block5-slider" class="splide" style="padding-top: 2em;">
          <div class="splide__track">
            <div class="splide__list">

              <li class="splide__slide card" tabindex="0">

                <div class="container__one-half" style="padding-top: 1em;">
                  <p style="padding-top: 4em;">
                    <b>Lawrence General Hospital</b> <a href="https://f.hubspotusercontent10.net/hubfs/2897117/Case_Studies/Lawrence_General_Customer_Success_Story.pdf"> implemented a customized ED Throughput dashboard</a> to identify bottlenecks, monitor return visits, and include provider and nurse scorecards. MPS also <a href="https://www.google.com/url?q=https://ehrintelligence.com/news/using-ehr-integrations-to-understand-social-determinants-of-health&sa=D&source=docs&ust=1642716212340194&usg=AOvVaw02cebWm-_YdJRXJODaGNoT">developed a health equity dashboard</a> to determine the impact of ethnicity, race, and social determinants of health on patient outcomes.
                  </p>
                </div>
                <div class="container__one-half sub-quote">
                  <p class="italic text--large"><b>“BCA has taken us to levels not achieved on previous EMRs</b>. It helps us answer questions about what’s happening at any given moment in the ED... [It] has <b>revolutionized</b> what I am able to do...not just for the ER, but also for the hospital.”</p>
                  <div class="headshot-container">
                    <div class="headshot">
                      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/omer_moin_headshot.png" style="max-width:90px; max-height:90px;" alt="Omer Moin head shot">
                    </div>
                    <div class="quote-name">
                      <p class="bold" style="line-height: 1.4;">Omer Moin, MD, FACEP</p>
                      <p>Chief of Emergency Medicine</p>
                      <p>Lawrence General Hospital</p>
                      <p>Lawrence, MA</p>
                    </div>
                  </div>
                </div>

              </li>

              <li class="splide__slide card" tabindex="0">

                <div class="container__one-half" style="padding-top: 1em;">
                  <p style="padding-top:8em;">
                    <b>Logan Health</b> created lab efficiency dashboards to highlight the turnaround time for processing lab tests from outside sites.
                  </p>
                </div>
                <div class="container__one-half sub-quote">
                  <p class="italic text--large">“<b>At Logan Health, we implemented MEDITECH Business and Clinical Analytics to evaluate our status as an organization, through one single source of truth.</b> By aggregating and analyzing clinical, financial, and operational data, analytical reports and dashboards inform decision-making and help improve outcomes and transparency.”</p>
                  <div class="headshot-container">
                    <div class="headshot">
                      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/john-tollerson.png" style="max-width:90px; max-height:90px;" alt="John Tollerson head shot">
                    </div>
                    <div class="quote-name">
                      <p class="bold" style="line-height: 1.4;">John Tollerson, DO</p>
                      <p>Chief Medical Information Officer</p>
                      <p>Logan Health</p>
                      <p>Kalispell, MT</p>
                    </div>
                  </div>
                </div>
              </li>

              <li class="splide__slide card" tabindex="0">
                <div class="container__one-half" style="padding-top: 1em;">
                  <p style="padding-top: 8em;">
                    <b>Emanate Health</b> worked with MPS to <a href="https://info.meditech.com/en/emanate-health-advances-covid-19-contact-tracing-with-meditech-professional-services">develop MEDITECH’s Disease Investigation and Contact Tracing dashboard</a>, which they use to identify and prioritize patients and staff at high risk for COVID-19 exposure.
                  </p>
                </div>
                <div class="container__one-half sub-quote">
                  <p class="italic text--large">“We can identify not just the source of COVID-19 infection — who did this patient/staff member likely get the virus from — but also who did the patient/staff member potentially expose during their infectious period. <b>I am not aware of any system in the market that can accomplish what we just did.</b>” </p>
                  <div class="headshot-container">
                    <div class="headshot">
                      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/Loucine-Kasparian.png" style="max-width:90px; max-height:90px;" alt="Loucine Kasparian head shot">
                    </div>
                    <div class="quote-name">
                      <p class="bold" style="line-height: 1.4;">Loucine Kasparian</p>
                      <p>Corporate Director, Infection Control</p>
                      <p>Emanate Health</p>
                      <p>Covina, CA</p>
                    </div>
                  </div>
                </div>
              </li>

              <li class="splide__slide card" tabindex="0">
                <div class="container__one-half" style="padding-top: 1em;">
                  <p style="padding-top: 8em;">
                    <b>Golden Valley Memorial Healthcare</b> used an MPS-created dashboard to <a href="https://cdn2.hubspot.net/hubfs/2897117/WPs,%20Case%20Studies,%20Special%20Reports/CustomerLeaders_GoldenValley_BCA.pdf">track relative value units</a> related to provider compensation — saving HIM staff approximately 140 hours per month.
                  </p>
                </div>
                <div class="container__one-half sub-quote">
                  <p class="italic text--large">“In addition to implementing the sepsis toolkit, MEDITECH Professional Services taught us how to embed Golden Valley Memorial Healthcare protocols into our workflows, and how to personalize and optimize Expanse to meet our needs. We are now
                    <b>empowered to take the principles we learned and apply them</b> to future quality initiatives.”
                  </p>
                  <div class="headshot-container">
                    <div class="headshot">
                      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/william-dailey.png" style="max-width:90px; max-height:90px;" alt="William Dailey head shot">
                    </div>
                    <div class="quote-name">
                      <p class="bold" style="line-height: 1.4;">William Dailey, MD</p>
                      <p>Chief Medical Information Officer</p>
                      <p>Golden Valley Memorial Healthcare</p>
                      <p>Clinton, MO</p>
                    </div>
                  </div>
                </div>
              </li>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script>
      document.addEventListener('DOMContentLoaded', function() {
        new Splide('#block5-slider', {
          type: "loop",
          perPage: 1,
          gap: "2em",
          padding: {
            right: "2em",
            left: "2em",
          },
          speed: "400",
          pagination: "true",
          keyboard: "true",
          updateOnMove: "true",
          breakpoints: {
            900: {
              perPage: 1,
              gap: "2em",
              padding: {
                right: "2em",
                left: "2em",
              }
            }
          }
        }).mount();
      });

    </script>
    <!-- END Block 5 quotes -->


    <!-- START Block 6 -->
    <div class="container">
      <div class="container__centered">
        <div class="auto-margins center" style="margin-bottom: 2em;">
          <h2>Get the support you need to become a high performer</h2>
          <p>Our team will support you exactly where you need it most. Each track is led by a MEDITECH Associate Vice President with over 20 years of experience, so you know you're in good hands. With MPS, you’ll benefit from:</p>
        </div>

        <div class="container shadow-box">

          <div class="container__one-fifth" style="width:100%;">
            <div class="container__one-third">
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--implementation-mps.svg" alt="hours down arrow icon" style="height:100px; width:100%;">
            </div>
            <div class="container__two-thirds">
              <p><b>Implementation</b></p>
              <p>A one-stop shop for <a href="https://ehr.meditech.com/mps/implementation">implementation</a> needs with a proven framework for success.</p>
            </div>
          </div>

          <div class="container__one-fifth" style="width:100%;">
            <div class="container__one-third">
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--physician-services-mps.svg" alt="hours down arrow icon" style="height:100px; width:100%;">
            </div>
            <div class="container__two-thirds">
              <p><b>Physician Services</b></p>
              <p>Guidance to create a customized experience for your <a href="https://ehr.meditech.com/mps/physician-advisory-services">providers</a> according to their workflows.</p>
            </div>
          </div>

          <div class="container__one-fifth" style="width: 100%;">
            <div class="container__one-third">
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--ehr-toolkits-quality-mps.svg" alt="hours down arrow icon" style="height:100px; width:100%;">
            </div>
            <div class="container__two-thirds">
              <p><b>EHR Toolkits & Quality</b></p>
              <p>Subject matter experts who provide executive help, oversight, and engagement for <a href="https://ehr.meditech.com/mps/ehr-toolkits-and-quality">quality</a> improvement.</p>
            </div>
          </div>

          <div class="container__one-fifth" style="width: 100%;">
            <div class="container__one-third">
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--bca-mps.svg" alt="hours down arrow icon" style="height:100px; width:100%;">
            </div>
            <div class="container__two-thirds">
              <p><b>Business & Clinical Analytics</b></p>
              <p>Sophisticated <a href="https://ehr.meditech.com/mps/business-and-clinical-analytics-and-data-repository">analytics</a> that allow for insightful decision-making based on reliable data.</p>
            </div>
          </div>

          <div class="container__one-fifth" style="width: 100%;">
            <div class="container__one-third">
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--interoperability-mps.svg" alt="hours down arrow icon" style="height:100px; width:100%;">
            </div>
            <div class="container__two-thirds">
              <p><b>Interoperability</b></p>
              <p>Various <a href="https://ehr.meditech.com/mps/interoperability">interoperability</a> assessments and onboarding strategies to help you share information in a way that works best for your organization and allows you to meet regulatory requirements.</p>
            </div>
          </div>

        </div>

        <div class="center" style="padding-top: 2em;">
          <p><b>Check out our <a href="https://home.meditech.com/en/d/mktcontent/otherfiles/professionalservicesmenu.pdf">menu of options</a> to determine which services best support your organization's needs.</b></p>

          <div class="btn-holder--content__callout center" style="margin-top:2em;">
            <a href="https://home.meditech.com/webforms/contact.asp?rcpt=professionalservices|meditech|com&rname=professionalservices" class="btn--orange btn--outline">Learn More About MEDITECH Professional Services</a>
          </div>

        </div>

      </div>
    </div>
    <!-- END Block 6 -->

  </div><!-- End Background Image -->


  <!-- START Block 7 -->
  <style>
    .shadow-box.w-headshot-outline-b7 {
      padding: 4em 2em 2em 2em;
      position: relative;
      margin-top: 50px;
      box-shadow: none;
      border: 1px solid #c8ced9;
      background-color: transparent;
    }

    .shadow-box.outline {
      box-shadow: none;
      border: 1px solid #c8ced9;
      background-color: transparent;
    }

    .headshot-b7 {
      position: absolute;
      top: -50px;
      left: 50%;
      transform: translate(-50%);
    }

    .headshot-b7 img {
      width: 100px;
      height: 100px;
      border-radius: 50%;
    }

  </style>

  <div class="container bg--purple-gradient">
    <div class="container__centered">

      <div class="container__one-third">
        <div class="shadow-box w-headshot-outline-b7">
          <div class="headshot-b7">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/Kelly-Lippold.png" alt="Kelly Lippold head shot">
          </div>
          <p class="italic">“We used MEDITECH Professional Services for a project in the past and they were <b>very responsive and great to work with</b>. We wanted to track patients in-house who had been tested [for COVID-19] and run occupancy stats from certain locations, but <b>our Professional Services analyst brought more ideas to the table and expanded what we would be able to look at.</b>”</p>
          <p><span class="bold no-margin--bottom text--meditech-green">
              Kelly Lippold</span><br>
            Director of Clinical Informatics<br>
            NMC Health<br>
            Newton, KS</p>
        </div>
      </div>

      <div class="container__one-third">
        <div class="shadow-box w-headshot-outline-b7">
          <div class="headshot-b7">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/sindee-morse.png" alt="Sindee Morse headshot">
          </div>
          <p class="italic">“Having a self-service approach has allowed users to optimize individual unit needs in a timely manner and has drastically reduced multiple requests of the IT team for writing custom reports. <b>I strongly recommend utilizing MEDITECH Professional Services to any organization underutilizing BCA,</b> as it is aligned strongly to meet needs for reporting and <b>eliminates time spent on manual abstraction and processing</b>.”</p>
          <p><span class="bold no-margin--bottom text--meditech-green">Sindee Morse, MSN, RN</span><br>
            Acute Application Manager<br>
            Conway Regional Health System<br>
            Conway, AR</p>
        </div>
      </div>

      <div class="container__one-third">
        <div class="shadow-box w-headshot-outline-b7">
          <div class="headshot-b7">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/louis-harris.png" alt="Louis Harris head shot">
          </div>
          <p class="italic">“The <b>MEDITECH Professional Services</b> consultant <b>was very helpful in working with us to meet our goals and objectives for provider efficiency and optimized workflows.</b> Our follow-up education session had 54 registered physician participants with another five to 10 ‘walk-ins.’ We had very positive feedback during and after the session from our providers.”</p>
          <p><span class="bold no-margin--bottom text--meditech-green">Louis B. Harris, MD</span><br>
            Citizens Memorial Hospital<br>
            Bolivar, MO</p>
        </div>
      </div>

    </div>
  </div>

  <!-- END Block 7 -->

</div>
<!-- end js__seo-tool__body-content -->

<!-- Start Block 8 -->
<div class="container">
  <div class="container__centered center">

    <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
    <h2>
      <?php print $cta->field_header_1['und'][0]['value']; ?>
    </h2>
    <?php } ?>

    <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
    <div>
      <?php print $cta->field_long_text_1['und'][0]['value']; ?>
    </div>
    <?php } ?>

    <div class="button--hubspot">
      <a href="https://meditech.us3.list-manage.com/subscribe?u=0ac5c60e03668ee7629106fe8&id=3a4a14fd93">Sign Up For Elevate</a>
    </div>

    <div style="margin-top:1em;">
      <?php print $share_link_buttons; ?>
    </div>

  </div>
</div>
<!-- END Block 8 -->


<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>
<!-- END campaign--node-3008.php -->
