<?php if( user_is_logged_in() ){ ?>
<!-- START style-guide--node-3375.php Background Accents -->

<!--Javascript & CSS reference for Prism-->
<script type="text/javascript" src="<?php print $url; ?>/sites/all/themes/meditech/js/prism.js"></script>

<link href="<?php print $url; ?>/sites/all/themes/meditech/css/prism.css" rel="stylesheet" />

<style>
    /* Style Guide Specific Styles */
    .blank-space {
        width: 90%;
        height: 85px;
        position: relative;
    }

    @media all and (max-width: 50em) {

        .blank-space>.bg-pattern--green-dots,
        .blank-space>.bg-pattern--blue-dots,
        .blank-space>.bg-pattern--green-squares,
        .blank-space>.bg-pattern--blue-squares,
        .blank-space>.bg-pattern--green-gradient,
        .blank-space>.bg-pattern--blue-gradient {
            display: block;
        }
    }

</style>

<section class="container__centered">

    <h1 class="page__title">
        <?php print $title; ?>
    </h1>

    <div class="container__two-thirds">

        <p>Our Background Accents are subtle, light colored designs that you can use to accentuate an image in the foreground. These designs are created with CSS and we recommend they be used in one-half containers. They are a great alternative to full width background images as they will load faster and you do not have to worry about sourcing an image with copy space.</p>

        <h2>Basic Usage</h2>

        <p>In the example below, you will see the background accent is layered behind the foreground image. We accomplish this by using CSS positioning and the z-index property.</p>
        <p>The HTML structure is fairly simple; first we have the wrapper class <code class=" language-css">.bg-pattern--container</code>, within that we have the foreground image followed by a div with the accent class name <code class=" language-css">.bg-pattern--green-dots</code> and a left or right offset designation <code class=" language-css">.bg-pattern--left</code>.</p>

        <!-- Green Dots Left Example -->
        <div class="demo-ct" style="padding: 2.5em 6em 2.5em 6em;">
            <div class="bg-pattern--container">
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/styleguide/style-guide-background-accents-image.jpg">
                <div class="bg-pattern--green-dots bg-pattern--left"></div>
            </div>
        </div>
        <!-- Green Dots Left Example -->

        <!-- Start Green Dots Left Code -->
        <pre style="margin-bottom:2em;"><code class="language-html">&lt;!-- Start Green Dots Background Accent -->
&lt;div class="bg-pattern--container">&lt;!-- Wrapper container -->
  &lt;img src="foreground-img.jpg">&lt;!-- Foreground image -->
  &lt;div class="bg-pattern--green-dots bg-pattern--left">&lt;/div>&lt;!-- Accent design + left or right class designation -->
&lt;/div>
&lt;!-- End Green Dots Background Accent -->
</code></pre>
        <!-- End Green Dots Left Code -->

        <h2>Background Accent Designs</h2>
        <p>Below are the CSS designs available to use along with their class names. These are specifically designed to be subtle so they do not clash with the images in the foreground.</p>

        <!-- Start Green Dots -->
        <div class="container no-pad--bottom" style="padding-top:1em;">
            <div class="container__one-half">
                <div class="blank-space">
                    <div class="bg-pattern--green-dots bg-pattern--left" style="top:0; left:0;"></div>
                </div>
            </div>
            <div class="container__one-half">
                <h5>Green Dots</h5>
                <code class="language-css">.bg-pattern--green-dots</code>
            </div>
        </div>
        <!-- End Green Dots -->

        <hr>

        <!-- Start Blue Dots -->
        <div class="container no-pad">
            <div class="container__one-half">
                <div class="blank-space">
                    <div class="bg-pattern--blue-dots bg-pattern--left" style="top:0; left:0;"></div>
                </div>
            </div>
            <div class="container__one-half">
                <h5>Blue Dots</h5>
                <code class="language-css">.bg-pattern--blue-dots</code>
            </div>
        </div>
        <!-- End Blue Dots -->

        <hr>

        <!-- Start Green Squares -->
        <div class="container no-pad">
            <div class="container__one-half">
                <div class="blank-space">
                    <div class="bg-pattern--green-squares bg-pattern--left" style="top:0; left:0;"></div>
                </div>
            </div>
            <div class="container__one-half">
                <h5>Green Squares</h5>
                <code class="language-css">.bg-pattern--green-squares</code>
            </div>
        </div>
        <!-- End Green Squares -->

        <hr>

        <!-- Start Blue Squares -->
        <div class="container no-pad">
            <div class="container__one-half">
                <div class="blank-space">
                    <div class="bg-pattern--blue-squares bg-pattern--left" style="top:0; left:0;"></div>
                </div>
            </div>
            <div class="container__one-half">
                <h5>Blue Squares</h5>
                <code class="language-css">.bg-pattern--blue-squares</code>
            </div>
        </div>
        <!-- End Blue Squares -->

        <hr>

        <!-- Start Green Gradient -->
        <div class="container no-pad">
            <div class="container__one-half">
                <div class="blank-space">
                    <div class="bg-pattern--green-gradient bg-pattern--left" style="top:0; left:0;"></div>
                </div>
            </div>
            <div class="container__one-half">
                <h5>Green Gradient</h5>
                <code class="language-css">.bg-pattern--green-gradient</code>
            </div>
        </div>
        <!-- End Green Gradient -->

        <hr>

        <!-- Start Blue Gradient -->
        <div class="container no-pad--top">
            <div class="container__one-half">
                <div class="blank-space">
                    <div class="bg-pattern--blue-gradient bg-pattern--left" style="top:0; left:0;"></div>
                </div>
            </div>
            <div class="container__one-half">
                <h5>Blue Gradient</h5>
                <code class="language-css">.bg-pattern--blue-gradient</code>
            </div>
        </div>
        <!-- End Blue Gradient -->


        <h2>Left &amp; Right Designation Classes</h2>

        <p>Since this design element is meant to be used in a one-half container, you will need to designate which side you want the image and background accent to appear on. We accomplish this with the <code class=" language-css">.bg-pattern--left</code> and <code class=" language-css">.bg-pattern--right</code> classes.</p>

        <p>For the content side, we have padding classes that add some additional space between the image and content. The two padding designation classes are called <code class=" language-css">.content--pad-left</code> and <code class=" language-css">.content--pad-right</code>. One of these classes would be placed on the one-half container side that has the content.</p>

        <p>In the example below, you will see the image has the background accent offset to the left, which the <code class=" language-css">.bg-pattern--left</code> class is determining. Since the content is in the container on the right, we want the padding to the left of that, so we use the <code class=" language-css">.content--pad-left</code> class on the one-half container.</p>


        <!-- Blue Dots Left Example -->
        <div class="demo-ct" style="padding: 3.5em 4em 2.5em 6em;">

            <div class="container__one-half">
                <div class="bg-pattern--container">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/styleguide/style-guide-background-accents-image.jpg">
                    <div class="bg-pattern--blue-dots bg-pattern--left"></div>
                </div>
            </div>

            <div class="container__one-half content--pad-left">
                <h4>Background Pattern: Blue Dots</h4>
                <p>Example paragraph text.</p>
                <div class="btn-holder--content__callout left">
                    <a class="btn--orange" href="#">Example Button</a>
                </div>
            </div>

        </div>
        <!-- Blue Dots Left Example -->

        <!-- Start Blue Dots Left Code -->
        <pre style="margin-bottom:2em;"><code class="language-html">&lt;!-- Start Blue Dots Background Accent -->
&lt;div class="container__one-half">
  &lt;div class="bg-pattern--container">&lt;!-- Wrapper container -->
    &lt;img src="foreground-img.jpg">&lt;!-- Foreground image -->
    &lt;div class="bg-pattern--blue-dots bg-pattern--left">&lt;/div>&lt;!-- Accent design + left or right class designation -->
  &lt;/div>
&lt;/div>

&lt;div class="container__one-half content--pad-left">&lt;!-- Left or right padding class designation -->
  &lt;!-- Standard right side container content goes here -->
&lt;/div>
&lt;!-- End Blue Dots Background Accent -->
</code></pre>
        <!-- End Blue Dots Left Code -->

        <h2>Full Height Class</h2>

        <p>By default, all the background accents are set to around 3/4 the height of the image. In situations where you may want the background accents to extend below the bottom of the image, or "full height", we have a class for that called <code class=" language-css">.bg-pattern--full-height</code>. All you need to do here is add the full height class to the div with the background accent class.</p>

        <!-- Blue Gradient Full Height Example -->
        <div class="demo-ct" style="padding: 3.5em 6em 5em 6em;">
            <div class="bg-pattern--container">
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/styleguide/style-guide-background-accents-image.jpg">
                <div class="bg-pattern--blue-gradient bg-pattern--left bg-pattern--full-height"></div>
            </div>
        </div>
        <!-- Blue Gradient Full Height Example -->

        <!-- Start Blue Gradient Full Height Code -->
        <pre style="margin-bottom:2em;"><code class="language-html">&lt;!-- Start Blue Gradient Full Height Background Accent -->
&lt;div class="bg-pattern--container">&lt;!-- Wrapper container -->
  &lt;img src="foreground-img.jpg">&lt;!-- Foreground image -->
  &lt;div class="bg-pattern--blue-gradient bg-pattern--left bg-pattern--full-height">&lt;/div>&lt;!-- Accent design + left or right class designation + full height -->
&lt;/div>
&lt;!-- End Blue Gradient Full Height Background Accent -->
</code></pre>
        <!-- End Blue Gradient Full Height Code -->

        <h2>Container Stacking</h2>

        <p>In mobile view or screen sizes lower than 50em (800px), you will notice the image stacks on top of the content or vice versa. For the occasions when the image is on the right, the image naturally stacks below the content. We do not want this to happen so we created <a href="https://css-tricks.com/snippets/css/a-guide-to-flexbox/" target="_blank">Flexbox</a> classes to reverse that stack ordering called <code class=" language-css">.flex-order--container</code> and <code class=" language-css">.flex-order--reverse</code>.</p>

        <p>In the example below, you will see we are wrapping both one-half containers inside a new div with the <code class=" language-css">.flex-order--container</code> class. In addition to that, we add the <code class=" language-css">.flex-order--reverse</code> class to the one-half container on the right. When you resize the screen below 50em (800px), you will see the image now stacks above the content.</p>

        <p><span class="italic"><strong>Note:</strong> The background accent is removed on screen sizes under 50em (800px) width.</span></p>

        <!-- Green Dots Right Example -->
        <div class="demo-ct" style="padding: 3.5em 6em 2.5em 4em;">

            <div class="flex-order--container">
                <div class="container__one-half content--pad-right">
                    <h4>Background Pattern: Green Dots</h4>
                    <p>Example paragraph text.</p>
                    <div class="btn-holder--content__callout left">
                        <a class="btn--orange" href="#">Example Button</a>
                    </div>
                </div>

                <div class="container__one-half flex-order--reverse">
                    <div class="bg-pattern--container">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/styleguide/style-guide-background-accents-image.jpg">
                        <div class="bg-pattern--green-dots bg-pattern--right"></div>
                    </div>
                </div>
            </div>

        </div>
        <!-- Green Dots Right Example -->

        <!-- Start Green Dots Right Code -->
        <pre style="margin-bottom:2em;"><code class="language-html">&lt;!-- Start Green Dots Background Accent -->
&lt;div class="flex-order--container">&lt;!-- Flexbox container class -->    
  &lt;div class="container__one-half content--pad-right">&lt;!-- Left or right padding class designation -->
    &lt;!-- Standard left side container content goes here -->
  &lt;/div>
  
  &lt;div class="container__one-half flex-order--reverse">&lt;!-- Flexbox reverse ordering class -->    
    &lt;div class="bg-pattern--container">&lt;!-- Wrapper container -->
      &lt;img src="foreground-img.jpg">&lt;!-- Foreground image -->
      &lt;div class="bg-pattern--green-dots bg-pattern--right">&lt;/div>&lt;!-- Accent design + left or right class designation -->
    &lt;/div>
  &lt;/div>
&lt;/div>
&lt;!-- End Green Dots Background Accent -->
</code></pre>
        <!-- End Green Dots Right Code -->

        <h2>Tips &amp; Further Exploration</h2>

        <p><span class="bold">Image Sizing:</span> One thing to be aware of is the foreground image size and aspect ratio. It is recommended we keep the image aspect ratio consistent across the site at <span class="bold">16:9</span>. The full height dots and squares background accents were designed around this ratio so that's where it will "fit" best. The recommended image size is <span class="bold">800px x 525px</span>.</p>

        <p><span class="bold">Grid Containers:</span> The background accents have been tested in one-third and two-thirds containers and work fine, but using these with the <code class=" language-css">.bg-pattern--full-height</code> class is not recommended. We do not recommend using these in one-fourth containers.</p>

        <p><span class="bold">Accent Color Overrides:</span> If you are looking to use a different colored background accent that is not listed here, you can override one of the existing classes with inline or in-page styling. Just be sure to only use the colors from our <a href="<?php print $url; ?>/style-guide/colors">Color Palette</a>. This option is fine in a pinch but it is recommended to properly test and add new designs to our CSS.</p>
        <p><span class="italic"><strong>Note:</strong> For cross-browser compatibility, always use RGB instead of hex codes for background-color with opacity (ie: background-color: rgba(0, 117, 168, 0.15);).</span></p>

        <p><span class="bold">New Designs:</span> If you have a great background accent design in mind we fully encourage exploring new ideas! We have set up a <a href="https://codepen.io/maxdennx/pen/yLePwoE?editors=1100" target="_blank">Background Accent CodePen</a> playground for just that. Feel free to use this environment to test new ideas and designs.</p>

    </div>

    <!-- SIDEBAR -->

    <aside class="container__one-third">

        <div class="sidebar__nav panel">
            <?php
        $styleBlock = module_invoke('menu', 'block_view', 'menu-style-guide-side-nav');
        print render($styleBlock['content']); 
        ?>
        </div>

    </aside>

    <!-- END SIDEBAR -->
</section>

<!-- END style-guide--node-3375.php Background Accents -->
<?php } ?>
