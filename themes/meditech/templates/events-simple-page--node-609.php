<!-- start events-simple-page--node-609.php -->
<?php // 'Trade Shows' page ================================================================================== ?>

<section class="container__centered">
<div class="container__two-thirds">

  <h1 class="page__title js__seo-tool__title"><?php print $title; ?></h1>

  <div class="js__seo-tool__body-content">

    <?php print render($content['field_body']); ?>

    <div class="tradeshows">
      <table class="table">
        <thead>
          <tr>
            <th style="width:15%;">Date</th><th>Event</th><th>Location</th>
          </tr>
        </thead>
        <tbody>   
          <?php print views_embed_view('events_trade_shows', 'block'); // adds 'events - trade shows' Views block... ?>
        </tbody>
      </table>
    </div>

  </div>

</div><!-- END container__two-thirds -->

<!-- SIDEBAR -->
<aside class="container__one-third panel">
  <div class="sidebar__nav">
    <?php
      $messnBlock = module_invoke('menu', 'block_view', 'menu-events-section-side-nav');
      print render($messnBlock['content']); 
    ?>
  </div>
</aside>
<!-- END SIDEBAR --> 
</section>   
<!-- end events-simple-page--node-609.php -->