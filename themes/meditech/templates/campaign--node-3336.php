<!-- START campaign--node-3336.php -->

<?php // This template is set up to control the display of the updated Digital Transformation Campaign

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');
$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
    .add-hover {
        border-bottom: 1px solid #fff;
        color: #fff;
    }

    .add-hover:hover {
        color: #00bbb3;
        border-bottom: 1px solid #00bbb3;
    }

    video {
        max-width: 100%;
    }

    .pause {
        background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--pause.svg);
    }

    .play {
        background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--play.svg);
    }

    .videoButton {
        position: absolute;
        bottom: 9px;
        left: 0;
        right: 0;
        overflow: hidden;
        width: 34px;
        height: 36px;
        max-width: 100%;
        transition: 0.2s ease;
        border-radius: 0.25em;
    }

    .videoButton:hover {
        height: 50px;
        width: 50px;
        transition: 0.2s ease;
    }

    .demo:hover+.videoButton {
        height: 50px;
        width: 50px;
        transition: 0.2s ease;
    }

    .demo {
        border-radius: 0.389em;
        box-shadow: 0px 0px 20px 1px rgb(120 120 120 / 20%);
        cursor: pointer;
    }

    .fa-angle-double-right:before {
        content: "\f101";
    }

    .box-pad {
        padding: 1em 2em;
    }

    .box-pad-2 {
        padding: 1em 0 1em 0;
        ;
    }

    .circle-icon {
        display: flex;
        justify-content: center;
        align-items: center;
        margin-bottom: 1em;
    }


    .auto-margins--one-half {
        max-width: 25em;
        margin: auto;
        margin-top: 6em;
    }

    /*	Extra padding for boxes with small amount of text */
    .gl-text-pad--xp {
        padding: 8em 6em !important;
    }

    @media (max-width: 1040px) {
        .hide__bg-image--tablet {
            background-image: none !important;
        }

        .gl-text-pad--xp {
            padding: 4em !important;
        }

        .auto-margins--one-half {
            margin-top: 2em;
        }


    }

    @media (max-width: 800px) {
        .gl-text-pad--xp {
            padding: 2em !important;
        }

        .phone-margin {
            margin-top: 2em;
        }


        .auto-margins--one-half {
            margin-top: inherit;
        }

    }

</style>

<div class="js__seo-tool__body-content">


    <h1 style="display:none;" class="js__seo-tool__title">MEDITECH Digital Transformation</h1>

    <!-- START Block 1 -->
    <div class="container no-pad background--cover hide__bg-image--tablet" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/digital-healthcare-collage.jpg);">
        <div class="gl-container" style="background-color: transparent;">
            <div class="container__one-half gl-text-pad">
                <h2>Innovative solutions to navigate healthcare’s digital transformation.</h2>
                <p>As COVID-19 is presenting the most pressing challenges to healthcare in at least a generation, digital technologies are helping organizations respond: Cloud technologies for rapid deployment. Virtual care for connecting patients and care teams. Mobile solutions for speed and agility. And artificial intelligence and voice recognition for hands-free efficiency.</p>
                <p>Now more than ever, healthcare providers need dependable, adaptable, intuitive technology that's safe, secure, and affordable for their organizations and patients. Read on to find out how your organization can transform with always-on access to patient data to make the most informed decisions when every second counts.</p>
                <div class="btn-holder--content__callout left">
                    <?php hubspot_button($cta_code, 'Download The Innovators Booklet'); ?>
                </div>
            </div>
        </div>
    </div>
    <!-- END Block 1 -->

    <!-- START Block 2 -->
    <div class="container no-pad">
        <div class="gl-container">
            <div class="container__one-half background--cover" style="background-image: url( <?php print $url; ?>/sites/all/themes/meditech/images/campaigns/woman-looking-confident-with-clouds-in-foreground.jpg); min-height:450px;"></div>
            <div class="container__one-half gl-text-pad background--cover text--white" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-gradient--bg.jpg);">
                <h2>Explore new possibilities in the Cloud.</h2>
                <p>Your healthcare organization’s potential should not be limited by legacy technology. Hardwired devices and data storage systems can be a burden to procure and maintain, especially for healthcare organizations shifting their focus to COVID-related spikes in patient volumes.</p>
                <p><a href="<?php print $url; ?>/ehr-solutions/meditech-as-a-service-maas">MEDITECH as a Service (MaaS)</a> delivers a cloud-hosted, scalable, modern EHR through streamlined implementation and pricing plans. Our subscription program includes the option of securely hosting patient data using resources like Google Cloud Platform.</p>
                <p><a href="<?php print $url; ?>/ehr-solutions/cloud-platform">MEDITECH Cloud Platform</a> further complements your EHR, enhancing the power, security, and affordability of Expanse with additional subscription-based solutions.</p>
                <p>Choose one or a combination of our growing set of cloud-based solutions:</p>
                <ul class="fa-ul">
                    <div id="modal2" class="modal">
                        <a class="close-modal" href="javascript:void(0)">&times;</a>
                        <div class="modal-content modal-content--vid">
                            <iframe src="https://player.vimeo.com/video/544527596" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                        </div>
                    </div>
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>
                        <div class="open-modal" data-target="modal2"><span class="add-hover">Expanse Patient Connect</span></div>, which facilitates automated, proactive communication with patients through their preferred channel, including text, email, and phone
                    </li>
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span><a href="<?php print $url; ?>/ehr-solutions/virtual-care">Virtual Care</a>, for hosting remote visits, whether routine or urgent</li>
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/expansenow.pdf" target="_blank">Expanse NOW</a>, for helping providers coordinate care wherever they are via our mobile app</li>
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/highavailabilitysnapshot.pdf" target="_blank">High Availability SnapShot</a>, for uninterrupted access to patient data during unplanned downtime events.</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END Block 2 -->

    <!-- START Block 3 -->
    <div class="container no-pad">
        <div class="gl-container">
            <div class="container__one-half gl-text-pad--xp">
                <h2>“OK, MEDITECH, make my life easier.”</h2>
                <p>MEDITECH and Nuance have collaborated to integrate artificial intelligence and voice-driven navigation into <a href="<?php print $url; ?>/ehr-solutions/virtual-assistant?hsCtaTracking=fca506cd-8f0c-4f7e-9b94-6026b7d3d4ce%7C1d0d7964-7d2c-4bc0-9a97-4b500911d47c">Expanse Virtual Assistant</a>, allowing physicians to use simple and intuitive voice commands for hands-free access to key areas of the patient chart.</p>
                <p>Physicians can now <a href="https://blog.meditech.com/exploring-the-power-and-potential-of-expanse-virtual-assistant-with-nuance">interact with their EHR</a> just like they do with other devices they use outside of work.</p>
                <div class="btn-holder--content__callout left">
                    <?php hubspot_button('fca506cd-8f0c-4f7e-9b94-6026b7d3d4ce', "Learn More About Expanse Virtual Assistant"); ?>
                </div>
            </div>
            <div class="container__one-half background--cover flex-order--reverse" style="background-image: url( <?php print $url; ?>/sites/all/themes/meditech/images/campaigns/doctor-using-virtual-assistant-speaking-with-patients.jpg); min-height:450px;">
            </div>
        </div>
    </div>
    <!-- END Block 3 -->

    <!-- START Block 4 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/green-gradient--bg.jpg); padding: 4em;">
        <div class="container__centered">
            <div class="container__one-half text--white">
                <h2>Powerful mobility tools for clinicians on the front lines.</h2>
                <p><a href="<?php print $url; ?>/ehr-solutions/meditech-nursing">Expanse Patient Care</a> provides nurses and specialists with complete access to the EHR, personalized to match their unique workflows — freeing them to move without limitations, wherever they're needed. Convenient, web-based tools give them full, mobile access to the enterprise-wide EHR, a unified user experience, and clinical decision support.</p>
                <p>Furthermore, <a href="<?php print $url; ?>/ehr-solutions/meditech-nursing/#MypatPoC">Expanse Point of Care</a> enhances patient care by providing documentation and ordering at the bedside via a smartphone so nurses and therapists have the right information, the way they want to see it. Better for patients, better for clinicians, no matter what challenges come their way.</p>
            </div>
            <div class="container__one-half center phone-margin">
                <div id="modal1" class="modal">
                    <a class="close-modal" href="javascript:void(0)">&times;</a>
                    <div class="modal-content">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Point-of-Care--Screenshot.jpg" alt="MEDITECH Point of Care Screenshot">
                    </div>
                </div>
                <div class="open-modal" data-target="modal1">
                    <div class="phone--white">
                        <img style="width: 275px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Point-of-Care--Screenshot.jpg" alt="MEDITECH Point of Care Screenshot">
                    </div>
                    <div class="mag-bg">
                        <i class="mag-icon fas fa-search-plus"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Block 4 -->


    <!-- START Block 5 -->
    <div class="container no-pad--bottom">
        <div class="container__centered center">
            <div class="auto-margins">
                <h2>Keep your digital front door open for your patients.</h2>
                <p>MEDITECH’s Patient and Consumer Health Portal helps patients keep up with their health and stay connected with their care teams, improving engagement and building patient loyalty. The portal includes several features designed to make the patient experience better, including:</p>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="container__centered right">
            <div class="container__one-half">
                <div class="auto-margins--one-half">
                    <p><a href="<?php print $url; ?>/ehr-solutions/virtual-care">Virtual Care</a> for connecting safely and conveniently with clinicians for scheduled and urgent care consultations and maintaining the relationship with their care team.</p>
                </div>
            </div>
            <div class="container__one-half">
                <div class="bg-pattern--container">
                    <video autoplay loop muted playsinline class="demo">
                        <source src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Screenshot-GIF--Expanse-Virtual-Visits.mp4" type="video/mp4">
                    </video>
                    <button class="videoButton pause" aria-labelledby="button-label">
                        <span id="button-label" hidden>Pause Demo</span>
                    </button>
                    <div class="bg-pattern--green-dots bg-pattern--right" style="top: -2.9em;"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="container no-pad--top">
        <div class="container__centered center">
            <div class="container__one-third box-pad-2">
                <div class="circle-icon">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--centralized-scheduling.svg" style="width: 175px;" alt="Centralized scheduling icon">
                </div>
                <p>Centralized scheduling, test results, Patient Generated Health Data (PGHD), bill pay, and communication with care teams to enhance patient access and convenience.</p>
            </div>
            <div class="container__one-third box-pad-2">
                <div class="circle-icon">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--health-records-iphone.svg" style="width: 175px;" alt="Health records on iPhone® icon">
                </div>
                <p><a href="<?php print $url; ?>/news/health-records-on-iphone-available-to-consumers-enrolled-in-meditechs-patient-portal">Health records on iPhone®</a> for aggregating health information, including data from fitness trackers and remote monitoring devices, to help patients take charge of their care. Patients using iOS 15 can also share certain health data from the Health app with their care providers to provide them with a more complete picture of their health and support more informed conversations.</p>
            </div>
            <div class="container__one-third box-pad-2">
                <div class="circle-icon">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--patient-registration.svg" style="width: 175px;" alt="Patient pre-registration icon">
                </div>
                <p>Patient pre-registration, questionnaires, and self check-in for in-person visits, to improve patient throughput and reduce wait times.</p>
            </div>
        </div>
    </div>
    <!--END Block 5-->

    <!-- START Block 6 -->
    <div class="container background--cover text--white" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/rotating-dna-glowing-molecule.jpg); background-position: top; padding: 5em 0;">
        <div class="container__centered">
            <div class="container__one-half transparent-overlay--xp">
                <h2>The first fully integrated, EHR-based genomics solution is here.</h2>
                <p><a href="<?php print $url; ?>/ehr-solutions/meditech-genomics">Expanse Genomics</a> puts the power of precision medicine in the hands of every physician, in every community. Now providers can order genetic tests, receive and review results, and access the interpretation and guidance they need to understand and apply those results with confidence. All from within their Expanse EHR. Empower your providers. Empower your patients. And deliver cutting edge care in your community.</p>
            </div>
        </div>
    </div>
    <!-- END Block 6 -->

    <!-- START Block 7 -->
    <div class="container no-pad">
        <div class="gl-container">
            <div class="container__one-half background--cover" style="background-image: url( <?php print $url; ?>/sites/all/themes/meditech/images/campaigns/patient-speaking-to-doctor-virtual-care.jpg); min-height:450px; background-position: top;"></div>
            <div class="container__one-half background--cover text--white gl-text-pad--xp" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-gradient--bg.jpg);">
                <h2>Support during COVID-19 and beyond.</h2>
                <p>From extending free use of our Virtual Visits solution to all customers at the start of the pandemic, to providing ongoing guidance for drive-thru testing, to helping customers prepare to administer and record vaccinations, MEDITECH provided important tools to our customers as they responded to this major health crisis.</p>
                <p>Frontline care teams and back office administrative staff alike embraced MEDITECH solutions in new ways to treat their patients and stop the spread. Read just a few of their impressive stories on our <a href="<?php print $url; ?>/covid-19-resources#customer-experiences">Customer Experiences</a> page.</p>
            </div>
        </div>
    </div>
    <!-- END Block 7 -->

    <!-- START Block 8 -->
    <div class="container no-pad">
        <div class="gl-container">
            <div class="container__one-half gl-text-pad">
                <div class="center" style="margin-bottom: 2em;">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/logo--expanse-now.svg" alt="Expanse Now logo" style="width: 350px;">
                </div>
                <h2>Stay connected with Expanse Now.</h2>
                <p>Expanse NOW allows physicians to efficiently manage their workload from anywhere using intuitive mobile device conventions and voice commands.
                    Optimized for the smaller form factors of smartphones, Expanse NOW allows doctors to:</p>
                <ul class="fa-ul">
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Manage tasks and workload in Expanse from a smartphone app </li>
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Use voice navigation for to-do messages, reminders, and task updates</li>
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Coordinate care with clinical teams and patients.</li>
                </ul>
            </div>
            <div class="container__one-half background--cover flex-order--reverse" style="background-image: url( <?php print $url; ?>/sites/all/themes/meditech/images/campaigns/man-using-cell-phone-car-back-seat.jpg); min-height:550px;"></div>
        </div>
    </div>
    <!-- END Block 8 -->

    <!-- START Block 9 -->
    <div class="background--cover text--white center" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/green-gradient--bg.jpg); padding:4em;">
        <div class="container no-pad">
            <div class="container__centered">
                <div class="auto-margins">
                    <h2>High Availability SnapShot.</h2>
                    <p>Leverage the latest technical innovations to keep your patient data safe and secure in the cloud with <a href="https://home.meditech.com/en/d/mktcontent/otherfiles/highavailabilitysnapshot.pdf" target="_blank">High Availability SnapShot</a>, which provides immediate access to patient data via a web browser during unanticipated downtime.</p>
                    <p>High Availability SnapShot:</p>
                </div>
            </div>
        </div>
        <div class="container no-pad">
            <div class="container__centered">
                <div class="container__one-third">
                    <div class="circle-icon">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--disaster-recovery.svg" style="width: 175px;" alt="Disaster recovery icon">
                    </div>
                    <p>Augments disaster recovery and business continuance strategies</p>
                </div>
                <div class="container__one-third">
                    <div class="circle-icon">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--cloud-platform.svg" style="width: 175px;" alt="Cloud platform icon">
                    </div>
                    <p>Hosts data securely in the Google Cloud Platform</p>
                </div>
                <div class="container__one-third">
                    <div class="circle-icon">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--data-access.svg" style="width: 175px;" alt="Data access icon">
                    </div>
                    <p>Provides access to needed data for uninterrupted care</p>
                </div>
            </div>
        </div>
    </div>
    <!-- END Block 9 -->

    <!-- START Block 10-->
    <div class="container no-pad">
        <div class="gl-container">
            <div class="container__one-half background--cover" style="background-image: url( <?php print $url; ?>/sites/all/themes/meditech/images/campaigns/nurse-using-mobile-phone.jpg); min-height:450px;"></div>
            <div class="container__one-half gl-text-pad center">
                <div style="margin-bottom: 2em;">
                    <a href="<?php print $url; ?>/meditech-api">
                        <img style="width:350px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/MEDITECH-Greenfield--green-logo.png" alt="MEDITECH Greenfield Logo">
                    </a>
                </div>
                <h2>Open the door to future innovation.</h2>
                <p>Experimenting with new digital technologies will help transform healthcare to meet the demands of the future. That’s why MEDITECH created <a href="<?php print $url; ?>/meditech-api">Greenfield</a>, an app development environment that gives developers the ability to execute APIs and test their own applications within a real MEDITECH EHR. Greenfield puts the power to innovate and transform in your hands.</p>
            </div>
        </div>
    </div>
    <!--END Block 10-->

    <!-- START Block 11 -->
    <div class="background--cover text--white center" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-gradient--bg.jpg); padding:4em 0;">
        <div class="container__centered center">

            <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
            <h2><?php print $cta->field_header_1['und'][0]['value']; ?></h2>
            <?php } ?>

            <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
            <div>
                <?php print $cta->field_long_text_1['und'][0]['value']; ?>
            </div>
            <?php } ?>

            <div class="center" style="margin-top:2em;">
                <?php hubspot_button($cta_code, 'Sign Up For The Virtual Care Webinar'); ?>
            </div>
            <div style="margin-top:1em;">
                <?php print $share_link_buttons; ?>
            </div>

        </div>
    </div>
    <!-- END Block 11 -->

</div>
<!-- end js__seo-tool__body-content -->


<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>

<!-- end campaign--node-3336.php template -->
