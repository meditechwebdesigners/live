<?php if( user_is_logged_in() ){ ?>
<!-- START style-guide--node-2809.php Testing page for Timeline -->

<?php
if( !isset($_GET['mtid']) || $_GET['mtid'] != 'review' ){
  print '<script type="text/javascript">';
  print 'window.location.replace("https://ehr.meditech.com/not-meant-for-your-eyes");';
  print '</script>';
}
else{
?>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/js/swiper.min.js"></script>

<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/css/swiper.min.css" media="all" />

<style>
  .text-shadow--black {
    text-shadow: 1px 1px 1px #000;
  }

  .inset-img {
    float: left;
    margin-right: 1em;
  }

  .timeline {
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: #fff;
    flex-direction: column;
  }

  .title {
    font-size: 38px;
    color: #616161;
    font-style: italic;
    font-weight: 800;
  }

  .swiper-container {
    height: 725px;
    width: 100%;
    position: relative;
  }

  /*
  .swiper-wrapper {
    transition: 1s cubic-bezier(0.68, -0.4, 0.27, 1.34) 0.1s;
  }
*/

  .swiper-slide {
    position: relative;
    color: #fff;
    overflow: hidden;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center center;
  }

  .swiper-slide::after {
    content: "";
    position: absolute;
    z-index: 1;
    right: -115%;
    bottom: -10%;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.7);
    box-shadow: 30px 0 800px 43vw rgba(0, 0, 0, 0.5);
    border-radius: 100%;
  }

  .swiper-slide-content {
    position: absolute;
    text-align: left;
    width: 80%;
    max-width: 70%;
    right: 50%;
    top: 13%;
    -webkit-transform: translate(50%, 0);
    transform: translate(50%, 0);
    z-index: 2;
  }

  .timeline-year {
    display: block;
    font-style: italic;
    font-size: 42px;
    /*    margin-bottom: 50px;*/
    -webkit-transform: translate3d(20px, 0, 0);
    transform: translate3d(20px, 0, 0);
    color: #00bc6f;
    font-weight: 300;
    opacity: 0;
    transition: 0.2s ease 0.4s;
  }

  .timeline-title {
    font-weight: 800;
    font-size: 34px;
    margin: 0 0 30px;
    opacity: 0;
    -webkit-transform: translate3d(20px, 0, 0);
    transform: translate3d(20px, 0, 0);
    transition: 0.2s ease 0.5s;
  }

  .timeline-text {
    line-height: 1.5;
    opacity: 0;
    -webkit-transform: translate3d(20px, 0, 0);
    transform: translate3d(20px, 0, 0);
    transition: 0.2s ease 0.6s;
  }

  .swiper-slide-active .timeline-year {
    opacity: 1;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
    transition: 0.4s ease 1.6s;
  }

  .swiper-slide-active .timeline-title {
    opacity: 1;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
    transition: 0.4s ease 1.7s;
  }

  .swiper-slide-active .timeline-text {
    opacity: 1;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
    transition: 0.4s ease 1.8s;
  }

  .swiper-pagination {
    right: 9.7% !important;
    height: 100%;
    display: none;
    flex-direction: column;
    justify-content: center;
    font-style: italic;
    font-weight: 300;
    font-size: 18px;
    z-index: 1;
  }

  .swiper-pagination::before {
    content: "";
    position: absolute;
    left: -30px;
    top: 0;
    height: 100%;
    width: 1px;
    background-color: rgba(255, 255, 255, 0.2);
  }

  .swiper-pagination-bullet {
    width: auto;
    height: auto;
    text-align: center;
    opacity: 1;
    background: transparent;
    color: #fff;
    margin: 15px 0 !important;
    position: relative;
  }

  .swiper-pagination-bullet::before {
    content: "";
    position: absolute;
    top: 11px;
    left: -32.5px;
    width: 6px;
    height: 6px;
    border-radius: 100%;
    background-color: #00bc6f;
    -webkit-transform: scale(0);
    transform: scale(0);
    transition: 0.2s;
  }

  .swiper-pagination-bullet-active {
    color: #00bc6f;
  }

  .swiper-pagination-bullet-active::before {
    -webkit-transform: scale(1);
    transform: scale(1);
  }

  .swiper-button-next,
  .swiper-button-prev {
    background-size: 20px 20px;
    top: 15%;
    width: 20px;
    height: 20px;
    margin-top: 0;
    z-index: 2;
    transition: 0.2s;
  }

  .swiper-button-prev {
    left: 8%;
    background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M0%2C22L22%2C0l2.1%2C2.1L4.2%2C22l19.9%2C19.9L22%2C44L0%2C22L0%2C22L0%2C22z'%20fill%3D'%23fff'%2F%3E%3C%2Fsvg%3E");
  }

  .swiper-button-prev:hover {
    -webkit-transform: translateX(-3px);
    transform: translateX(-3px);
  }

  .swiper-button-next {
    right: 8%;
    background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M27%2C22L27%2C22L5%2C44l-2.1-2.1L22.8%2C22L2.9%2C2.1L5%2C0L27%2C22L27%2C22z'%20fill%3D'%23fff'%2F%3E%3C%2Fsvg%3E");
  }

  .swiper-button-next:hover {
    -webkit-transform: translateX(3px);
    transform: translateX(3px);
  }

  @media screen and (max-width: 570px) {
    .swiper-container {
      height: 900px;
    }

    .timeline-year {
      text-align: center;
      margin-bottom: .3em;
    }

    /*

    .swiper-slide-content {
      top: 4%;
      max-width: 90%;
    }

    .swiper-button-prev,
    .swiper-button-next {
      top: 7%;
    }
*/
  }

  @media screen and (min-width: 768px) {

    .swiper-slide::after {
      right: -30%;
      bottom: -8%;
      width: 240px;
      height: 50%;
      box-shadow: 30px 0 800px 43vw rgba(0, 0, 0, 0.5);
    }

    .swiper-slide-content {
      right: 24%;
      top: 50%;
      -webkit-transform: translateY(-50%);
      transform: translateY(-50%);
      width: 70%;
    }

    .timeline-year {
      margin-bottom: 0;
      font-size: 3em;
    }

    .timeline-title {
      font-size: 46px;
      margin: 0;
    }

    .swiper-pagination {
      display: flex;
    }

    .swiper-button-prev {
      top: 23%;
      left: auto;
      right: 10%;
      -webkit-transform: rotate(90deg) translate(0, 10px);
      transform: rotate(90deg) translate(0, 10px);
    }

    .swiper-button-prev:hover {
      -webkit-transform: rotate(90deg) translate(-3px, 10px);
      transform: rotate(90deg) translate(-3px, 10px);
    }

    .swiper-button-next {
      top: auto;
      bottom: 23%;
      right: 10%;
      -webkit-transform: rotate(90deg) translate(0, 10px);
      transform: rotate(90deg) translate(0, 10px);
    }

    .swiper-button-next:hover {
      -webkit-transform: rotate(90deg) translate(3px, 10px);
      transform: rotate(90deg) translate(3px, 10px);
    }
  }

  @media screen and (max-width: 768px) {
    .swiper-slide-content {
      top: 7%;
    }

    .swiper-button-prev,
    .swiper-button-next {
      top: 11%;
    }
  }

  @media screen and (max-width: 800px) {
    .transparent-overlay {
      margin-bottom: 1em;
    }
  }

  @media screen and (min-width:800px) and (max-width: 1024px) {
    .swiper-slide-content p {
      font-size: 85%;
    }
  }

  @media screen and (min-width: 1024px) {
    .swiper-slide::after {
      right: -20%;
      bottom: -12%;
      width: 240px;
      height: 50%;
      box-shadow: 30px 0 800px 43vw rgba(0, 0, 0, 0.5);
    }

    .swiper-slide-content {
      right: 21%;
    }
  }

</style>

<div class="timeline">
  <div class="swiper-container">
    <div class="swiper-wrapper">
      <div class="swiper-slide" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/old-meditech-building2.jpg" data-year="1960's">
        <div class="swiper-slide-content"><span class="timeline-year">1960's</span>
          <div class="timeline-text text-shadow--black">
            <div class="container__one-third transparent-overlay">
              <p><span class="bold text--large">1965:</span> The Social Security Act Amendments are signed into law by President Lyndon Johnson on July 30, 1965 - establishing Medicare health insurance for the elderly, and Medicaid health insurance for the poor. This legislation would greatly expand access to healthcare services in the U.S., while challenging providers to accommodate an enormous influx of new patients.</p>
            </div>
            <div class="container__one-third transparent-overlay">
               <p style="float:left;"><img class="inset-img" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/neil-college-headshot.jpg" style="width:117px;" alt=""><span class="bold text--large">1966:</span> MIT graduate A. Neil Pappalardo collaborates with Robert Greenes and Curt Marble to develop the first computer software language for the hospital environment. Initially used for admissions and laboratory testing at Massachusetts General Hospital, the MUMPS technology continues to be used today by many large hospitals for high-throughput transaction data processing.</p>
            </div>
            <div class="container__one-third transparent-overlay">
             <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/business-partners.jpg" alt="">
              <p><span class="bold text--large">1968-1969:</span> Pappalardo joins with four other business partners to found Medical Information Technologies Inc. (MEDITECH), a Massachusetts-based software and service company selling laboratory information systems for healthcare organizations. It officially opens for business on August 4, 1969.</p>
            </div>
          </div>
        </div>
      </div>
      <div class="swiper-slide" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/old-meditech-building3.jpg" data-year="1970's">
        <div class="swiper-slide-content"><span class="timeline-year">1970's</span>
          <div class="timeline-text text-shadow--black">
            <div class="container__one-third transparent-overlay">
              <p>Healthcare costs escalate during this decade of high Medicare expenditures, rapid inflation, rising hospital expenses and profits, and greater use of medical technology. New Magnetic Resonance Imaging (MRI) and Computer Tomography (CT) scans show how digital technologies can dramatically improve early diagnosis and treatments.</p>
            </div>
            <div class="container__one-third transparent-overlay">
              <p>Although the bipartisan proposal for a national healthcare insurance program fails, President Richard Nixon signs the Health Maintenance Organization Act of 1973, providing federal endorsement, certification, and assistance for prepaid group health plans - now known as HMOs.</p>
            </div>
            <div class="container__one-third transparent-overlay">
              <p>MEDITECH expands its product line to encompass a more integrated healthcare information system (HIS), with Pharmacy, Patient Care, and Financial applications. The company also releases a powerful new platform called MAGIC - which would be the foundation of MEDITECH’s software for the next decade. </p>
            </div>
          </div>
        </div>
      </div>
      <div class="swiper-slide" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/old-meditech-office2.jpg" data-year="1980's">
        <div class="swiper-slide-content"><span class="timeline-year">1980's</span>
          <div class="timeline-text text-shadow--black">
            <div class="container__one-third transparent-overlay">
              <p>In the absence of a federally funded insurance program, U.S. healthcare shifts toward increased privatization and more corporate (profit-based) focus. Under President Ronald Reagan, Medicare shifts to payment by diagnosis (DRG), instead of by treatment.</p>
            </div>
            <div class="container__one-third transparent-overlay">
              <p>The AIDS epidemic and rising recreational cocaine use highlight a growing need for proactive education and public health strategies for higher-risk populations. At the same time, the introduction of commercial cell phones and first Macintosh/personal computers set the stage for more informed, tech-savvy consumers. </p>
            </div>
            <div class="container__one-third transparent-overlay">
              <p>MEDITECH continues to grow its integrated MAGIC product line - with Payroll/Personnel, Case Mix, and Nursing applications - while also expanding into international territory, with new customers in Canada and Saudi Arabia.</p>
            </div>
          </div>
        </div>
      </div>
      <div class="swiper-slide" style="background-image: url(https://unsplash.it/1920/500?image=14;" data-year="1990's">
        <div class="swiper-slide-content"><span class="timeline-year">1990's</span>
          <div class="timeline-text text-shadow--black">
            <div class="container__one-third transparent-overlay">
              <p>Health care costs rise at double the rate of inflation, yet federal healthcare reform still fails to pass in the U.S. Congress under President Bill Clinton. Managed care is expanded to help alleviate costs, but by the decade’s end, 44 million Americans are still living without health insurance.</p>
            </div>
            <div class="container__one-third transparent-overlay">
              <p>The Internet opens up a whole new world for patients to have greater access to healthcare information, while e-mail makes communication between providers (as well as between providers and patients) both easier and faster.</p>
            </div>
            <div class="container__one-third transparent-overlay">
              <p>With the introduction of HIPAA and the widespread release of the Institute of Medicine’s “To Err is Human” report on medical errors, clinicians must take on greater responsibility for providing safer, more secure, and higher quality care. MEDITECH addresses the increasing needs of clinicians with its new, user-friendly Client/Server IT platform, which uses Microsoft Windows’ industry standard operating system.</p>
            </div>
          </div>
        </div>
      </div>
      <div class="swiper-slide" style="background-image: url(https://unsplash.it/1920/500?image=15;" data-year="2000's">
        <div class="swiper-slide-content"><span class="timeline-year">2000's</span>
          <div class="timeline-text text-shadow--black">
            <div class="container__one-third transparent-overlay">
              <p>In 2000, the National Institutes of Health (NIH) announces the initial sequencing of the human genome - a scientific achievement that would considerably advance prevention, diagnosis, and treatment strategies for genetic disorders, including many cancers.</p>
            </div>
            <div class="container__one-third transparent-overlay">
              <p>MEDITECH introduces its 6.0 EHR platform as the first step towards a truly integrated, interoperable healthcare ideal. With the signing of Hilo Medical Center (Hilo, HI),  MEDITECH has a growing presence in all 50 U.S. states, as well as in Canada, the UK, Ireland, and South Africa. </p>
            </div>
            <div class="container__one-third transparent-overlay">
              <p>The Health Information Technology for Economic and Clinical Health (HITECH) Act, enacted by President Barack Obama as part of the American Recovery and Reinvestment Act (ARRA) of 2009, creates financial incentives for the adoption and meaningful use of EHRs. Interest and investment in Electronic Health Records predictably skyrockets, but true culture change takes time.</p>
            </div>
          </div>
        </div>
      </div>
      <div class="swiper-slide" style="background-image: url(https://unsplash.it/1920/500?image=16;" data-year="2010's">
        <div class="swiper-slide-content"><span class="timeline-year">2010's</span>
          <div class="timeline-text text-shadow--black">
            <div class="container__one-third transparent-overlay">
              <p>As part of the Affordable Care Act (ACA) to expand cost-efficient healthcare insurance coverage across the U.S., providers are pushed to adopt care delivery methods that emphasize value, and move away from old fee-for-service payment models. More healthcare organizations undertake population health and predictive surveillance strategies, in order to proactively address patients at risk for unnecessary hospitalizations or deadly sepsis infections.</p>
            </div>
            <div class="container__one-third transparent-overlay">
              <p>The U.S. opioid crisis, as well as increasing prevalence of chronic diseases like diabetes, highlights the need for providers to build strong relationships with patients and encourage better lifestyle choices for longer-term wellness. The integration of MEDITECH’s Patient Portal with wearable activity trackers empower patients to take more responsibility over their health, and communicate progress more regularly with their PCP.</p>
            </div>
            <div class="container__one-third transparent-overlay">
              <p>As healthcare expands far beyond the walls of the hospital and physician’s office, the associated administrative demands on clinicians begin to take their toll. To help guide them through this complex landscape, MEDITECH releases Expanse - the first entirely web-based EHR - offering clinicians a more mobile, personalized user experience, while connecting the complete patient story across all care settings and networks.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>
    <div class="swiper-pagination"></div>
  </div>
</div>

<script>
  var timelineSwiper = new Swiper('.timeline .swiper-container', {
    direction: 'vertical',
    loop: false,
    speed: 1000,
    pagination: '.swiper-pagination',
    paginationBulletRender: function(swiper, index, className) {
      var year = document.querySelectorAll('.swiper-slide')[index].getAttribute('data-year');
      return '<span class="' + className + '">' + year + '</span>';
    },
    paginationClickable: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    breakpoints: {
      768: {
        direction: 'horizontal',
      }
    }
  });

</script>

<?php
}
?>

<!-- END style-guide--node-2809.php Testing page for Timeline -->
<?php } ?>