<?php // This template is for each row of the Views block: NEWS HERO ....................... 

$url = $GLOBALS['base_url']; // grabs the site url
// get node ID...
$nid = $fields['nid']->content;
$node = node_load($nid);
?>  
<!-- Start views-view-fields--news-hero--block.tpl.php template -->
<div class="content__callout">
  <div class="content__callout__media content__callout__bg__img--green" style="background-image: url('<?php
    // if feature article has an image...
    if( !empty($fields['field_text_1']->content) ){
      print $fields['field_text_1']->content; // image URL
    }
    // use backup image if main image is missing...
    else{
      print $url.'/sites/all/themes/meditech/images/light-grey-squares-pattern.jpg';  
    }
  ?>');">       
  </div>
  <div class="content__callout__content--green">
    <div class="content__callout__body">
      <h2><?php print $fields['title']->content; ?></h2>
      <?php print $fields['field_long_text_1']->content; // summary ?>
      
      <div class="btn-holder--content__callout">
        <?php $hubspot_embed_code = '1f13df10-0f19-4f79-9103-7102ae657648'; ?>
        <div class="button--hubspot no-target-icon">
        <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $hubspot_embed_code; ?>"><span class="hs-cta-node hs-cta-<?php print $hubspot_embed_code; ?>" id="hs-cta-<?php print $hubspot_embed_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $hubspot_embed_code; ?>" class="news_hero_link_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $hubspot_embed_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $hubspot_embed_code; ?>.png"  alt="button"/></a></span>
          <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
          <script type="text/javascript">
            hbspt.cta.load(2897117, '<?php print $hubspot_embed_code; ?>', {});
          </script>
          </span>
        <!-- end HubSpot Call-to-Action Code -->
        </div>
        <?php // add Edit Video link...
          if( user_is_logged_in() ){ 
            print '<div style="display:block; text-align:center; margin:1em 0;"><span style="font-size:12px;">'; print l( t('Edit Hero Text'),'node/'. $fields['nid']->content .'/edit', array('attributes' => array('style' => array('color:white') ) ) ); print "</span>";
            print ' | <span style="font-size:12px;"><a href="https://app.hubspot.com/cta/2897117" target="_blank" style="color:white;">Edit Hero Button in Hubspot</a></span></div>'; 
          } 
        ?>  
      </div>
      
    </div>
  </div>
</div>
<!-- end views-view-fields--news-hero--block.tpl.php template -->