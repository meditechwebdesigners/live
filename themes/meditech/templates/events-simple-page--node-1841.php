<!-- start events-simple-page--node-1841.php -->
<?php // IF is 'Upcoming Events & Webinars' page (AKA: the Events Page) ==================================== ?>
<h1 style="display:none;">MEDITECH Events</h1>

<style>
  .bg {
    background-color: #E2F2EB;
    background-image: url(<?php print $websiteURL; ?>/sites/all/themes/meditech/images/events/MEDITECH-LIVE-22--coworkers-chatting-at-event.jpg);
  }

  .event h2 {
    display: inline-block;
    line-height: 1;
    font-weight: 600;
    font-family: "montserrat", Verdana, sans-serif;
    color: #D2479D;
    /*non-webkit fallback*/
    font-size: 2em;
    text-transform: uppercase;
    background: -webkit-linear-gradient(135deg, #e65b25, #cd4699, #af1e4b);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    margin-top: 0;
  }

  .event-logo img {
    width: 275px;
    margin: 0.5em 0;
  }

  .event.container__one-half {
    padding-left: 4em;
  }

  @media all and (max-width: 1000px) {
    .bg {
      background-image: none;
    }

    .event h2 {
      font-size: 3em;
    }

    .event.container__one-half {
      width: 100%;
      padding-left: 0;
    }

    .event img {
      width: 375px;
    }
  }

  @media all and (max-width: 440px) {
    .event h2 {
      font-size: 2em;
    }
  }

</style>

<div class="container background--cover bg">
  <div class="container__centered">
    <div class="event container__one-half">
      &nbsp;
    </div>
    <div class="event container__one-half center">
      <p class="bold text--large no-margin--bottom">CALLING ALL</p>
      <h2><span style="font-weight:800;">CHANGE</span>MAKERS</h2>
      <div class="event-logo">
        <img src="<?php print $websiteURL; ?>/sites/all/themes/meditech/images/events/MEDITECH-Live22-logo.svg" alt="MEDITECH LIVE 2022">
      </div>
      <p class="bold text--large">Foxborough, MA<br>
        September 20th - 22nd</p>
      <p style="padding:0 2em;">Join us for a transformational leadership experience!</p>
      <a href="<?php print $websiteURL; ?>/events/meditech-live-22" class="btn--orange" style="margin: 1em 0;">Read More!</a>
    </div>
  </div>
</div>

<!-- Hero -->
<?php //print views_embed_view('events_next_big_event', 'block'); // adds 'Events - Next Big Event' Views block... ?>
<!-- End Hero -->

<section class="container__centered">
  <div class="container__two-thirds">

    <div class="js__seo-tool__body-content">

      <h2>Upcoming Events</h2>

      <?php print views_embed_view('events_upcoming_events', 'block'); // adds 'Upcoming Events' Views block... ?>

      <h2 style="margin-top:2em;">Upcoming Webinars</h2>

      <?php print views_embed_view('events_upcoming_webinars', 'block'); // adds 'Upcoming Webinars' Views block... ?>

    </div>

  </div><!-- END container__two-thirds -->

  <!-- SIDEBAR -->
  <aside class="container__one-third panel">
    <div class="sidebar__nav">
      <?php
      $messnBlock = module_invoke('menu', 'block_view', 'menu-events-section-side-nav');
      print render($messnBlock['content']); 
    ?>
    </div>
  </aside>
  <!-- END SIDEBAR -->
</section>
<!-- end events-simple-page--node-1841.php -->
