<?php // This template is for each row of the Views block: VENDOR LOGOS SELECTION \\\\\\\\\\\\\\\\\\\\\\\\ ....................... 
  $url = $GLOBALS['base_url']; // grabs the site url
?>

<!-- start views-view-fields--vendor-logos-selection--block.tpl.php template -->
<div class="container__one-third no-target-icon center">
  <a href="<?php print $fields['field_website_url']->content; ?>"><?php print $fields['field_logo']->content; ?></a>
</div>
<!-- end views-view-fields--vendor-logos-selection--block.tpl.php template -->