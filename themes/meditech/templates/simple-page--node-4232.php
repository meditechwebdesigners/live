<!-- start simple-page--node-4232.tpl.php template -- Partnership brochure PAGE -->

<section class="container__centered">

  <div class="container__two-thirds">

    <div class="js__seo-tool__body-content">
      <h1 class="js__seo-tool__title"><?php print $title; ?></h1>

      <div class="container bg--green-gradient card card__info text--large center"><a href="https://meditech.us3.list-manage.com/subscribe?u=0ac5c60e03668ee7629106fe8&amp;id=3a4a14fd93">Subscribe to our quarterly newsletter, the MEDITECH Brief</a></div>

      <div class="container bg--purple-gradient card card__info text--large center"><a href="https://ehr.meditech.com/ehr-solutions/meditech-circle">MEDITECH Circle</a></div>

      <div class="container bg--green-gradient card card__info text--large center"><a href="https://home.meditech.com/en/d/govtindustryreg/homepage.htm">MEDITECH’s Regulatory resources</a></div>

      <div class="container bg--purple-gradient card card__info text--large center"><a href="https://customer.meditech.com/en/d/regulatorybestpractices/pages/regulatorymailbox.htm">Access a regulatory specialist via the Regulatory Mailbox</a></div>

      <div class="container bg--green-gradient card card__info text--large center"><a href="https://customer.meditech.com/en/d/eventscustomers/pages/0622cisoptimizingupdates.htm">Optimizing Updates and Avoiding Landmines eLearning</a></div>

      <div class="container bg--purple-gradient card card__info text--large center"><a href="https://ehr.meditech.com/meditech-professional-services">MEDITECH Professional Services</a></div>


      <div style="padding:1em 0;">
        <p><strong>Have ideas for what you’d like to hear more about in our newsletter?</strong>
          <br>Email us at <a href="mailto:meditechbrief@meditech.com">meditechbrief@meditech.com</a>.
        </p>
      </div>


      <h2>Connect with us on social media</h2>

      <ul>
        <li><a href="https://www.facebook.com/MeditechEHR" rel="noreferrer noopener" target="_blank">Facebook</a></li>
        <li><a href="https://instagram.com/meditechehr" rel="noreferrer noopener" target="_blank">Instagram</a></li>
        <li><a href="https://www.linkedin.com/company/meditech" rel="noreferrer noopener" target="_blank">LinkedIn</a></li>
        <li><a href="https://twitter.com/MEDITECH" rel="noreferrer noopener" target="_blank">Twitter</a></li>
      </ul>



    </div>


  </div>

</section>

<!-- END simple-page--node-4232.tpl.php template -->
