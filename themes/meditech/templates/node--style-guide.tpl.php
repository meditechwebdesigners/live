<?php // This template is set up to control the display of the STYLEGUIDE content type

  $url = $GLOBALS['base_url']; // grabs the site url

?>
<!-- start node--style-guide.php template -->
 
<?php
$node_id = $node->nid;
include('style-guide--node-'.$node_id.'.php');
?>
       
<div class="container__centered">
 
  <hr />

  <p><strong>Page Identification Information</strong></p>
  <table>
    <tr>
      <td style="background-color:#70d0d0; font-size:1.5em;"><strong>Guidelines</strong></td><td style="background-color:white;">&nbsp;</td>
    </tr>
    <tr>
      <td width="30%">Document Status</td><td>Approved</td>
    </tr>
    <tr>
      <td width="30%">Approved By</td><td>Sean Collins</td>
    </tr>
    <tr>
      <td width="30%">Approval Date/Time</td><td>Mar 30, 2021 13:00</td>
    </tr>
    <tr>
      <td width="30%">Approval Comment</td><td></td>
    </tr>
    <tr>
      <td width="30%">Approved Version</td><td>1</td>
    </tr>
    <tr>
      <td width="30%">Effective Date</td><td>Mar 30, 2021</td>
    </tr>
    <tr>
      <td width="30%">Review Date</td><td>Jul 29, 2022</td>
    </tr>
  </table>
  
</div>
<!-- end node--style-guide.tpl.php template -->