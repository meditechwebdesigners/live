<?php // This template is set up to control the display of the 'EHR SOLUTIONS MAIN' content type 
$url = $GLOBALS['base_url']; // grabs the site url

// check first to see if field collection exists...
$field_collection_1 = field_get_items('node', $node, 'field_fc_head_ltext_ltext_unl_1');
$field_collection_2 = field_get_items('node', $node, 'field_fc_3_ltext_boxes');
  
if( $field_collection_1 != '' && !empty($field_collection_1) ){
  $sections = multi_field_collection_data($node, 'field_fc_head_ltext_ltext_unl_1', 8); 
} 
if( $field_collection_2 != '' && !empty($field_collection_2) ){
  $text_boxes = field_collection_data($node, 'field_fc_3_ltext_boxes');
} 
?>
<!-- start node--ehr-solutions-main.tpl.php template -->
<style>
    .subtitle {
        padding-right: 4em;
    }

    .ehr-block h2 {
        margin: .5em 0 1em 0;
    }

    .ehr-block h4 {
        margin-top: 1.5em;
    }

    .ehr-block ul li {
        margin-bottom: .5em;
    }

    .ehr-block hr {
        margin: 2em 0 .5em;
        border: 2px solid #e6e9ee;
    }

    @media all and (max-width: 800px) {
        .center-mobile {
            text-align: center;
        }

        .subtitle {
            padding-right: 0;
        }

        .image-width-limit {
            width: 300px;
        }

        .ehr-block-spacing {
            margin-bottom: 2em;
        }

        .ehr-block ul li {
            margin-bottom: 1em;
        }
    }
        .btn--outline {
            background-color: transparent;
            color: #3E4545 !important;
            border-width: 4px !important;
            -webkit-transition: all 400ms ease-in-out;
        }

        .btn--orange:hover {
            -webkit-transition: all 400ms ease-in-out;
        }

        .ehr-block .btn--orange,
        .ehr-block .button--hubspot a#cta_button_2897117_32aeb45c-2849-4217-b786-c99d78b20916 {
            width: 65%;
            margin: 0 17% .5em;
        }

        .ehr-block div.button--hubspot {
            margin-bottom: 0;
        }

</style>

<div class="container ehr-solutions-main-gae" style="padding-top:1em;">

    <div class="container__centered ehr-block">
        
        <h1 style="margin-bottom:1em;"><?php print $title; ?></h1>

        <div class="container__one-half ehr-block-spacing">
            
            <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/ehr-solutions/meditech-expanse--ehr-solutions.jpg" alt="MEDITECH Expanse Logo">
            
            <h2>MEDITECH Expanse</h2>

            <p class="subtitle" style="margin-top:.25em; font-weight:normal;"><?php print render($content['field_sub_header_1']); ?></p>
            
            <ul>
                <li>
                    <a href="https://ehr.meditech.com/ehr-solutions/meditech-expanse">Explore Expanse</a>
                </li>
              <li><a href="https://ehr.meditech.com/ehr-solutions/expanse-ambulatory">Ambulatory</a></li>
                <li>
                    <a href="https://ehr.meditech.com/ehr-solutions/#fiscal-responsibility">Fiscal Responsibility</a>
                </li>
                <li>
                    <a href="https://ehr.meditech.com/ehr-solutions/#interoperability">Interoperability</a>
                </li>
                <li>
                    <a href="https://ehr.meditech.com/ehr-solutions/#nurse-specialty-care">Nurse &amp; Specialty Care</a>
                </li>
                <li>
                    <a href="https://ehr.meditech.com/ehr-solutions/#patient-experience">Patient Experience</a>
                </li>
                <li>
                    <a href="https://ehr.meditech.com/ehr-solutions/#physician-efficiency">Physician Efficiency</a>
                </li>
                <li>
                    <a href="https://ehr.meditech.com/ehr-solutions/#population-health">Population Health</a>
                </li>
                <li>
                    <a href="https://ehr.meditech.com/ehr-solutions/#quality-outcomes">Quality Outcomes</a>
                </li>
            </ul>
            
        </div>

        <div class="container__one-half">
            <div class="center" id="technologies-transform-care">
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/ehr-solutions/meditech-professional-services--ehr-solutions-image.jpg" alt="MEDITECH Professional Services logo displayed in front of 3 business people in discussion with tablets." style="width:100%;">
            </div>
            <h2>MEDITECH Professional Services</h2>
            <h3>Learn the Benefits</h3>
            <div>
                <ul>
                    <li><a href="https://ehr.meditech.com/meditech-professional-services">See how your EHR can reach its fullest potential</a></li>
                    <li><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/professionalservicesmenu.pdf">Digital Menu of Professional Services</a></li>
                    <li><a href="https://vimeo.com/meditechehr/review/333190293/5c445102d7">Introducing Professional Services</a> [video]</li>
                </ul>
            </div>
            <h3>See the Results</h3>
            <div>
                <ul>
                    <li><a href="https://blog.meditech.com/5-organizations-using-professional-services-to-drive-collaboration-and-positive-outcomes">Blog: 5 organizations using Professional Services to drive collaboration and positive outcomes</a></li>
                    <li><a href="https://f.hubspotusercontent10.net/hubfs/2897117/Case_Studies/Case_Study_Emanate_Contact_Tracing.pdf">Case Study: Emanate Health Advances COVID-19 Contact Tracing</a></li>
                    <li><a href="https://cdn2.hubspot.net/hubfs/2897117/WPs,%20Case%20Studies,%20Special%20Reports/CustomerLeaders_GoldenValley_BCA.pdf">Case Study: Golden Valley Memorial Healthcare Improves RVU Transparency</a></li>
                </ul>
            </div>
        </div>

    </div>

    <div class="container__centered ehr-block hide-on-tablets">
        <div class="container__one-half">
            <hr />
        </div>
        <div class="container__one-half">
            <hr />
        </div>
    </div>


    <div class="container__centered ehr-block" style="margin-top: 2em;">

        <div class="container__one-half ehr-block-spacing">
            <div class="center" id="interoperability">
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/ehr-solutions/orbit-shot-of-earth-with-lights-and-connective-grid.jpg" alt="family eating at backyard picnic" style="width:100%;">
            </div>
            <h2><?php print $sections[0]->field_text_1['und'][0]['value']; ?></h2>
            <h3>Learn the Benefits</h3>
            <div>
                <?php print $sections[0]->field_long_text_1['und'][0]['value']; ?>
            </div>
            <h3>See the Results</h3>
            <div>
                <?php print $sections[0]->field_long_text_2['und'][0]['value']; ?>
            </div>
        </div>

        <div class="container__one-half">
            <div class="center" id="technologies-transform-care">
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/ehr-solutions/nurse-hands-on-mobile-device.jpg" alt="family eating at backyard picnic" style="width:100%;">
            </div>
            <h2><?php print $sections[1]->field_text_1['und'][0]['value']; ?></h2>
            <h3>Learn the Benefits</h3>
            <div>
                <?php print $sections[1]->field_long_text_1['und'][0]['value']; ?>
            </div>
            <h3>See the Results</h3>
            <div>
                <?php print $sections[1]->field_long_text_2['und'][0]['value']; ?>
            </div>
        </div>

    </div>


    <div class="container__centered ehr-block hide-on-tablets">
        <div class="container__one-half">
            <hr />
        </div>
        <div class="container__one-half">
            <hr />
        </div>
    </div>


    <div class="container__centered ehr-block" style="margin-top: 2em;">

        <div class="container__one-half ehr-block-spacing">
            <div class="center" id="patient-experience">
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/ehr-solutions/female-patient-on-mobile-device-on-train.jpg" alt="female patient on mobile device on train" style="width:100%;">
            </div>
            <h2><?php print $sections[2]->field_text_1['und'][0]['value']; ?></h2>
            <h3>Learn the Benefits</h3>
            <div>
                <?php print $sections[2]->field_long_text_1['und'][0]['value']; ?>
            </div>
            <h3>See the Results</h3>
            <div>
                <?php print $sections[2]->field_long_text_2['und'][0]['value']; ?>
            </div>
        </div>

        <div class="container__one-half">
            <div class="center" id="population-health">
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/ehr-solutions/family-backyard-picnic.jpg" alt="family eating at backyard picnic" style="width:100%;">
            </div>
            <h2><?php print $sections[3]->field_text_1['und'][0]['value']; ?></h2>
            <h3>Learn the Benefits</h3>
            <div>
                <?php print $sections[3]->field_long_text_1['und'][0]['value']; ?>
            </div>
            <h3>See the Results</h3>
            <div>
                <?php print $sections[3]->field_long_text_2['und'][0]['value']; ?>
            </div>
        </div>

    </div>


    <div class="container__centered ehr-block hide-on-tablets">
        <div class="container__one-half">
            <hr />
        </div>
        <div class="container__one-half">
            <hr />
        </div>
    </div>


    <div class="container__centered ehr-block" style="margin-top: 2em;">

        <div class="container__one-half ehr-block-spacing">
            <div class="center" id="physician-efficiency">
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/ehr-solutions/male-physician-showing-tablet-to-female-patient.jpg" alt="family eating at backyard picnic" style="width:100%;">
            </div>
            <h2><?php print $sections[4]->field_text_1['und'][0]['value']; ?></h2>
            <h3>Learn the Benefits</h3>
            <div>
                <?php print $sections[4]->field_long_text_1['und'][0]['value']; ?>
            </div>
            <h3>See the Results</h3>
            <div>
                <?php print $sections[4]->field_long_text_2['und'][0]['value']; ?>
            </div>
        </div>

        <div class="container__one-half">
            <div class="center" id="nurse-specialty-care">
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/ehr-solutions/female-nurse-smiling-hugging-female-patient.jpg" alt="family eating at backyard picnic" style="width:100%;">
            </div>
            <h2><?php print $sections[5]->field_text_1['und'][0]['value']; ?></h2>
            <h3>Learn the Benefits</h3>
            <div>
                <?php print $sections[5]->field_long_text_1['und'][0]['value']; ?>
            </div>
            <h3>See the Results</h3>
            <div>
                <?php print $sections[5]->field_long_text_2['und'][0]['value']; ?>
            </div>
        </div>

    </div>


    <div class="container__centered ehr-block hide-on-tablets">
        <div class="container__one-half">
            <hr />
        </div>
        <div class="container__one-half">
            <hr />
        </div>
    </div>


    <div class="container__centered ehr-block" style="margin-top: 2em;">

        <div class="container__one-half ehr-block-spacing">
            <div class="center" id="quality-outcomes">
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/ehr-solutions/physician-and-nurse-assisting-patient-outside-in-wheelchair.jpg" alt="family eating at backyard picnic" style="width:100%;">
            </div>
            <h2><?php print $sections[6]->field_text_1['und'][0]['value']; ?></h2>
            <h3>Learn the Benefits</h3>
            <div>
                <?php print $sections[6]->field_long_text_1['und'][0]['value']; ?>
            </div>
            <h3>See the Results</h3>
            <div>
                <?php print $sections[6]->field_long_text_2['und'][0]['value']; ?>
            </div>
        </div>

        <div class="container__one-half" id="fiscal-responsibility">
            <div class="center">
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/ehr-solutions/jar-of-coins-with-growing-plant--small.jpg" alt="family eating at backyard picnic" style="width:100%;">
            </div>
            <h2><?php print $sections[7]->field_text_1['und'][0]['value']; ?></h2>
            <h3>Learn the Benefits</h3>
            <div>
                <?php print $sections[7]->field_long_text_1['und'][0]['value']; ?>
            </div>
            <h3>See the Results</h3>
            <div>
                <?php print $sections[7]->field_long_text_2['und'][0]['value']; ?>
            </div>
        </div>

    </div>


</div>
<!-- end node--ehr-solutions-main.tpl.php template -->
