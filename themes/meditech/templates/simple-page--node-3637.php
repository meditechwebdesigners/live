<!-- start simple-page--node-3637.php template -- Podcast Index page -->
<style>
	.episode--card {
		padding: 0.5em 1em;
		margin: 0.5em 0;
		border-radius: 7px;
	}

	.showcase-container {
		padding: 2em 3em;
		width: 98%;
		border-radius: 7px;
		border: 2px solid #c7e6ff;
		background-color: #f9fcff;
		display: inline-block;
		margin: 18px 0 36px 0;
		margin-left: 0.5em;
	}


	.card__wrapper {
		align-items: flex-start;
	}


	.article--card {
		width: 48%;
		padding: 0;
		margin-bottom: 2.35765%;
		background-color: #fff;
		overflow: hidden;
		border-radius: 7px;
		box-shadow: 0px 0px 20px 1px rgba(120, 120, 120, .2);
	}

	.article--info {
		padding: 0.5em 1em;
		margin-bottom: 1em;
		max-height: 450px;
		min-height: 450px;
		overflow-y: auto;

	}

	.article--info li {
		margin-bottom: 0.3em;
		line-height: 1.45em;
	}

	.article--info p {
		line-height: 1.35em;
	}

	.article--info ul {
		margin-bottom: 1em;
	}


	.hero-image {
		display: flex;
		order: 99;
		flex-grow: 1;
		align-self: flex-end;

	}

	.hero-text {
		padding-right: 1em;
	}

	.topic {
		font-weight: 400;
		margin-bottom: 0;
		border-left: 4px solid #00BC6F;
		padding-left: 0.3em;
	}

	.header-micro {
		margin-top: 0;
	}

	.hero-image img {
		height: auto;
	}

	.article--hero {

		margin-bottom: 0;
		padding-bottom: 0;
	}

	.article--header {
		padding: 1.5em 0 0 1.5em;
	}

	.bg--dark-blue-gradient {
		background: rgb(0, 53, 76);
		background: radial-gradient(circle at 10% 60%, rgba(0, 53, 76, 1) 5%, rgba(0, 117, 168, 1) 60%);
	}

	.bg--emerald-gradient {
		background: rgb(0, 53, 76);
		background: radial-gradient(circle at 10% 60%, rgba(3, 44, 36, 1) 5%, rgba(8, 126, 104, 1) 60%);
	}

	.bg--fresh-mint-gradient {
		background: rgb(0, 53, 76);
		background: radial-gradient(circle at 10% 60%, rgba(5, 79, 76, 1) 5%, rgba(0, 187, 179, 1) 60%);
	}

	.bg--meditech-green-gradient {
		background: rgb(0, 53, 76);
		background: radial-gradient(circle at 10% 60%, rgba(2, 108, 65, 1) 5%, rgba(0, 188, 111, 1) 60%);
	}

	.share-icons img {
		max-width: 3.5%
	}

	@media all and (max-width: 50em) {

		.card--wrapper {
			flex-direction: column;
		}


		.article--card {
			width: 100%;
		}

		.article--info {
			max-height: inherit;
			min-height: inherit;
		}

	}

</style>

<div class="js__seo-tool__body-content">

	<!-- Hero -->
	<div class="container__centered">
		<div class="container">
			<div class="container__one-third center no-target-icon">
				<a href="https://meditech-podcast.simplecast.com/">
					<img src="<?php print $url; ?>/sites/all/themes/meditech/images/podcasts/MEDITECH-Podcast--Apple--Resized-Web.png" alt="MEDITECH Podcasts logo">
				</a>
			</div>
			<div class="container__two-thirds" style="margin-top:2em;">
				<h1 class="js__seo-tool__title">MEDITECH Podcast Directory</h1>

				<?php print render($content['field_body']); ?>

				<div class="share-icons no-target-icon">
					<p style="float:left; margin-right:0.5em;">Subscribe to the podcast:</p>
					<a href="https://podcasts.apple.com/us/podcast/meditech-podcast/id1566866668"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/podcasts/apple-podcast--icon.png" alt="Apple Podcast Logo"></a>
					<a href="https://www.google.com/podcasts?feed=aHR0cHM6Ly9mZWVkcy5zaW1wbGVjYXN0LmNvbS9RTFYxZE93Ng%3D%3D"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/podcasts/google-podcast--icon.png" alt="Google Podcast Logo"></a>
					<a href="https://open.spotify.com/show/7q7wbf2Q6J1y5IGItYrBxR"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/podcasts/spotify--icon.png" alt="Spotify Logo"></a>
					<a href="https://meditech-podcast.simplecast.com/"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/podcasts/simplecast--icon.png" alt="Simplecast Logo"></a>
				</div>

				<!--
                <div class="btn-holder--content__callout left">
                    < ?php hubspot_button('a72fc490-9378-403f-a3e6-091819cb5557', 'Subscribe to our Podcasts'); ?>
                </div>
-->
			</div>
		</div>


		<!-- END Hero -->

		<?php print views_embed_view('podcasts', 'block'); // adds 'Podcasts' Views block... ?>


		<div class="container no-pad">
			<div class="showcase-container">
				<div class="auto-margins">
					<h3>Achieving Healthcare Excellence through Patient Safety and Equity</h3>
					<iframe height="52px" width="100%" frameborder="no" scrolling="no" seamless src="https://player.simplecast.com/34e50047-8d39-4719-8a3a-8a8acaa9e3b0?dark=false&color=F9FCFF"></iframe>
					<h3>“Better often exists in islands, little points of light that are only available to people who are in the location of an innovative practice. The challenge is how to make that true for everyone.”</h3>
					<p><span class="bold">Jennifer Zelmer, PhD</span> <br>President and CEO of Healthcare Excellence Canada</p>

				</div>
			</div>
		</div>

		<?php print views_embed_view('podcasts_part_2', 'block'); // adds 'Podcasts - part 2' Views block... ?>

		<div class="container no-pad">
			<div class="showcase-container">
				<div class="auto-margins">
					<h3>One Year Later, Health System Stories of Strength</h3>
					<iframe height="52px" width="100%" frameborder="no" scrolling="no" seamless src="https://player.simplecast.com/09bf3a15-f33c-489b-a12c-98170b8cca8e?dark=false&color=F9FCFF"></iframe>
					<h3>“This is different than a hurricane, different than a tornado. This didn’t just come and go. We had to invent the wheel.”</h3>
					<p><span class="bold">Jesse Diaz, VP, CIO</span> <br>Phoebe Putney Health System</p>
				</div>
			</div>

		</div>
	</div>
</div>
<!-- end simple-page--node-3637.php template -->
