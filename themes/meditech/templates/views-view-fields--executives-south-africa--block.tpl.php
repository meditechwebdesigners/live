<?php // This template is for each row of the Views block: EXECUTIVES - SOUTH AFRICA ....................... 
  $url = $GLOBALS['base_url']; // grabs the site url
?>
<!-- start views-view-fields--executives-south-africa--block.tpl.php template -->
<figure class="info__card">
  <div class="info__card__media desaturateToColorRollover">
    <a class="info__card__media__img"><?php print $fields['field_image']->content; ?></a>
  </div>
  <figcaption class="center">
    <h3 class="header-five no-margin"><?php print $fields['title']->content; ?></h3>
    <p><?php print $fields['field_job_title']->content; ?></p>
    <?php // add Edit Video link...
      if( user_is_logged_in() ){ 
        print '<div style="display:block; text-align:center;"><span style="font-size:12px;">'; print l( t('Edit'),'node/'. $fields['nid']->content .'/edit' ); print "</span></div>"; 
      } 
    ?> 
  </figcaption>
</figure>
<!-- end views-view-fields--executives-south-africa--block.tpl.php template -->