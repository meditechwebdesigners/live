<!-- START campaign--node-4127.php -- GREENFIELD WORKSPACE -->
<?php
$url = $GLOBALS['base_url']; // grabs the site url
  
$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
  .shadow-box--var {
    padding: 2em;
    color: #3e4545;
    background-color: #fff;
    border-radius: 7px;
    box-shadow: 0px 0px 20px 1px rgba(120, 120, 120, .2);
    max-width: 500px;
    position: absolute;
    right: 18px;
    top: 50%;
    transform: translateY(-50%);
  }

  .container--bg-img {
    max-width: 900px;
    border-radius: 7px;
  }

  .container--rel {
    position: relative;
  }

  ul.numbers {
    padding: 0;
  }

  ul.numbers li {
    list-style: none;
    text-align: center;
  }

  ul.numbers li span {
    font-size: 2em;
    font-weight: bold;
    margin-right: 0.15em;
    position: relative;
    top: 0.15em;
  }

  @media all and (max-width: 950px) {
    .shadow-box--var {
      max-width: 100%;
      position: relative;
      margin: 0 auto;
      width: 90%;
      right: 0;
    }

    .container--bg-img {
      max-width: 100%;
      border-radius: 7px;
    }

    .container--rel {
      margin-bottom: -8em;
    }
  }

  @media all and (max-width: 500px) {
    .shadow-box--var {
      transform: translateY(0%);
      width: 100%;
      margin-top: 2.35765%;
    }

    .container--rel {
      margin-bottom: 0;
    }
  }

  .btn--orange {
    border: 4px solid #ff8300 !important;
  }

  .btn--outline {
    background-color: unset;
    color: #3E4545 !important;
    border-width: 4px !important;
  }

  .btn--orange:hover {
    -webkit-transition: all 600ms ease-in-out;
  }

  .icons img {
    width: 40%;
    height: 40%;
    margin-top: 1.5em;
  }

  .pipe-margin {
    margin: 0 0.5em;
  }

</style>

<div class="js__seo-tool__body-content">


  <!-- Block -->
  <div class="container container__centered container--rel">
    <img class="container--bg-img" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-Greenfield-Workspace--2-female-developers.jpg" alt="MEDITECH Greenfield Workspace - two female developers at work">
    <div class="shadow-box--var">
      <h1>Welcome to the Greenfield Workspace!</h1>
      <p>Come create, share, and grow with us. We invite you to take the next step forward in interoperability using MEDITECH Greenfield Workspace — a testing ground for app developers to test their integration with Expanse. Developers can execute APIs and test their applications against a real MEDITECH EHR. It also gives application developers access to interactive documentation related to our APIs.</p>
      <div class="center">
        <a href="https://info.meditech.com/welcome-to-meditech-greenfield-rfi" class="btn--orange btn--outline">Register</a>&nbsp;&nbsp;&nbsp;<a href="https://greenfield.meditech.com/" class="btn--orange">Log In</a>
      </div>
    </div>
  </div>
  <!-- Block -->


  <!-- Block -->
  <div class="container bg--green-gradient">
    <div class="container__centered">
      <div class="container__two-thirds">
        <h2>What's available in the Greenfield Workspace</h2>
        <ul>
          <li><strong>US Core FHIR R4 APIs:</strong> Provides view-only access to patient-facing data after patients give the requesting app authorization to access their data. USCDI V1 and DSTU2/R4 compatible. Additional documentation can be found on our website or in our Workspace web app. Only patient workflows are supported.</li>
          <li><strong>FHIR Scheduling APIs:</strong> Allows organizations who have multiple EHRs, scheduling solutions, and ambulatory practice systems to present patients with a consistent online scheduling experience — regardless of where they are cared for in the organization. User and patient workflows are supported for Expanse customers only.</li>
        </ul>
        <p>If you have a request regarding a specific use case not referenced here, please use the <a href="https://home.meditech.com/webforms/contact.asp?rcpt=greenfieldinfo%7Cmeditech%7Ccom&rname=greenfieldinfo">Greenfield Workspace Contact form</a> to provide more information regarding your desired API integration. We will contact you with more guidance as our capabilities expand.</p>
      </div>
      <div class="container__one-third center">
        <!-- Start hidden modal box -->
        <div id="modal1" class="modal">
          <a class="close-modal" href="javascript:void(0)">&times;</a>
          <div class="modal-content">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/iPhoneX-AllRecords--screen-shot--2021.png" alt="">
          </div>
        </div>
        <!-- End hidden modal box -->
        <!-- Start modal trigger -->
        <div class="open-modal" data-target="modal1">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/iPhoneX-AllRecords--screen-shot--2021.png" alt="Apple iPhone medical health records" style="width:250px;">
          <div class="mag-bg" style="bottom: -5px; left: -5px;">
            <!-- Include if using image trigger -->
            <i class="mag-icon fas fa-search-plus"></i>
          </div>
        </div>
        <!-- End modal trigger -->
      </div>
    </div>
  </div>
  <!-- End Block -->

  <!-- Block -->
  <div class="container container__centered center icons">
    <h2 style="margin-bottom:2em;">Get started in the Greenfield Workspace</h2>
    <div class="card__wrapper">

      <div class="container__one-fourth card bg--emerald">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--learning.svg" width="50" height="50">
        <div class="card__info">
          <p><a href="https://ehr.meditech.com/ehr-solutions/how-to-work-in-the-greenfield-workspace">How to work in the Greenfield Workspace</a></p>
        </div>
      </div>
      <div class="container__one-fourth card bg--emerald">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--resources.svg" width="50" height="50">
        <div class="card__info">
          <p><a href="https://ehr.meditech.com/ehr-solutions/greenfield-resources">Greenfield resources</a></p>
        </div>
      </div>
      <div class="container__one-fourth card bg--dark-blue">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--construction.svg" width="50" height="50">
        <div class="card__info">
          <p><a href="https://greenfield.meditech.com/">Enter the Greenfield Workspace</a></p>
        </div>
      </div>
      <div class="container__one-fourth card bg--dark-blue">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--email.svg" width="50" height="50">
        <div class="card__info">
          <p><a href="https://jira.meditech.com/servicedesk/customer/portal/1">Contact our Service Desk for help</a></p>
        </div>
      </div>

    </div>
  </div>
  <!-- End Block -->


  <!-- Block -->
  <div class="gl-container bg--purple-gradient">

    <div class="container__one-half background--cover hide-on-tablets" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/three-developers-smiling-at-a-monitor.jpg);">
      &nbsp;
    </div>

    <div class="container__one-half" style="padding:6em 2em;">
      <h2 class="no-margin--top" style="margin-left:1em;">Get involved</h2>
      <div style="width:90%; padding-left:5%;">
        <div class="container__one-half">
          <h3 class="no-margin--bottom">Have a new idea?</h3>
          <p><a href="https://home.meditech.com/webforms/contact.asp?rcpt=greenfieldinfo|meditech|com&rname=greenfieldinfo">Share it with us</a>.</p>
        </div>
        <div class="container__one-half">
          <h3 class="no-margin--bottom">Want to stay informed?</h3>
          <p>Subscribe to the <a href="https://info.meditech.com/subscribe-for-greenfield-updates">Greenfield newsletter</a>.</p>
        </div>
      </div>
      <div style="width:90%; padding-left:5%;">
        <div class="container__one-half">
          <h3 class="no-margin--bottom">Learn about</h3>
          <p><a href="https://ehr.meditech.com/ehr-solutions/greenfield-alliance">Greenfield Alliance</a>.</p>
        </div>
        <div class="container__one-half">
          <h3 class="no-margin--bottom">Have a question?</h3>
          <p><a href="https://home.meditech.com/webforms/contact.asp?rcpt=greenfieldinfo|meditech|com&rname=greenfieldinfo">Contact us</a> with your Greenfield questions.</p>
        </div>
      </div>
    </div>

  </div>
  <!-- End Block -->


</div>
<!-- end js__seo-tool__body-content -->


<!-- START Buttons -->
<div class="container container__centered" style="padding-bottom: 6em;">
  <div class="center">
    <a href="https://info.meditech.com/welcome-to-meditech-greenfield-rfi" class="btn--orange btn--outline">Register for Greenfield Workspace</a>&nbsp;&nbsp;&nbsp;<a href="https://greenfield.meditech.com/" class="btn--orange">Log in to Greenfield Workspace</a>
    <div style="margin-top:2em;">
      <?php print $share_link_buttons; ?>
    </div>
  </div>
</div>
<!-- START Buttons -->



<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>

<!-- END campaign--node-4127.php -->
