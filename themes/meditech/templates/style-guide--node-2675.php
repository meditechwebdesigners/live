<?php if( user_is_logged_in() ){ ?>
<!-- START style-guide--node-2675.php Grid -->

<!--Javascript & CSS reference for Prism-->
<script type="text/javascript" src="<?php print $url; ?>/sites/all/themes/meditech/js/prism.js"></script>

<link href="<?php print $url; ?>/sites/all/themes/meditech/css/prism.css" rel="stylesheet" />

<style>
  /*  Grid Demo */
  .grid-boxes {
    background-color: #087E68;
    padding: 1em;
    margin-bottom: 1em;
    color: #fff;
    text-align: center;
  }

  .grid-boxes--nested {
    background-color: #00bc6f;
    margin-bottom: 0;
    padding: 1em;
    color: #fff;
    text-align: center;
  }

  .grid-boxes--empty {
    margin-bottom: 0;
    padding: .9em;
    text-align: center;
    background: transparent;
    color: #3e4545;
    border: 2px dashed #e6e9ee;
  }

  @media all and (max-width: 50em) {

    .grid-boxes--nested,
    .grid-boxes--empty {
      margin-bottom: 1em;
    }

    .grid-boxes--nested:last-child {
      margin-bottom: 0;
    }
  }

  /* Divide Boxes Demo  */
  .divided-box {
    background-color: rgba(255, 255, 255, .8);
    overflow: auto;
    display: flex;
    flex-flow: row wrap;
  }

  .divided-box>div {
    margin-right: 0;
    padding: 2em;
  }

  .divided-box>div:last-child {
    padding-right: 10px;
  }

  .full-width-box {
    padding: 0;
    background-color: rgba(255, 255, 255, .8);
    overflow: auto;
  }

  .full-width-box>div {
    padding: 2em;
  }

  @media all and (max-width: 50em) {
    .divided-box>div:last-child {
      padding: 2em;
    }
  }

</style>

<section class="container__centered">

  <h1 class="page__title">
    <?php print $title; ?>
  </h1>

  <div class="container__two-thirds">
    <p>The grid is one of the main building blocks to our site. It allows us to create multi-device layouts quickly and responsively. The code is very simple and can be customized from full width down to one-fifth columns.</p>

    <h2>Wrapper Containers</h2>

    <p>We'll start with our two necessary container classes: <code class="language-css">.container</code> and <code class="language-css">.container--centered</code>. These will create a horizontal block to contain the vertical columns.</p>
    <p>The <code class="language-css">.container</code> class allows the content to be full width and the <code class="language-css">.container--centered</code> class allows the content to be centered within our defined width.</p>

    <!-- Start Containers Code -->
    <pre style="border-radius: 6px;"><code class="language-html">&lt;!-- Full width container -->
&lt;div class="container">&lt;/div>

&lt;!-- Centered container -->
&lt;div class="container--centered">&lt;/div>
</code></pre>
    <!-- End Containers Code -->

    <h2>Column Variations</h2>
    <p>Next, we'll need to add some columns within our container classes. All possible grid arrangments are below.</p>
    <p><span class="italic"><strong>Note:</strong> The grid containers can be swapped from one side to the other. For example, a 1/5 container can be put on the right and a 4/5 on the left.</span></p>

    
     <div class="demo-ct">
      <div class="container no-pad">
        <div class="container__one-sixth grid-boxes">1/6</div>
        <div class="container__one-sixth grid-boxes">1/6</div>
        <div class="container__one-sixth grid-boxes">1/6</div>
        <div class="container__one-sixth grid-boxes">1/6</div>
        <div class="container__one-sixth grid-boxes">1/6</div>
        <div class="container__one-sixth grid-boxes">1/6</div>
      </div>

      <div class="container no-pad">
        <div class="container__one-sixth grid-boxes">1/6</div>
        <div class="container__five-sixths grid-boxes">5/6</div>
      </div>
    </div>

    <!-- Start 1/6 Containers Code -->
    <pre><code class="language-html">&lt;!-- Start one sixth containers -->
&lt;div class="container">
  &lt;div class="container__one-sixth">1/6&lt;/div>
  &lt;div class="container__one-sixth">1/6&lt;/div>
  &lt;div class="container__one-sixth">1/6&lt;/div>
  &lt;div class="container__one-sixth">1/6&lt;/div>
  &lt;div class="container__one-sixth">1/6&lt;/div>
  &lt;div class="container__one-sixth">1/6&lt;/div>
&lt;/div>
&lt;!-- End one sixth containers -->

&lt;!-- Start one sixth / five sixths containers -->
&lt;div class="container">
  &lt;div class="container__one-sixth">1/6&lt;/div>
  &lt;div class="container__five-sixths">5/6&lt;/div>
&lt;/div>
&lt;!-- End one sixth / five sixths containers -->
</code></pre>
    <!-- End 1/6 Containers Code -->
     
     <div class="demo-ct">
      <div class="container no-pad">
        <div class="container__one-fifth grid-boxes">1/5</div>
        <div class="container__one-fifth grid-boxes">1/5</div>
        <div class="container__one-fifth grid-boxes">1/5</div>
        <div class="container__one-fifth grid-boxes">1/5</div>
        <div class="container__one-fifth grid-boxes">1/5</div>
      </div>

      <div class="container no-pad">
        <div class="container__one-fifth grid-boxes">1/5</div>
        <div class="container__four-fifths grid-boxes">4/5</div>
      </div>
    </div>

    <!-- Start 1/5 Containers Code -->
    <pre><code class="language-html">&lt;!-- Start one fifth containers -->
&lt;div class="container">
  &lt;div class="container__one-fifth">1/5&lt;/div>
  &lt;div class="container__one-fifth">1/5&lt;/div>
  &lt;div class="container__one-fifth">1/5&lt;/div>
  &lt;div class="container__one-fifth">1/5&lt;/div>
  &lt;div class="container__one-fifth">1/5&lt;/div>
&lt;/div>
&lt;!-- End one fifth containers -->

&lt;!-- Start one fifth / four fifths containers -->
&lt;div class="container">
  &lt;div class="container__one-fifth">1/5&lt;/div>
  &lt;div class="container__four-fifths">4/5&lt;/div>
&lt;/div>
&lt;!-- End one fifth / four fifths containers -->
</code></pre>
    <!-- End 1/5 Containers Code -->

    <div class="demo-ct">
      <div class="container no-pad">
        <div class="container__one-fourth grid-boxes">1/4</div>
        <div class="container__one-fourth grid-boxes">1/4</div>
        <div class="container__one-fourth grid-boxes">1/4</div>
        <div class="container__one-fourth grid-boxes">1/4</div>
      </div>

      <div class="container no-pad">
        <div class="container__one-fourth grid-boxes">1/4</div>
        <div class="container__three-fourths grid-boxes">3/4</div>
      </div>
    </div>

    <!-- Start 1/4 Containers Code -->
    <pre><code class="language-html">&lt;!-- Start one fourth containers -->
&lt;div class="container">
  &lt;div class="container__one-fourth">1/4&lt;/div>
  &lt;div class="container__one-fourth">1/4&lt;/div>
  &lt;div class="container__one-fourth">1/4&lt;/div>
  &lt;div class="container__one-fourth">1/4&lt;/div>
&lt;/div>
&lt;!-- End one fourth containers -->

&lt;!-- Start one fourth / three fourths containers -->
&lt;div class="container">
  &lt;div class="container__one-fourth">1/4&lt;/div>
  &lt;div class="container__three-fourths">3/4&lt;/div>
&lt;/div>
&lt;!-- End one fourth / three fourths containers -->
</code></pre>
    <!-- End 1/4 Containers Code -->

    <div class="demo-ct">
      <div class="container no-pad">
        <div class="container__one-third grid-boxes">1/3</div>
        <div class="container__one-third grid-boxes">1/3</div>
        <div class="container__one-third grid-boxes">1/3</div>
      </div>

      <div class="container no-pad">
        <div class="container__one-third grid-boxes">1/3</div>
        <div class="container__two-thirds grid-boxes">2/3</div>
      </div>
    </div>

    <!-- Start 1/3 Containers Code -->
    <pre><code class="language-html">&lt;!-- Start one third containers -->
&lt;div class="container">
  &lt;div class="container__one-third">1/3&lt;/div>
  &lt;div class="container__one-third">1/3&lt;/div>
  &lt;div class="container__one-third">1/3&lt;/div>
&lt;/div>
&lt;!-- End one third containers -->

&lt;!-- Start one third / two thirds containers -->
&lt;div class="container">
  &lt;div class="container__one-third">1/3&lt;/div>
  &lt;div class="container__two-thirds">2/3&lt;/div>
&lt;/div>
&lt;!-- End one third / two thirds containers -->
</code></pre>
    <!-- End 1/3 Containers Code -->

    <div class="demo-ct">
      <div class="container no-pad">
        <div class="container__one-half grid-boxes">1/2</div>
        <div class="container__one-half grid-boxes">1/2</div>
      </div>

      <div class="container no-pad grid-boxes">1/1</div>
    </div>

    <!-- Start 1/2 Containers Code -->
    <pre><code class="language-html">&lt;!-- Start one half containers -->
&lt;div class="container">
  &lt;div class="container__one-half">1/2&lt;/div>
  &lt;div class="container__one-half">1/2&lt;/div>
&lt;/div>
&lt;!-- End one half containers -->

&lt;!-- Start full width containers -->
&lt;div class="container">1/1&lt;/div>
&lt;!-- End full width containers -->
</code></pre>
    <!-- End 1/2 Containers Code -->

    <h2>Nested Containers</h2>
    <p>You can nest grid containers indefinitely, though at a certain point it will get a bit absurd.</p>

    <div class="demo-ct">
      <div class="container no-pad">
        <div class="container__one-half grid-boxes">
          <div class="container__one-third grid-boxes--nested">1/3</div>
          <div class="container__one-third grid-boxes--nested">1/3</div>
          <div class="container__one-third grid-boxes--nested">1/3</div>
        </div>
        <div class="container__one-half grid-boxes">
          <div class="container__one-half grid-boxes--nested">
            <div class="container__one-half grid-boxes--nested" style="background-color:#3e4545; padding:0;">1/2</div>
            <div class="container__one-half grid-boxes--nested" style="background-color:#3e4545; padding:0;">1/2</div>
          </div>
          <div class="container__one-half grid-boxes--nested" style="background-color:#00bc6f;">1/2</div>
        </div>
      </div>
    </div>

    <!-- Start 1/2 Empty Containers Code -->
    <pre><code class="language-html">&lt;!-- Start nested containers -->
&lt;div class="container">
  &lt;div class="container__one-half">
    &lt;div class="container__one-third">1/3&lt;/div>
    &lt;div class="container__one-third">1/3&lt;/div>
    &lt;div class="container__one-third">1/3&lt;/div>
  &lt;/div>
  
  &lt;div class="container__one-half">
    &lt;div class="container__one-half">
      &lt;div class="container__one-half">1/2&lt;/div>
      &lt;div class="container__one-half">1/2&lt;/div>
    &lt;/div>
  &lt;/div>
&lt;/div>
&lt;!-- End nested containers -->
</code></pre>
    <!-- End 1/2 Empty Containers Code -->

    <h2>Incomplete Rows</h2>
    <p>At some point, you may want to have a container with content on just the right side and nothing on the left. For example, in these one half containers below we would still need both but one side would only have a non-breaking space (&amp;nbsp;) in it.</p>
    <p><span class="italic"><strong>Note:</strong> If the empty container is on the right you do not need to include it in the code. The containers float left by default.</span></p>

    <div class="demo-ct">
      <div class="container no-pad">
        <div class="container__one-half grid-boxes--empty" style="">(Empty Container)</div>
        <div class="container__one-half grid-boxes">1/2</div>
      </div>
    </div>

    <!-- Start 1/2 Empty Containers Code -->
    <pre><code class="language-html">&lt;!-- Start incomplete row containers -->
&lt;div class="container">
  &lt;div class="container__one-half">&amp;nbsp;&lt;/div>
  &lt;div class="container__one-half">1/2&lt;/div>
&lt;/div>
&lt;!-- End incomplete row containers -->
</code></pre>
    <!-- End 1/2 Empty Containers Code -->

  </div>

  <!-- SIDEBAR -->
  <aside class="container__one-third">

    <div class="sidebar__nav panel">
      <?php
        $styleBlock = module_invoke('menu', 'block_view', 'menu-style-guide-side-nav');
        print render($styleBlock['content']); 
        ?>
    </div>

  </aside>
  <!-- END SIDEBAR -->

</section>

<!-- END style-guide--node-2675.php Grid -->
<?php } ?>