
<!-- START campaign--node-2845.php -->
<?php // This template is set up to control the display of the campaign patient engagement content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');
$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>


<style>

    @media all and (max-width: 50em) {
        .content__callout__content {
            padding-left: 6%;
            color: #3e4545;
            padding-top: 0em;
        }

        .video {
            max-width: 100%;
        }
    }

    .video--shadow {
        border-radius: 6px;
        box-shadow: 13px 13px 40px rgba(0, 0, 0, 0.3);
    }

</style>


<div class="js__seo-tool__body-content">

<!-- Block 1 Hero -->
<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/three_generations_of_women2.jpg);background-color:#3e4545;">
  <div class="container__centered">
    <div class="container__one-half transparent-overlay--xp text--white text-shadow--black" style="background-color: rgba(95, 78, 71, 0.8);"> 
      <h1 class="js__seo-tool__title">The patient experience matters: support their journey with MEDITECH Expanse.</h1>
      <p>Getting — and keeping — patients involved in their own care is the foundation of a positive patient experience. Foster that engagement with MEDITECH Expanse. Our solutions create a <i>convenient, simple, and mobile-enabled journey for your patients.</i> What's more, Expanse helps your organization to build better partnerships between patients and their trusted providers.
      </p>

  	<div class="btn-holder--content__callout">
        <div class="center" style="margin-top:2em;">
          <?php hubspot_button($cta_code, "Sign Up For The Population Health Insights Webinar"); ?>
        </div>
      </div>

    </div>
  </div>
</div>
<!-- Close Block 1 -->


<!-- NEW CAMPAIGN BLOCK 2 -->
<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/black_abstract.jpg);">
   <div class="container__centered">
    <div class="container__one-half center">
      <div class="container no-pad">
        <div class="container__one-half">
            <figure>
                <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/iphone1.png" 
                alt="mobile figure">
            </figure>
        </div>
        <div class="container__one-half">
            <figure>
                <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/iphone2.png" 
                alt="mobile figure">
            </figure>
        </div>
      </div>
      <div style="clear:both;">
        <p class="text--white center">Questionnaires Specific to Appointment Type</p>
      </div>
    </div>
          
    <div class="container__one-half">
        <h2 class="text--white">MEDITECH's MHealth makes it easy for patients to "own" their healthcare.</h2>
            <p class="text--white">New features improve patient access and convenience:</p>
            <ul class="text--white">
              <li>Direct appointment booking</li>
              <li>Pre-visit questionnaires</li>
              <li><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/patientexperienceflyer.pdf">Pre-registration</a></li>
              <li>Shared access</li>
              <li><a href="https://www.meditech.com/productbriefs/flyers/TelehealthApproach.pdf">Remote patient monitoring capabilities</a></li>
              <li><a href="https://www.meditech.com/productbriefs/flyers/virtual_visits_Flyer.pdf">Virtual visit capabilities</a></li>
            </ul> 

          	<figure class="no-target-icon" style="display: inline-block; margin-right:1em;">
                <a href="https://itunes.apple.com/us/app/meditech-mhealth/id1143209032?mt=8" target="_blank"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/App-Store-Badge.png" alt="App Store Download Badge"></a>
            </figure>
              
            <figure class="no-target-icon" style="display: inline-block;">
                <a href="https://play.google.com/store/apps/details?id=com.meditech.PatientPhm&hl=en" target="_blank"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Google-Play-Badge.png" alt="Google Play Download Badge"></a>
            </figure>
     </div>
   </div>
</div>
<!-- End of NEW CAMPAIGN BLOCK 2 -->


 <!-- Block 3 - Video -->
<div class="content__callout border-none" style="background-color: #E6E9EE !important;">
    <div class="content__callout__media">
        <div class="content__callout__image-wrapper" style="padding:3em !important;">
            <div class="video js__video video--shadow" data-video-id="280779080">
              <figure class="video__overlay">
                <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/video-overlay--med-center-health-virtual-visits.jpg" alt="Virtual Visits">
              </figure>
              <a class="video__play-btn" href="https://vimeo.com/280779080"></a>
              <div class="video__container"></div>
            </div>
        </div>
    </div>
    <div class="content__callout__content">
        <div class="content__callout__body">
            <div class="content__callout__body__text">
                <h2>The doctor will see you <i>NOW</i>.</h2>
              <p>See healthcare through a new lens. MEDITECH's <a href="https://blog.meditech.com/delivering-care-differently-in-the-age-of-covid-19-through-virtual-care">Virtual Visit functionality</a> offers convenient, remote interaction with your patients. It's embedded into your workflow, including the ability to bill and document care. Patients no longer have to wait anxiously to have questions answered — all they need is their device of choice.</p>
            </div>
        </div>
    </div>
</div>
<!-- End Block 3 Video -->


<div class="container no-pad background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/two_tone_holding_iphone_for_abigail.jpg);">
    <div class="container__centered">
      <div class="container__one-third">
        &nbsp;
      </div>
        <div class="container__two-thirds text--white" style="padding:2em;">
            <h2>Empower Your Community with Health Records on iPhone<sup>&reg;</sup></h2>
            <p>Give your community access to their medications, allergies, test results, and other important information by syncing the MEDITECH portal with Health Records on iPhone. One aggregated record, accessible via the Health app on iPhone, simplifies data access for your community by enabling them to:
            </p>
            <ul>
              <li>View their health data across multiple participating organizations in one aggregated record.</li>
              <li>Combine their health record data with data captured on their own fitness trackers and remote monitoring devices.</li>
              <li>Have immediate access to medications, allergies, test results, procedures, conditions, immunizations, and vitals.</li>
              <li>Present key elements of their medical history to their providers, filling gaps in their records and guiding safer treatment decisions.</li>
              <li>Receive notifications when new records become available.</li>
            </ul>
        </div>
    </div>
</div>



<!-- NEW CAMPAIGN BLOCK 4 -->

<div class="container background">
    <div class="container__centered" style="padding-bottom: 90px;">
      <h2 class="text--emerald" style="padding-bottom: 20px;">Engage chronic disease patients with Expanse.</h2>
        <div class="container__one-third center" style="padding-right: 30px;">
          <div>
            <figure>
                <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Teresa_patient_series_2.jpg" alt="population analytics" style="margin-bottom: 1em; width: 400px;">
            </figure>
          </div>  
        </div>
          
        <div class="container__two-thirds">
            <ul>
              <li>Teresa has been diagnosed with CHF. She's discharged from the hospital with a Bluetooth-enabled scale connected to MEDITECH's Patient and Consumer Health Portal.</li>
              <li>She <a href="https://vimeo.com/358106877" target="_blank">shares access to the portal</a> with her daughter, Angela, who is helping her to adjust to life with a chronic condition. Teresa weighs herself daily as part of her CHF care plan.</li>
              <li>The care manager in her cardiologist's office uses <a href="https://info.meditech.com/meditechs-building-a-foundation-for-population-health-patient-registries-white-paper">registries</a> to track Teresa and other CHF patients, and can follow up if necessary.</li>
              <li>Teresa has changed her eating habits for the better, because she knows her care manager is monitoring her.</li>
            </ul> 
          </div>
        </div>

  <div class="container__centered" style="padding-bottom: 90px;">
    <h2 class="text--emerald" style="padding-bottom: 20px;">Convenience is key for engaging busy consumers.</h2>
    
          <div class="container__two-thirds" style="padding-right: 30px;">
            <ul>
              <li>Angela's daughter, Isabella, has a rash. The busy mom wants to take her child to a pediatrician, but she doesn't have time to wait for hours in an urgent care clinic.</li>
              <li>Instead, she schedules a virtual visit with Isabella's pediatrician.</li>
              <li>Angela completes a pre-visit questionnaire, pre-registers her daughter, and pays the copay via the portal.</li>
              <li>While sitting in their kitchen, Angela and Isabella can launch the visit directly from MEDITECH's Patient and Consumer Health Portal.</li>
              <li>The pediatrician documents the virtual visit in Isabella's chart. Upon noticing that Isabella was recently prescribed amoxicillin for strep, the pediatrician diagnoses the rash as an allergic reaction.</li>
              <li>She documents the allergy and e-prescribes Omnicef&reg; instead.</li>
            </ul> 
          </div>
          <div class="container__one-third center">
          <div>
            <figure>
                <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Isabella_patient_series.jpg" alt="population analytics" style="margin-top: 1em; width: 400px;">
            </figure>
          </div>  
        </div>
    </div>
  


    <div class="container__centered">
      <h2 class="text--emerald" style="padding-bottom: 20px;">Mobile capabilities keep people engaged, wherever they are.
      </h2>
        <div class="container__one-third center" style="background-color: rgba(7, 79, 65, 0.0); padding-right: 30px;">
          <div>
            <figure>
                <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Angela_patient_series.jpg" alt="population analytics" style="margin-bottom: 1em; width: 400px;">
            </figure>
          </div>  
        </div>
          
        <div class="container__two-thirds" style="background-color: rgba(7, 79, 65, 0.0);">
            <ul>
              <li>Angela receives an email reminder from her doctor that she's overdue for her annual mammogram.</li>
              <li>Via <a href="https://ehr.meditech.com/news/mhealth-meditech-in-the-app-store">MHealth</a>, Angela schedules a mammogram at a time that fits her schedule.</li>
              <li>While in the app, she notices an outstanding balance for Isabella's, and her own, prior visits. She pays both balances online and clears her account.</li>
              <li>Angela does all this in five minutes via MHealth — no need to call radiology for an appointment or mail a check.</li>
            </ul> 
          </div>
        </div>
  </div>

<!-- End of NEW CAMPAIGN BLOCK 4 -->

<!-- Block 7 - Video -->
<div class="content__callout border-none" style="background-color: #E6E9EE !important;">
    <div class="content__callout__media">
        <div class="content__callout__image-wrapper" style="padding:3em !important;">
            <div class="video js__video video--shadow" data-video-id="297139238">
                <figure class="video__overlay">
                    <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/video-overlay--ontario-shores.jpg" alt="Palo Pinto Mobile Health Initiative Video Covershot">
                </figure>
                <a class="video__play-btn" href="https://vimeo.com/297139238"></a>
                <div class="video__container"></div>
            </div>
        </div>
    </div>
    <div class="content__callout__content">
        <div class="content__callout__body">
            <div class="content__callout__body__text">
                <h2>'It's about adopting that spirit of transparency in care...'</h2>
                <p>From patient to executive, everyone has a hand in the "common courtesy" of a transparent consumer experience at Ontario Shores Centre for Mental Health Sciences. <a href="https://info.meditech.com/case-study-ontario-shores-advances-patient-engagement-with-meditech-0">Read the case study</a> to learn more about how Ontario Shores advances patient engagement.</p>
            </div>
        </div>
    </div>
</div>
<!-- End of Block 7 -->


<!-- Block 8 -->
<div class="container no-pad background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/medical_mapping3.jpg);">
    <div class="container__centered bg-overlay--black">
        <div class="container__two-thirds text--white" style="padding:2em;">
            <h2>Keep your patients and their records connected.</h2>
            <p>There are many different routes that a patient and their records can travel. But no matter what direction your organization wants to go, MEDITECH can help you get there, with <a href="https://ehr.meditech.com/ehr-solutions/meditech-interoperability"> interoperability efforts</a> that include:</p>
            <ul>
              <li>Local affiliations and regional HIEs.</li>
              <li>All connected practitioners using <a href="https://www.commonwellalliance.org/"> CommonWell Health Alliance&reg;.</a></li>
              <li>The opportunity to connect with Carequality via Commonwell Health Alliance, supporting the exchange of member records with nearly all major EHR vendors.</li>
            </ul>
        </div>
    </div>
</div>
<!-- End of Block 8 -->


</div>
<!-- end js__seo-tool__body-content -->


<!-- Block 9 -->
<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/X-background-white-left.png); background-color:#d7d1c4; padding:5.5em;">
    <div class="container__centered center">
       
      <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
        <h2>
          <?php print $cta->field_header_1['und'][0]['value']; ?>
        </h2>
      <?php } ?>

      <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
        <div>
          <?php print $cta->field_long_text_1['und'][0]['value']; ?>
        </div>
      <?php } ?>

      <div class="center" style="margin-top:2em;">
        <?php hubspot_button($cta_code, "Watch the Advancing Patient Engagement Webinar"); ?>
      </div>

      <div style="margin-top:1em;">
        <?php print $share_link_buttons; ?>
      </div>

  </div>
</div>
<!-- End of Block 9 -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>
<!-- END campaign--node-2845.php -->