<?php // This template is set up to control the display of the CUSTOMER MATERIALS PAGE WITH MENU content type

  $url = $GLOBALS['base_url']; // grabs the site url

?>
<!-- start node--customer-materials-page-menu.tpl.php template -->
 
<script type="text/javascript">
  var $jq = jQuery.noConflict();
  $jq(document).ready(function(){
    $jq('#page a[href$=".pdf"]').each(function(){
      $jq(this).attr('rel', 'noindex');
    });
  });
</script>

<?php
$node_id = $node->nid;
include('customer-materials-page-menu--node-'.$node_id.'.php');
?>
        
<!-- end node--customer-materials-page-menu.tpl.php template -->

<?php 
/*
https://www.drupal.org/docs/7/theming/template-theme-hook-suggestions

Because Drupal limits how templates can be named and creating "theme suggestions" require the use of complex preprocess functions,
I came up with this include procedure that allows us to name our templates using the content type and a node ID.

Technically you could name them anything and add a node ID but that wouldn't be very organized.

The possibility of using this base template as a wrapper for each of the included templates is also possible. 
We could have something appear above and below each of these nodes without having to use the page.tpl.php.
*/
?>