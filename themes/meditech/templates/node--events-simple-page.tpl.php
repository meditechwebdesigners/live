<?php // This template is set up to control the display of the 'Event simple pages' content type 

$url = $GLOBALS['base_url']; // grabs the site url

$node_id = $node->nid;
$special_pages = array(609, 1761, 1841, 2374, 4173, 4175, 4186, 4213, 4216, 4217, 4218);
// if current node ID (page) is in the above array, then use a special template...
if( in_array($node_id, $special_pages) === true ){
  include('events-simple-page--node-'.$node_id.'.php');
}
// otherwise use the following code...
else{
?>

<!-- start node--events-simple-page.tpl.php template -->
  <section class="container__centered">
    <div class="container__two-thirds">

      <h1 class="page__title js__seo-tool__title"><?php print $title; ?></h1>

      <div class="js__seo-tool__body-content">
        <?php print render($content['field_body']); ?>
      </div>
        

    </div><!-- END container__two-thirds -->

    <!-- SIDEBAR -->
    <aside class="container__one-third panel">
      <div class="sidebar__nav">
        <?php
          $messnBlock = module_invoke('menu', 'block_view', 'menu-events-section-side-nav');
          print render($messnBlock['content']); 
        ?>
      </div>
    </aside>
    <!-- END SIDEBAR -->
  </section>

<?php
}
?>
    
  
  <?php // SEO tool for internal use...
  if(node_access('update',$node)){
    if($node->nid != '1841' && $node->nid != '1761' && $node->nid != '609'){ // don't show on main events page or webinar page or trade show page...
      print '<!-- SEO Tool is added to this div -->';
      print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
    }
  } 
?>  
 
<!-- end node--events-simple-page.tpl.php template -->