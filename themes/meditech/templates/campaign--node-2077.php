<!-- START campaign--node-2077.php -->
<?php // This template is set up to control the display of the CAMPAIGN_MEDITECH_SURVEILLANCE content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<div class="js__seo-tool__body-content">

    <style>
        h5.new-toolkit:before {

            content: "NEW!";
            font-size: 11px;
            padding-left: 140px;
            position: absolute;
            color: #00BC6F;
            float: right;
        }

        .toolkit--wrapper {
            display: flex;
            flex-wrap: wrap;
            flex-direction: row;
            justify-content: space-evenly;
        }

        .toolkit--item {
            margin: 1.5em;
        }

        .grey-outline-box {
            border: grey 2px solid;
        }

        .grey-divider-lines-left {
            border: grey 2px solid;
            border-bottom: none;
            border-right: none;
            border-top: none;
        }

        .bg--black-coconut-gradient {
            background: #3E4545;
            background: -webkit-linear-gradient(#4c5252, #181919);
            background: -o-linear-gradient(#4c5252, #181919);
            background: -moz-linear-gradient(#4c5252, #181919);
            background-image: linear-gradient(-150deg, #4c5252, #181919);
            color: #fff;
        }

        .angled-bg--meditech-green {
            background-image: linear-gradient(-70deg, transparent 55.9%, rgba(0, 188, 111, .9) 56%);
            padding: 3em 0;
        }


        .angled-bg--white {
            background-image: linear-gradient(70deg, transparent 38.9%, rgba(255, 255, 255, .8) 39%);
            padding: 3em 0;
        }

        @media all and (max-width: 71.250em) {
            .toolkit--item {
                margin: 1em;
            }
        }

        @media all and (max-width: 62.375em) {
            .toolkit--item {
                margin: 0.7em;
            }

        }

        @media all and (max-width: 58.375em) {
            .toolkit--item {
                margin: 1.2em;
            }
        }

        @media all and (max-width: 54.375em) {
            .toolkit--item {
                margin: 1.1em;
            }
        }

        @media all and (max-width: 50em) {
            .angled-bg--white {
                background-image: linear-gradient(70deg, rgba(255, 255, 255, .85) 49.9%, rgba(255, 255, 255, .85) 50%);
            }

            .angled-bg--meditech-green {
                background-image: linear-gradient(70deg, rgba(8, 126, 104, .85) 49.9%, rgba(8, 126, 104, .85) 50%);
            }

            h5.new-toolkit:before {
                padding-left: 105px;
            }
        }

    </style>

    <!-- Hero -->
    <div class="container background--cover no-pad" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/male-patient-being-checked-on-by-female-nurse.jpg); background-position: left;">
        <div class="angled-bg--white">
            <div class="container__centered">
                <div class="container__one-half">&nbsp;</div>
                <div class="container__one-half">
                    <h1 class="js__seo-tool__title">Intervene faster with Surveillance, our predictive analytics solution</h1>
                    <p>Catch patients before they fall through the cracks, help keep them well, and intervene when they’re trending poorly. MEDITECH Surveillance analyzes your data in real time and automatically identifies patients who need attention, whether they qualify for a potential HAC or a clinical quality measure. Our integrated toolset fits smoothly into clinical workflows, helping care teams to prioritize care, take action sooner, and improve outcomes.</p>

                    <div class="center" style="margin-top:2em;">
                        <?php hubspot_button($cta_code, "Watch the Antimicrobial Stewardship Toolkit Webinar Recording"); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Hero -->

    <!--Block 2 - Video -->
    <div class="content__callout border-none">
        <div class="content__callout__media">
            <div class="content__callout__image-wrapper">
                <div class="video js__video" data-video-id="358104613">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--How-does-MEDITECH-help-identify-high-risk-patients.jpg" alt="How does MEDITECH help identify high-risk patients Video Covershot">
                    </figure>
                    <a class="video__play-btn" href="https://vimeo.com/358104613"></a>
                    <div class="video__container"></div>
                </div>
            </div>
        </div>
        <div class="content__callout__content">
            <div class="content__callout__body">
                <div class="content__callout__body__text">
                    <h2>How does MEDITECH help identify high-risk patients?</h2>
                    <p>Hear from Chief Quality and Compliance Officer Cathy Pimple, MS, APRN, as she explains how Newman Regional Health (Newman, KS) is reducing all-cause readmissions.</p>
                </div>
            </div>
        </div>
    </div>
    <!--End of Block 2-->

    <!--Block 3 - Screenshot -->
    <!-- Start hidden modal box -->
    <div id="modal1" class="modal">
        <a class="close-modal" href="javascript:void(0)">&times;</a>
        <div class="modal-content">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/XPC-Home-Screen--Large.jpg">
        </div>
    </div>
    <!-- End hidden modal box -->
    <div class="container bg--green-gradient">
        <div class="container__centered">
            <h2 class="center">Stop emerging issues in their tracks</h2>
            <div class="container__one-half">
                <p><a href="https://ehr.meditech.com/ehr-solutions/meditech-nursing">Your nurses</a> and physicians will know as soon as a patient meets the criteria for sepsis, CAUTI, and other risks.</p>

                <p>Evidence-based rules search the clinical and demographic data in MEDITECH’s EHR, 24/7. Patients who meet profile criteria automatically populate tracking boards that indicate if the patient is showing signs of a potential HAC. Clinicians can order, document, or <a href="https://ehr.meditech.com/ehr-solutions/meditechs-care-coordination">message the care team</a> from these actionable boards, right on the spot.</p>

                <p>Our integrated solution broadcasts alerts to status boards and trackers throughout MEDITECH Expanse, reducing communication delays and expediting interventions.</p>
            </div>

            <div class="container__one-half center">
                <!-- Start modal trigger -->
                <div class="open-modal" data-target="modal1">
                    <div class="tablet--white">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/XPC-Home-Screen--Small.jpg">
                    </div>
                    <!-- Add modal trigger here -->
                    <div class="mag-bg">
                        <!-- Include if using image trigger -->
                        <i class="mag-icon fas fa-search-plus"></i>
                    </div>
                </div>
                <!-- End modal trigger -->
            </div>
        </div>
    </div>
    <!--End of Block 3-->

    <!-- Block 4 - Diagram -->
    <div class="container bg--white">
        <div class="container__centered">
            <div class="page__title--center">
                <h2>Prevent sepsis and other hospital-acquired conditions</h2>
            </div>

            <br>

            <div class="container__centered center">
                <figure>
                    <img class="hide--mobile hide--tablet" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/sepsis-surveillance.png" alt="Sepsis surveillance graphic">

                    <img class="hide--desktop" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/sepsis-surveillance--vertical.png" alt="Sepsis surveillance graphic">
                </figure>
                <br>
                <p>Surveillance works in the background of MEDITECH Expanse, pushing information to you as an early warning system.</p>
            </div>
        </div>
    </div>
    <!-- End Block 4 -->

    <!-- Block 5 - Quote -->
    <!--
    <div class="container background--cover" style="background-image: url(< ?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dark-blurred-elderly-female-physician-using-laptop-at-night.jpg);">
        <article class="container__centered text--white center auto-margins">
            <figure>
                <img class="quote__content__img" src="https://ehr.meditech.com/sites/all/themes/meditech/images/quote.svg" onerror="this.src='https://ehr.meditech.com/sites/all/themes/meditech/images/quote.png';this.onerror=null;" style="width: 150px;" alt="Quote bubble">
            </figure>
            <div class="text--large italic" style="font-size:1.4em; padding-top: .75em;">
                <p>“. . . MEDITECH's Surveillance is a game changer. We now have action items built into our EHR, so we can alert physicians quickly when patients meet sepsis criteria and prompt the appropriate orders and documentation.”</p>
            </div>
            <p class="no-margin--bottom text--large bold text--meditech-green">Jon Martell, MD, CMO</p>
            <p>Hilo Medical Center</p>
            <br>
            <p>Download the <a href="https://info.meditech.com/case-study-hilo-medical-center-improves-sep-1-compliance-by-34-percent-using-meditechs-expanse-ehr">case study</a> to find out how Hilo improved SEP-1 compliance by 34 percentage points.</p>
        </article>
    </div>
-->
    <!-- End Block 5 -->

    <!-- Block 6 -->
    <div class="container bg--black-coconut">
        <div class="container__centered">
            <div class="container no-pad auto-margins">
                <h2 class="center">Versatility for improved quality and reimbursement</h2>
            </div>
            <div class="container__centered">
                <div class="container no-pad">
                    <div class="gl-container bg--dark-blue" style="background-color: inherit;">
                        <div class="container__one-half bg--white">
                            <div class="center">
                                <img style="max-width: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--countdown-clock.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--countdown-clock.png';this.onerror=null;" alt="Flexibility icon">
                            </div>
                            <h3>The Surveillance Countdown Clock:</h3>
                            <ul>
                                <li>Automatically appears on boards and trackers, indicating the intervention must be completed within a specified timeframe.</li>
                                <li>Alerts nurses to the amount of time remaining to meet the measure.</li>
                            </ul>
                        </div>
                        <div class="container__one-half bg--dark-blue">
                            <div class="center">
                                <img style="max-width: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--flexibility--white.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--flexibility--white.png';this.onerror=null;" alt="Flexibility icon">
                            </div>
                            <h3>Flexibility to suit your needs</h3>
                            <p>The Valley Hospital created an ED copayment surveillance profile to help track whether registrars requested patients’ copayments. The profile helps staff to be more efficient in their collections and curbs lost revenue.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Block 6 -->

    <!-- Block 7 - Screenshot-->
    <!-- Start hidden modal box -->
    <div id="modal2" class="modal">
        <a class="close-modal" href="javascript:void(0)">&times;</a>
        <div class="modal-content">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/screenshot--surveillance-desktop-large.jpg">
        </div>
    </div>
    <!-- End hidden modal box -->
    <div class="container bg--blue-gradient">
        <div class="container__centered">
            <div class="auto-margins">
                <h2 class="center">Stay vigilant with a real-time global watchlist</h2>
                <p>Tired of reports that are outdated as soon as they’re run? Put the power of Quality and Surveillance in the hands of everyone who needs it, when and where they need it. Identify your most vulnerable patients across the enterprise with our actionable Watchlist. This real-time, high-level view itemizes every profile your patients meet. Simply click on the indicators to view patients’ qualifying criteria.</p>
            </div>
            <div class="center">
                <div class="open-modal" data-target="modal2">
                    <div class="tablet--white">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/screenshot--surveillance-desktop-medium.jpg">
                    </div>
                    <div class="mag-bg">
                        <i class="mag-icon fas fa-search-plus"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Block 7 -->

    <!--Block 8-->
    <div class="container bg--green-gradient">
        <div class="container__centered text--white">

            <div class="center auto-margins">
                <h2>Track COVID-19</h2>
                <p>Identify and monitor patients who meet the COVID-19 criteria with Surveillance.</p>
            </div>
            <div class="gl-container center left" style="background: inherit;">
                <div class="container__one-third">
                    <div class="center">
                        <img style="max-width: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--male-wearing-facemask--white.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--male-wearing-facemask--white.png';this.onerror=null;" alt="patient with mask icon">
                    </div>
                    <h3><strong>Profiles</strong> identify patients:</h3>
                    <ul>
                        <li>Who are at risk.</li>
                        <li>With ordered COVID-19 tests.</li>
                        <li>Who tested positive.</li>
                    </ul>
                </div>
                <div class="container__one-third">
                    <div class="center">
                        <img style="max-width: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--magnifying-glass-virus--white.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--magnifying-glass-virus--white.png';this.onerror=null;" alt="magnifying glass virus icon">
                    </div>
                    <h3><strong>Watchlists</strong> indicate all patients who are at risk for COVID-19 and their status.</h3>
                    <h5>Can be used by:</h5>
                    <ul>
                        <li>Infection control staff.</li>
                        <li>Nurse managers.</li>
                    </ul>
                </div>
                <div class="container__one-third">
                    <div class="center">
                        <img style="max-width: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--virus-warning--white.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--virus-warning--white.png';this.onerror=null;" alt="Flexibility icon">
                    </div>
                    <h3><strong>Special Indicators:</strong></h3>
                    <ul>
                        <li>Alert clinicians to at-risk patients.</li>
                        <li>Are accessed in clinician’s workflows.</li>
                        <li>Guide clinicians to take appropriate precautions.</li>
                    </ul>
                </div>

            </div>

        </div>
    </div>
    <!--End Block 8-->
    <!-- End Block 9 -->
    <div class="container bg--light-gray">
        <div class="container__centered">
            <h2 class="center">Predict early and act fast with expert-based content</h2>
            <div class="gl-container" style="background: inherit;">
                <div class="container__one-third bg--dark-blue">
                    <h3 class="center">Quality Measures</h3>
                    <ul>
                        <li>AMI</li>
                        <li>Stroke</li>
                        <li>VTE</li>
                        <li>Pneumonia</li>
                        <li>SCIP</li>
                        <li>Newborn</li>
                        <li>Sepsis</li>
                    </ul>
                </div>
                <div class="container__one-third bg--white">
                    <h3 class="center">Conditions</h3>
                    <ul>
                        <li>CAUTI</li>
                        <li>CLABSI</li>
                        <li>VAP</li>
                        <li>Sepsis</li>
                        <li>Pediatric Sepsis</li>
                        <li>Pressure Ulcers</li>
                        <li>Positive Microbiology Results</li>
                        <li>Fall Risk </li>
                    </ul>
                </div>
                <div class="container__one-third bg--dark-blue">
                    <h3 class="center">Consults</h3>
                    <ul>
                        <li>Respiratory Therapy</li>
                        <li>Occupational Therapy</li>
                        <li>Physical Therapy</li>
                        <li>Speech Therapy</li>
                        <li>Dietitian Consult</li>
                    </ul>
                </div>
                <div class="container no-pad bg--black-coconut">
                    <h3 class="center">Preventive Measures/Other</h3>
                    <div class="container__one-half">
                        <ul>
                            <li>Readmissions</li>
                            <li>Restraint Orders</li>
                            <li>CYP2C19 Genotype Poor Metabolizer</li>
                            <li>Heparin-Induced Thrombocytopenia</li>
                        </ul>
                    </div>
                    <div class="container__one-half">
                        <ul>
                            <li>Hyperkalemia</li>
                            <li>Unsigned Admit Orders</li>
                            <li>Unsigned Orders to Observation</li>
                            <li>Antimicrobial Stewardship</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Block 8 -->

    <!-- Block 9 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-gradient--bg.jpg);">
        <div class="container__centered center text--white">
            <div class="auto-margins">
                <h2 class="">Best practice for better outcomes</h2>
                <p>Exceed benchmarks with MEDITECH’s evidence-based EHR Toolkits. Condition-specific Surveillance Boards, best practice workflows, and other resources combine forces to help your organization improve clinical and operational outcomes.</p>
            </div>
            <div class="toolkit--wrapper">
                <div class="toolkit--item">
                    <img style="height: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/antimicrobial--toolkit-icon.svg" alt="Antimicrobial icon">
                    <h5><a href="https://ehr.meditech.com/ehr-solutions/ehr-toolkits/meditechs-antimicrobial-stewardship-toolkit" rel="noreferrer noopener">Antimicrobial
                            <br>Stewardship</a></h5>
                </div>
                <div class="toolkit--item">
                    <img style="height: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/cauti--toolkit-icon.svg" alt="urinary tract icon">
                    <h5><a href="https://ehr.meditech.com/ehr-solutions/ehr-toolkits/meditechs-cauti-prevention-toolkit" rel="noreferrer noopener">CAUTI
                            <br>Prevention</a></h5>
                </div>
                <div class="toolkit--item">
                    <img style="height: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Depression-Screening-and-Suicide-Prevention--toolkit-icon.svg" alt="Depression Screening & Suicide Prevention icon">
                    <h5><a href="https://ehr.meditech.com/ehr-solutions/ehr-toolkits/meditechs-depression-screening-and-suicide-prevention-toolkit" rel="noreferrer noopener">Depression Screening
                            <br>&amp; Suicide Prevention</a></h5>
                </div>
                <div class="toolkit--item">
                    <img style="height: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/diabetes--toolkit-icon.svg" alt="Diabetes-icon">
                    <h5><a href="https://ehr.meditech.com/ehr-solutions/ehr-toolkits/meditechs-diabetes-management-toolkit" rel="noreferrer noopener">Diabetes Prevention
                            <br>&amp; Management</a></h5>
                </div>
                <div class="toolkit--item">
                    <img style="height: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/fall-risk--toolkit-icon.svg" alt="person falling icon">
                    <h5><a href="https://ehr.meditech.com/ehr-solutions/ehr-toolkits/meditechs-fall-risk-management-toolkit" rel="noreferrer noopener">Fall Risk
                            <br>Management</a></h5>
                </div>
                <div class="toolkit--item">
                    <img style="height: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--heart-failure-management-toolkit.svg" alt="heart failure management icon">
                    <h5><a href="https://ehr.meditech.com/ehr-solutions/ehr-toolkits/meditechs-heart-failure-management-toolkit" rel="noreferrer noopener">Heart Failure
                            <br> Management</a></h5>
                </div>
                <div class="toolkit--item">
                    <img style="height: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--hypertension-toolkit.svg" alt="Hypertension Management icon">
                    <h5><a href="https://ehr.meditech.com/ehr-solutions/ehr-toolkits/meditechs-hypertension-management-toolkit">Hypertension
                            <br>Management</a></h5>
                </div>
                <div class="toolkit--item">
                    <img style="height: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/maternity--toolkit-icon.svg" alt="OB Hemorrhage icon">
                    <h5 class="new-toolkit"><a href="https://ehr.meditech.com/ehr-solutions/ehr-toolkits/obstetric-hemorrhage-management-toolkit" rel="noreferrer noopener">OB Hemorrhage
                            <br>Management</a></h5>
                </div>
                <div class="toolkit--item">
                    <img style="height: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/opioid--toolkit-icon.svg" alt="Opioid-icon">
                    <h5><a href="https://ehr.meditech.com/ehr-solutions/ehr-toolkits/meditechs-opioid-stewardship-toolkit" rel="noreferrer noopener">Opioid
                            <br>Stewardship</a></h5>
                </div>
                <div class="toolkit--item">
                    <img style="height: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/sepsis--toolkit-icon.svg" alt="thermometer icon">
                    <h5><a href="https://ehr.meditech.com/ehr-solutions/ehr-toolkits/meditechs-sepsis-management-toolkit" rel="noreferrer noopener">Sepsis
                            <br> Management</a></h5>
                </div>
                <div class="toolkit--item">
                    <img style="height: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--telemetry-toolkit.svg" alt="telemetry monitor icon">
                    <h5><a href="https://ehr.meditech.com/ehr-solutions/ehr-toolkits/meditechs-telemetry-appropriateness-toolkit" rel="noreferrer noopener">Telemetry
                            <br> Appropriateness</a></h5>
                </div>
            </div>
        </div>
    </div>
    <!-- End Block 9 -->

    <!-- Block 11 - CTA Block -->
    <div class="container bg--white">
        <div class="container__centered" style="text-align: center;">

            <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
            <h2>
                <?php print $cta->field_header_1['und'][0]['value']; ?>
            </h2>
            <?php } ?>

            <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
            <div>
                <?php print $cta->field_long_text_1['und'][0]['value']; ?>
            </div>
            <?php } ?>

            <div class="center" style="margin-top:2em;">
                <?php hubspot_button($cta_code, "Sign Up For The Quality Improvements And Toolkits Webinar"); ?>
            </div>

            <div style="margin-top:1em;">
                <?php print $share_link_buttons; ?>
            </div>

        </div>
    </div>
    <!-- End Block 11 -->

</div>
<!-- end js__seo-tool__body-content -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>

<style>
    @media (min-width: 941px) {
        .hide--desktop {
            display: none;
        }
    }

</style>

<!-- END campaign--node-2077.php --
