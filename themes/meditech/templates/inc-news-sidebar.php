<!-- START inc-news-sidebar.php -->
<aside class="container__one-third">

  <div class="panel">
    <div class="sidebar__nav news_sidebar_gae">
      <?php
        $mnsBlock = module_invoke('menu', 'block_view', 'menu-news-sidebar');
        print render($mnsBlock['content']); 
      ?>
    </div>
  </div>

  <div class="panel">
    <div class="sidebar__nav communications_news_sidebar_gae">
      <h3>Press Tools</h3>
      <ul class="menu">
        <li class=""><a href="https://www.meditech.com/productbriefs/flyers/corporateprofile.pdf">Corporate Profile</a></li>
        <li class=""><a href="https://www.meditech.com/productbriefs/flyers/MEDITECH_Highlights.pdf" title="corporate infographic">Corporate Infographic</a></li>
      </ul>
      <div class="center" style="margin-top:2em;">
        <a href="https://ehr.meditech.com/meditech-communications-team" class="btn--orange">Communications Team</a>
        <p style="margin-top:1em;">From the media? We're here to help.</p>
      </div>
    </div>
  </div>

</aside>
<!-- END inc-news-sidebar.php -->