<!-- START campaign--node-3154.php -->


<?php // This template is set up to control the display of the campaign: Fall 2020 Expanse Patient Care Refresh

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');
$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>


<style>
    .bg {
        background-color: #edf5f7;
        background-image: url(/sites/all/themes/meditech/images/campaigns/XPC--nurse-hugging-little-boy-white-angled-bg.jpg);
    }

    /*
    .event h2 {
        display: inline-block;
        line-height: 1;
        font-weight: 600;
        font-family: "montserrat", Verdana, sans-serif;
        color: #D2479D;
        non-webkit fallback
        font-size: 2em;
        text-transform: uppercase;
        background: -webkit-linear-gradient(135deg, #e65b25, #cd4699, #af1e4b);
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
        margin-top: 0;
    }
*/

    .event.container__one-half {
        padding-left: 4em;
    }


    .headshot {
        position: absolute;
        top: -50px;
        left: 50%;
        transform: translate(-50%);
        z-index: 3;
    }

    .headshot img {
        width: 100px;
        height: 100px;
        border-radius: 50%;
    }

    .quote-box {
        padding: 3em;
        background-color: rgba(3, 3, 30, .5);
        border-left: 5px solid #00bc6f;
        margin-bottom: 2.35765%;
        border-radius: 7px;
        box-shadow: 0px 0px 20px 1px rgba(0, 0, 0, .2);
        position: relative;
    }

    .shadow-box.w-headshot {
        padding: 4.5em 2em 2em 2em;
        position: relative;
        margin-top: 60px;
    }

    .shadow-box.w-headshot-outline {
        padding: 4.5em 2em 2em 2em;
        position: relative;
        margin-top: 60px;
        box-shadow: none;
        border: 1px solid #c8ced9;
        background-color: transparent;
    }

    .shadow-box.outline {
        box-shadow: none;
        border: 1px solid #c8ced9;
        background-color: transparent;
    }

    .card__info {
        padding: 1.5em;
    }

    blockquote {
        border-left: 2px solid #0075A8;
    }

    #point-of-care {
        top: -50px;
    }

    .fa-arrow-up:before {
        content: "\f062";
    }

    .fa-arrow-left:before {
        content: "\f060";
    }

    .fa-arrow-right:before {
        content: "\f061";
    }

    .signage-container {
        max-width: 20em;
        margin: 0 auto;
        border: #93bec5 0.5em solid;
        box-shadow: 1px 1px 10px 3px rgba(120, 120, 120, .3);
    }

    .sign-box-shadow {
        background: #86b0b7;
        height: 2px;
        width: 100%;
        margin: 0 auto;
    }

    .sign-box-header {
        background: #aed5dc;
        width: 100%;
        padding: 1em 0.3em 1em 1em;
        color: #fff;
    }

    .sign-box-list {
        background: #aed5dc;
        width: 100%;
        padding: 0.75em 0.1em 0.75em 2em;
        display: flex;
        font-weight: bold;
    }

    .sign-box-list p a {
        border-bottom: none;
    }

    .sign-box-list p {
        margin: 0;
    }

    .sign-box-list i {
        padding-right: 1em;
        font-size: 1.5em;
        color: white;
    }

    .circle-icon {
        height: 100px;
        width: 100px;
        color: #fff;
        display: flex;
        justify-content: center;
        align-items: center;
        border-radius: 50%;
        margin: 0 auto;
        margin-bottom: 1em;
    }

    .circle-icon--large {
        height: 125px;
        width: 125px;
        color: #fff;
        display: flex;
        justify-content: center;
        align-items: center;
        border-radius: 50%;
        margin: 0 auto;
        margin-bottom: 1em;
    }

    .content__callout,
    .content__callout__content {
        background-color: white;
    }

    .content__callout__image-wrapper {
        padding: 3em 0;
    }

    .content__callout__image-wrapper:after {
        border: 0;
    }

    @media all and (max-width: 62.500em) {
        .bg {
            background-image: none;
        }

        /*
        .event h2 {
            font-size: 3em;
        }
*/
        .quote-box.container__one-half:last-child {
            margin-top: 1.5em;
        }

        .event.container__one-half {
            width: 100%;
            padding-left: 0;
        }
    }

    @media all and (max-width: 56.250em) {
        .quote-box {
            width: 100%;
        }
    }

    @media all and (max-width: 50em) {

        .shadow-box.w-headshot,
        .shadow-box.w-headshot-outline {
            padding: 6em 2em 2em 2em;
            margin-top: 5em;
        }

        .quote-box {
            padding: 2em;
            margin-bottom: 3em;
        }


    }

    @media all and (max-width: 27.500em) {
        .event h2 {
            font-size: 2em;
        }
    }

</style>

<!--
    
    *****************************************************
    ****************** DESIGNER NOTES *******************
    
    NOTES FROM 5/26
    ONLY CONTENT HAS BEEN UPDATED, NO IMAGES/SCREENSHOTS HAVE BEEN REPLACED, DESIGN NEEDS COMPLETE REFRESH
    
    NOTES FROM 5/30
    FIRST PASS OF DESIGN BUT NOTHING OUTSTANDING
    
    -->

<div class="js__seo-tool__body-content">

    <h1 style="display:none;" class="js__seo-tool__title">MEDITECH Nursing Solution</h1>

    <!-- Start of Block 1 -->
    <div class="container background--cover bg">
        <div class="container__centered">
            <div class="event container__one-half">
                &nbsp;
            </div>
            <div class="event container__one-half">
                <h1 class="js__seo-tool__title">Expanse Patient Care</h1>
                <h2>The best care. <span class="italic">Humanly</span> possible.</h2>
                <p>
                    Your nurses and therapists want to move without limitations, wherever they’re needed.
                </p>
                <p>

                    Now you can give them full, mobile access to the enterprise-wide EHR, a unified user experience, and clinical decision support. Plus, Expanse Patient Care’s web-based tools are intuitive, so clinicians can concentrate on the patient instead of the technology.
                </p>
                <p>
                    <b>It’s time to get moving.</b>
                </p>
                <div class="btn-holder--content__callout" style="text-align:left;">
                    <?php hubspot_button($cta_code, "Sign Up For The Monthly Expanse Patient Care Webex"); ?>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Block 1 -->

    <!-- Start of Block 2 -->
    <div class="container" style="background-color: rgba(1, 108, 145, .07);">
        <div class="container__centered">
            <div class="flex-order--container">
                <div class="container__one-half flex-order--reverse">
                    <div class="bg-pattern--container">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/nurse-using-tablet-at-elderly-patient-bedside.jpg" style="background-position: right;">
                        <div class="bg-pattern--green-dots bg-pattern--left"></div>
                    </div>
                </div>
                <div class="container__one-half content--pad-left">
                    <h2>Better for everyone.</h2>
                    <p>When nurses and therapists practice with the smart devices they already know and love, it's a win-win for them and their patients.</p>
                    <p>Nurses and therapists can focus more on care when they:</p>
                    <ul>
                        <li>Grab a tablet to perform hand-offs, review patient education, and discuss discharge plans, right at the bedside.</li>
                        <li>Navigate the personalized chart and document when they're with patients, not later on at the nurses' station.</li>
                        <li>Administer medications without lugging disruptive technology to patients' rooms.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Block 2 -->

    <!-- Start of Block 3 -->
    <div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/about/bg--organic-shapes-green-full-width-2.svg);">
        <div class="container__centered">
            <div class="card__wrapper" style="align-items:flex-start;">
                <div class="container__one-third card">
                    <div class="card__info text--black-coconut">
                        <div class="center">
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--secure-mobile-profile--with-bg.svg" alt="Secure Electronic Health Record" style="width:30%;    height: 115px;">
                        </div>
                        <h3 class="center">Safe and simple</h3>
                        <p>"With the increased mobility of Expanse on a handheld device, nurses can slip into a room and administer medication in under a minute, saving time and barely interrupting a recovering patient."</p>
                        <p><strong>Jureza Moselina, MSN/INF, BSN, RN</strong><br>Director of Clinical Informatics<br> Val Verde Regional Medical Center</p>
                    </div>
                </div>
                <div class="container__one-third card">
                    <div class="card__info text--black-coconut">
                        <div class="center">
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--phone-with-ROI--with-bg.svg" alt="Phone with cyclical money icon" style="width:30%;    height: 115px;">
                        </div>
                        <h3 class="center">Mobility that pays for itself</h3>
                        <p>"Moving to a web-based solution allowed us to reduce our third-party network licensing costs by over two-thirds. That's more than $40,000 in annual savings — which is expected to go up as we eliminate additional servers."</p>
                        <p><strong>Carl Smith</strong><br>Chief Information Officer<br>King's Daughters Medical Center</p>
                    </div>
                </div>
                <div class="container__one-third card">
                    <div class="card__info text--black-coconut">
                        <div class="center">
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--one-click-health--with-bg.svg" alt="Phone with cyclical money icon" style="width:30%;    height: 115px;">
                        </div>
                        <h3 class="center">Leadership buy-in</h3>
                        <p>"I get excited when I see the tools for our clinicians. I want the latest and greatest for them, and I want things to be seamless. Expanse Patient Care has given us all of that and more."</p>
                        <p><strong>Carol Huesman </strong><br>Chief Information Officer<br>Major Health Partners</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Block 3 -->


    <!-- Start of Block 4 -->

    <!-- Start hidden modal box -->
    <div id="modal10" class="modal">
        <a class="close-modal" href="javascript:void(0)">&times;</a>
        <div class="modal-content">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/screenshot--XPC-patient-janice-jorrey.png">
        </div>
    </div>
    <!-- End hidden modal box -->
    <div class="container bg--purple-gradient">
        <div class="container__centered">
            <div class="container__one-half">
                <h2>Gain time with personalized tools.</h2>
                <div>
                    <p>Everybody's different. Our agile solution lets nurses and therapists practice their way. Features include:</p>
                    <ul>
                        <li>Task management using tablets and smartphones.</li>
                        <li>Real-time status board monitoring of all patient activity for smoother hand-offs.</li>
                        <li>Search functionality to find any item in a patient's chart, saving time and bringing nurses directly to meaningful patient data.</li>
                        <li>View and workflow personalization to practice the way that makes the most sense.</li>
                        <li>Same user interface as their physician colleagues.</li>
                    </ul>
                </div>
            </div>
            <div class="container__one-half center">
                <!-- Start modal trigger -->
                <div class="open-modal" data-target="modal10" style="margin:2em 0;">
                    <div class="tablet--white">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/screenshot--XPC-patient-janice-jorrey.png">
                        <!-- Add modal trigger here -->
                    </div>
                    <div class="mag-bg">
                        <!-- Include if using image trigger -->
                        <i class="mag-icon fas fa-search-plus"></i>
                    </div>

                </div>
                <!-- End modal trigger -->
            </div>
        </div>
    </div>
    <!-- End of Block 4 -->


    <!-- Start of Block 5 -->
    <div class="container bg--white">
        <div class="container__centered">
            <div class="center" style="margin-bottom:2em;">
                <h2>Patient safety. First.</h2>
                <p>Expanse provides the stopgap measures to prevent errors and preserve your peace of mind. Your nurses and patients will benefit from:</p>
            </div>
            <div class="card__wrapper" style="align-items:flex-start;">
                <div class="container__one-fourth card">
                    <div class="card__info">
                        <div class="center">
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--tablet-with-checkmark.svg" alt="Checkmark" style="width: 100%; max-height: 90px;">
                        </div>
                        <p class="text--black-coconut">
                            Bedside medication, transfusion, and phlebotomy verification.
                        </p>
                    </div>
                </div>
                <div class="container__one-fourth card">
                    <div class="card__info">
                        <div class="center">
                            <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/icon-triangle-with-exclamation-point.svg" alt="Exclamation Point" style="width: 100%; max-height: 90px;">
                        </div>
                        <p class="text--black-coconut">
                            Surveillance boards and notifications to flag for patients at risk for sepsis, falls, and other hospital-acquired conditions.
                        </p>
                    </div>
                </div>
                <div class="container__one-fourth card">
                    <div class="card__info">
                        <div class="center">
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--magnifying-glass.svg" alt="Magnifying Glass" style="width: 100%; max-height: 90px;">
                        </div>
                        <p class="text--black-coconut">
                            Evidence-based content, rules, and workflows, so you're never alone in your decision-making.
                        </p>
                    </div>
                </div>
                <div class="container__one-fourth card">
                    <div class="card__info">
                        <div class="center">
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--bell-with-exclamation-point.svg" alt="Bell Graphic" style="width: 100%; max-height: 90px;">
                        </div>
                        <p class="text--black-coconut">
                            Real-time notifications of out-of-range values and overdue interventions.
                        </p>
                        <div id="point-of-care"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Block 5 -->

    <!-- REMOVED PART OF BLOCK 5

    <div class="container shadow-box auto-margins">
                <h2 class="center text--black-coconut">Customer Successes</h2>



                <div class="container__one-third" style="width:100%;">
                    <div class="container__one-fourth">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--case-study-with-accent-bg.svg" alt="case study icon" style="height:90px; width:100%;">
                    </div>
                    <div class="container__three-fourths">
                        <p><span style="font-variant: small-caps;">[case study]</span><br>
                            <b><a href="https://info.meditech.com/en/kings-daughters-medical-center-gives-back-100-plus-hours-to-nurses-with-meditech-expanse-patient-care" style="color: #3e4545;">
                                King's Daughters Medical Center Gives Back 100+ Hours to Nurses With MEDITECH Expanse Patient Care</b></a>
                        </p>
                    </div>
                </div>

                <div class="container__one-third" style="width:100%;">
                    <div class="container__one-fourth">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--success-story.svg" alt="success study icon" style="height:90px; width:100%;">
                    </div>
                    <div class="container__three-fourths">
                        <p><span style="font-variant: small-caps;">[success story]</span><br>
                            <b><a href="https://f.hubspotusercontent10.net/hubfs/2897117/Case_Studies/Newman_Regional_Customer_Success_Story.pdf" style="color: #3e4545;">
                                Newman Regional Health Uses MEDITECH to Transform Sepsis Treatment</b></a>
                        </p>
                    </div>
                </div>

                <div class="container__one-third" style="width:100%;">
                    <div class="container__one-fourth">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--podcast.svg" alt="MEDITECH podcast icon" style="height:90px; width:100%;">
                    </div>
                    <div class="container__three-fourths">
                        <p><span style="font-variant: small-caps;">[meditech podcast]</span><br>
                            <a href="https://meditech-podcast.simplecast.com/episodes/improving-safety-and-efficiency-with-predictive-analytics" style="color: #3e4545;">
                                <b>Improving Safety and Efficiency with Predictive Analytics</b></a>
                        </p>
                    </div>
                </div>


            </div>
    -->



    <!-- Section for divider between blocks 5 & 6 -->
    <div class="container__centered">

    </div>



    <!-- Start of Block 6 -->
    <div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/about/bg--organic-shapes-green-full-width-2.svg);">
        <div class="container__centered">

            <h2 class="center text--black-coconut">Customer Successes</h2>

            <div class="card__wrapper">

                <div class="container__one-third card">
                    <figure>
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/XPC--giving-nurses-back-time.jpg">
                    </figure>
                    <div class="card__info">
                        <p>
                            <a href="https://info.meditech.com/en/kings-daughters-medical-center-gives-back-100-plus-hours-to-nurses-with-meditech-expanse-patient-care">
                                King's Daughters Medical Center Gives Back 100+ Hours to Nurses With MEDITECH Expanse Patient Care (Case Study)</a>
                        </p>
                    </div>
                </div>

                <div class="container__one-third card">
                    <figure>
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/XPC--sepsis-treatment.jpg">
                    </figure>
                    <div class="card__info">
                        <p>
                            <a href="https://f.hubspotusercontent10.net/hubfs/2897117/Case_Studies/Newman_Regional_Customer_Success_Story.pdf">
                                Newman Regional Health Uses MEDITECH to Transform Sepsis Treatment (Success Story)</a>
                        </p>
                    </div>
                </div>

                <div class="container__one-third card">
                    <figure>
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-podcast-for-cards.jpg">
                    </figure>
                    <div class="card__info">
                        <p>
                            <a href="https://meditech-podcast.simplecast.com/episodes/improving-safety-and-efficiency-with-predictive-analytics">
                                Improving Safety and Efficiency with Predictive Analytics (MEDITECH Podcast)</a>
                        </p>
                    </div>
                </div>

            </div>

        </div>
    </div>
    <!-- End of Block 6 -->


    <!-- Start hidden modal box -->
    <!--
    <div id="modal20" class="modal">
        <a class="close-modal" href="javascript:void(0)">&times;</a>
        <div class="modal-content">
            <img src="< ?php print $url; ?>/sites/all/themes/meditech/images/campaigns/screenshot--expanse-patient-care--patient-John-Fraser.png">
        </div>
    </div>
-->
    <!-- End hidden modal box -->
    <!--
    <div class="container background--cover" style="background-image: url(< ?php print $url; ?>/sites/all/themes/meditech/images/campaigns/bg--blue-layered-shapes-3.svg);">
        <div class="container__centered">
            <div class="container__one-third">
                <h2>Keep your therapists on the move.</h2>
                <div>
                    <p>Your therapists cover a lot of ground treating and educating their patients. Our mobile, web-based tools include therapy-specific widgets that allow personalized views for respiratory, speech, occupational, and physical therapists.</p>
                </div>
            </div>
            <div class="container__two-thirds center">
                <div class="open-modal" data-target="modal20" style="margin:2em 0;">
                    <div class="tablet--white">
                        <img src="< ?php print $url; ?>/sites/all/themes/meditech/images/campaigns/screenshot--expanse-patient-care--patient-John-Fraser.png">
                    </div>
                    <div class="mag-bg">
                        <i class="mag-icon fas fa-search-plus"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
-->

    <!-- Start of Block 7 -->
    <div id="modal20" class="modal">
        <a class="close-modal" href="javascript:void(0)">&times;</a>
        <div class="modal-content">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/screenshot--XPC--John-Fraser.png">
        </div>
    </div>
    <div class="container" style="background-color: rgba(1, 108, 145, .07); padding:padding:4em 0 3em 0;">
        <div class="container__centered">
            <div class="flex-order--container">
                <div class="container__one-half flex-order--reverse">
                    <div class="bg-pattern--container">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/physical-therapist-helping-patient-while-using-expanse-patient-care.jpg" style="background-position: right;">
                        <div class="bg-pattern--green-squares bg-pattern--left"></div>
                    </div>
                </div>
                <div class="container__one-half content--pad-left">
                    <h2>Keep your therapists on the move.</h2>
                    <div>
                        <p>Your therapists cover a lot of ground treating and educating their patients. Our mobile, web-based tools include therapy-specific widgets that allow personalized views for respiratory, speech, occupational, and physical therapists.</p>
                    </div>
                    <div class="open-modal" data-target="modal20" style="margin: 1em 0 0 0;max-width: 85%;">
                        <div class="tablet--white">
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/screenshot--XPC--John-Fraser.png">
                        </div>
                        <div class="mag-bg">
                            <i class="mag-icon fas fa-search-plus"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Block 7 -->

    <!-- Start of Block 8 -->
    <div class="container bg--purple-gradient" style="padding:4em 0 3em 0;">
        <div class="container__centered">

            <div class="center auto-margins">
                <h2>Close communication gaps during hand-offs.</h2>
                <p>When changing shifts, nurses need a quick, customizable method to relay patient information to oncoming colleagues. Expanse Patient Care’s hand-off functionality communicates patient information without double documenting or searching the chart, for safer transitions of care.</p>
            </div>
            <div class="container no-pad--bottom">
                <div class="container__one-half quote-box">
                    <div class="headshot">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/quote-outline.svg" alt="Quote graphic">
                    </div>

                    <p class="text--large italic">“Clear communication is the heartbeat of a first-rate hand-off. Expanse Patient Care gives us the tools to efficiently deliver patient information to our colleagues, whether at shift change or when transitioning to a different level of care.”</p>
                    <p class="bold no-margin--bottom">Jessica Troxclair, BSN, RN</p>
                    <p>Nurse Clinical Informatics Specialist <br>Lane Regional Medical Center</p>
                </div>
                <!--
                    <div class="container__one-third">
                        <img src="< ?php print $url; ?>/sites/all/themes/meditech/images/campaigns/two-nurses-elbow-bumping.jpg" style="background-position: left;">
                    </div>
-->
                <div class="container__one-half quote-box">
                    <div class="headshot">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/quote-outline.svg" alt="Quote graphic">
                    </div>
                    <p class="text--large italic">“In our busy ED, hand-offs are a great way to keep the team on the same page, without having to go through the entire chart. We can pull that info into the hand-off, and nurses love it. It's efficient, quick, and maybe even safer.”</p>
                    <p class="bold no-margin--bottom">Sara Lewis, RN</p>
                    <p>Clinical Informatics Specialist <br>Major Health Partners</p>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Block 8 -->

    <!-- Start of Block 9 -->
    <div class="container" style="background-color: rgba(1, 108, 145, .07); padding:padding:4em 0 3em 0;">
        <div class="container__centered">
            <div class="flex-order--container">
                <div class="container__one-half flex-order--reverse">
                    <div class="bg-pattern--container">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/nurse-with-mask-using-mobile-phone.jpg" style="background-position: right;">
                        <div class="bg-pattern--green-dots bg-pattern--left"></div>
                    </div>
                </div>
                <div class="container__one-half content--pad-left">
                    <h2>Stay forward-facing.</h2>
                    <p>Your nurses and therapists can stay facing their patients with this mobile, modern solution. Point of Care components give them:</p>
                    <ul>
                        <li>Intuitive navigation.</li>
                        <li>Easy, flexible documentation tools.</li>
                        <li>A convenient, reliable option that fits in their pocket.</li>
                        <li>Frictionless interactions with patients.</li>
                        <li>Access to patient charts with a simple wristband scan.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!--
    <div class="container no-pad">
        <div class="gl-container">
            <div class="container__one-half gl-text-pad background--cover no-pad text--white" style="background-image: url(< ?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-gradient--bg.jpg);">
                <h2>Stay forward facing.</h2>
                <p>Your nurses and therapists can stay facing their patients with this mobile, modern solution. Point of Care components give them:</p>
                <ul>
                    <li>A mobile, modern solution with intuitive navigation.</li>
                    <li>Easy-to-use, flexible documentation tools.</li>
                    <li>A convenient, reliable option that fits in their pocket.</li>
                    <li>Frictionless interactions with patients.</li>
                    <li>Access to patient information, whenever and wherever they need it.</li>
                    <li>Access to patient charts with a simple wristband scan</li>
                </ul>
            </div>
            <div class="container__one-half background--cover flex-order--reverse" style="background-image: url( < ?php print $url; ?>/sites/all/themes/meditech/images/campaigns/smiling-nurse-checking-phone-in-empty-hospital-room.jpg); min-height:450px; background-position: right;">
            </div>
        </div>
    </div>
-->
    <!-- End of Block 9 -->


    <!-- Start of Block 10 -->
    <div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/about/bg--organic-shapes-green-full-width-2.svg);">
        <div class="container__centered">
            <div class="flex-order--container auto-margins">
                <h2>Smart tools for every specialty.</h2>
                <p class="text--large">Whatever your role, Expanse Patient Care can be tailored to how <span class="italic">you</span> want to practice.</p>
                <div class="container__one-half">
                    <!--
                                        <div class="headshot">
                                            <img src="< ?php print $url; ?>/sites/all/themes/meditech/images/people/Bruk-Kammerman.png" alt="Bruk Kammerman head shot">
                                        </div>
                    -->

                    <ul class="fa-ul text--large">
                        <li><span class="fa-li text--emerald"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span><a href="https://info.meditech.com/case-study-ehr-steers-avera-mckennan-ed-nurse-navigator-program-to-475000-annual-savings">ED Nurses</a></li>

                        <li><span class="fa-li text--emerald"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span><a href="https://blog.meditech.com/thinking-outside-the-box-with-our-ehr-and-surveillance-in-the-rehab-setting">Rehab Nurses and Therapists</a></li>

                        <li><span class="fa-li text--emerald"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span><a href="https://f.hubspotusercontent10.net/hubfs/2897117/Case_Studies/Customer_Success_Northeastern_Vermont_L&D.pdf">Labor and Delivery Nurses</a></li>

                        <li><span class="fa-li text--emerald"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span><a href="https://f.hubspotusercontent10.net/hubfs/2897117/Case_Studies/SuccessStory_KingmanHumana.pdf">Quality Managers</a></li>
                    </ul>
                </div>
                <!--
                    <div class="container__one-third">
                        <img src="< ?php print $url; ?>/sites/all/themes/meditech/images/campaigns/two-nurses-elbow-bumping.jpg" style="background-position: left;">
                    </div>
-->
                <div class="container__one-half">
                    <!--
                                        <div class="headshot">
                                            <img src="< ?php print $url; ?>/sites/all/themes/meditech/images/people/Bruk-Kammerman.png" alt="Bruk Kammerman head shot">
                                        </div>
                    -->
                    <ul class="fa-ul text--large">
                        <li><span class="fa-li text--emerald"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span><a href="https://ehr.meditech.com/ehr-solutions/meditech-surgical-services">Surgical Nurses</a></li>

                        <li><span class="fa-li text--emerald"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span><a href="https://ehr.meditech.com/ehr-solutions/critical-care">Critical Care Nurses</a></li>

                        <li><span class="fa-li text--emerald"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span><a href="https://ehr.meditech.com/ehr-solutions/meditechs-care-coordination">Case Managers</a></li>

                        <li><span class="fa-li text--emerald"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span><a href="https://ehr.meditech.com/ehr-solutions/meditechs-care-coordination">Long-Term Care Nurses</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- OLD LAYOUT BUT KEEP FOR NOW JUNE 2022
    <div class="container background--cover" style="background-image: url(< ?php print $url; ?>/sites/all/themes/meditech/images/campaigns/illustrated-hospital-hallway.png); background-position: center;">
        <div class="container--centered" style="padding: 0 0 16em 0;">
            <div class="container no-pad center">
                <h2>Smart tools for every specialty.</h2>
                <h3>Whatever your role, Expanse Patient Care can be tailored to how <span class="italic">you</span> want to practice.</h3>
            </div>
            <div class="signage-container">
                <div class="sign-box-header">
                    <h2 class="no-margin bold center">Hospital Directory</h2>
                </div>

                <div class="sign-box-shadow"></div>

                <div class="sign-box-list">
                    <i class="fas fa-arrow-up"></i>
                    <p class="bold"><a href="https://info.meditech.com/case-study-ehr-steers-avera-mckennan-ed-nurse-navigator-program-to-475000-annual-savings">ED Nurses</a></p>
                </div>

                <div class="sign-box-shadow"></div>

                <div class="sign-box-list">
                    <i class="fas fa-arrow-up"></i>
                    <p class="bold"><a href="https://ehr.meditech.com/ehr-solutions/meditech-surgical-services">Surgical Nurses</a></p>
                </div>

                <div class="sign-box-shadow"></div>

                <div class="sign-box-list">
                    <i class="fas fa-arrow-up"></i>
                    <p class="bold"><a href="https://ehr.meditech.com/ehr-solutions/labor-and-delivery">Labor &amp; Delivery Nurses</a></p>
                </div>

                <div class="sign-box-shadow"></div>

                <div class="sign-box-list">
                    <i class="fas fa-arrow-left"></i>
                    <p class="bold"><a href="https://ehr.meditech.com/ehr-solutions/critical-care">Critical Care Nurses</a></p>
                </div>

                <div class="sign-box-shadow"></div>

                <div class="sign-box-list">
                    <i class="fas fa-arrow-left"></i>
                    <p class="bold"><a href="https://ehr.meditech.com/ehr-solutions/bridging-the-gaps-in-care">Case Managers</a></p>
                </div>

                <div class="sign-box-shadow"></div>

                <div class="sign-box-list">
                    <i class="fas fa-arrow-right"></i>
                    <p class="bold"><a href="https://ehr.meditech.com/ehr-solutions/long-term-care-post-acute-services">Long Term Care Nurses</a></p>
                </div>

                <div class="sign-box-shadow"></div>

                <div class="sign-box-list">
                    <i class="fas fa-arrow-right"></i>
                    <p class="bold"><a href="https://ehr.meditech.com/ehr-solutions/meditech-surveillance">Quality Managers</a></p>
                </div>
            </div>
        </div>
    </div>
-->
    <!-- End of Block 10 -->


    <!-- Start of Block 11 CTA -->
    <div class="container bg--purple-gradient">
        <div class="container__centered center">

            <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
            <h2>
                <?php print $cta->field_header_1['und'][0]['value']; ?>
            </h2>
            <?php } ?>

            <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
            <h3>
                <?php print $cta->field_long_text_1['und'][0]['value']; ?>
            </h3>
            <?php } ?>

            <div class="center" style="margin-top:2em;">
                <?php hubspot_button($cta_code, "Sign Up For The Expanse Patient Care Webinar"); ?>
            </div>

            <div style="margin-top:1em;">
                <?php print $share_link_buttons; ?>
            </div>

        </div>
    </div>
    <!-- End of Block 11 -->





    <!-- end js__seo-tool__body-content -->


    <?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>

    <!-- End of Campaign Node 3154 -->
