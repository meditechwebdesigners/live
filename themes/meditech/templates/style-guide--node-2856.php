<?php if( user_is_logged_in() ){ ?>
<!-- START style-guide--node-2856.php Icons -->

<!--Javascript & CSS reference for Prism-->
<script type="text/javascript" src="<?php print $url; ?>/sites/all/themes/meditech/js/prism.js"></script>

<link href="<?php print $url; ?>/sites/all/themes/meditech/css/prism.css" rel="stylesheet" />

<style>
  .circle-icon {
    position: relative;
    display: flex;
    margin-bottom: 1em;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 0;
    padding: 50% 0;
    border-radius: 50%;
  }
</style>

<section class="container__centered">

  <h1 class="page__title">
    <?php print $title; ?>
  </h1>

  <div class="container__two-thirds">

    <h2>Font Awesome Icons</h2>
    <p>We use <a href="https://fontawesome.com/" target="_blank">Font Awesome 5</a> for many of the icons seen around the site. Below are two basic HTML examples showing a doctor avatar and a checkbox.</p>

    <div class="demo-ct">
      <i class="fas fa-user-md fa-2x"></i>&nbsp; &nbsp; &nbsp;<i class="far fa-check-square fa-2x"></i>
    </div>

    <!-- Start Font Awesome HTML Code -->
    <pre><code class="language-html">&lt;i class="fas fa-user-md">&lt;/i>
&lt;i class="far fa-check-square">&lt;/i>
</code></pre>
    <!-- End Font Awesome HTML Code -->

    <h3>CSS Usage</h3>
    <p>In an ongoing effort to keep our files as light-weight as possible, we only add the icons we use to the CSS. Once you have found the icon you want to use, add it to the CSS under the "Typography &amp; Icons" section. Below is an example of how the icon code should be written within the CSS.</p>

    <!-- Start Font Awesome CSS Code -->
    <pre style="margin-bottom:2em; border-radius: 6px;"><code class="language-CSS">.fa-user-md:before {
  content: "\f0f0";
}
</code></pre>
    <!-- End Font Awesome CSS Code -->

    <h3>Styling</h3>
    <p>Since these icons are technically a font, they can be styled as such in different colors and sizes. Below are 3 examples with different text color classes applied as well as font-size classes created by Font Awesome ("fa-2x" = "font-size: 2em").</p>
    <p class="italic"><strong>Note:</strong> For more examples and instructions please visit the <a href="https://fontawesome.com/how-to-use/on-the-web/referencing-icons/basic-use" target="_blank">Font Awesome Basic Usage</a> documentation.</p>

    <div class="demo-ct">
      <i class="fas fa-user-md"></i>&nbsp; &nbsp; &nbsp;<i class="fas fa-user-md fa-2x text--meditech-green"></i>&nbsp; &nbsp; &nbsp;<i class="fas fa-user-md fa-3x text--emerald"></i>
    </div>

    <!-- Start Font Awesome Color & Size Code -->
    <pre><code class="language-html">&lt;i class="fas fa-user-md">&lt;/i>
&lt;i class="fas fa-user-md fa-2x text--meditech-green">&lt;/i>
&lt;i class="fas fa-user-md fa-3x text--emerald">&lt;/i>
</code></pre>
    <!-- End Font Awesome Color & Size Code -->
    
    <h2>Custom Icons</h2>
    <p>Since Font Awesome icons can be somewhat limited, we encourage designers to get creative with custom icons. Typically we try to match the Font Awesome look with line icons, which could be something like the following:</p>
    <div class="container" style="padding: 1.5em 0;">
      <div class="container__one-fourth">
        <div class="circle-icon bg--emerald">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon-sharing-data.png" alt="image" style="width:50%;">
        </div>
      </div>
      <div class="container__one-fourth">
        <div class="circle-icon bg--emerald">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon-medical-history.png" alt="image" style="width:50%;">
        </div>
      </div>
      <div class="container__one-fourth">
        <div class="circle-icon bg--emerald">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon-data-retrieval.png" alt="image" style="width:50%;">
        </div>
      </div>
      <div class="container__one-fourth">
        <div class="circle-icon bg--emerald">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon-data-exchange.png" alt="image" style="width:50%;">
        </div>
      </div>
    </div>

  </div>

  <!-- SIDEBAR -->
  <aside class="container__one-third">

    <div class="sidebar__nav panel">
      <?php
        $styleBlock = module_invoke('menu', 'block_view', 'menu-style-guide-side-nav');
        print render($styleBlock['content']); 
        ?>
    </div>

  </aside>
  <!-- END SIDEBAR -->

</section>

<!-- END style-guide--node-2856.php Icons -->
<?php } ?>