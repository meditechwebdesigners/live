<!-- start events-simple-page--node-4213.php template -- COVID-19 Webinars PAGE -->

<section class="container__centered">
 
  <div class="container__two-thirds">
    <div class="container no-pad">

      <div class="container no-pad js__seo-tool__body-content">
        <h1 class="page__title js__seo-tool__title"><?php print $title; ?></h1>
        <?php print render($content['field_body']); ?>
        
        <h3 style="margin:2em 0;">Upcoming Educational Webinars</h3>
        <?php print views_embed_view('covid_19_webinars_upcoming', 'block'); // adds 'COVID-19 Webinars Upcoming' Views block... ?>
        
        <h3 style="margin:2em 0;">On Demand Educational Webinars</h3>
        <?php print views_embed_view('covid_19_webinars_on_demand', 'block'); // adds 'COVID-19 Webinars On Demand' Views block... ?>
        
      </div>
      
    </div>
  </div>
  
  <!-- SIDEBAR -->
  <aside class="container__one-third">
    <div class="panel">
      <div class="sidebar__nav news_sidebar_gae">
        <?php include('inc-covid19-sidebar.php'); ?>
      </div>
    </div>
  </aside>
  <!-- END SIDEBAR -->
              
</section>

<!-- end events-simple-page--node-4213.php template -->