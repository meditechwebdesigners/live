<?php // get current page URL info (minus domain)...
$currentURL = $_SERVER[REQUEST_URI];
?>
<!DOCTYPE html>
<html class="no-js" lang="en"> 

<?php $url = $GLOBALS['base_url']; // use this variable to replace https://ehr.meditech.com ?>
  
<!-- start html.tpl.php template -->
<head>
  
  <meta charset="utf-8">

  <title><?php print $head_title; ?></title>


  <?php
  // if node has the Hide Page box checked, add noindex meta... 
  if( isset($field_not_for_everyone) && $field_not_for_everyone == 1){
    print '<meta name="robots" content="noindex">';
  }
  ?>


  <?php
  // if node has Block Search Engine box checked, add noindex meta tag...
  if( isset($field_block_search_engines) && $field_block_search_engines == 1){
    $no_index = 'yes';
  }
  else if( isset($currentURL) && ($currentURL == '/new-or-updated-content' || $currentURL == '/new-or-updated-content/') ){ // if this page is displaying, add noindex meta tag...
    $no_index = 'yes';
  }
  else{
    $no_index = 'no';
  }

  // add noindex if hiding from search engines...
  if($no_index == 'yes'){
    print '<meta name="robots" content="noindex">';
    ?>
    <script type="text/javascript">
      var previousPage = document.referrer;
      var previousPageArray = previousPage.split('/');
      var referrerWebsite = previousPageArray[0] + '//' + previousPageArray[2];
      if( referrerWebsite == "https://www.google.com" || referrerWebsite == "https://www.bing.com" || referrerWebsite == "https://r.search.yahoo.com" || referrerWebsite == "https://duckduckgo.com" ){
        window.location.replace("<?php print $url; ?>/not-meant-for-your-eyes");
      }
    </script>
    <?php
  }
  
  // add meta keywords and description to News main page...
  if( $currentURL == '/news' || $currentURL == '/news/' ){
    print '<meta name="keywords" content="MEDITECH News">
    <meta name="description" content="MEDITECH News articles">';
  }
  ?>
  
  <!-- add charset, favicon (turn off in theme to override), generator, rss, etc. -->
  <?php print $head; ?>

  <!-- IE Fix ================================================== -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <!-- Mobile Specific Metas ================================================== -->
  <meta name="HandheldFriendly" content="True" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta name="format-detection" content="telephone=no"> <!-- don't hyperlink telephone numbers on device -->

  <!-- Favicons ================================================== -->
  <link rel="icon" type="image/vnd.microsoft.icon" href="<?php print $url; ?>/sites/all/themes/meditech/images/favicon.ico" />
  <link rel="shortcut icon" href="<?php print $url; ?>/sites/all/themes/meditech/images/favicon.ico">  
  <!-- generics -->
  <link rel="icon" href="<?php print $url; ?>/sites/all/themes/meditech/images/m-icon-32x32.png" sizes="32x32">
  <link rel="icon" href="<?php print $url; ?>/sites/all/themes/meditech/images/m-icon-57x57.png" sizes="57x57">
  <link rel="icon" href="<?php print $url; ?>/sites/all/themes/meditech/images/m-icon-76x76.png" sizes="76x76">
  <link rel="icon" href="<?php print $url; ?>/sites/all/themes/meditech/images/m-icon-96x96.png" sizes="96x96">
  <link rel="icon" href="<?php print $url; ?>/sites/all/themes/meditech/images/m-icon-128x128.png" sizes="128x128">
  <!-- Android -->
  <link rel="shortcut icon" href="<?php print $url; ?>/sites/all/themes/meditech/images/m-icon-196x196.png" sizes="196x196">
  <!-- iOS -->
  <link rel="apple-touch-icon" href="<?php print $url; ?>/sites/all/themes/meditech/images/m-icon-72x72.png" sizes="72x72">
  <link rel="apple-touch-icon" href="<?php print $url; ?>/sites/all/themes/meditech/images/m-icon-114x114.png" sizes="114x114">
  <link rel="apple-touch-icon" href="<?php print $url; ?>/sites/all/themes/meditech/images/m-icon-120x120.png" sizes="120x120">
  <link rel="apple-touch-icon" href="<?php print $url; ?>/sites/all/themes/meditech/images/m-icon-152x152.png" sizes="152x152">
  <link rel="apple-touch-icon" href="<?php print $url; ?>/sites/all/themes/meditech/images/m-icon-167x167.png" sizes="167x167">
  <link rel="apple-touch-icon" href="<?php print $url; ?>/sites/all/themes/meditech/images/m-icon-180x180.png" sizes="180x180">
  <!-- Windows 8 IE 10 -->
  <meta name="msapplication-TileColor" content="#FFFFFF">
  <meta name="msapplication-TileImage" content="<?php print $url; ?>/sites/all/themes/meditech/images/m-icon-144x144.png">
  <!-- Windows 8.1 + IE11 and above -->
  <meta name="msapplication-config" content="<?php print $url; ?>/sites/all/themes/meditech/browserconfig.xml">

  <!-- adds system stylesheets, module stylesheets, Google Fonts, theme stylesheet -->
  
  <!--   __  __   ______   _____    _____   _______   ______    _____   _    _ 
        |  \/  | |  ____| |  __ \  |_   _| |__   __| |  ____|  / ____| | |  | |
        | \  / | | |__    | |  | |   | |      | |    | |__    | |      | |__| |
        | |\/| | |  __|   | |  | |   | |      | |    |  __|   | |      |  __  |
        | |  | | | |____  | |__| |  _| |_     | |    | |____  | |____  | |  | |
        |_|  |_| |______| |_____/  |_____|    |_|    |______|  \_____| |_|  |_|
  -->
   
  <!-- Fonts --> 
  <link rel="stylesheet" href="https://use.typekit.net/duk4kto.css">


  <?php print $styles; ?>

  <!-- adds CMS's jquery, js scripts, theme scripts -->
  <?php print $scripts; ?>
  
  <script type="text/javascript" src="<?php print $url; ?>/sites/all/themes/meditech/js/jquery-1.11.1.min.js"></script>
  
  <script src="<?php print $url; ?>/sites/all/themes/meditech/js/stickynav.js"></script>
  <script src="<?php print $url; ?>/sites/all/themes/meditech/js/slidebars.js"></script>
  <script src="<?php print $url; ?>/sites/all/themes/meditech/js/main.js"></script>
  
  <?php if( $url == 'https://ehr.meditech.com' && user_is_logged_in() === false ){  // this is the LIVE site AND user is NOT logged in ?>
   
  <!-- Cookie Consent -->
  <script src="https://cdn.meditech.com/assets/external/scripts/include-cookieconsent-gaoff.js"></script>
  
  <!-- Google Analytics - Global site tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-22228657-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-22228657-1', { 'anonymize_ip': true });
  </script>
  
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-M4HQ8J4');</script>
  <!-- End Google Tag Manager -->
 
  <!-- Vimeo tag script -->
  <script type="text/javascript" defer="defer" src="https://extend.vimeocdn.com/ga/16406563.js"></script>
  
  <?php } ?> 
  
  <link type="text/css" rel="stylesheet" href="<?php print $url; ?>/sites/all/themes/meditech/css/main.css" media="all" />
  <?php if( user_is_logged_in() ){ ?>
    <link type="text/css" rel="stylesheet" href="<?php print $url; ?>/sites/all/themes/meditech/css/admin.css" media="all" />
  <?php } ?>
    
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>

  <?php if( $url == 'https://ehr.meditech.com' && user_is_logged_in() === false ){  // this is the LIVE site AND user is NOT logged in ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M4HQ8J4"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
  <?php } ?>
  
  <!-- accessiblity link -->
  <a href="#main-content" class="element-invisible element-focusable accessiblity_gae"><?php print t('Skip to main content'); ?></a>


  <?php print $page_top; // initial markup for modules that affect page ?>

    <?php print $page; // page template starts.. ?>

  <?php print $page_bottom; // closing markup for modules that affect page ?>


<?php if( $url == 'https://ehr.meditech.com' && user_is_logged_in() === false ){  // this is the LIVE site AND user is NOT logged in ?>
  <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/2897117.js"></script>
<?php } ?>

<div id="outdated"></div>
<link type="text/css" rel="stylesheet" href="<?php print $url; ?>/sites/all/themes/meditech/js/outdatedbrowser.min.css">
<script type="text/javascript" src="<?php print $url; ?>/sites/all/themes/meditech/js/outdatedbrowser.min.js"></script>
<script type="text/javascript" src="<?php print $url; ?>/sites/all/themes/meditech/js/outdatedbrowserinclude.js"></script>

<style>
  .js__seo-tool tbody:last-child tr:last-child>td:first-child { width: auto; }
</style>

<!-- end html.tpl.php template -->
</body>
</html>