<!-- START campaign--node-3548.php -->
<?php // This template is set up to control the display of the NEW campaign patient engagement content type 2021

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');
$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<script src="<?php print $url; ?>/sites/all/themes/meditech/js/splide.min.js"></script>

<style>
    .share-icons img {
        max-width: 3.5%
    }

    .shadow-box.w-headshot-outline {
        padding: 4.5em 2em 2em 2em;
        position: relative;
        margin-top: 60px;
        box-shadow: none;
        border: 1px solid #c8ced9;
        background-color: transparent;
    }

    .fa-podcast-link:after {
        content: "\f2ce" !important;
        font-style: normal;
    }

    /*	Splide Carousel Styles*/
    #quote-slider .card {
        box-shadow: none;
        border: 1px solid #c8ced9;
        align-self: flex-start;
    }

    #quote-slider .card a {
        color: #00BC6F;
    }


    #quote-slider .card__info {
        padding: 2em;
    }

    #quote-slider .splide__arrow--prev {
        left: -1em;
    }

    #quote-slider .splide__slide {
        transition: transform 1s;
        transform: scale(1);
    }

    #quote-slider .splide__pagination {
        text-align: center;
        padding-top: 2em;
    }

    .headshot-container {
        display: flex;
        align-items: center;
        margin-top: 1.5em;
    }

    .headshot {
        max-width: 100px;
        max-height: 100px;
        flex-grow: 1;
        margin-right: 1.5em;
    }

    .headshot img {
        border-radius: 50%;
    }

    .quote-name {
        flex-grow: 2;
    }

    .quote-name p {
        margin-bottom: 0;
    }

    .splide__slide {
        transition: transform 1s;
        transform: scale(.9);
    }

    .splide__slide:focus-visible {
        border: 1px solid #3e4545;
    }

    .splide__slide a:focus-visible {
        font-weight: bold;
    }

    .splide__slide.is-active {
        transform: scale(1);
        transition: transform .5s .4s;
    }

    .splide__arrow:hover {
        background: #dde0e7;
        transition: background .25s ease-in-out;
    }

    .splide__arrow:focus-visible {
        outline: 3px solid black;
    }

    #nav-slider {
        margin: 0 2em;
    }

    #nav-slider .splide__track {
        padding-top: 1em;
    }

    #nav-slider .splide__slide {
        transform: scale(1);
        cursor: pointer;
        color: #087E68;
        border: 1px solid #c8ced9;
        border-radius: 7px;
        padding: .5em 1em;
        font-size: 15px;
        font-weight: normal;
        transition: all .2s ease;
        background-color: #fff;
    }

    #nav-slider .splide__slide:hover {
        color: #3e4545;
        border: 1px solid #3e4545;
    }

    #nav-slider .is-active,
    #nav-slider .splide__slide:active,
    #nav-slider .splide__slide:focus {
        color: #3e4545;
        border: 1px solid #3e4545;
        transition: all .2s ease;
    }

    .content__callout,
    .content__callout__content {
        background-color: transparent;
        color: #fff;
    }

    /* Colored bars block 9*/
    .dark-purple {
        height: 20px;
        width: 100%;
        background-color: #51429c;
    }

    .dark-blue {
        height: 20px;
        width: 100%;
        background-color: #4f50b2;
    }

    .cornflower-blue {
        height: 20px;
        width: 100%;
        background-color: #4667ca;
    }

    .royal-blue {
        height: 20px;
        width: 100%;
        background-color: #4491e3;
    }

    .blue-green {
        height: 20px;
        width: 100%;
        background-color: #10bab3;
    }

    .green {
        height: 20px;
        width: 100%;
        background-color: #25b570;
    }

    .quote-person {
        margin-left: 50%;
        padding-top: 1em;
    }

    .quote-text {
        width: 100%;
        padding: 1em 3em .5em 3em;
        font-style: italic;
        font-size: 1.4em;
        color: white;
        text-align: left;
    }

    .circle-icon {
        height: 75px;
        width: 75px;
        color: #fff;
        display: inline-flex;
        justify-content: center;
        align-items: center;
        border-radius: 50%;
        margin: 0 auto;
        margin-right: 1em;
    }

    .fa-arrow-alt-circle-right:before {
        content: "\f35a";
        color: rgb(5, 118, 169);
    }

    .fa-ul {
        list-style-type: none;
        margin-left: 2.5em;
        padding-left: 0;
    }

    .slick-arrow {
        color: rgba(0, 0, 0, 1);
    }

    .fw-pad2 {
        padding: 3em 5em;
        max-width: 1800px;
        margin: 0 auto;
    }


    @media all and (max-width: 50em) {

        .shadow-box.w-headshot,
        .shadow-box.w-headshot-outline {
            padding: 6em 2em 2em 2em;
            margin-top: 5em;
        }

        .content__callout__content {
            padding-left: 6%;
            padding-top: 0em;
        }

        .content__callout__content p a {
            color: #ffffff;
            border-bottom: 1px solid #e6e9ee;
        }

        .content__callout__content p a:hover {
            color: #e6e9ee;
            border-bottom: 1px solid #e6e9ee;
        }

        .video {
            max-width: 100%;
        }
    }

    .video--shadow {
        border-radius: 6px;
        box-shadow: 13px 13px 40px rgba(0, 0, 0, 0.3);
    }

    @media all and (max-width: 1100px) {
        .mobile-padding {
            padding: 0em 2em;
        }
    }

</style>


<div class="js__seo-tool__body-content">

    <!-- START Block 1 -->
    <div class="bg--purple-gradient" style="padding: 2em 0;">
        <div class="content__callout">
            <div class="content__callout__media">
                <div class="content__callout__image-wrapper">
                    <div class="video js__video" data-video-id="646091802">
                        <figure class="video__overlay">
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay-group-of-people-sitting-watching-sunset.jpg" alt="People watching a sunset on a dock - video covershot">
                        </figure>
                        <a class="video__play-btn" href="https://vimeo.com/646091802"></a>
                        <div class="video__container"></div>
                    </div>
                </div>
            </div>
            <div class="content__callout__content">
                <div class="content__callout__body">
                    <h2>Healthcare consumers want choice. Your patients want care.</h2>
                    <p>Consumers want to access care when and where it’s convenient for them, using technology that they already know and like. Patients want attentive care from clinicians who are in the know. Deliver both with MEDITECH Expanse Patient Engagement solutions.</p>
                    <p><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/patientexperienceflyer.pdf">Improve outcomes, enhance customer loyalty, and increase revenues</a> with tools that keep your healthcare organization connected to consumers and patients alike.
                    </p>
                    <div class="center" style="margin-top:2em;">
                        <?php hubspot_button($cta_code, "Listen To The Patient Portal Podcast"); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Block 1 -->


    <div class="container bg--light-gray add-mq">
        <div class="container__centered">

            <div class="container no-pad--top center">
                <h2>Engage your patients in their healthcare journey</h2>

                <p class="auto-margins">By <a href="https://home.meditech.com/en/d/mktcontent/otherfiles/patientengagmentfunctionalitychecklist.pdf">offering your patients tools</a> that fit their lifestyles, you lay the foundation for long-lasting partnerships and a healthier community of patients who are active participants in their healthcare. </p>
                <p class="text--large">According to a <a href="https://www.pressganey.com/about-us/news/consumer-report">Press Ganey survey</a> of over 1,000 people:</p>
                <div class="container__one-third">
                    <p class="text--large"><span class="header-xl" style="color:#4040a3;">63<sup>%</sup></span><br>prefer to book an appointment digitally.</p>
                </div>
                <div class="container__one-third">
                    <p class="text--large"><span class="header-xl" style="color:#4040a3;">59<sup>%</sup></span><br>consider good pre-and post-appointment communication as key to their loyalty.</p>
                </div>
                <div class="container__one-third">
                    <p class="text--large"><span class="header-xl" style="color:#4040a3;">41<sup>%</sup></span><br>prefer convenient billing/payment options.</p>
                </div>
            </div>


            <!-- Hidden Modal -->
            <div id="modal20" class="modal">
                <a class="close-modal" href="javascript:void(0)">&times;</a>
                <div class="modal-content">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Home-Screen_MHealth_Portal--PE.jpg" alt="MHealth Health Portal and tracker screenshot">
                </div>
            </div>
            <!-- End of Hidden Modal -->
            <div class="container no-pad">
                <div class="container__one-fourth center" style="margin-right:3em; padding: 0 1em 1em 1em;">
                    <div class="open-modal" data-target="modal20">
                        <div class="phone--black">
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Home-Screen_MHealth_Portal--PE.jpg" alt="MHealth portal screenshot">
                        </div>

                        <div class="mag-bg">
                            <i class="mag-icon fas fa-search-plus"></i>
                        </div>
                    </div>
                </div>



                <div class="container__two-thirds">
                    <p>MEDITECH's MHealth app and Patient and Consumer Health Portal — also available in French and Spanish — make it easy for your patients to stay engaged, empowered, and brand loyal.</p>
                    <p>
                        <strong>Request virtual visits:</strong> Ease of scheduling at a place and time that's best for everyone (self-scheduling)
                    </p>
                    <p>
                        <strong>Medications:</strong> Self-service features to request prescription refills without spending time on the phone
                    </p>
                    <p>
                        <strong>Billing:</strong> Online bill pay to more easily manage financial responsibility for patients and their families
                    </p>
                    <p>
                        <strong>Health Tracker:</strong> Better manage patients with chronic conditions by enabling them to upload data from medical devices and personal fitness trackers
                    </p>

                </div>
            </div>
        </div>
    </div>


    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/bg--blue-layered-shapes-4.svg);">
        <div class="container__centered">
            <div class="container no-pad--top">
                <div class="container__one-fourth center"><a href="<?php print $url; ?>/podcast-index"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/podcasts/MEDITECH-Podcast--Apple--Resized-Web.png" alt="MEDITECH Podcast logo" width="160px" height="160px" style="width:60%; height: auto;"></a></div>
                <div class="container__three-fourths">
                    <h2 style="margin-top:1em;">Hear how our customers are improving outcomes</h2>
                    <div class="share-icons no-target-icon">
                        <p style="float:left; margin-right:0.5em;">Subscribe to the podcast:</p>
                        <a href="https://podcasts.apple.com/us/podcast/meditech-podcast/id1566866668" style="border-bottom: none;" target="_blank" rel="noreferrer noopener noreferrer noopener"><img src="https://ehr.meditech.com/sites/all/themes/meditech/images/podcasts/apple-podcast--icon.png" alt="Apple Podcast Logo"></a>
                        <a href="https://www.google.com/podcasts?feed=aHR0cHM6Ly9mZWVkcy5zaW1wbGVjYXN0LmNvbS9RTFYxZE93Ng%3D%3D" style="border-bottom: none;" target="_blank" rel="noreferrer noopener noreferrer noopener"><img src="https://ehr.meditech.com/sites/all/themes/meditech/images/podcasts/google-podcast--icon.png" alt="Google Podcast Logo"></a>
                        <a href="https://open.spotify.com/show/7q7wbf2Q6J1y5IGItYrBxR" style="border-bottom: none;" target="_blank" rel="noreferrer noopener noreferrer noopener"><img src="https://ehr.meditech.com/sites/all/themes/meditech/images/podcasts/spotify--icon.png" alt="Spotify Logo"></a>
                        <a href="https://meditech-podcast.simplecast.com/" style="border-bottom: none;" target="_blank" rel="noreferrer noopener noreferrer noopener"><img src="https://ehr.meditech.com/sites/all/themes/meditech/images/podcasts/simplecast--icon.png" alt="Simplecast Logo"></a>
                    </div>
                </div>
            </div>


            <div class="container no-pad">
                <div class="container__one-third shadow-box center">
                    <p class="no-margin">Citizens Memorial Hospital <a class="fa-podcast-link" href="https://meditech-podcast.simplecast.com/episodes/leveraging-virtual-care-through-covid-19-and-beyond">removes barriers to help drive portal engagement and virtual visits</a>, especially with harder-to-reach populations, such as long-term care and mental health patients.</p>
                </div>

                <div class="container__one-third shadow-box center">
                    <p class="no-margin">Ontario Shores Centre for Mental Health Sciences <a class="fa-podcast-link" href="https://meditech-podcast.simplecast.com/episodes/supporting-mental-health-in-ontario-canada">supports the future of mental health</a>, engages more patients, and <a class="fa-podcast-link" href="https://meditech-podcast.simplecast.com/episodes/emerging-mental-health-apps">improves outcomes</a> with the Patient and Consumer Health Portal.</p>
                </div>

                <div class="container__one-third shadow-box center">
                    <p class="no-margin">St. Luke’s Hospital <a class="fa-podcast-link" href="https://meditech-podcast.simplecast.com/episodes/stlukes-doubled-portal-enrollement-during-pandemic">doubled patient portal enrollment</a> by enlisting the help of their registration, marketing, and IT staff to bring the resource to the forefront.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Block 2 -->


    <!-- Block 3 Health Records on iphone -->
    <div class="container bg--purple-gradient">
        <div class="container__centered">


            <div class="container__one-half">
                <h2>Empower your community with Health Records on iPhone<sup>®</sup></h2>
                <p>Construct a more holistic view of your patient’s health. Give them access to their medications, allergies, test results, and other important information by syncing the MEDITECH portal with <a href="<?php print $url; ?>/ehr-solutions/health-records-on-iphone">Health Records on iPhone</a>. Your patients can:
                </p>
                <ul class="fa-ul">
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>View their health data across multiple participating organizations in one aggregated record.</li>
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Combine their health record data with data captured on their own fitness trackers and remote monitoring devices.</li>
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Access medications, allergies, test results, procedures, conditions, immunizations, and vital signs.</li>
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Share certain data from the Health app with their providers who use iOS 15, filling gaps in their records and guiding safer treatment decisions.</li>
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Receive notifications when new records become available.</li>
                </ul>
            </div>

            <div class="container__one-half">
                <div class="bg-pattern--container hide--mobile">
                    <img style="margin-top:6em;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/person-viewing-health-records-on-iphone.jpg" alt="Person checking their health records on an iPhone">
                    <div class="bg-pattern--blue-squares bg-pattern--right"></div>
                </div>
            </div>

        </div>
    </div>
    <!-- End of Block 3 Health records on iphone -->


    <!-- Block 5 Customer Quote -->
    <div class="container">
        <div class="container__centered auto-margins">
            <div>
                <h2>Consistent care for a hybrid world</h2>
                <p>Keep providers and patients connected, no matter the circumstances. <a href="<?php print $url; ?>/ehr-solutions/virtual-care">MEDITECH's Virtual Care solution</a> helps your organization to maintain the continuity of care and convenient access that patients expect.</p>
            </div>

            <blockquote class="bq--green">
                <p class="italic text--large">"With <a href="https://home.meditech.com/en/d/mktcontent/otherfiles/virtualondemandcareinfographicweb.pdf">Expanse Virtual On Demand Care</a>, our patients are treated by our doctors — clinicians they trust — and are never outsourced to a contracted virtual care service. This strengthens our patient-provider relationships and it ensures that the information in our patient records is always complete and available."
                </p>
                <div class="headshot-container">
                    <div class="headshot">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/headshot--roger-lutz.jpg" style="max-width:90px; height:90px; outline: #e6e9ee solid 2px;" alt="Roger Lutz head shot">
                    </div>
                    <div class="quote-name">
                        <p><span class="bold text--meditech-green" style="line-height: 1.4;">Roger Lutz</span><br>
                            Chief Information Officer<br>
                            <a href="<?php print $url; ?>/news/butler-health-system-improves-access-relationships-with-expanse-virtual-on-demand-care">Butler Health System</a>
                        </p>
                    </div>
                </div>
            </blockquote>
        </div>
    </div>
    <!-- End of Block 5 Customer Quote -->


    <!-- Block 4 (New Expanse Patient Connect Block) -->
    <div class="content__callout bg--purple-gradient">
        <div class="content__callout__media">
            <div class="content__callout__image-wrapper">
                <div class="video js__video" data-video-id="544527596">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--Communicate-at-the-Speed-of-Life-with-Expanse-Patient-Connect.jpg" alt="Expanse Patient Connect - video covershot">
                    </figure>
                    <a class="video__play-btn" href="https://vimeo.com/544527596"></a>
                    <div class="video__container"></div>
                </div>
            </div>
        </div>
        <div class="content__callout__content">
            <div class="content__callout__body text--white">
                <h2>Communicate at the speed of life with Expanse Patient Connect</h2>
                <p>Meet patients where they are, using technology they’re already comfortable with. <a href="<?php print $url; ?>/ehr-solutions/expanse-patient-connect">Expanse Patient Connect</a> is a real-time, two-way communication solution that improves efficiency while strengthening the bonds between healthcare organizations and communities.</p>
            </div>
        </div>
    </div>
    <!-- End Block 4 (New Expanse Patient Connect Block) -->


    <!-- Block 6 -->
    <!-- START Quote Slider -->
    <div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/bg--blue-layered-shapes-3.svg);">
        <div class="container__centered">
            <h2 class="center">What our customers are saying</h2>
            <div id="quote-slider" class="splide">
                <div class="splide__track">
                    <div class="splide__list">

                        <li class="splide__slide quote card bg--white" tabindex="0">
                            <div class="card__info">
                                <p class="italic text--large">"By having our patients use MEDITECH patient portal questionnaires to enter COVID-19 screening and consent information, we're able to save our staff approximately 15 minutes per instance over traditional phone screening. And patients love the convenience of submitting on their own time. It's a win-win for everyone."</p>
                                <div class="headshot-container">
                                    <div class="headshot">
                                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/headshot--Kimberly-McCarty.jpg" style="max-width:90px; height:90px;" alt="Kimberly McCarty head shot">
                                    </div>
                                    <div class="quote-name">
                                        <p><span class="bold text--meditech-green" style="line-height: 1.4;">Kimberly McCarty</span><br>
                                            IT Applications Supervisor<br>
                                            Lincoln Surgical Hospital
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="splide__slide quote card bg--white" tabindex="0">
                            <div class="card__info">
                                <p class="italic text--large">“For us, it becomes really important to see the person that we're serving as the expert in their own experience and really help navigate them through their recovery and mental health. You are a human being first and we support you, provide treatment with you, and really empower you to be able to not only be engaged in the care that you're receiving, but also give you information about your mental health care and help you understand your own trajectory.”</p>
                                <div class="headshot-container">
                                    <div class="headshot">
                                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/headshot--Sanaz-Riahi.jpg" style="max-width:90px; height:90px;" alt="Sanaz Riahi head shot">
                                    </div>
                                    <div class="quote-name">
                                        <p><span class="bold text--meditech-green" style="line-height: 1.4;">Sanaz Riahi, RN, PhD</span><br>
                                            VP, Practice, Academics<br>
                                            Chief Nurse Executive<br>
                                            Ontario Shores Centre<br>
                                            for Mental Health Sciences
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="splide__slide quote card bg--white" tabindex="0">
                            <div class="card__info">
                                <p class="italic text--large">“Our patients really appreciate the online self-scheduling. In this fast-paced world, it's easy, it's quick to do. It gives <a href="https://meditech-podcast.simplecast.com/episodes/stlukes-doubled-portal-enrollement-during-pandemic">instant gratification</a> in a way that works for them.”
                                </p>
                                <div class="headshot-container">
                                    <div class="headshot">
                                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/headshot--Missy-Francisco-Carlson.jpg" style="max-width:90px; height:90px;" alt="Missy Francisco Carlson head shot">
                                    </div>
                                    <div class="quote-name">
                                        <p><span class="bold text--meditech-green" style="line-height: 1.4;">Missy Francisco Carlson</span><br>
                                            Patient Experience Program Manager<br>
                                            St. Luke’s Hospital</p>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="splide__slide quote card bg--white" tabindex="0">
                            <div class="card__info">
                                <p class="italic text--large">“While COVID-19 has forced us to change some of the ways we deliver care, it has also served as an opportunity for us to <a href="https://f.hubspotusercontent10.net/hubfs/2897117/Case_Studies/CustomerSuccess_ValVerde_Regional_RPM.pdf">test the waters on new technologies</a>, to the benefit of both our providers and our patients.”</p>
                                <div class="headshot-container">
                                    <div class="headshot">
                                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/headshot--Keith-Willey.jpg" style="max-width:90px; height:90px;" alt="Doug Kanis head shot">
                                    </div>
                                    <div class="quote-name">
                                        <p><span class="bold text--meditech-green" style="line-height: 1.4;">
                                                Keith Willey</span><br>
                                            Chief Information Officer<br>
                                            Val Verde Regional Medical Center</p>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="splide__slide quote card bg--white" tabindex="0">
                            <div class="card__info">
                                <p class="italic text--large">“With virtual urgent care, we stay connected to our community while patients have the convenience to meet with our providers whenever and wherever they happen to be.”
                                </p>
                                <div class="headshot-container">
                                    <div class="headshot">
                                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/headshot--roger-lutz.jpg" style="max-width:90px; height:90px;" alt="Roger Lutz head shot">
                                    </div>
                                    <div class="quote-name">
                                        <p><span class="bold text--meditech-green" style="line-height: 1.4;">Roger Lutz</span><br>
                                            Chief Information Officer<br>
                                            Butler Health System</p>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="splide__slide quote card bg--white" tabindex="0">
                            <div class="card__info">
                                <p class="italic text--large">“It is really exciting to think that now you can schedule your own well-person visit at your convenience, you can look at an estimate of what something will cost, you can pay a bill, remote patient monitoring, you can be engaged with your provider. We want that portal to make the patient experience with us the <a class="fa-podcast-link" href="https://meditech-podcast.simplecast.com/episodes/leveraging-virtual-care-through-covid-19-and-beyond">easiest and most valuable thing</a> they can do.”</p>
                                <div class="headshot-container">
                                    <div class="headshot">
                                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/headshot--Sherry-Montileone.jpg" style="max-width:90px; height:90px;" alt="Sherry Montileone head shot">
                                    </div>
                                    <div class="quote-name">
                                        <p><span class="bold text--meditech-green" style="line-height: 1.4;">Sherry Montileone</span><br>
                                            Chief Information Officer<br>
                                            Citizens Memorial Hospital
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var primarySlider = new Splide('#quote-slider', {
                type: "loop",
                perPage: 2,
                gap: "2em",
                padding: {
                    right: "2em",
                    left: "2em",
                },
                speed: "400",
                pagination: "true",
                keyboard: "true",
                updateOnMove: "true",
                breakpoints: {
                    900: {
                        perPage: 1,
                        gap: "2em",
                        padding: {
                            right: "2em",
                            left: "2em",
                        }
                    }
                }
            }).mount();
        });

    </script>

    <!-- END Quote slider -->
    <!-- End of Block 6 -->


    <!-- Block 7 -->
    <div class="container bg--purple-gradient">
        <div class="container__centered">
            <div class="container__one-half">
                <h2>Enhance their experience with automated patient intake</h2>
                <p>Your patients don’t want to sit around in crowded waiting rooms. Enable them to wait anywhere by giving them the tools to <a href="https://ehr.meditech.com/news/patient-intake-made-easy-automated-paperless">preregister before arrival</a>. They can:
                </p>
                <ul class="fa-ul">
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Manage demographic and insurance updates.</li>
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Add to personal, social, and family history.</li>
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Update medications and allergies.</li>
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Respond to pre-visit questionnaires.</li>
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Scan a QR code for contactless <a href="https://home.meditech.com/en/d/mktcontent/otherfiles/selfservicepatientintake.pdf">self-check-in</a>.</li>
                </ul>
                <p>Your staff benefits, too. Increased patient involvement:</p>
                <ul class="fa-ul">
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Minimizes interactions for in-office appointments.</li>
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Reduces denials and helps your staff encounter fewer scanning and transcription errors.</li>
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Keeps appointments on schedule and reduces the frustration associated with delays.</li>
                </ul>
            </div>

            <div class="container__one-half">
                <div class="bg-pattern--container hide--mobile">
                    <img style="margin-top:6em;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/older-hispanic-man-checking-phone-in-car.jpg" alt="Older hispanic man checking in on phone in car before entering doctor's office">
                    <div class="bg-pattern--blue-squares bg-pattern--right"></div>
                </div>
            </div>

        </div>
    </div>
    <!-- End of Block 7 Self Check In Video -->


    <!-- Block 8 Timeline-->
    <div class="container bg--white">
        <div class="container__centered center">

            <h2>Healthier patients for a healthier bottom line</h2>

            <p class="auto-margins">
                Connecting with patients in a meaningful way has never been more important. As the industry shifts to value-based care, patients — as consumers — are looking for more control over how they spend their healthcare dollars.
            </p>

        </div>


        <div class="center" style="background-color: transparent; padding: 3em;">
            <div class="container__one-sixth">
                <div>
                    <img style="max-width: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/virtual-care--block9-PE.svg" alt="Holding phone">
                </div>
                <div class="dark-purple">
                    &nbsp;
                </div>
                <h4>Virtual Care</h4>
                <p><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/virtualcareflyer.pdf" targe=_blank>Fewer delays, faster results</a></p>
            </div>

            <div class="container__one-sixth">
                <div>
                    <img style="max-width: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Fewer-missed-apts--block9-PE.svg" alt="calendar">
                </div>
                <div class="dark-blue">
                    &nbsp;
                </div>
                <h4>Fewer Missed Appointments</h4>
                <p><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/expansepatientconnectinfographic.pdf">Convenient scheduling and integrated text-based reminders and confirmations</a></p>
            </div>

            <div class="container__one-sixth">
                <div>
                    <img style="max-width: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Improved-provider-eff--block9-PE.svg" alt="gas guage icon">
                </div>
                <div class="cornflower-blue">
                    &nbsp;
                </div>
                <h4>Improved Provider Efficiency</h4>
                <p><a href="https://f.hubspotusercontent10.net/hubfs/2897117/Case_Studies/CustomerSuccess_ValVerde_Regional_RPM.pdf">Know the entire patient story before starting the visit</a></p>
            </div>

            <div class="container__one-sixth">
                <div>
                    <img style="max-width: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/New-rev-streams--block9-PE.svg" alt="Light bulb with dollar sign">
                </div>
                <div class="royal-blue">
                    &nbsp;
                </div>
                <h4>New Revenue Streams</h4>
                <p><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/virtualondemandcareinfographicweb.pdf">Convenient options to recruit and retain patients</a></p>
            </div>

            <div class="container__one-sixth">
                <div>
                    <img style="max-width: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/online-bill-pay--block9-PE.svg" alt="computer">
                </div>
                <div class="blue-green">
                    &nbsp;
                </div>
                <h4>Online Bill Payments</h4>
                <p><a href="<?php print $url; ?>/ehr-solutions/meditechs-revenue-cycle">Get paid sooner</a></p>
            </div>

            <div class="container__one-sixth">
                <div>
                    <img style="max-width: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/better-outcomes--block9-PE.svg" alt="bar graph">
                </div>
                <div class="green">
                    &nbsp;
                </div>
                <h4>Better Outcomes</h4>
                <p><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/telehealthapproach.pdf" target=_blank>More involved patients have better outcomes, more positive opinions of their care and their providers</a></p>
            </div>

        </div>
    </div>
    <!-- End of Block 8 Timeline -->

</div>
<!-- end js__seo-tool__body-content -->


<!-- Block 9 -->
<!--style="padding:6em 0;"-->
<div class="container bg--purple-gradient">
    <div class="container__centered center auto-margins">

        <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
        <h2>
            <?php print $cta->field_header_1['und'][0]['value']; ?>
        </h2>
        <?php } ?>

        <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
        <div>
            <?php print $cta->field_long_text_1['und'][0]['value']; ?>
        </div>
        <?php } ?>

        <div class="center" style="margin-top:2em;">
            <?php hubspot_button($cta_code, "Watch the Advancing Patient Engagement Webinar"); ?>
        </div>

        <div style="margin-top:1em;">
            <?php print $share_link_buttons; ?>
        </div>

    </div>
</div>
<!-- End of Block 9 -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>
<!-- END campaign--node-2845.php -->
