<!-- inc-breadcrumbs.php -->
<style>
  ul.breadcrumbs { list-style: none; padding: 0; margin: 0; font-size: .8em; }
  ul.breadcrumbs li { margin: 0; padding: 0; display: inline; }
  ul.breadcrumbs li:after { content: '>'; }
  ul.breadcrumbs li:last-child:after { content: ''; }
  ul.breadcrumbs li a,
  ul.breadcrumbs li span { margin: 0 .5em; }
  ul.breadcrumbs li:first-child a { margin-left: 0; }
  @media (max-width: 50em){
    ul.breadcrumbs { display: none; }
  }
</style>
       
       <div class="container--page-title" style="padding:.5em 0;">
        <div class="container__centered">
        <?php
        // ====== BREADCRUMBS ===============================================
        print '<div class="container no-pad" style="margin:0;">';
        print '<ul class="breadcrumbs">';
        
        // $currentURLarray[0] = http: or https:
        // $currentURLarray[1] = ''
        // $currentURLarray[2] should always be the main domain (home)
        // $currentURLarray[3] maybe 'drupal' if on DRUDEV
        switch($currentURLarray[2]){
          case 'ehr.meditech.com':
            $URL_starter = $currentURLarray[0].'//'.$currentURLarray[2].'/';
            unset($currentURLarray[0]);
            unset($currentURLarray[1]);
            unset($currentURLarray[2]);
            break;
          case 'at_drudev.meditech.com':
            $URL_starter = $currentURLarray[0].'//'.$currentURLarray[2].'/drupal/';
            unset($currentURLarray[0]);
            unset($currentURLarray[1]);
            unset($currentURLarray[2]);
            unset($currentURLarray[3]);
            break;
        }
        // reset array index to zero using new array...
        $remainingURLarray = array_values($currentURLarray);
          
        
        // check URL for Hubspot code and remove it...
        if( isset($remainingURLarray[0]) ){
          
          if( isset($remainingURLarray[1]) ){
            
            if( strpos($remainingURLarray[1], '?') !== true ){
              $separate_out_hubspot_code = explode('?', $remainingURLarray[1]);
              $remainingURLarray[1] = $separate_out_hubspot_code[0];
              $separate_out_hubspot_code_again = explode('?', $currentURL);
              $currentURL = $separate_out_hubspot_code_again[0];
            }
            
          }
          else{

            if( strpos($remainingURLarray[0], '?') !== true ){
              $separate_out_hubspot_code = explode('?', $remainingURLarray[0]);
              $remainingURLarray[0] = $separate_out_hubspot_code[0];
              $separate_out_hubspot_code_again = explode('?', $currentURL);
              $currentURL = $separate_out_hubspot_code_again[0];
            }
            
          }
          
        }
          
        
        // create home link...
        if($currentURL == $URL_starter || $currentURL == $URL_starter.'/'){
          print '<li><span style="margin-left:0;">Home</span></li>';
        }
        else{
          print '<li><a href="'.$URL_starter.'">Home</a></li>';
        }
        
        // create 1st level link if not on home page...
        if( isset($remainingURLarray[0]) ){
          switch($remainingURLarray[0]){
            case 'ehr-solutions':
              if($currentURL == $URL_starter.$remainingURLarray[0] || $currentURL == $URL_starter.$remainingURLarray[0].'/'){
                print '<li><span>EHR Solutions</span></li>';
              }
              else{
                print '<li><a href="'.$URL_starter.$remainingURLarray[0].'">EHR Solutions</a></li>';
              }
              break;
            case 'expanse':
              if($currentURL == $URL_starter.$remainingURLarray[0] || $currentURL == $URL_starter.$remainingURLarray[0].'/'){
                print '<li><a href="'.$URL_starter.'ehr-solutions">EHR Solutions</a></li><li><span>MEDITECH Expanse</span></li>';
              }
              break;
            case 'news':
              if($currentURL == $URL_starter.$remainingURLarray[0] || $currentURL == $URL_starter.$remainingURLarray[0].'/'){
                print '<li><span>News</span></li>';
              }
              else{
                print '<li><a href="'.$URL_starter.$remainingURLarray[0].'">News</a></li>';
              }
              break;
            case 'news-tags':
              if($currentURL == $URL_starter.$remainingURLarray[0] || $currentURL == $URL_starter.$remainingURLarray[0].'/'){
                print '<li><span>News</span></li>';
              }
              else{
                print '<li><a href="'.$URL_starter.'news">News</a></li>';
              }
              break;
            case 'blog':
              if($currentURL == $URL_starter.$remainingURLarray[0] || $currentURL == $URL_starter.$remainingURLarray[0].'/'){
                print '<li><span>Blog</span></li>';
              }
              else{
                print '<li><a href="'.$URL_starter.'blog">Blog</a></li>';
              }
              break;
            case 'blog-tags':
              if($currentURL == $URL_starter.$remainingURLarray[0] || $currentURL == $URL_starter.$remainingURLarray[0].'/'){
                print '<li><span>Blog</span></li>';
              }
              else{
                print '<li><a href="'.$URL_starter.'blog">Blog</a></li>';
              }
              break;
            case 'author':
              if($currentURL == $URL_starter.$remainingURLarray[0] || $currentURL == $URL_starter.$remainingURLarray[0].'/'){
                print '<li><span>Blog</span></li>';
              }
              else{
                print '<li><a href="'.$URL_starter.'blog">Blog</a></li>';
              }
              break;
            case 'cta':
              if($currentURL == $URL_starter.$remainingURLarray[0] || $currentURL == $URL_starter.$remainingURLarray[0].'/'){
                print '<li><span>Events</span></li>';
              }
              else{
                print '<li><a href="'.$URL_starter.'events">Events</a></li>';
              }
              break;
            case 'events':
              if($currentURL == $URL_starter.$remainingURLarray[0] || $currentURL == $URL_starter.$remainingURLarray[0].'/'){
                print '<li><span>Events</span></li>';
              }
              else{
                print '<li><a href="'.$URL_starter.$remainingURLarray[0].'">Events</a></li>';
              }
              break;
            case 'about':
              if($currentURL == $URL_starter.$remainingURLarray[0] || $currentURL == $URL_starter.$remainingURLarray[0].'/'){
                print '<li><span>About</span></li>';
              }
              else{
                print '<li><a href="'.$URL_starter.$remainingURLarray[0].'/meditech">About</a></li>';
              }
              break;
            case 'contact':
              if($currentURL == $URL_starter.$remainingURLarray[0] || $currentURL == $URL_starter.$remainingURLarray[0].'/'){
                print '<li><span>Contact</span></li>';
              }
              else{
                print '<li><a href="'.$URL_starter.$remainingURLarray[0].'">Contact</a></li>';
              }
              break;
            case 'meditech-communications-team':
              if($currentURL == $URL_starter.$remainingURLarray[0] || $currentURL == $URL_starter.$remainingURLarray[0].'/'){
                print '<li><a href="'.$URL_starter.'contact">Contact</a></li><li><span>MEDITECH Communications Team</span></li>';
              }
              else{
                print '<li><a href="'.$URL_starter.$remainingURLarray[0].'">Contact</a></li>';
              }
              break;
            case 'careers':
              if($currentURL == $URL_starter.$remainingURLarray[0].'/careers' || $currentURL == $URL_starter.$remainingURLarray[0].'/careers/'){
                print '<li><a href="'.$URL_starter.'about/meditech">About</a></li>';
                print '<li><span>Careers</span></li>';
              }
              else{
                print '<li><a href="'.$URL_starter.'about/meditech">About</a></li>';
                print '<li><a href="'.$URL_starter.$remainingURLarray[0].'/careers-at-meditech">Careers</a></li>';
              }
              break;
            case 'global':
              if($currentURL == $URL_starter.$remainingURLarray[0] || $currentURL == $URL_starter.$remainingURLarray[0].'/'){
                print '<li><span>Global</span></li>';
              }
              else{
                print '<li><a href="'.$URL_starter.$remainingURLarray[0].'">Global</a></li>';
              }
              break;
            case 'mps':
              if($currentURL == $URL_starter.$remainingURLarray[0] || $currentURL == $URL_starter.$remainingURLarray[0].'/'){
                print '<li><span>MEDITECH Professional Services</span></li>';
              }
              else{
                print '<li><a href="'.$URL_starter.'meditech-professional-services">MEDITECH Professional Services</a></li>';
              }
              break;
            case 'search':
              $search_term = str_replace('%20', ' ', $remainingURLarray[2]);
              print '<li><span>Search Results for <strong>'.$search_term.'</strong></span></li>';
              break;
            case 'drupal-documentation':
              if($currentURL == $URL_starter.$remainingURLarray[0] || $currentURL == $URL_starter.$remainingURLarray[0].'/'){
                print '<li><span>Documentation</span></li>';
              }
              else{
                print '<li><a href="'.$URL_starter.$remainingURLarray[0].'">Documentation</a></li>';
              }
              break;
          }
        }
        
        // create 2nd level link if it exists...
        if( isset($remainingURLarray[1]) ){
          
          if( isset($node) ){
            
            if($node->type == 'hotel'){
              print '<li><a href="'.$URL_starter.$remainingURLarray[0].'/area-hotels">Area Hotels</a></li>';
            }
            
            // if South Africa pages, don't add 'meditech-south-africa' twice on main page's breadcrumbs...
            if($node->type == 'international_south_africa'){
              if($node->nid != 1926){
                print '<li><a href="'.$URL_starter.$remainingURLarray[0].'/meditech-south-africa">MEDITECH South Africa</a></li>';
              }
            }
            
            // if Asia Pacific pages, don't add 'meditech-asia-pacific' twice on main page's breadcrumbs...
            if($node->type == 'international_asia_pacific'){
              if($node->nid != 2093){
                print '<li><a href="'.$URL_starter.$remainingURLarray[0].'/meditech-asia-pacific">MEDITECH Asia Pacific</a></li>';
              }
            }
            
            // if UK/IRE pages, don't add 'meditech-uk-ire' twice on main page's breadcrumbs...
            if($node->type == 'international_uk_ire'){
              if($node->nid != 2384){
                print '<li><a href="'.$URL_starter.$remainingURLarray[0].'/meditech-uk-ireland">MEDITECH in the UK & Ireland</a></li>';
              }
            }

            // remove duplicate Careers link...
            if($node->nid != 2830){
              print '<li><span>'.$node->title.'</span></li>';
            }
            
          }
          else{
            // news taxonomy links...
            if($remainingURLarray[0] == 'news-tags'){
              if($remainingURLarray[1] != 'c-level'){
                $news_tag = $remainingURLarray[1];
                $news_tag = str_replace('-', ' ', $news_tag);
                print '<li><span>'.ucwords($news_tag).'</span></li>';
              }
              else{
                print '<li><span>'.ucwords($remainingURLarray[1]).'</span></li>';
              }
            }
          }
          
        }
          
        if(node_access('update',$node)){ 
          print '<li>'; print l( t('Edit Page'),'node/'.$node->nid.'/edit' ); print "</li>"; 
        }
        print '</ul>';       
        print '</div>';
        // ====== BREADCRUMBS ===============================================
        ?>
        </div>
      </div>
<!-- inc-breadcrumbs.php -->      