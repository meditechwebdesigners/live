<!-- start customer-materials-page-menu--node-2350.php template CLINICIAN LIAISON page -->
<?php
$url = $GLOBALS['base_url']; // grabs the site url

$content_sections = multi_field_collection_data_unrestricted($node, 'field_fc_head_ltext_1');
$buttons = multi_field_collection_data_unrestricted($node, 'field_fc_button_unl_1');
$side_menu = multi_field_collection_data_unrestricted($node, 'field_fc_text_link_unl_1');
?>

<style>
  .transparent-overlay--white { padding: 1em; background-color: rgba(256, 256, 256, 0.9); min-height: 5em }
</style>


<!-- Hero Block -->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/customer/physicians-in-a-circle-with-tablet.jpg);">
  <div class="container__centered">
   
    <div class="container__one-half transparent-overlay text--white">
      <h1><?php print $title; ?></h1>
      <?php if( !empty($content['field_sub_header_1']) ){ ?>
        <h2><?php print render($content['field_sub_header_1']); ?></h2>
      <?php } ?>
    
      <?php
      if( !empty($content_sections[0]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[0]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      print $content_sections[0]->field_long_text_1['und'][0]['value'];   
      ?>
    </div>
    <div class="container__one-half">
    </div>
    
  </div>
</div>
<!-- End of Hero Block -->
  
  
  
<!-- Mission Block -->
<div class="container" style="background-color:#d8d2c4;">
  <div class="container__centered">
   
    <div class="container__one-third center">
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/customer/physicians-illustration-graphic.png" alt="physicians graphic">
    </div>
    <div class="container__two-thirds">
      <?php
      if( !empty($content_sections[1]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[1]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      print $content_sections[1]->field_long_text_1['und'][0]['value'];   
      ?>
    </div>
    
  </div>
</div>
<!-- End of Mission Block -->  

<!-- video block -->
<div class="container" style="background-color:#fff;">
  <div class="container__centered center">
    <?php
    if( !empty($content_sections[2]->field_header_1['und'][0]['value']) ){
      print '<h2>';
      print $content_sections[2]->field_header_1['und'][0]['value']; 
      print '</h2>';
    } 
    ?>
    <div style="padding:1em 3em;">
      <div class="video js__video" data-video-id="245747206">
        <figure class="video__overlay">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/customer/physician-liaison-video-overlay.jpg" alt="Physician Liaison Video Covershot">
        </figure>
        <a class="video__play-btn" href="https://vimeo.com/245747206"></a>
        <div class="video__container"></div>
      </div>
    </div>
  </div>
</div> 
<!-- End of video block -->
  
   
        
         
<!-- quotes -->
<div class="container no-pad--top">
  <div class="container__centered">
   
    <div class="container__one-third center">
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/quotes-blue.png" alt="quotes graphic">
      <?php
      if( !empty($content_sections[3]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[3]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      print $content_sections[3]->field_long_text_1['und'][0]['value'];   
      ?>
    </div>
    
    <div class="container__one-third center">
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/quotes-orange.png" alt="quotes graphic">
      <?php
      if( !empty($content_sections[4]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[4]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      print $content_sections[4]->field_long_text_1['und'][0]['value'];   
      ?>
    </div>
    
    <div class="container__one-third center">
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/quotes-green.png" alt="quotes graphic">
      <?php
      if( !empty($content_sections[5]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[5]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      print $content_sections[5]->field_long_text_1['und'][0]['value'];   
      ?>
    </div>

    
  </div>
</div>
<!-- End of quotes -->




<!-- one meditech -->  
<div class="container" style="background-color:#e5ddcf;">
  <div class="container__centered">
   
    <div class="container__one-half">
      <?php
      if( !empty($content_sections[6]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[6]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }   
      print $content_sections[6]->field_long_text_1['und'][0]['value']; 
      ?>
    </div>
        
    <div class="container__one-half center">
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/customer/One-MEDITECH-Circle-logo.png" alt="One MEDITECH Circle logo.png" style="width:60%;" />
    </div>
         
  </div>
</div>
<!-- End one meditech -->




<!-- Join Our Team -->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/customer/physician-and-nurses-green.jpg); background-color:#268d2e;">
  <div class="container__centered text--white">
   
    <div class="center">
      <?php
      if( !empty($content_sections[7]->field_header_1['und'][0]['value']) ){
        print '<h2 style="margin-bottom:1.5em;">';
        print $content_sections[7]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      ?>
    </div>
   
    <div class="container__two-thirds">
      <?php print $content_sections[7]->field_long_text_1['und'][0]['value']; ?>
    </div>
    
    <div class="container__one-third">
      <?php
      print $content_sections[8]->field_long_text_1['und'][0]['value'];   
      ?>
    </div>
    
  </div>
</div>
<!-- End of Join Our Team -->





<!-- Physician Scripts -->
<div class="container" style="background-color:#fff;">
  <div class="container__centered" style="min-height:200px;">
   
    <div class="container no-pad">
      <?php
      if( !empty($content_sections[9]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[9]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }   
      ?>
    </div>
    
    <div>
     
      <div class="container__one-half" style="margin-bottom:2em;">
        <div class="video js__video" data-video-id="260462512">
          <figure class="video__overlay">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/customer/clinician-liaison-video-overlay--expanse-acute-script.jpg" alt="Expanse Acute Video Covershot">
          </figure>
          <a class="video__play-btn" href="http://vimeo.com/260462512"></a>
          <div class="video__container"></div>
        </div>
        <p class="center" style="margin-top:1em;"><a href="https://home.meditech.com/en/d/clinicianliaisonprogram/otherfiles/signininstructionsacuteuseretucker.pdf" target="_blank">Expanse Acute User - E. Tucker</a></p>
      </div>
      
      <div class="container__one-half" style="margin-bottom:2em;">
        <div class="video js__video" data-video-id="272583052">
          <figure class="video__overlay">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/customer/clinician-liaison-video-overlay--expanse-acute-talking-points.jpg" alt="Expanse Acute Video Covershot">
          </figure>
          <a class="video__play-btn" href="http://vimeo.com/272583052"></a>
          <div class="video__container"></div>
        </div>
        <p class="center" style="margin-top:1em;"><a href="https://home.meditech.com/en/d/clinicianliaisonprogram/otherfiles/expanseacutenarrativetalkingpointspostoppatient.pdf" target="_blank">Expanse Acute Narrative Talking Points - Post Op Patient</a></p>
      </div>
      
    </div>
      
    <div>

      <div class="container__one-half" style="margin-bottom:2em;">
        <div class="video js__video" data-video-id="260471093">
          <figure class="video__overlay">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/customer/clinician-liaison-video-overlay--expanse-ed-script.jpg" alt="Expanse ED Video Covershot">
          </figure>
          <a class="video__play-btn" href="http://vimeo.com/260471093"></a>
          <div class="video__container"></div>
        </div>
        <p class="center" style="margin-top:1em;"><a href="https://home.meditech.com/en/d/clinicianliaisonprogram/otherfiles/signininstructionswebeduserawells.pdf" target="_blank">Expanse ED User - A. Wells</a></p>
      </div>
      
      <div class="container__one-half" style="margin-bottom:2em;">
        <div class="video js__video" data-video-id="271696294">
          <figure class="video__overlay">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/customer/clinician-liaison-video-overlay--expanse-ed-talking-points.jpg" alt="Expanse ED Video Covershot">
          </figure>
          <a class="video__play-btn" href="http://vimeo.com/271696294"></a>
          <div class="video__container"></div>
        </div>
        <p class="center" style="margin-top:1em;"><a href="https://home.meditech.com/en/d/clinicianliaisonprogram/otherfiles/expanseednarrativetalkingpointsanklepatient.pdf" target="_blank">Expanse ED Narrative Talking Points - Ankle Patient</a></p>
      </div>
      
    </div>

    <div>
     
      <div class="container__one-half" style="margin-bottom:2em;">
        <div class="video js__video" data-video-id="260972201">
          <figure class="video__overlay">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/customer/clinician-liaison-video-overlay--expanse-ambulatory-script.jpg" alt="Expanse Ambulatory Video Covershot">
          </figure>
          <a class="video__play-btn" href="http://vimeo.com/260972201"></a>
          <div class="video__container"></div>
        </div>
        <p class="center" style="margin-top:1em;"><a href="https://home.meditech.com/en/d/clinicianliaisonprogram/otherfiles/signininstructionsambulatoryuserlburke.pdf" target="_blank">Expanse Ambulatory User - L. Burke</a></p>
      </div>
      
      <div class="container__one-half" style="margin-bottom:2em;">
        <div class="video js__video" data-video-id="272604287">
          <figure class="video__overlay">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/customer/clinician-liaison-video-overlay--expanse-ambulatory-talking-points.jpg" alt="Expanse Ambulatory Video Covershot">
          </figure>
          <a class="video__play-btn" href="http://vimeo.com/272604287"></a>
          <div class="video__container"></div>
        </div>
        <p class="center" style="margin-top:1em;"><a href="https://home.meditech.com/en/d/clinicianliaisonprogram/otherfiles/expanseambnarrativetalkingpointssorethroatwdiabetes.pdf" target="_blank">Expanse Ambulatory Narrative Talking Points - Sore Throat with Diabetes</a></p>
      </div>
      
    </div>
    
  </div>
</div>
<!-- end Physician Scripts -->                  

            
<!-- Additional training information -->  
<div class="container no-pad--top" style="background-color:#fff;">
  <div class="container__centered" style="min-height:200px;">
   
    <div class="container no-pad">
      <?php
      if( !empty($content_sections[10]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[10]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }   
      ?>
    </div>
     
    <div>         
      <div class="container__one-half" style="margin-bottom:2em;">
        <div class="video js__video" data-video-id="242635028">
          <figure class="video__overlay">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/customer/video-overlay--expanse-ed-acute.jpg" alt="Expanse ED and Acute Video Covershot">
          </figure>
          <a class="video__play-btn" href="http://vimeo.com/242635028"></a>
          <div class="video__container"></div>
        </div>
      </div>

      <div class="container__one-half" style="margin-bottom:2em;">
        <div class="video js__video" data-video-id="242634916">
          <figure class="video__overlay">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/customer/video-overlay--medical-reconciliation.jpg" alt="Medical Reconciliation Video Covershot">
          </figure>
          <a class="video__play-btn" href="http://vimeo.com/242634916"></a>
          <div class="video__container"></div>
        </div>
      </div>
    </div>

    <div>
      <div class="container__one-half" style="margin-bottom:2em;">
        <div class="video js__video" data-video-id="242635120">
          <figure class="video__overlay">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/customer/video-overlay--md-product-overview.jpg" alt="Web Products Video Covershot">
          </figure>
          <a class="video__play-btn" href="http://vimeo.com/242635120"></a>
          <div class="video__container"></div>
        </div>
      </div>
    </div>
    
  </div>
</div>
<!-- End Additional training information -->





<!-- additional resources -->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/customer/road-through-green-fields-partly-cloudy-day.jpg);">
  <div class="container__centered">

    <div class="container transparent-overlay--white">
     
      <?php
      if( !empty($content_sections[11]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[11]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      ?>

      <div class="container__one-half">
        <?php print $content_sections[11]->field_long_text_1['und'][0]['value']; ?>
      </div>

      <div class="container__one-half">
        <?php print $content_sections[12]->field_long_text_1['und'][0]['value']; ?>
      </div>
      
    </div>
    
  </div>
</div>
<!-- End of additional resources -->






<!-- Contact -->
<div class="container">
  <div class="container__centered center">

      <?php
      if( !empty($content_sections[13]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[13]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      ?>
      <p><strong>Contact:</strong></p>
      <p><a href="https://home.meditech.com/webforms/contact.asp?rcpt=clinnane|meditech|com&rname=clinnane">Christine Linnane, Clinical Liaison Coordinator</a></p>
      <p><a href="https://home.meditech.com/webforms/contact.asp?rcpt=scanavan|meditech|com&rname=scanavan">Stephanie Canavan, Supervisor, Clinical Liaison Program</a></p>
      <p><a href="https://home.meditech.com/webforms/contact.asp?rcpt=jkimball|meditech|com&rname=jkimball">Jeff Kimball, Director, Sales Operations and Enablement</a></p>
    
  </div>
</div>
<!-- End of Contact -->
     
    


<?php
/*
// button code...
if( isset($buttons) && !empty($buttons) ){
  $number_of_buttons = count($buttons);
  for($b=0; $b<$number_of_buttons; $b++){
    if( !empty($buttons[$b]->field_hubspot_embed_code_1['und'][0]['value']) ){
      $button_code = $buttons[$b]->field_hubspot_embed_code_1['und'][0]['value']; 
      print '<div class="center" style="margin-top:2em;">';
      hubspot_button($button_code, "button");
      print '</div>';
    }
    else{
      print '<div class="center"><a href="'.$buttons[$b]->field_button_url_1['und'][0]['value'].'" class="btn--orange button-vertical-adjustment">'.$buttons[$b]->field_button_text_1['und'][0]['value'].'</a></div>';
    }
  }
}
*/

/*
// side menu links code...
if( isset($side_menu) && !empty($side_menu[0]) ){
  print '<aside class="container__one-third panel">';
  print '<div class="sidebar__nav solutions_sidebar_gae">';
  print '<ul class="menu">';
  $number_of_menu_links = count($side_menu);
  for($ml=0; $ml<$number_of_menu_links; $ml++){
    print '<li><a href="';
    print $side_menu[$ml]->field_link_url_1['und'][0]['value']; 
    print '">';
    print $side_menu[$ml]->field_link_text_1['und'][0]['value']; 
    print '</a></li>';
  }
  print '</ul>';
  print '</div>';
  print '</aside>';
}
*/
?>
<!-- end customer-materials-page-menu--node-2350.php template -->