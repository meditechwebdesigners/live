<!-- START campaign--node-3404.php -->

<?php // This template is set up to control the display of the campaign: Virtual Assistant

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');
$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
  .shadow-box {
		background-color: transparent;
		margin-bottom: 0;
  }

  .headshot--border {
    max-width: 100px;
    max-height: 100px;
    border-radius: 50%;
    margin-right: 2em;
    float: left;
    margin-bottom: 1em;
  }

  .headshot--title {
    float: left;
  }

  .speach-bubble {
    display: inline-block;
    padding: 1em 0em 1em 1em;
    margin-bottom: 0.5em;
  }

  .speach-bubble p {
    font-weight: 600;
    margin-bottom: 0;
    margin-top: 10px;
    display: block;
    overflow: hidden;
  }

  .speach-bubble i {
    font-size: 26px;
  }

  .fa-microphone:before {
    content: "\f130";
  }

  .circle-icon {
    height: 50px;
    width: 50px;
    color: #fff;
    float: left;
    display: flex;
    border: 2px solid #00bc6f;
    justify-content: center;
    align-items: center;
    border-radius: 50%;
    margin-right: 16px;
  }

  .zen-doctor {
    padding: 3em 1em 0 0;
  }
  
  .video {
    padding-bottom: 57%;
    overflow: visible;
    margin: 0;
    box-shadow: none;
    max-width: 100%;
  }

  .content__callout__image-wrapper {
    padding: 0;
  }

  @media all and (max-width: 65em) {
    .hide-on-tablets {
      display: none;
    }
  }

  @media all and (max-width: 800px) {
    .zen-doctor {
      padding: 0 3em 2em 3em;
    }

    .tablet-img {
      margin-top: 3em;
      width: 375px;
    }
  }

</style>

<div class="js__seo-tool__body-content">

  <h1 style="display:none;" class="js__seo-tool__title">MEDITECH's Virtual Assistant</h1>


  <!-- START Updated Block 1 -->
  <div class="container no-pad">
    <div class="gl-container">
      <div class="container__one-half gl-text-pad background--cover text--white" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-gradient--bg.jpg);">
        <h2>"OK, MEDITECH." <span style="font-weight: 400;">Make my life easier.</span></h2>
        <p><span class="bold">Sometimes the quickest way to get what you want is to ask for it</span>. With Expanse Virtual Assistant, physicians can now ask more of their EHRs — and get it. Want to see your patient’s last blood pressure reading? Ask for it. Need to quickly review her allergies? Just say so.</p>
        <p>Expanse Virtual Assistant responds to simple verbal commands by retrieving the information you’re looking for — without you having to type, point, click, or even touch your device. Clinicians can experience new levels of efficiency and usability, while keeping their focus squarely on patients, not their computer systems.</p>
        <div style="margin-top:2em;">
          <?php hubspot_button($cta_code, "Register for MEDITECH's 2019 Physician and CIO Forum"); ?>
        </div>
      </div>


      <div class="container__one-half bg--light-gray gl-text-pad">

        <div class="content__callout__image-wrapper">
          <div class="video js__video" data-video-id="493341928">
            <figure class="video__overlay">
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Expanse-Virtual-Assistant--video-overlay.jpg" style="border-radius: 7px;" alt="Expanse Virtual Assistant video thumbnail">
            </figure>
            <a class="video__play-btn video_gae" href="https://vimeo.com/493341928"></a>
            <div class="video__container">
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
  <!-- END Updated Block 1 -->


  <!-- Block 2 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/sound-waves.svg); background-position: bottom; padding: 6em 0;">
    <div class="container__centered">
      <div class="container__one-half">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/hands-free-doctor-voicing-commands.jpg" class="zen-doctor" alt="Hands free doctor voicing commands">
      </div>
      <div class="container__one-half shadow-box">
        <h2>The next step in healthcare’s digital transformation</h2>
        <p>Expanse Virtual Assistant is built on the same advanced AI that powers your smartphone and other intelligent devices. So it works just like the personal devices you’re already accustomed to. The difference is that our virtual assistant comes with a medical degree — or at least medical training. That’s because MEDITECH and Nuance have developed a solution with healthcare terminology and skills baked in — so you’re not just heard, you’re understood. Transform the way you use your EHR with Expanse Virtual Assistant.</p>
      </div>
    </div>
  </div>
  <!-- END Block 2 -->

  <!-- Block 3 -->
  <div class="container no-pad">
    <div class="gl-container">
      <div class="container__one-half gl-text-pad background--cover no-pad text--white" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-gradient--bg.jpg);">
        <img class="headshot--border" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dr-adams-internist--headshot.jpg" alt="Dr. Adams head shot">

        <div class="headshot--title">
          <h2 class="no-margin--bottom">Meet Dr. Adams</h2>
          <p>Internist</p>
        </div>
        <div style="clear:both;"></div>
        <p>Dr. Adams is preparing for his work day and a full schedule of office visits. He’s getting ready for his first patient and has a few minutes while vitals are taken and PFSH is being collected, so he turns to his Expanse Virtual Assistant:</p>

        <div class="speach-bubble">
          <div class="circle-icon bg--emerald">
            <i class="fas fa-microphone"></i>
          </div>
          <p>“OK, MEDITECH. Show me Barbara Smith’s last HbA1c.”</p>
        </div>

        <p>The Virtual Assistant goes to work, pulling up his patient’s lab panel and automatically displaying her last glycated haemoglobin test. He notes a reading of 6.2 — slightly elevated.</p>

        <div class="speach-bubble">
          <div class="circle-icon bg--emerald">
            <i class="fas fa-microphone"></i>
          </div>
          <p>“OK, MEDITECH. Open vitals.”</p>
        </div>

        <p>Dr. Adams sees that the vital signs taken moments ago are already in the system. He observes that her blood pressure reading is above normal.</p>

        <div class="speach-bubble">
          <div class="circle-icon bg--emerald">
            <i class="fas fa-microphone"></i>
          </div>
          <p>“OK, MEDITECH. Show me medications.”</p>
        </div>

        <p>The Virtual Assistant opens Barbara’s med list and Dr. Adams notices that she has not renewed her prescription for lisinopril.</p>

        <p>Within just a couple of minutes, Dr. Adams has efficiently identified an important topic of discussion and formulated a plan for the visit simply by talking to his EHR, using MEDITECH’s Virtual Assistant.</p>
        </p>
      </div>


      <div class="container__one-half background--cover hide-on-tablets" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/internist-speaking-with-virtual-assistant.jpg); min-height:25em; background-position: top;">

      </div>

    </div>
  </div>
  <!-- END Block 3 -->


  <!-- Block 5 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/sound-waves--reversed.svg); background-position: bottom; padding: 6em 0;">
    <div class="container__centered">
      <div class="container__one-half shadow-box">
        <h2>Look, but don’t touch</h2>
        <p>Physicians know that devices like keyboards, tablets, and smartphones can be breeding grounds for germs; and viruses like COVID-19 can survive on surfaces for hours, or even days. Expanse Virtual Assistant minimizes the need for touching devices — lowering the risk of spreading disease and keeping physicians’ hands free for conducting exams or performing procedures.</p>
        <p>Our touchless technology isn’t just faster, it’s safer—and that’s good for both providers and their patients.</p>
      </div>
      <div class="container__one-half" style="text-align:center;">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/voice-recongnition-tablet.png" alt="Voicing commands to a tablet" class="tablet-img">
      </div>
    </div>
  </div>
  <!-- END Block 5 -->

  <!-- Block 6 -->
  <div class="container no-pad">
    <div class="gl-container">
      <div class="container__one-half background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/doctor-hands-free-dictation.jpg); min-height: 23em;">

      </div>
      <div class="container__one-half gl-text-pad background--cover text--white" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-gradient--bg.jpg);">
        <h2 class="header-one">Lay your burden down</h2>
        <p>We designed Expanse to be the most intuitive and efficient EHR ever produced, and we’re continually searching for new ways to lower physicians’ cognitive load. Expanse Virtual Assistant was created to do just that: Reduce your burdens by providing a simple and natural way to access the data you need.</p>
        <p>It’s the latest tool in MEDITECH’s advanced physician toolkit and it represents another leap forward in usability and efficiency. So lay your devices down and raise up your voice with Expanse Virtual Assistant. </p>
      </div>

    </div>
  </div>
  <!-- END Block 6 -->

  <!-- Block 7 -->
  <div class="container bg--white background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/sound-waves.svg); background-position: bottom; padding: 6em 2em;">
    <div class="container__centered center auto-margins">

      <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
      <h2>
        <?php print $cta->field_header_1['und'][0]['value']; ?>
      </h2>
      <?php } ?>

      <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
      <div>
        <?php print $cta->field_long_text_1['und'][0]['value']; ?>
      </div>
      <?php } ?>

      <div class="center" style="margin-top:2em;">
        <?php hubspot_button($cta_code, "Register for MEDITECH's 2019 Physician and CIO Forum"); ?>
      </div>

      <div style="margin-top:1em;">
        <?php print $share_link_buttons; ?>
      </div>

    </div>
  </div>
  <!-- END Block 7 -->

</div>


<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>

<!-- END campaign--node-3404.php -->
