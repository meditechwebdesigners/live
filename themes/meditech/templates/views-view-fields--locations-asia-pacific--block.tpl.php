<?php 
  /*
   * This template is for each row of the Views block: LOCATIONS - ASIA PACIFIC ....................... 
   */

// get node ID...
  $nid = $fields['nid']->content;
  $node = node_load($nid);

  $link = $fields['field_directions_url']->content;
?>
<!-- start views-view-fields--locations-asia-pacific--block.tpl.php template -->
<figure class="container no-pad">
  <div class="container__one-third">
    <?php
      // get crop orientation option to set proper class name for image...
      if($fields['field_image_1_crop_orientation']->content == 'Show Left Side of Image'){
        $crop = 'crop-left';
      }
      elseif($fields['field_image_1_crop_orientation']->content == 'Show Right Side of Image'){
        $crop = 'crop-right';
      }
      else{
        $crop = 'crop-center';
      }
    ?>
    <div class="square-img-cropper <?php print $crop; ?>">
      <?php print $fields['field_location_image']->content; ?>
    </div>
  </div>
  <figcaption class="container__two-thirds">
    <h3 class="no-margin--top header-four"><?php print $fields['title']->content; ?></h3>
    <p class="no-margin--bottom"><?php 
      $address = $fields['field_location_address']->content; 
      // if address line has a comma, break up the text onto 2 lines... 
      $address_array = explode(',', $address);
      print $address_array[0].'<br />'.$address_array[1];
    ?></p>
    
    <p><?php print $fields['field_meditech_facility']->content; // city
      // display state if there is one...
      $stateTerms = field_view_field('node', $node, 'field_meditech_location'); 
      if(!empty($stateTerms)){
        foreach($stateTerms["#items"] as $sTerm){
          $eventState = ', '.$sTerm["taxonomy_term"]->description;
        }
      }
      else{
        $eventState = '';
      }
      print $eventState; 
    
    print $fields['field_location_zip']->content; ?><br />
    <?php
    switch($fields['title']->content){
      case 'MEDITECH Australia':
        print 'Australia';
        break;
      case 'MEDITECH Singapore':
        print 'Singapore';
        break;
    }
    ?></p>
    
    <a class="directions_gae" href="<?php print $link ?>" target="_blank">Directions</a>
  </figcaption>
</figure>
<hr>
<!-- end views-view-fields--locations-asia-pacific--block.tpl.php template -->