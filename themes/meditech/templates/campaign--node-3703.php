<!-- START campaign--node-3703.php -- Work in Greenfield Workspace -->
<?php
$url = $GLOBALS['base_url']; // grabs the site url
  
$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
  .shadow-box--var {
    padding: 2em;
    color: #3e4545;
    background-color: #fff;
    border-radius: 7px;
    box-shadow: 0px 0px 20px 1px rgba(120, 120, 120, .2);
    max-width: 500px;
    position: absolute;
    right: 18px;
    top: 50%;
    transform: translateY(-50%);
  }

  .container--bg-img {
    max-width: 900px;
    border-radius: 7px;
  }

  .container--rel {
    position: relative;
  }

  @media all and (max-width: 950px) {
    .shadow-box--var {
      max-width: 100%;
      position: relative;
      margin: 0 auto;
      width: 90%;
      right: 0;
    }

    .container--bg-img {
      max-width: 100%;
      border-radius: 7px;
    }

    .container--rel {
      margin-bottom: -8em;
    }
  }

  @media all and (max-width: 500px) {
    .shadow-box--var {
      transform: translateY(0%);
      width: 100%;
      margin-top: 2.35765%;
    }

    .container--rel {
      margin-bottom: 0;
    }
  }

  .btn--orange {
    border: 4px solid #ff8300 !important;
  }

  .btn--outline {
    background-color: unset;
    color: #3E4545 !important;
    border-width: 4px !important;
  }

  .btn--orange:hover {
    -webkit-transition: all 600ms ease-in-out;
  }

  .icons img {
    width: 40%;
    height: 40%;
    margin-top: 1.5em;
  }

  div.expander {
    margin-bottom: 1em;
  }

  div.expander span:before {
    content: "+ ";
    font-weight: bold;
    font-size: 1.25em;
    position: relative;
    top: .05em;
  }

  div.expander span.expanded:before {
    content: "\2013 ";
    top: .02em;
    letter-spacing: .25em;
  }

  .collapsible {
    display: none;
    width: 100%;
    transition: display 0.5s ease-out;
    padding: 1em;
    margin-bottom: 1em;
    border-radius: 0.389em;
    border: solid 1px #bbb;
    box-shadow: 0px 0px 20px 1px rgb(120 120 120 / 20%);
  }

  .jump-links a {
    margin: 0 1em;
  }

  .squish-list li {
    margin-bottom: .5em;
  }

</style>

<div class="js__seo-tool__body-content">


  <!-- Block 1 -->
  <div class="container container__centered" style="padding-bottom:0;">
    <div class="container__one-third">
      <h1 style="font-size: 1.7em;"><?php print $title; ?></h1>
      <p>Greenfield lays the groundwork for visionaries and innovators to create a better future for providers, patients, and consumers, with technology that connects communities and enables better care. Developers are invited to explore our documentation area to better understand what is available. Access to MEDITECH's API sandbox also enables you to test your apps with APIs currently available, as well as those still being developed without requiring direct customer engagement.</p>
    </div>
    <div class="container__two-thirds center">
      <img class="container--bg-img" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/illustration--coding-laptop-and-phone.png" alt="" style="width:90%;">
      <div class="center">
        <a href="https://info.meditech.com/welcome-to-meditech-greenfield-rfi" class="btn--orange btn--outline">Register </a>&nbsp;&nbsp;&nbsp;<a href="https://greenfield.meditech.com/" class="btn--orange">Log in</a>
      </div>
    </div>
  </div>
  <!-- Block 1 -->


  <!-- START diagram -->
  <div id="register" class="container container__centered">
    <h2>How to register for Greenfield Workspace</h2>
    <p>As a developer, after submitting your Greenfield Workspace registration request, you will have access to our documentation resources. You will also have the option to develop and test your application as follows:</p>

    <div class="card__wrapper">
      <div class="container__one-half card bg--emerald">
        <div class="card__info" style="padding:1em 2em;">
          <h3 style="margin-bottom:1.5em;">Explore Greenfield Workspace documentation and web application</h3>
          <p>Features include:</p>
          <ul class="squish-list">
            <li>OAuth2 documentation</li>
            <li>FHIR DSTU2/R4 specs</li>
            <li>Sample patient data/examples</li>
            <li>Ability to try sample requests through interactive documentation</li>
            <li>Access to review JSON payload returned in browser</li>
          </ul>
          <hr />
          <h3>Next steps</h3>
          <ol>
            <li>A member of the Greenfield support team will email you a welcome packet including information on how to execute the Greenfield End-User License Agreement (EULA).</li>
            <li>MEDITECH requires your Google ID to obtain access to the Greenfield web application and documentation area.</li>
            <li>Your email address, if different, is also required to access Jira Service Desk.
              <div class="expander"><a href="#e">More information on Google IDs</a></div>
              <div class="collapsible">
                <p>Google ID serves two purposes:</p>
                <ol>
                  <li>Provides access to our interactive web application and documentation</li>
                  <li>Serves as our identity provider (IDP) for Greenfield Workspace. This ID is linked to the test patients in the back-end EHR and used during the OAuth flow (Note: MEDITECH customers typically use MEDITECH's Patient Portal as our IDP. Therefore they would log in using their Patient Portal credentials)</li>
                </ol>
              </div>
            </li>
          </ol>
        </div>
      </div>
      <div class="container__one-half card bg--dark-blue">
        <div class="card__info" style="padding:1em 2em;">
          <h3 style="margin-bottom:1.5em;">Develop and test your application</h3>
          <p>Features include:</p>
          <ul class="squish-list">
            <li>Client ID and secret provided for OAuth2 testing and Authorization Code Grant Type testing</li>
            <li>Ability to hit our DSTU2/R4 endpoints from your application or testing tool</li>
          </ul>
          <hr />
          <h3>Next steps</h3>
          <ol>
            <li>Provide the MEDITECH Greenfield support team with a <a href="https://www.OAuth.com/OAuth2-servers/redirect-uris/">redirect/callback URI</a> to generate an OAuth client ID and secret. We will schedule a brief call with you to provide this information.</li>
            <li>Once Greenfield Workspace access is granted, you can review additional documentation, test in Greenfield using your application or testing tool, and submit questions or report issues to our Service Desk.</li>
          </ol>
        </div>
      </div>
    </div>

    <div class="center">
      <a href="https://info.meditech.com/welcome-to-meditech-greenfield-rfi" class="btn--orange">Register for Greenfield Workspace Today!</a>
    </div>
  </div>
  <!-- END diagram -->


  <div id="test" class="container container__centered">
    <h2>Greenfield Workspace test patient examples</h2>
    <p>Greenfield Workspace contains test patient information only and has no connection with any health care organization's actual data. These patient examples can be used within the Greenfield ecosystem. Each patient will work within the Greenfield Workspace testing app or through any software with RESTful API calls.</p>
    <p>If you have a request regarding a specific use case not referenced here, please use the <a href="https://home.meditech.com/webforms/contact.asp?rcpt=greenfieldinfo%7Cmeditech%7Ccom&rname=greenfieldinfo">Greenfield Contact form</a> to provide more information regarding your desired API integration. We will contact you as our capabilities expand.</p>

    <div class="container card__wrapper auto-margins">

      <div class="container__one-third card bg--emerald">
        <figure>
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/elderly-black-woman-face.jpg" width="735" height="225" alt="black woman's face">
        </figure>
        <div class="card__info">
          <h3>Meet Abigail</h3>
          <p>Abigail has hypertension and was recently diagnosed with congestive heart failure.</p>
          <hr />
          <p><strong>Name:</strong> Abigail Francis<br>
            <strong>ID:</strong> 925293E8-3491-4E2B-9FE1-46310EE776C2<br>
            <strong>DOB:</strong> 05/10/1934<br>
            <strong>Address:</strong> 239 Middle Rd Canton MA 02021<br>
            <strong>Phone:</strong> 508-555-1931<br>
            <strong>Email:</strong> 1234test@email.com<br>
          </p>
          <!-- Start modal button trigger -->
          <div class="center">
            <a class="btn--orange open-modal" data-target="modal1" href="javascript:void(0)">View Abigail's Data</a>
          </div>
          <!-- End modal button trigger -->
        </div>
      </div>
      <div class="container__one-third card bg--dark-blue">
        <figure>
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/white-male-face.jpg" width="735" height="225" alt="causasian male's face">
        </figure>
        <div class="card__info">
          <h3>Meet Tim</h3>
          <p>Tim has been working with his care team to keep his asthma under control.</p>
          <hr />
          <p><strong>Name:</strong> Timothy Gibbs<br>
            <strong>ID:</strong> 6E412ED2-6AEC-43A3-8349-B3861751C234<br>
            <strong>DOB:</strong> 09/03/1981<br>
            <strong>Address:</strong> 22 Main St Boston MA 02110<br>
            <strong>Phone:</strong> 781-555-9999<br>
            <strong>Email:</strong> test5678@email.com<br>
          </p>
          <!-- Start modal button trigger -->
          <div class="center">
            <a class="btn--orange open-modal" data-target="modal2" href="javascript:void(0)">View Tim's Data</a>
          </div>
          <!-- End modal button trigger -->
        </div>
      </div>
      <div class="container__one-third card bg--emerald">
        <figure>
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/middle-aged-hispanic-woman-face.jpg" width="735" height="225" alt="Hispanic woman's face">
        </figure>
        <div class="card__info">
          <h3>Meet Raquel</h3>
          <p>Following a kidney transplant, Raquel has been hospitalized with pneumonia.</p>
          <hr />
          <p><strong>Name:</strong> Raquel Gregory<br>
            <strong>ID:</strong> 4325226D-91B4-4CA4-847B-A856DA54DEB6<br>
            <strong>DOB:</strong> 10/06/1964<br>
            <strong>Address:</strong> 4525 Island Rd Plymouth MA 02360<br>
            <strong>Phone:</strong> 877-555-7777<br>
            <strong>Email:</strong> test3579@email.com<br>
          </p>
          <!-- Start modal button trigger -->
          <div class="center">
            <a class="btn--orange open-modal" data-target="modal3" href="javascript:void(0)">View Raquel's Data</a>
          </div>
          <!-- End modal button trigger -->
        </div>
      </div>

    </div>
    <p>In order to make API calls outside of the Workspace testing app, you'll need OAuth credentials. Credentials will be received during registration and acceptance into Greenfield.</p>

    <!-- Start hidden modal box -->
    <div id="modal1" class="modal">
      <a class="close-modal" href="javascript:void(0)">&times;</a>
      <div class="modal-content modal-content--vid" style="height:70%; overflow:auto; text-align:left;">
        <table style="background:white;">
          <tbody>
            <tr>
              <th>Name</th>
              <td>Abigail Francis</td>
            </tr>
            <tr>
              <th>Sex</th>
              <td>F</td>
            </tr>
            <tr>
              <th>DOB</th>
              <td>05/10/1934</td>
            </tr>
            <tr>
              <th>Race</th>
              <td>Black or African American</td>
            </tr>
            <tr>
              <th>Ethnicity</th>
              <td>Not Hispanic or Latino</td>
            </tr>
            <tr>
              <th>Pref Language</th>
              <td>English</td>
            </tr>
            <tr>
              <th>Smoking Status</th>
              <td>Never smoker</td>
            </tr>
            <tr>
              <th>Problems</th>
              <td>"Myocardial infarction
                <br>Systolic congestive...
                <br>hypertension"
              </td>
            </tr>
            <tr>
              <th>Meds</th>
              <td>furosemide 20 mg
                <br>lisinopril
                <br>metoprolol sucinate
              </td>
            </tr>
            <tr>
              <th>Allergies</th>
              <td>penicillin G</td>
            </tr>
            <tr>
              <th>Labs</th>
              <td>UA (includes many other tests)</td>
            </tr>
            <tr>
              <th>Vitals</th>
              <td>Interventions:
                <br>1) Height and Weight
                <br>2) Vital Signs (N)
              </td>
            </tr>
            <tr>
              <th>Procedures</th>
              <td>RT pulmonary function test</td>
            </tr>
            <tr>
              <th>Care Team</th>
              <td>MEDITECH</td>
            </tr>
            <tr>
              <th>Immunizations</th>
              <td>"dtap
                <br>influenza"
              </td>
            </tr>
            <tr>
              <th>Unique Device Identifier</th>
              <td>Kennestone Loop Recorder Pack</td>
            </tr>
            <tr>
              <th>Assessment Plan of Treatment</th>
              <td>Reviewed dietary and exercise concerns with the patient. Advised on high humidity/warm days to try to walk early in morning or late afternoon. </td>
            </tr>
            <tr>
              <th>Goals</th>
              <td>Continue with medication regimen as prescribed.</td>
            </tr>
            <tr>
              <th>Health Concerns</th>
              <td>Patient recently has been diagnosed with high blood pressure. </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- End hidden modal box -->

    <!-- Start hidden modal box -->
    <div id="modal2" class="modal">
      <a class="close-modal" href="javascript:void(0)">&times;</a>
      <div class="modal-content modal-content--vid" style="height:70%; overflow:auto; text-align:left;">
        <table style="background:white;">
          <tbody>
            <tr>
              <th>Name</th>
              <td>Timothy Gibbs</td>
            </tr>
            <tr>
              <th>Sex</th>
              <td>M</td>
            </tr>
            <tr>
              <th>DOB</th>
              <td>09/03/1981</td>
            </tr>
            <tr>
              <th>Race</th>
              <td>White</td>
            </tr>
            <tr>
              <th>Ethnicity</th>
              <td>Not Hispanic or Latino</td>
            </tr>
            <tr>
              <th>Pref Language</th>
              <td>English</td>
            </tr>
            <tr>
              <th>Smoking Status</th>
              <td>Never Smoker</td>
            </tr>
            <tr>
              <th>Problems</th>
              <td>Vitamin D deficiency
                <br>Hypertension
                <br>Seasonal Allergies
                <br>Asthma
              </td>
            </tr>
            <tr>
              <th>Meds</th>
              <td>Proair
                <br>Claritin
              </td>
            </tr>
            <tr>
              <th>Allergies</th>
              <td>Bactrim (composed of sulfamethoxazole and trimethoprim)</td>
            </tr>
            <tr>
              <th>Labs</th>
              <td>CREAT</td>
            </tr>
            <tr>
              <th>Vitals</th>
              <td>Interventions:
                <br>1) Height and Weight
                <br>2) Vital Signs (N)
              </td>
            </tr>
            <tr>
              <th>Procedures</th>
              <td>XR CHEST 2V</td>
            </tr>
            <tr>
              <th>Care Team</th>
              <td>MEDITECH</td>
            </tr>
            <tr>
              <th>Immunizations</th>
              <td>Hep A
                <br>Influenza
              </td>
            </tr>
            <tr>
              <th>Unique Device Identifier</th>
              <td>Tibia Nail, sterile</td>
            </tr>
            <tr>
              <th>Assessment Plan of Treatment</th>
              <td>Reviewed at home Peak Flow testing. Discussed exercise program and reviewed impact of environmental factors. Advised patient of the option for dietary consult. Continuing with medication regimen as prescribed.</td>
            </tr>
            <tr>
              <th>Goals</th>
              <td>Continue with prescriptions as directed. Implement exercise program modifications as discussed. Contact our offices if you experience any significant change in condition.</td>
            </tr>
            <tr>
              <th>Health Concerns</th>
              <td>"1. Documented Chronic Asthma problem
                <br>2. Documented Seasonal Allergy problem
                <br>3. Documented Hypertension problem"
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- End hidden modal box -->

    <!-- Start hidden modal box -->
    <div id="modal3" class="modal">
      <a class="close-modal" href="javascript:void(0)">&times;</a>
      <div class="modal-content modal-content--vid" style="height:70%; overflow:auto; text-align:left;">
        <table style="background:white;">
          <tbody>
            <tr>
              <th>Name</th>
              <td>Raquel Gregory</td>
            </tr>
            <tr>
              <th>Sex</th>
              <td>F</td>
            </tr>
            <tr>
              <th>DOB</th>
              <td>10/06/1964</td>
            </tr>
            <tr>
              <th>Race</th>
              <td>Nat Hawaiian/Other Pacific Isl</td>
            </tr>
            <tr>
              <th>Ethnicity</th>
              <td>Hispanic or Latino</td>
            </tr>
            <tr>
              <th>Pref Language</th>
              <td>Spanish</td>
            </tr>
            <tr>
              <th>Smoking Status</th>
              <td>Current Every Day</td>
            </tr>
            <tr>
              <th>Problems</th>
              <td>Interstitial pneumonia
                <br>Iron deficiency anemia
                <br>Chronic rejection of renal transplant
                <br>Sever hypothyroidism
                <br>Essential hypertension
              </td>
            </tr>
            <tr>
              <th>Meds</th>
              <td>everolimus
                <br>ciprofloxacin
                <br>prednisolone
              </td>
            </tr>
            <tr>
              <th>Allergies</th>
              <td>ampicilin and penicillin G</td>
            </tr>
            <tr>
              <th>Labs</th>
              <td>UA (includes many other tests)</td>
            </tr>
            <tr>
              <th>Vitals</th>
              <td>Interventions:
                <br>1) Height and Weight
                <br>2) Vital Signs (N)
              </td>
            </tr>
            <tr>
              <th>Procedures</th>
              <td>1) RT Pulmonary function test
                <br>2) XR CHEST 2V
              </td>
            </tr>
            <tr>
              <th>Care Team</th>
              <td>MEDITECH</td>
            </tr>
            <tr>
              <th>Immunizations</th>
              <td>Influenza
                <br>Tetanus
              </td>
            </tr>
            <tr>
              <th>Unique Device Identifier</th>
              <td>Entered 00643169007222 (Cardiac Resynchronization...)</td>
            </tr>
            <tr>
              <th>Assessment Plan of Treatment</th>
              <td>The patient was found to have Anemia and Ms Raquel was treated for Anemia during her stay at the hospital. Ms Raquel recovered from Anemia during the stay and is being discharged in a stable condition. If there is fever greater than 101.5 F or onset of chest pain or breathlessness the patient is advised to contact emergency services.</td>
            </tr>
            <tr>
              <th>Goals</th>
              <td>a. Need to gain more energy to do regular activities
                <br>b. Negotiated Goal to keep Body Temperature at 98-99 degrees Fahrenheit with regular monitoring.
              </td>
            </tr>
            <tr>
              <th>Health Concerns</th>
              <td>a. Chronic Sickness exhibited by patient
                <br>b. HealthCare Concerns refer to underlying clinical facts
                <br>i. Documented HyperTension problem
                <br>ii. Documented HypoThyroidism problem
                <br>iii. Watch Weight of patient
                <br>iv. Documented Anemia problem
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <!-- End hidden modal box -->

  </div>


  <!-- Block -->
  <div class="container container__centered center icons">
    <h2 style="margin-bottom:2em;">Get started in the Greenfield Workspace</h2>
    <div class="card__wrapper">

      <div class="container__one-fourth card bg--emerald">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/MEDITECH-Greenfield-Workspace-logo--white-knockout.svg" width="50" height="50" style="width:60%; height:auto; margin-top:2.5em; margin-bottom:0.5em;">
        <div class="card__info">
          <p><a href="https://ehr.meditech.com/ehr-solutions/greenfield-workspace">About Greenfield Workspace</a></p>
        </div>
      </div>
      <div class="container__one-fourth card bg--emerald">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--resources.svg" width="50" height="50">
        <div class="card__info">
          <p><a href="https://ehr.meditech.com/ehr-solutions/greenfield-resources">Greenfield resources</a></p>
        </div>
      </div>
      <div class="container__one-fourth card bg--dark-blue">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--construction.svg" width="50" height="50">
        <div class="card__info">
          <p><a href="https://greenfield.meditech.com/">Enter the Greenfield Workspace</a></p>
        </div>
      </div>
      <div class="container__one-fourth card bg--dark-blue">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--email.svg" width="50" height="50">
        <div class="card__info">
          <p><a href="https://jira.meditech.com/servicedesk/customer/portal/1">Contact our Service Desk for help</a></p>
        </div>
      </div>

    </div>
  </div>
  <!-- End Block -->


  <!-- Block -->
  <div class="container">
    <div class="container__centered center">
      <h2>MEDITECH Greenfield Alliance</h2>
      <p>MEDITECH is creating an ecosystem of partner organizations with proven, successful, and interoperable solutions.</p>
      <a href="https://ehr.meditech.com/ehr-solutions/greenfield-alliance" class="btn--orange btn--outline">Learn more about Greenfield Alliance</a>

      <div style="margin-top:2em;">
        <?php print $share_link_buttons; ?>
      </div>
    </div>
  </div>
  <!-- End Block -->


  <?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>

  <!-- END campaign--node-3703.php -->
