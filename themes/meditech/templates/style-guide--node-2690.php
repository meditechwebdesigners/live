<?php if( user_is_logged_in() ){ ?>
<!-- Start style-guide--node-2690.php Modal Pop-up -->

<!--Javascript & CSS reference for Prism-->
<script type="text/javascript" src="<?php print $url; ?>/sites/all/themes/meditech/js/prism.js"></script>

<link href="<?php print $url; ?>/sites/all/themes/meditech/css/prism.css" rel="stylesheet" />

<section class="container__centered">

  <h1 class="page__title">
    <?php print $title; ?>
  </h1>

  <div class="container__two-thirds">
    <div class="container no-pad">
      <p>A modal is a dialog box/popup window that is displayed on top of the current page. This tool can be used to present a larger version of an image, a pop up window for text, and more. The modal tool toggles your hidden content on demand via JavaScript.</p>

      <ul>
        <li>This modal is designed to appear above everything but the sticky nav. This way users know they are still on MEDITECH's website and not in a new browser window.</li>
        <li>The "close out" trigger is basically the entire modal including the image and background mask (minus the sticky nav). This is done to make it completely obvious and user-friendly.</li>
      </ul>

      <h2>Usage</h2>
      <p>Below is the proper HTML structure. You will see that we start out with the hidden content HTML, then we create a trigger to display the content. This trigger can be an image, text, button, etc. as long as it's wrapped in the <code class="language-css">.open-modal</code> class. For the example below we've chosen a thumbnail image trigger with a magnifying glass icon that appears on hover.</p>

      <p><span class="italic"><strong>Note:</strong> A media query has been set at 50em (800px) to disable this function. It is not needed because most images should already be at 100% width on tablet and mobile devices.</span></p>

      <h3>Modal Pop-up for Screenshot</h3>

      <div class="demo-ct">
        <!-- Start Modal Pop-up on Screenshot Demo -->
        <div id="modal1" class="modal">
          <a class="close-modal" href="javascript:void(0)">&times;</a>
          <div class="modal-content">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/BCA-Ambulatory--Regulatory-Clinical-Quality-Analysis.jpg" />
          </div>
        </div>

        <div class="open-modal" data-target="modal1">
          <img style="width:350px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/BCA-Ambulatory--Regulatory-Clinical-Quality-Analysis.jpg">
          <div class="mag-bg">
            <i class="mag-icon fas fa-search-plus"></i>
          </div>
        </div>
        <!-- End Modal Pop-up on Screenshot Demo -->
      </div>

      <!-- Start Modal Pop-up on Screenshot Code -->
      <pre><code class="language-html">&lt;!-- Start hidden modal box -->
&lt;div id="modal1" class="modal">
  &lt;a class="close-modal" href="javascript:void(0)">&amp;times;&lt;/a>
  &lt;div class="modal-content">
    &lt;img src="full-size.jpg"> &lt;!-- Add modal content here -->
  &lt;/div>
&lt;/div>
&lt;!-- End hidden modal box -->

&lt;!-- Start modal trigger -->
&lt;div class="open-modal" data-target="modal1">
  &lt;img src="thumbnail.jpg"> &lt;!-- Add modal trigger here -->
  &lt;div class="mag-bg"> &lt;!-- Include if using image trigger -->
    &lt;i class="mag-icon fas fa-search-plus">&lt;/i>
  &lt;/div>
&lt;/div>
&lt;!-- End modal trigger -->
</code></pre>
      <!-- End Modal Pop-up on Screenshot Code -->

    </div>

    <div class="container no-pad">
      <h3>Modal Pop-up for Video</h3>
      <p>The modal pop-up for videos works very similar to screenshots. Instead of adding a full size image in the modal content div, you would add your Vimeo or YouTube iframe. It is recommended that you adjust the iframe's width and height to 100% to properly fill the screen. Don't forget to add the additional <code class="language-css">modal-content--vid</code> class.</p>
      <div class="demo-ct">
        <!-- Start Modal Pop-up for Video Demo -->
        <div id="modal2" class="modal">
          <a class="close-modal" href="javascript:void(0)">&times;</a>
          <div class="modal-content modal-content--vid">
            <iframe src="https://player.vimeo.com/video/336802953" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
          </div>
        </div>

        <div class="open-modal" data-target="modal2">
          <img style="width:350px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/video-overlay.jpg">
          <div class="mag-bg">
            <i class="mag-icon fas fa-play"></i>
          </div>
        </div>
        <!-- End Modal Pop-up for Video Demo -->
      </div>

      <!-- Start Modal Pop-up for Video Code -->
      <pre><code class="language-html">&lt;!-- Start hidden modal box -->
&lt;div id="modal2" class="modal">
  &lt;a class="close-modal" href="javascript:void(0)">&amp;times;&lt;/a>
  &lt;div class="modal-content modal-content--vid"> &lt;!-- Add "--vid" to class name -->
    &lt;iframe src="https://player.vimeo.com/video/336802953" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen>&lt;/iframe> &lt;!-- Add iframe content here -->
  &lt;/div>
&lt;/div>
&lt;!-- End hidden modal box -->

&lt;!-- Start modal trigger -->
&lt;div class="open-modal" data-target="modal2">
  &lt;img src="video.jpg"> &lt;!-- Add video modal trigger here -->
  &lt;div class="mag-bg"> &lt;!-- Include if using image trigger -->
    &lt;i class="mag-icon fas fa-play">&lt;/i> &lt;!-- Change Font Awesome icon to play button -->
  &lt;/div>
&lt;/div>
&lt;!-- End modal trigger -->
</code></pre>
      <!-- End Modal Pop-up for Video Code -->
    </div>

    <div class="container no-pad">
      <h3>Modal Pop-up for Text/Button</h3>
      <p>Displayed below is an example of a modal trigger using a plain text link and a button. The content hidden inside the modal is a very simple H1 and p tag wrapped in a stylized div. Typically we try to avoid hiding textual content but in some cases it can be appropriate.</p>

      <p><span class="italic"><strong>Note:</strong> By default, you will not be able to add clickable links or buttons within this content modal because the close out "x" is overlaid across the whole viewport.</span></p>

      <div class="demo-ct">
        <!-- Start Modal Pop-up on Text Demo -->
        <div class="container__one-half center">
          <div id="modal3" class="modal">
            <a class="close-modal" href="javascript:void(0)">&times;</a>
            <div class="modal-content">
              <div class="modal-text">
                <h2>This is an example of a modal text headline</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis id accumsan odio, sed lacinia eros. Morbi eget quam turpis. Suspendisse quis ante et mauris tempor tempor. Phasellus ullamcorper enim erat, ut laoreet sem maximus et. Duis consequat rutrum odio, sit amet tempor magna vestibulum sit amet.</p>
              </div>
            </div>
          </div>

          <a class="open-modal" data-target="modal3" href="javascript:void(0)">
            <p style="margin-top: .75em;">Click Me to Open Modal</p>
          </a>
        </div>
        <!-- End Modal Pop-up on Text Demo -->

        <!-- Start Modal Pop-up on Button Demo -->
        <div class="container__one-half center">
          <div id="modal4" class="modal">
            <a class="close-modal" href="javascript:void(0)">&times;</a>
            <div class="modal-content">
              <div class="modal-text">
                <h2>This is an example of a modal text headline</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis id accumsan odio, sed lacinia eros. Morbi eget quam turpis. Suspendisse quis ante et mauris tempor tempor. Phasellus ullamcorper enim erat, ut laoreet sem maximus et. Duis consequat rutrum odio, sit amet tempor magna vestibulum sit amet.</p>
              </div>
            </div>
          </div>

          <a class="btn--orange open-modal" data-target="modal4" href="javascript:void(0)">
            Click Me to Open Modal
          </a>
        </div>
        <!-- End Modal Pop-up on Button Demo -->

      </div>

      <!-- Start Modal Pop-up on Text Code -->
      <pre><code class="language-html">&lt;!-- Start modal text link trigger -->
&lt;a class="open-modal" data-target="modal3" href="javascript:void(0)">
  Click Me to Open Modal
&lt;/a>
&lt;!-- End modal text link trigger -->

&lt;!-- Start modal button trigger -->
&lt;a class="btn--orange open-modal" data-target="modal4" href="javascript:void(0)">
  Click Me to Open Modal
&lt;/a>
&lt;!-- End modal button trigger -->
</code></pre>
      <!-- End Modal Pop-up on Text Code -->

    </div>

    <div class="container no-pad">
      <h3>Multiple Modal Pop-ups</h3>
      <p>At some point you'll probably need to add multiple modal pop-ups on the same page. In order to do this without conflicts, you'll need to specify which trigger is opening which content. Use the same HTML structure listed above but this time add IDs on the trigger and content.</p>

      <div class="demo-ct">
        <!-- Start Multiple Modal Pop-ups Demo -->
        <div class="container__one-half">
          <!-- Start Modal 1 -->
          <div id="modal5" class="modal">
            <a class="close-modal" href="javascript:void(0)">&times;</a>
            <div class="modal-content">
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/BCA-Ambulatory--Regulatory-Clinical-Quality-Analysis.jpg">
            </div>
          </div>

          <div class="open-modal" data-target="modal5">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/BCA-Ambulatory--Regulatory-Clinical-Quality-Analysis.jpg" style="width:100%;">
            <div class="mag-bg">
              <i class="mag-icon fas fa-search-plus"></i>
            </div>
          </div>
          <!-- End Modal 1 -->
        </div>

        <div class="container__one-half">
          <!-- Start Modal 2 -->
          <div id="modal6" class="modal">
            <a class="close-modal" href="javascript:void(0)">&times;</a>
            <div class="modal-content">
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/BCA-Ambulatory--CPT-Revenue-Analysis.jpg">
            </div>
          </div>

          <div class="open-modal" data-target="modal6">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/BCA-Ambulatory--CPT-Revenue-Analysis.jpg" style="width:100%;">
            <div class="mag-bg">
              <i class="mag-icon fas fa-search-plus"></i>
            </div>
          </div>
          <!-- End Modal 2 -->
        </div>
        <!-- End Multiple Modal Pop-ups Demo -->

      </div>

      <!-- Start Multiple Modal Pop-ups Code -->
      <pre><code class="language-html">&lt;!-- Start modal pop-up 1 -->
&lt;div id="modal4" class="modal"> &lt;!-- Add specific ID here -->
&lt;div class="open-modal" data-target="modal4"> &lt;!-- Add data target here -->
&lt;!-- End modal pop-up 1 -->

&lt;!-- Start modal pop-up 2 -->
&lt;div id="modal5" class="modal"> &lt;!-- Add specific ID here -->
&lt;div class="open-modal" data-target="modal5"> &lt;!-- Add data target here -->
&lt;!-- End modal pop-up 2 -->
</code></pre>
      <!-- End Multiple Modal Pop-ups Code -->


    </div>

  </div>

  <!-- Start Sidebar -->
  <aside class="container__one-third">

    <div class="sidebar__nav panel">
      <?php
        $styleBlock = module_invoke('menu', 'block_view', 'menu-style-guide-side-nav');
        print render($styleBlock['content']); 
        ?>
    </div>

  </aside>
  <!-- End Sidebar -->

</section>

<!-- End style-guide--node-2690.php Modal Pop-up -->
<?php } ?>