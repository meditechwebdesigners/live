<?php // This template is for each row of the Views block: EVENTS - TRADE SHOWS ....................... 

  // get node ID...
  $nid = $fields['nid']->content;
  $node = node_load($nid);

  // get event date data...
  $eventDate = field_get_items('node', $node, 'field_event_date');
  // get first date data...
  $date1 = date_create($eventDate[0]['value']);
  $day1 = date_format($date1, 'd');
  $month1 = date_format($date1, 'm');
  // get second date data...
  $date2 = date_create($eventDate[0]['value2']);
  $day2 = date_format($date2, 'd');
  $month2 = date_format($date2, 'm');

?>
<!-- start views-view-fields--events-trade-shows--block.tpl.php template -->
  <tr <?php if ($row_classes[$row_count]) { print 'class="' . implode(' ', $row_classes[$row_count]) .'"';  } ?>>
    <td>
      <?php
        print $month1.'/'.$day1;
        // if start date and end date are not in the same month...
        if($month1 != $month2){
          print " - ".$month2.'/'.$day2;
        }
        // else if dates are in the same month...
        elseif($month1 == $month2 && $day1 != $day2){
          print " - ".$day2;
        }
        // otherwise one day event...
        else{
        }
      ?> 
      <?php 
      if( user_is_logged_in() ){ 
        print '<p style="font-size:12px;"><a href="https://ehr.meditech.com/node/'.$nid.'/edit">Edit</a></p>';
      }
      ?> 
    <td>
      <strong><a class="tradeshow_gae" href="<?php print $fields['field_website_url']->content; ?>" target="_blank"><?php print $fields['title']->content; ?></a></strong><br />
      <?php print $fields['field_location_name']->content; ?>
      <?php if($fields['field_booth_number']->content){ ?>
        <br />[Booth <strong>#<?php print $fields['field_booth_number']->content; ?></strong>]
      <?php } ?>
    </td>
    <td>
      <?php print $fields['field_location_city']->content; ?>
    </td>
    <td>
      <?php  
      if( $fields['field_location_country']->content == 'United States' || $fields['field_location_country']->content == 'Canada' ){
        print $fields['field_location_state']->content;
      }
      else{
        print $fields['field_location_country']->content;
      }
      ?>
    </td>
  </tr>
<!-- end views-view-fields--events-trade-shows--block.tpl.php template -->