
<!-- START campaign--node-3280.php -->
<?php // This template is set up to control the display of the NEW campaign patient engagement content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');
$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>


<style>

  .quote-text { 
    width: 100%; 
    padding: 1em 3em .5em 3em; 
    font-style: italic; 
    font-size: 1.4em; 
    color: white; 
    text-align: left; 
  }

  .content__callout__image-wrapper_2:after {
    border: solid transparent;
    content: " ";
    height: 0;
    width: 0;
    position: absolute;
    top: 50%;
    right: 0;
    margin-top: -3.5em;
    border-color: transparent;
    border-right-color: transparent;
    border-width: 1.2em 1.6em;
    pointer-events: none;
  }

  
  .bg--purple-gradient {
    background: #B24ec4;
    background: -webkit-linear-gradient(#B24ec4, #540262);
    background: -o-linear-gradient(#B24ec4, #540262);
    background: -moz-linear-gradient(#B24ec4, #540262);
    background-image: linear-gradient(-150deg, #B24ec4, #540262);
    color: #fff
  }

  .circle-icon {
    height: 75px;
    width: 75px;
    color: #fff;
    display:inline-flex;
    justify-content: center;
    align-items: center;
    border-radius: 50%;
    margin: 0 auto;
    margin-right: 1em;
}

    @media all and (max-width: 50em) {
        .content__callout__content {
            padding-left: 6%;
            color: #3e4545;
            padding-top: 0em;
        }

        .video {
            max-width: 100%;
        }
    }

    .video--shadow {
        border-radius: 6px;
        box-shadow: 13px 13px 40px rgba(0, 0, 0, 0.3);
    }

    @media all and (max-width: 1100px) {
      .mobile-padding {padding: 0em 2em;}
    }



</style>


<div class="js__seo-tool__body-content">

<!-- Block 1 Hero -->
<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/three_generations_of_women2.jpg);background-color:#3e4545;">
  <div class="container__centered">
    <div class="container__one-half transparent-overlay--xp text--white text-shadow--black" style="background-color: rgba(95, 78, 71, 0.8);"> 
      <h1 class="js__seo-tool__title">The patient experience matters: support their journey with MEDITECH Expanse.</h1>
      <p>Getting — and keeping — patients involved in their own care is the foundation of a positive patient experience. Foster that engagement with MEDITECH Expanse. Our solutions create a <i>convenient, simple, and mobile-enabled journey for your patients.</i> What's more, Expanse helps your organization to build better partnerships between patients and their trusted providers.
      </p>

  	<div class="btn-holder--content__callout">
        <div class="center" style="margin-top:2em;">
          <?php hubspot_button($cta_code, "Sign Up For The Population Health Insights Webinar"); ?>
        </div>
      </div>

    </div>
  </div>
</div>
<!-- Close Block 1 -->



<!-- Block 2 - Virtual Visits -->
<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/gray_textured_gradient.png);">
  <div class="container__centered">
  
    <div class="container__two-thirds">
      <h2>Stay connected no matter what, through virtual care.</h2>
      
      <p>The COVID-19 pandemic has awakened all of us to the importance of keeping patients safe and healthy by meeting them wherever they are. <a href="https://ehr.meditech.com/ehr-solutions/virtual-care">MEDITECH’s Virtual Care solutions</a> enable patients to stay connected to their care providers (and your organization) via convenient video visits, <a href="https://home.meditech.com/en/d/mktcontent/otherfiles/virtualondemandcareinfographicweb.pdf" target="_blank">no matter the circumstances.</a> And now, with Virtual On Demand Care, healthcare organizations can extend their virtual care offering by providing timely and convenient urgent care appointments to both new and existing patients. </p>
    </div>
    <div class="container__one-third">
      <div class="container__centered transparent-overlay text--white" style="margin: .5em 2em;">
        <?php include('inc-virtual-care-minutes.php'); ?>
      </div>
    </div>

  </div>
</div>
<!-- End Block 2 - Virtual Visits -->



<!-- Start of Quotes Block -->
<div class="container no-pad">
    <div class="gl-container">
    
    <div class="container__one-half bg--blue-gradient" style="padding:3em;">
      <article class="container__centered text--white center auto-margins">
      <figure>
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Doug-Kanis--headshot.png" alt="Doug-Kanis-DO" style="width:150px;">
      </figure>
      <div class="text--large italic" style="font-size:1.4em; padding-top: .75em;">
        <p>“We recently fast-tracked the Virtual Visit functionality. In literally a matter of days, I was doing Virtual Visits on a cardiac transplant patient and a kidney transplant patient recovering from prostate cancer surgery. We also had <a href="https://blog.meditech.com/how-virtual-visits-are-keeping-providers-and-patients-safe-at-citizens-memorial-healthcare">great support</a> and guidance from Citizens Memorial in Bolivar, Missouri.”</p>
      </div>
      <p class="no-margin--bottom text--large bold text--meditech-green">Doug Kanis, DO</p>
      <p>Pella Regional Health Center</p>
    </article>
    </div>

    <div class="container__one-half bg--purple-gradient" style="padding:3em;">
      <article class="container__centered text--white center auto-margins">
      <figure>
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/William-Dailey--headshot.png" alt="William-Dailey-MD" style="width:150px;">
      </figure>
      <div class="text--large italic" style="font-size:1.4em; padding-top: .75em;">
        <p>“Now we have integrated ambulatory <a href="https://ehr.meditech.com/news/meditech-virtual-visits-connect-providers-patients-at-med-center-health">video visits</a> up in Expanse. The EMR does instantaneous registration, spawns the visit documentation, and establishes the bidirectional video link. The remarkable thing is, it only took a week to set up.”</p>
      </div>
      <p class="no-margin--bottom text--large bold text--meditech-green">William Dailey, MD</p>
      <p>MS, MSMI, CMIO, Golden Valley Memorial Healthcare</p>
    </article>
    </div>

  </div>
</div>
<!-- End of Quotes Block -->


<!-- Block 3 MHealth -->
<div class="container background--cover" style="background-color: rgba(62, 69, 69, 1);">
   <div class="container__centered text--white">

      <div>
        <h2>Encourage patients to own their healthcare.</h2>
          <p>MEDITECH's MHealth app makes it easy for patients to share health data with care providers, bring family members or health proxies up to date, and engage in their own wellness journey, all from their device of choice.</p>
      </div>

    <div class="container__one-half center">
      <div class="container no-pad">
        <div class="container__one-half">

          <!-- Hidden Modal -->

        <div id="modal17" class="modal">
          <a class="close-modal" href="javascript:void(0)">&times;</a>
          <div class="modal-content">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MHealth_diabetes_screenshot1--large.png" alt="MHealth screenshot diabetes">
          </div>
        </div>

          <!-- End of Hidden Modal -->

          <!-- Start modal trigger -->

                <div class="open-modal" data-target="modal17">
                    <div class="phone--white">
                      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MHealth_diabetes_screenshot1--small.png" alt="MHealth screenshot diabetes">
                    </div>
                     <!-- Add modal trigger here -->
                    <div class="mag-bg">
                        <!-- Include if using image trigger -->
                        <i class="mag-icon fas fa-search-plus"></i>
                    </div>
                </div>

          <!-- End modal trigger -->

        </div>
        <div class="container__one-half">

          <!-- Hidden Modal -->

        <div id="modal16" class="modal">
          <a class="close-modal" href="javascript:void(0)">&times;</a>
          <div class="modal-content">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MHealth_diabetes_screenshot2--large.png" alt="MHealth screenshot diabetes">
          </div>
        </div>

          <!-- End of Hidden Modal -->

          <!-- Start modal trigger -->

                <div class="open-modal" data-target="modal16">
                    <div class="phone--white">
                      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MHealth_diabetes_screenshot2--small.png" alt="MHealth screenshot diabetes">
                    </div>
                     <!-- Add modal trigger here -->
                    <div class="mag-bg">
                        <!-- Include if using image trigger -->
                        <i class="mag-icon fas fa-search-plus"></i>
                    </div>
                </div>

          <!-- End modal trigger -->

        </div>
      </div>
      <div style="clear:both;">
        <p>Questionnaires Specific to Appointment Type</p>
      </div>
            <figure class="no-target-icon" style="display: inline-block; margin-right:1em;">
                <a href="https://itunes.apple.com/us/app/meditech-mhealth/id1143209032?mt=8" target="_blank"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/App-Store-Badge.png" alt="App Store Download Badge"></a>
            </figure>
              
            <figure class="no-target-icon" style="display: inline-block;">
                <a href="https://play.google.com/store/apps/details?id=com.meditech.PatientPhm&hl=en" target="_blank"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Google-Play-Badge.png" alt="Google Play Download Badge"></a>
            </figure>
    </div>
          
    <div class="container__one-half">
        <h4>Remote patient monitoring capabilities.</h4>
              <p>When it’s important to keep patients at home or limit staff exposure, care providers can practice remote monitoring, and encourage patients to <a href="https://www.meditech.com/productbriefs/flyers/TelehealthApproach.pdf">upload data</a> from personal health devices and medical device kits into MEDITECH’s Patient and Consumer Health Portal. By tracking and trending this data, providers can keep a close eye on patients with chronic or preexisting conditions and adjust treatment accordingly, without having the patient come in for an office visit.</p>
        <h4>Shared access</h4>
              <p>It’s easy for patients to involve other caregivers, family members, and proxies in their care, when they can easily <a href="https://home.meditech.com/en/d/mktcontent/otherfiles/patientexperienceflyer.pdf">manage who has access</a> to their health records. Shared Access spares clinicians and office staff the administrative hassle of tracking down paperwork or following up on tasks that can be completed online.</p>
     </div>
   </div>
</div>
<!-- End of MHealh -->

<!-- Start Intake Block -->
<div class="container bg--white">

  <div class="container__centered">

    <div class="container__one-third">
      <h2>Automated patient intake for the tech-savvy consumer.</h2>
        <p>MEDITECH’s self-service intake solutions improve the patient experience and enable office staff to minimize the cumbersome tasks associated with traditional paper forms. Learn how our Pre-Registration and Self Check-In functionality can help your organization to become more efficient, and focus more energy on the patient experience, instead of the paper trail.</p>
    </div>

    <div class="container__two-thirds">

    <div class="content__callout border-none" style="background-color: #FFFFFF !important;">
    <div class="content__callout__media">
        <div class="content__callout__image-wrapper_2" style="padding:3em !important;">
            <div class="video js__video video--shadow" data-video-id="396714649">
                <figure class="video__overlay">
                    <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/pre-registration--video-overlay.jpg" alt="Pre Registration Check in video">
                </figure>
                <a class="video__play-btn" href="https://vimeo.com/meditechehr/review/396714649/eea6921213"></a>
                <div class="video__container"></div>
            </div>
        </div>
    </div>
    </div>

    </div>
  </div>

  <div class="container__centered" style="padding: 2em 0em;">
      
      <div class="container__one-half mobile-padding">
        <div class="container__one-fifth center">
          <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/icon--patient_profile.png" alt="Schedule icon">
        </div>
        <div class="container__four-fifths">
          <p><b>Fewer scanning and transcription errors, as well as reduced denials.</b></p>
          <p>Patients update their patient, family, and social history; demographics and insurance information; and complete questionnaires online prior to the appointment, leading to quicker processing and intake.</p>
        </div>
      </div>

      <div class="container__one-half mobile-padding">
        <div class="container__one-fifth center">
          <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/icon--digital_check_in.png" alt="Schedule icon">
        </div>
        <div class="container__four-fifths">
        <p><b>More efficient check-in process.</b></p>
        <p>Using the MHealth App, patients can electronically check in for their appointments, as well as settle copays online without having to go to the check-in desk.</p>
      </div>
    </div>
  </div>

  <div class="container__centered" style="padding: 0em 0em 3em 0em;">
      <div class="container__one-half mobile-padding">
        <div class="container__one-fifth center">
          <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/icon--bill_pay.png" alt="Schedule icon">
        </div>
        <div class="container__four-fifths">
        <p><b>Know what charges to expect.</b></p>
        <p>Upon completing pre-registration, patients are presented with their expected copay before setting foot through the office doors.</p>
      </div>
      </div>

      <div class="container__one-half mobile-padding">
        <div class="container__one-fifth center">
          <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/icon--appointments_on_time.png" alt="Schedule icon">
        </div>
        <div class="container__four-fifths">
        <p><b>More appointments start on time with shorter lines in the office.</b></p>
        <p>Experience better patient flow and minimize community spread in packed waiting rooms.</p>
      </div>
  </div>

      
  </div>
</div>
<!-- End Intake Block -->

<!-- Health Records for iphone -->
<div class="container no-pad background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/two_tone_holding_iphone_for_abigail.jpg);">
    <div class="container__centered">
      <div class="container__one-third">
        &nbsp;
      </div>
        <div class="container__two-thirds text--white" style="padding:2em;">
            <h2>Empower Your Community with Health Records on iPhone<sup>&reg;</sup></h2>
            <p>Give your community access to their medications, allergies, test results, and other important information by syncing the MEDITECH portal with <a href="https://ehr.meditech.com/ehr-solutions/health-records-on-iphone"> Health Records on iPhone.</a>
             One aggregated record, accessible via the Health app on iPhone, simplifies data access for your community by enabling them to:
            </p>
            <ul>
              <li>View their health data across multiple participating organizations in one aggregated record.</li>
              <li>Combine their health record data with data captured on their own fitness trackers and remote monitoring devices.</li>
              <li>Have immediate access to medications, allergies, test results, procedures, conditions, immunizations, and vitals.</li>
              <li>Present key elements of their medical history to their providers, filling gaps in their records and guiding safer treatment decisions.</li>
              <li>Receive notifications when new records become available.</li>
            </ul>
        </div>
    </div>
</div>
<!-- Health Records for iphone -->

<!-- Start of KDMC block -->
<div class="content__callout border-none" style="background-color: #E6E9EE !important;">
    <div class="content__callout__media">
        <div class="content__callout__image-wrapper" style="padding:3em !important;">
            <div class="video js__video video--shadow" data-video-id="383517385">
                <figure class="video__overlay">
                    <img src="https://ehr.meditech.com/sites/default/files/styles/video-overlay-breakpoints_theme_meditech_big-medium_2x/public/images/video/KDMC-patient-satisfaction-video--video-overlay.jpg?itok=QhpjY0GQ&timestamp=1579291333" alt="KDMC Patient Satisfaction">
                </figure>
                <a class="video__play-btn" href="https://vimeo.com/383517385?&autoplay=1"></a>
                <div class="video__container"></div>
            </div>
        </div>
    </div>
    <div class="content__callout__content">
        <div class="content__callout__body">
            <div class="content__callout__body__text">
                <h2>Quality interactions for an improved patient experience.</h2>
                <p>Clinicians spend more quality time with patients, and patients are spending less time waiting for care. The result: a more improved patient experience.</p>
                <p><i>“Every patient just wants to feel like you really hear them… The more time you’re spending with the patient the happier they are and the more well taken care of they feel and that makes our job better at the end of the day.”</i></p>
                <p>
                Taylor Berry, RN<br>
                Clinical Applications Specialist, King’s Daughters Medical Center
                </p>
            </div>
        </div>
    </div>
</div>
<!-- End of KDMC block -->

<!-- Start Patient Scenarios -->
<div class="container background">
    <div class="container__centered" style="padding-bottom: 90px;">
      <h2 class="text--emerald" style="padding-bottom: 20px;">Engage chronic disease patients with Expanse.</h2>
        <div class="container__one-third center" style="padding-right: 30px;">
          <div>
            <figure>
                <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Teresa_patient_series_2.jpg" alt="population analytics" style="margin-bottom: 1em; width: 400px;">
            </figure>
          </div>  
        </div>
          
        <div class="container__two-thirds">
            <ul>
              <li>Teresa has been diagnosed with CHF. She's discharged from the hospital with a Bluetooth-enabled scale connected to MEDITECH's Patient and Consumer Health Portal.</li>
              <li>She <a href="https://vimeo.com/358106877" target="_blank">shares access to the portal</a> with her daughter, Angela, who is helping her to adjust to life with a chronic condition. Teresa weighs herself daily as part of her CHF care plan.</li>
              <li>The care manager in her cardiologist's office uses <a href="https://info.meditech.com/meditechs-building-a-foundation-for-population-health-patient-registries-white-paper">registries</a> to track Teresa and other CHF patients, and can follow up if necessary.</li>
              <li>Teresa has changed her eating habits for the better, because she knows her care manager is monitoring her.</li>
            </ul> 
          </div>
        </div>

  <div class="container__centered" style="padding-bottom: 90px;">
    <h2 class="text--emerald" style="padding-bottom: 20px;">Convenience is key for engaging busy consumers.</h2>
    
          <div class="container__two-thirds" style="padding-right: 30px;">
            <ul>
              <li>Angela's daughter, Isabella, has a rash. The busy mom wants to take her child to a pediatrician, but she doesn't have time to wait for hours in an urgent care clinic.</li>
              <li>Instead, she schedules a virtual visit with Isabella's pediatrician.</li>
              <li>Angela completes a pre-visit questionnaire, pre-registers her daughter, and pays the copay via the portal.</li>
              <li>While sitting in their kitchen, Angela and Isabella can launch the visit directly from MEDITECH's Patient and Consumer Health Portal.</li>
              <li>The pediatrician documents the virtual visit in Isabella's chart. Upon noticing that Isabella was recently prescribed amoxicillin for strep, the pediatrician diagnoses the rash as an allergic reaction.</li>
              <li>She documents the allergy and e-prescribes Omnicef&reg; instead.</li>
            </ul> 
          </div>
          <div class="container__one-third center">
          <div>
            <figure>
                <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Isabella_patient_series.jpg" alt="population analytics" style="margin-top: 1em; width: 400px;">
            </figure>
          </div>  
        </div>
    </div>
  


    <div class="container__centered">
      <h2 class="text--emerald" style="padding-bottom: 20px;">Mobile capabilities keep people engaged, wherever they are.
      </h2>
        <div class="container__one-third center" style="background-color: rgba(7, 79, 65, 0.0); padding-right: 30px;">
          <div>
            <figure>
                <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Angela_patient_series.jpg" alt="population analytics" style="margin-bottom: 1em; width: 400px;">
            </figure>
          </div>  
        </div>
          
        <div class="container__two-thirds" style="background-color: rgba(7, 79, 65, 0.0);">
            <ul>
              <li>Angela receives an email reminder from her doctor that she's overdue for her annual mammogram.</li>
              <li>Via <a href="https://ehr.meditech.com/news/mhealth-meditech-in-the-app-store">MHealth</a>, Angela schedules a mammogram at a time that fits her schedule.</li>
              <li>While in the app, she notices an outstanding balance for Isabella's, and her own, prior visits. She pays both balances online and clears her account.</li>
              <li>Angela does all this in five minutes via MHealth — no need to call radiology for an appointment or mail a check.</li>
            </ul> 
          </div>
        </div>
  </div>

<!-- End of Patient Scenarios -->

<!-- Start of Interoperability Block -->
<div class="container no-pad background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/medical_mapping3.jpg);">
    <div class="container__centered bg-overlay--black">
        <div class="container__two-thirds text--white" style="padding:2em;">
            <h2>Keep your patients and their records connected.</h2>
            <p>There are many different routes that a patient and their records can travel. But no matter what direction your organization wants to go, MEDITECH can help you get there, with <a href="https://ehr.meditech.com/ehr-solutions/meditech-interoperability"> interoperability efforts</a> that include:</p>
            <ul>
              <li>Local affiliations and regional HIEs.</li>
              <li>All connected practitioners using <a href="https://www.commonwellalliance.org/"> CommonWell Health Alliance&reg;.</a></li>
              <li>The opportunity to connect with Carequality via Commonwell Health Alliance, supporting the exchange of member records with nearly all major EHR vendors.</li>
            </ul>
        </div>
    </div>
</div>
<!-- End of Interoperability -->


</div>
<!-- end js__seo-tool__body-content -->


<!-- Block 9 -->
<div class="container bg--light-gray" style="padding:6em 0;">
  <div class="container__centered center">
       
      <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
        <h2>
          <?php print $cta->field_header_1['und'][0]['value']; ?>
        </h2>
      <?php } ?>

      <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
        <div>
          <?php print $cta->field_long_text_1['und'][0]['value']; ?>
        </div>
      <?php } ?>

      <div class="center" style="margin-top:2em;">
        <?php hubspot_button($cta_code, "Watch the Advancing Patient Engagement Webinar"); ?>
      </div>

      <div style="margin-top:1em;">
        <?php print $share_link_buttons; ?>
      </div>

  </div>
</div>
<!-- End of Block 9 -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>
<!-- END campaign--node-2845.php -->
