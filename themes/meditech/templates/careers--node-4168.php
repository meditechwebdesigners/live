<!-- start career-page--node-2438.php template CAREERS: Veteran's Page -->
<?php
$url = $GLOBALS['base_url']; // grabs the site url
?>
<!-- start career-page--node-2438.php template CAREERS: Veteran's Page -->
<style>
    .video--loop-container {
        min-height: 450px;
        max-height: 650px;
    }

    @media (max-width: 800px) {
        .video--loop-container {
            min-height: 480px;
        }
    }

    .video--loop-container video {
        /* Make video to at least 100% wide and tall */
        min-width: 100%;
        min-height: 100%;

        /* Setting width & height to auto prevents the browser from stretching or squishing the video */
        width: auto;
        height: auto;

        /* Center the video */
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }

    .video--title {
        max-width: 1152px;
        margin-left: auto;
        margin-right: auto;
        padding-left: 0em;
        padding-right: 0em;
        position: absolute;
        top: 10%;
        bottom: 0;
        right: 0%;
        text-align: left;
        color: #fff;
        z-index: 3;
    }

    .video {
        padding-bottom: 55.56098%;
        max-width: 100%;
        margin-bottom: 1em;
    }

    .video .video__play-btn {
        font-size: 4.25em;
        height: 53px;
        width: 43px;
        line-height: 53px;
    }

    @media all and (max-width: 50em) {
        .video .video__play-btn {
            font-size: 5.25em;
            height: 45px;
            width: 45px;
            line-height: 45px;
        }
    }

    @media (max-width: 87.5em) {
        .video--title {
            right: -10%;
        }
    }

    @media (max-width: 50em) {
        .video--title {
            text-align: center;
            left: 6%;
            padding-left: 2em;
            padding-right: 8em;
        }

        .video--loop-container {
            min-height: 375px;
        }

        .grid-item--white {
            border: none !important;
        }
    }

    .transparent-overlay--white {
        padding: 1em;
        background-color: rgba(255, 255, 255, 0.8);
        min-height: 5em;
    }

    .grid-item--white {
        float: left;
        padding: 0em 1em 0em 0em;
        text-align: center;
        border-right: 1px solid #ffffff;
        height: 18em;
    }

    .grid-item-last--white {
        float: left;
        padding: 0em 0em 0em 0em;
        text-align: center;
        height: 18em;
    }

</style>
<!-- Hero Block -->
<div class="video--loop-container">
    <video class="video--loop" preload="auto" autoplay loop muted playsinline>
        <source src="https://player.vimeo.com/external/273583648.hd.mp4?s=6e71ccd5ce08314c5930554907df2491382c57dd&profile_id=174">
    </video>
    <div class="container__centered video--title text--white">
        <div class="container__two-thirds transparent-overlay center">
            <div class="text-shadow--black">
                <h1>Welcome Veterans</h1>
            </div>
            <h2>"You completed your mission. Now join ours."</h2>

            <div class="btn-holder--content__callout no-margin--top">
                <div class="center"><a href="https://ehr.meditech.com/careers/job-listings" class="btn--orange">See Our Current Career Opportunities</a></div>
            </div>
        </div>
    </div>
</div>
<!-- End of Hero Block -->

<!-- Block 2 -->
<div class="container bg--green-gradient content__callout">
    <div class="container__centered text--white">
        <div class="page__title--center auto-margins">
            <div class="container__one-third grid-item--white">
                <h2>MEDITECH’s<br>Mission</h2>
                <p>To enable customers to provide higher quality care, with greater efficiency, to more people, at a lower cost.</p>
            </div>
            <div class="container__one-third grid-item--white">
                <h2>MEDITECH’s<br>Vision</h2>
                <p>A world where every person can access their health information and participate in their care and every organization can deliver safe, quality care to their community through instant access to records, knowledge, and data.</p>
            </div>
            <div class="container__one-third grid-item-last--white">
                <h2>MEDITECH’s<br>Goal</h2>
                <p>To empower customers to deliver progressive care through the use of our cutting-edge technology. We're here to work with people and organizations everywhere to meet their goals and advance healthcare toward a sustainable future.</p>
            </div>
        </div>
    </div>
</div>
<!-- Block 2 -->

<!-- Block 3 Quote -->
<div class="container bg--white">
    <article class="container__centered">
        <figure class="container__one-fourth center">
            <img class="quote__content__img" src="<?php print $url; ?>/sites/all/themes/meditech/images/careers/EvHazel.png" onerror="this.src='https://ehr.meditech.com/sites/all/themes/meditech/images/quote.png';this.onerror=null;">
        </figure>
        <div class="container__three-fourths">
            <!-- Going to need to come back to this with Sean, he mentioned using Javascript to make it look like it does in the mock up-->
            <div class="quote__content__text text--large">
                <p>"MEDITECH is an excellent fit for veterans. As an Air Force veteran, I have always valued the dedication to the mission, the strength of the company leadership, and the respect that the company shows to its employees. Our use of cutting-edge technologies and commitment to customer service excellence are examples of the forward thinking and compassion of the MEDITECH team."</p>
                <p><strong>Ev Hazel, Associate Vice President</strong><br>
                    US Air Force, 1978-1988, Tech Sergeant</p>
            </div>
        </div>
    </article>
</div>
<!-- End Block 3 -->


<!-- START New Video Block -->
<div class="container bg--black-coconut">
    <div class="container__centered">
        <h2 class="center">What MEDITECH's Veterans Want to Share With You</h2>
        <p class="center">Hear from staff members about their experiences with joining MEDITECH after a career in the military.</p>

        <div class="container">
            <div class="container__one-half">
                <div class="video js__video" data-video-id="498351561">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/careers/video-overlay--Neil-Allen.jpg" alt="Veteran Neil Allen video covershot">
                    </figure>
                    <a class="video__play-btn" href="https://vimeo.com/498351561"></a>
                    <div class="video__container"></div>
                </div>
                <h3 class="text--meditech-green" style="margin-bottom: .25em;">Neil Allen</h3>
                <p class="no-margin--bottom">US Navy/Machinist Mate First Class</p>
                <p>MEDITECH Software Developer</p>
            </div>

            <div class="container__one-half">
                <div class="video js__video" data-video-id="498351375">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/careers/video-overlay--Leroy-Krueger.jpg" alt="Veteran Leroy Krueger video covershot">
                    </figure>
                    <a class="video__play-btn" href="https://vimeo.com/498351375"></a>
                    <div class="video__container"></div>
                </div>
                <h3 class="text--meditech-green" style="margin-bottom: .25em;">Leroy Krueger</h3>
                <p class="no-margin--bottom">US Army Reserves/Specialist</p>
                <p>MEDITECH Supervisor Development</p>
            </div>
        </div>

        <div class="container no-pad">
            <div class="container__one-half">
                <div class="video js__video" data-video-id="508554750">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/careers/video-overlay--Dean-Lamsa.jpg" alt="Veteran Dean Lamsa video covershot">
                    </figure>
                    <a class="video__play-btn" href="https://vimeo.com/508554750"></a>
                    <div class="video__container"></div>
                </div>
                <h3 class="text--meditech-green" style="margin-bottom: .25em;">Dean Lamsa</h3>
                <p class="no-margin--bottom">US Army/Specialist 5</p>
                <p>MEDITECH Project Manager Strategic Projects</p>
            </div>

            <div class="container__one-half">
                <div class="video js__video" data-video-id="498351764">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/careers/video-overlay--Chris-Reece.jpg" alt="Veteran Chris Reece video covershot">
                    </figure>
                    <a class="video__play-btn" href="https://vimeo.com/498351764"></a>
                    <div class="video__container"></div>
                </div>
                <h3 class="text--meditech-green" style="margin-bottom: .25em;">Chris Reece</h3>
                <p class="no-margin--bottom">US Navy/Lieutenant</p>
                <p>MEDITECH Supervisor Development</p>
            </div>
        </div>

        <div class="container no-pad--bottom">
            <div class="container__one-half">
                <div class="video js__video" data-video-id="498352302">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/careers/video-overlay--Ron-Rora.jpg" alt="Veteran Ron Rora video covershot">
                    </figure>
                    <a class="video__play-btn" href="https://vimeo.com/498352302"></a>
                    <div class="video__container"></div>
                </div>
                <h3 class="text--meditech-green" style="margin-bottom: .25em;">Ron Rora</h3>
                <p class="no-margin--bottom">US Army/Sergeant</p>
                <p>MEDITECH Specialist Client Services</p>
            </div>

            <div class="container__one-half">
                <div class="video js__video" data-video-id="498352002">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/careers/video-overlay--Joseph-Sullivan.jpg" alt="Veteran Joseph Sullivan video covershot">
                    </figure>
                    <a class="video__play-btn" href="https://vimeo.com/498352002"></a>
                    <div class="video__container"></div>
                </div>
                <h3 class="text--meditech-green" style="margin-bottom: .25em;">Joseph Sullivan</h3>
                <p class="no-margin--bottom">US Navy/Logistics</p>
                <p>MEDITECH Supervisor Client Services</p>
            </div>
        </div>

    </div>
</div>
<!-- END New Video Block -->


<!-- Block 5 -->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/careers/military-personnel-working-on-multiple-monitors.jpg); padding: 6em 0;">
    <div class="container__centered">
        <div class="page__title--center">
            <div class="text--white">
                <h2>Put your military skills to work</h2>
            </div>
            <div class="container__one-third transparent-overlay--white">
                <h3>Do you enjoy public speaking and traveling?</h3>
                <p>Sales Representative, Marketing Solutions Specialist, Client Training Specialist</p>
            </div>
            <div class="container__one-third transparent-overlay--white">
                <h3>Are you a tech guru and enjoy learning new technology?</h3>
                <p>Programming, Software Developer, Software Tester - QC</p>
            </div>
            <div class="container__one-third transparent-overlay--white">
                <h3>Are you a hands on problem solver?</h3>
                <p>Client Support Specialist, Building Operations Specialist</p>
            </div>
        </div>
    </div>
</div>
<!-- End Block 5 -->

<!-- Block 6 -->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/careers/person-proofreading-with-red-pen.jpg);">
    <div class="container__centered transparent-overlay" style="padding: 1em 3em;">
        <div class="page__title text--white">
            <h2>Resume Mistakes to Avoid</h2>
            <p>In many cases, your resume is the first contact you’ll make with employers. It’s important to make a good first impression. Your resume provides a snapshot of your skills, achievements, and professional background. It is also the first display of your communication skills.</p>
            <div class="container__one-half">
                <ul>
                    <li>Review for typos or grammatical errors</li>
                    <li>Use action verbs to describe work history (led a squadron, managed supply levels)</li>
                    <li>List current contact information</li>
                    <li>Include information relevant to the job (don't include personal details such as birthdate, marital status)</li>
                    <li>Provide a timeline of work history, accounting for any classified assignments</li>
                </ul>
            </div>
            <div class="container__one-half">
                <ul>
                    <li>Include any military coursework in the Education portion of your resume (use civilian terms to describe the course or school)</li>
                    <li>Display information in a standard font that will not distract from the content</li>
                    <li>Explain industry/company specific language (ie: spell out acronyms that were common in your assignment)</li>
                    <li>Use a professional email address</li>
                    <li>Utilize civilian language instead of military-specific jargon</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End Block 6 -->


<!-- Block 8 -->
<div class="container bg--blue-gradient">
    <div class="container__centered" style="padding: 1em 3em;">
        <div class="page__title text--white">
            <div class="container" style="padding-top:0em; padding-bottom:0em;">
                <div class="center">
                    <h2>Questions You May Hear in an Interview</h2>
                </div>
                <div class="container__two-thirds">
                    <p>When interviewing, you will be asked questions that will delve into both your hard and soft skills. Thinking about these skills and your experiences ahead of time can help you to provide a solid answer to these types of queries.</p>
                </div>
                <div class="container__one-third center">
                    <img style="max-width:50%;" src="<?php print $url; ?>/sites/all/themes/meditech/images/careers/Icon--Question-Mark.png" alt="Question Mark Icon">
                </div>
            </div>

            <div class="container__one-half">
                <ul>
                    <li>What motivates you? (Answer with specifics, not ‘because it was an order’)</li>
                    <li>How would you describe your ideal work environment? (Be specific and draw on parallels to the company with which you are interviewing)</li>
                    <li>What attracts you to this industry? (Draw on past experience and tie that to the industry you are currently exploring)</li>
                    <li>What is the most valuable skill you bring to this position? (Draw on military experience, but use civilian terms to explain)</li>
                    <li>If I were to call your previous superior how would he/she describe you? (Be honest and think about reviews with past superiors)</li>
                    <li>What is your dream job? (Be realistic when answering and if you do not have a specific title in mind, share the qualities of your ideal role)</li>
                </ul>
            </div>
            <div class="container__one-half">
                <ul>
                    <li>Share a challenge that you faced and how you worked through this challenge. (Highlight a specific assignment or task, but be sure to break it down for a civilian audience)</li>
                    <li>What else do I need to know about you, that I have not asked, that will help me to determine if you are a fit for the role? (Describing confidential assignments can be challenging - think about this kind of answer ahead of time)</li>
                    <li>What concerns do you have with transitioning from a military to a civilian role? (Be honest and share hesitations you may have so those can be addressed, but also end your response on a positive note)</li>
                    <li>What are the characteristics you want to see in your supervisor? (While you may feel you can work with any management style, take the time to reflect on this and think about past situations where you have seen the most success)</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End Block 8 -->
<!-- end career-page--node-2438.php template -->
