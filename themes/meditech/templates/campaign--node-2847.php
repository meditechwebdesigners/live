<!-- START campaign--node-2847.php -->
<?php // This template is set up to control the display of the COMMONWELL CONNECTED

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');
$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>


<style>
  @media all and (max-width: 50em) {
    .content__callout__content {
      padding-left: 6%;
      color: #3e4545;
      padding-top: 0em;
    }

    .video {
      max-width: 100%;
    }
  }

  .video-iframe {
    max-width: 84.5%;
    position: relative;
    padding-bottom: 56.25%;
    overflow: hidden;
    margin: 0 auto;
    border-radius: 6px;
    box-shadow: 7px 8px 26px rgba(0, 0, 0, 0.1);
  }

  .video-iframe iframe {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }

  @media all and (max-width: 50em) {
    .video-iframe {
      max-width: 100%;
    }
  }

</style>

<!-- Block 1 for Commonwell -->

<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/male_doctor_working_on_ipad.jpg);">
  <div class="container__centered">
    <div class="container__one-third center" style="padding-right: 30px;">
      <div>
        <figure>
          <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/CommonWell_Connected_Seal_LightBlue.png" alt="population analytics" style="padding-top: 2em; margin-bottom: 1em; width: 400px;">
        </figure>
      </div>
    </div>

    <div class="container__two-thirds transparent-overlay text--white" style="background-color: rgba(0, 113, 143, 0.4);">
      <h2 class="text--white" style="padding-bottom: 20px;">Care comes together with MEDITECH Expanse and CommonWell Connected™.
      </h2>

      <div>
        <p> MEDITECH is committed to truly interoperable care, where both clinicians and patients benefit from the seamless sharing of information across the entire continuum. As a <a href="https://www.commonwellalliance.org/how-to-participate/alliance-members/">CommonWell Connected™ Member</a>, MEDITECH ensures that health information is free-flowing across clinicians and networks, enabling a “frictionless” coordinated care experience for everyone.
        </p>
        <p>
          With Expanse, clinicians get the whole patient story as well as real-time access to all records embedded right within their daily workflows — even when care occurs outside your typical network. With the additional bi-directional exchange between CommonWell and Carequality, our system can share member records with all major EHR vendors, including those that primarily serve large academic medical centers.
        </p>
        <p>
          There is no one right way to achieve interoperability, but no matter what route suits your organization, MEDITECH and CommonWell can get you there.
        </p>
      </div>

    </div>
  </div>
</div>

<!-- End of Block 1 for Commonwell -->


<!-- Block 2 - Video -->
<div class="content__callout border-none" style="background-color: #E6E9EE !important;">
  <div class="content__callout__media">
    <div class="content__callout__image-wrapper" style="padding:3em !important;">
      <div class="video js__video video--shadow" data-video-id="378330075">
        <figure class="video__overlay">
          <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Mike-Cordeiro-CommonWell--video-overlay.jpg" alt="Mike Cordeiro: MEDITECH Expands Interoperability with CommonWell">
        </figure>
        <a class="video__play-btn" href="https://vimeo.com/378330075"></a>
        <div class="video__container"></div>
      </div>
    </div>
  </div>
  <div class="content__callout__content">
    <div class="content__callout__body">
      <div class="content__callout__body__text">
        <h2> Share information, save money, and improve care coordination. </h2>
        <p>
          In addition to enhancing efficiency, MEDITECH and <a href="https://www.commonwellalliance.org">CommonWell Health AllianceⓇ</a> can improve your bottom line by cutting down on redundant patient testing as well as costly system interfaces. MEDITECH customers benefit from the following <a href="https://www.commonwellalliance.org/wp-content/uploads/2019/01/CommonWell-Overview-Flier-February-2019.pdf">features available through CommonWell:</a>
        </p>
        <ul>
          <li>
            <p>
              Patient Identification and Linking
            </p>
          </li>
          <li>
            <p>
              Record Locator Service
            </p>
          </li>
          <li>
            <p>
              Data Broker (Query and Retrieval)
            </p>
          </li>
          <li>
            <p>
              Trust Fabric (Privacy and Security)
            </p>
          </li>
        </ul>

        <div class="center">
          <p style="margin-bottom:.5em;"><strong>
              For more details, MEDITECH customers should check out our
            </strong></p>
          <a href="https://home.meditech.com/en/d/commonwell/homepage.htm" role="button" class="btn--emerald" style="margin-bottom:2em;" title="resource page">CommonWell Resource page</a>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End of Block 2 - Video -->


<!-- New Block 2  move the button up just a hair from the bottom-->

<!--
<div class="content__callout border-none" style="background-color: #E6E9EE !important;">
  <div class="content__callout__media">
    <div class="content__callout__image-wrapper" style="padding:3em !important;">
      <div class="video-iframe">
        <iframe width="500" height="315" src="https://www.youtube.com/embed/7Ywt6vz8JxA" frameborder="0" allowfullscreen></iframe>
      </div>
    </div>
  </div>
  <div class="content__callout__content">
    <div class="content__callout__body">
      <div class="content__callout__body__text">
        <h2> Share information, save money, and improve care coordination. </h2>
        <p>
          In addition to enhancing efficiency, MEDITECH and <a href="https://www.commonwellalliance.org">CommonWell Health AllianceⓇ</a> can improve your bottom line by cutting down on redundant patient testing as well as costly system interfaces. MEDITECH customers benefit from the following <a href="https://www.commonwellalliance.org/wp-content/uploads/2019/01/CommonWell-Overview-Flier-February-2019.pdf">features available through CommonWell:</a>
        </p>
        <ul>
          <li>
            <p>
              Patient Identification and Linking
            </p>
          </li>
          <li>
            <p>
              Record Locator Service
            </p>
          </li>
          <li>
            <p>
              Data Broker (Query and Retrieval)
            </p>
          </li>
          <li>
            <p>
              Trust Fabric (Privacy and Security)
            </p>
          </li>
        </ul>

      </div>


    </div>

    <div class="container__centered center">

      <p style="margin-bottom:.5em;"><strong>
          For more details, MEDITECH customers should check out our
        </strong></p>
      <a href="https://home.meditech.com/en/d/commonwell/homepage.htm" role="button" class="btn--emerald" style="margin-bottom:2em;" title="resource page">CommonWell Resource page</a>

    </div>
  </div>
</div>
-->

<!-- New Block 2 End -->

<!-- Block 3 No quotes graphic here, they would like them-->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dark-blurred-abstract-building-background.jpg);">
    <article class="container__centered text--white center auto-margins">
      <div class="text--large italic" style="font-size:1.4em; padding-top: .75em;">
        <p>"We’re excited to be at the forefront of interoperability breakthroughs and technical innovation in healthcare. Even more importantly, we’re excited to bridge gaps in patient care through connection to CommonWell and deliver the best outcomes possible for our community."</p>
      </div>
      <p class="no-margin--bottom text--large bold text--meditech-green">David Shroades, COO</p>
      <p>Aultman Alliance Community Hospital</p>
    </article>
  </div>
<!-- End of Block 3 -->

<!-- Block 3 No quotes graphic here, they would like them-->
<!--

<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/white-x-texture.jpg);">
  <article class="container__centered">
    <figure class="container__one-fourth center">
      <img class="quote__content__img" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/quote.svg" onerror="this.src='https://ehr.meditech.com/sites/all/themes/meditech/images/quote.svg';this.onerror=null;" alt="quote bubble graphic">
    </figure>
    <div class="container__three-fourths">
      <div class="quote__content__text text--large">
        <p>
          "We’re excited to be at the forefront of interoperability breakthroughs and technical innovation in healthcare. Even more importantly, we’re excited to bridge gaps in patient care through connection to CommonWell and deliver the best outcomes possible for our community."
        </p>
      </div>
      <p class="text--large no-margin--bottom">
        David Shroades, COO
      </p>
      <p>
        Aultman Alliance Community Hospital
      </p>
    </div>
  </article>
</div>
-->
<!-- End of Block 3 -->

<!-- Block 4 -->
<div class="content__callout border-none">
  <div class="content__callout__media">
    <div class="content__callout__image-wrapper">
      <div class="js__video video-iframe">
        <iframe width="500" height="315" src="https://www.youtube.com/embed/3Kj6ZuUUZKY" frameborder="0" allowfullscreen></iframe>
      </div>
    </div>
  </div>
  <div class="content__callout__content">
    <div class="content__callout__body">
      <div class="content__callout__body__text">
        <h2>Checking in on the future of interoperability</h2>
        <p>CommonWell TV talks with MEDITECH AVP Christine Parent about FHIR and the direction of interoperability post-pandemic.</p>
        <p>Parent highlights:</p>
        <ul>
        	<li><p>What the new direction with FHIR means for providers and patients.</p></li>
        	<li>
        		<p>How having access to discrete data will save providers time by making information more readily available and actionable.</p>
        	</li>
        	<li><p>How data quality, as it relates to discrete data, can impact health equity in addition to improving data exchange.</p></li>
        	<li><p>Her vision of what interoperability may look like in 2030.</p></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- End of Block 4 -->

<!-- Block 5 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dark-blurred-male-using-computer.jpg);">
    <article class="container__centered text--white center auto-margins">
      <figure>
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Jeff-Allport-Headshot.png" alt="Jeff Allport headshot" style="width:150px;">
      </figure>
      <div class="text--large italic" style="font-size:1.4em; padding-top: .75em;">
        <p>"Out of all our interoperability options, CommonWell comes closest to what ONC and CMS envisioned and requires the least development effort...I’m thrilled the discussion has transformed from focusing on how to get the information to how to present and manage the information we’re getting."</p>
      </div>
      <p class="no-margin--bottom text--large bold text--meditech-green">Jeff Allport, CIO and VP</p>
      <p>Valley Presbyterian Hospital</p>
    </article>
  </div>
<!-- End of Block 5 -->

<!-- Block 6 -->

<div class="container no-pad background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/bridging_gaps.jpg);">
  <div class="container__centered bg-overlay--black">
    <div class="container__one-third text--white" style="padding:4em 2em;">
      <h2>See the results for yourself.</h2>
      <p>Interoperable care isn’t just a goal for the future; it’s here right now. <a href="https://ehr.meditech.com/ehr-solutions/meditech-interoperability?hsCtaTracking=3da3b14e-6ae1-4686-a184-f2d294f7b3a0%7C13c83314-d9db-469a-b637-504ff6ff7f6f">Learn how </a>healthcare organizations are using MEDITECH’s EHR to improve care delivery through information sharing, including a <a href="https://vimeo.com/meditechehr/review/311922713/cf7cd85c67">video</a> and <a href="https://info.meditech.com/case-study-union-hospital-improves-care-efficiency-with-multiple-health-information-exchange-strategy-0">case study.</a></p>
    </div>
  </div>
</div>

<!-- End of Block 6 -->


<!-- Block 7 -->
<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/X-background-white-left.png); background-color:#d7d1c4; padding:5.5em;">
  <div class="container__centered center">

    <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
    <h2>
      <?php print $cta->field_header_1['und'][0]['value']; ?>
    </h2>
    <?php } ?>

    <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
    <div>
      <?php print $cta->field_long_text_1['und'][0]['value']; ?>
    </div>
    <?php } ?>

    <div class="center" style="margin-top:2em;">
      <?php hubspot_button($cta_code, "Sign Up For The Population Health Insights Webinar"); ?>
    </div>

    <div style="margin-top:1em;">
      <?php print $share_link_buttons; ?>
    </div>

  </div>
</div>
<!-- End of Block 7 -->

<!-- END campaign--node-2847.php -->
