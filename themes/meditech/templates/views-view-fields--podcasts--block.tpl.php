<?php // This template is for each row of the Views block: PODCASTS ....................... 
  $url = $GLOBALS['base_url']; // grabs the site url
?>

<!-- start views-view-fields--podcasts--block.tpl.php template -->
<section class="article--card">

    <figure class="article--hero bg--dark-blue-gradient text--white">
        <div class="article--header">
            <div class="hero-text">
                <p class="header-micro"><?php print $fields['field_podcast_topic']->content; ?></p>
                <h2 class="header-three no-margin--bottom"><?php print $fields['title']->content; ?></h2>
            </div>
            <div class="hero-image">
                <?php print $fields['field_podcast_image']->content; ?>
            </div>
        </div>
    </figure>

    <div class="article--info">
        <?php // add Edit Video link...
          if( user_is_logged_in() ){ 
            print '<div style="float:right;"><span style="font-size:12px;">'; print l( t('Edit This Series'),'node/'. $fields['nid']->content .'/edit' ); print "</span></div>"; 
          } 
        ?>
        <h3>Special Guest(s):</h3>
        <?php print $fields['field_long_text_2']->content; ?>

        <div class="episode--card bg--light-gray">
            <?php
        $node_data = node_load($fields['nid']->content);
        foreach($node_data->field_embed_codes['und'] as $key => $entity_id){
            $fc_data = entity_load('field_collection_item', array($entity_id['value']));
            foreach($fc_data as $fc_object){
                $wrapper = entity_metadata_wrapper('field_collection_item', $fc_object);
                $episode_title = $wrapper->field_header_1->value();
                $embed_code = $wrapper->field_embed_code_1->value();
                $share_link = $wrapper->field_header_2->value();
                print '<p class="no-margin--bottom bold">'.$episode_title.'</p>';
                 print '<p class="text--small">'.$share_link.'</p>';
                print '<iframe  style="margin-bottom:1em;" height="52px" width="100%" frameborder="no" scrolling="no" seamless src="https://player.simplecast.com/'.$embed_code.'?dark=false"></iframe>';
               
            }
        }
        ?>
        </div>


        <?php
        // display additional resources if any...
        if($fields['field_long_text_1']->content){
            print '<h3>Additional Resources</h3>';
            print $fields['field_long_text_1']->content;
        }
        ?>
    </div>
</section>
<!-- end views-view-fields--podcasts--block.tpl.php template -->
