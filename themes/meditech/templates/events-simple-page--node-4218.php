<!-- START events-simple-page--node-4218.php -->
<?php // This template is set up to control the display of the MEDITECH at ViVE22

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');
$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
  .block-1-artwork {
    float: right;
    margin: 2em 0 0 0;
    width: 70%;
  }

  .vive {
    width: 30%;
  }

  @media all and (max-width: 600px) {
    .block-1-artwork {
      display: none;
    }

    .vive {
      width: 100%;
    }
  }

  .shadow-box.w-headshot {
    padding: 4em 2em 2em 2em;
    position: relative;
    margin-top: 50px;
  }

  .headshot {
    position: absolute;
    top: -50px;
    left: 50%;
    transform: translate(-50%);
  }

  .headshot img {
    width: 100px;
    height: 100px;
    border-radius: 50%;
  }

  .shadow-box.w-headshot.medium {
    padding: 6em 2em 2em 2em;
  }

  .headshot.medium {
    top: -75px;
  }

  .headshot.medium img {
    width: 150px;
    height: 150px;
  }

  .headshot.tweet {
    top: unset;
    bottom: -15px;
  }

  .fa-twitter:before {
    content: "\f099";
  }

  a.tweet-btn {
    height: 20px;
    padding: 5px 12px 5px 12px;
    background-color: #1d9bf0;
    color: #fff;
    border-radius: 50px;
    font-size: 12pt;
    white-space: nowrap;
    border-bottom: none;
    -webkit-transition: background 0.25s ease-in-out;
    -moz-transition: background 0.25s ease-in-out;
    transition: background 0.25s ease-in-out;

  }

  a:hover.tweet-btn {
    color: #fff;
    background-color: #0c7abf;
    border-bottom: none;
    -webkit-transition: background 0.25s ease-in-out;
    -moz-transition: background 0.25s ease-in-out;
    transition: background 0.25s ease-in-out;
  }

  a:focus-visible.tweet-btn {
    outline: 2px solid #fff;
  }

  a.tweet-btn[target="_blank"]:after {
    content: '';
  }

  @media all and (max-width: 800px) {
    .shadow-box.w-headshot:not(.medium) {
      margin-bottom: 4em;
    }
  }

  .quoter {
    line-height: 1.3em;
  }

</style>

<div class="js__seo-tool__body-content" style="background: #305b9f; background: linear-gradient(0deg, #09193d, #0d1e45, #11244e, #152956, #192f5f, #1c3567, #203b70, #234179, #274883, #2a4e8c, #2d5495, #305b9f);">

  <!-- START Block 1 -->
  <div class="container container__centered text--white">
    <h1 class="js__seo-tool__title" style="display:none;">MEDITECH at ViVE</h1>
    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/vive-22--transforming-healthcare.png" alt="" class="block-1-artwork">
    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/MEDITECH-at-ViVE-2022.png" alt="MEDITECH at ViVE March 6th to 9th 2022" class="vive">
    <h2>Driving change through innovation.</h2>
    <p>Healthcare has undergone a monumental transformation in how it's provided, accessed, and received. Now more than ever, healthcare providers need dependable, adaptable, intuitive technology that's safe, secure, and affordable for their organizations and patients.</p>

    <p>Join us for this inaugural event, powered by CHIME, the professional organization for CIOs and other senior digital health leaders, together with HLTH, the leading platform bringing together the entire health ecosystem. <strong><a href="https://vive2022.mapyourshow.com/8_0/exhview/index.cfm?selectedBooth=712">Step inside booth #712</a></strong> at #ViVE2022 to discover how <a href="https://ehr.meditech.com/ehr-solutions/meditech-expanse">MEDITECH Expanse</a> gives you the momentum to evolve.</p>
  </div>
  <!-- END Block 1 -->


  <div class="container container__centered center">
    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/GettyImages-1278560410.gif" alt="">
  </div>


  <!-- START Block 2 -->
  <div class="container container__centered">
    <div class="container__one-half text--white">
      <h2>Working together, advancing healthcare.</h2>

      <p>As a CHIME Foundation Partner, MEDITECH thrives on sharing strategies and results. Whether through annual Spring and Fall Forums, focus groups, webinars, or other events, our collaboration with CHIME provides valuable opportunities for our customers to share their successes with the CHIME community. These include:</p>
      <ul>
        <li>Several MEDITECH customers have been named to the <a href="https://ehr.meditech.com/news/meditech-customers-named-to-chimes-most-wired-2021">Digital Health Most Wired list</a>, with Avera Health (Sioux Falls, SD) one of only seven organizations nationwide to achieve level 10 status in both the <a href="https://ehr.meditech.com/ehr-solutions/meditech-acute">acute</a> and <a href="https://ehr.meditech.com/ehr-solutions/meditech-ambulatory">ambulatory</a> settings.</li>

        <li><a href="https://hcahealthcare.com/">HCA Healthcare</a> (Nashville, TN) and MEDITECH were this year's recipients of the distinguished <a href="https://ehr.meditech.com/news/hca-healthcare-and-meditech-honored-with-chime-collaboration-award">CHIME Collaboration Award</a>. This award exemplifies nearly 30 years of innovative partnership.</li>
      </ul>
    </div>
    <div class="container__one-half">
      <div class="shadow-box w-headshot medium" style="margin-top: 80px;">
        <div class="headshot medium">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/Helen-Waters.png" alt="Helen Waters head shot">
        </div>
        <p class="italic text--large">"Our customers are progressive leaders that have made a point of embracing advanced technologies to open up new possibilities for transformational change across their organizations. It's been an honor partnering with them as they continue to show the benefits a modern EHR can bring to their communities."</p>
        <p class="quoter"><strong>Helen Waters</strong><br>Executive Vice President & COO, MEDITECH<br>Foundation Board Member, CHIME</p>
        <div class="headshot tweet">
          <a href="https://twitter.com/intent/tweet?url=https%3A%2F%2Fehr.meditech.com%2Fevents%2Fmeditech-at-vive22&via=MEDITECH&text=%22Our%20customers%20are%20progressive%20leaders%20that%20have%20made%20a%20point%20of%20embracing%20advanced%20technologies%20to%20open%20up%20new%20possibilities%20for%20transformational%20change%20across%20their%20organizations.%22%20Helen%20Waters%2C%20EVP%2C%20COO%2C%20MEDITECH&hashtags=ViVE2022" class="tweet-btn" target="_blank"><i class="fab fa-twitter"></i>&nbsp; Tweet this</a>
        </div>
      </div>
    </div>
  </div>
  <!-- END Block 2 -->


  <div class="container container__centered center">
    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/GettyImages-1278560410.gif" alt="">
  </div>


  <!-- START Block 3 -->
  <div class="container container__centered text--white">
    <div class="container__one-half">
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/woman-on-phone-among-street-crowd.jpg" alt="woman on phone among street crowd" style="border-radius: 7px;">
    </div>
    <div class="container__one-half content--pad-left">
      <h2>Mobility, usability, and efficiency. That's Expanse.</h2>
      <p>Your patient population is always in motion, and an EHR should be just as mobile. See how MEDITECH Expanse allows you to personalize care and follow the patient journey throughout all care settings - whether from the clinic, in your backyard, or around the world.</p>

      <!--populate once we know the demos-->

      <div style="margin-top:2em;">
        <?php hubspot_button("19541aee-6200-4c7f-be21-4770966203f7", "Connect with us at ViVE"); ?>
      </div>
    </div>
  </div>

  <div class="container container__centered text--white center no-pad">
    <div class="container__one-third" style="padding-bottom:2em;">
      <p class="bold text--large"><a href="<?php print $url; ?>/ehr-solutions/care-compass">Expanse Care Compass</a></p>
      <p>Proactively manage your patient populations.</p>
    </div>
    <div class="container__one-third" style="padding-bottom:2em;">
      <p class="bold text--large"><a href="<?php print $url; ?>/ehr-solutions/expanse-for-physicians">Expanse for Physicians</a></p>
      <p>Focus on your patients, not your EHR.</p>
    </div>
    <div class="container__one-third" style="padding-bottom:2em;">
      <p class="bold text--large"><a href="<?php print $url; ?>/ehr-solutions/meditech-interoperability">Traverse</a></p>
      <p>Put people back at the center of healthcare.</p>
    </div>
  </div>
  <!-- END Block 3 -->



  <div class="container container__centered center">
    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/GettyImages-1278560410.gif" alt="">
  </div>


  <!-- START Block 4 -->
  <div class="container container__centered text--white">
    <div class="container__one-half content--pad-left">
      <h2>MEDITECH Customer on the ViVE Main Stage</h2>
      <p class="bold">TUESDAY, MARCH 8 | 9 A.M.</p>
      <p>Hear <a href="https://www.viveevent.com/speakers/octavio-diaz-md-mph">Octavio Diaz, MD, MPH</a>, CMO of Steward Health Care System speak during the panel session, <a href="https://www.viveevent.com/agenda"><em>How Healthy is Your Health System?</em></a> Dr. Diaz will join his peers in discussing the signs to measure strong performance in effectiveness, safety, accessibility, efficiency, revenue, patient-centeredness, and market competitiveness.</p>
    </div>
    <div class="container__one-half center">
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/Dr-Octavio-J.png" alt="woman on phone among street crowd" style="width:60%;">
    </div>
  </div>

<!-- END Block 4 -->



<div class="container container__centered center">
  <img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/GettyImages-1278560410.gif" alt="">
</div>

<!-- START CTA -->
<div class="container text--white">
  <div class="container__centered auto-margins center">
    <div style="margin-bottom:2em;">
      <?php print $share_link_buttons; ?>
    </div>
  </div>
</div>
<!-- END CTA -->


</div><!-- end js__seo-tool__body-content -->


<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>
<!-- end events-simple-page--node-4218.php template -->
