<!-- start page.tpl.php template -->
<?php
// get page URL...
$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$currentURLarray = explode('/', $currentURL);
$urlArrayCountMinusThree = count($currentURLarray) -3;
$urlArrayCountMinusTwo = count($currentURLarray) -2;
$urlArrayCountMinusOne = count($currentURLarray) -1;
$lastURLsegment = $currentURLarray[$urlArrayCountMinusOne];
$secondToLastURLsegment = $currentURLarray[$urlArrayCountMinusTwo];
$thirdToLastURLsegment = $currentURLarray[$urlArrayCountMinusThree];

$thisURLarray = explode('//', $currentURL);
$thisURL = $thisURLarray[1];

$websiteURL = $GLOBALS['base_url']; // grabs the site url
$pass_protected = strpos($currentURL, '/protected-page?');
$lastCharacter = substr($thisURL, -1);
?>

<style>
	form.gsc-search-box {
		font: inherit;
		margin-top: 0;
		margin-right: 0;
		margin-bottom: 0;
		margin-left: 0;
		width: 100%;
		position: relative;
	}

	table.gsc-search-box {
		border-style: none;
		border-width: 0;
		border-spacing: 0 0;
		width: 100%;
		margin-bottom: 0;
		margin-top: 0;
	}

	.gsc-input-box {
		border: 0;
		background: #fff;
		height: auto;
		border-color: white;
	}

	table.gsc-search-box {
		border: 0;
		margin: 0;
	}

	table.gsc-search-box tbody,
	table.gsc-search-box tr:hover>td,
	table.gsc-search-box tr:hover>th {
		background-color: inherit;
	}

	td.gsc-search-button {
		width: 20%;
		padding: 0;
	}

	table.gsc-search-box td.gsc-input

	/*,
        tbody:last-child tr:last-child>td:first-child,
        tbody:last-child tr:last-child>td:last-child */
		{
		border: 0;
		border-bottom-left-radius: 0;
		padding: 0;
	}

	/*
        tbody:last-child tr:last-child>td:first-child {
            width: 80%;
        }
        */
	table#gs_id50,
	table#gs_id51 {
		border: 1px solid #7b8888;
	}

	td#gs_tti50,
	td#gs_tti51 {
		padding: .7em 3%;
	}

	table.gsc-search-box td.gsc-search-button {
		border: 0;
	}

	table.gsc-search-box td {
		vertical-align: inherit;
	}

	table.gsc-search-box td.gsib_b {
		vertical-align: middle;
	}

	table.gsc-search-box table#gs_id50,
	table.gsc-search-box table#gs_id51 {
		margin: 0;
	}

	.gsc-search-box-tools .gsc-search-box .gsc-input {
		padding-right: 0;
	}

	.gsc-input table {
		border: none;
	}

	input.gsc-input {
		font-size: 1em;
	}

	.gsib_a {
		padding: 0;
		width: 80%;
	}

	.gsib_b {
		padding: 0;
		width: 10%;
		text-align: center;
	}

	input.gsc-search-button,
	input.gsc-search-button:hover,
	input.gsc-search-button:focus {
		border-color: #109372;
		background-color: #109372;
		background-image: none;
		filter: none;
		color: white;
	}

	input.gsc-search-button-v2 {
		width: 13px;
		height: 13px;
		padding: 6px 27px;
		min-width: 13px;
		margin-top: 2px;
	}

	input.gsc-search-button {
		font-family: inherit;
		font-size: inherit;
		font-weight: bold;
		color: #fff;
		padding: 1em 1.5em;
		margin: 0;
		height: 3.1em;
		width: 80px;
		border-radius: inherit;
		-moz-border-radius: inherit;
		-webkit-border-radius: inherit;
		border: 0;
	}

	.gcsc-find-more-on-google {
		display: none;
	}


	#page i.fa-lock:before {
		font-size: .6em;
		margin-left: .15em;
		position: relative;
		top: -.35em;
	}

	.nav__menu--large ul.menu li.last {
		margin-right: 0;
	}

	@media (max-width: 71.875em) {
		.nav__menu--large ul.menu li {
			margin-right: 1.3em;
		}
	}

</style>

<!-- HEADER ================================================================== -->
<!-- Start PRINT ONLY header content -->
<div class="print-header">
	<div class="container__centered">
		<img src="<?php print $websiteURL; ?>/sites/all/themes/meditech/images/meditech-logo.svg" alt="MEDITECH logo">
	</div>
</div>
<!-- End PRINT ONLY header content -->

<header class="header">
	<div class="container no-pad" style="margin-top:1.3em;">
		<div class="container__centered">
			<div class="container__left">
				<div class="header__logo">
					<a class="header__logo__link logo_desktop_gae" href="<?php print $websiteURL; ?>" title="Home Page">
						<img src="<?php print $websiteURL; ?>/sites/all/themes/meditech/images/meditech-logo.svg" style="width:251px;" alt="MEDITECH logo">
					</a>
				</div>
			</div>
			<div class="container__right">
				<div class="header__search">
					<form class="form__search" method="get" action="https://ehr.meditech.com/search-results" id="search-block-form" accept-charset="UTF-8">
						<div class="form-item input-group form-item-search-block-form">
							<label class="element-invisible" for="edit-search-block-form--2">Site Search </label>
							<input title="Enter the terms you wish to search for." placeholder="" class="cus-content-search-input form__search__input" type="search" name="as_q" value="">
							<button type="submit" class="cus-content-search-button">Search</button>
						</div>
					</form>
				</div>
				<div class="header__button">
					<a href="https://home.meditech.com/en/d/customer/" role="button" class="btn--orange customers_button_gae" title="Customers">Customers</a>
				</div>
			</div>
		</div>
	</div>
</header>
<!-- End of HEADER -->

<!-- STICKY NAVIGATION ================================================================== -->
<nav class="nav js-nav" role="navigation" aria-labelledby="sticky-nav">
	<div class="container__centered" id="sticky-nav">
		<!-- Navigation Menu Button -->
		<ul class="nav__menu--small">
			<li class="nav__menu__item--menu"><a class="btn--menu sb-toggle-left menu_mobile_gae" href="#">Menu</a></li>
		</ul>
		<!-- Navigation Logos -->
		<div class="nav__logo--large">
			<a class="logo_mobile_gae" href="<?php print $websiteURL; ?>" title="Home Page">
				<img src="<?php print $websiteURL; ?>/sites/all/themes/meditech/images/meditech-logo.svg" style="width:130px;" alt="MEDITECH mobile logo">
			</a>
		</div>
		<div class="nav__logo--small">
			<a class="logo_M_gae" href="<?php print $websiteURL; ?>" title="Home Page">
				<img src="<?php print $websiteURL; ?>/sites/all/themes/meditech/images/M-Logo--2020.svg" onerror="this.src='<?php print $websiteURL; ?>/sites/all/themes/meditech/images/M-Logo--2020.png'; this.onerror=null;" style="width:37px; height:35px;" alt="MEDITECH M logo">
			</a>
		</div>
		<!-- Navigation Menu Expanded -->
		<div class="nav__menu--large header_link_gae" role="navigation" aria-labelledby="header-nav">
			<?php
          $active_solutions = '';
          $active_news = '';
          $active_events = '';
          $active_about = '';
					$active_careers = '';
          $active_contact = '';

          // need to check this since DEV server has a different folder structure...
          if($currentURLarray[3] == 'drupal'){
            $folder_name = $currentURLarray[4];
          }
          else{
            $folder_name = $currentURLarray[3];
          }

          // because contact is getting variables sent to its URL, need to split it...
          $explode_folder_name = explode('?', $folder_name);

          switch($explode_folder_name[0]){
            case 'ehr-solutions':
              $active_solutions = 'active';
              break;
            case 'news':
            case 'news-tags':
              $active_news = 'active';
              break;
            case 'events':
              $active_events = 'active';
              break;
            case 'about':
              $active_about = 'active';
              break;
            case 'contact':
              $active_contact = 'active';
              break;
            case 'global':
              $active_global = 'active';
              break;
						case 'careers':
							$active_careers = 'active';
							break;
          }
        ?>
			<ul class="menu" id="header-nav">
				<li class="first"><a class="<?php if( isset($active_solutions) ){ print $active_solutions; } ?>" href="<?php print $websiteURL; ?>/ehr-solutions">EHR Solutions</a></li>
				<li><a class="<?php if( isset($active_news) ){ print $active_news; } ?>" href="<?php print $websiteURL; ?>/news">News</a></li>
				<li><a href="https://blog.meditech.com">Blog</a></li>
				<li><a class="<?php if( isset($active_events) ){ print $active_events; } ?>" href="<?php print $websiteURL; ?>/events">Events</a></li>
				<li><a class="<?php if( isset($active_about) ){ print $active_about; } ?>" href="<?php print $websiteURL; ?>/about/meditech">About</a></li>
				<li><a class="<?php if( isset($active_global) ){ print $active_global; } ?>" href="<?php print $websiteURL; ?>/global">Global</a></li>
				<li><a class="<?php if( isset($active_careers) ){ print $active_careers; } ?>" href="<?php print $websiteURL; ?>/careers/careers-at-meditech">Careers</a></li>
				<li class="last"><a class="<?php if( isset($active_contact) ){ print $active_contact; } ?>" href="<?php print $websiteURL; ?>/contact">Contact</a></li>
			</ul>
		</div>

		<!-- Navigation - Customer Login & Search ================================= -->
		<ul class="nav__secondarymenu">
			<li class="nav__menu__item--secondary">
				<a class="js__search__button btn--search sb-close search_icon_gae" href="#" title="Search MEDITECH.com"><i class="nav__icon__search fas fa-search"></i></a>
			</li>
			<li class="nav__menu__item--secondary--last">
				<a class="btn--login customers_icon_gae" href="https://home.meditech.com/en/d/customer/" title="Customers"><i class="nav__icon__login fas fa-user"></i></a>
			</li>
		</ul>
	</div>
	<div class="container__centered">
		<!-- Search Box Dropdown -->
		<div class="js__search__container nav__search" id="js-searchbox">
			<form class="form__search" method="get" action="https://ehr.meditech.com/search-results" id="search-block-form-dropdown" accept-charset="UTF-8">
				<div class="form-item input-group form-item-search-block-form">
					<label class="element-invisible" for="edit-search-block-form--2">Site Search </label>
					<input title="Enter the terms you wish to search for." placeholder="" class="cus-content-search-input form__search__input" type="search" name="as_q" value="">
					<button type="submit" class="cus-content-search-button">Search</button>
				</div>
			</form>
		</div>
	</div>
</nav>
<!-- End of STICKY NAVIGATION -->

<!-- ============================

          BEGIN CONTENT

  ================================= -->

<!-- SLIDEOUT NAV WRAPPER -->
<div id="sb-site">

	<!-- MAIN CONTENT ================================================================== -->
	<main id="main-content" class="main" role="main">
		<!-- accessiblity link destination -->

		<div id="page">

			<?php if($messages): ?>
			<!-- HTML for status and error messages. -->
			<div class="container__centered">
				<div id="messages">
					<div class="section clearfix">
						<?php print $messages; ?>
					</div>
				</div>
			</div>
			<!-- end container__centered -->
			<?php endif; ?>


			<?php include('inc-breadcrumbs.php'); ?>


			<?php
      // if node has the Hide Page box checked...  
      if( isset($field_not_for_everyone) && $field_not_for_everyone == 1){
        $hide = 'yes';
      }
      else{
        $hide = 'no';
      }
      // if "your-eyes-only" is not present, send user to the "Not for your eyes" page...
      if( $hide == 'yes' && ( !isset($_GET['mtid']) || $_GET['mtid'] != 'your-eyes-only' ) ){
        print '<script type="text/javascript">';
        print 'window.location.replace("https://ehr.meditech.com/not-meant-for-your-eyes");';
        print '</script>';
      }
      else{ // reveal the actual page...
      ?>


			<?php // ************** START checking page URL to determine which layout goes to which section of the site **************** ?>


			<?php // if current page is a NEWS TAXONOMY page ================================================================================
        if( ($secondToLastURLsegment == 'news-tags') || ($thirdToLastURLsegment == 'news-tags') ){
      ?>

			<section class="container__centered">

				<div class="container__two-thirds">
					<?php
                $questionMark = strpos($lastURLsegment, '?');
                if($questionMark == true){
                  $lastURLsegmentArray = explode('?', $lastURLsegment);
                  $lastURLsegment = $lastURLsegmentArray[0];
                }
                if($lastURLsegment != 'c-level'){
                  if($lastURLsegment == 'meditech-in-the-press'){
                    $lastURLsegment = 'MEDITECH in the press';
                  }
                  else{
                    $lastURLsegment = str_replace('-', ' ', $lastURLsegment);
                  }
                }
              ?>
					<h1 class="page__title" style="margin-bottom:1.5em;">
						<?php print ucwords($lastURLsegment);
              $news_tags = array('press releases', 'videos', 'industry awards', 'MEDITECH in the press');
              if( !in_array($lastURLsegment, $news_tags) ){
                print ' News';
              }
              ?>
					</h1>

					<!-- start Content REGION -->
					<div class="taxonomy-results">
						<?php print render($page['content']); ?>
					</div>

					<?php print $pager; ?>

					<?php
                // Get the total number of search results...
                $totalResults = $GLOBALS['pager_total_items'][0];
                if($totalResults > 10){ // don't show Load More if less than 11 results
              ?>
					<div>
						<ul class="pager-load-more">
							<li><button class="load-more-ajax">Load more</button></li>
						</ul>
					</div>
					<?php } ?>

					<script type="text/javascript">
						(function($) {
							Drupal.behaviors.loadMoreAjax = {
								attach: function(context, settings) {
									$('.load-more-ajax', context).click(function() {
										var nextPage = $('.pager .pager-next a').attr('href');
										var lastPage = $('.pager .pager-last a').attr('href');
										$.get(nextPage, function(data) {
											$(data).find('.taxonomy-results').insertBefore($('.item-list'));
											$('.item-list .pager').remove();
											if (nextPage == lastPage) {
												$('.load-more-ajax').remove();
											} else {
												$(data).find('.item-list .pager').appendTo($('.item-list'));
												Drupal.attachBehaviors($('.item-list'));
											}
										});
									});
									$('.item-list .pager').hide();
								}
							};
						})(jQuery);

					</script>
					<!-- end Content REGION -->
				</div>
				<!-- END container__two-thirds -->

				<?php include('inc-news-sidebar.php'); ?>

			</section>

			<?php } // END if News Taxonomy Page
        
        // ****
        // ****
        // ****
        // ****
        
      // if current page is the NEWS ARTICLES WITH CTA BUTTONS PAGE ==================================================================
        else if($currentURL == $websiteURL.'/news-articles-with-cta-buttons' || $currentURL == $websiteURL.'/news-articles-with-cta-buttons/'){
      ?>

			<section class="container__centered">
				<div class="container__two-thirds">
					<h1>News Articles with CTA Buttons</h1>
					<!-- start Content REGION -->
					<?php print render($page['content']); ?>
					<!-- end Content REGION -->
				</div>
				<!-- END container__two-thirds -->
			</section>

			<?php } // END if News Articles with CTA Buttons Page

        // ****
        // ****
        // ****
        // ****
        
      // if current page is PASSWORD PROTECTED ==================================================================
      else if( $pass_protected > -1 ){
      ?>
      
      <style>
        #protected-pages-enter-password fieldset { border:0; }
      </style>
			<div class="container__centered">
        <!-- start Content REGION -->
        <?php print render($page['content']); ?>
        <!-- end Content REGION -->
			</div>

			<?php } // END if PASSWORD PROTECTED 

        // ****
        // ****
        // ****
        // ****

        // ALL OTHER pages ============================================================================================
        else { ?>

			<!-- start Content REGION -->

			<?php print render($page['content']); ?>

			<!-- end Content REGION -->

			<?php } // END if other page ?>


			<?php // END if mtid check
      }
      ?>

		</div>
		<!-- End of #Page -->
	</main>
	<!-- End of MAIN -->

	<!-- Modal Pop-up Overlay -->
	<div id="mask"></div>


	<!-- ============================

         
          END CONTENT
          

  ================================= -->


	<!-- FOOTER CONTENT ================================================================== -->

	<footer>

		<?php // START customer footer...
if($currentURLarray[3] == 'customer-materials'){
?>

		<style>
			.footer__customer ul {
				list-style: none;
				padding-left: 0;
			}

			.footer__customer ul li {
				line-height: 1.25em;
				margin-bottom: 1.25em;
			}

			#phone {
				background-color: #e6e9ee;
				font-size: 1.6em;
				text-align: center;
				padding-top: 20px;
				padding-bottom: 20px;
			}

			hr {
				margin: 1em 0;
			}

		</style>

		<hr />

		<!-- SITEMAP ================================================================== -->
		<section class="container__centered" role="navigation" aria-labelledby="footer-nav">
			<div class="footer__customer main_nav_gae" id="footer-nav">

				<div class="container__one-fourth">
					<h5>Customer Resources</h5>
					<ul>
						<li><a href="https://home.meditech.com/en/d/migrationbenefits/homepage.htm">5.x to Expanse Migration Benefits</a></li>
						<li><a href="https://customer.meditech.com/en/d/optimization/homepage.htm">5.x Optimization Assessments</a></li>
						<li><a href="https://home.meditech.com/en/d/cstrainingresources/pages/parameters.htm">Parameter Documentation</a></li>
						<li><a href="https://customer.meditech.com/en/d/6xservicegroups/pages/optimizationhomepage.htm">6.x Optimization Assessments</a></li>
						<li><a href="https://www.meditech.com/Programs/ams.exe?TYPE=OnLine.html" title="MEDITECH's Customer Support">Customer Support</a></li>
						<li><a href="https://home.meditech.com/en/d/newsroom/pages/clientservicesnewsletters.htm">Client Services Newsletters</a></li>
						<li><a href="https://home.meditech.com/en/d/clickhome/homepage.htm">Click Reduction</a></li>
						<li><a href="https://home.meditech.com/en/d/newmeditech/pages/collaborativesolutions.htm">Collaborative Solutions</a></li>
						<li><a href="https://ehr.meditech.com/">MEDITECH.com</a></li>
						<li><a href="https://customer.meditech.com/rules/ ">MEDITECH R.U.L.E.S.</a></li>
						<li><a href="https://customer.meditech.com/en/d/5xmultientity/homepage.htm">Multi-Entity Capabilities</a></li>
						<li><a href="https://home.meditech.com/en/d/newmeditech/pages/mobilityresourcescenter.htm">Mobile Devices Resource Center </a></li>
						<li><a href="https://www.meditech.com/workflowintegration/wig.htm" title="Workflow Integration Guide (5.x)">Workflow Integration Guide</a></li>
					</ul>
				</div>

				<div class="container__one-fourth">
					<h5>Best Practices</h5>
					<ul>
						<li><a href="https://customer.meditech.com/en/d/bestpracticesexp/homepage.htm">Expanse</a></li>
						<li><a href="https://customer.meditech.com/en/d/6xreadyimpl/homepage.htm">6.x</a></li>
						<li><a href="https://customer.meditech.com/en/d/bestpractices5x/pages/cshomepage.htm">Client/Server</a></li>
						<li><a href="https://customer.meditech.com/en/d/bestpractices5x/pages/magichomepage.htm">MAGIC</a></li>
						<li><a href="https://customer.meditech.com/en/d/protectednews/pages/stage67resources.htm">Stage 6 &amp; 7</a></li>
					</ul>
					<h5>Product Resources</h5>
					<ul>
						<li><a href="https://customer.meditech.com/productresource/menu/index.php?prodln=E">Expanse</a></li>
						<li><a href="https://customer.meditech.com/productresource/menu/index.php?prodln=maas">Expanse MaaS</a></li>
						<li><a href="https://www.meditech.com/productresources/Pages/PRbiProductResourcesIndexSIX.asp">6.x</a></li>
						<li><a href="https://www.meditech.com/productresources/Pages/PRbiProductResourcesIndexCS.asp">Client/Server</a></li>
						<li><a href="https://www.meditech.com/productresources/Pages/PRbiProductResourcesIndexMAGIC.asp">MAGIC</a></li>
					</ul>

					<h5>Application Programming Interface (API) Resources</h5>
					<ul>
						<li><a href="https://home.meditech.com/en/d/restapiresources/pages/apidoc.htm">App Developer Resources</a></li>
						<li><a href="https://home.meditech.com/en/d/restapiresources/homepage.htm">RESTful API Infrastructure & Interoperability Services Resource Center</a></li>
					</ul>
				</div>

				<div class="container__one-fourth">
					<h5>Customer Share</h5>
					<ul>
						<li><a href="https://www.meditech.com/reportbase/">NPR Report Writer Reports/Design</a></li>
					</ul>
					<h5>Learning</h5>
					<ul>
						<li><a href="https://home.meditech.com/en/d/cstrainingresources/homepage.htm">5.x Fast Track Training</a></li>
						<li><a href="https://home.meditech.com/en/d/newsroom/pages/physicianclpp.htm">CLPP</a></li>
						<li><a href="https://home.meditech.com/en/d/newsroom/pages/doctorshours.htm">Doctors' Hours</a></li>
						<li><a href="https://home.meditech.com/en/d/customer/pages/seminars.htm">Seminars</a></li>
						<li><a href="https://www.meditech.com/kb/homepage.htm">Knowledge Base</a></li>
						<li><a href="https://customer.meditech.com/en/d/clientservicesenhresources/pages/issuemanagement.htm">Issue Management Tutorial</a></li>
						<li><a href="https://home.meditech.com/en/d/casestudies/homepage.htm">White Papers &amp; Case Studies</a></li>
						<li><a href="https://customer.meditech.com/en/d/clinicalnotifications/pages/clnabfeallapplications.htm">Clinical Notifications/Notices</a></li>
						<li><a href="https://customer.meditech.com/en/d/esignature/pages/esignabfeallapplicationsnew.htm">Electronic Signature</a></li>
						<li><a href="https://customer.meditech.com/en/d/prwehrwide/pages/abfetutorials.htm">e-Learning Tutorials</a></li>
						<li><a href="https://customer.meditech.com/en/d/meditechondemand/homepage.htm">MEDITECH On Demand</a></li>
					</ul>
				</div>

				<div class="container__one-fourth">
					<h5>Development Resources</h5>
					<ul>
						<li><a href="https://www.meditech.com/docsearch/">Release Documentation Search</a></li>
						<li><a href="https://customer.meditech.com/testingguidesearch/">Testing Guide Search</a></li>
						<li><a href="https://customer.meditech.com/bloodbankvalidationsearch/">Blood Bank Validation Guides</a></li>
						<li><a href="https://customer.meditech.com/standardcontentsearch/">Standard Content Search</a></li>
					</ul>
					<h5>EHR &amp; Regulatory</h5>
					<ul>
						<li><a href="https://home.meditech.com/en/d/govregulatory/homepage.htm" title="Regulatory">Regulatory</a></li>
						<li><a href="https://customer.meditech.com/en/d/hipaa/homepage.htm" title="HIPAA">HIPAA</a></li>
						<li><a href="https://customer.meditech.com/en/d/informationsecurity/homepage.htm" title="Information Security">Information Security</a></li>
					</ul>
					<h5>Social Media</h5>
					<ul>
						<li><a href="https://www.facebook.com/MeditechEHR?ref=br_tf" rel="noopener noreferrer" title="MEDITECH Facebook">Facebook</a></li>
						<li><a href="https://twitter.com/MEDITECH" rel="noopener noreferrer" title="MEDITECH Twitter">Twitter</a></li>
						<li><a href="https://www.linkedin.com/company/meditech" rel="noopener noreferrer" title="MEDITECH LinkedIn">LinkedIn</a></li>
					</ul>
					<a class="footer_logo_gae" href="<?php print $websiteURL; ?>"><img src="<?php print $websiteURL; ?>/sites/all/themes/meditech/images/meditech-logo.png" alt="MEDITECH footer logo" style="margin:1em 0;"></a>
				</div>

			</div>
		</section>
		<!-- End of SITEMAP -->

		<!-- Phone Number Area-->
		<div id="phone" class="cus-footer-phone">
			<div class="row">
				<a href="tel:781-821-3000" class="phone_number_gae">
					<i class="fas fa-phone fa-lg cus-footer-phone-icon"></i>
					<span class="cus-footer-phone-number">781-821-3000</span>
				</a>
			</div>
		</div>
		<!-- #phone -->


		<?php
} // START general footer...
else{
?>

		<!-- NEWSLETTER Signup Link -->
		<div class="container bg--emerald" style="padding-bottom: 1.75em;">
			<div class="container__newsletter" style="width:80%;">
				<h3 class="newsletter__title">Sign Up for MEDITECH Email Updates</h3>
				<p>Find out about our events, webinars, blog posts, and more when you sign up for our mailing list!</p>

				<div class="center">
					<?php hubspot_button('864299ec-5abf-4004-9c6d-2d051794101f', "Subscribe to receive the MEDITECH email newsletter"); ?>
				</div>

			</div>
		</div>
		<!-- END NEWSLETTER -->


		<style>
			.footer__sitemap__list__item {
				margin: 1em 0;
				line-height: 1.25em;
			}

		</style>

		<!-- SITEMAP ================================================================== -->
		<section class="container__centered" role="navigation" aria-labelledby="footer-nav">
			<div class="footer_link_gae" id="footer-nav">
				<div class="container no-pad center">
					<a class="footer_logo_gae" href="<?php print $websiteURL; ?>">
						<img src="<?php print $websiteURL; ?>/sites/all/themes/meditech/images/meditech-logo.svg" style="width:250px; margin:1em 0;" alt="MEDITECH footer logo">
					</a>
				</div>

				<div class="container__one-fourth">
					<h4><a href="<?php print $websiteURL; ?>/ehr-solutions">EHR Solutions</a></h4>
					<ul>
						<li><a href="<?php print $websiteURL; ?>/ehr-solutions/#fiscal-responsibility">Fiscal Responsibility</a></li>
						<li><a href="<?php print $websiteURL; ?>/ehr-solutions/#interoperability">Interoperability</a></li>
						<li><a href="<?php print $websiteURL; ?>/ehr-solutions/#nurse-specialty-care">Nurse & Specialty Care</a></li>
						<li><a href="<?php print $websiteURL; ?>/ehr-solutions/#patient-experience">Patient Experience</a></li>
						<li><a href="<?php print $websiteURL; ?>/ehr-solutions/#physician-efficiency">Physician Efficiency</a></li>
						<li><a href="<?php print $websiteURL; ?>/ehr-solutions/#population-health">Population Health</a></li>
						<li><a href="<?php print $websiteURL; ?>/ehr-solutions/#quality-outcomes">Quality Outcomes</a></li>
					</ul>
					<h4><a href="<?php print $websiteURL; ?>/ehr-solutions/meditech-professional-services">Professional Services</a></h4>
					<h4><a href="<?php print $websiteURL; ?>/ehr-solutions/meditech-greenfield">MEDITECH Greenfield</a></h4>
				</div>
				<!-- End of Container -->

				<div class="container__one-fourth">
					<h4><a href="<?php print $websiteURL; ?>/news">News</a></h4>
					<ul>
						<li><a href="<?php print $websiteURL; ?>/news-tags/press-releases">Press Releases</a></li>
						<li><a href="<?php print $websiteURL; ?>/news-tags/signings">Signings</a></li>
						<li><a href="<?php print $websiteURL; ?>/news-tags/videos">Videos</a></li>
					</ul>
					<h4><a href="https://blog.meditech.com">Blog</a></h4>
					<ul>
						<li><a href="<?php print $websiteURL; ?>/case-studies">Case Studies</a></li>
						<li><a href="<?php print $websiteURL; ?>/podcast-index">Podcasts</a></li>
						<li><a href="<?php print $websiteURL; ?>/ebooks">eBooks</a></li>
					</ul>
					<h4><a href="<?php print $websiteURL; ?>/events">Events</a></h4>
					<ul>
						<li><a href="<?php print $websiteURL; ?>/events/meditech-webinars">Upcoming Webinars</a></li>
						<li><a href="<?php print $websiteURL; ?>/events/meditech-on-demand-webinars">On Demand Webinars</a></li>
					</ul>
				</div>
				<!-- End of Container -->

				<div class="container__one-fourth">
					<h4><a href="<?php print $websiteURL; ?>/global">Global</a></h4>
					<ul>
						<li><a href="<?php print $websiteURL; ?>/global/meditech-canada">MEDITECH in Canada</a></li>
						<li><a href="<?php print $websiteURL; ?>/global/meditech-asia-pacific">MEDITECH Asia Pacific</a></li>
						<li><a href="<?php print $websiteURL; ?>/global/meditech-south-africa">MEDITECH South Africa</a></li>
						<li><a href="<?php print $websiteURL; ?>/global/meditech-uk-ireland">MEDITECH UK & Ireland</a></li>
					</ul>
					<h4><a href="<?php print $websiteURL; ?>/careers/careers-at-meditech">Careers</a></h4>
					<ul>
						<li><a href="<?php print $websiteURL; ?>/careers/benefits-perks">Benefits &amp; Perks</a></li>
						<li><a href="<?php print $websiteURL; ?>/careers/life-at-meditech">Life at MEDITECH</a></li>
						<li><a href="<?php print $websiteURL; ?>/careers/college-to-career">College to Career</a></li>
						<li><a href="<?php print $websiteURL; ?>/careers/recruiting-events">Recruiting Events</a></li>
						<li><a href="<?php print $websiteURL; ?>/careers/career-faqs">Career FAQs</a></li>
						<li><a href="<?php print $websiteURL; ?>/careers/veterans">Veterans</a></li>
					</ul>
				</div>
				<!-- End of Container -->

				<div class="container__one-fourth">
					<h4><a href="<?php print $websiteURL; ?>/about/meditech">About MEDITECH</a></h4>
					<ul>
						<li><a href="<?php print $websiteURL; ?>/about/executives">Executives</a></li>
						<li><a href="<?php print $websiteURL; ?>/about/community">Community</a></li>
						<li><a href="<?php print $websiteURL; ?>/about/customer-leaders">Customer Leaders</a></li>
						<li><a href="<?php print $websiteURL; ?>/about/meditech-customer-awards">Customer Awards</a></li>
						<li><a href="<?php print $websiteURL; ?>/about/directions-to-meditech">Directions</a></li>
						<li><a href="<?php print $websiteURL; ?>/about/area-hotels">Area Hotels</a></li>
					</ul>

					<?php
          // for South Africa pages...
          if($node->type == 'international_south_africa'){
          ?>
					<h4><a href="<?php print $websiteURL; ?>/global/meditech-south-africa/contact-meditech-south-africa">Contact MEDITECH South Africa</a></h4>
					<ul>
						<li><a href="https://www.facebook.com/MEDITECH-SA-317306641687653">Facebook</a></li>
						<li><a href="https://za.linkedin.com/in/meditech-south-africa-a5340453">LinkedIn</a></li>
						<li><a href="https://twitter.com/MEDITECHSA">Twitter</a></li>
					</ul>
					<?php
          }
          // for Asia Pacific pages...
          elseif($node->type == 'international_asia_pacific'){
          ?>
					<h4><a href="<?php print $websiteURL; ?>/global/meditech-south-africa/contact-meditech-asia-pacific">Contact MEDITECH Asia Pacific</a></h4>
					<ul>
						<li><a href="https://www.facebook.com/Meditech-Australia-360851333989795/">Facebook</a></li>
						<li><a href="https://au.linkedin.com/in/meditech-australia-63715257/de">LinkedIn</a></li>
						<li><a href="https://twitter.com/au_meditech">Twitter</a></li>
					</ul>
					<?php
          }
          // for UK/IRE pages...
          elseif($node->type == 'international_uk_ire'){
          ?>
					<h4><a href="<?php print $websiteURL; ?>/global/meditech-uk-ireland/contact-meditech-uk-ireland">Contact MEDITECH UK & Ireland</a></h4>
					<ul>
						<li><a href="https://twitter.com/MEDITECH_UK">Twitter</a></li>
						<!--<li><a href="https://au.linkedin.com/in/meditech-australia-63715257/de">LinkedIn</a></li>-->
					</ul>
					<?php
          }
          // for US pages...
          else{
          ?>
					<h4><a href="<?php print $websiteURL; ?>/contact">Contact MEDITECH</a></h4>
					<ul>
						<li><a href="https://www.facebook.com/MeditechEHR">Facebook</a></li>
						<li><a href="https://instagram.com/meditechehr">Instagram</a></li>
						<li><a href="https://www.linkedin.com/company/meditech">LinkedIn</a></li>
						<li><a href="https://twitter.com/MEDITECH">Twitter</a></li>
					</ul>
					<?php
          }
          ?>

					<h4><a href="https://home.meditech.com/en/d/customer/">Customers</a></h4>
				</div>
				<!-- End of Container -->

			</div>
			<!-- End of .footer_link_gae -->
		</section>
		<!-- End of SITEMAP -->


		<?php
} // END general footer
?>


		<!-- Legal Information =================================  -->
		<div class="container bg--black-coconut no-pad">
			<section class="container__centered">
				<div class="container__one-half">
					<h5 class="no-margin--top">Medical Information Technology, Inc.</h5>
					<p class="text--small">Copyright &copy; <span id="cYear">2021</span> Medical Information Technology, Inc.</p>
					<p class="text--small footer_link_gae"><a href="<?php print $websiteURL; ?>/cookie-policy">Cookie Policy</a> | <a href="<?php print $websiteURL; ?>/privacy-policy">Privacy Policy</a></p>
				</div>
			</section>
		</div>
		<script>
			var date = new Date();
			document.getElementById("cYear").innerHTML = date.getFullYear();

		</script>
		<!-- End of Legal Information -->

	</footer>

	<!-- Start PRINT ONLY footer content -->
	<div class="print-footer">
		<div class="container__centered">
			<p>Medical Information Technology, Inc.</p>
			<p class="text--small">Copyright &copy; <span id="cYear">2021</span> Medical Information Technology, Inc.</p>
		</div>
	</div>
	<!-- End PRINT ONLY footer content -->
	<!-- End of FOOTER -->

</div>
<!-- End of SLIDEOUT NAV WRAPPER -->

<!-- Slideout Navigation =================================================================================================== -->
<div class="slideoutnav sb-slidebar sb-left" role="navigation" aria-labelledby="mobile-nav">
	<ul class="menu" id="mobile-nav">
		<li class="first"><a class="<?php if( isset($active_solutions) ){ print $active_solutions; } ?>" href="<?php print $websiteURL; ?>/ehr-solutions">EHR Solutions</a></li>
		<li><a class="<?php if( isset($active_news) ){ print $active_news; } ?>" href="<?php print $websiteURL; ?>/news">News</a></li>
		<li><a href="https://blog.meditech.com">Blog</a></li>
		<li><a class="<?php if( isset($active_events) ){ print $active_events; } ?>" href="<?php print $websiteURL; ?>/events">Events</a></li>
		<li><a class="<?php if( isset($active_about) ){ print $active_about; } ?>" href="<?php print $websiteURL; ?>/about/meditech">About</a></li>
		<li><a class="<?php if( isset($active_global) ){ print $active_global; } ?>" href="<?php print $websiteURL; ?>/global">Global</a></li>
		<li><a class="<?php if( isset($active_careers) ){ print $active_careers; } ?>" href="<?php print $websiteURL; ?>/careers/careers-at-meditech">Careers</a></li>
		<li class="last"><a class="<?php if( isset($active_contact) ){ print $active_contact; } ?>" href="<?php print $websiteURL; ?>/contact">Contact</a></li>
	</ul>
</div>
<!-- End of Slideout Navigation -->

<script>
	$jq(document).ready(function() {
		$jq('#page a[href*="customer.meditech.com"], #page a[href*="jira.meditech.com/servicedesk/customer"], #page a[href*="greenfield.meditech.com"]').each(function() {
			$jq(this).append(' <i class="fas fa-lock"></i>');
		});

		// function to expand text containers that are mostly hidden **********************
		$jq(".expand-text").mouseenter(function() {
			// get height of collapsed content...
			var contentH = $jq(".collapsed-text", this).height();
			// add collapsed content height and then some to show all content...
			var newOuterH = contentH + 100;
			//alert(contentH);
			if (contentH > 120) {
				$jq(".collapsed-text", this).addClass("expand");
				$jq(".collapsed-text", this).animate({
					height: newOuterH
				}, 100);
			}
		}).mouseleave(function() {
			$jq(".collapsed-text", this).animate({
				height: "150"
			}, 100);
			$jq(".collapsed-text", this).removeClass("expand");
		});
	});

</script>

<?php if( user_is_logged_in() ){ ?>
<script type="text/javascript" src="<?php print $websiteURL; ?>/sites/all/themes/meditech/js/seo-tool.js"></script>
<?php } ?>

<!-- end page.tpl.php template
