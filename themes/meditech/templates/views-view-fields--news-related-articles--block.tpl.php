<!-- start views-view-fields--news-related-articles--block.tpl.php template -->
<?php 
  /* This template is for each row of the Views block: NEWS - RELATED ARTICLES ....................... 
   *
   * Because the Media Coverage links go to external sources, 
   * the PHP needs to pick those out from the rest and treat them differently.
   * Therefore the HTML is duplicated with slight differences.
   *
   */
     

  if($fields['field_source_url']->content != ''){ // if is a 'Media Coverage' (external) link...
?>
   
    <div class="panel">
      <h5 class="sidebar--newsarticle__title media"><a class="news_related_article_gae" href="<?php print $fields['field_source_url']->content; ?>" title="external link" target="_blank"><?php print $fields['title']->content; ?></a></h5>
      <p><span class="sidebar__newsarticle--callout"><?php print $fields['published_at']->content; ?></span> - <em><?php print $fields['field_source_name']->content; ?></em></p>
    </div>
    
<?php
  }
  else{ // if it is NOT a 'Media Coverage' (external) link...
?>
   
    <div class="panel">
      <h5 class="sidebar--newsarticle__title"><a class="news_related_article_gae" href="<?php print $fields['path']->content; ?>"><?php print $fields['title']->content; ?></a></h5>
      <p><span class="sidebar__newsarticle--callout"><?php print $fields['published_at']->content; ?></span></p>
    </div>
    
<?php
  }
?>
<!-- end views-view-fields--news-related-articles--block.tpl.php template -->