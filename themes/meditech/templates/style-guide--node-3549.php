<?php if( user_is_logged_in() ){ ?>
<!-- START style-guide--node-3549.php Designer Resources -->

<!--Javascript & CSS reference for Prism-->
<script type="text/javascript" src="<?php print $url; ?>/sites/all/themes/meditech/js/prism.js"></script>

<link href="<?php print $url; ?>/sites/all/themes/meditech/css/prism.css" rel="stylesheet" />

<style>
	.sg-section {
		margin-bottom: 2em;
	}

	.sg-section ul li {
		margin-bottom: .5em;
	}

	.sg-section input[type=checkbox] {
		display: none;
	}

	/* to hide the checkbox itself */
	.sg-section input[type=checkbox]+label:before {
		font-family: Font Awesome 5 Free;
		display: inline-block;
	}

	.sg-section input[type=checkbox]+label:before {
		content: "\f0c8";
		left: -31px;
		margin-right: -21px;
	}

	/* space between checkbox and label */

	.sg-section input[type=checkbox]:checked+label:before {
		content: "\f14a";
	}

</style>

<section class="container__centered">

	<h1 class="page__title">
		<?php print $title; ?>
	</h1>

	<div class="container__two-thirds">

		<h2>Announcements / Alerts</h2>
		<p>If downtime or other alerts are to be scheduled then we can use this alert box format. Only use this at the top of the Home Page.</p>
		<!-- Start Announcements/Alerts Code -->
		<pre style="border-radius: 6px;"><code class="language-html">&lt;!-- Announcements/Alerts -->
&lt;div class="container--page-title" style="background-color:#087E68; color:white; padding:.5em 0;">
  &lt;div class="container__centered">
    &lt;p style="margin:0; font-size:.85em;">Enter Announcement here.&lt;/p>
  &lt;/div>
&lt;/div>
</code></pre>
		<!-- End Announcements/Alerts Code -->

		<h2>AVP Review Code</h2>
		<p>Use the following code if you want to hide your page from the public but still allow access with a special URL (typically used for AVP review). At the end of your URL add this text "<strong>?mtid=review</strong>" for access. If it does not have this text at the end, meaning you are just using the normal URL for the page, you get redirected to a "not for your eyes" page. The page has to be published in order for non-logged in people to see it.</p>

		<!-- Start AVP Review Code -->
		<pre style="border-radius: 6px;"><code class="language-php">&lt;!-- Add this to the top of the template -->
&lt;?php
if( !isset($_GET['mtid']) || $_GET['mtid'] != 'review' ){
  print '&lt;script type="text/javascript">';
  print 'window.location.replace("https://ehr.meditech.com/not-meant-for-your-eyes");';
  print '&lt;/script>';
}
else{
?>

Your Code Here

&lt;!-- Add this to the bottom of the template -->
&lt;?php } ?>
</code></pre>
		<!-- End AVP Review Code -->

		<div class="sg-section">
			<h2>Campaign Checklist</h2>
			<ul class="fa-ul">
				<li>
					<input id="box1" type="checkbox">
					<label for="box1">For new campaigns, create a new campaign node and use the title the writers provide or something more SEO friendly (can discuss with supervisor for ideas).</label>
				</li>
				<li>
					<input id="box2" type="checkbox">
					<label for="box2">Remember the node ID number of that new campaign for template creation.</label>
				</li>
				<li><input id="box3" type="checkbox">
					<label for="box3">For campaign replacement, use designated PHP code to point to new include template that can only be seen with "review" URL (include template can follow naming convention something like draft--CAMPAIGN-NAME.php).</label>
				</li>
				<li><input id="box4" type="checkbox">
					<label for="box4">New campaigns should title their templates like so: campaign--node-ID.php (replace ID with the actual node ID number for the new campaign).</label>
				</li>
				<li><input id="box5" type="checkbox">
					<label for="box5">Add/edit the filename within the template comments at the top and bottom of the template.</label>
				</li>
				<li><input id="box6" type="checkbox">
					<label for="box6">Design your template to meet the writer's requests.</label>
				</li>
				<li><input id="box7" type="checkbox">
					<label for="box7">Make sure the template is using the most recent CTA code/functions.</label>
				</li>
				<li><input id="box8" type="checkbox">
					<label for="box8">Make sure the social media sharing code is included and that the icons are appearing on your page beneath the CTA in the last block.</label>
				</li>
				<li><input id="box9" type="checkbox">
					<label for="box9">Make sure to compress any large image files and double check your page performance using <a href="https://developers.google.com/web/tools/lighthouse" target="_blank">Google Lighthouse</a>.</label>
				</li>
				<li><input id="box10" type="checkbox">
					<label for="box10">Upload template to site and "flush" Drupal if it is a new template.</label>
				</li>
				<li><input id="box11" type="checkbox">
					<label for="box11">As long as the "review (redirect)" code/link is used, publish the campaign so AVPs can view it with the "review" URL.</label>
				</li>
				<li><input id="box12" type="checkbox">
					<label for="box12">Create the necessary social media images for the campaign and send to Christina Noel and the writer.</label>
				</li>
				<li><input id="box13" type="checkbox">
					<label for="box13">Upload the Twitter image to the campaign so it is available when shared via the buttons.</label>
				</li>
				<li><input id="box14" type="checkbox">
					<label for="box14">After approval, remove "review" codes and re-upload template.</label>
				</li>
				<li><input id="box15" type="checkbox">
					<label for="box15">Update Home Page with new campaign block if requested.</label>
				</li>
			</ul>
		</div>

		<div class="sg-section">
			<h2>Drupal Documentation</h2>
			<p><a href="http://www.tenten71.com/drupal-theme-from-scratch/" target="_blank">Drupal Theme from Scratch</a></p>
			<p><a href="<?php print $url; ?>/drupal-documentation/seo-best-practices">SEO Best Practices</a></p>
		</div>

		<h2>Hide Code Section on Live Site</h2>
		<p>Wrap your code in this PHP to hide it from public view but appear when logged in.</p>
		<!-- Start Hide on Live Code -->
		<pre style="border-radius: 6px;"><code class="language-php">&lt;!-- Add this above your code -->
&lt;?php if( user_is_logged_in() ){ ?>

Your Code Here

&lt;!-- Add this to the bottom of your code -->
&lt;?php } ?>
</code></pre>
		<!-- End Hide on Live Code -->

		<div class="sg-section">
			<h2>Google Analytics</h2>
			<p>For Google Analytics, use the following view "Marketing View - www.meditech.com - (external)". This will generate stats for the following domains:</p>
			<ul>
				<li>customer.meditech.com</li>
				<li>ehr.meditech.com</li>
				<li>meditech.com</li>
			</ul>
		</div>

		<h2>Hubspot CTA Buttons</h2>
		<p>Hubspot CTA buttons are called into our templates with the following code:</p>
		<!-- Start Hubspot CTA Code -->
		<pre style="border-radius: 6px;"><code class="language-php">&lt;!-- This code is used for the top CTA (enter your ALT tag within the quotes) -->
&lt;?php hubspot_button($cta_code, "Enter ALT Tag Here"); ?>

&lt;!-- This is used in the last block of the campaign which grabs the header and text from the field collection (if any) -->
&lt;?php cta_text($cta); ?>

&lt;!-- If you need multiple Hubspot buttons, use the CTA code number instead of "$cta_code" -->
&lt;?php hubspot_button('33997285-e326-4093-ac3e-d72779e0a5f1', "Enter ALT Tag Here"); ?>
</code></pre>
		<!-- End Hubspot CTA Code -->

		<div class="sg-section">
			<h2>SFTP Access</h2>
			<p>For file transfer access information, please reference <a href="https://docs.google.com/document/d/1ZUp4Bb8HKMDoyXZ8b4P6tQ4r1nMiktnUnJS5ecrN_cc/edit">this document</a>.</p>
			<p class="italic"><strong>Note:</strong> This document is only to be shared with members of the current web team.</p>
		</div>

		<h2>Social Share Buttons</h2>
		<p>Social share buttons are called into our templates with the following code:</p>
		<!-- Start Social Share Code -->
		<pre style="border-radius: 6px;"><code class="language-php">&lt;!-- Add this code to the end of your campaign in the CTA block -->
&lt;?php print $share_link_buttons; ?>
</code></pre>
		<!-- End Social Share Code -->

		<h2>Update Campaign Without Affecting Live Version</h2>
		<p>This code is used to create a copy of the old campaign template which allows you to edit it without affecting the live campaign. The code prevents anyone from seeing the include version unless they add <strong>?mtid=review</strong> at the end of the URL.</p>
		<p>When you're done with the updated campaign, remove the review code and copy the new template's code onto the old template. No node to delete, no redirecting, and no flushing needed. Be sure to delete the "updated-file-for-review.php" file when finished.</p>
		<!-- Start New Review Include Code -->
		<pre style="border-radius: 6px;"><code class="language-php">&lt;!-- Place this code at the top of the live page -->
&lt;?php
if( isset($_GET['mtid']) && $_GET['mtid'] == 'review' ){
  include('updated-file-for-review.php');
}
else{
?>

Live Template Code Here

&lt;!-- Place this code at the bottom of the page -->
&lt;?php } ?>
</code></pre>
		<!-- End New Review Code -->


	</div>

	<!-- SIDEBAR -->
	<aside class="container__one-third">

		<div class="sidebar__nav panel">
			<?php
        $styleBlock = module_invoke('menu', 'block_view', 'menu-style-guide-side-nav');
        print render($styleBlock['content']); 
        ?>
		</div>

	</aside>
	<!-- END SIDEBAR -->

</section>

<!-- END style-guide--node-3549.php Designer Resources -->
<?php } ?>
