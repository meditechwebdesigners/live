<!-- start simple-page--node-4115.tpl.php template -- KLAS Research Study on Interoperability -->

<div class="container no-pad--bottom">
  <div class="container__centered">
    <div class="container__one-half content--pad-right">
      <h1><?php print $title; ?></h1>
      <h2>Resources for Participating</h2>
      <p>The KLAS Research Study on Interoperability will soon be available for data collection. This opportunity is a great way for you to take stock of your interoperability journey and offer a testament to the path you are on&mdash;both where you are now, and where you are headed.</p>
      <p>As your vendor partner, MEDITECH is here for you every step of the way. We welcome you to review our resources here to help familiarize yourself with the new survey format, as well as MEDITECH's interoperability offerings, and ease your process in preparing.</p>
    </div>
    <div class="container__one-half" style="padding-top: 2em;">
      <div class="bg-pattern--container">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/healthcare-interoperability.jpg" alt="connected images of healthcare">
        <div class="bg-pattern--blue-squares bg-pattern--left"></div>
      </div>
    </div>

  </div>
</div>

<div class="container container-pad">
  <div class="container__centered">
    <h2>Survey Timeline</h2>
    <div class="text--large text--white">
      <div class="container__one-third shadow-box center" style="background-color: #087E68;">
        <strong>Survey Launches</strong><br>
        Mid April
      </div>
      <div class="container__one-third shadow-box center" style="background-color: #00BC6F;">
        <strong>Survey Closes</strong><br>
        Mid June
      </div>
      <div class="container__one-third shadow-box center" style="background-color: #00bbb3;">
        <strong>Notifications Sent</strong><br>
        September
      </div>
    </div>
  </div>
</div>

<style>
  .card { min-height: 14.25em; }
  @media (max-width: 800px) {
    .card { min-height: unset; }
  }
</style>
<div class="container container-pad">
  <div class="container__centered">
    <h2>KLAS Framework Pillars</h2>
    <div class="card-wrapper text--white">
      <div class="container__one-fourth card center bg--green-gradient" style="padding:1em;">
        <h3>Connectivity</h3>
        <p>Is the vendor effectively supporting connections that allow data liquidity between third-party app and EMR?</p>
      </div>
      <div class="container__one-fourth card center bg--purple-gradient" style="padding:1em;">
        <h3>Utility</h3>
        <p>Is the vendor displaying shared data in a way that empowers the healthcare organization to effectively use it?</p>
      </div>
      <div class="container__one-fourth card center bg--green-gradient" style="padding:1em;">
        <h3>Use Cases</h3>
        <p>Is the vendor's interoperability support positively impacting important use cases for each care setting?</p>
      </div>
      <div class="container__one-fourth card center bg--purple-gradient" style="padding:1em;">
        <h3>Outcomes</h3>
        <p>Is the vendor's interoperability support helping healthcare organizations be more efficient and effective?</p>
      </div>
    </div>
  </div>
</div>

<section class="container__centered">
  <div class="container__two-thirds">
    <?php print render($content['field_body']); ?>

    <script type='text/javascript' src='https://click.appcast.io/pixels/generic3-15067.js?ent=84'></script>
  </div>
</section>

<!-- end simple-page--node-4115.tpl.php template -->
