<?php // This template is for each row of the Views block:  COVID-19 WEBINARS Upcoming ....................... 

// get node ID...
$nid = $fields['nid']->content;
$node = node_load($nid);

// get_webinar_image function is in the template.php file

// check to see if URL is external...
if( strpos($fields['field_text_1']->content, 'community.museweb.org') !== false ){
  $target = 'target="_blank"';
}
else{
  $target = '';
}
?>
<!-- start views-view-fields--covid-19-webinars-upcoming--block.tpl.php template -->
<figure class="container no-pad">
  <div class="container__one-third">
    <?php $webinar_image = get_webinar_image($node); ?>
    <div class="square-img-cropper <?php print $webinar_image['crop']; ?>">
      <a class="webinars_link_gae" href="<?php print $fields['field_text_1']->content; ?>" <?php print $target; ?>><img src="<?php print $webinar_image['url']; ?>" alt="webinar thumbnail"></a>
    </div>
  </div>
  <figcaption class="container__two-thirds">
    <h3 class="header-four no-margin"><a class="webinar_live_link_gae" href="<?php print $fields['field_text_1']->content; ?>" <?php print $target; ?>><?php print $fields['title']->content; ?></a></h3>
    <?php // temp fix...
    if($nid == '3369'){
      print '<h5 class="no-margin--bottom">TBD</h5>';
    }
    else{
    ?>
      <h5 class="no-margin--bottom"><?php print $fields['field_date_and_time']->content.', '.$fields['field_date_and_time_1']->content; ?> (Eastern) 
      <?php 
      if( !empty($fields['field_duration']->content) ){ 
        print ' | '.$fields['field_duration']->content; 
      }
      ?></h5>
    <?php 
    } 
    ?>
      
    <p><?php print $fields['field_summary']->content; ?></p>
  </figcaption>
</figure>

<?php 
if( user_is_logged_in() ){ 
  print '<p style="text-align:right; font-size:12px;"><a href="https://ehr.meditech.com/node/'.$nid.'/edit">Edit this content</a></p>';
}
?>
<hr>
<!-- end views-view-fields--covid-19-webinars-upcoming--block.tpl.php template -->