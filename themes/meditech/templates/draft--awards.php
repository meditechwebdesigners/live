<!-- START campaign--node-3570.php -->

<?php // This template is set up to control the display of the MEDITECH Awards Campaign

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');
$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
    .headshot {
        position: absolute;
        top: -50px;
        left: 50%;
        transform: translate(-50%);
        z-index: 3;
    }

    .headshot img {
        width: 100px;
        height: 100px;
        border-radius: 50%;
    }

    .pipe-margin {
        margin: 0 0.5em;
    }

    .video {
        padding-bottom: 57%;
        overflow: visible;
        margin: 0;
        box-shadow: none;
        max-width: 100%;
    }

    .content__callout__image-wrapper {
        padding: 0;
    }

    .pad-adjust {
        padding: 2em 4em !important;
    }

    .award-img {
        width: 250px;
        margin-bottom: 1em;
    }

    .container-xp {
        padding: 5em 0;
    }

    .award-title {
        padding-top: 3em;
    }

    .sb-margin {
        margin-left: 2em;
    }

    .sb-min {
        min-height: 275px;
    }

    .quote-box {
        padding: 3em;
        background-color: rgba(3, 3, 30, .5);
        border-left: 5px solid #00bc6f;
        margin-bottom: 2.35765%;
        border-radius: 7px;
        box-shadow: 0px 0px 20px 1px rgba(0, 0, 0, .2);
        position: relative;
    }

    /*
    .quotation-marks:before {
        content: open-quote;
        top: 0.325em;
        left: -0.15em;
    }


    .quotation-marks:after {
        content: close-quote;
        bottom: -0.3em;
        right: 0em;
    }

    .quotation-marks:before .quotation-marks:after {
        color: #00BC6F;
        opacity: 0.17;
        position: absolute;
        line-height: 0.1em;
        z-index: -10;
        font-size: 8em;
    }
*/

    .text--small {
        line-height: 1.5;
    }

    .float-left {
        float: left;
    }

    .float-right {
        float: right;
    }

    .tools-list {
        padding-left: 1em;
    }

    .tools-list li {
        list-style-type: none;
        padding: 1em 0 1em 4em;
        background-repeat: no-repeat;
        background-position: left center;
        background-size: 55px 55px;
        margin-bottom: 0;
    }

    .tools-list h4 a {
        border-bottom: 1px solid;
    }

    .tools-group li:nth-child(1) {
        background-image: url("<?php print $url; ?>/sites/all/themes/meditech/images/about/icon--genomics.svg");
    }

    .tools-group li:nth-child(2) {
        background-image: url("<?php print $url; ?>/sites/all/themes/meditech/images/about/icon--care-compass.svg");
    }

    .tools-group li:nth-child(3) {
        background-image: url("<?php print $url; ?>/sites/all/themes/meditech/images/about/icon--patient-connect.svg");
    }

    .tools-group li:nth-child(4) {
        background-image: url("<?php print $url; ?>/sites/all/themes/meditech/images/about/icon--health-records-on-iphone.svg");
    }

    .tools-group li:nth-child(5) {
        background-image: url("<?php print $url; ?>/sites/all/themes/meditech/images/about/icon--virtual-assistant.svg");
    }

    .tools-group2 li:nth-child(1) {
        background-image: url("<?php print $url; ?>/sites/all/themes/meditech/images/about/icon--patient-care.svg");
    }

    .tools-group2 li:nth-child(2) {
        background-image: url("<?php print $url; ?>/sites/all/themes/meditech/images/about/icon--oncology.svg");
    }

    .tools-group2 li:nth-child(3) {
        background-image: url("<?php print $url; ?>/sites/all/themes/meditech/images/about/icon--labor-and-delivery.svg");
    }

    .tools-group2 li:nth-child(4) {
        background-image: url("<?php print $url; ?>/sites/all/themes/meditech/images/about/icon--virtual-care.svg");
    }

    @media all and (min-width: 1550px) {
        .pad-adjust {
            padding: 4em !important;
        }
    }

    @media all and (max-width: 56.250em) {
        .quote-box {
            width: 100%;
        }
    }

    @media all and (max-width: 50em) {
        .award-img {
            width: 200px;
        }

        .award-title {
            padding-top: 0;
            text-align: center;
            margin-bottom: 2em;
        }

        .container-xp {
            padding: 3em 0;
        }

        .quote-box {
            padding: 2em;
            margin-bottom: 1em;
        }

        .sb-margin {
            margin: 2em 2em 0em 2em;
        }

        .sb-min {
            min-height: 0;
        }

        .tools-list li {
            padding-left: 5em;
        }
    }

</style>

<div class="js__seo-tool__body-content">

    <h1 style="display:none;" class="js__seo-tool__title">MEDITECH Awards</h1>

    <!-- START Updated Block 1 -->
    <!--
    <div class="container no-pad">
        <div class="gl-container">
            <div class="container__one-half gl-text-pad bg--purple-gradient text--white pad-adjust">
                <h2>MEDITECH Expanse Continues Rise to the Top</h2>
                <p>Consistency is at the heart of MEDITECH’s success, and for the 8th consecutive year, that success has earned MEDITECH recognition by industry research firm KLAS. This is the second year in a row that KLAS rated MEDITECH Expanse a top performer in all categories ranked, including Best in KLAS in three key segments and the #2 overall software suite.</p>
                <p>But there’s more to the story. MEDITECH’s industry accolades have translated into a wave of momentum for Expanse, with the platform seeing the second highest rise in market share in 2021 among all EHR vendors.</p>
            </div>


            <div class="container__one-half bg--light-gray gl-text-pad">
                &nbsp;
            </div>
        </div>
    </div>
-->

    <div class="container bg--purple-gradient">
        <div class="container__centered">
            <div class="container__one-half">
                <h2>MEDITECH Expanse Continues Rise to the Top</h2>
                <p>Consistency is at the heart of MEDITECH’s success, and for the 8th consecutive year, that success has earned MEDITECH recognition by industry research firm KLAS. This is the second year in a row that KLAS rated MEDITECH Expanse a top performer in all categories ranked, including Best in KLAS in three key segments and the #2 overall software suite.</p>
                <p>But there’s more to the story. MEDITECH’s industry accolades have translated into a wave of momentum for Expanse, with the platform seeing the second highest rise in market share in 2021 among all EHR vendors.</p>
            </div>

            <div class="container__one-half">
                <img style="border-radius: 0.389em;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/male-and-female-doctor-having-discussion-with-arrow-graphics.jpg" alt="One doctor holding digital tablet talking to another doctor in a hallway">
            </div>

        </div>
    </div>
    <!-- END Updated Block 1 -->


    <!-- START Block 2 -->
    <div class="container">
        <div class="container__centered center">
            <h2>Best in KLAS</h2>
            <p class="italic" style="margin-bottom: 2em;">For the 2nd consecutive year, MEDITECH Expanse received Best in KLAS in three key segments:</p>
            <div class="container no-pad--top">
                <div class="container__one-third">
                    <img class="award-img" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-KLAS-Award-2022--Acute-Care-EMR.png" alt="Best in KLAS 2022 award - Acute Care EMR (community hospital) for MEDITECH Expanse">
                    <p class="bold no-margin--bottom">Acute Care EMR</p>
                    <p>Community Hospital</p>
                </div>
                <div class="container__one-third">
                    <img class="award-img" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-KLAS-Award-2022--Patient-Accounting-and-Patient-Management.png" alt="Best in KLAS 2021 award - Patient Accounting & Patient Management (community hospital) for MEDITECH Expanse">
                    <p class="bold no-margin--bottom">Patient Accounting &amp;
                        Patient Management</p>
                    <p>Community Hospital</p>
                </div>
                <div class="container__one-third">
                    <img class="award-img" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-KLAS-Award-2022--Home-Health.png" alt="Best in KLAS award - Home Health (small) for MEDITECH Expanse">
                    <p class="bold no-margin--bottom">Home Health EHR</p>
                    <p>1-200 average daily census</p>
                </div>
            </div>
            <ul style="list-style:none;">
                <p class="italic" style="margin-bottom: 2em;">This was also the 2nd consecutive year that Expanse was rated a Top Performer (#2 overall) for: <br>
                    <span class="bold">Overall Software Suite <span class="pipe-margin">|</span> Acute Care EMR (Large) <span class="pipe-margin">|</span> Ambulatory EMR (Over 75 physicians)</span>
                </p>
            </ul>
        </div>
    </div>

    <!-- END Block 2 -->

    <!-- START Block 3 -->
    <div class="container bg--purple-gradient container-xp">
        <div class="container__centered">
            <div class="container__one-half">
                <h2>Expanse Ambulatory Continues Momentous Climb</h2>
                <p>Ranked the #2 Ambulatory EMR for large physician practices, MEDITECH’s Expanse Ambulatory EHR continues to gain accolades for its intuitive web-based design and ease of personalization. Now, as availability extends to the independent and physician-owned practice sector, providers are taking notice. </p>
                <p><span class="bold text--large">100%</span> of 2022 Best in KLAS survey respondents who use MEDITECH Expanse Ambulatory say it is part of their long-term plans.</p>
                <p><span class="bold text--large">95%</span> of 2022 Best in KLAS survey respondents using Expanse Ambulatory report that MEDITECH avoids charging for every little thing (the only vendor with a 95% or better rating)
                </p>
            </div>
            <div class="container__one-half quote-box" style="margin-top:2em;">
                <div class="headshot">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/quote-outline.svg" alt="Quote graphic">
                </div>
                <p class="italic">"The product does work exactly as promoted. I would say that it is perfect. The product is as designed and as promised. We meet everything that we need to meet in terms of outcomes. I would tell others that this EHR is a solid platform that continues to be enhanced regularly, and we have been able to achieve satisfaction in a very short time. We are going to hopefully expand our ambulatory usage of the product in the future."</p>
                <p class="bold no-margin--bottom">— CIO, May 2021</p>
            </div>
        </div>
    </div>
    <!-- END Block 3 -->

    <!-- START Block 4 -->
    <div class="container background--cover container-xp" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/about/bg--organic-shapes-green-right.svg);">
        <div class="container__centered">
            <div class="container__one-third">
                <h2>Expanse Success Contributes to Strong Market Growth</h2>
                <p>According to the 2022 KLAS US Hospital Market Share report, MEDITECH saw the second highest net growth in acute care hospital market share in 2021, thanks to the growing interest in Expanse. But there is even more to our success:</p>
            </div>
            <div class="container__two-thirds shadow-box">
                <ul class="fa-ul">
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span><strong>+1,077 beds</strong> (2nd most net gain)</li>
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span><strong>+ 18 hospitals </strong> (2nd most net gain)</li>
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span><strong>50 legacy migrations</strong> (Largest legacy retention rate of any vendor)</li>
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span><strong>11% growth </strong> among critical access hospitals</li>
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span><strong>184:</strong> Most standalone community hospitals of any vendor</li>
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span><strong>Net gain in 4 of 5</strong> organizational sizes that saw movement</li>
                </ul>
                <p class="no-margin--bottom">MEDITECH was in the top two with one of the strongest Overall Performance scores for Customer Experience Pillars.</p>
            </div>
        </div>
    </div>
    <!-- END Block 4 -->

    <!-- START Block 5 -->
    <div class="container bg--purple-gradient">
        <div class="container__centered" style="margin-bottom: 2em;">
            <div class="center auto-margins" style="margin-bottom: 2em;">
                <h2>Our Customers Have Spoken</h2>
                <p>Best in KLAS is more than an award. It’s testament that we are making a positive impact in the lives of our customers. It’s their experiences that drive us to be our best.</p>
            </div>
            <div class="container__one-third quote-box">
                <div class="headshot">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/quote-outline.svg" alt="Quote graphic">
                </div>
                <p class="text--large bold">Personalization</p>
                <p class="italic">“I like how customizable Expanse Acute Care EMR is. We have the flexibility to make the EMR our own. I like that we aren't tied into a community product. We can do what we want with the product.”</p>
                <p class="bold no-margin--bottom">— CIO, July 2021</p>
            </div>
            <div class="container__two-thirds quote-box">
                <div class="headshot">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/quote-outline.svg" alt="Quote graphic">
                </div>
                <p class="text--large bold">Integration</p>
                <p class="italic">“The nice part about the Expanse platform is that everything ties together. We have one system that does everything together. The system looks and feels the same between the different areas. If a provider is in an ambulatory clinic one day and a hospital the next day, the system looks nearly identical. That makes things easier for the providers because they don't have to readjust. Patient information flows well. We share one medical record per patient across all our sites, and this system does that very well. Patients can visit any facility, and the record is the same all the way around.”</p>
                <p class="bold no-margin--bottom">— Director, August 2021</p>
            </div>
        </div>
        <div class="container__centered">
            <div class="container__two-thirds quote-box">
                <div class="headshot">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/quote-outline.svg" alt="Quote graphic">
                </div>
                <p class="text--large bold">Financials</p>
                <p class="italic"> “We were very proud of our transition onto Expanse Patient Accounting. We were dropping claims a few days after go-live. We were very happy that there was no interruption. I have been through EMR transitions where there was oftentimes an interruption with the revenue cycle, but we did not experience one interruption with our transition to Expanse Patient Accounting.”</p>
                <p class="bold no-margin--bottom">— CIO, February 2022</p>
            </div>
            <div class="container__one-third quote-box">
                <div class="headshot">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/quote-outline.svg" alt="Quote graphic">
                </div>
                <p class="text--large bold">Satisfaction</p>
                <p class="italic">“The product is very intuitive. It meets our clinical and administrative needs. Of all of the MEDITECH products that I have dealt with, Expanse Acute Care EMR is their best product by far.”</p>
                <p class="bold no-margin--bottom">— VP/Other Executive, July 2021</p>
            </div>
        </div>
    </div>


    <!-- END Block 5 -->

    <!-- START Block 6 
<div class="container background--cover" style="background-image: url(< ?php print $url; ?>/sites/all/themes/meditech/images/about/bg--organic-shapes-green-right-2.svg);">
    <div class="container__centered">
        <div class="container__one-half">
            <h2>Making Your EHR Your Own</h2>
            <p>Clinicians who have the knowledge and ability to personalize their EHRs are more satisfied and efficient users. This is why we made personalization a defining quality of Expanse. In their Large Ambulatory EMR 2021 report, KLAS Research and the Arch Collaborative gave MEDITECH high marks for clinician satisfaction with EHR personalization, noting “MEDITECH receives consistently high ratings for making EMR personalization tools available. Respondents say MEDITECH has stepped up in recent years, leading to a much-improved end-user experience with personalization tools.”</p>
            <p class="italic text--small">Note: Data for MEDITECH, NexGen, and eClinicalWorks was limited. For full details visit <a href="https://klasresearch.com/" target="_blank">klasresearch.com</a>.</p>
        </div>
        <div class="container__one-half">
            <div class="sb-margin">
                <div id="modal1" class="modal">
                    <a class="close-modal" href="javascript:void(0)">&times;</a>
                    <div class="modal-content">
                        <img src="< ?php print $url; ?>/sites/all/themes/meditech/images/about/klas-provider-satisfaction-with-emr-personalization--full.jpg" alt="KLAS Performance Data - Satisfaction with the level of EMR personalization available for providers (MEDITECH numbers in focus)">
                    </div>
                </div>
                <div class="open-modal" data-target="modal1">
                    <img style="border-radius:7px; box-shadow: 0px 0px 20px 1px rgba(120, 120, 120, .2);" src="< ?php print $url; ?>/sites/all/themes/meditech/images/about/klas-provider-satisfaction-with-emr-personalization--bold.jpg" alt="KLAS Performance Data - Satisfaction with the level of EMR personalization available for providers.">
                    <div class="mag-bg">
                        <i class="mag-icon fas fa-search-plus"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Block 6 -->

    <!-- START Block 7 
<div class="container bg--purple-gradient">
    <div class="container__centered">
        <div class="container__one-third center">
            <img class="award-img" src="< ?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-KLAS-Award-2021-Home-Health-EHR.png" alt="Best in KLAS award - Home Health (small) for MEDITECH Expanse">
            <p class="bold">MEDITECH Home Care is a flexible and scalable home care solution for organizations of all sizes.</p>
        </div>
        <div class="container__two-thirds">
            <h2>Expanse Brings Home a New Award — Home Care</h2>
            <p>The power of Expanse is its ability to connect the entire continuum of care with a single EHR — the same intuitive, web-based system for every setting. That includes home care. Now, for the first time ever, Expanse has been named the top small Home Health EHR. While data for MEDITECH was more limited in the large Home Health category, Expanse received similarly high marks for organizations over 200 average daily census.</p>
            <div class="quote-box" style="padding:2em; margin-top: 2em;">
                <p class="italic">“The vendor is great, and <strong>their product does what they say it does</strong>. It doesn’t let us do something that is incorrect. The system covers billing, orders, and scheduling, so we can’t make a visit without an order, and that is great. <strong>The system is very reliable</strong>. It covers quality and regulatory issues.”</p>
                <p class="bold no-margin--bottom">—Director, October 2020</p>
            </div>
        </div>
    </div>
</div>
<!-- END Block 7 -->

    <!-- START Block 8 
<div class="container background--cover" style="background-image: url(< ?php print $url; ?>/sites/all/themes/meditech/images/about/bg--organic-shapes-green-full-width.svg);">
    <div class="container__centered center">
        <div class="auto-margins" style="margin-bottom:2em;">
            <h2>Black Book Research</h2>
            <p>KLAS isn’t the only organization taking note of MEDITECH Expanse. In 2020, Black Book Research — a leading provider of competitive intelligence, client satisfaction and market research for the healthcare IT industry — gave MEDITECH its Highest Client Satisfaction Award for Inpatient EHR and rated Expanse #1 in usability for nurses.</p>
        </div>
        <div class="container__one-half shadow-box sb-min">
            <img style="width:125px; margin-bottom:1em;" src="< ?php print $url; ?>/sites/all/themes/meditech/images/about/icon--Client-Satisfaction-Award.svg" alt="2020 Black Book Highest Client Satisfaction Award for Inpatient EHR icon">
            <p class="text--large no-margin--bottom">2020 Black Book Highest Client Satisfaction Award for Inpatient EHR</p>
        </div>
        <div class="container__one-half shadow-box sb-min">
            <img style="width:125px; margin-bottom:1em;" src="< ?php print $url; ?>/sites/all/themes/meditech/images/about/icon--Usability-for-Nurses.svg" alt="2020 Black Book #1 EHR in Usability for Nurses icon">
            <p class="text--large no-margin--bottom">2020 Black Book #1 EHR in Usability for Nurses</p>
        </div>
    </div>
</div>
<!-- END Block 8 -->

    <!-- START Block 9 
<div class="container no-pad">
    <div class="gl-container">
        <div class="container__one-half background--cover" style="background-image: url(< ?php print $url; ?>/sites/all/themes/meditech/images/about/view-looking-upwards-between-city-buildings.jpg); min-height:350px;"></div>
        <div class="container__one-half gl-text-pad bg--purple-gradient">
            <h2>Expanding Market Share</h2>
            <p>The market footprint continues to grow for MEDITECH Expanse, with new hospitals and IDNs joining the existing MEDITECH customers migrating to our web-based solution.</p>
            <p>Highlights from the 2020 KLAS Market Share report:</p>
            <ul class="fa-ul">
                <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Expanse’s mobile, web-based platform and modern EHR capabilities drove a second consecutive year of increased market share for MEDITECH</li>
                <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>MEDITECH’s acute care market share growth came from new IDNs and standalone hospitals</li>
                <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Customers rated MEDITECH highly in loyalty and value, reflecting long-term relationships and trust, as well as superior TCO.</li>
                <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>MEDITECH surpassed Cerner in the number of smaller organizations signed.</li>
            </ul>
        </div>
    </div>
</div>
<!-- END Block 9 -->

    <!-- START Block 10
<div class="container no-pad">
    <div class="gl-container">
        <div class="container__one-half gl-text-pad bg--purple-gradient">
            <h2>An Expansive View</h2>
            <p>At MEDITECH we have an expansive view of the future, always believing the best is yet to come. 2020 was a challenging year but we remained productive and hopeful, hitting some impressive milestones:</p>
            <ul class="fa-ul">
                <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>8 new innovative products that extend the Expanse EHR</li>
                <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>3 new cloud-hosted solutions, including a new physician mobility app</li>
                <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>A robust new web-based interface and modern workflows for nurses</li>
                <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>60+ go-lives — from small Critical Access Hospitals to large IDNs</li>
                <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Implementation of the largest private healthcare network in the U.S.</li>
                <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Dramatic increase in our MEDITECH as a Service model (138%)</li>
                <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Robust growth in new hospitals choosing Expanse (60% of bookings)</li>
            </ul>
        </div>
        <div class="container__one-half background--cover flex-order--reverse" style="background-image: url(< ?php print $url; ?>/sites/all/themes/meditech/images/about/colorful-sunrise-over-mountains.jpg); background-position: right; min-height:350px;"></div>
    </div>
</div>
<!-- END Block 10 -->

    <!-- START Block 11 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/about/bg--organic-shapes-green-full-width-2.svg);">
        <div class="container__centered">
            <div class="auto-margins center" style="margin-bottom: 2em;">
                <h2>New Tools for a New Era</h2>
                <p>There’s a reason MEDITECH Expanse is continuing to capture attention and market share. It’s a modern EHR with a growing set of tools for the changing healthcare landscape.</p>
                <p class="text--large">Here are a few recent additions to the Expanse family:</p>
            </div>
            <div class="container__one-half shadow-box">
                <ul class="tools-list tools-group">
                    <li>
                        <h4><a href="<?php print $url; ?>/ehr-solutions/meditech-genomics">Expanse Genomics</a></h4>
                    </li>
                    <li>
                        <h4><a href="<?php print $url; ?>/ehr-solutions/care-compass">Expanse Care Compass</a></h4>
                    </li>
                    <li>
                        <h4><a href="<?php print $url; ?>/ehr-solutions/expanse-patient-connect">Expanse Patient Connect</a></h4>
                    </li>
                    <li>
                        <h4><a href="<?php print $url; ?>/ehr-solutions/health-records-on-iphone">Health Records on iPhone</a></h4>
                    </li>
                    <li class="no-margin--bottom">
                        <h4><a href="<?php print $url; ?>/ehr-solutions/virtual-assistant">Expanse Virtual Assistant</a></h4>
                    </li>
                </ul>
            </div>
            <div class="container__one-half shadow-box">
                <ul class="tools-list tools-group2">
                    <li>
                        <h4><a href="<?php print $url; ?>/ehr-solutions/meditech-nursing">Expanse Patient Care</a></h4>
                    </li>
                    <li>
                        <h4><a href="<?php print $url; ?>/ehr-solutions/meditech-oncology">Expanse Oncology</a></h4>
                    </li>
                    <li>
                        <h4><a href="<?php print $url; ?>/ehr-solutions/labor-and-delivery?hsCtaTracking=b2772783-0455-4993-a5c7-609a31c4b178%7Cec235006-b7b1-401b-afb5-e61ad74334b0">Expanse Labor and Delivery</a></h4>
                    </li>

                    <li class="no-margin--bottom">
                        <h4><a href="<?php print $url; ?>/ehr-solutions/virtual-care">Virtual Care On Demand</a></h4>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END Block 11 -->

    <!--CTA-->
    <div class="container bg--purple-gradient">
        <div class="container__centered center auto-margins">

            <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
            <h2>
                <?php print $cta->field_header_1['und'][0]['value']; ?>
            </h2>
            <?php } ?>

            <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
            <div>
                <?php print $cta->field_long_text_1['und'][0]['value']; ?>
            </div>
            <?php } ?>

            <div class="center" style="margin-top:2em;">
                <?php hubspot_button($cta_code, "Download the Innovators Booklet"); ?>
            </div>

            <div style="margin-top:1em;">
                <?php print $share_link_buttons; ?>
            </div>

        </div>
    </div>
    <!--END CTA-->

</div>
<!-- end js__seo-tool__body-content -->


<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>

<!-- end campaign--node-3570.php template -->
