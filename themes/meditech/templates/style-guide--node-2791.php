<?php if( user_is_logged_in() ){ ?>
<!-- START style-guide--node-2791.php Gutterless Grid -->

<!--Javascript & CSS reference for Prism-->
<script type="text/javascript" src="<?php print $url; ?>/sites/all/themes/meditech/js/prism.js"></script>

<link href="<?php print $url; ?>/sites/all/themes/meditech/css/prism.css" rel="stylesheet" />

<section class="container__centered">

  <h1 class="page__title">
    <?php print $title; ?>
  </h1>

  <div class="container__two-thirds">

    <p>Piggybacking the <a href="<?php print $url; ?>/style-guide/grid">Grid</a> layouts, we have a variant called Gutterless Grid. It is pretty straightforward in that it is a grid without gutters (aka no margin between divs). In the example below, you will see that we have a <code class="language-css">.gl-container</code> (gutterless container) class which wraps the grid elements and styles them a bit differently.</p>
    <p>This class removes the margin from the right side of each div, adds extra padding, and also uses <a href="https://css-tricks.com/snippets/css/a-guide-to-flexbox/" target="_blank">Flexbox</a> to keep the child divs the same height no matter how much content is inside them. Long story short, all you have to do is wrap your grid elements inside the <code class="language-css">.gl-container</code> to achieve this result.</p>
    <p><span class="italic"><strong>Note:</strong> By default the background color is white at 80% opacity and the grid containers have a padding of 2em. You can override this by adding different inline styling or color classes as needed.</span></p>

    <div class="demo-ct">
      <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/CHF-patient-with-cardiologist.jpg); background-position: top; margin-bottom:2em;">
        <div class="container__centered">
          <div class="gl-container">
            <div class="container__one-fourth bg--emerald">
              <h2>1/4</h2>
            </div>
            <div class="container__three-fourths">
              <h2>3/4</h2>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Start Gutterless Containers Code -->
    <pre><code class="language-html">&lt;!-- Start gutterless containers example -->
&lt;div class="container">
  &lt;div class="container__centered">
    &lt;div class="gl-container">
      &lt;div class="container__one-fourth bg--emerald">1/4&lt;/div>
      &lt;div class="container__three-fourths">3/4&lt;/div>
    &lt;/div>
  &lt;/div>
&lt;/div>
&lt;!-- End gutterless containers example -->
</code></pre>
    <!-- End Gutterless Containers Code -->


    <h2>Full Width Gutterless Grid Containers</h2>
    <p>At some point you may want to use the Gutterless Grid with split containers that extend the width of the page. In order to do so, you'll have to modify the original markup a bit:</p>
    <ol>
      <li>Remove the <code class="language-css">.container__centered</code> div in order to extend the full width of the page.</li>
      <li>Add the <code class="language-css">.no-pad</code> utility class to the wrapping <code class="language-css">.container</code> div to remove the default padding.</li>
      <li>Add background <a href="<?php print $url; ?>/style-guide/colors">color classes</a> or images to either container if desired.</li>
      <li>Adjust the padding in the text container so that it is centered within the block better. You can do this with the <code class="language-css">.gl-text-pad</code> class that adds "padding: 4em" by default, or you can override the class to your needs with in-page styling.</li>
      <li>In this example, the image is in the right-hand container. When we scale down to mobile, we want the image to appear on top when the containers are stacked. To do so, add the <code class="language-css">.flex-order--reverse</code> class to the container with the image.</li>
      <li>Finally, you may want to add a "min-height" to the container with the image if you do not have text in it. When it stacks on mobile, you'll see a lot of the image gets cropped if there is no content in the container. Alternatively, if you'd like to hide the image container at tablet/mobile sizes, just add the <code class="language-css">.hide-on-tablets</code> or <code class="language-css">.hide-on-mobile</code> classes.</li>
    </ol>

    <div class="demo-ct" style="padding:0;">
      <div class="container">
        <div class="gl-container">
          <div class="container__one-half bg--blue-gradient gl-text-pad" style="min-height: 17em;">
            <h2>1/2 Container</h2>
            <p>Example paragraph text.</p>
          </div>
          <div class="container__one-half background--cover flex-order--reverse" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/styleguide/style-guide-background-accents-image.jpg); background-position: right; min-height:17em;">
          </div>
        </div>
      </div>
    </div>

    <!-- Start Full Width Gutterless Containers Code -->
    <pre><code class="language-html">&lt;!-- Start full width gutterless containers example -->
&lt;div class="container no-pad">
  &lt;div class="gl-container">
    &lt;div class="container__one-half bg--blue-gradient gl-text-pad">
      &lt;h2>1/2 Container&lt;/h2>
      &lt;p>Example paragraph text.&lt;/p>
    &lt;/div>
    &lt;div class="container__one-half background--cover flex-order--reverse" style="background-image: url(your-image.jpg); min-height:17em;">&lt;/div>
  &lt;/div>
&lt;/div>
&lt;!-- End full width gutterless containers example -->
</code></pre>
    <!-- End Full Width Gutterless Containers Code -->


    <h2>Additional Gutterless Grid Layout Options</h2>

    <div class="container center">
      <div class="container__centered" style="margin-bottom:2em;">
        <div class="gl-container">
          <div class="container__one-fifth bg--light-gray">
            1/5
          </div>
          <div class="container__one-fifth bg--light-brown">
            1/5
          </div>
          <div class="container__one-fifth bg--light-gray">
            1/5
          </div>
          <div class="container__one-fifth bg--light-brown">
            1/5
          </div>
          <div class="container__one-fifth bg--light-gray">
            1/5
          </div>
        </div>
      </div>

      <div class="container__centered" style="margin-bottom:2em;">
        <div class="gl-container">
          <div class="container__one-fourth bg--light-gray">
            1/4
          </div>
          <div class="container__one-fourth bg--light-brown">
            1/4
          </div>
          <div class="container__one-fourth bg--light-gray">
            1/4
          </div>
          <div class="container__one-fourth bg--light-brown">
            1/4
          </div>
        </div>
      </div>

      <div class="container__centered" style="margin-bottom:2em;">
        <div class="gl-container">
          <div class="container__one-third bg--light-gray">
            1/3
          </div>
          <div class="container__one-third bg--light-brown">
            1/3
          </div>
          <div class="container__one-third bg--light-gray">
            1/3
          </div>
        </div>
      </div>

      <div class="container__centered" style="margin-bottom:2em;">
        <div class="gl-container">
          <div class="container__one-half bg--light-gray">
            1/2
          </div>
          <div class="container__one-half bg--light-brown">
            1/2
          </div>
        </div>
      </div>

      <div class="container__centered" style="margin-bottom:2em;">
        <div class="gl-container">
          <div class="container__one-fifth bg--light-gray">
            1/5
          </div>
          <div class="container__four-fifths bg--light-brown">
            4/5
          </div>
        </div>
      </div>

      <div class="container__centered" style="margin-bottom:2em;">
        <div class="gl-container">
          <div class="container__one-fourth bg--light-gray">
            1/4
          </div>
          <div class="container__three-fourths bg--light-brown">
            3/4
          </div>
        </div>
      </div>

      <div class="container__centered">
        <div class="gl-container">
          <div class="container__one-third bg--light-gray">
            1/3
          </div>
          <div class="container__two-thirds bg--light-brown">
            2/3
          </div>
        </div>
      </div>

    </div>

    <h2>Stacked Full Width Gutterless Containers</h2>
    <p>For Gutterless Grid containers that are stacked and full width, we have to change the wrapper class to <code class="language-css">.fw-gl-container</code>.</p>

    <div class="demo-ct">
      <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blurred-hospital-lobby.jpg); margin-bottom:2em;">
        <div class="container__centered">
          <div class="fw-gl-container">
            <div class="container bg--emerald">
              <div class="center">
                <h2>Stacked Full Width</h2>
              </div>
            </div>
            <div class="container center">
              <h2>Stacked Full Width</h2>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Start Stacked Full Width Gutterless Containers Code -->
    <pre><code class="language-html">&lt;!-- Start stacked full width gutterless containers example -->
&lt;div class="container">
  &lt;div class="container__centered">
    &lt;div class="fw-gl-container">
      &lt;div class="container bg--emerald">Stacked Full Width&lt;/div>
      &lt;div class="container">Stacked Full Width&lt;/div>
    &lt;/div>
  &lt;/div>
&lt;/div>
&lt;!-- End stacked full width gutterless containers example -->
</code></pre>
    <!-- End Stacked Full Width Gutterless Containers Code -->

  </div>

  <!-- SIDEBAR -->
  <aside class="container__one-third">

    <div class="sidebar__nav panel">
      <?php
        $styleBlock = module_invoke('menu', 'block_view', 'menu-style-guide-side-nav');
        print render($styleBlock['content']); 
        ?>
    </div>

  </aside>
  <!-- END SIDEBAR -->

</section>

<!-- END style-guide--node-2791.php Gutterless Grid -->
<?php } ?>