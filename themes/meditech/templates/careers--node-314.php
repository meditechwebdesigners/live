<?php $url = $GLOBALS['base_url']; // grabs the site url ?>
<style>
.view-career-events .view-content .table tbody:last-child tr:last-child>td:first-child,
.view-career-events .view-content .table tbody:last-child tr:last-child>td:last-child {
    padding: 0.5em;
  }
</style>
<!-- START careers--node-314.php template CAREER EVENTS page -->
    <section class="container__centered">
      <div class="container__two-thirds">

        <h1 class="page__title js__seo-tool__title"><?php print $title; ?></h1>

        <div class="js__seo-tool__body-content">
          <?php print render($content['field_body']); ?>
        </div>
        
        <div class="js__seo-tool__body-content">
          <?php print views_embed_view('career_events', 'block'); // adds 'career events' Views block... ?>
        </div>
        
      </div><!-- END container__two-thirds -->
      
      <!-- CAREERS SIDEBAR -->
      <aside class="container__one-third panel">
	      <div class="sidebar__nav careers_sidebar_gae">
          <?php
            $massnBlock = module_invoke('menu', 'block_view', 'menu-careers-section-side-nav');
            print render($massnBlock['content']); 
          ?>
        </div>
      </aside>
      <!-- END CAREERS SIDEBAR -->
    
    </section>
    <!-- END SECTION container__centered -->
    
    <?php // VIDEO SECTION =================================================================
      // get value from field_video to pass to View (if video exists)...
      $video = render($content['field_video']);
      if(!empty($video)){ // if the page has a video...
        print '<!-- VIDEO -->';
        print '<div class="js__seo-tool__body-content">';
        // remove apostrophes from titles to prevent View from breaking...
        $video_filtered = str_replace("&#039;", "'", $video);
        // adds 'video' Views block...
        print views_embed_view('video_boxed', 'block', $video_filtered);
        print '</div>';
        print '<!-- END VIDEO -->';
      }
    ?>

    <?php // SEO tool for internal use...
      if(node_access('update',$node)){
        print '<!-- SEO Tool is added to this div -->';
        print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
      } 
    ?> 
<!-- END careers--node-314.php template -->