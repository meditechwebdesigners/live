<!-- START campaign--node-3872.php -->

<?php // This template is set up to control the display of the MEDITECH Post Acute Campaign

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');
$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
	div.button--hubspot {
		margin-bottom: 0;
	}

	.container.pad-adjust {
		padding: 5em 0;
	}

	.container__one-half.pad-adjust {
		padding-top: 5em;
	}

	.shadow-box.w-headshot {
		padding: 4em 2em 2em 2em;
		position: relative;
		margin-top: 50px;
	}

	.shadow-box.w-headshot-outline {
		padding: 4em 2em 2em 2em;
		position: relative;
		margin-top: 50px;
		box-shadow: none;
		border: 1px solid #c8ced9;
		background-color: transparent;
	}

	.shadow-box.outline {
		box-shadow: none;
		border: 1px solid #c8ced9;
		background-color: transparent;
	}

	.headshot {
		position: absolute;
		top: -50px;
		left: 50%;
		transform: translate(-50%);
	}

	.headshot img {
		width: 100px;
		height: 100px;
		border-radius: 50%;
	}

	.gl-text-pad {
		padding: 5em !important;
	}

	.card {
		box-shadow: none;
		background-color: transparent;
	}

	.card__info {
		border: 1px solid #c8ced9;
		border-radius: 0px 0px 7px 7px;
		padding: 2em;
		margin-top: -10px;
	}

	.fa-ul>li {
		margin-bottom: 0.5em;
	}

	@media all and (max-width: 75em) {
		.gl-text-pad {
			padding: 4em !important;
		}
	}

	@media all and (max-width: 59.375em) {
		.container.pad-adjust {
			padding: 3em 0;
		}
	}

	@media all and (max-width: 50em) {
		.container__one-half.pad-adjust {
			padding-top: 0;
		}
	}

</style>

<div class="js__seo-tool__body-content">

	<h1 style="display:none;" class="js__seo-tool__title">MEDITECH Post Acute</h1>

	<!-- START Block 1 -->
	<div class="container no-pad">
		<div class="gl-container">
			<div class="container__one-half gl-text-pad bg--purple-gradient">
				<h2>Care doesn’t just happen in one place. Support greater flexibility and lifelong healing with Expanse.</h2>
				<p>The pandemic has challenged all of us in healthcare to quickly meet the needs of our communities, far beyond traditional settings. Expanded care solutions offer a path to greater flexibility, convenience, and above all, a more connected and holistic approach to caring for patients.</p>
				<p>Whether you are supporting a patient’s recovery following hospitalization, regular therapy visits, or chronic disease management in the home, Expanse will help you to stay connected to patients and facilitate lifelong healing wherever they may be.</p>
				<div style="margin-top:2em;">
					<?php hubspot_button($cta_code, "Register For The 2022 Home Care Symposium"); ?>
				</div>
			</div>
			<div class="container__one-half background--cover flex-order--reverse" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/patient-rehabbing-with-trainer.jpg); min-height:350px;"></div>
		</div>
	</div>
	<!-- END Block 1 -->


	<!-- START Block 2 -->
	<div class="container pad-adjust">
		<div class="container__centered">
			<div class="container__one-half">
				<div class="bg-pattern--container">
					<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/man-looking-out-window-with-coffee.jpg" alt="Man looking out window while holding a coffee">
					<div class="bg-pattern--blue-squares bg-pattern--left"></div>
				</div>
			</div>
			<div class="container__one-half content--pad-left">
				<h2>Easy access to care that heals the mind, body, and spirit</h2>
				<p>Employers predict that mental health and chronic disease needs will increase as employees face the long-term effects of the coronavirus pandemic in 2022.*</p>
				<p>Hear Sanaz Riahi, RN, PhD, Vice President, Practice, Academics and Chief Nursing Executive at Ontario Shores Centre for Mental Health Sciences discuss how her organization supported the <a href="https://meditech-podcast.simplecast.com/episodes/supporting-mental-health-in-ontario-canada" target="_blank">mental health</a> of patients and frontline caregivers throughout the pandemic, as well as the emergence of <a href="https://meditech-podcast.simplecast.com/episodes/emerging-mental-health-apps" target="_blank">mental health apps</a>.</p>
				<p class="text--small">*<a href="https://www.businessgrouphealth.org/en/who-we-are/newsroom/press-releases/health-equity-impact-of-pandemic-among-large-employers-top-concerns" target="_blank">Business Group on Health Survey</a></p>
			</div>
		</div>
	</div>
	<!-- END Block 2 -->


	<!-- START Block 3 -->
	<div class="container bg--purple-gradient">
		<div class="container__centered">

			<div class="center" style="margin-bottom: 2em;">
				<h2>Achieve measurable and improved community outcomes</h2>
				<p>MEDITECH’s <a href="<?php print $url; ?>/ehr-solutions/meditech-ehr-excellence-toolkits">EHR Excellence Toolkits</a> span across care settings, so you can address your top healthcare priorities.</p>
			</div>
			<div class="card__wrapper">
				<div class="container__one-half card">
					<figure>
						<img src="https://blog.meditech.com/hs-fs/hubfs/MEDITECH-Presents-HIMSS-Webinar-on-EHRs-and-Opioid-Crisis--LinkedIn.jpg" alt="A medicine bottle tipped over with pills spilling out">
					</figure>
					<div class="card__info">
						<p class="no-margin--bottom">Hear from Dr. Sarah Porter, Senior Medical Director of Family Practice at Southern Ohio Medical Center, as she discusses the stigma associated with addiction in the <a href="https://blog.meditech.com/chime-opioid-action-center-podcast-addressing-opioid-addiction-stigma">CHIME Opioid Action Center podcast</a>.</p>
					</div>
				</div>
				<div class="container__one-half card">
					<figure>
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/doctor-screening-man-for-depression.jpg" alt="Doctor speaking with patient while using tablet">
					</figure>
					<div class="card__info">
						<p class="no-margin--bottom">After implementing MEDITECH's Depression Screening and Suicide Prevention Toolkit, <a href="https://info.meditech.com/meditech-success-story-coffeyville-regional-medical-center-improves-patient-care-with-meditechs-depression-screening-and-suicide-prevention-toolkit">Coffeyville Regional Medical Center</a> detected five at-risk patients in the first month and improved their Merit-based Incentive Payment System (MIPS) score for the CMS2 measure, increasing from 20 percent to 67 percent attestation.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END Block 3 -->


	<!-- START Block 4 -->
	<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/bg--organic-shapes-green-blue-reversed.svg); background-position: top;">
		<div class="container__centered">
			<div class="auto-margins center" style="margin-bottom: 2em;">
				<h2>Meet patients where they are</h2>
				<p>Expand the power of your <a href="<?php print $url; ?>/ehr-solutions/virtual-care">virtual care</a> offerings, by creating new avenues to care access that include more personalized tools and extended services in a variety of settings.</p>
			</div>
			<div class="container__one-half">
				<div class="shadow-box w-headshot">
					<div class="headshot">
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/louis-harris.png" alt="Dr. Louis Harris head shot">
					</div>
					<p class="italic">“For the nurse to facilitate a visit with the patients at home was just genius and really helpful. It was so helpful to know that the patients felt like they were continuing to get cared for, even in this very strict lockdown where they couldn't see their family anymore. They couldn't do those kinds of things, but they were able to see us and be able to continue to get care that way.”</p>
					<p class="bold">Dr. Louis Harris, CMIO, Citizens Memorial Healthcare</p>
				</div>
			</div>
			<div class="container__one-half">
				<div class="shadow-box w-headshot">
					<div class="headshot">
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/clark-averill.png" alt="Clark Averill head shot">
					</div>
					<p class="italic">“We still continue to see a very large number of virtual visits being done, especially in our mental health clinic. I think people that are undergoing that mental health stress or having issues with their life would really prefer not to have to come in. They're very self-conscious about that type of visit and so virtual visits I think are going to be the new standard way for mental health.”</p>
					<p class="bold">Clark Averill, IT Director, St. Luke's Health System</p>
				</div>
			</div>
		</div>
	</div>
	<!-- END Block 4 -->


	<!-- START Block 5 -->
	<div class="container no-pad">
		<div class="gl-container">
			<div class="container__one-half background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/case-manager-discharging-patient.jpg); background-position:bottom; min-height:400px;"></div>
			<div class="container__one-half gl-text-pad bg--purple-gradient">
				<h2>Facilitate coordinated post-acute care transitions</h2>
				<p>Empower case managers to work with patients and/or their families to review post-acute options and match patients with the appropriate transitional care services for better patient outcomes.</p>
				<p>MEDITECH’s integrated <a href="https://home.meditech.com/en/d/mktcontent/otherfiles/communitycaretransitionportal.pdf" target="_blank">Community Care Transition Portal</a> improves the coordination of transitional case management activities without case managers having to navigate multiple screens or third-party systems.</p>
			</div>
		</div>
	</div>
	<!-- END Block 5 -->


	<!-- START Block 6 -->
	<div class="container no-pad">
		<div class="gl-container">
			<div class="container__one-half gl-text-pad bg--purple-gradient">
				<h2>Deliver personalized care when it really matters</h2>
				<p>The demand for care outside the acute setting is expected to grow in the coming decades as the world’s population ages at a rapid rate*, and as people wish to remain in their homes as long as possible despite changes in health. Our <a href="<?php print $url; ?>/ehr-solutions/meditech-home-care">Home Health</a> solution gives patients a compassionate, personalized experience in accordance with their wishes.</p>
				<p>Some of the most challenging and intimate moments in a patient’s life often occur during hospice care. MEDITECH’s integrated <a href="<?php print $url; ?>/ehr-solutions/meditech-home-care#Ecectpaf">Hospice</a> solution enables care teams to document what matters most to the patient and their family, as well as facilitates getting the right resources and support to address their complex physical and emotional needs.</p>
				<p class="text--small no-margin--bottom">*<a href="https://www.census.gov/topics/population/older-aging.html" target="_blank">https://www.census.gov/topics/population/older-aging.html</a></p>
			</div>
			<div class="container__one-half background--cover flex-order--reverse" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/home-care-worker-with-patient.jpg); min-height:400px;"></div>
		</div>
	</div>
	<!-- END Block 6 -->


	<!-- START Block 7 -->
	<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/bg--organic-shapes-green-blue.svg); background-position:top;">
		<div class="container__centered">
			<div class="container__one-half pad-adjust">
				<h2>Think outside the box of traditional healthcare delivery</h2>
				<p>Rehabilitation services help people become as independent as possible in their everyday activities. Whether a patient is recovering from surgery, an accident, or addressing underlying conditions, the broad range of rehab services aim to restore quality of life.</p>
			</div>
			<div class="container__one-half">
				<div class="shadow-box w-headshot">
					<div class="headshot">
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/carroll-castro.png" alt="Carroll Castro head shot">
					</div>
					<p class="italic">“Traditional health care delivery solutions don’t necessarily fit the rehabilitation setting, which is why we’re always looking at ways to get the most out of our technology. When you have a system that enables you to be nimble, you can improve the quality of care for your patients as well as the degree of satisfaction for your clinicians. We’ll keep innovating because our patients work hard to achieve the highest level of recovery so they can re-enter their lives.”</p>
					<p class="bold">Carroll Castro, MSN, RN, CRRN, WCC, Nursing Informaticist Lead, Brooks Rehabilitation Hospital</p>
					<p><a href="https://blog.meditech.com/thinking-outside-the-box-with-our-ehr-and-surveillance-in-the-rehab-setting">Read Carroll’s blog on Surveillance in the rehab setting.</a></p>
				</div>
			</div>
		</div>
	</div>
	<!-- END Block 7 -->


	<!-- START Block 8 -->
	<div class="container bg--purple-gradient center">
		<div class="container__centered" style="margin-bottom: 2em;">
			<div class="auto-margins" style="margin-bottom: 2em;">
				<h2>Support innovative care delivery models</h2>
				<p>Consider the possibility of improving outcomes, reducing health care costs, and enhancing the patient experience by providing some acute-level care services at home.</p>
			</div>
			<div class="container__one-third shadow-box w-headshot-outline">
				<div class="headshot">
					<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--registration-ordering.png" alt="Icon of a tablet device showing registration options">
				</div>
				<p class="no-margin">The MEDITECH registration, ordering, and clinical applications are set up to handle patients admitted to special locations and beds to account for those patients being treated at home.</p>
			</div>
			<div class="container__one-third shadow-box w-headshot-outline">
				<div class="headshot">
					<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--patient-location.png" alt="Icon of a house with a heart">
				</div>
				<p class="no-margin">By leveraging MEDITECH’s integrated remote monitoring and virtual visit functionality, care teams can monitor and stay connected to patients in their homes.</p>
			</div>
			<div class="container__one-third shadow-box w-headshot-outline">
				<div class="headshot">
					<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--payment-processing.png" alt="Icon of credit cards">
				</div>
				<p>Ordering and documentation for patients use standard routines. </p>
				<p>Billing is handled according to the normal Inpatient Prospective Payment System (IPPS).</p>
			</div>
		</div>
		<div class="container__centered">
			<p>MEDITECH Expanse meets the needs of healthcare organizations providing <a href="https://www.cms.gov/newsroom/press-releases/cms-announces-comprehensive-strategy-enhance-hospital-capacity-amid-covid-19-surge" target="_blank">acute hospital care at home</a> services.</p>
		</div>
	</div>
	<!-- END Block 8 -->


	<!-- START Block 9 -->
	<div class="container">
		<div class="container__centered">
			<div class="container__one-half" style="margin-top: 1em;">
				<div class="bg-pattern--container">
					<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/female-nurse-with-elderly-patient.jpg" alt="Female nurse assisting elderly patient">
					<div class="bg-pattern--green-squares bg-pattern--left"></div>
				</div>
			</div>
			<div class="container__one-half content--pad-left">
				<h2>Navigate the next level of continuing care</h2>
				<p>Our single, integrated EHR provides the clinical, administrative, and financial tools to manage the complex needs of patients and residents across different environments, including long term care, long term acute care, and skilled nursing facilities.</p>
				<ul class="fa-ul">
					<li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>
						<p>Flexible Care Plans</p>
					</li>
					<li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>
						<p>Support for MDS Requirements</p>
					</li>
					<li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>
						<p>Leave of Absence Functionality</p>
					</li>
					<li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>
						<p>Split and Consolidated Billing</p>
					</li>
					<li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>
						<p>Medicare/Medicaid Bed Certification</p>
					</li>
					<li class="no-margin--bottom"><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>
						<p>Resident Financials and Allowances</p>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- END Block 9 -->


	<!-- START Block 10 -->
	<div class="container bg--purple-gradient">
		<div class="container__centered auto-margins center">
			<?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
			<h2>
				<?php print $cta->field_header_1['und'][0]['value']; ?>
			</h2>
			<?php } ?>

			<?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
			<div>
				<?php print $cta->field_long_text_1['und'][0]['value']; ?>
			</div>
			<?php } ?>
			<div class="center" style="margin-top:2em;">
				<?php hubspot_button($cta_code, "Example Button TBD"); ?>
			</div>
			<div style="margin-top:2em;">
				<?php print $share_link_buttons; ?>
			</div>
		</div>
	</div>
	<!-- END Block 10 -->


</div><!-- end js__seo-tool__body-content -->


<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>
<!-- end campaign--node-3872.php template -->
