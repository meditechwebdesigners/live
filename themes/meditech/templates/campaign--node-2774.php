<!-- START campaign--node-2774.php -->
<?php // This template is set up to control the display of the campaign population health content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
    .text--beige {
        color: #FDE598;
    }

    .angled-bg--solid-top-left {
        background-image: linear-gradient(240deg, transparent 49.9%, rgba(27, 66, 83, 1) 50%);
    }

    @media all and (max-width: 984px) {
        .angled-bg--solid-top-left {
            background-image: linear-gradient(242deg, transparent 49.9%, rgba(27, 66, 83, 1) 50%);

        }

        @media all and (max-width: 936px) {
            .angled-bg--solid-top-left {
                background-image: linear-gradient(246deg, transparent 49.9%, rgba(27, 66, 83, 1) 50%);

            }

            @media all and (max-width: 882px) {
                .angled-bg--solid-top-left {
                    background-image: linear-gradient(250deg, transparent 49.9%, rgba(27, 66, 83, 1) 50%);

                }

                @media all and (max-width: 800px) {
                    .angled-bg--solid-top-left {
                        background-image: linear-gradient(250deg, transparent -100%, rgba(27, 66, 83, 1) 50%);

                    }

</style>

<div class="js__seo-tool__body-content">

    <!-- Block 1 Hero -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/people-collage-light.jpg);">
        <div class="container__centered">
            <div class="container__one-half transparent-overlay--xp text--white" style="background-color: rgba(0, 0, 0, 0.8);">
                <h1 class="js__seo-tool__title">See population health through a clear lens</h1>
                <p>If there's one thing care teams need when tackling population health, it's clarity. With the integrated solutions of MEDITECH’s Expanse Population Health Management Platform, you’ll have the tools you need to get a clear picture of your patient populations &mdash; who they are, where they've been, and where they're going. And you'll have the functionality to support individual patients and help them manage health risks at every stage of life &mdash; no matter where their care journey takes them.
                </p>

                <div class="btn-holder--content__callout">
                    <div class="center" style="margin-top:2em;">
                        <?php hubspot_button($cta_code, "Sign Up For The Population Health Insights Webinar"); ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- End Block 1 -->


    <!-- Block 2 Patient story slider -->
    <div class="container background--cover hide__bg-image--mobile" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/family-happy-together-gradient.jpg); background-position: top; background-color:#045748;">

        <div class="container__centered">
            <div class="container__centered auto-margins text--white">
                <h2>It's all about the patient</h2>
                <p>Population health is about more than trends and reporting; it's really about meeting patients where they are. Let's take a look at how an effective population health strategy could impact the lives of four very different people.</p>
            </div>

            <div class="slider text--white">

                <!-- Slider1 -->
                <div>
                    <div class="container__one-half">
                        <h2 class="text--beige" style="padding-bottom:15px;">David, 8 years old</h2>
                        <h4 class="text--beige">Patient Profile:</h4>
                        <ul>
                            <li>Has asthma</li>
                            <li>Loves to draw</li>
                            <li>Goes to day care after school</li>
                            <li>Gets frequent colds</li>
                        </ul>
                        <h4 class="text--beige">Patient Portal Use:</h4>
                        <ul>
                            <li>His mother uses his Patient Portal to schedule wellness appointments and virtual visits with his pediatrician.</li>
                            <li>She also uses the portal to manage David's immunizations, renew his asthma medications, and email his doctor.</li>
                        </ul>
                    </div>
                    <div class="container__one-half" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/david-smiling-in-rain.jpg); height: 600px; background-position: top right; background-size: 100%; background-repeat: no-repeat;">
                        &nbsp;
                    </div>
                </div>
                <!-- End Slider1 -->

                <!-- Slider2 -->
                <div>
                    <div class="container__one-half">
                        <h2 class="text--beige" style="padding-bottom:15px;">Sarah, 30 years old</h2>
                        <h4 class="text--beige">Patient Profile:</h4>
                        <ul>
                            <li>Active</li>
                            <li>Generally healthy</li>
                            <li>Loves the outdoors</li>
                            <li>Celiac disease</li>
                            <li>Only sees physician when feeling sick</li>
                        </ul>
                        <h4 class="text--beige">Patient Portal Use:</h4>
                        <ul>
                            <li>Patient Portal reminds her to schedule flu shots and annual wellness visits, and includes convenient access to lab results.</li>
                            <li>Uses Patient Generated Health Data with wearables to monitor gluten-free diet and fitness goals. This data is integrated into her EHR.</li>
                        </ul>
                    </div>
                    <div class="container__one-half" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/sarah-watering-garden.jpg); height: 600px; background-position: top right; background-size: 100%; background-repeat: no-repeat;">
                        &nbsp;
                    </div>
                </div>
                <!-- End Slider2 -->

                <!-- Slider3 -->
                <div>
                    <div class="container__one-half">
                        <h2 class="text--beige" style="padding-bottom:15px;">Patricia, 58 years old</h2>
                        <h4 class="text--beige">Patient Profile:</h4>
                        <ul>
                            <li>High blood pressure</li>
                            <li>Mild arthritis</li>
                            <li>Family history of breast cancer</li>
                            <li>Exercises infrequently</li>
                            <li>Active in the community</li>
                        </ul>
                        <h4 class="text--beige">Patient Portal Use:</h4>
                        <ul>
                            <li>Uses the Patient Portal to track her test results, appointments, and provider emails.</li>
                            <li>Protocols and registries help care navigators to track and schedule wellness visits, including annual mammograms.</li>
                            <li>Portal sends her information on yoga classes at the local hospital, to help with arthritis pain.</li>
                        </ul>
                    </div>
                    <div class="container__one-half" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/patricia-leaning-on-window.jpg); height: 600px; background-position: top right; background-size: 100%; background-repeat: no-repeat;">
                        &nbsp;
                    </div>
                </div>
                <!-- End Slider3 -->

                <!-- Slider 4-->
                <div>
                    <div class="container__one-half">
                        <h2 class="text--beige" style="padding-bottom:15px;">Richard, 76 years old</h2>
                        <h4 class="text--beige">Patient Profile:</h4>
                        <ul>
                            <li>Insulin-dependent diabetic</li>
                            <li>Congestive heart failure</li>
                            <li>Limited mobility</li>
                            <li>Lives alone in rural setting</li>
                        </ul>
                        <h4 class="text--beige">Patient Portal Use:</h4>
                        <ul>
                            <li>Richard's daughter accesses his information through the Patient Portal, to help manage his ongoing care.</li>
                            <li>Now using home care services, telehealth devices send vitals to his care team.</li>
                            <li>Virtual visits enable convenient, regular provider communication.</li>
                        </ul>
                    </div>
                    <div class="container__one-half" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/richard-wheel-chair.jpg); height: 600px; background-position: top right; background-size: 100%; background-repeat: no-repeat;">
                        &nbsp;
                    </div>
                </div>
                <!-- End Slider 4-->

            </div>
        </div>
    </div>

    <link rel="stylesheet" href="<?php print $url; ?>/sites/all/themes/meditech/css/slick.css">
    <link rel="stylesheet" href="<?php print $url; ?>/sites/all/themes/meditech/css/slick-theme.css">
    <script src="<?php print $url; ?>/sites/all/themes/meditech/js/slick.min.js"></script>
    <script>
        jQuery(document).ready(function($) {
            $('.slider').slick({
                dots: true,
                prevArrow: '<a class="slick-prev">&#10092;</a>',
                nextArrow: '<a class="slick-next">&#10093;</a>',
            });
        });

    </script>
    <!-- End of Block 2 -->

    <!-- Block 3 - Patient Registry Screenshot -->
    <div class="container bg--emerald">
        <div class="container__centered">
            <div class="auto-margins center" style="padding-bottom: 1em;">
                <h2>Take action with Patient Registries</h2>
                <p>Our actionable Patient Registries in <a href="<?php print $url; ?>/ehr-solutions/care-compass">Care Compass</a> are bolstered by the supplemental information, including claims and a broad range of disparate EHR data, generated by collaboration with data aggregators. Care teams can examine entire groups of patients, determine who they're accountable for, and decide on the appropriate interventions — all from just one screen. By identifying at-risk patients, gaps in care, and overdue health maintenance, our Patient Registries can help your organization promote wellness and take a more proactive approach to care coordination and disease management.
                </p>
            </div>
            <div class="auto-margins">
                <div id="modal1" class="modal">
                    <a class="close-modal" href="javascript:void(0)">&times;</a>
                    <div class="modal-content">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/screenshot--enrolled-in-CCM-program.png" alt="Patient Registry Screenshot">
                    </div>
                </div>

                <div class="open-modal" data-target="modal1">
                    <div class="tablet--white">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/screenshot--enrolled-in-CCM-program.png" alt="Patient Registry Screenshot">
                    </div>
                    <div class="mag-bg">
                        <i class="mag-icon fas fa-search-plus"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Block 3 -->

    <!-- Block 4 Close Gaps in Care -->

    <!--
    <div class="container background--cover no-pad" style="background-image:url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Bridging_gaps_hiker_tech_overlay.jpg);">
        <div class="angled-bg--solid-top-left" style="padding:5em 0;">
            <div class="container__centered">
                <div class="container__one-third text--white">
                    <h2>
                        Close gaps in care
                    </h2>
                    <p>
                        Care gaps typically happen when care deviates from recommended best practice &#8212; and the frequency of these gaps tends to increase when treating higher volumes of complex patients. With Expanse, care teams can use customizable chart widgets to flag common care gaps, so physicians have actionable information queued up upon entering the exam room. This functionality eliminates paper processes and the need to navigate multiple screens, giving clinicians a smoother, simpler, and gap-free workflow.
                    </p>
                </div>
                <div class="container__two container__two-thirds">
                    &nbsp;
                </div>
            </div>
        </div>
    </div>
-->

    <div class="container background--cover no-pad" style="background-image:url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Bridging_gaps_hiker_tech_overlay.jpg);">
        <div class="angled-bg--solid-top-left" style="padding:5em 0;">
            <div class="container__centered">
                <div class="container__one-third text--white">
                    <h2>
                        Close gaps in care
                    </h2>
                    <p>
                        Proactively identify opportunities for care gap closure to maintain wellness and manage chronic conditions. With Expanse, care teams can use customizable chart widgets to flag common care gaps, so physicians have actionable information queued up upon entering the exam room. This functionality eliminates paper processes and the need to navigate multiple screens, giving clinicians a smoother, simpler, and gap-free workflow.
                    </p>
                </div>
                <div class="container__two container__two-thirds">
                    &nbsp;
                </div>
            </div>
        </div>
    </div>

    <!-- End of Block 4 Close Gaps in Care -->

    <!-- Block 5 - MEDITECH Customers -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/hands-together-6.jpg);">
        <div class="container__centered">
            <div class="page__title--center">
                <div class="container no-pad auto-margins">
                    <h2>
                        Address Social Determinants of Health
                    </h2>
                    <p>
                        Deliver care that reaches far beyond the hospital and physician's office. Connect patients with social services and community resources, and use Expanse to help them take better control over their own health.
                    </p>
                </div>
                <div class="container no-pad--top text--white center">

                    <h4 style="margin-bottom: 1.5em; color:#3e4545;">
                        See how MEDITECH customers are addressing social determinants of health in their communities:
                    </h4>
                    <div class="container__one-fourth transparent-overlay" style="background-color: rgba(0, 0, 0, 0.8);">
                        <h3>
                            <a href="https://blog.meditech.com/the-importance-of-social-determinants-in-behavioral-health">Bristol Hospital</a>
                        </h3>
                        <p>
                            Used surveys to identify patients in need of additional resources or support, and deployed local nursing students to follow up with patients in need.
                        </p>
                    </div>
                    <div class="container__one-fourth transparent-overlay" style="background-color: rgba(0, 0, 0, 0.8);">
                        <h3>
                            <a href="https://blog.meditech.com/beyond-numbers-creative-ways-to-address-population-health">Frisbie Memorial Hospital</a>
                        </h3>
                        <p>
                            Found that food insecurity was the root of their super utilizer problem in the ED, as many patients presented only to receive a hot meal. Frisbie partnered with community organizations to host weekly potluck dinners that kept community members properly fed and out of the hospital.
                        </p>
                    </div>
                    <div class="container__one-fourth transparent-overlay" style="background-color: rgba(0, 0, 0, 0.8);">
                        <h3>
                            <a href="https://blog.meditech.com/grocery-store-walkthroughs-one-step-toward-improving-health-literacy">Kalispell Regional Medical Center</a>
                        </h3>
                        <p>
                            Established a program in which community health workers take Congestive Heart Failure patients on walkthroughs of their local grocery store to combat health illiteracy and help them establish healthy diets.
                        </p>
                    </div>
                    <div class="container__one-fourth transparent-overlay" style="background-color: rgba(0, 0, 0, 0.8);">
                        <h3>
                            <a href="https://blog.meditech.com/how-providers-can-bridge-care-gaps-caused-by-social-determinants-of-health">Southwestern Vermont Medical Center</a>
                        </h3>
                        <p>
                            Designed a transitional care program aimed at addressing the gaps in care that occured due to the social determinants of health affecting their rural community.
                        </p>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- End of Block 5 -->

    <!-- Block 6 - Video -->
    <div class="content__callout">
        <div class="content__callout__media">
            <div class="content__callout__image-wrapper">
                <div class="video js__video" data-video-id="288754788">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--PPGH-Bridge-to-Health.jpg" alt="Palo Pinto Mobile Health Initiative Video Covershot">
                    </figure>
                    <a class="video__play-btn" href="https://vimeo.com/288754788?"></a>
                    <div class="video__container"></div>
                </div>
            </div>
        </div>
        <div class="content__callout__content">
            <div class="content__callout__body">
                <div class="content__callout__body__text">
                    <h2>Mobile health initiatives go on the road</h2>
                    <p>All aboard Palo Pinto's mobile health clinic! In this video, see how Palo Pinto General Hospital's fully-equipped medical bus gives their community a “bridge to health,” and enables providers to truly meet their patients where they are.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Block 6 -->

    <!-- Block 7 - Portal Screenshot -->
    <!--
    <div class="container bg--emerald">
        <div class="container__centered">
            <div class="auto-margins center" style="padding-bottom: 1em;">
                <h2>Support your decisions with solid population analytics</h2>
                <p>Back your population health initiatives with data that gives you a complete view of outcomes and utilization. MEDITECH's <a href="https://ehr.meditech.com/ehr-solutions/meditech-business-clinical-analytics">Business and Clinical Analytics</a> solution lets strategic teams look for trends across patient populations, including readmission rates, chronic conditions, and gaps in care. Armed with this data, you can target programs that deliver the highest quality care at the lowest cost. With the use of embedded clinical workflows and vendor-supplied data, care teams can fill in the patient story, for an accurate and meaningful patient encounter every time.</p>
            </div>
            <div class="auto-margins">
                <div id="modal2" class="modal">
                    <a class="close-modal" href="javascript:void(0)">&times;</a>
                    <div class="modal-content">
                        <img src="< ?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Utilization-screen-grab.jpg" alt="population analytics">
                    </div>
                </div>

                <div class="open-modal" data-target="modal2">
                    <div class="tablet--white">
                        <img src="< ?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Utilization-screen-grab.jpg" alt="population analytics">
                    </div>
                    <div class="mag-bg">
                        <i class="mag-icon fas fa-search-plus"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
-->

    <div class="container bg--emerald">
        <div class="container__centered">
            <div class="auto-margins center" style="padding-bottom: 1em;">
                <h2>Support your decisions with solid population analytics</h2>
                <p>Back your population health initiatives with data that gives you a complete view of outcomes and utilization. MEDITECH's <a href="https://ehr.meditech.com/ehr-solutions/meditech-business-clinical-analytics">Business and Clinical Analytics</a> solution lets strategic teams look for patterns across patient populations, including the prevalence of chronic conditions, risk gaps, and opportunities for care gap closure. Armed with this data, you can target programs that deliver the highest quality care at the lowest cost. With the use of embedded clinical workflows and vendor-supplied data, care teams can fill in the patient story, for an accurate and meaningful patient encounter every time.</p>
            </div>
            <div class="auto-margins">
                <div id="modal2" class="modal">
                    <a class="close-modal" href="javascript:void(0)">&times;</a>
                    <div class="modal-content">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-screenshot--care-gaps-patient-detail.png" alt="population analytics">
                    </div>
                </div>

                <div class="open-modal" data-target="modal2">
                    <div class="tablet--white">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-screenshot--care-gaps-patient-detail.png" alt="population analytics">
                    </div>
                    <div class="mag-bg">
                        <i class="mag-icon fas fa-search-plus"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- End of Block 7 -->

    <!-- Block 8 - Patient centered image -->
    <div class="container background--cover hide__bg-image--mobile" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/happy-patient-with-dr3.jpg); background-color: #e6e9ee;">

        <div class="container__centered">
            <div class="container__one-half">
                &nbsp;
            </div>
            <div class="container__one-half transparent-overlay text--white" style="background-color: rgba(102, 108, 119, 0.8);">
                <h2> Interoperable care is connected care</h2>
                <p>True <a href="<?php print $url; ?>/ehr-solutions/meditech-interoperability">interoperability</a> connects communities and enables better population health strategies by supporting the sharing of patient information across all systems. This means that no matter where a patient seeks care, their care team will have the information they need to deliver better care and outcomes.</p>

                <p>As a Contributor Member of the CommonWell Health Alliance, MEDITECH's interoperability efforts extend beyond local and regional affiliations. The patient's information goes where they go, so clinicians can always treat the person and not just the problem — a crucial component of value-based care.</p>
            </div>
        </div>
    </div>
    <!-- End of Block 8 -->

    <!-- Block 9 - Revenue Cycle -->
    <div class="container background" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/financial-background.png);">
        <div class="container__centered">
            <div class="container__three-fourths transparent-overlay text--white" style="background-color: rgba(7, 79, 65, 0.7);">
                <h2>Revenue Cycle Reimbursement</h2>
                <p>MEDITECH will also help you navigate the shift to value-based care while <a href="https://ehr.meditech.com/ehr-solutions/meditechs-revenue-cycle">keeping your organization financially viable.</a> Unite all care fronts and cover patient access, middle, and back office processes to boost financial performance. A truly centralized business office and consolidated revenue cycle reports allow you to monitor AR days, cash flow, and denials to ensure your organization maintains healthy margins.
                </p>
            </div>

            <div class="container__one-fourth" style="text-align:center; margin-top: 1em;">
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Financial-cycle-icon.png" alt="population analytics">
            </div>
        </div>
    </div>
    <!-- End of Block 9 -->


</div>
<!-- end js__seo-tool__body-content -->

<!-- Block 9 - CTA -->
<div class="container background" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-boxes-pattern.jpg);">
    <div class="container__centered center text--white">

        <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
        <h2>
            <?php print $cta->field_header_1['und'][0]['value']; ?>
        </h2>
        <?php } ?>

        <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
        <div>
            <?php print $cta->field_long_text_1['und'][0]['value']; ?>
        </div>
        <?php } ?>

        <div class="center" style="margin-top:2em;">
            <?php hubspot_button($cta_code, "Download eBook Now"); ?>
        </div>

        <div style="margin-top:1em;">
            <?php print $share_link_buttons; ?>
        </div>

    </div>
</div>
<!-- End of Block 9 -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>
<!-- END campaign--node-2774.php -->
