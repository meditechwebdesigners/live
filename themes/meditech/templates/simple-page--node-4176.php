<!-- start simple-page--node-4176.tpl.php template -- Joint Commission -->

  <div class="container">
    <div class="container__centered">
      <h1><?php print $title; ?></h1>
      <h2>2022 National Patient Safety Goals</h2>
    </div>
  </div>
  
  <div class="container__centered">
   
    <div class="container__two-thirds">
      
      <p><a href="https://www.jointcommission.org/standards/national-patient-safety-goals/" target="_blank">The Joint Commission 2022 National Patient Safety Goals</a> set forth clear guidelines for how healthcare organizations can improve the quality of care. As the following sections illustrate, MEDITECH's Electronic Health Record (EHR) provides many tools to help organizations increase patient safety and meet The Joint Commission's goals.</p>

      <div class="container no-pad--top">
           
        <!-- Accordions -->
        <div class="accordion">
          <ul class="accordion__list">

            <li class="accordion__list__item"> 
              <a class="accordion__link" href="#">Goal #1: Improve the accuracy of patient identification.<div class="accordion__list__control"></div></a>
              <div class="accordion__dropdown">
                <p><strong>NPSG.01.01.01</strong><br>Use at least two ways to identify patients. For example, use the patient's name <em>and</em> date of birth. This is done to make sure that each patient gets the correct medicine and treatment.</p>

                <h4>MEDITECH Tools</h4>

                <p>MEDITECH's EHR ensures positive patient identification. In conjunction with swipe technology, caregivers can use filed medical numbers, health insurance numbers, account numbers, names, and birth dates to identify their patients.</p>

                <p>MEDITECH's EHR helps to ensure safe medication administration through the Five Rights of medication management: Right Patient, Right Medication, Right Dosage, Right Route, and Right Time. To ensure that the right patient gets the right medication, MEDITECH's system offers a variety of patient identifiers, including the ability to scan bar codes on patient wristbands and medications to correctly identify the patient and the appropriate medication. Caregivers utilize bar code scanning technology prior to administering medications to confirm patient identity and medication information against data readily available via MEDITECH's on-line Medication Administration Record.</p>

                <p>MEDITECH's Blood Bank and Patient Care and Patient Safety solutions are fully integrated, fostering the sharing of Blood Bank unit and patient data between the two products. All data relevant to transfusions is updated in real time in the Transfusion Administration Record (TAR), a component of our Patient Care and Patient Safety solution.</p>

                <p>The TAR, accessible to both nurses and Blood Bank personnel, is an electronic method for documenting patient vital signs and transfusion reactions before, during, and after blood transfusions. When used in conjunction with MEDITECH's Bedside Verification, staff can use bar code readers to scan and match patients with blood products to verify the right patient, blood product, and transfusion time, as well as donor blood type compatibility and blood product expiration dates.</p>
                
                <p>While in the Operating Room area a patient can be identified by using a combination of methods such as the patient's name, date of birth, account number, medical record number, case number, and scanning the armband. Additionally, the World Health Organization's (WHO) Surgical Safety Checklist has been embedded within the timeout section of the surgical chart in an effort to make sure each patient gets the correct treatment.</p>
                
                <p>The Imaging & Therapeutic Services / Imaging & Documentation Management application allows a patient to be identified by utilizing any of the available methods such as the patient's name, date of birth, account number, medical record number, requisition# and accession#.  The requisition and accession#'s allow a given patient to be uniquely identified as well as the associated order(s).  This not only helps streamline entry, but uniquely identifies both the patient and the associated order(s).</p>

              <a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Goal #1</a>  
              </div>
            </li>

            <li class="accordion__list__item">
              <a class="accordion__link" href="#">Goal #2: Improve the effectiveness of communication among caregivers.<div class="accordion__list__control"></div></a>
              <div class="accordion__dropdown">
                <p><strong>NPSG.02.03.01</strong><br>Report critical results of tests and diagnostic procedures on a timely basis.</p>

                <h4>MEDITECH Tools</h4>

                <p>MEDITECH's fully-integrated system facilitates comprehensive, enterprise-wide communication throughout a healthcare organization. Physician desktops and nursing status boards act as global views of caregivers' patients and serve as central points from which to process all aspects of patient care. Status Boards can display new test results, with abnormal values flagged. Notifications can be created if needed for abnormal results.</p>

                <p>MEDITECH's table-driven system helps your organization meet The Joint Commission's list of "Do Not Use" abbreviations, acronyms, and symbols. Relevant clinical data displays provide readily available, real-time patient information when managing medication therapies at the point of order entry and during medication administration. When ordering or administering medications, clinicians have the right information at the right time for critical decision making.</p>

                <p>Integrated capabilities throughout the MEDITECH EHR ensure coordinated and safe ordering processes. The entire care team, including pharmacists, nurses, laboratory technicians, and radiology technicians, are included in the physician-initiated process. Physicians can also sign verbal orders and view results anywhere throughout the continuum.</p>
                
                <p>MEDITECH's Expanse Patient Care's Handoff routine can be leveraged to facilitate transitions of care and shift change communication among care providers, replacing the current practice of documenting on paper. Customized handoff formats are embedded directly into the home screen or tracker to create a concise, streamlined workflow for capturing both objective and subjective data. </p>
                
                <p>Special Indicator data items can be added to the trackers in the Emergency Department and in Surgical Services to facilitate communication between caregivers. Result Indicators can be utilized on the Big Boards in Operating Room Management (ORM) and Trackers in Surgical Services (SUR). Additionally, the World Health Organization's (WHO) Surgical Safety Checklist, embedded within the timeout section of the surgical chart, helps to decrease errors and adverse events and increase teamwork and communication in surgery.</p>
                
                <p>The MEDITECH Imaging & Therapeutic Services / Imaging & Documentation Management routine allows for a customized public and private tracker (6.1 & Expanse releases.)  This allows for the data on the screen to be tailored to the users given workflow, helping improve clinical decision-making and patient safety accordingly. Additionally, standard order and report-based desktops allow for data to appear in an organized fashion based on user preference (All Platforms.)</p>
                
                <p>The New Activity List in Pharmacy (PHA) allows pharmacists to review changes to patient accounts, including order activity and test results.  For orders that are generated via computerized physician order entry (CPOE), Pharmacists can utilize the New Activity List to review and verify orders generated outside of the Pharmacy as well.</p>
                
                <a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Goal #2</a>
              </div>
            </li>

            <li class="accordion__list__item">
              <a class="accordion__link" href="#">Goal #3: Improve the safety of using medications.<div class="accordion__list__control"></div></a>
              <div class="accordion__dropdown">
                <p><strong>NPSG.03.04.01</strong><br>Before a procedure, label medicines that are not labeled. For example, medicines in syringes, cups and basins. Do this in the area where medicines and supplies are set up.</p>

                <p><strong>NPSG.03.05.01</strong><br>Take extra care with patients who take medicines to thin their blood.</p>

                <p><strong>NPSG.03.06.01</strong><br>Record and pass along correct information about a patient's medicines. Find out what medicines the patient is taking. Compare those medicines to new medicines given to the patient. Give the patient written information about the medicines they need to take. Tell the patient it is important to bring their up-to-date list of medicines every time they visit a doctor.</p>

                <h4>MEDITECH Tools</h4>

                <p>MEDITECH supports users in providing the capability of printing labels in supporting applications. Labels can be printed for each drug dose, the medication carts and for e-MAR scanning.</p>

                <p>MEDITECH's clinical applications can provide alerts for patients on blood thinning medications and can highlight dosage and interaction warnings. Nurses can also get easy access to the patient's latest international normalized ratio (INR) values.</p>

                <p>Staff can use MEDITECH's clinical applications to record, edit, and retrieve Active Medication Lists and accurately collect home medication information during triage or the admission process using structured data. The Active Medication List consists of available information for medications prescribed outside of the hospital network or taken over-the-counter. During a patient's visit, any medication orders placed using computerized provider order entry (CPOE) or Pharmacy (PHA) appear on the patient's Active Medication List. Medication lists, containing both home and inpatient medications, can be viewed within the Medication Reconciliation routine, as well as via the Summary Panel in the patient's electronic record. Full medication information and instructions are provided to patients at discharge.</p>
                
                <p>There are several ways in which medications can be used safely while a patient is in the Operating Room area such as printing labels for syringes for each drug dose, the medication carts, and for e-MAR scanning. Med Reconciliation and Medication Lists can be utilized to review a patient's medication. In addition, PHA rules and label comments are applicable to Operating Room Management (ORM) and Surgical Services (SUR). Bedside Medication Verification (BMV) can be implemented in the preoperative and postoperative phases of the surgical encounter to ensure the 5 patient rights are followed and help avoid potential medical administration errors.</p>
                
                <p>The Pharmacy product ensures patient safety by performing medication conflict checking when orders are created, edited and verified.  Conflict checking capabilities include allergy interactions, drug-drug, drug-food, and drug-condition interactions, IV incompatibilities, contraindications, duplicate medications, dose range checking and lifetime dose checks.</p>
                
                <a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Goal #3</a>
              </div>
            </li>

            <li class="accordion__list__item">
              <a class="accordion__link" href="#">Goal #7: Reduce the risk of health care-associated infections.<div class="accordion__list__control"></div></a>
              <div class="accordion__dropdown">
                <p><strong>NPSG.07.01.01</strong><br>Use the hand cleaning guidelines from the <a href="https://www.cdc.gov/handhygiene/">Centers for Disease Control and Prevention</a> or the <a href="https://www.who.int/gpsc/5may/tools/who_guidelines-handhygiene_summary.pdf">World Health Organization</a>. Set goals for improving hand cleaning. Use the goals to improve hand cleaning.</p>

                <h4>MEDITECH Tools</h4>

                <p>The infection control functionality of MEDITECH's Laboratory Information System helps providers effectively and efficiently identify healthcare-associated infections. An organization's infection control group can easily track patient infections by automatically flagging organisms with markers or through other Customer-Defined screens. Staff can generate reports based on patient, location, physician, site of the infection, and type of organism–whether sensitive or resistant to certain antibiotics. Depending on the outcome of the reports, the infection control group can automatically receive alerts to help track nosocomial infections.</p>

                <p>Evidence Based Links associated with Interventions and Outcomes provide valuable information to assist care providers in preventing infections. Status Board Condition Lists also provide a method of monitoring patient specific indicators and lab results that can be acted upon as needed to prevent infection from developing.</p>

                <p>Authorized clinicians can also pull data from the MEDITECH EHR and store it in a secure database for robust reporting and benchmarking. Organizations can track and report on a variety of issues such as patient and visitor incidents, adverse drug events, employee health and safety, blood utilization, and infections with complete confidentiality. Staff can effectively analyze their efforts and devise strategies for improving outcomes and regulatory compliance.</p>
                
                <p>MEDITECH has several toolkits and Best Practices that can be utilized to assist in reducing the risk of healthcare associated infections such as the:</p>
                <ul>
                  <li><a href="https://customer.meditech.com/en/d/ehrtoolkits/pages/cautigettingstarted.htm">Catheter-Associated Urinary Tract Infections (CAUTI) Toolkit.</a></li>
                  <li><a href="https://customer.meditech.com/en/d/ehrtoolkits/pages/sepsisgettingstarted.htm">Sepsis Management Toolkit.</a></li>
                  <li><a href="https://customer.meditech.com/en/d/bestpracticesexp/otherfiles/omebbpmdroresponseandcontainmentguidelines.pdf">Multidrug-Resistant Organism (MDRO) Response & Containment Guidelines.</a></li>
                </ul>
                
                <a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Goal #7</a>
              </div>
            </li>

            <li class="accordion__list__item">
              <a class="accordion__link" href="#">Goal #9: Reduce the risk of patient harm resulting from falls.<div class="accordion__list__control"></div></a>
              <div class="accordion__dropdown">
                <p><strong>NPSG.09.02.01</strong><br>Find out which patients are most likely to fall. For example, is the patient taking any medicines that might make them weak, dizzy or sleepy? Take action to prevent falls for these patients.</p>

                <h4>MEDITECH Tools</h4>

                <p>Authorized care providers can use MEDITECH's clinical applications to promote proactive fall risk management best practices across the continuum of care. Built-in nursing assessment algorithms — adapted for both the ambulatory and acute care settings — use sophisticated rules and calculations to evaluate the patient's risk level, requiring care team members to take precautionary measures. Electronic alerts communicate the patient's increased risk, cueing care team members to take action. Finally, integrated clinical decision support (CDS) embedded in nursing, ordering, and documentation tools streamlines workflow — facilitating effective identification and management to improve outcomes.</p>
                
                <p>See <a href="https://customer.meditech.com/en/d/ehrtoolkits/pages/6xfallriskgettingstarted.htm" target="_blank">MEDITECH's Expanse Fall Risk Management Toolkit</a>.</p>
                
                <a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Goal #9</a>
              </div>
            </li>

            <li class="accordion__list__item">
              <a class="accordion__link" href="#">Goal #14: Prevent health care-associated pressure ulcers (decubitus ulcers).<div class="accordion__list__control"></div></a>
              <div class="accordion__dropdown">
                <p><strong>NPSG.14.01.01</strong><br>Find out which residents are most likely to have bed sores. Take action to prevent bed sores in these patients and residents. From time to time, re-check residents for bed sores.</p>

                <h4>MEDITECH Tools</h4>

                  <p>Care providers can use MEDITECH's <a href="https://ehr.meditech.com/ehr-solutions/meditech-nursing">Patient Care System</a> to document assessments and interventions at the point of care, including documentation on the integumentary system, wounds, and pressure injuries. The use of directions on assessments can also trigger a visual reminder that the assessment is due on the nurse’s Status Board. Caregivers can also document risk assessments such as the Braden Scale to identify patients who may be at risk for developing a pressure ulcer during their encounter. In addition, care providers can annotate images to document details about injuries or wounds.</p>
                  
                  <p>Quality management (QM) teams can monitor all patients with known or suspected pressure injuries housewide using MEDITECH’s <a href="https://ehr.meditech.com/ehr-solutions/meditech-surveillance">Quality Management Surveillance</a> solution. <a href="https://customer.meditech.com/en/d/prwqm/pages/qrmeifeasinformation.htm">Surveillance profiles</a> create cohorts of patients that are being monitored for different conditions, such as those with documented pressure ulcers, those with advanced-stage pressure ulcers, and those at risk of developing pressure ulcers based on elevated risk screening results. Quality managers or clinical staff can take action directly from surveillance profiles, initiating order sets, care plans, or physician notifications.</p>

                <p>Finally, clinicians can use Status Boards and rounding lists to continue monitoring patients with pressure ulcers, including:</p>

                <ul>
                    <li>Viewing pertinent client demographic and clinical data.</li>
                    <li>Quickly scanning due/overdue interventions, orders, and medications.</li>
                    <li>Displaying query responses such as risk levels and current pressure ulcers so they can be tracked and easily monitored.</li>
                </ul>

                <a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Goal #14</a>
              </div>
            </li>

            <li class="accordion__list__item">
              <a class="accordion__link" href="#">Goal #15: The organization identifies safety risks inherent in its patient population.<div class="accordion__list__control"></div></a>
              <div class="accordion__dropdown">
                <p><strong>NPSG.15.01.01</strong><br>Reduce the risk for suicide.</p>

                <p><strong>NPSG.15.02.01</strong><br>Find out if there are any risks for patients who are getting oxygen. For example, fires in the patient's home.</p>

                <h4>MEDITECH Tools</h4>

                <p>Authorized care providers can use MEDITECH's clinical applications to document standard and user-defined assessments which help caregivers identify at-risk patients, as well as tools to track and trend outcomes. In addition, clinicians use fixed or mobile devices to document everything from assessments to medication administrations at the point of care.</p>
                
                <p>MEDITECH's Operating Room Management (ORM) and Surgical Services (SUR) solutions contain customizable nursing documentation tools that surgical staff can use to document the surgical "Time Out." The World Health Organization's Surgical Safety Checklist within the timeout section of the surgical chart helps to reduce the risk of errors and adverse events.</p>
                
                <p>See <a href="https://customer.meditech.com/en/d/ehrexcellence/pages/depressionsuicidetoolkitoverview.htm" target="_blank">MEDITECH's Depression Screening and Suicide Prevention Toolkit</a>.</p>
                
                <a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Goal #15</a>
              </div>
            </li>

            <li class="accordion__list__item">
              <a class="accordion__link" href="#">Universal Protocol: Prevent mistakes in surgery.<div class="accordion__list__control"></div></a>
              <div class="accordion__dropdown">
                <p><strong>UP.01.01.01</strong><br>Make sure that the correct surgery is done on the correct patient and at the correct place on the patient’s body.</p>

                <p><strong>UP.01.02.01</strong><br>Mark the correct place on the patient's body where the surgery is to be done.</p>

                <p><strong>UP.01.03.01</strong><br>Pause before the surgery to make sure that a mistake is not being made.</p>

                <h4>MEDITECH Tools</h4>

                <p>MEDITECH's Operating Room Management (ORM) and Surgical Services (SUR) solutions contain a customizable nursing documentation tool surgical staff can use to document the surgical "Time Out" and Surgical Safety Checklist.</p>
                
                <p>The Imaging & Therapeutic Services / Imaging & Documentation Management application leverages integrated order rules with Order Entry/Order Management (OE/POM/OM) for related site and procedure tracking.  Some examples of orders rules are noted below accordingly: </p>
                
                <ul>
                    <li><a href="https://customer.meditech.com/kb/Custform.ASP?urn=62497">Contrast Ordering - utilizing Alerts to assist with delivery of optimal patient care</a></li>
                    <li><a href="https://customer.meditech.com/kb/Custform.ASP?urn=39111">Requisition based on ordering site</a></li>
                    <li><a href="https://customer.meditech.com/kb/Custform.ASP?urn=62150">History of Exposure to Iodinated Contrast When Ordering Contrast Imaging Procedure</a></li>
                </ul>
                
                <a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Universal Protocol</a>
              </div>
            </li>
          
          </ul>
        </div>
        <!-- END Accordion -->    
        
      </div><!-- END container -->
      
    </div><!-- END container__two-thirds -->

    
    <!-- SIDEBAR -->
    <aside class="container__one-third">

    </aside>
    <!-- END SIDEBAR -->

  </div>

<!-- end simple-page--node-4176.tpl.php template -->
