<?php // This template is for each row of the Views block: EVENTS - PAST EVENTS ....................... 

  // get node ID...
  $nid = $fields['nid']->content;
  $node = node_load($nid);
?>
<!-- start views-view-fields--events-past-events--block-1.tpl.php template -->
<figure class="container no-pad">
  <div class="container__one-third">
    <?php
      // get crop orientation option to set proper class name for image...
      if($fields['field_image_1_crop_orientation']->content == 'Show Left Side of Image'){
        $crop = 'crop-left';
      }
      elseif($fields['field_image_1_crop_orientation']->content == 'Show Right Side of Image'){
        $crop = 'crop-right';
      }
      else{
        $crop = 'crop-center';
      }
    ?>
    <div class="square-img-cropper <?php print $crop; ?>">
      <a class="events_past_link_gae" href="<?php print $fields['path']->content; ?>"><?php print $fields['field_event_image']->content; ?></a>
    </div>
  </div>
  <figcaption class="container__two-thirds">
    <h3 class="header-four no-margin"><a class="events_past_link_gae" href="<?php print $fields['path']->content; ?>"><?php print $fields['title']->content; ?></a></h3>
    <h5 class="no-margin--bottom"><?php print $fields['field_event_date']->content; ?></h5>
    <h5 class="no-margin--top">
      <?php 
        print $fields['field_location_city']->content; 
        if($fields['field_location_state']->content){
          $stateTerms = field_view_field('node', $node, 'field_location_state'); 
          if(!empty($stateTerms)){
            foreach($stateTerms["#items"] as $sTerm){
              $eventState = ', '.$sTerm["taxonomy_term"]->description;
            }
          }
          else{
            $eventState = '';
          }
          print $eventState; 
        }
        if($fields['field_location_country']->content){
          if($fields['field_location_country']->content != 'United States' && $fields['field_location_country']->content != 'Canada'){
            print ', '.$fields['field_location_country']->content;
          }
        }
      ?>
    </h5>
    <p><?php print $fields['field_summary']->content; ?></p>
  </figcaption>
</figure>
<hr>
<!-- end views-view-fields--events-past-events--block-1.tpl.php template -->