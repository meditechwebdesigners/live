<!-- START campaign--node-2451.php -->
<?php // This template is set up to control the display of the Clinical Decision Support content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
  @media only screen and (max-width: 35em) {
    .tablet-half {
      float: left;
      display: block;
      margin-right: 2.35765%;
      width: 100%;
    }

    .top-margin {
      margin-top: 1em;
    }

    .mobile-smaller-image {
      max-width: 230px;
      display: block;
      margin: auto !important;
      float: none;
    }
  }

  .squish-list li {
    margin-bottom: .5em;
  }

  @media all and (max-width: 50em) {
    .transparent-overlay {
      margin-bottom: 1em;
    }
  }

</style>

<div class="js__seo-tool__body-content">

  <!-- Hero -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/doctor-with-smiling-patient.jpg);">
    <div class="container__centered">
      <div class="container__two-thirds transparent-overlay text--white">
        <h1 class="js__seo-tool__title no-margin--bottom">Expanse Clinical Decision Support</h1>
        <h2>Turn discrete data into relevant information.</h2>
        <p>Be sure your care teams have the information they need for clinical decision-making. MEDITECH’s wide range of Clinical Decision Support interventions (CDSi) include active formats that engage clinicians while ordering or documenting, and passive formats that require clinicians to take action, such as linked references.</p>
        <p>Our evidence- and experience-based content is embedded in Expanse to give your organization immediate EHR benefits. Tools from collaborative vendors round out our CDS options, helping healthcare organizations of all sizes to improve patient safety, quality metrics, and efficiency — the goals of value-based care. </p>

        <div class="center" style="margin-top:2em;">
          <?php hubspot_button($cta_code, "Download Hilo Medical Center's Surveillance Case Study"); ?>
        </div>

      </div>
    </div>
  </div>
  <!-- End of Hero -->


  <!-- BLOCK 2 -->
  <div id="modal1" class="modal">
    <a class="close-modal" href="javascript:void(0)">×</a>
    <div class="modal-content">
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-ambulatory-registries--large.jpg" alt="MEDITECH Ambulatory Registries">
    </div>
  </div>
  <div id="modal2" class="modal">
    <a class="close-modal" href="javascript:void(0)">×</a>
    <div class="modal-content">
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/sepsis-monitoring-board.jpg" alt="Sepsis Monitoring Board">
    </div>
  </div>
  <div class="container bg--blue-gradient">
    <div class="container__centered text--white">
      <h2>Built in, for better outcomes.</h2>
      <p>Keep a close eye on patients’ conditions, whether they’re outside or inside your organization. Patient Registries and Surveillance work in tandem to identify patients who may need your attention.</p>
      <p>Pre-loaded content includes standard displays, rules, and algorithms to help you monitor and manage common conditions, HACs, and quality measures. Use it as is, tailor it to meet your patients’ needs, or create your own.</p>
      <div class="slider">
        <div>
          <div class="container__one-third tablet-half">
            <h3 class="no-margin--top">Cast a Wide Net for Your Patient Populations</h3>
            <div class="squish-list">
              <p>Actionable Patient Registries integrate with Telehealth for patient-reported values</p>
              <ul>
                <li>Condition registries (diabetes, COPD, hypertension, and several other conditions)</li>
                <li>Health maintenance registries</li>
                <li>Event registries (Recent ED Visit, Pre-Visit Planning, and more)</li>
              </ul>
            </div>
          </div>
          <div class="container__two-thirds tablet-half">
            <div class="open-modal" data-target="modal1">
              <div class="tablet--white">
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-ambulatory-registries.jpg" alt="MEDITECH Ambulatory Registries">
              </div>
              <div class="mag-bg">
                <i class="mag-icon fas fa-search-plus"></i>
              </div>
            </div>
          </div>
        </div>
        <div>
          <div class="container__one-third tablet-half">
            <h3 class="no-margin--top">Ensure Patients Receive the Right Care at the Right Time</h3>
            <div class="squish-list">
              <p>Surveillance identifies at-risk patients, monitors their care, and guides the care team. Standard Content includes:</p>
              <ul>
                <li>Actionable Status Boards</li>
                <ul>
                  <li>Quality Measures</li>
                  <li>Conditions (VAP, Sepsis, etc.)</li>
                  <li>Consults</li>
                  <li>Preventive Measures</li>
                </ul>
                <li>Action lists, including orders, documentation, and notifications</li>
                <li>Quality Watchlists </li>
              </ul>
            </div>
          </div>
          <div class="container__two-thirds tablet-half">
            <div class="open-modal" data-target="modal2">
              <div class="tablet--white">
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/sepsis-monitoring-board.jpg" alt="Sepsis Monitoring Board">
              </div>
              <div class="mag-bg">
                <i class="mag-icon fas fa-search-plus"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <link rel="stylesheet" href="<?php print $url; ?>/sites/all/themes/meditech/css/slick.css">
  <link rel="stylesheet" href="<?php print $url; ?>/sites/all/themes/meditech/css/slick-theme.css">
  <script src="<?php print $url; ?>/sites/all/themes/meditech/js/slick.min.js"></script>
  <script>
    jQuery(document).ready(function($) {
      $('.slider').slick({
        dots: true,
        prevArrow: '<a class="slick-prev">&#10092;</a>',
        nextArrow: '<a class="slick-next">&#10093;</a>',
      });
    });

  </script>
  <!-- BLOCK 2 -->

  <!-- Block 3 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/hospital-hallway-blurred.jpg);">
    <div class="container__centered">

      <div class="page__title--center auto-margins" style="margin-bottom:2em;">
        <h2>Excellence included.</h2>
        <p>Advance clinical decision-making from day one. Our multidisciplinary Standard Content is evidence- and expert-based. Plus, it’s coordinated with best practice workflows, for safer patient care from the start.</p>
      </div>

      <div class="container__one-fourth text--white">
        <div class="transparent-overlay" style="text-align:center;">
          <div class="text--fresh-mint" style="padding-bottom:1em;"><i class="fas fa-user-md fa-3x"></i></div>
          <p>600+ documentation templates — developed by our physician team — address key specialties.</p>
        </div>
      </div>
      <div class="container__one-fourth text--white">
        <div class="transparent-overlay" style="text-align:center;">
          <div class="text--fresh-mint" style="padding-bottom:1em;"><i class="fas fa-bell fa-3x"></i></div>
          <p><a href="https://ehr.meditech.com/ehr-solutions/meditech-ehr-excellence-toolkits">EHR Excellence Toolkits</a> for CAUTI, sepsis, fall risk, and diabetes management include content, plus optimal workflows and system guidance to improve outcomes. Coming soon to Expanse: Opioid Stewardship Toolkit.</p>
        </div>
      </div>
      <div class="container__one-fourth text--white">
        <div class="transparent-overlay" style="text-align:center;">
          <div class="text--fresh-mint" style="padding-bottom:1em;"><i class="fas fa-database fa-3x"></i></div>
          <p>Clinical Rules Database reduces obstacles to decision support. Search our repository of approved, multidisciplinary rules for common workflows and issues.</p>
        </div>
      </div>
      <div class="container__one-fourth text--white">
        <div class="transparent-overlay" style="text-align:center;">
          <div class="text--fresh-mint" style="padding-bottom:1em;"><i class="fas fa-exclamation fa-3x"></i></div>
          <p>Monitoring tools for alerts and clinical standardization <a href="https://blog.meditech.com/how-to-eliminate-alert-fatigue-0">minimize alert fatigue</a>, track care variations, and fine-tune quality improvement programs.</p>
        </div>
      </div>

    </div>
  </div>
  <!-- End Block 3 -->

  <!-- Block 4 -->
  <div class="container">
    <div class="container__centered">
      <div class="page__title--center auto-margins">
        <h2>Care-full collaborations.</h2>

        <p>MEDITECH has joined forces with the best knowledge vendors in the industry to simplify CDS acquisition. The results? You get their content embedded in Expanse’s clinical workflows.</p>
        <div class="container__centered" style="margin:2em 0 2em;">

          <div class="container__one-third"><img src="<?php print $url; ?>/sites/default/files/vendors/IMO-logo.png" alt="Intelligent Medical Objects Logo"></div>

          <div class="container__one-third"><img src="<?php print $url; ?>/sites/default/files/vendors/Zynx-logo.png" alt="ZynxHealth Logo"></div>

          <div class="container__one-third"><img src="<?php print $url; ?>/sites/default/files/vendors/first-databank-logo.jpg" alt="First Databank logo"></div>

        </div>
        <div class="container__centered">

          <div class="container__one-half"><img src="<?php print $url; ?>/sites/default/files/vendors/NDSC_Care_Select_logo--300x80.jpg" alt="National Decision Support Company Logo"></div>

          <div class="container__one-half"><img src="<?php print $url; ?>/sites/default/files/vendors/National-Comprehensive-Cancer-Network.jpg" alt="National Comprehensive Cancer Network Logo"></div>

        </div>
      </div>
    </div>
  </div>
  <!-- End Block 4 -->

  <!-- Block 5 -->
  <div class="content__callout border-none">
    <div class="content__callout__media">

      <div class="content__callout__image-wrapper">
        <div class="video js__video" data-video-id="271338623">
          <figure class="video__overlay">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--make-patient-engagement-more-accessible.jpg" alt="Video Covershot">
          </figure>
          <a class="video__play-btn video_gae" href="https://vimeo.com/271338623"></a>
          <div class="video__container"></div>
        </div>
      </div>
    </div>
    <div class="content__callout__content">
      <div class="content__callout__body">
        <div class="content__callout__body__text">
          <h2>Evolving to meet physicians’ needs.</h2>
          <p>“These tools aren’t meant to replace physicians,” said Bryan Bagdasian, MD, physician informaticist. He explains how MEDITECH’s CDSi augments care processes for providers, in this Facebook Live video with Janet Desroche, AVP, Client Services Division.</p>
          <p><a target="_blank" href="https://www.facebook.com/MeditechEHR/videos/10157310264448378/?t=7">Watch the entire interview on Facebook.</a></p>
        </div>
      </div>
    </div>
  </div>
  <!-- End Block 5 -->

  <!-- Block 6 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/meditech-ehr-solutions-bg.jpg);">
    <div class="container__centered">
      <div class="container__one-half">
        <div class="transparent-overlay">
          <div class="text--fresh-mint" style="text-align:center; padding-bottom:1em;"><i class="far fa-comments fa-3x"></i></div>
          <div class="text--white">
            <p class="italic">"We implemented evidence-based clinical practice guidelines, making it easy for physicians to follow best practices by using automated clinical decision support within MEDITECH’s physician documentation, and we’ve seen significant results."</p>
            <p class="bold">Ilan Fischler, MD, FRPC, Medical Director Clinical Informatics</p>
            <p class="text--small">Ontario Shores Centre for Mental Health Sciences (Whitby, Ontario)</p>
          </div>
        </div>
      </div>

      <div class="container__one-half">
        <div class="transparent-overlay">
          <div class="text--fresh-mint" style="text-align:center; padding-bottom:1em;"><i class="far fa-comments fa-3x"></i></div>
          <div class="text--white">
            <p class="italic">“You can’t possibly pick up on every indication of a patient that could be going downhill fast. Having these status boards and surveillance tools, we can now flag someone in real time and say ‘Maybe you should go take a look at this patient.’"</p>
            <p class="bold">Alicia Brubaker MSN, RN, CCRN-K, Informatics Nurse</p>
            <p class="text--small">Valley Health System (Ridgewood, NJ)</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Block 6 -->

</div>
<!-- end js__seo-tool__body-content -->

<!-- Block 7 - CTA -->
<div class="container">
  <div class="container__centered" style="text-align: center;">

    <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
    <h2>
      <?php print $cta->field_header_1['und'][0]['value']; ?>
    </h2>
    <?php } ?>

    <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
    <div>
      <?php print $cta->field_long_text_1['und'][0]['value']; ?>
    </div>
    <?php } ?>

    <div class="center" style="margin-top:2em;">
      <?php hubspot_button($cta_code, "Download the Frederick Memorial Sepsis Case Study"); ?>
    </div>

    <div style="margin-top:1em;">
      <?php print $share_link_buttons; ?>
    </div>

  </div>
</div>
<!-- End Block 7 CTA -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>
<!-- END campaign--node-2451.php -->
