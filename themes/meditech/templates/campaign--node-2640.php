<!-- START campaign--node-2640.php HOME CARE campaign -->
<?php // This template is set up to control the display of the Home Care 2018 content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
	.button-vertical-adjustment {
		margin-top: 6em;
	}

	@media (max-width: 800px) {
		.button-vertical-adjustment {
			margin-top: 1em;
		}
	}

	.auto-margins--left {
		max-width: 50em;
		margin-right: auto;
	}

	.floating-quote-box {
		margin-bottom: 0em !important;
	}

	.content__callout__content {
		background-color: #E6E9EE;
		padding-left: 0;
		padding-right: 6%;
	}

	@media all and (max-width: 50em) {
		.content__callout__content {
			padding-left: 6%;
			color: #3e4545;
			padding-top: 0em;
		}

		.video {
			max-width: 100%;
		}
	}

	.video--shadow {
		border-radius: 6px;
		box-shadow: 13px 13px 40px rgba(0, 0, 0, 0.3);
	}

	.transparent-overlay--white {
		padding: 1em;
		background-color: rgba(255, 255, 255, 0.6);
		min-height: 5em;
	}

	.transparent-overlay--black {
		padding: 1em;
		background-color: rgba(62, 69, 69, 0.6);
		min-height: 5em;
	}

	.transparent-overlay--blue {
		padding: 1em;
		background-color: rgba(0, 117, 168, 0.6);
		min-height: 5em;
	}

</style>

<!-- Block 1 Hero -->

<div class="js__seo-tool__body-content">

	<div class="container background--cover  hide__bg-image--mobile" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Female-dr-with-stethoscope-whitebg.jpg); background-color:#D7D1c4;">
		<div class="container__centered">
			<div class="container__one-half">
				&nbsp;
			</div>
			<div class="container__one-half text--black-coconut">
				<h1 class="text--black-coconut js__seo-tool__title">
					See the value of an integrated Home Care and Hospice solution.
				</h1>
				<p>
					Support patients through every stage of life, using MEDITECH's integrated Home Care solution with the Expanse EHR. You'll be able to keep patients safe and well-cared for outside traditional acute settings &mdash; using home health, telehealth, and hospice features to give patients a personalized experience from the comfort of home.
				</p>

				<div class="center" style="margin-top:2em;">
					<?php hubspot_button($cta_code, "Sign Up For Home Care Optimization Symposium"); ?>
				</div>

			</div>
		</div>
	</div>

	<!-- Close Block 1 -->

	<!-- Block 2 -->
	<div class="container background--cover text--white hide__bg-image--mobile" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Female-dr-with-Stethoscope-and-blue-wall.jpg); background-color:#0b4a56;">
		<div class="container__centered">
			<div class="container no-pad">
				<div class="container__one-half">
					<h2 class="text--white">True integration bridges gaps in care.</h2>
					<p class="text--white">Prevent costly readmissions and ED visits with the power of a single, integrated EHR. Expanse helps homecare agencies and hospitals to closely monitor recently-discharged and/or chronically ill patients, and easily exchange information between entities for streamlined <a href="https://ehr.meditech.com/ehr-solutions/bridging-the-gaps-in-care"> transitions </a> and improved care consistency. </p>
				</div>
			</div>
		</div>
	</div>
	<!--Close Block 2 -->

	<!-- Block 3 VIDEO-->
	<div class="content__callout border-none" style="background-color: #E6E9EE !important;">
		<div class="content__callout__media">
			<div class="content__callout__image-wrapper" style="padding:3em !important;">
				<div class="video js__video video--shadow" data-video-id="288730125">
					<figure class="video__overlay">
						<img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/video-overlay-Home-Care-2018-video1.jpg" alt="Kalispell Regional">
					</figure>
					<a class="video__play-btn" href="https://vimeo.com/288730125"></a>
					<div class="video__container"></div>
				</div>
			</div>
		</div>
		<div class="content__callout__content">
			<div class="content__callout__body">
				<div class="content__callout__body__text">
					<h2>
						Experience a mobile solution that moves with you.
					</h2>
					<p>
						Using MEDITECH's <a href="https://ehr.meditech.com/ehr-solutions/ehr-mobility"> mobile </a> home care solution, aides and field staff can access their schedules and view patient charts on the move. The easy-to-use solution is accessible via standard web browser on any smart device. Aides can generate more accurate plan-of-care documentation using the solution's structured responses, integrity checks, and simplified documentation tasks.
					</p>
				</div>
			</div>
		</div>
	</div>
	<!-- End Block 3 VIDEO  transparent-overlay--white-->


	<!-- START Block 4 - UPDATED KLAS BLOCK -->
	<style>
		.flex-cont {
			display: flex;
			justify-content: space-between;
			align-items: center;
			background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Nurse-Smiling-MEDITECH-KLAS-Awards--bg.jpg);
			min-height: 450px;
			background-position: left;
		}

		.flex-image:nth-child(2) {
			margin: 0 1em;
		}

		@media all and (max-width: 550px) {
			.flex-cont {
				flex-wrap: wrap;
				background-image: none;
				justify-content: center;
			}

			.flex-image:nth-child(2) {
				margin: 0;
			}
		}

	</style>
	<div class="container no-pad">
		<div class="gl-container">
			<div class="container__one-half bg--blue-gradient gl-text-pad">
				<h2>Expanse Earns Top Marks for Home Care Performance</h2>
				<p>MEDITECH Expanse <a href="https://ehr.meditech.com/news/meditech-receives-best-in-klas-award-in-3-key-segments">was ranked #1</a> in the 2022 Best in KLAS: Software &amp; Services report for Home Health EHR (small 1-200 average daily census) for the second straight year. MEDITECH also ranked first in two other segments, Acute Care EMR (Community Hospital) and Patient Accounting &amp; Patient Management (Community Hospital).</p>
				<blockquote class="bq--blue" style="margin-bottom:0;">
					<p class="text--large italic">“MEDITECH Home Health EHR is easy to maneuver around in. It is easy to see past visit notes or put in orders and see what medications patients are on. The system has some nice reports that it can run. I like how on some of the reports, we can easily see how many nurses visits we have done. There are reports to see how we are doing on our quality measures. They come up with the results fast, and they are accurate.”</p>
					<p class="bold">Nurse, KLAS Research, November 2021</p>
				</blockquote>
			</div>

			<div class="container__one-half background--cover gl-text-pad flex-cont flex-order--reverse" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/subtle-light-shining-dark-bg.jpg);">
				<div class="flex-image" style="width: 250px;"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-KLAS-Award-2022--Home-Health.png" alt="MEDITECH KLAS Award 2022 - Home Health EHR"></div>
				<div class="flex-image" style="width: 250px;"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-KLAS-Award-2022--Acute-Care-EMR.png" alt="MEDITECH KLAS Award 2022 - Acute Care EMR"></div>
				<div class="flex-image" style="width: 250px;"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-KLAS-Award-2022--Patient-Accounting-and-Patient-Management.png" alt="MEDITECH KLAS Award 2022 - Patient Accounting and Patient Management"></div>
			</div>

		</div>
	</div>
	<!-- END Block 4 - UPDATED KLAS BLOCK -->


	<!-- Block 5 -->
	<div class="container background--cover  hide__bg-image--mobile" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Youngman-taking-blood-pressure.jpg); background-color:#72838a;">
		<div class="container__centered">
			<div class="container__one-half transparent-overlay--black">
				<h2 class="text--white">Stay in touch with telehealth.</h2>
				<p class="text--white">MEDITECH's telehealth technology keeps providers, patients, and caregivers connected, no matter the distance. Integration of patient-captured vital signs through <a href="http://www.ideallife.com">Ideal Life</a>'s smart devices alert staff of abnormal readings in real time. Home care professionals can coordinate care with primary care physicians by remotely monitoring metrics, vital signs, and any ongoing conditions that the patient may have. And patients can use virtual visits to keep appointments, even when they can't travel. </p>
			</div>
		</div>
	</div>
	<!--Close Block 5 -->

	<!-- Block 6 -->
	<div class="container background--cover text--white" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/blue-boxes-pattern.jpg);">
		<div class="container__centered">
			<div class="page__title--center">
				<h2 class="text--white">Access the tools you need to ace your next audit.</h2>
			</div>
			<h4 class="text--white" style="text-align:center;">Audits have you stressed?
				<br>With MEDITECH Home Care, agencies have the tools they need to approach each <a href="https://blog.meditech.com/how-to-survive-your-next-home-care-audit">audit</a> confidently.
			</h4>
			<br>
			<div>
				<div class="container__one-half center">
					<img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/audit-balance-scale.png" alt="Scale" style="width:30%;" />
					<p>The clinical and financial integration of MEDITECH Home Care
						<br> creates a checks and balances system to provide accurate financials
						<br> and prevent errors in billing.
					</p>
				</div>
				<div class="container__one-half center">
					<img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/audit-checklist-icon.png" alt="Checks" style="width:30%;" />
					<p>Our mock surveys, checklists, and comprehensive Audit Guide
						<br> are all specially designed to put agencies on the right track
						<br> to receive top marks.
					</p>
				</div>
			</div>
		</div>
	</div>
	<!--Close Block 6 -->

	<!-- Block 7 -->
	<div class="content__callout border-none" style="background-color: #E6E9EE !important;">
		<div class="content__callout__media">
			<div class="content__callout__image-wrapper" style="padding:3em !important;">
				<div class="video js__video video--shadow" data-video-id="287268875">
					<figure class="video__overlay">
						<img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/video-overlay-Home-Care-2018-video2.jpg" alt="Dr Fletcher Patient Populations Video Covershot">
					</figure>
					<a class="video__play-btn" href="https://vimeo.com/meditechehr/review/287268875/3fc42ecb0e"></a>
					<div class="video__container"></div>
				</div>
			</div>
		</div>
		<div class="content__callout__content">
			<div class="content__callout__body">
				<div class="content__callout__body__text">
					<h2>
						Use management reports to get a bird's eye view.
					</h2>
					<p>
						See how Med Center Health's Director of Home Care uses MEDITECH Home Care management <a href="https://blog.meditech.com/using-reporting-to-become-a-home-health-top-performer">reports</a> to monitor her organization's performance on a daily basis. Clinicians and field staff are also leveraging MEDITECH's task-driven system to improve documentation and provide more informed patient care.
					</p>
				</div>
			</div>
		</div>
	</div>
	<!--Close Block 7 -->


	<!-- Block 8 -->
	<div class="container background--cover hide__bg-image--mobile" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Patient-with-family-member-holding-hands.jpg);  background-color:#72838a;">
		<div class="container__centered">

			<div class="page__title--center">
				<h2 class="text--white">Extend compassionate end-of-life care to patients and families.</h2>
				<p class="text--white">MEDITECH's integrated Hospice solution supports patients, families, and hospice staff throughout a sensitive time. While patient care stays consistent, the solution can be customized to reflect the wishes of each patient and family.</p>

			</div>

			<div class="container center no-pad--bottom">

				<div class="container__one-third text--white" style="margin-bottom:1em;">
					<div class="transparent-overlay">
						<h4>Patients</h4>
						<p>take comfort knowing that their
							<br> wishes will be reflected
							<br> in personalized care plans.
						</p>
					</div>
				</div>

				<div class="container__one-third text--white" style="margin-bottom:1em;">
					<div class="transparent-overlay">
						<h4>Providers</h4>
						<p>can use the solution to handle complex
							<br> hospice admissions, schedule IDT
							<br> meetings, and create customizable
							<br> care plans.
						</p>
					</div>
				</div>

				<div class="container__one-third text--white">
					<div class="transparent-overlay">
						<h4>Families</h4>
						<p>feel supported with access to bereavement care plans
							<br> that facilitate activities and mailings.
						</p>
					</div>
				</div>

			</div>

		</div>
	</div>
	<!-- Block 8 -->


	<!-- Block 9 -->
	<div class="container" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/blue-boxes-pattern.jpg);">
		<div class="container__centered center text--white">

			<?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
			<h2 class="text--white">
				<?php print $cta->field_header_1['und'][0]['value']; ?>
			</h2>
			<?php } ?>

			<?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
			<div class="text--white">
				<?php print $cta->field_long_text_1['und'][0]['value']; ?>
			</div>
			<?php } ?>

			<div class="center" style="margin-top:2em;">
				<?php hubspot_button($cta_code, "Download the Home Care Audit Guide Checklist"); ?>
			</div>

			<div style="margin-top:1em;">
				<?php print $share_link_buttons; ?>
			</div>

		</div>
	</div>
	<!-- Close Block 9 -->

</div>
<!-- end js__seo-tool__body-content -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>
<!-- END campaign--node-2640.php -->
