<?php // This template is for each row of the Views block: HOTELS ....................... 

  // get node ID...
  $nid = $fields['nid']->content;
  $node = node_load($nid);

  $link = $fields['field_directions_url']->content;
?>
<!-- start views-view-fields--hotels--block.tpl.php template -->
<figure class="container no-pad">
  <div class="container__one-third">
    <?php
      // get crop orientation option to set proper class name for image...
      if($fields['field_image_1_crop_orientation']->content == 'Show Left Side of Image'){
        $crop = 'crop-left';
      }
      elseif($fields['field_image_1_crop_orientation']->content == 'Show Right Side of Image'){
        $crop = 'crop-right';
      }
      else{
        $crop = 'crop-center';
      }
    ?>
    <div class="square-img-cropper <?php print $crop; ?>">
      <?php print $fields['field_location_image']->content; ?>
    </div>
  </div>
  <figcaption class="container__two-thirds">
    <h3 class="header-four no-margin--top"><?php print $fields['title']->content; ?></h3>
    <p class="no-margin--bottom"><?php print $fields['field_location_address']->content; ?></p>
    <p class="no-margin--bottom"><?php print $fields['field_location_city']->content;
      $stateTerms = field_view_field('node', $node, 'field_location_state'); 
      if(!empty($stateTerms)){
        foreach($stateTerms["#items"] as $sTerm){
          $eventState = ', '.$sTerm["taxonomy_term"]->description;
        }
      }
      else{
        $eventState = '';
      }
      print $eventState; 
    ?>
    <?php print $fields['field_location_zip']->content; ?></p>
    <p class="no-margin--bottom"><a class="phone_gae" href="tel:<?php print $fields['field_phone_number']->content; ?>"><?php print $fields['field_phone_number']->content; ?></a></p>
    <p class="no-margin--bottom"><a class="directions_gae" href="<?php print $link ?>" target="_blank">Directions</a>&nbsp;|&nbsp;<a class="hotel_gae" href="<?php print $fields['path']->content; ?>">More Information</a></p>
  </figcaption>
</figure>

<?php 
if( user_is_logged_in() ){ 
  print '<p style="text-align:right; font-size:12px;"><a href="https://ehr.meditech.com/node/'.$nid.'/edit">Edit this content</a></p>';
}
?>
<hr>
<!-- end views-view-fields--hotels--block.tpl.php template -->