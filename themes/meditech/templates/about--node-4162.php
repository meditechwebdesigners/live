<!-- START about--node-4162.php -->

<style>
	.content__callout,
	.content__callout__content {
		background-color: transparent;
		color: #fff;
	}

	.content__callout__body__text {
		padding: 1.5em 0 2em 0;
	}

	.hide-wide {
		display: none;
		width: 100%;
	}

	.hide-wide img {
		border-radius: 7px;
		margin-bottom: .5em;
	}

	@media all and (max-width: 1150px) {
		.container__one-half {
			width: 100%;
		}

		.hide__bg-image--tablet {
			background-image: none !important;
		}

		.shadow-box {
			padding: 0;
			box-shadow: none;
		}

		.remove-pad {
			padding-bottom: 0em;
		}

		.hide-wide {
			display: block;
		}
	}

</style>

<div class="js__seo-tool__body-content">

	<h1 style="display:none;" class="js__seo-tool__title">About MEDITECH</h1>

	<!-- START Block 1 -->
	<div class="content__callout text--white background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/green-gradient--bg.jpg);">
		<div class="content__callout__media">
			<div class="content__callout__image-wrapper">
				<div class="video js__video" data-video-id="502762364">
					<figure class="video__overlay">
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/MEDITECH-The-Best-Care--video-overlay.jpg" alt="MEDITECH: The best care. Humanly possible. (Video)">
					</figure>
					<a class="video__play-btn" href="https://player.vimeo.com/video/502762364"></a>
					<div class="video__container"></div>
				</div>
			</div>
		</div>
		<div class="content__callout__content">
			<div class="content__callout__body">
				<div class="content__callout__body__text">
					<h2 class="header-one">The best care. <span class="italic">Humanly possible</span>.</h2>
					<p>MEDITECH develops EHRs to simplify and enhance clinicians' interactions with patients. We make technologies that encourage human connection, instead of getting in the way.</p>
          <p>A partnership with MEDITECH can <a href="https://ehr.meditech.com/about/customer-leaders">open up new possibilities</a> for transformational change at your organization. Want to improve your bottom line, physician satisfaction, quality and safety? We will help you do it.</p>
					<div style="margin-top: 1.5em;">
						<a href="<?php print $url; ?>/ehr-solutions" class="btn--orange about_buttons_gae">Explore EHR possibilities</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END Block 1 -->

	<!-- START Block 2 -->
	<div class="container background--cover hide__bg-image--tablet remove-pad" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/about/doctor-speaking-with-patients-virtually.jpg); background-position: right;">
		<div class="container__centered">
			<div class="hide-wide"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/doctor-speaking-with-patients-virtually--mobile.jpg" alt="Doctor speaking with patients virtually"></div>
			<div class="container__one-half shadow-box">
				<h2>Innovation is our mission.</h2>
				<p>MEDITECH wants to help build a world where every patient can access their health information and participate in their own care. We think every healthcare organization can serve their community quicker and more safely if they have instant access to records, knowledge, and data.</p>
				<p>To that end, we are empowering healthcare organizations large and small with better tools for secure and <a href="<?php print $url; ?>/ehr-solutions/meditechs-revenue-cycle">cost-effective</a> care, including our scalable <a href="<?php print $url; ?>/ehr-solutions/cloud-platform">Cloud Platform</a>, <a href="<?php print $url; ?>/ehr-solutions/meditech-as-a-service-maas">MEDITECH as a Service (MaaS)</a>, and our strategic partnerships with <a href="<?php print $url; ?>/ehr-solutions/meditech-and-google-cloud">Google</a> and <a href="<?php print $url; ?>/ehr-solutions/health-records-on-iphone">Apple</a>. And with future enhancements in areas like <a href="https://ehr.meditech.com/ehr-solutions/ehr-mobility">mobility</a>, <a href="<?php print $url; ?>/ehr-solutions/meditech-genomics">genomics</a>, and <a href="<?php print $url; ?>/news/meditech-launches-expanse-virtual-assistant-through-strategic-conversational-ai-collaboration">AI</a>, we will be ready for what comes next.</p>
				<div style="margin-top: 1.5em;">
					<a href="<?php print $url; ?>/about/executives" class="btn--orange about_buttons_gae">Meet our Innovative Executives</a>
				</div>
			</div>
		</div>
	</div>
	<!-- END Block 2 -->


	<!-- START Block 3 UPDATED KLAS BLOCK -->
	<style>
		div.button--hubspot {
			margin-bottom: 0;
		}

	</style>
	<div class="container background--cover hide__bg-image--tablet remove-pad" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/about/happy-young-nurse-smiling-with-tablet--reversed.jpg); background-position: top left;">
		<div class="container__centered">
			<div class="hide-wide"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/happy-young-nurse-smiling-with-tablet--reversed.jpg" alt="Happy young nurse smiling with tablet"></div>
			<div class="container__one-half">
				<p class="no-margin--bottom" style="height: 1px;">&nbsp;</p>
			</div>
			<div class="container__one-half shadow-box">
				<h2>Achieving industry-wide recognition.</h2>
				<p>In 2022, KLAS honored us for the eighth consecutive year - ranking MEDITECH Expanse #1 in three market segments, including Acute Care EMR (Community Hospital), Patient Accounting &amp; Patient Management (Community Hospital), and Home Health EHR (small 1-200 average daily census). This is the second consecutive year that MEDITECH earned Best in KLAS in all three segments. MEDITECH was also a top performer for Overall Software Suite, Acute EMR (Large/IDN), and Ambulatory EMR (>75 physicians), as well as among the leaders in market share growth for 2022.</p>
				<div style="margin-top: 1.5em;">
					<a href="<?php print $url; ?>/about/meditech-awards" class="btn--orange about_buttons_gae">Learn More</a>
				</div>
			</div>
		</div>
	</div>
	<!-- END Block 3 UPDATED KLAS BLOCK -->


	<!-- START Block 4 -->
	<div class="container background--cover hide__bg-image--tablet" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/about/doctor-off-hours-spending-time-with-family.jpg); background-position: right;">
		<div class="container__centered">
			<div class="hide-wide"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/doctor-off-hours-spending-time-with-family--mobile.jpg" alt="Doctor off hours spending time with family"></div>
			<div class="container__one-half shadow-box">
				<h2>You can depend on us.</h2>
				<p>84% of our sites have been with us for <a href="<?php print $url; ?>/ehr-solutions/long-term-partnerships">over a decade</a>, and we know why. Far from the transactional experience you might be used to, a relationship with MEDITECH is a collaboration. <a href="https://ehr.meditech.com/ehr-solutions/meditech-circle">Working together</a>, we make sure that our solutions can address your unique needs.</p>
				<p>Whether your priority is giving <a href="https://www.youtube.com/watch?v=AZiz6uyo0EI">physicians and nurses time back</a> in their day, providing <a href="<?php print $url; ?>/ehr-solutions/patient-engagement">patients a safe, comfortable experience</a>, or <a href="<?php print $url; ?>/ehr-solutions/meditechs-revenue-cycle">improving your reimbursement potential</a> - with MEDITECH, you will see the possibilities of better care <a href="<?php print $url; ?>/about/meditech-customer-awards">transform into tangible results</a>.</p>
				<div style="margin-top: 1.5em;">
					<a href="<?php print $url; ?>/about/community" class="btn--orange about_buttons_gae">What Makes MEDITECH Unique</a>
				</div>
			</div>
		</div>
	</div>
	<!-- END Block 4 -->


</div>
<!-- end js__seo-tool__body-content -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>

<!-- END about--node-4162.php -->
