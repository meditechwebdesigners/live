<!-- START careers--node-4171.php -->

<?php
$url = $GLOBALS['base_url']; // grabs the site url
include('inc-share-buttons.php');
?>

<style>
    /* block 1 styles */
    .shadow-box--var {
        padding: 2em;
        color: #3e4545;
        background-color: rgba(255, 255, 255, 0.9);
        border-radius: 7px;
        box-shadow: 0px 0px 20px 1px rgba(120, 120, 120, .2);
        max-width: 500px;
        position: absolute;
        right: 18px;
        top: 50%;
        transform: translateY(-50%);
    }

    .shadow-box {
        padding: 2em 1.9em;
    }

    .container--bg-img {
        max-width: 900px;
        border-radius: 7px;
    }

    .container--rel {
        position: relative;
    }

    @media all and (max-width: 950px) {
        .shadow-box--var {
            max-width: 100%;
            position: relative;
            margin: 0 auto;
            right: 0;
            transform: translateY(0%);
            width: 100%;
            margin-top: 2.35765%;
        }

        .container--bg-img {
            display: none;
        }

        .container--rel {
            margin-bottom: 0;
        }
    }


    .horizontal-menu {
        list-style-type: none;
        padding-left: 0;
    }

    .horizontal-menu li a {
        font-family: "montserrat", Verdana, sans-serif;
        font-weight: 500;
        float: left;
        display: block;
        padding: 1em 1.5em;
        margin-bottom: 0;
        text-decoration: none;
        border-bottom: 0;
        transition: 0.3s ease;
    }

    .horizontal-menu li:first-child {
        padding-left: 0em;
    }

    .horizontal-menu li a:hover {
        color: #fff;
        border-bottom: 0;
        background-color: #0a9178;
        transition: 0.3s ease;
    }

    .accordion__dropdown {
        border-left: none;
        border-right: none;
        border-bottom: 3px solid #087E68;
    }

    .accordion__dropdown ul {
        list-style-type: none;
        text-align: center;
        padding-left: 0;
        margin: 0;
    }

    .accordion__dropdown li {
        padding: .25em 0;
    }

    .video--loop {
        width: auto !important;
        left: 50%;
        top: 70%;
        position: absolute;
        min-width: 100%;
        min-height: 100%;
        transform: translate(-50%, -50%);
    }


     /*  Block 2 - Video */
    .content__callout,
    .content__callout__content {
        background-color: transparent;
    }

    .content__callout__image-wrapper {
        padding: 4em 5em;
    }

    .video-iframe {
        position: relative;
        padding-bottom: 56.25%;
        overflow: hidden;
        margin: 0 auto;
        border-radius: 6px;
        box-shadow: 7px 8px 26px rgba(0, 0, 0, 0.1);
    }

    .video-iframe iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }

    /*  Employee Bubbles */
    .eb-1 {
        background-image: url(<?php print $url;
 ?>/sites/all/themes/meditech/images/campaigns/Galvin-Chow.jpg);
        background-position: center top;
    }

    .eb-2 {
        background-image: url(<?php print $url;
 ?>/sites/all/themes/meditech/images/campaigns/DeVann-Vincent.jpg);
        margin: auto;
        background-position: center top;
    }

    .eb-3 {
        background-image: url(<?php print $url;
 ?>/sites/all/themes/meditech/images/campaigns/Marijo-Carnino-updated.jpg);
        background-position: center top;
        margin-left: auto;
    }

    .eb-4 {
        background-image: url(<?php print $url;
 ?>/sites/all/themes/meditech/images/campaigns/Fred-Mayer.jpg);
        background-position: center top;
    }

    .eb-5 {
        background-image: url(<?php print $url;
 ?>/sites/all/themes/meditech/images/campaigns/Ros-Pierce.jpg);
        margin: auto;
        background-position: center top;
    }

    .eb-6 {
        background-image: url(<?php print $url;
 ?>/sites/all/themes/meditech/images/campaigns/Melissa-Rosario.jpg);
        margin-left: auto;
        background-position: center top;
    }

    .eb-7 {
        background-image: url(<?php print $url;
 ?>/sites/all/themes/meditech/images/campaigns/Shannon-Perkins.jpg);
        background-position: center top;
    }

    .eb-8 {
        background-image: url(<?php print $url;
 ?>/sites/all/themes/meditech/images/campaigns/Lexi-Haley.jpg);
        margin: auto;
        background-position: center top;
    }

    .eb-9 {
        background-image: url(<?php print $url;
 ?>/sites/all/themes/meditech/images/campaigns/Jacqueline-Rivera.jpg);
        margin-left: auto;
        background-position: center top;
    }

    .Modal .container__two-thirds {
        max-height: 500px;
        overflow: auto;
    }

    .employee-bubble {
        width: 250px;
        height: 250px;
        border: 10px solid #e6e9ee;
        border-radius: 50%;
        /*    position: relative;*/
        cursor: pointer;
        box-shadow: inset 0 0 0 0 rgba(0, 188, 111, 0.95);
        -webkit-transition: all 0.3s ease-in-out;
        -moz-transition: all 0.3s ease-in-out;
        -o-transition: all 0.3s ease-in-out;
        -ms-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
    }

    .employee-bubble:hover .eb-info {
        opacity: 1;
        -webkit-transform: scale(1);
        -moz-transform: scale(1);
        -o-transform: scale(1);
        -ms-transform: scale(1);
        transform: scale(1);
    }

    .employee-bubble:hover {
        box-shadow: inset 0 0 0 135px rgba(0, 188, 111, 0.95);
    }

    .employee-card {
        padding-bottom: 1.5em;
    }

    .employee-card-plus {
        padding-top: 3em;
        padding-bottom: 1.5em;
    }

    .eb-info {
        position: absolute;
        width: 250px;
        height: 250px;
        border-radius: 50%;
        opacity: 0;
        -webkit-transition: all 0.4s ease-in-out;
        -moz-transition: all 0.4s ease-in-out;
        -o-transition: all 0.4s ease-in-out;
        -ms-transition: all 0.4s ease-in-out;
        transition: all 0.4s ease-in-out;
        -webkit-transform: scale(0);
        -moz-transform: scale(0);
        -o-transform: scale(0);
        -ms-transform: scale(0);
        transform: scale(0);
        -webkit-backface-visibility: hidden;
    }

    .eb-info h3 {
        color: #fff;
        position: relative;
        padding: 80px 19px 5px 0;
        text-align: center;
    }

    .eb-info p {
        color: #fff;
        position: relative;
        padding: 0px 19px 0 0;
        text-align: center;
    }

    /*  Modal Window */
    .Modal {
        position: absolute;
        z-index: 99;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        background: rgba(0, 0, 0, 0);
        visibility: hidden;
    }

    .Modal .content {
        position: absolute;
        left: 50%;
        top: 35%;
        max-width: 1050px;
        width: 100%;
        padding: 3em 2em 3em 2em;
        border-radius: 6px;
        background: #fff;
        color: #3e4545;
        transform: translate(-50%, -30%) scale(0);
    }

    .Modal .close {
        position: absolute;
        top: 5px;
        right: 18px;
        cursor: pointer;
        color: #3e4545;
    }

    .Modal .close:before {
        content: '\2715';
        font-size: 30px;
    }

    .Modal.is-visible {
        visibility: visible;
        background: rgba(0, 0, 0, 0.75);
        transition: background 0.35s;
        transition-delay: 0.1s;
    }

    .Modal.is-visible .content {
        position: fixed;
        top: 55%;
        left: 50%;
        transform: translate(-50%, -50%) scale(1);
        transition: transform 0.35s;
    }

    .modal-employee {
        width: 200px;
        height: 200px;
        border: 10px solid #e6e9ee;
        border-radius: 50%;
        margin: auto;
        margin-top: 1em;
        background-size: 115%;
    }

    .divided-box {
        background-color: rgba(255, 255, 255, .8);
        overflow: auto;
        display: flex;
        flex-flow: row wrap;
    }

    .left-box {
        width: 60%;
        float: left;
    }

    .right-box {
        width: 40%;
        padding: 6em;
        float: left;
    }

    @media all and (min-width: 762px) {
        .dropdown-menu {
            display: none;
        }
    }

    @media all and (max-width: 760px) {
        .horizontal-menu {
            display: none;
        }
    }

    @media all and (max-width: 1040px) {

        .left-box,
        .right-box {
            width: 100%;
            padding: 2em;
        }
    }

    @media all and (min-width: 801px) and (max-width: 1000px) {
        .horizontal-menu li a {
            font-size: 85%;
            padding: 1em;
        }
    }

    @media all and (max-height: 700px) {
        .Modal.is-visible .content {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 90%;
            -webkit-overflow-scrolling: touch;
            border-radius: 0;
            transform: scale(1);
            margin-top: 4em;
            max-width: 100%;
            overflow-y: auto;
            padding-bottom: 6em;
        }
    }

    @media all and (max-width: 50em) {
        .Modal.is-visible .content {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            -webkit-overflow-scrolling: touch;
            border-radius: 0;
            transform: scale(1);
            margin-top: 5em;
            overflow-y: auto;
            padding-bottom: 6em;
        }

        .employee-card {
            border-right: none;
            border-bottom: 1px solid #e6e9ee;
            margin-bottom: 1em;
        }

        .employee-card-plus {
            border-right: none;
            border-bottom: 1px solid #e6e9ee;
            padding-top: 0;
            margin-bottom: 1em;
        }

        .employee-bubble {
            margin: auto;
            margin-bottom: 1em;
        }

</style>

<!-- Mobile Navigation -->
<div class="no-print" style="background-color: #087E68;">
    <div class="container__centered no-pad">
        <div class="accordion" style="margin-top: 0;">
            <ul class="accordion__list dropdown-menu">
                <li class="accordion__list__item" style="margin-bottom:0; background-color: #087E68;">
                    <a class="accordion__link" href="#" style="color: #fff; font-size:1.2em">Careers Menu<div class="accordion__list__control"></div></a>
                    <div class="accordion__dropdown">
                        <ul>
                            <li>
                                <h3><a href="<?php print $url; ?>/careers/job-listings">Job Listings</a></h3>
                            </li>
                            <li>
                                <h3><a href="<?php print $url; ?>/careers/applying-at-meditech">FAQs</a></h3>
                            </li>
                            <li>
                                <h3><a href="<?php print $url; ?>/about/community">Community</a></h3>
                            </li>
                            <li>
                                <h3><a href="<?php print $url; ?>/careers/benefits-perks">Benefits &amp; Perks</a></h3>
                            </li>
                            <li>
                                <h3><a href="<?php print $url; ?>/careers/career-events">Recruiting Events</a></h3>
                            </li>
                            <li>
                                <h3><a href="https://ehr.meditech.com/careers/veterans">Veterans</a></h3>
                            </li>
                        </ul>
                        <a style="text-align: center;" class="js__accordion__toggle accordion__close" href="#"><i class="fa fa-minus-circle"></i>&nbsp; Close Menu</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- End Mobile Navigation -->


<!-- Horizontal Navigation -->
<div class="no-print" style="background-color: #087E68;">
    <div class="container__centered no-pad">
        <ul class="horizontal-menu text--white" style="margin-top:-1em;">
            <li><a href="<?php print $url; ?>/careers/job-listings">Job Listings</a></li>
            <li><a href="<?php print $url; ?>/careers/applying-at-meditech">FAQs</a></li>
            <li><a href="<?php print $url; ?>/about/community">Community</a></li>
            <li><a href="<?php print $url; ?>/careers/benefits-perks">Benefits &amp; Perks</a></li>
            <li><a href="<?php print $url; ?>/careers/career-events">Recruiting Events</a></li>
            <li><a href="https://ehr.meditech.com/careers/veterans">Veterans</a></li>
        </ul>
    </div>
</div>
<!-- End Horizontal Navigation -->


<!-- Start Block 1 (MEDITECH IS HIRING!) -->
<div class="container__centered container--rel" style="padding:2em 1em; ">
    <img class="container--bg-img" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/careers-page--block1.jpg" alt="black male doctor with black child patient and mother" style="max-height: 500px;">
    <div class="shadow-box--var">
        <h1 class="header-two">MEDITECH is hiring!</h1>
        <p>We have a number of open roles that will allow you to have a hand in shaping the future of healthcare. From development to sales representatives to database administrators each of these roles allows you to make an impact.</p>
        <div class="btn-holder--content__callout no-margin--top">
            <a href="<?php print $url; ?>/careers/job-listings" class="btn--orange">Apply Now!</a>
        </div>
    </div>
</div>
<!-- End Block 1 (MEDITECH IS HIRING!) -->

<!-- Block 2 -->
    <div class="bg--light-gray">
        <div class="content__callout">
            <div class="content__callout__media">
                <div class="content__callout__image-wrapper">
                    <div class="js__video video-iframe">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/Ravtf0nLNX8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <div class="content__callout__content">
                <div class="content__callout__body">
                    <h2>Experience the MEDITECH life.</h2>
                    <p>Be sure to follow @meditechcareers on <a href="https://twitter.com/MEDITECHCareers" target="_blank">Twitter</a> and <a href="https://www.instagram.com/meditechcareers/" target="_blank">Instagram</a> and get a first hand glimpse of what MEDITECH is doing, and why so many talented folks from all walks of life continue to join our ranks each year.</p>

                <div style="margin-top:1em; margin-bottom:2em;">
                    <?php print $share_link_buttons; ?>
                </div>

                </div>
            </div>
        </div>
    </div>
    <!-- END Block 2 -->


<!-- Start Block 3 -->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/careers/bokeh-lights-bg.jpg); padding-bottom: 8em; background-position:top;">

    <div class="container__centered text--white center auto-margins" style="padding-bottom:2em;">
        <h2>More than software.</h2>
        <p>We’re more than software. Healthcare is all about people, and MEDITECH is no exception. We may be best known for our solutions, but our identity runs deeper than that. Meet some of the unique people at our company.</p>
        <p>Take a look and you’ll quickly see a crowd that exists beyond both healthcare and I.T. Regardless of your background, studies, or experience, MEDITECH is always looking for unique individuals who want to stand apart, and work together.</p>
    </div>

    <div class="container__centered" style="padding-bottom:2em;">

        <div class="container__one-third">
            <a href="#Popup1" class="button">
                <div class="employee-bubble eb-1">
                    <div class="eb-info">
                        <h3 class="no-margin--bottom">Galvin Chow</h3>
                        <p>Learn more</p>
                    </div>
                </div>
            </a>

            <div id="Popup1" class="Modal">
                <div class="content">
                    <div>

                        <div class="container__one-third center employee-card">
                            <div class="modal-employee eb-1" style="margin-bottom:1em;"></div>
                            <h3 class="no-margin--bottom">Galvin Chow</h3>
                            <p class="no-margin--bottom">Supervisor, Client Support</p>
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/meditech-logo.svg" style="width:175px; margin-top:1em;" alt="MEDITECH Logo">
                        </div>

                        <div class="container__two-thirds">
                            <h4 class="text--meditech-green no-margin--bottom">MEDITECH is…</h4>
                            <p>A great place to work that is staffed with people who are passionate about helping healthcare organizations improve patient care.</p>

                            <h4 class="text--meditech-green no-margin--bottom">If you have traveled in any of your roles at MEDITECH, what was your favorite travel destination?</h4>
                            <p>Puerto Rico! I took a few vacation days after completing a hospital visit to enjoy the sun and beaches in San Juan. I had a great time!</p>

                            <h4 class="text--meditech-green no-margin--bottom">What is your favorite benefit?</h4>
                            <p>I feel lucky to work at a company that values our well-being and contributes 85% to our health coverage.</p>
                        </div>

                    </div>
                    <div class="close"></div>
                </div>
            </div>
        </div>


        <div class="container__one-third">

            <a href="#Popup2" class="button">
                <div class="employee-bubble eb-2">
                    <div class="eb-info">
                        <h3 class="no-margin--bottom">DeVann Vincent</h3>
                        <p>Learn more</p>
                    </div>
                </div>
            </a>

            <div id="Popup2" class="Modal">
                <div class="content">
                    <div>
                        <div class="container__one-third center employee-card">
                            <div class="modal-employee eb-2" style="margin-bottom:1em;"></div>
                            <h3 class="no-margin--bottom">DeVann Vincent</h3>
                            <p class="no-margin--bottom">Senior Technical Support Specialist, Client Services</p>
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/meditech-logo.svg" style="width:175px; margin-top:1em;" alt="MEDITECH Logo">
                        </div>

                        <div class="container__two-thirds">
                            <h4 class="text--meditech-green no-margin--bottom">How has your career evolved?</h4>
                            <p>I walked into MEDITECH in 2014 as someone who was a MEDITECH novice. Now, I'm considered as a great resource in a variety of areas, thanks to continual learning and experience.</p>

                            <h4 class="text--meditech-green no-margin--bottom">If you have traveled in any of your roles at MEDITECH, what was your favorite travel destination?
                            </h4>
                            <p>I don't have a traveling role, but I do support a hospital located in Hawaii...that would be nice!</p>

                            <h4 class="text--meditech-green no-margin--bottom">Best piece of advice for someone going through the interview/hiring process?</h4>
                            <p>It's simple — be yourself and ask questions.</p>

                            <h4 class="text--meditech-green no-margin--bottom">What is your favorite benefit?</h4>
                            <p>If I had to choose, I would say the employee assistance program (EAP). I was able to find an outstanding grief counselor that helped me a lot &mdash; and of course, the annual bonus.</p>

                            <h4 class="text--meditech-green no-margin--bottom">What have you done in your career at MEDITECH that has made the biggest impact?</h4>
                            <p>I've come a long way. I've become a strong resource for my group as well as other applications and communities at MEDITECH. The work I've been able to do with the MOCA (MEDITECHers of Color Alliance) workplace community has received a lot of positive feedback and helped to generate greater awareness.</p>
                        </div>


                    </div>
                    <div class="close"></div>
                </div>
            </div>
        </div>

        <div class="container__one-third">

            <a href="#Popup3" class="button">
                <div class="employee-bubble eb-3">
                    <div class="eb-info">
                        <h3 class="no-margin--bottom">Marijo Carnino</h3>
                        <p>Learn more</p>
                    </div>
                </div>
            </a>

            <div id="Popup3" class="Modal">
                <div class="content">
                    <div>
                        <div class="container__one-third center employee-card">
                            <div class="modal-employee eb-3" style="margin-bottom:1em;"></div>
                            <h3 class="no-margin--bottom">Marijo Carnino</h3>
                            <p class="no-margin--bottom">Director, Corporate Governance and EHR Safety
                            </p>
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/meditech-logo.svg" style="width:175px; margin-top:1em;" alt="MEDITECH Logo">
                        </div>
                        <div class="container__two-thirds">
                            <h4 class="text--meditech-green no-margin--bottom">Describe your most impactful experience at MEDITECH?</h4>
                            <p>Working at MEDITECH provides me daily opportunities to make a difference. I am able to ensure that patient safety is always the #1 priority in our products. There is rarely a day that I leave and don't feel like I've made contributions in areas that make a difference.</p>

                            <h4 class="text--meditech-green no-margin--bottom">What is your favorite part of the MEDITECH picnic?</h4>
                            <p>Sharing favorite foods with family and friends and volunteering at the Kids Prize Table.</p>

                            <h4 class="text--meditech-green no-margin--bottom">Best piece of advice for someone going through the interview/hiring process?</h4>
                            <p>My advice to someone going through the interview process is to let those interviewing you get to know the authentic you. Also, this is as much an interview of MEDITECH/us as it is of MEDITECH interviewing you - ask the questions you have.</p>

                            <h4 class="text--meditech-green no-margin--bottom">What is your favorite benefit?</h4>
                            <p>MEDITECH's $5,000 referral bonus program is a great win-win as it assists in the hiring of quality employees who already have knowledge and insight into the company.</p>
                        </div>

                    </div>
                    <div class="close"></div>
                </div>
            </div>
        </div>

    </div>

    <div class="container__centered" style="padding-bottom:2em;">

        <div class="container__one-third">
            <a href="#Popup4" class="button">
                <div class="employee-bubble eb-4">
                    <div class="eb-info">
                        <h3 class="no-margin--bottom">Fred Mayer</h3>
                        <p>Learn more</p>
                    </div>
                </div>
            </a>

            <div id="Popup4" class="Modal">
                <div class="content">
                    <div>
                        <div class="container__one-third center employee-card">
                            <div class="modal-employee eb-4" style="margin-bottom:1em;"></div>
                            <h3 class="no-margin--bottom">Fred Mayer</h3>
                            <p class="no-margin--bottom">Senior Promotional Writer, Marketing</p>
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/meditech-logo.svg" style="width:175px; margin-top:1em;" alt="MEDITECH Logo">
                        </div>
                        <div class="container__two-thirds">

                            <h4 class="text--meditech-green no-margin--bottom">How has your career evolved?</h4>
                            <p>I started as a technical writer over 20 years ago, creating new documentation for our first C/S product. After that, I moved into marketing and began developing new product literature. Since then I’ve had the opportunity to develop content in almost every imaginable format, from blog material, to videos, to presentations, web campaigns and beyond. One of the things I value most at MEDITECH is the variety of work I get to do!</p>

                            <h4 class="text--meditech-green no-margin--bottom">If you have traveled in any of your roles at MEDITECH, what was your favorite travel destination?</h4>
                            <p>A few years ago, I had an amazing trip to Louisiana and Mississippi to visit a couple of our sites and conduct video interviews. The opportunity to meet clinical and executive staff using our software and see firsthand how it’s benefiting them was really inspiring. One thing mentioned at both the organizations I visited was just how much they valued their relationship with MEDITECH and our staff. This is something I think we do really well as a company &mdash; build genuine relationships with our customers.</p>

                            <h4 class="text--meditech-green no-margin--bottom">Best piece of advice for someone going through the interview/hiring process?</h4>
                            <p>Be genuine and be enthusiastic. For most positions you don’t need expertise in healthcare, but it’s important to demonstrate a genuine interest in what we do.
                            </p>

                            <h4 class="text--meditech-green no-margin--bottom">What is your favorite benefit?</h4>
                            <p>Our medical benefits are fantastic, but I also love the ‘soft’ benefit of a flexible work schedule. This flexibility is really helpful and appreciated.</p>

                        </div>

                    </div>
                    <div class="close"></div>
                </div>
            </div>
        </div>

        <div class="container__one-third">
            <a href="#Popup5" class="button">
                <div class="employee-bubble eb-5">
                    <div class="eb-info">
                        <h3 class="no-margin--bottom">Ros Pierce</h3>
                        <p>Learn more</p>
                    </div>
                </div>
            </a>

            <div id="Popup5" class="Modal">
                <div class="content">
                    <div>
                        <div class="container__one-third center employee-card-plus">
                            <div class="modal-employee eb-5" style="margin-bottom:1em;"></div>
                            <h3 class="no-margin--bottom">Ros Pierce</h3>
                            <p class="no-margin--bottom">Senior Specialist, Staff Development</p>
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/meditech-logo.svg" style="width:175px; margin-top:1em;" alt="MEDITECH Logo">
                        </div>
                        <div class="container__two-thirds">

                            <h4 class="text--meditech-green no-margin--bottom">What have you done in your career at MEDITECH that has made the biggest impact?</h4>
                            <p>I would say the biggest impact I’ve made in my MEDITECH career is contributing to our inclusion and belonging initiatives as part of our Community, Connection, & Culture program. I’ve truly enjoyed working on the myriad of projects that have impacted management and staff across the company. I’ve personally been enriched by the MEDITECHers I’ve been able to work with.</p>

                            <h4 class="text--meditech-green no-margin--bottom">If you have traveled in any of your roles at MEDITECH, what was your favorite travel destination?</h4>
                            <p>My favorite travel destination when I was in Client Services was traveling to Southern California, where I had dinner on Rodeo Drive in Beverly Hills.</p>

                            <h4 class="text--meditech-green no-margin--bottom">What is your favorite benefit?</h4>
                            <p>My favorite benefit is our health insurance coverage. It’s affordable and I truly feel cared for by the company with our coverage.</p>

                            <h4 class="text--meditech-green no-margin--bottom">Best piece of advice for someone going through the interview/hiring process?</h4>
                            <p>The best advice I would offer to a candidate in the hiring process is to do your homework. You have to know about the business aspects of an organization, but also get a sense of its culture. Make sure you find a place where you feel you will truly belong. You do that by talking to people that work there that are not all or part of the hiring team.</p>
                        </div>

                    </div>
                    <div class="close"></div>
                </div>
            </div>
        </div>

        <div class="container__one-third">
            <a href="#Popup6" class="button">
                <div class="employee-bubble eb-6">
                    <div class="eb-info">
                        <h3 class="no-margin--bottom">Melissa Rosario</h3>
                        <p>Learn more</p>
                    </div>
                </div>
            </a>

            <div id="Popup6" class="Modal">
                <div class="content">
                    <div>
                        <div class="container__one-third center employee-card">
                            <div class="modal-employee eb-6" style="margin-bottom:1em;"></div>
                            <h3 class="no-margin--bottom">Melissa Rosario</h3>
                            <p class="no-margin--bottom">Senior Specialist, Client Support</p>
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/meditech-logo.svg" style="width:175px; margin-top:1em;" alt="MEDITECH Logo">
                        </div>
                        <div class="container__two-thirds">
                            <h4 class="text--meditech-green no-margin--bottom">How has your career evolved?</h4>
                            <p>I started as an Implementation Claims Specialist, Senior Specialist, and then Senior Service Specialist, supporting Claims, Ambulatory Claims, and previously Revenue Cycle. Most recently, I joined our Development Division as a Software Tester. I'm looking forward to gaining experience and perhaps exploring the possibility of a design position in the future.</p>

                            <h4 class="text--meditech-green no-margin--bottom">What is your favorite benefit?</h4>
                            <p>The flexibility with my schedule. I have always been able to take time off or rearrange time when needed.</p>

                            <h4 class="text--meditech-green no-margin--bottom">What have you done in your career at MEDITECH that has made the biggest impact?</h4>
                            <p>Being involved in the MOCA (MEDITECHers of Color Alliance) Workplace Community by far has had the biggest impact on me, and it’s where I've made the biggest impact. I've made connections with people that I never would have otherwise. I have worked with people from Minnesota and Atlanta, and also some in Massachusetts that I had never previously met. I have connected with people of all tenures and have learned so much working on various projects with them.</p>
                        </div>

                    </div>
                    <div class="close"></div>
                </div>
            </div>
        </div>

    </div>

    <div class="container__centered">

        <div class="container__one-third">
            <a href="#Popup7" class="button">
                <div class="employee-bubble eb-7">
                    <div class="eb-info">
                        <h3 class="no-margin--bottom">Shannon Perkins</h3>
                        <p>Learn more</p>
                    </div>
                </div>
            </a>

            <div id="Popup7" class="Modal">
                <div class="content">
                    <div>

                        <div class="container__one-third center employee-card">
                            <div class="modal-employee eb-7" style="margin-bottom:1em;"></div>
                            <h3 class="no-margin--bottom">Shannon Perkins</h3>
                            <p class="no-margin--bottom">Senior Communications Manager</p>
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/meditech-logo.svg" style="width:175px; margin-top:1em;" alt="MEDITECH Logo">
                        </div>
                        <div class="container__two-thirds">

                            <h4 class="text--meditech-green no-margin--bottom">How has your career evolved?</h4>
                            <p>I started as a support specialist for the Emergency Department application. After about a year-and-a-half in my initial role, I was offered an opportunity to take on a brand new communications-related position in the Client Services Division, and I've been in the communications world ever since. I love what I do, and the continual evolution of my role prevents my job from being anything but boring!</p>

                            <h4 class="text--meditech-green no-margin--bottom">What have you done in your career at MEDITECH that has made the biggest impact?</h4>
                            <p>I spearheaded the creation of an internal, company-wide newsletter called The Friday Roundup. Internal comms has always been integral to an organization's success, but with COVID-19 and everyone going virtual, it became more important than ever before.
                            </p>

                            <h4 class="text--meditech-green no-margin--bottom">Best piece of advice for someone going through the interview/hiring process?</h4>
                            <p>Be prepared. Do your research. Come into the interview with a solid understanding of MEDITECH. As my father always says, "Luck is what happens when preparation meets opportunity.”</p>

                            <h4 class="text--meditech-green no-margin--bottom">What is your favorite project or initiative you’ve been involved in?</h4>
                            <p>The MEDITECH Brand Advocate Program (which aims to educate staff about new product promotions and MEDITECH's position as an established and innovative leader in the EHR market) has been a great way to connect with colleagues and to engage staff in sharing our company's story and successes.</p>
                        </div>

                    </div>
                    <div class="close"></div>
                </div>
            </div>
        </div>


        <div class="container__one-third">

            <a href="#Popup8" class="button">
                <div class="employee-bubble eb-8">
                    <div class="eb-info">
                        <h3 class="no-margin--bottom">Lexi Haley</h3>
                        <p>Learn more</p>
                    </div>
                </div>
            </a>

            <div id="Popup8" class="Modal">
                <div class="content">
                    <div>
                        <div class="container__one-third center employee-card">
                            <div class="modal-employee eb-8" style="margin-bottom:1em;"></div>
                            <h3 class="no-margin--bottom">Lexi Haley</h3>
                            <p class="no-margin--bottom">Computer Scientist, System Tools</p>
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/meditech-logo.svg" style="width:175px; margin-top:1em;" alt="MEDITECH Logo">
                        </div>
                        <div class="container__two-thirds">

                            <h4 class="text--meditech-green no-margin--bottom">Best piece of advice for someone going through the interview/hiring process?</h4>
                            <p>Be honest, demonstrate what you know, and indicate how you're capable of further growth.</p>

                            <h4 class="text--meditech-green no-margin--bottom">What is your favorite project or initiative you’ve been involved in?</h4>
                            <p>Participating in MEDITECH's Community, Connection, & Culture team’s efforts to grow our workplace actions and practices of diversity, equity, and inclusivity. This work has felt like a renewal of who I am. As someone once said, "When I do good, I feel good."</p>

                            <h4 class="text--meditech-green no-margin--bottom">What intramural teams have you been a part of at MEDITECH (if any)?</h4>
                            <p>Looooong ago, I was a (comically bad) softball player on the "Bullfrogs" softball team.</p>
                        </div>
                    </div>
                    <div class="close"></div>
                </div>
            </div>
        </div>

        <div class="container__one-third">

            <a href="#Popup9" class="button">
                <div class="employee-bubble eb-9">
                    <div class="eb-info">
                        <h3 class="no-margin--bottom">Jacqueline Rivera</h3>
                        <p>Learn more</p>
                    </div>
                </div>
            </a>

            <div id="Popup9" class="Modal">
                <div class="content">
                    <div>
                        <div class="container__one-third center employee-card">
                            <div class="modal-employee eb-9" style="margin-bottom:1em;"></div>
                            <h3 class="no-margin--bottom">Jacqueline Rivera</h3>
                            <p class="no-margin--bottom">Analyst, Clinical Excellence & Physician Services</p>
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/meditech-logo.svg" style="width:175px; margin-top:1em;" alt="MEDITECH Logo">
                        </div>
                        <div class="container__two-thirds">

                            <h4 class="text--meditech-green no-margin--bottom">How has your career evolved?</h4>
                            <p>As my career has evolved, I’ve been given exciting opportunities to be a part of so many different communities here at MEDITECH. I’m currently a representative for the Community, Connection, & Culture Committee, the MEDITECHers of Color Alliance (MOCA), and the MEDITECH Nurses. I love that I get to raise awareness for important social issues, and help shape the culture here.</p>

                            <h4 class="text--meditech-green no-margin--bottom">Best piece of advice for someone going through the interview/hiring process?</h4>
                            <p>Be honest about what is most important to you &mdash; whether you realize it or not, your work will impact patients and healthcare providers in many different ways, and you will want to ensure your role here is the best fit for you.</p>

                            <h4 class="text--meditech-green no-margin--bottom">What is your favorite benefit?</h4>
                            <p>Working from home!</p>

                            <h4 class="text--meditech-green no-margin--bottom">What have you done in your career at MEDITECH that has made the biggest impact?</h4>
                            <p>I have created two toolkits (best practices to help customers achieve measurable and improved outcomes for their patients) — (1) Depression Screening and Suicide Prevention & (2) OB Hemorrhage Management. These toolkits focus on early interventions to prevent tragedies related to these topics. Ensuring that everyone has the same access to the best possible care is why we make these toolkits, and why I am so passionate about them.</p>
                        </div>

                    </div>
                    <div class="close"></div>
                </div>
            </div>

        </div>

    </div>

</div>


<script>
    $jq.fn.expose = function(options) {
        var $modal = $jq(this),
            $trigger = $jq("a[href=" + this.selector + "]");
        $modal.on("expose:open", function() {
            $modal.addClass("is-visible");
            $modal.trigger("expose:opened");
        });
        $modal.on("expose:close", function() {
            $modal.removeClass("is-visible");
            $modal.trigger("expose:closed");
        });
        $trigger.on("click", function(e) {
            e.preventDefault();
            $modal.trigger("expose:open");
        });
        $modal.add($modal.find(".close")).on("click", function(e) {

            e.preventDefault();

            // if it isn't the background or close button, bail
            if (e.target !== this)
                return;

            $modal.trigger("expose:close");
        });
        return;
    }
    $jq("#Popup1").expose();
    $jq("#Popup2").expose();
    $jq("#Popup3").expose();
    $jq("#Popup4").expose();
    $jq("#Popup5").expose();
    $jq("#Popup6").expose();
    $jq("#Popup7").expose();
    $jq("#Popup8").expose();
    $jq("#Popup9").expose();

    // Example Cancel Button
    $jq(".cancel").on("click", function(e) {

        e.preventDefault();
        $jq(this).trigger("expose:close");
    });

</script>
<!-- End Block 3 -->

<!-- Block 4 -->
<div class="bg--light-gray">
    <div class="container no-pad">
        <div class="divided-box">
            <div class="left-box background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/careers/young-man-mentoring-college-students.jpg); min-height:250px;">
            </div>
            <div class="right-box bg--blue-gradient">
                <h2>Growth Through Education</h2>
                <p>We believe it’s important to stay on top of what’s going on in your particular area of expertise while continually expanding your knowledge base. Our extensive array of onsite training empowers you to build and grow your skills, abilities, and career by providing fresh challenges and engaging classes. From day one, MEDITECH offers robust curriculums to educate you on the company, your role, and big-picture healthcare so you’ll be equipped with the tools and resources to give it your all. And, our continual mentoring ensures you never feel left behind.</p>
            </div>

        </div>
    </div>
</div>
<!-- End Block 4 -->

<!-- END careers--node-4171.php -->
