<!-- START campaign--node-2894.php -->
<?php // This template is set up to control the display of the campaign APPLE HEALTH

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');
$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>


<style>
    .circle-icon {
        height: 100px;
        width: 100px;
        color: #fff;
        display: flex;
        justify-content: center;
        align-items: center;
        border-radius: 50%;
        margin: 0 auto;
        margin-bottom: 1em;
    }

    .card--uneven-bottom {
        align-self: flex-start;
    }

    .angled-bg--off-white {
        background-image: linear-gradient(-70deg, transparent 44.9%, rgba(237, 226, 212, .8) 45%);
        /*        padding: 0;*/
    }

    .angled-bg--light-purple {
        background-image: linear-gradient(-70deg, transparent 44.9%, rgba(208, 198, 209, .8) 45%);
        /*        padding: 0;*/
    }

    .bg--light-green-gradient {
        background-image: linear-gradient(150deg, #cce3e2, #ccf1e2)
    }

    .extra-margin--bottom {
        margin-bottom: 2em;
    }


    .text--small {
        font-size: .75em;
    }

    .big {
        font-size: 2em;
        font-weight: bold;
    }

    .squish-list li {
        margin-bottom: .5em;
    }

    .floating-container {
        padding: 8em 0;
        position: relative;
    }

    .floating-header {
        margin-top: 5em;
    }

    .floating {
        position: absolute;
        top: -40px;
        width: 100%;
    }

    .floating img {
        width: 405px;
    }

    .floating-list {
        margin: 2em 0 0 0;
    }

    .floating-list--left {
        margin: 0 2.7em 0 0;
        ;
    }

    .floating-list--right {
        margin: 0 0 0 2.7em;
        ;
    }

    .floating-list-right-fixes {
        padding-top: 4.6em;

    }

    @media all and (max-width: 65em) {
        .angled-bg--off-white {
            background-image: linear-gradient(-70deg, transparent 41.9%, rgba(237, 226, 212, .8) 42%);
        }

        .angled-bg--light-purple {
            background-image: linear-gradient(-70deg, transparent 41.9%, rgba(208, 198, 209, .8) 42%);
        }

        .floating-list {
            margin: 2em 0 0 15%;
        }

    }


    @media all and (max-width: 50em) {

        .pie-chart--max-width {
            max-width: 25em;
        }

        .angled-bg--off-white {
            background-image: linear-gradient(70deg, rgba(237, 226, 212, .8) 49.9%, rgba(237, 226, 212, .8) 50%);
        }

        .angled-bg--light-grey {
            background-image: linear-gradient(70deg, rgba(208, 198, 209, .8) 49.9%, rgba(208, 198, 209, .8) 50%);
        }

        .right-to-left {
            text-align: left
        }

        .floating-list-right-fixes {
            padding-top: 0;

        }

        .floating-list {
            margin: 0 0 0 35%;
        }

        /* 800px */
        .remove {
            display: none;
        }

        .floating-container {
            padding: 2em 0;
            position: inherit;
        }

        .floating {
            position: inherit;
            top: auto;
            width: 100%;
        }


        .floating-header {
            margin-top: 0;
        }


        .floating img {
            max-width: 300px;
            position: relative;
            top: 0;
            left: 0;
        }

        .floating-list--left {
            margin: 0 0 0 35%;
        }

        .floating-list--right {
            margin: 0 0 0 35%;
        }

        .show-on-mobile {
            display: block;
        }
    }

    .no-borders,
    .no-borders th,
    .no-borders td {
        border: none;
    }

    .padding,
    .padding th,
    .padding td,
    .padding tbody:last-child tr:last-child>td:first-child,
    .padding tbody:last-child tr:last-child>td:last-child {
        padding: 1em;
    }

    table tr:nth-child(even),
    table tbody,
    table tr:nth-child(even),
    table tbody {
        background-color: inherit;
    }

</style>


<div class="js__seo-tool__body-content">

    <!-- Block 1 -->
    <div class="container container-pad bg--purple-gradient">
        <div class="container__centered">
            <div class="container__one-half content--pad-right" style="margin-top:1em;">
                <h1 class="text--white js__seo-tool__title" style="display:none;">MEDITECH health information through Health Records on iPhone<sup>&reg;</sup></h1>
                <h2>Empower your community and providers with Health Records on iPhone<sup>&reg;</sup></h2>
                <p>MEDITECH is empowering consumers with access to their health information through Health Records on iPhone. The feature is a secure option for consumers to take their health information with them, wherever they go. It’s convenient, simple to use, and <a href="https://ehr.meditech.com/ehr-solutions/patient-engagement">helps people better understand their health</a>.</p>
                <p>And with iOS 15, MEDITECH will be among the first vendors to enable patients sharing health data with their care providers from their Health app. This integration, which is embedded in physician workflow, can help lead to more meaningful conversations between patients and providers regarding their health and wellness.</p>
                <p class="text--small">*Sharing Health app data with providers is available in the U.S. only.</p>
            </div>
            <div class="container__one-half">
                <div class="bg-pattern--container">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/person-viewing-health-records-on-iphone.jpg" alt="Physician tying mask in preperation of surgery">
                    <div class="bg-pattern--blue-squares bg-pattern--left"></div>
                </div>
            </div>

        </div>
    </div>
    <!-- End of Block 1 -->


    <!-- Block 2 -->
    <div class="container bg--white">
        <div class="container__centered">
            <h2>One record of care</h2>
            <div class="container__one-third">
                <p>Patients' records are aggregated across participating organizations and EHRs, simplifying access to, and understanding of, their health information.</p>
                <p>With iOS 15, patients can securely share certain health data from the Health app with participating healthcare organizations of their choosing.</p>
            </div>
            <div class="container__two-thirds center">
                <div class="container__one-fifth"><img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/icon-in-circle-on-stick--hospital.png" alt="h icon" /><br><strong>Hospitals</strong></div>
                <div class="container__one-fifth"><img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/icon-in-circle-on-stick--medical-bag.png" alt="medical bag icon" /><br><strong>Physician Practices</strong></div>
                <div class="container__one-fifth"><img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/icon-in-circle-on-stick--stethescope.png" alt="stethescope icon" /><br><strong>Clinics</strong></div>
                <div class="container__one-fifth"><img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/icon-in-circle-on-stick--sign.png" alt="street sign icon" /><br><strong>Out-of-Network Providers</strong></div>
                <div class="container__one-fifth"><img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/icon-in-circle-on-stick--watch-pulse.png" alt="pulse symbol on a watch" /><br><strong>Health App Data</strong></div>
            </div>
        </div>
    </div>
    <!-- End of Block 2 -->


    <!-- Block 3 -->
    <div id="modal1" class="modal">
        <a class="close-modal" href="javascript:void(0)">&times;</a>
        <div class="modal-content">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/screenshot--apple-heath-data-provider.png">
        </div>
    </div>
    <div class="container bg--purple-gradient">
        <div class="container__centered ">

            <h2>Patients can now share data from the Apple Health app with providers</h2>
            <div class="container__one-half">
                <p>When patients choose to share health data from the Apple Health app, physicians can gain a more holistic view of their patients’ health, leading to more informed patient conversations. By using the new share with provider feature, physicians can access Health app data from across care settings, which enables them to:</p>
                <ul>
                    <li>Launch into an easy-to-read view of the health data patients have shared from their summary or reference panel
                    </li>
                    <li>Review their patients’ health and fitness information — including summary, wellness, and lab information</li>
                    <li>Use the scratchpad feature to copy and paste summary elements of relevant data into the patient medical chart.</li>
                </ul>
            </div>
            <div class="container__one-half">
                <div class="open-modal" data-target="modal1"><img style="max-height: 500px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/screenshot--apple-heath-data-provider--LAPTOP.png" alt="MEDITECH Medical Summary Widget Screenshot">
                    <div class="mag-bg">
                        <i class="mag-icon fas fa-search-plus"></i>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- End of Block 3 -->


    <!-- Block 4 -->
    <div class="container bg--white" style="padding:2em 0 3em 0;">
        <div class="container__centered">

            <div class="container no-pad center">
                <h2 class="center" style="padding-bottom:1em;">By the numbers</h2>

                <div class="container__one-half">
                    <img class="pie-chart--max-width" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/pie-chart-85--with-text.svg" alt="Pie chart showing 85%">
                    <p class="text--large"><strong>85%</strong> of patients using online medical records deemed them useful for monitoring their health <sup>1</sup></p>
                </div>
                <div class="container__one-half">
                    <img class="pie-chart--max-width" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/pie-chart-90--with-text.svg" alt="Pie chart showing 90%">
                    <p class="text--large"><strong>90%</strong> of patients said access to Apple Health Records enhanced their understanding of their own health and communications with clinicians and family caregivers. <sup>2</sup></p>
                </div>
            </div>
            <div class="container  no-pad--top">
                <ol class="text--small squish-list">
                    <li>Vaishali Patel, MPH PhD &amp; Christian Johnson, MPH, <a href="https://www.healthit.gov/sites/default/files/page/2018-03/HINTS-2017-Consumer-Data-Brief-3.21.18.pdf" target="_blank">"Individuals' use of online medical records and technology for health needs"</a>, ONC Data Brief, No. 40, April, 2018, accessed April 8, 2019.</li>
                    <li>Julie Spitzer, <a href="https://www.beckershospitalreview.com/healthcare-information-technology/uc-san-diego-patients-like-apple-health-records-initial-survey-finds.html" target="_blank">"UC San Diego patients like Apple health records, initial survey finds,"</a> Becker's Health IT &amp; CIO Report, January 14, 2019, accessed April 8, 2019.</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- End of Block 4 -->


    <!-- Block 5 -->
    <div class="container floating-container bg--purple-gradient">
        <div class="container__centered">

            <div class="container__one-third center" style="padding:1em;">
                <!--right-to-left-->
                <h2 class="floating-header">What's available</h2>
            </div>

            <div class="container__one-third remove" style="padding:1em;">
                &nbsp;
            </div>

            <div class="container__one-third" style="padding: 1em 0;">
                <ul class="squish-list floating-list">
                    <h4>Additional Health App Data</h4>
                    <li style="margin-left:1em;">Activity</li>
                    <li style="margin-left:1em;">Heart rate</li>
                    <li style="margin-left:1em;">Blood pressure</li>
                    <li style="margin-left:1em;">Cycle tracking</li>
                    <li style="margin-left:1em;">Sleep</li>
                    <li style="margin-left:1em;">Irregular rhythm notifications</li>
                    <li style="margin-left:1em;">Falls</li>
                    <li style="margin-left:1em;">Weight</li>
                </ul>
            </div>

        </div>
        <div class="floating center">
            <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/iPhoneX-AllRecords--screen-shot--2021.png" alt="iPhone with screen shot of Apple's Apple Health app">
        </div>
    </div>
    <!-- End of Block 5 -->


    <!-- Block 6 -->
    <div class="container bg--white">
        <div class="container__centered">
            <div class="container">
                <div class="auto-margins center">
                    <h2>When sharing is this simple, everyone wins</h2>
                    <p>With the Health app’s new Sharing tab, available in iOS 15, patients and physicians alike can enjoy timely access to health data from the Health app.</p>
                </div>
            </div>
            <div class="card__wrapper">
                <div class="container__one-third card card--uneven-bottom">
                    <div class="card__info">
                        <div class="circle-icon bg--purple-gradient"> <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--person.svg" alt="Person icon" style="max-width:50%;"></div>
                        <h3 class="center">Patients</h3>
                        <ul>
                            <li>Share health data securely with their providers from the Health app on iPhone.</li>
                            <li>Their providers can see a more holistic view of their health.</li>
                        </ul>
                    </div>
                </div>

                <div class="container__one-third card card--uneven-bottom">
                    <div class="card__info">
                        <div class="circle-icon bg--purple-gradient"> <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--doctor.svg" alt="Doctor icon" style="max-width:50%;"></div>
                        <h3 class="center">Physicians</h3>
                        <ul>
                            <li>Seamlessly access patient Health app data from within their native workflow — no separate sign on required.</li>
                            <li>Review the health and fitness information for more holistic care and informed patient conversations.</li>
                        </ul>
                    </div>
                </div>

                <div class="container__one-third card card--uneven-bottom">
                    <div class="card__info">
                        <div class="circle-icon bg--purple-gradient"> <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--hospital.svg" alt="Hospital icon" style="max-width:50%;"></div>
                        <h3 class="center">Healthcare Organization</h3>
                        <ul>
                            <li>Offering patients the latest leading-edge technology helps support patient recruitment and retainment.</li>
                            <li>Easy deployment, with no fees to the healthcare organization or patient.</li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- End of Block 6 -->

    <!-- Block 7 QUOTE -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blurred-medical-lab.jpg);">
        <article class="container__centered text--white center auto-margins">
            <figure>
                <img class="quote__content__img" src="<?php print $url; ?>/sites/all/themes/meditech/images/people/MelanieSwenson.png" style="width: 150px;" alt="Melanie Swenson Headshot">
            </figure>
            <div class="text--large italic" style="font-size:1.4em; padding-top: .75em;">
                <p>"A high volume of our patients are seasonal, whether it's residents migrating South for the winter or vacationers coming to their lake homes for the summer. Having Health Records on iPhone helps us to trend lab results across institutions a thousand miles apart. We can pull those summaries into patient notes to bridge any visible gaps in the data, and see the full picture."</p>
            </div>
            <p class="no-margin--bottom text--large bold text--meditech-green">Melanie Swenson, Executive Director, HIT</p>
            <p>Logan Health (Kalispell, MT)</p>
        </article>
    </div>
    <!-- End of Block 7 -->

    <!-- Block 8 CHANGE IMAGE -->
    <div class="container bg--white">
        <div class="container__centered">
            <h2>Secure data</h2>
            <div class="container__two-thirds">


                <ul>
                    <li>When your patient’s device is locked with a passcode, Touch ID, or Face ID, all of their health and fitness data in the Health app — other than their Medical ID — is encrypted.</li>
                    <li>When your patients share data from the Health app with your healthcare organization, the data is encrypted in transit and at rest.</li>
                    <li>Apple does not have access to health data shared with a healthcare organization through this feature.</li>
                </ul>

                <div class="container no-pad">
                    <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Apple_Health_Badge.png" alt="Apple Health logo" style="width:200px;" />
                    <p class="text--small">Apple, the Apple logo, and iPhone are trademarks of Apple Inc., registered in the U.S. and other countries.</p>
                </div>

            </div>
            <div class="container__one-third" style="display: flex; justify-content: center;">
                <img style="max-height: 30em;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/illustration--secure-phone.svg" alt="Secure phone illustration">
            </div>
        </div>
    </div>
    <!-- End of Block 5 -->


</div>
<!-- end js__seo-tool__body-content -->

<!-- LAST Block -->
<div class="container bg--purple-gradient" style="padding:5.5em;">
    <div class="container__centered center">

        <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
        <h2>
            <?php print $cta->field_header_1['und'][0]['value']; ?>
        </h2>
        <?php } ?>

        <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
        <div>
            <?php print $cta->field_long_text_1['und'][0]['value']; ?>
        </div>
        <?php } ?>

        <div class="center" style="margin-top:2em;">
            <?php hubspot_button($cta_code, "View Enrollment Details"); ?>
        </div>

        <div style="margin-top:1em;">
            <?php print $share_link_buttons; ?>
        </div>

    </div>
</div>
<!-- End of LAST Block -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>
<!-- END campaign--node-2894.php -->
