<?php if( user_is_logged_in() ){ ?>
<!-- START style-guide--node-3353.php Mobile Device CSS Backgrounds -->

<!--Javascript & CSS reference for Prism-->
<script type="text/javascript" src="<?php print $url; ?>/sites/all/themes/meditech/js/prism.js"></script>

<link href="<?php print $url; ?>/sites/all/themes/meditech/css/prism.css" rel="stylesheet" />

<style>
	.flex-container {
		display: flex;
		flex-flow: row wrap;
		justify-content: space-around;
		list-style: none;
		text-align: center;
		margin: 0;
		padding: 0;
	}

	.phone--white-button:after {
		bottom: 6%;
	}

	.phone--black-button:after {
		bottom: 6%;
	}

</style>

<section class="container__centered">

	<h1 class="page__title">
		<?php print $title; ?>
	</h1>

	<div class="container__two-thirds">

		<p>These are our mobile device backgrounds created with CSS. The idea here is that the CSS will mimic a real life phone or tablet and wrap around whatever size screenshot we attach it to. The benefits of doing this with CSS are to:</p>

		<ul>
			<li>Make all devices consistent across the site</li>
			<li>Easily update the look just by changing a few CSS classes</li>
			<li>Fit any screenshot size without tweaking the code</li>
			<li>Save us time by not having to Photoshop devices onto the screenshots</li>
		</ul>

		<p>The design is mainly based on percentages of width, height, padding, etc. so as the device is scaled down it will maintain the same look at any size. They are fairly simple in design so as to not distract the user from the main focus, the screenshot.</p>

		<p><span class="italic"><strong>Note:</strong> You should always be adding a <a href="<?php print $url; ?>/style-guide/modal-pop-up">modal</a> to your screenshot for better accessibility. The full size pop-up image should NOT have the mobile device background. See example below.</span></p>

		<h2>Phones</h2>

		<div class="demo-ct">
			<div class="flex-container">
				<div>
					<!-- Start modal -->
					<div id="modal1" class="modal">
						<a class="close-modal" href="javascript:void(0)">&times;</a>
						<div class="modal-content">
							<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/results-expanse-now-2.png">
						</div>
					</div>
					<div class="open-modal" data-target="modal1">
						<div class="phone--white">
							<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/results-expanse-now-2.png" style="width:225px;" alt"">
						</div>
						<div class="mag-bg">
							<i class="mag-icon fas fa-search-plus"></i>
						</div>
					</div>
					<!-- End modal -->
				</div>
				<div>
					<!-- Start modal -->
					<div id="modal2" class="modal">
						<a class="close-modal" href="javascript:void(0)">&times;</a>
						<div class="modal-content">
							<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/results-expanse-now-2.png">
						</div>
					</div>
					<div class="open-modal" data-target="modal2">
						<div class="phone--black">
							<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/results-expanse-now-2.png" style="width:225px;" alt"">
						</div>
						<div class="mag-bg">
							<i class="mag-icon fas fa-search-plus"></i>
						</div>
					</div>
					<!-- End modal -->
				</div>
			</div>
		</div>

		<!-- Start Phone Code -->
		<pre style="margin-bottom:2em;"><code class="language-html">&lt;!-- Start White Phone Background -->
&lt;div class="phone--white">
  &lt;img src="phone-image.jpg">
&lt;/div>
&lt;!-- End White Phone Background -->

&lt;!-- Start Black Phone Background -->
&lt;div class="phone--black">
  &lt;img src="phone-image.jpg">
&lt;/div>
&lt;!-- End Black Phone Background -->
</code></pre>
		<!-- End Phone Code -->

		<h2>Tablets</h2>

		<div class="demo-ct">
			<div class="flex-container">
				<div style="margin-bottom: 3em;">
					<!-- Start modal -->
					<div id="modal3" class="modal">
						<a class="close-modal" href="javascript:void(0)">&times;</a>
						<div class="modal-content">
							<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/sepsis-monitoring-board.jpg">
						</div>
					</div>
					<div class="open-modal" data-target="modal3">
						<div class="tablet--white">
							<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/sepsis-monitoring-board.jpg" style="width:600px;" alt"">
						</div>
						<div class="mag-bg">
							<i class="mag-icon fas fa-search-plus"></i>
						</div>
					</div>
					<!-- End modal -->
				</div>
				<div>
					<!-- Start modal -->
					<div id="modal4" class="modal">
						<a class="close-modal" href="javascript:void(0)">&times;</a>
						<div class="modal-content">
							<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/sepsis-monitoring-board.jpg">
						</div>
					</div>
					<div class="open-modal" data-target="modal4">
						<div class="tablet--black">
							<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/sepsis-monitoring-board.jpg" style="width:600px;" alt"">
						</div>
						<div class="mag-bg">
							<i class="mag-icon fas fa-search-plus"></i>
						</div>
					</div>
					<!-- End modal -->
				</div>
			</div>
		</div>

		<!-- Start Tablet Code -->
		<pre style="margin-bottom:2em;"><code class="language-html">&lt;!-- Start White Tablet Background -->
&lt;div class="tablet--white">
  &lt;img src="tablet-image.jpg">
&lt;/div>
&lt;!-- End White Tablet Background -->

&lt;!-- Start Black tablet Background -->
&lt;div class="tablet--black">
  &lt;img src="tablet-image.jpg">
&lt;/div>
&lt;!-- End Black Tablet Background -->
</code></pre>
		<!-- End Tablet Code -->

	</div>

	<!-- SIDEBAR -->
	<aside class="container__one-third">

		<div class="sidebar__nav panel">
			<?php
        $styleBlock = module_invoke('menu', 'block_view', 'menu-style-guide-side-nav');
        print render($styleBlock['content']); 
        ?>
		</div>

	</aside>
	<!-- END SIDEBAR -->

</section>

<!-- END style-guide--node-3353.php Mobile Device CSS Backgrounds -->
<?php } ?>
