<?php // This template is for each row of the Views block: VENDOR LOGOS \\\\\\\\\\\\\\\\\\\\\\\\ ....................... 
  $url = $GLOBALS['base_url']; // grabs the site url
?>

<!-- start views-view-fields--vendor-logos--block.tpl.php template -->
<div class="no-target-icon">
  <a href="<?php print $fields['field_website_url']->content; ?>"><?php print $fields['field_logo']->content; ?></a>
</div>
<!-- end views-view-fields--vendor-logos--block.tpl.php template -->