<!-- start customer-materials-page-menu--node-2580.php template Greenfield Announcement page -->
<?php
$content_sections = multi_field_collection_data_unrestricted($node, 'field_fc_head_ltext_1');
$buttons = multi_field_collection_data_unrestricted($node, 'field_fc_button_unl_1');
$side_menu = multi_field_collection_data_unrestricted($node, 'field_fc_text_link_unl_1');
?>


<!-- HERO section -->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/green-field-background-01.jpg); padding:4em 0;">
  <div class="container__centered center">
    <h1 class="text--white text-shadow--black">Create Your Own Mobile Apps with...</h1>
    <?php if( !empty($content['field_sub_header_1']) ){ ?>
      <h2 class="text--white text-shadow--black"><?php print render($content['field_sub_header_1']); ?></h2>
    <?php } ?>
    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/MEDITECH-Greenfield-logo--white.png" style="width:40%;" />
  </div>
</div>
<!-- End of HERO section -->

<section class="container__centered">

  <div class="container__two-thirds">
     
    <?php

    if( isset($content_sections) && !empty($content_sections) ){
      $number_of_content_sections = count($content_sections);
      for($cs=0; $cs<$number_of_content_sections; $cs++){
        print '<h2>';
        print $content_sections[$cs]->field_header_1['und'][0]['value']; 
        print '</h2>';
        print '<div>';
        print $content_sections[$cs]->field_long_text_1['und'][0]['value']; 
        print '</div>';
      }
    }

    if( isset($buttons) && !empty($buttons) ){
      $number_of_buttons = count($buttons);
      for($b=0; $b<$number_of_buttons; $b++){
        print $buttons[$b]->field_hubspot_embed_code_1['und'][0]['value']; 
        print $buttons[$b]->field_button_text_1['und'][0]['value']; 
        print $buttons[$b]->field_button_url_1['und'][0]['value']; 
      }
    }

    ?>
    
  </div>

  <aside class="container__one-third <?php 
    if( isset($side_menu) && $side_menu[0] != '' ){ 
      print 'panel';
    } 
  ?>">

    <?php
    if( isset($side_menu) && $side_menu[0] != '' ){
      print '<div class="sidebar__nav solutions_sidebar_gae">';
      print '<ul class="menu">';
      $number_of_menu_links = count($side_menu);
      for($ml=0; $ml<$number_of_menu_links; $ml++){
        print '<li><a href="';
        print $side_menu[$ml]->field_link_url_1['und'][0]['value'];
        print '"';
        if($side_menu[$ml]->field_external_link['und'][0]['value'] == 1){
          print ' target="_blank"';
        }
        print '>';
        print $side_menu[$ml]->field_link_text_1['und'][0]['value']; 
        print '</a></li>';
      }
      print '</ul>';
      print '</div>';
    }
    ?>

  </aside>

</section>
<!-- end customer-materials-page-menu--node-2580.php template -->