<!-- start simple-page--node-1172.tpl.php template -- CONTACT CONFIRMATION PAGE directed to from the Webform -->

    <section class="container__centered">

      <div class="container__two-thirds">
        <h1><?php print $title; ?></h1>
        <?php print render($content['field_body']); ?>
      </div>

      <aside class="container__one-third panel">
        <div class="sidebar__nav">
          <h2 class="sidebar__nav__title">Facilities</h2>
          <p>Click a location for directions.</p>
          <?php print views_embed_view('locations_contact_page', 'block'); // adds 'locations contact page' Views block... ?>
          <a class="sidebar__nav__link--special" href="about/directions-to-meditech" style="border-top:0;">See directions to all our facilities.</a>
        </div>
      </aside>

    </section>
    
<!-- end simple-page--node-1172.tpl.php template -->    