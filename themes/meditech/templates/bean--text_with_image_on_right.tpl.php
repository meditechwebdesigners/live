<?php
/**
 * @file
 * Default theme implementation for beans (block-types).
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) entity label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-{ENTITY_TYPE}
 *   - {ENTITY_TYPE}-{BUNDLE}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
 $url = $GLOBALS['base_url']; // grabs the site url 
?>
<!-- start bean--text_with_image_on_right.tpl.php template -->
<?php 
  $objectData = $content['field_long_text_unl_1']['#object'];
  
  //check to see which bean block this is for...
  if( $objectData->label == 'Benefits' ){
      $images = array( // list images in the order they will appear...
        'careers/Icons--Insurance-careers.svg',
        'careers/Icons--Financial-Benefits-careers.svg',
        'careers/Icons--Time-Away-From-Work-careers.svg',
        'careers/Icons--Family-matters-careers.svg',
      );
  }

  if( $objectData->label == 'We Are Green' ){
    $images = array( // list images in the order they will appear...
      'about/GreenByDesign.svg',
      'about/LetThereBeLight.svg',
      'about/ClimateControl.svg',
      'about/PipeDream.svg',
      'about/PaperTrail.svg',
      'about/WontGoToWaste.svg',
    );
  }

  // grab text content...
  $textContent = $content['field_long_text_unl_1']['#items'];
  // count number of text boxes...
  $arrayTotal = count($textContent);
  
  // loop through text boxes and generate HTML...
  for($i=0; $i<$arrayTotal; $i++){
    print '<div class="container no-pad">';
    print '<div class="container__two-thirds">';
    print $textContent[$i]['value']; 
    print '</div>';
    print '<div class="container__one-third">';
    print '<img src="'.$url.'/sites/all/themes/meditech/images/'.$images[$i].'" alt="" />';
    print '</div>';
    print '</div>';
    print '<hr>';
  }
?>  

<?php 
if( user_is_logged_in() ){ 
  print '<p><a href="https://ehr.meditech.com/block/'.$objectData->delta.'/edit?destination=admin/content/blocks">Edit the above content</a></p>';
}
?>
<!-- end bean--text_with_image_on_right.tpl.php template -->