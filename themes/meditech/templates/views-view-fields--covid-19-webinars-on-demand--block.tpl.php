<?php // This template is for each row of the Views block:  COVID-19 ON-DEMAND WEBINARS ....................... 

// get node ID...
$nid = $fields['nid']->content;
$node = node_load($nid);

// get_webinar_image function is in the template.php file

// check to see if URL is external...
if( strpos($fields['field_text_1']->content, 'community.museweb.org') !== false ){
  $target = 'target="_blank"';
}
else{
  $target = '';
}
?>
<!-- start views-view-fields--covid-19-webinars-on-demand--block.tpl.php template -->
<figure class="container no-pad">
  <div class="container__one-third">
    <?php $webinar_image = get_webinar_image($node); ?>
    <div class="square-img-cropper <?php print $webinar_image['crop']; ?>">
      <?php if($fields['field_text_1']->content == ''){ // image is NOT clickable if no link is provided... ?>
        <img src="<?php print $webinar_image['url']; ?>" alt="webinar thumbnail">
      <?php } else { ?>
        <a class="webinars_link_gae" href="<?php print $fields['field_text_1']->content; ?>" <?php print $target; ?>><img src="<?php print $webinar_image['url']; ?>" alt="webinar thumbnail"></a>
      <?php } ?>
    </div>
  </div>
  <figcaption class="container__two-thirds">
    <?php if($fields['field_text_1']->content == ''){ // title is NOT clickable if no link is provided... ?>
      <h3 class="header-four no-margin"><?php print $fields['title']->content; ?></h3>
      <p><?php print $fields['field_summary']->content; ?></p>
      <p>Coming soon.</p>
    <?php } else { ?>
      <h3 class="header-four no-margin"><a class="webinar_live_link_gae" href="<?php print $fields['field_text_1']->content; ?>" <?php print $target; ?>><?php print $fields['title']->content; ?></a></h3>
      <p><?php print $fields['field_summary']->content; ?></p>
      <?php 
      if( !empty($fields['field_duration']->content) ){ 
        print '<p>'.$fields['field_duration']->content.'</p>'; 
      }
      ?>
    <?php } ?>
  </figcaption>
</figure>

<?php 
if( user_is_logged_in() ){ 
  print '<p style="text-align:right; font-size:12px;"><a href="https://ehr.meditech.com/node/'.$nid.'/edit">Edit this content</a></p>';
}
?>
<hr>
<!-- end views-view-fields--covid-19-webinars-on-demand--block.tpl.php template -->