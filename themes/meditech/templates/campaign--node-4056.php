<!-- START campaign--node-4056.php -->

<?php // This template is set up to control the display of the 2022 Digital Materials content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
  /* <!-- Outlined Button --> */
  .btn--outline {
    background-color: transparent;
    color: #ffffff !important;
    border-width: 4px !important;
    -webkit-transition: all 400ms ease-in-out;
  }

  .btn--orange:hover {
    -webkit-transition: all 400ms ease-in-out;

  }

  .btn--outline .button--hubspot a {
    font-size: 0.89em !important;
    height: auto !important;
    line-height: normal !important;
    outline: 0 !important;
    padding: 1em 1.5em !important;
    display: inline-block !important;
    font-weight: 700 !important;
    cursor: pointer !important;
    color: #3E4545 !important;
    text-align: center !important;
    -webkit-transition: all 400ms ease-in-out !important;
    -moz-transition: all 400ms ease-in-out !important;
    transition: all 400ms ease-in-out !important;
    border: 4px solid #ff8300 !important;
    background-color: transparent !important;
  }

  .btn--outline .button--hubspot a:hover {
    color: #fff !important;
    -webkit-transition: all 400ms ease-in-out !important;
    -moz-transition: all 400ms ease-in-out !important;
    transition: all 400ms ease-in-out !important;
    background: #ff610a !important;
    border: 4px solid #ff610a !important;
  }

  .btn--outline .button--hubspot a:visited {
    color: #3E4545 !important;
  }

  iframe {
    /*width: 100%;*/
  }

  .video2 {
    position: relative;
    padding-bottom: 56.25%; /* 16:9 */
    height: 0;
  }
  
  .video2 iframe {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }
  
  .video2 ~ h3 { margin: 1em 0; }

  .hidden {
    width: 0;
    height: 0;
    margin-left: -5000px;
  }

</style>

<div class="js__seo-tool__body-content text--white" style="background: #305b9f; background: linear-gradient(0deg, #09193d, #0d1e45, #11244e, #152956, #192f5f, #1c3567, #203b70, #234179, #274883, #2a4e8c, #2d5495, #305b9f);">


  <!-- Block -->
  <div class="container container__centered no-pad--bottom">
    <div class="center">
      <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/MEDITECH-expanse-logo--green-white.svg" alt="MEDITECH Expanse" width="800" height="215" style="margin-bottom:2em;">
    </div>
    <div>
      <div class="container__two-thirds">
       <div class="video2"><iframe width="640" height="360" src="https://www.youtube-nocookie.com/embed/vhOOyyNsJOE?rel=0" title="YouTube video player" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true"></iframe></div>
        <h3 class="center">MEDITECH Expanse - It's All There</h3>
      </div>
      <div class="container__one-third">
        <ul>
          <li><a href="https://ehr.meditech.com/ehr-solutions/meditech-expanse">MEDITECH Expanse</a></li>
          <li><a href="https://ehr.meditech.com/ehr-solutions">EHR Solutions</a></li>
          <li><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/meditechhighlights.pdf">MEDITECH Today</a></li>
          <li><a href="https://cdn2.hubspot.net/hubfs/2897117/WPs,%20Case%20Studies,%20Special%20Reports/Case_Study_Booklet_SinglePage.pdf">Innovators Booklet</a></li>
          <li><a href="https://ehr.meditech.com/podcast-index">MEDITECH Podcasts</a></li>
          <li><a href="https://www.youtube.com/user/MEDITECHvideo">MEDITECH's YouTube Channel</a></li>
        </ul>
      </div>
    </div>
  </div>

  <div class="container container__centered">
    <div class="center">
      <a href="#A">Ambulatory/Physician</a><span style="margin:0 1em;">|</span><a href="#EPC">Expanse Patient Care</a><span style="margin:0 1em;">|</span><a href="#ECC">Expanse Care Compass</a><span style="margin:0 1em;">|</span><a href="#EG">Expanse Genomics</a>
      <br><a href="#I">Interoperability</a><span style="margin:0 1em;">|</span><a href="#BaCA">Business and Clinical Analytics</a><span style="margin:0 1em;">|</span><a href="#PE">Patient Experience</a><span style="margin:0 1em;">|</span><a href="#MPS">MEDITECH Professional Services</a>
    </div>
  </div>
  <!-- END Block -->


  <div class="container container__centered center">
    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/GettyImages-1278560410.gif" alt="">
  </div>


  <!-- Block -->
  <div class="container container__centered center">

    <h2 style="margin-bottom:2em;">Ambulatory/Physician</h2>

    <div class="center">
      <div class="container__one-third">
        <div class="video2"><iframe width="640" height="360" src="https://www.youtube-nocookie.com/embed/gHPkl5mUT4w?rel=0" title="YouTube video player" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true"></iframe></div>
        <h3>Expanse Ambulatory: Personalized for the practice</h3>
      </div>

      <div class="container__one-third">
        <div class="video2"><iframe width="640" height="360" src="https://www.youtube-nocookie.com/embed/FIkLnYmzwjQ?rel=0" title="YouTube video player" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true"></iframe></div>
        <h3>MEDITECH Ambulatory boosts productivity for Dr. Kanis</h3>
      </div>

      <div class="container__one-third">
        <div class="video2"><iframe width="640" height="360" src="https://www.youtube-nocookie.com/embed/r3KjbS1Ch5I?rel=0" title="YouTube video player" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true"></iframe></div>
        <h3>Physicians see value in collaboration with MEDITECH</h3>
      </div>
    </div>

    <a href="https://ehr.meditech.com/ehr-solutions/expanse-ambulatory" class="btn--orange btn--outline" style="margin:2em 0;">Expanse Ambulatory</a>

    <p><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/expanseambulatoryinfographic.pdf">MEDITECH Expanse Ambulatory:
A modern, mobile Ambulatory EHR</a></p>
    <p><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/ehrphysicianstimeback.pdf">How EHRs can give physicians their “pajama time” back</a></p>
    <p><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/meditechvirtualassistant.pdf">Stop typing, start talking with MEDITECH Expanse Virtual Assistant</a></p>
    <p><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/expansenow.pdf">Expanse Now: Balance amidst burnout</a></p>

  </div>
  <!-- END Block -->


  <div class="container container__centered center">
    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/GettyImages-1278560410.gif" alt="">
  </div>



  <!-- Block -->
  <div class="container container__centered">
    <div class="container__one-half">
      <h2 class="center">Expanse Patient Care</h2>
      <div class="video2"><iframe width="640" height="360" src="https://www.youtube-nocookie.com/embed/IYbMjmIguSg?rel=0" title="YouTube video player" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true"></iframe></div>
      <h3 class="center">Give back time to nurses and therapists with MEDITECH Expanse Patient Care</h3>
      <ul>
        <li><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/expansepatientcare.pdf">MEDITECH Expanse Patient Care</a></li>
        <li><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/nursemobilityscenarios.pdf">MEDITECH Nursing solutions</a></li>
        <li><a href="https://info.meditech.com/en/kings-daughters-medical-center-gives-back-100-plus-hours-to-nurses-with-meditech-expanse-patient-care">KDMC gives back 100+ hours to nurses with MEDITECH Expanse Patient Care</a></li>
      </ul>
    </div>
    <div class="container__one-half">
      <h2 class="center">Expanse Care Compass</h2>
      <div class="video2"><iframe width="640" height="360" src="https://www.youtube-nocookie.com/embed/jIfcmN9HwyY?rel=0" title="YouTube video player" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true"></iframe></div>
      <h3 class="center" style="margin-bottom:2em;">Help guide patients between visits with Expanse Care Compass</h3>
      <ul>
        <li><a href="https://ehr.meditech.com/ehr-solutions/care-compass">Help guide patients between visits with Expanse Care Compass</a></li>
        <li><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/carecompass.pdf">Expanse Care Compass overview</a></li>
      </ul>
    </div>
  </div>
  <!-- END Block -->


  <div class="container container__centered center">
    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/GettyImages-1278560410.gif" alt="">
  </div>


  <!-- Block -->
  <div class="container container__centered">
    <div class="container__one-half">
      <h2 class="center">Expanse Genomics</h2>

      <div class="video2"><iframe width="640" height="360" src="https://www.youtube-nocookie.com/embed/zBn50qyNaA0?rel=0" title="YouTube video player" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true"></iframe></div>
      <h3 class="center" style="margin-bottom:2em;">Genetic data integrated in the EHR</h3>
      <ul>
        <li><a href="https://ehr.meditech.com/ehr-solutions/meditech-genomics?hsCtaTracking=6665f033-2f04-43c2-bed2-22c11d042721%7Cd0fb337d-192b-49de-86e0-69a7b48f883f">Expanse Genomics: The future of medicine. Right now.</a></li>
        <li><a href='https://home.meditech.com/en/d/mktcontent/otherfiles/genomicinfographic.pdf'>Expanse Genomics: Precision medicine for all</a></li>
        <li><a href="https://meditech-podcast.simplecast.com/episodes/how-genomics-will-revolutionize-healthcare-in-the-next-decade">How genomics will revolutionize healthcare in the next decade</a></li>
      </ul>
    </div>
    <div class="container__one-half">
      <h2 class="center">Interoperability</h2>

      <div class="video2"><iframe width="640" height="360" src="https://www.youtube-nocookie.com/embed/qozX2PDFBAg?rel=0" title="YouTube video player" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true"></iframe></div>
      <h3 class="center">Bridging care gaps and increasing physician efficiency with MEDITECH's interoperable EHR</h3>
      <ul>
        <li><a href="https://ehr.meditech.com/ehr-solutions/meditech-interoperability">Stay connected with Traverse, MEDITECH's interoperability solution</a></li>
        <li><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/traverseinfographic.pdf">Connecting communities with Traverse</a></li>
        <li><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/interoperabilityflyer.pdf">Connecting communities with
MEDITECH Expanse</a></li>
      </ul>
    </div>
  </div>
  <!-- END Block -->


  <div class="container container__centered center">
    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/GettyImages-1278560410.gif" alt="">
  </div>



  <!-- Block -->
  <div class="container container__centered">
    <h2 class="center">Business and Clinical Analytics</h2>
    <div>
      <div class="container__one-half">
        <div class="video2"><iframe width="640" height="360" src="https://www.youtube-nocookie.com/embed/BzgiGvm7bZI?rel=0" title="YouTube video player" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true"></iframe></div>
        <h3 class="center">Same data, new insight at Golden Valley</h3>
      </div>
      <div class="container__one-half">
        <div class="video2"><iframe width="640" height="360" src="https://www.youtube-nocookie.com/embed/Lxu2krpf6fQ?rel=0" title="YouTube video player" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true"></iframe></div>
        <h3 class="center">Deborah Heart and Lung fuels performance improvement with MEDITECH BCA</h3>
      </div>
    </div>

    <div class="center">
      <a href="https://ehr.meditech.com/ehr-solutions/meditech-business-clinical-analytics" class="btn--orange btn--outline" style="margin-top:2em;">Fuel more informed decisions with your data</a>
    </div>
  </div>
  <!-- END Block -->


  <div class="container container__centered center">
    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/GettyImages-1278560410.gif" alt="">
  </div>



  <!-- Block -->
  <div class="container container__centered">
    <h2 class="center">Patient Experience</h2>
    <div class="auto-margins">
      <div class="video2"><iframe width="640" height="360" src="https://www.youtube-nocookie.com/embed/MD6-5kU8H9o?rel=0" title="YouTube video player" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true"></iframe></div>
      <h3 class="center">How the Expanse Patient Portal benefits your organization</h3>
    </div>
    <div>
      <div class="container__one-half">
        <ul>
          <li><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/patientexperienceflyer.pdf">Attract, engage, and empower consumers with MEDITECH's Patient Engagement Solutions</a></li>
          <li><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/applehealthinfographic.pdf">Empower your community with Health Records on iPhone®</a></li>
          <li><a href="https://ehr.meditech.com/ehr-solutions/virtual-care">Stay connected no matter what, through virtual care</a></li>
        </ul>
      </div>
      <div class="container__one-half">
        <ul>
          <li><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/patientengagmentfunctionalitychecklist.pdf">Patient Engagement functionality checklist</a></li>
          <li><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/expansepatientconnectinfographic.pdf">Communicate at the speed of life with Expanse Patient Connect</a></li>
        </ul>
      </div>
    </div>
  </div>
  <!-- END Block -->


  <div class="container container__centered center">
    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/GettyImages-1278560410.gif" alt="">
  </div>



  <!-- Block -->
  <div class="container container__centered" style="padding-bottom: 6em;">
    <div class="auto-margins center">
      <h2 class="hidden">MEDITECH Professional Services</h2>
      <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/logo--MEDITECH-Professional-Services--white-and-green.png" alt="MEDITECH Professional Services logo" style="width:60%; margin-bottom:2em;">
    </div>
    <div>
      <div class="container__one-half">
        <div class="video2"><iframe width="640" height="360" src="https://www.youtube-nocookie.com/embed/nDtYfp2ym1M?rel=0" title="YouTube video player" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true"></iframe></div>
        <h3 class="center">Optimize your Expanse EHR with MEDITECH Professional Services</h3>
      </div>
      <div class="container__one-half">
        <ul>
          <li><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/professionalservicesmenu.pdf">Select from a variety of services to meet your specific needs</a></li>
          <li><a href="https://info.meditech.com/en/emanate-health-advances-covid-19-contact-tracing-with-meditech-professional-services">Emanate Health advances COVID-19 contact tracing with MEDITECH Professional Services</a></li>
          <li><a href="https://f.hubspotusercontent10.net/hubfs/2897117/Case_Studies/SuccessStory_KingmanHumana.pdf">Kingman Regional Medical Center makes strides in value-based care with MEDITECH</a></li>
          <li><a href="https://blog.meditech.com/how-jump-starting-quality-improvement-efforts-can-lead-to-transformational-change">How jump starting quality improvement efforts can lead to transformational change</a></li>
          <li><a href="https://f.hubspotusercontent10.net/hubfs/2897117/Case_Studies/Lawrence_General_Customer_Success_Story.pdf">Lawrence General Hospital increases physician efficiency with MEDITECH's BCA solution</a></li>
        </ul>
      </div>
    </div>
    <div class="container center no-pad" style="clear:both;">
      <a href="https://ehr.meditech.com/meditech-professional-services" class="btn--orange btn--outline" style="margin-top:2em;">Reach your goals faster with MEDITECH Professional Services</a>
    </div>
  </div>
  <!-- END Block -->


</div>
<!-- end js__seo-tool__body-content -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>

<!-- END campaign--node-4056.php HIMSS22 Digital Materials -->