<!-- START campaign--node-2021.php BCA CAMPAIGN -->

<?php
$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<div class="js__seo-tool__body-content">

  <!-- Block 1 -->
  <div class="container no-pad">
    <div class="gl-container">

      <div class="container__one-half bg--green-gradient gl-text-pad text--white">
        <h1 class="js__seo-tool__title">Fuel more informed decisions with your data</h1>
        <p>Managing big data is critical to the health of your organization and your patients. But these initiatives can be a drain on time and resources. Now, Business &amp; Clinical Analytics (BCA) does the heavy lifting for you.</p>

        <p>BCA is a web-based data visualization solution that helps organizations increase efficiency, measure progress, and improve performance. Easy to implement, easy to use, and fully integrated with MEDITECH's EHR, BCA puts your clinical, financial, and operational data to work and <a href="https://blog.meditech.com/3-stories-on-using-business-and-clinical-analytics-to-increase-efficiency-and-patient-safety">points your organization in the right direction.</a></p>

        <div class="center" style="margin-top:2em;">
          <?php hubspot_button($cta_code, "Download The Value-Based Care eBook"); ?>
        </div>
      </div>


      <div class="container__one-half background--cover flex-order--reverse" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Building-new-business--BCA.jpg); min-height:17em;">
      </div>

    </div>
  </div>
  <!-- End of Block 1 -->


  <!-- Block 2 -->
  <div class="content__callout">
    <div class="content__callout__media">
      <div class="content__callout__image-wrapper">
        <div class="video js__video" data-video-id="336802953">
          <figure class="video__overlay">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Dr-Dailey-BCA-Data.jpg" alt="Dr. Dailey video thumbnail">
          </figure>
          <a class="video__play-btn video_gae" href="https://vimeo.com/336802953"></a>
          <div class="video__container">
          </div>
        </div>
      </div>
    </div>
    <div class="content__callout__content">
      <div class="content__callout__body">
        <h2>Same data, new insight at Golden Valley</h2>
        <p>Watch CMIO William Dailey, MD explain how Golden Valley Memorial Hospital (Clinton, MO) is gaining insight from their data with help from BCA and read how the organization is improving RVU transparency with <a href="https://ehr.meditech.com/ehr-solutions/meditech-professional-services"> MEDITECH’s Professional Services</a></p>

        <div style="margin-top:2em;">
          <div>
            <strong><a href="https://cdn2.hubspot.net/hubfs/2897117/WPs,%20Case%20Studies,%20Special%20Reports/CustomerLeaders_GoldenValley_BCA.pdf?__hstc=188859636.0ca12a78d5efdfd2e457d2491192deae.1597332901079.1600948607327.1600953421908.17&__hssc=&hsCtaTracking=98d80786-f7f6-416b-aa84-c99cdaff5bd6%7C37e0805c-118a-40d8-b054-7f733a48f76c?hsCtaTracking=98d80786-f7f6-416b-aa84-c99cdaff5bd6" target="_blank">Read The Golden Valley BCA Success Story</a></strong>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Block 2 -->


  <!-- Block 3.1 -->
  <div class="container no-pad">
    <div class="gl-container">

      <div class="container__one-half bg--black-coconut gl-text-pad text--white">
        <div>
          <h2>Customer success using BCA</h2>
          <p>Read our case studies to learn how customers are improving outcomes with BCA. Examples include:</p>
          <ul>
            <li>
              <a href="https://info.meditech.com/summit-pacific-increases-reimbursement-clinic-volumes-with-meditech-analytics-solution-0">Summit Pacific</a> saw an 8% rise in daily clinic volumes
            </li>
            <li>
              <a href="https://info.meditech.com/case-study-deborah-heart-and-lung-optimizes-patient-throughput-with-meditech-businsess-and-clinical-analytics">Deborah Heart and Lung</a> experienced increased patient satisfaction with a sixfold increase in discharges before 10 a.m.
            </li>
          </ul>
        </div>

        <div style="padding-top:2em;">
          <div>
            <strong><a href="https://ehr.meditech.com/case-studies?hsCtaTracking=cea23166-cbae-4164-9adb-22bcc761049c%7Cc166d667-3d0c-4861-ba3c-8328c9030838" target="_blank">Download Our Case Studies</a></strong>
          </div>
        </div>
      </div>

      <div class="container__one-half background--cover flex-order--reverse" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Clinicians-reviewing--BCA.jpg); min-height:17em;">
      </div>

    </div>
  </div>
  <!-- End of Block 3.1 -->


  <!-- Block 5 -->
  <div class="container bg--green-gradient">
    <div class="container__centered">
      <div style="padding-bottom: 1em;">
        <h2>Start fast with a wealth of Standard Content...</h2>
        <p>Working with as much information as you do requires a lot of care, feeding, and maintenance. That's why BCA is loaded with standard dashboards to map out your organization's data and get you up and running on your big data initiatives.</p>
      </div>

      <div class="center" style="padding-bottom: 0em;">

        <div class="container__one-half text--white" style="text-align: left;">

          <p>Our extensive library of standard dashboards covers:</p>

          <div class="container__one-half">
            <ul>
              <li>Meaningful Use/Quality</li>
              <li>Census</li>
              <li>Ambulatory</li>
              <li>General Ledger and Payroll</li>
              <li>Revenue Cycle</li>
            </ul>
          </div>

          <div class="container__one-thalf">
            <ul>
              <li>Supply Chain</li>
              <li>Service Line</li>
              <li>ED and Surgery</li>
              <li>Population Health</li>
            </ul>
          </div>

        </div>

        <div class="container__one-half">

          <!-- Hidden Modal -->
          <div id="modal21" class="modal">
            <a class="close-modal" href="javascript:void(0)">&times;</a>
            <div class="modal-content">
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/BCA-Ambulatory--Regulatory-Clinical-Quality-Analysis.jpg" alt="AMB Regulatory Graphical Clinical Quality Analysis">

            </div>
          </div>
          <!-- End of Hidden Modal -->

          <!-- Start modal trigger -->
          <div class="open-modal" data-target="modal21">
            <div class="tablet--black">
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/BCA-Ambulatory--Regulatory-Clinical-Quality-Analysis--small.jpg" alt="AMB Regulatory Graphical Clinical Quality Analysis">
            </div>
            <!-- Add modal trigger here -->
            <div class="mag-bg">
              <!-- Include if using image trigger -->
              <i class="mag-icon fas fa-search-plus"></i>
            </div>
          </div>
          <!-- End modal trigger -->

        </div>
        
      </div>

      <div>
        <p>MEDITECH refreshes your dashboard library at regular intervals. Plus, we'll continue to add new dashboards and analyses, with a variety of selectors to filter by time periods, providers, payers, patient classes, clinical conditions, and more.</p>
      </div>

    </div>
  </div>
  <!-- End Block 5 -->


  <!-- Block 4 -->
  <div class="container no-pad">
    <div class="gl-container">

      <div class="container__one-half background--cover flex-order--reverse" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/self-service-business-analytics.jpg); min-height:17em;">

      </div>


      <div class="container__one-half bg--black-coconut gl-text-pad text--white">
        <div class="container__centered">
          <h2>...and unleash the power of self-service analytics</h2>
          <p>Creating personalized dashboards is a cinch with intuitive, interactive tools that draw from MEDITECH's pre-built datasets. User-defined dashboards are easy to assemble and even easier to digest. No programming expertise required — simply drag and drop metrics into colorful charts and graphs to display your data in meaningful ways. Customize our standard dashboards or build unique dashboards from scratch.</p>
        </div>
      </div>

    </div>
  </div>
  <!-- End of Block 4 -->


  <!-- Block 6 -->
  <div class="content__callout">
    <div class="content__callout__media">
      <div class="content__callout__image-wrapper">
        <div class="video js__video" data-video-id="457732917">
          <figure class="video__overlay">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Innovator-in-Action-Newton-Medical-Center.jpg" alt="Newton Medical video thumbnail">
          </figure>
          <a class="video__play-btn video_gae" href="https://vimeo.com/457732917"></a>
          <div class="video__container">
          </div>
        </div>
      </div>
    </div>
    <div class="content__callout__content">
      <div class="content__callout__body">
        <h2>Newton Medical Center uncovers COVID-19 insights with MEDITECH’s BCA Solution</h2>
        <p>See how <a href="https://ehr.meditech.com/news/newton-medical-center-uncovers-covid-19-insights-with-meditechs-bca-solution">Newton Medical Center is using COVID-19 dashboards</a> to gain a single source of truth for critical information, including supply tracking for PPE.</p>
      </div>
    </div>
  </div>
  <!-- End of Block 6 -->


  <!-- Block 7 -->
  <div class="container center bg--green-gradient">
    <div class="container__centered text--white">
      <h2>Business and Clinical Analytics is backed by the best</h2>
      <div class="container__centered">
        <p>BCA is powered by MicroStrategy, a MEDITECH third-party vendor. MicroStrategy stood out among a field of 20 vendors in a recently published Gartner Special Report on <a href="https://www.microstrategy.com/us/resources/resource-library/research-and-reports/2020-gartner-critical-capabilities-for-analytics-and-business-intelligence-platforms?CID=7012R000001ANCAQA4&mkt_tok=eyJpIjoiT0RWbE5EZ3daRFZsWXpreSIsInQiOiJLeXp0bjhjMDZxUnVSbGl2VnFcLzB1N2ljcTh2RlFnVEVQbUxGaEtrbG1KRWt1cHVvb2VPZjZsNTgrSG9LT3ppbTgyWng3Tk4rNm4yXC9PczRFUjQ0Z0gwRExueHl3bVowK1hpS1Z1OXJXdkhYN2RHWDdBZ1oyTGVUTlR5Q2VOOGxVIn0%3D" target="_blank">critical capabilities for business intelligence and analytics platforms.</a></p>
      </div>

      <div class="container__centered" style="padding-top: 2em;">

        <div>
          <strong>Strong Integrated Product for all Use Cases</Strong>
          <br>
          <i>"MicroStrategy has outstanding scores for security, reporting, embedded and manageability"</i>
        </div>
        <p style="margin-left:.5em;"> - Gartner Special Report, 2020
        </p>
      </div>

    </div>
  </div>
  <!-- End Block 7 -->


  <!-- Block 8 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/BCA-Executive-Financial-Dashboards.jpg);">
    <div class="container__centered text--white">
      <div class="container__one-half transparent-overlay">
        <h2>Nurture financial success and sustainability</h2>
        <p>Identify trends and drill down for an illuminating view of your organization's fiscal workings and outlook. Determine the links between clinical and financial performance to avoid costly penalties and capture fuller reimbursement. Gauge your advancement toward organizational goals with dashboards that display targets and assess progress. When the right information is at your fingertips, <a href="https://ehr.meditech.com/ehr-solutions/ehr-value-and-sustainability">maximizing revenues and controlling costs</a> have never been more manageable.</p>
      </div>
    </div>
  </div>
  <!-- End Block 8 -->


  <!-- Block 9 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/green-layered-background.jpg);">
    <div class="container__centered text--black-coconut" style="margin-bottom:2em;">

      <div class="page__title--center auto-margins" style="margin-bottom:2em;">
        <h2>Deliver safer, higher quality care, with enlightening analytics</h2>
        <p>Detect costly and unnecessary variations in care with Quality Dashboards that assess performance at the individual, practice, and group level, and support adherence to best practices.</p>
      </div>

      <div class="container__one-third shadow-box">
        <div>
          <h2 style="text-align:center;">CQO</h2>
          <div>
            <p>Identify root causes of deficiencies among your CQMs to improve patient outcomes, decrease readmission rates, and reduce penalties. Gain a better understanding of your patient populations as well.</p>
          </div>
        </div>
      </div>

      <div class="container__one-third shadow-box">
        <div>
          <h2 style="text-align:center;">CMO</h2>
          <div>
            <p>Safeguard quality care, review benchmarks, and ensure that medical staff and administration are united in pursuing organizational goals. BCA's specialized dashboards track readmissions, HACs, Level of Service analysis, variations in care, and more.</p>
          </div>
        </div>
      </div>

      <div class="container__one-third shadow-box">
        <div>
          <h2 style="text-align:center;">CNO</h2>
          <div>
            <p>Track and manage readmissions, HACs, turnaround times, surgical throughput, time to prepare room, average outpatient visits, and much more to keep your department running like a well-oiled machine.</p>
          </div>
        </div>
      </div>

    </div>

    <div class="container__centered auto-margins">

      <!-- Hidden Modal -->
      <div id="modal23" class="modal">
        <a class="close-modal" href="javascript:void(0)">&times;</a>
        <div class="modal-content">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/BCA--Quality-Readmissions-Summary.jpg" alt="Quality Readmissions Summary">

        </div>
      </div>
      <!-- End of Hidden Modal -->

      <!-- Start modal trigger -->
      <div class="open-modal tablet--white" data-target="modal23">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/BCA--Quality-Readmissions-Summary.jpg" alt="Quality Readmissions Summary">
        <!-- Add modal trigger here -->
        <div class="mag-bg">
          <!-- Include if using image trigger -->
          <i class="mag-icon fas fa-search-plus"></i>
        </div>
      </div>
      <!-- End modal trigger -->

    </div>
  </div>
  <!-- End Block 9 -->


  <!-- Block 10 -->
  <div class="container bg--green-gradient">
    <div class="container__centered">
      <div style="padding-bottom: 1em;">
        <h2>Elevate the patient and consumer experience</h2>
        <p>See what patients and consumers think about your organization. Explore opportunities for improvement and growth with dashboards that help you to deliver more focused, streamlined care.</p>
      </div>

      <div class="container__centered" style="padding-bottom: 1em;">

        <div class="container__one-half text--white" style="text-align: left;">
          <p>Our extensive library of standard dashboards covers:</p>
          <div>
            <ul>
              <li>Track patient satisfaction scores, portal enrollment, and patient education.</li>
              <li>Perform real-time assessment of ED wait times to reduce bottlenecks.</li>
              <li>Fine-tune location management to maximize patient convenience.</li>
            </ul>
          </div>
        </div>

        <div class="container__one-half">
          <!-- Hidden Modal -->
          <div id="modal24" class="modal">
            <a class="close-modal" href="javascript:void(0)">&times;</a>
            <div class="modal-content">
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/BCA-ED--Geographic-Analysis.jpg" alt="ED Geographic Analysis">
            </div>
          </div>
          <!-- End of Hidden Modal -->

          <!-- Start modal trigger -->
          <div class="open-modal tablet--white" data-target="modal24">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/BCA-ED--Geographic-Analysis.jpg" alt="ED Geographic Analysis">
            <!-- Add modal trigger here -->
            <div class="mag-bg">
              <!-- Include if using image trigger -->
              <i class="mag-icon fas fa-search-plus"></i>
            </div>
          </div>
          <!-- End modal trigger -->
        </div>
      </div>

      <div>
        <p>Get a perspective on patient populations in multiple areas with Geographic Analysis Dashboards. Pinpoint where patients are coming from, so you can tailor your <a href="https://ehr.meditech.com/ehr-solutions/meditech-population-health">population health strategies</a> and optimize resource allocation. Identify new service line opportunities and potential locations for care.</p>
      </div>

    </div>
  </div>
  <!-- End Block 10 -->

</div>
<!-- end js__seo-tool__body-content -->

<!-- Block 7 - CTA Block -->
<div class="container bg--white">
  <div class="container__centered" style="text-align: center;">

    <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
    <h2>
      <?php print $cta->field_header_1['und'][0]['value']; ?>
    </h2>
    <?php } ?>

    <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
    <div>
      <?php print $cta->field_long_text_1['und'][0]['value']; ?>
    </div>
    <?php } ?>

    <div class="center" style="margin-top:2em;">
      <?php hubspot_button($cta_code, "Download The Summit Pacific Analysis case study"); ?>
    </div>

    <div style="margin-top:1em;">
      <?php print $share_link_buttons; ?>
    </div>

  </div>
</div>
<!-- End Block 7 -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>

<!-- END campaign--node-2021.php -->
