<?php if( user_is_logged_in() ){ ?>
<!-- START style-guide--node-3664.php Web Utility & Beyond -->

<!--Javascript & CSS reference for Prism-->
<script type="text/javascript" src="<?php print $url; ?>/sites/all/themes/meditech/js/prism.js"></script>

<link href="<?php print $url; ?>/sites/all/themes/meditech/css/prism.css" rel="stylesheet" />

<style>
	.sg-section {
		margin-bottom: 2em;
	}

</style>

<section class="container__centered">

	<h1 class="page__title">
		<?php print $title; ?>
	</h1>

	<div class="container__two-thirds">

		<h2>Customer Service Alerts</h2>
		<p>If anouncements, downtime, or other alerts are to be scheduled then we can use this alert box format. The alert code will go right above the comment in the "customerstandard" template that says: <code class="language-html">&lt;!-- Featured news story section --></code>.</p>
		<p><span class="italic"><strong>Note:</strong> This alert box may already be added to the "customerstandard" template. If that's the case, just add the alert text to the Web Utility page editor.</span></p>
		<!-- Start Alert Code -->
		<pre style="border-radius: 6px;"><code class="language-html">&lt;!-- Start Alert Code --> 
&lt;div class="row">
	&lt;div class="large-12 columns">
		&lt;!-- elementAlertSection start -->&lt;span id="elementAlertSection">
			&lt;div class="callout notice">
				&lt;p>Announcement text goes here.&lt;/p>
			&lt;/div>
		&lt;/span>&lt;!-- elementAlertSection end -->
	&lt;/div>
&lt;/div>
&lt;!-- End Alert Code --> 
</code></pre>
		<!-- End Alert Code -->

		<div class="sg-section">
		<h2>Web Utility Documentation</h2>
			<p><a href="https://staff.meditech.com/en/d/wu/homepage.htm" target="_blank">This documentation</a> is designed to share with you what the Web Utility is, who might be a good candidate to use it, and how to navigate through the various Web Utility functions.</p>
		</div>

		<div class="sg-section">
			<h2>Axure Documentation</h2>

			<p><a href="https://docs.google.com/document/d/1PmuRRX7KXwyCmsysyI2O_2fg3UDh0N2bM0oCHmLGGY0/edit?usp=sharing" target="_blank">Axure Pro RP Instructions</a></p>

			<p><a href="https://docs.google.com/document/d/18n9CCR3QyrlN2k7_HsU75f6Fs4vvmZVJ7YOrXp5s9GQ/edit?usp=sharing" target="_blank">Axure Screen Capture Guide</a></p>

			<p><a href="https://docs.google.com/document/d/1ywNxGAT7c_AhBC_lcwHJeV--fi_7yFtfCpaV26VbRa0/edit?ts=5a7df894" target="_blank">Axure Tips &amp; Tricks</a></p>
		</div>

		<h2>Miscellaneous</h2>

		<p><a href="https://staff.meditech.com/mktwebdocumentation/style-guide/index.php" target="_blank">Original Style Guide (deprecated)</a></p>

	</div>

	<!-- SIDEBAR -->
	<aside class="container__one-third">

		<div class="sidebar__nav panel">
			<?php
        $styleBlock = module_invoke('menu', 'block_view', 'menu-style-guide-side-nav');
        print render($styleBlock['content']); 
        ?>
		</div>

	</aside>
	<!-- END SIDEBAR -->

</section>

<!-- END style-guide--node-3664.php Web Utility & Beyond -->
<?php } ?>
