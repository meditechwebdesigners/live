<!-- START draft -- GREENFIELD PAGE -->
<?php
$url = $GLOBALS['base_url']; // grabs the site url
  
$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<div class="js__seo-tool__body-content">


  <!-- block 1 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/greenfield-background-digital-components.jpg); background-position:center top;">
    <div class="container__centered">
      <h1 class="js__seo-tool__title" style="display:none;">MEDITECH Greenfield | MEDITECH API</h1>

      <div class="container__two-thirds text--white">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/MEDITECH-Greenfield-logo--white.svg" alt="MEDITECH Greenfield logo" style="width:50%;" />
        <div style="margin:0 1.5em;">
          <h2 class="header-one">Creating innovation<br>through connection</h2>
          <p>MEDITECH Greenfield is expanding to include two new collaborative environments. <strong>Greenfield Alliance</strong> represents partner organizations with proven, successful, and interoperable products which are aligned with MEDITECH and our solutions. <strong>Greenfield Workspace</strong> gives third-party developers and Expanse customers an environment to test their own innovative solutions within a real MEDITECH EHR.</p>
        </div>
      </div>

      <div class="container__one-third center">
        <div class="shadow-box">
          <p>Learn more about our partner program to benefit clinicians, patients, and consumers.</p>
          <div class="center" style="margin-top:1em;">
            <a href="https://ehr.meditech.com/ehr-solutions/greenfield-alliance" class="btn--orange">Greenfield Alliance</a>
          </div>
        </div>
        <div class="shadow-box">
          <p>Explore a testing ground to help you develop integrated solutions with MEDITECH Expanse.</p>
          <div class="center" style="margin-top:1em;">
            <a href="https://ehr.meditech.com/ehr-solutions/greenfield-workspace" class="btn--emerald">Greenfield Workspace</a>
          </div>
        </div>
      </div>

    </div>
  </div>
  <!-- End block 1 -->



  <div class="container container__centered center">

    <h2>Stay connected with Greenfield updates!</h2>
    <div style="margin-top:2em; margin-bottom:2em;">
      <a href="https://info.meditech.com/subscribe-for-greenfield-updates" class="btn--emerald">Subscribe to our Greenfield Newsletter</a>
    </div>

  </div>


  <?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>

  <!-- END draft -->
