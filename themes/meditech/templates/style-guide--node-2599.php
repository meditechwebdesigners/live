<?php if( user_is_logged_in() ){ ?>
<!-- START style-guide--node-2599.php COLORS -->

<!--Javascript & CSS reference for Prism-->
<script type="text/javascript" src="<?php print $url; ?>/sites/all/themes/meditech/js/prism.js"></script>

<link href="<?php print $url; ?>/sites/all/themes/meditech/css/prism.css" rel="stylesheet" />

<style>
  .palette-box {
    margin-bottom: 1em;
    width: auto;
    display: block;
    border-radius: 6px;
    min-height: 200px;
  }

  .hex-box {
    height: auto;
    padding: 1em;
    width: auto;
    display: block;
  }

  .language-css {
    cursor: pointer;
    margin-bottom: .5em;
  }

  #p1,
  #p2,
  #p3,
  #p4,
  #p5,
  #p6,
  #p7,
  #p8,
  #p9,
  #p10,
  #p11,
  #p12,
  #p13,
  #p14,
  #p15,
  #p16,
  #p17,
  #p18,
  #p19,
  #p20,
  #p21,
  #p22,
  #p23,
  #p24,
	#p25 {
    display: none;
  }

  .copy-element {
    display: inline;
    opacity: 0;
    margin-left: 1em;
    transition: .2s ease-in-out;
  }

  .copy-container:hover .copy-element {
    opacity: 1;
    transition: .2s ease-in-out;
  }

  @media all and (max-width: 50em) {
    .palette-box {
      min-height: 150px;
    }

    .palette-spacing {
      margin-bottom: 2.35765%;
    }
  }

</style>

<section class="container__centered">

  <h1 class="page__title">
    <?php print $title; ?>
  </h1>

  <div class="container__two-thirds">

    <p>Web-specific colors have been chosen to complement our Print Brand Identity. We've included background and text classes for most of the colors within the CSS. However, most text color classes should be used for things like Font Awesome icons, not actual heading/paragraph text.</p>


    <div class="container no-pad">
      <h2>Primary</h2>
      <div class="container__one-half">
        <div class="palette-box bg--meditech-green palette-spacing">
          <div class="hex-box">
            <p><span class="bold">MEDITECH Green</span><br>
              #00BC6F<br>
              <a href="#">Example Link</a><br>
            </p>

            <span class="copy-container">
              <code class="language-css" onclick="copyToClipboard('#p1')">.bg--meditech-green</code>
              <span class="copy-element">Copy</span>
              <span id="p1">bg--meditech-green</span>
            </span>
            <br>

            <span class="copy-container">
              <code class="language-css" onclick="copyToClipboard('#p2')">.text--meditech-green</code>
              <span class="copy-element">Copy</span>
              <span id="p2">text--meditech-green</span>
            </span>
          </div>
        </div>
      </div>
      <div class="container__one-half">
        <div class="palette-box bg--emerald">
          <div class="hex-box">
            <p><span class="bold">Emerald</span><br>
              #087E68<br>
              <a href="#">Example Link</a></p>
            <span class="copy-container">
              <code class="language-css" onclick="copyToClipboard('#p3')">.bg--emerald</code>
              <span class="copy-element">Copy</span>
              <span id="p3">bg--emerald</span>
            </span>
            <br>

            <span class="copy-container">
              <code class="language-css" onclick="copyToClipboard('#p4')">.text--emerald</code>
              <span class="copy-element">Copy</span>
              <span id="p4">text--emerald</span>
            </span>
          </div>
        </div>
      </div>
    </div>
    <div class="container no-pad">
      <p><span class="italic"><strong>Note:</strong> Emerald should be primarily used for text links on light backgrounds.</span></p>
    </div>

    <div class="container no-pad">
      <h2>Neutral</h2>
      <div class="container__one-half">
        <div class="palette-box bg--white palette-spacing" style="border:1px solid #e6e9ee;">
          <div class="hex-box">
            <p><span class="bold">White</span><br>
              #FFF<br>
              <a href="#">Example Link</a></p>
            <span class="copy-container">
              <code class="language-css" onclick="copyToClipboard('#p5')">.bg--white</code>
              <span class="copy-element">Copy</span>
              <span id="p5">bg--white</span>
            </span>
            <br>

            <span class="copy-container">
              <code class="language-css" onclick="copyToClipboard('#p6')">.text--white</code>
              <span class="copy-element">Copy</span>
              <span id="p6">text--white</span>
            </span>
          </div>
        </div>
      </div>
      <div class="container__one-half">
        <div class="palette-box bg--light-gray">
          <div class="hex-box">
            <p><span class="bold">Light Gray</span><br>
              #E6E9EE<br>
              <a href="#">Example Link</a></p>
            <span class="copy-container">
              <code class="language-css" style="background-color:#dee0e4;" onclick="copyToClipboard('#p7')">.bg--light-gray</code>
              <span class="copy-element">Copy</span>
              <span id="p7">bg--light-gray</span>
            </span>
            <br>

            <span class="copy-container">
              <code class="language-css" style="background-color:#dee0e4;" onclick="copyToClipboard('#p8')">.text--light-gray</code>
              <span class="copy-element">Copy</span>
              <span id="p8">text--light-gray</span>
            </span>
          </div>
        </div>
      </div>
    </div>

    <div class="container no-pad">
      <div class="container__one-half">
        <div class="palette-box bg--light-brown palette-spacing">
          <div class="hex-box">
            <p><span class="bold">Light Brown</span><br>
              #D7D1C4<br>
              <a href="#">Example Link</a></p>
            <span class="copy-container">
              <code class="language-css" onclick="copyToClipboard('#p9')">.bg--light-brown</code>
              <span class="copy-element">Copy</span>
              <span id="p9">bg--light-brown</span>
            </span>
            <br>

            <span class="copy-container">
              <code class="language-css" onclick="copyToClipboard('#p10')">.text--light-brown</code>
              <span class="copy-element">Copy</span>
              <span id="p10">text--light-brown</span>
            </span>
          </div>
        </div>
      </div>
      <div class="container__one-half">
        <div class="palette-box bg--black-coconut">
          <div class="hex-box">
            <p><span class="bold">Black Coconut</span><br>
              #3E4545<br>
              <a href="#">Example Link</a></p>
            <span class="copy-container">
              <code class="language-css" onclick="copyToClipboard('#p11')">.bg--black-coconut</code>
              <span class="copy-element">Copy</span>
              <span id="p11">bg--black-coconut</span>
            </span>
            <br>

            <span class="copy-container">
              <code class="language-css" onclick="copyToClipboard('#p12')">.text--black-coconut</code>
              <span class="copy-element">Copy</span>
              <span id="p12">text--black-coconut</span>
            </span>
          </div>
        </div>
      </div>
    </div>

    <div class="container no-pad">
      <div class="container__one-half">
        <div class="palette-box bg--black palette-spacing">
          <div class="hex-box">
            <p><span class="bold">Black</span><br>
              #000<br>
              <a href="#">Example Link</a></p>
            <span class="copy-container">
              <code class="language-css" onclick="copyToClipboard('#p23')">.bg--black</code>
              <span class="copy-element">Copy</span>
              <span id="p23">bg--black</span>
            </span>
            <br>

            <span class="copy-container">
              <code class="language-css" onclick="copyToClipboard('#p24')">.text--black</code>
              <span class="copy-element">Copy</span>
              <span id="p24">text--black</span>
            </span>
          </div>
        </div>
      </div>
    </div>

    <div class="container no-pad">
      <p><span class="italic"><strong>Note:</strong> Black Coconut or White should be primarily used for heading/paragraph text.</span></p>
    </div>

    <div class="container no-pad">
      <h2>Accent</h2>
      <div class="container__one-half">
        <div class="palette-box bg--orange palette-spacing">
          <div class="hex-box">
            <p><span class="bold">Orange</span><br>
              #FF8300<br>
              Only use as orange button/action link.</p>
            <span class="copy-container">
              <code class="language-css" onclick="copyToClipboard('#p13')">.bg--orange</code>
              <span class="copy-element">Copy</span>
              <span id="p13">bg--orange</span>
            </span>
            <br>

            <span class="copy-container">
              <code class="language-css" onclick="copyToClipboard('#p14')">.text--orange</code>
              <span class="copy-element">Copy</span>
              <span id="p14">text--orange</span>
            </span>
          </div>
        </div>
      </div>
      <div class="container__one-half">
        <div class="palette-box text--white" style="background-color:#FF610A;">
          <div class="hex-box">
            <p><span class="bold">Dark Orange</span><br>
              #FF610A<br>
              Only use as orange button hover state.</p>
          </div>
        </div>
      </div>
    </div>

    <div class="container no-pad">
      <div class="container__one-half">
        <div class="palette-box bg--fresh-mint palette-spacing">
          <div class="hex-box">
            <p><span class="bold">Fresh Mint</span><br>
              #00BBB3<br>
              <a href="#">Example Link</a></p>
            <span class="copy-container">
              <code class="language-css" onclick="copyToClipboard('#p15')">.bg--fresh-mint</code>
              <span class="copy-element">Copy</span>
              <span id="p15">bg--fresh-mint</span>
            </span>
            <br>

            <span class="copy-container">
              <code class="language-css" onclick="copyToClipboard('#p16')">.text--fresh-mint</code>
              <span class="copy-element">Copy</span>
              <span id="p16">text--fresh-mint</span>
            </span>
          </div>
        </div>
      </div>
      <div class="container__one-half">
        <div class="palette-box bg--dark-blue">
          <div class="hex-box">
            <p><span class="bold">Dark Blue</span><br>
              #0075A8<br>
              <a href="#">Example Link</a></p>
            <span class="copy-container">
              <code class="language-css" onclick="copyToClipboard('#p17')">.bg--dark-blue</code>
              <span class="copy-element">Copy</span>
              <span id="p17">bg--dark-blue</span>
            </span>
            <br>

            <span class="copy-container">
              <code class="language-css" onclick="copyToClipboard('#p18')">.text--dark-blue</code>
              <span class="copy-element">Copy</span>
              <span id="p18">text--dark-blue</span>
            </span>
          </div>
        </div>
      </div>
    </div>

    <div class="container no-pad">
      <div class="container__one-half">
        <div class="palette-box bg--purple">
          <div class="hex-box">
            <p><span class="bold">Purple</span><br>
              #B24EC4<br>
              <a href="#">Example Link</a></p>
            <span class="copy-container">
              <code class="language-css" onclick="copyToClipboard('#p19')">.bg--purple</code>
              <span class="copy-element">Copy</span>
              <span id="p19">bg--purple</span>
            </span>
            <br>

            <span class="copy-container">
              <code class="language-css" onclick="copyToClipboard('#p20')">.text--purple</code>
              <span class="copy-element">Copy</span>
              <span id="p20">text--purple</span>
            </span>
          </div>
        </div>
      </div>
    </div>
    <div class="container no-pad">
      <p><span class="italic"><strong>Note:</strong> Orange &amp; Dark Orange should only be used for buttons and action items. Fresh Mint &amp; Purple are accent colors and should be used sparingly.</span></p>
    </div>

    <div class="container no-pad">
      <h2>Gradient</h2>
      <div class="container__one-half">
        <div class="palette-box bg--green-gradient palette-spacing">
          <div class="hex-box">
            <p><span class="bold">Green Gradient</span><br>
              #00BC6F - #00706C<br>
              <a href="#">Example Link</a></p>
            <span class="copy-container">
              <code class="language-css" onclick="copyToClipboard('#p21')">.bg--green-gradient</code>
              <span class="copy-element">Copy</span>
              <span id="p21">bg--green-gradient</span>
            </span>
          </div>
        </div>
      </div>
      <div class="container__one-half">
        <div class="palette-box bg--blue-gradient">
          <div class="hex-box">
            <p><span class="bold">Blue Gradient</span><br>
              #09193D - #305B9F<br>
              <a href="#">Example Link</a></p>
            <span class="copy-container">
              <code class="language-css" onclick="copyToClipboard('#p22')">.bg--blue-gradient</code>
              <span class="copy-element">Copy</span>
              <span id="p22">bg--blue-gradient</span>
            </span>
          </div>
        </div>
      </div>
    </div>
    
    <div class="container no-pad">
      <div class="container__one-half">
        <div class="palette-box bg--purple-gradient palette-spacing">
          <div class="hex-box">
            <p><span class="bold">Purple Gradient</span><br>
              #32325D - #25253B<br>
              <a href="#">Example Link</a></p>
            <span class="copy-container">
              <code class="language-css" onclick="copyToClipboard('#p25')">.bg--purple-gradient</code>
              <span class="copy-element">Copy</span>
              <span id="p25">bg--purple-gradient</span>
            </span>
          </div>
        </div>
      </div>
      </div>
      
    <div class="container no-pad">
      <p><span class="italic"><strong>Note:</strong> Gradients should only be used for backgrounds. </span></p>
    </div>

  </div>

  <script>
    function copyToClipboard(element) {
      var $temp = $jq("<input>");
      $jq("body").append($temp);
      $temp.val($jq(element).text()).select();
      document.execCommand("copy");
      $temp.remove();
    }

  </script>

  <!-- SIDEBAR -->
  <aside class="container__one-third">

    <div class="sidebar__nav panel">
      <?php
        $styleBlock = module_invoke('menu', 'block_view', 'menu-style-guide-side-nav');
        print render($styleBlock['content']); 
        ?>
    </div>

  </aside>
  <!-- END SIDEBAR -->

</section>

<!-- END style-guide--node-2599.php COLORS -->
<?php } ?>