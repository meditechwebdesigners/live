<!-- START about--node-4215.php -->

<?php // This template is set up to control the display of the 50th Anniversary content type
  
$url = $GLOBALS['base_url']; // grabs the site url
  
$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');
?>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/js/swiper.min.js"></script>

<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/css/swiper.min.css" media="all" />

<style>
  /*  Employee Bubbles */
  .eb-1 {
    background-image: url(<?php print $url;
 ?>/sites/all/themes/meditech/images/campaigns/Nhien-Bui.jpg);
    background-position: center top;
  }

  .eb-2 {
    background-image: url(<?php print $url;
 ?>/sites/all/themes/meditech/images/campaigns/Fred-Mayer.jpg);
    background-position: center top;
    margin-left: auto;
  }

  .eb-3 {
    background-image: url(<?php print $url;
 ?>/sites/all/themes/meditech/images/campaigns/Michael-Triggs.jpg);
    background-position: center top;
  }

  .eb-4 {
    background-image: url(<?php print $url;
 ?>/sites/all/themes/meditech/images/campaigns/Samantha-Kerley.jpg);
    background-position: center top;
    margin: auto;
  }

  .eb-5 {
    background-image: url(<?php print $url;
 ?>/sites/all/themes/meditech/images/campaigns/Caryn-Budd.jpg);
    background-position: center top;
    margin: auto;
  }

  .eb-6 {
    background-image: url(<?php print $url;
 ?>/sites/all/themes/meditech/images/campaigns/Javier-Guzman.jpg);
    background-position: center top;
    margin-left: auto;
  }

  .employee-bubble {
    width: 250px;
    height: 250px;
    border: 10px solid #e6e9ee;
    border-radius: 50%;
    cursor: pointer;
    box-shadow: inset 0 0 0 0 rgba(0, 188, 111, 0.95);
    -webkit-transition: all 0.3s ease-in-out;
    -moz-transition: all 0.3s ease-in-out;
    -o-transition: all 0.3s ease-in-out;
    -ms-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
  }

  .employee-bubble:hover .eb-info {
    opacity: 1;
    -webkit-transform: scale(1);
    -moz-transform: scale(1);
    -o-transform: scale(1);
    -ms-transform: scale(1);
    transform: scale(1);
  }

  .employee-bubble:hover {
    box-shadow: inset 0 0 0 135px rgba(0, 188, 111, 0.95);
  }

  .employee-card {
    padding-bottom: 1em;
  }

  .employee-card-plus {
    padding-top: 3em;
    padding-bottom: 1em;
  }

  .eb-info {
    position: absolute;
    width: 250px;
    height: 250px;
    border-radius: 50%;
    opacity: 0;
    -webkit-transition: all 0.4s ease-in-out;
    -moz-transition: all 0.4s ease-in-out;
    -o-transition: all 0.4s ease-in-out;
    -ms-transition: all 0.4s ease-in-out;
    transition: all 0.4s ease-in-out;
    -webkit-transform: scale(0);
    -moz-transform: scale(0);
    -o-transform: scale(0);
    -ms-transform: scale(0);
    transform: scale(0);
    -webkit-backface-visibility: hidden;
  }

  .eb-info h3 {
    color: #fff;
    position: relative;
    padding: 80px 19px 5px 0;
    text-align: center;
  }

  .eb-info p {
    color: #fff;
    position: relative;
    padding: 0px 19px 0 0;
    text-align: center;
  }

  .slick-slide {
    padding: .5% 1.5% 0 1.5% !important;
  }

  /*  Modal Window */

  .Modal {
    position: absolute;
    z-index: 99;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: rgba(0, 0, 0, 0);
    visibility: hidden;
  }

  .Modal .content {
    position: absolute;
    left: 50%;
    top: 35%;
    max-width: 1050px;
    width: 100%;
    padding: 3em 2em 3em 2em;
    border-radius: 6px;
    background: #fff;
    transform: translate(-50%, -30%) scale(0);
  }

  .Modal .close {
    position: absolute;
    top: 5px;
    right: 18px;
    cursor: pointer;
    color: #3e4545;
  }

  .Modal .close:before {
    content: '\2715';
    font-size: 30px;
  }

  .Modal.is-visible {
    visibility: visible;
    background: rgba(0, 0, 0, 0.75);
    transition: background 0.35s;
    transition-delay: 0.1s;
  }

  .Modal.is-visible .content {
    position: fixed;
    top: 55%;
    left: 50%;
    transform: translate(-50%, -50%) scale(1);
    transition: transform 0.35s;
  }

  .modal-employee {
    width: 200px;
    height: 200px;
    border: 10px solid #e6e9ee;
    border-radius: 50%;
    margin: auto;
    margin-top: 1em;
    background-size: 115%;
  }

  /*  Collage Banner */

  .collage-banner {
    opacity: 1;
    filter: alpha(opacity=100);
    /* For IE8 and earlier */
    -webkit-transition: all 0.5s ease-in-out;
    -moz-transition: all 0.5s ease-in-out;
    transition: all 0.5s ease-in-out;
  }

  .collage-banner:hover {
    opacity: 0;
    filter: alpha(opacity=0);
    /* For IE8 and earlier */
    -webkit-transition: all 0.5s ease-in-out;
    -moz-transition: all 0.5s ease-in-out;
    transition: all 0.5s ease-in-out;
  }

  .logo-position {
    margin-left: 13%;
  }

  .logo-box-height {
    height: 200px;
  }

  @media all and (max-width: 104.688em) {
    .logo-position {
      margin-left: 8%;
    }
  }

  @media all and (max-width: 93.75em) {
    .logo-position {
      margin-left: 8%;
    }
  }

  @media all and (max-width: 90.625em) {
    .logo-position {
      margin-left: 2%;
    }
  }

  @media all and (max-width: 82em) {
    .logo-position {
      margin-left: 6%;
    }

    .logo-box-height {
      height: 100px;
    }
  }

  @media all and (max-width: 78.125em) {
    .logo-position {
      margin-left: 2%;
    }

    .logo-box-height {
      height: 50px;
    }
  }

  @media all and (max-width: 68.750em) {
    .logo-position {
      display: none;
    }

    .logo-box-height {
      height: 200px;
    }
  }

  @media all and (max-height: 700px) {
    .Modal.is-visible .content {
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 90%;
      -webkit-overflow-scrolling: touch;
      border-radius: 0;
      transform: scale(1);
      margin-top: 4em;
      max-width: 100%;
      overflow-y: auto;
      padding-bottom: 6em;
    }
  }

  @media all and (max-width: 50em) {
    .Modal.is-visible .content {
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      -webkit-overflow-scrolling: touch;
      border-radius: 0;
      transform: scale(1);
      margin-top: 5em;
      overflow-y: auto;
      padding-bottom: 6em;
    }

    .employee-card {
      border-right: none;
      border-bottom: 1px solid #e6e9ee;
      margin-bottom: 1em;
    }

    .employee-card-plus {
      border-right: none;
      border-bottom: 1px solid #e6e9ee;
      padding-top: 0;
      margin-bottom: 1em;
    }

    .employee-bubble {
      margin: auto;
      margin-bottom: 1em;
    }
  }

  /* Timeline Styles */

  .text-shadow--black {
    text-shadow: 1px 1px 1px #000;
  }

  .transparent-overlay {
    background-color: rgba(0, 0, 0, 0.75);
  }

  .inset-img {
    float: left;
    margin-right: 1em;
  }

  .timeline {
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: #fff;
    flex-direction: column;
  }

  .title {
    font-size: 38px;
    color: #616161;
    font-style: italic;
    font-weight: 800;
  }

  .swiper-container {
    height: 725px;
    width: 100%;
    position: relative;
  }

  .swiper-slide {
    position: relative;
    color: #fff;
    overflow: hidden;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center center;
  }

  .swiper-slide::after {
    content: "";
    position: absolute;
    z-index: 1;
    right: -115%;
    bottom: -10%;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.7);
    box-shadow: 30px 0 800px 43vw rgba(0, 0, 0, 0.5);
    border-radius: 100%;
  }

  .swiper-slide-content {
    position: absolute;
    text-align: left;
    width: 80%;
    max-width: 70%;
    right: 50%;
    top: 13%;
    -webkit-transform: translate(50%, 0);
    transform: translate(50%, 0);
    z-index: 2;
  }

  .timeline-year {
    display: block;
    font-style: italic;
    font-size: 42px;
    /*    margin-bottom: 50px;*/
    -webkit-transform: translate3d(20px, 0, 0);
    transform: translate3d(20px, 0, 0);
    color: #00bc6f;
    font-weight: 300;
    opacity: 0;
    transition: 0.2s ease 0.4s;
  }

  .timeline-title {
    font-weight: 800;
    font-size: 34px;
    margin: 0 0 30px;
    opacity: 0;
    -webkit-transform: translate3d(20px, 0, 0);
    transform: translate3d(20px, 0, 0);
    transition: 0.2s ease 0.5s;
  }

  .timeline-text {
    line-height: 1.5;
    opacity: 0;
    -webkit-transform: translate3d(20px, 0, 0);
    transform: translate3d(20px, 0, 0);
    transition: 0.2s ease 0.6s;
  }

  .swiper-slide-active .timeline-year {
    opacity: 1;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
    transition: 0.4s ease .6s;
  }

  .swiper-slide-active .timeline-title {
    opacity: 1;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
    transition: 0.4s ease .7s;
  }

  .swiper-slide-active .timeline-text {
    opacity: 1;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
    transition: 0.4s ease .8s;
  }

  .swiper-pagination {
    right: 9.7% !important;
    height: 100%;
    display: none;
    flex-direction: column;
    justify-content: center;
    font-style: italic;
    font-weight: 300;
    font-size: 18px;
    z-index: 1;
  }

  .swiper-pagination::before {
    content: "";
    position: absolute;
    left: -30px;
    top: 0;
    height: 100%;
    width: 1px;
    background-color: rgba(255, 255, 255, 0.2);
  }

  .swiper-pagination-bullet {
    width: auto;
    height: auto;
    text-align: center;
    opacity: 1;
    background: transparent;
    color: #fff;
    margin: 15px 0 !important;
    position: relative;
  }

  .swiper-pagination-bullet::before {
    content: "";
    position: absolute;
    top: 11px;
    left: -32.5px;
    width: 6px;
    height: 6px;
    border-radius: 100%;
    background-color: #00bc6f;
    -webkit-transform: scale(0);
    transform: scale(0);
    transition: 0.2s;
  }

  .swiper-pagination-bullet-active {
    color: #00bc6f;
  }

  .swiper-pagination-bullet-active::before {
    -webkit-transform: scale(1);
    transform: scale(1);
  }

  .swiper-button-next,
  .swiper-button-prev {
    background-size: 20px 20px;
    top: 15%;
    width: 20px;
    height: 20px;
    margin-top: 0;
    z-index: 2;
    transition: 0.2s;
  }

  .swiper-button-prev {
    left: 8%;
    background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M0%2C22L22%2C0l2.1%2C2.1L4.2%2C22l19.9%2C19.9L22%2C44L0%2C22L0%2C22L0%2C22z'%20fill%3D'%23fff'%2F%3E%3C%2Fsvg%3E");
  }

  .swiper-button-prev:hover {
    -webkit-transform: translateX(-3px);
    transform: translateX(-3px);
  }

  .swiper-button-next {
    right: 8%;
    background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M27%2C22L27%2C22L5%2C44l-2.1-2.1L22.8%2C22L2.9%2C2.1L5%2C0L27%2C22L27%2C22z'%20fill%3D'%23fff'%2F%3E%3C%2Fsvg%3E");
  }

  .swiper-button-next:hover {
    -webkit-transform: translateX(3px);
    transform: translateX(3px);
  }


  @media screen and (max-width: 570px) {
    .swiper-container {
      height: 750px;
    }

    .timeline-year {
      margin-bottom: .3em;
    }
  }

  /*
    @media screen and (max-width: 400px) {
    .swiper-container {
      height: 960px;
    }
  }
*/

  @media screen and (min-width: 769px) {

    .swiper-slide::after {
      right: -30%;
      bottom: -8%;
      width: 240px;
      height: 50%;
      box-shadow: 30px 0 800px 43vw rgba(0, 0, 0, 0.5);
    }

    .swiper-slide-content {
      right: 24%;
      top: 50%;
      -webkit-transform: translateY(-50%);
      transform: translateY(-50%);
      width: 70%;
    }

    .timeline-year {
      margin-bottom: 0;
      font-size: 3em;
    }

    .timeline-title {
      font-size: 46px;
      margin: 0;
    }

    .swiper-pagination {
      display: flex;
    }

    .swiper-button-prev {
      background-size: 30px 30px;
      top: 23%;
      left: auto;
      right: 10%;
      width: 30px;
      height: 20px;
      -webkit-transform: rotate(90deg) translate(0, 10px);
      transform: rotate(90deg) translate(0, 10px);
    }

    .swiper-button-prev:hover {
      -webkit-transform: rotate(90deg) translate(-3px, 10px);
      transform: rotate(90deg) translate(-3px, 10px);
    }

    .swiper-button-next {
      top: auto;
      bottom: 23%;
      right: 10%;
      -webkit-transform: rotate(90deg) translate(0, 10px);
      transform: rotate(90deg) translate(0, 10px);
    }

    .swiper-button-next:hover {
      -webkit-transform: rotate(90deg) translate(3px, 10px);
      transform: rotate(90deg) translate(3px, 10px);
    }
  }

  @media screen and (max-width: 769px) {
    .swiper-slide-content {
      top: 3%;
      width: 90%;
      max-width: 90%;
    }

    .swiper-button-prev,
    .swiper-button-next {
      background-size: 30px 30px;
      top: 6%;
      width: 30px;
      height: 30px;
    }

    .timeline-year {
      text-align: center;
      margin-bottom: .5em;
    }
  }

  @media screen and (max-width: 800px) {
    .transparent-overlay {
      margin-bottom: 1em;
    }
  }

  @media screen and (max-width: 980px) {
    .inset-img {
      margin-right: 100%;
    }
  }

  @media screen and (min-width:800px) and (max-width: 1024px) {
    .swiper-slide-content p {
      font-size: 85%;
    }
  }

  @media screen and (min-width: 1024px) {
    .swiper-slide::after {
      right: -20%;
      bottom: -12%;
      width: 240px;
      height: 50%;
      box-shadow: 30px 0 800px 43vw rgba(0, 0, 0, 0.5);
    }

    .swiper-slide-content {
      right: 21%;
    }
  }

</style>

<div class="js__seo-tool__body-content">

  <!-- Block 1 -->
  <div class="background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-Collage.jpg);">
    <div class="background--cover collage-banner hide__bg-image--mobile" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-Collage--bw.jpg);">
      <div class="container__centered">
        <div class="container center">
          <div style="height:450px;"></div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Block 1 -->

  <!-- Block 2 - Video -->
  <div class="content__callout">
    <div class="content__callout__media">
      <div class="content__callout__image-wrapper">
        <div class="video js__video" data-video-id="305095475">
          <figure class="video__overlay">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--50th-Neil-Pappalardo.jpg" alt="50th Anniversary Video">
          </figure>
          <a class="video__play-btn" href="https://vimeo.com/305095475"></a>
          <div class="video__container"></div>
        </div>
      </div>
    </div>
    <div class="content__callout__content">
      <div class="content__callout__body">
        <div class="content__callout__body__text">
          <h1 class="js__seo-tool__title" style="display:none;">MEDITECH's 50th Anniversary</h1>
          <h2>50 years of bold innovation.</h2>
          <p>A. Neil Pappalardo founded MEDITECH with the revolutionary belief that computers could transform the way we care for one another. Five decades later, that innovative spark burns brighter than ever. MEDITECH has stood at the vanguard of healthcare IT since Day One, embracing whatever changes the industry throws at us, and finding new ways to improve healthcare for patients, providers, and organizations everywhere. The most exciting part? We feel like we’re just getting started.</p>
        </div>
      </div>
    </div>
  </div>
  <!-- End Block 2 - Video -->


  <!-- Start Block 3 - Timeline -->
  <div class="timeline">
    <div class="swiper-container">
      <div class="swiper-wrapper">
        <div class="swiper-slide" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/meditech-cambridge-60s.jpg)" data-year="1960s">
          <div class="swiper-slide-content"><span class="timeline-year">1960s</span>
            <div class="timeline-text text-shadow--black">
              <div class="container__one-third transparent-overlay">
                <img class="hide--mobile" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/washington-dc-capitol-building.jpg" alt="U.S. Social Security Act Amendments" style="width:100%;">
                <p>The U.S. Social Security Act Amendments are signed into law on July 30, 1965, establishing Medicare and Medicaid health insurance for some of the most vulnerable populations. This legislation would challenge providers to accommodate an enormous influx of new patients.</p>
              </div>
              <div class="container__one-third transparent-overlay">
                <p style="float:left;"><img class="inset-img hide--mobile" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/neil-college-headshot.jpg" style="width:105px; margin-bottom: 0.5em;" alt="A. Neil Pappalardo">In 1966, MIT graduate A. Neil Pappalardo and his colleagues establish the first computer software language for the hospital environment. The MUMPS technology continues to be used today by many large hospitals for high-throughput transaction data processing.</p>
              </div>
              <div class="container__one-third transparent-overlay">
                <img class="hide--mobile" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/business-partners.jpg" alt="Business Partners" style="width:100%;">
                <p>In 1968, Pappalardo and his business partners found Medical Information Technologies Inc. (MEDITECH), a Massachusetts-based company creating lab IT systems for healthcare organizations. It officially opens for business on August 4, 1969.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="swiper-slide" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/meditech-westwood-70s.jpg); background-position: bottom;" data-year="1970s">
          <div class="swiper-slide-content"><span class="timeline-year">1970s</span>
            <div class="timeline-text text-shadow--black">
              <div class="container__one-third transparent-overlay">
                <img class="hide--mobile" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MRI-machine.jpg" alt="MRI scans introduced" style="width:100%;">
                <p>Healthcare costs rise dramatically during this decade of inflation, increasing hospital expenses and profits, and driving greater use of medical technology. New MRI and CT scans show how digital technologies can improve early diagnosis and treatments.</p>
              </div>
              <div class="container__one-third transparent-overlay">
                <img class="hide--mobile" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/HMO-Act-1973.jpg" alt="HMO Act of 1973 is enacted" style="width:100%;">
                <p>The Health Maintenance Organization (HMO) Act of 1973 is enacted, providing federal endorsement, certification, and assistance for prepaid group health plans.</p>
              </div>
              <div class="container__one-third transparent-overlay">
                <img class="hide--mobile" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MAGIC-software.jpg" alt="MAGIC platform introduced" style="width:100%;">
                <p>MEDITECH expands its product line to encompass a more integrated healthcare information system (HIS). In 1979, the company also releases a powerful new I.T. platform called MAGIC, the foundation of MEDITECH’s software for the next decade.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="swiper-slide" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/meditech-office-80s.jpg); background-position:bottom;" data-year="1980s">
          <div class="swiper-slide-content"><span class="timeline-year">1980s</span>
            <div class="timeline-text text-shadow--black">
              <div class="container__one-third transparent-overlay">
                <img class="hide--mobile" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Profit-Based-Focus.jpg" alt="Privatization and profit based focus" style="width:100%;">
                <p>U.S. healthcare shifts toward increased privatization and a more corporate (profit-based) focus. Medicare shifts to payment by diagnosis (DRG), instead of by treatment, and private health insurance plans soon follow suit.</p>
              </div>
              <div class="container__one-third transparent-overlay">
                <img class="hide--mobile" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Magic-software-2.jpg" alt="First Macintosh PCs" style="width:100%;">
                <p>The HIV/AIDS epidemic and rising cocaine use highlight a growing need for more proactive education and public health strategies. At the same time, commercial cell phones and the first Macintosh PCs set the stage for more informed, tech-savvy consumers.</p>
              </div>
              <div class="container__one-third transparent-overlay">
                <img class="hide--mobile" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/canada.jpg" alt=" MAGIC customers grow in Canada and Saudi Arabia" style="width:100%;">
                <p>MEDITECH continues to grow its integrated MAGIC product line, while also expanding into international territory with new customers in Canada and Saudi Arabia. </p>
              </div>
            </div>
          </div>
        </div>
        <div class="swiper-slide" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/meditech-canton-90s.jpg);" data-year="1990s">
          <div class="swiper-slide-content"><span class="timeline-year">1990s</span>
            <div class="timeline-text text-shadow--black">
              <div class="container__one-third transparent-overlay">
                <img class="hide--mobile" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/No-Health-Insurance.jpg" alt="Health care reform fails" style="width:100%;">
                <p>Healthcare reform fails, and costs rise at double the rate of inflation despite Managed Care efforts. By the decade’s end, 44 million Americans are living without health insurance.</p>
              </div>
              <div class="container__one-third transparent-overlay">
                <img class="hide--mobile" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/the-internet.jpg" alt="Introduction of The Internet" style="width:100%;">
                <p>The Internet opens up a whole new world for patients to have greater access to healthcare information, while email makes communication between providers (as well as between providers and patients) both easier and faster.</p>
              </div>
              <div class="container__one-third transparent-overlay">
                <img class="hide--mobile" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/HIPAA.jpg" alt="Introduction of HIPAA" style="width:100%;">
                <p>With the introduction of HIPAA and the IOM’s “To Err is Human” report on medical errors, clinicians must take on greater responsibility for providing safer, more secure, and higher quality care. MEDITECH introduces its new, user-friendly Client/Server IT platform.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="swiper-slide" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/meditech-southcoast-00s.jpg);" data-year="2000s">
          <div class="swiper-slide-content"><span class="timeline-year">2000s</span>
            <div class="timeline-text text-shadow--black">
              <div class="container__one-third transparent-overlay">
                <img class="hide--mobile" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/human-genome.jpg" alt="NIH announces sequencing of human genome" style="width:100%;">
                <p>In 2000, the National Institutes of Health (NIH) announces the initial sequencing of the human genome, an achievement that would advance prevention, diagnosis, and treatment strategies for many genetic disorders.</p>
              </div>
              <div class="container__one-third transparent-overlay">
                <img class="hide--mobile" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/50-states.jpg" alt="MEDITECH introduces 6.0 EHR platform" style="width:100%;">
                <p>MEDITECH introduces its 6.0 EHR platform as the first step towards a truly integrated, interoperable healthcare ideal. The company now has a growing presence in all 50 U.S. states, as well as Canada, the UK, Ireland, and South Africa.</p>
              </div>
              <div class="container__one-third transparent-overlay">
                <img class="hide--mobile" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/HITECH-Act.jpg" alt="HITECH Act creates financial incentives" style="width:100%;">
                <p>The Health Information Technology for Economic and Clinical Health (HITECH) Act creates financial incentives for the adoption and meaningful use of EHRs. Interest and investment in EHRs skyrocket, but true culture change takes time.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="swiper-slide" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/meditech-foxboro-auditorium-10s.jpg); background-position:bottom;" data-year="2010s">
          <div class="swiper-slide-content"><span class="timeline-year">2010s</span>
            <div class="timeline-text text-shadow--black">
              <div class="container__one-third transparent-overlay">
                <img class="hide--mobile" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/ACA.jpg" alt="Affordable Care Act creates cost-efficient health insurance" style="width:100%;">
                <p>As the Affordable Care Act (ACA) expands cost-efficient healthcare insurance coverage across the U.S., providers are pushed to adopt care delivery methods that emphasize value. More healthcare organizations undertake population health and predictive surveillance strategies as a result.</p>
              </div>
              <div class="container__one-third transparent-overlay">
                <img class="hide--mobile" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/activity-tracker.jpg" alt="MEDITECH's Patient Portal empowers patients" style="width:100%;">
                <p>The opioid crisis, as well as increasing prevalence of chronic diseases, highlights the need for providers to build stronger relationships with patients and promote wellness. The integration of MEDITECH’s Patient Portal with wearable activity trackers empowers patients to take more responsibility for their own health.</p>
              </div>
              <div class="container__one-third transparent-overlay">
                <img class="hide--mobile" src="<?php print $url; ?>/sites/all/themes/meditech/images/MEDITECH-expanse-logo--green-white.svg" alt="MEDITECH Expanse logo" style="padding: 1em; width:100%;">
                <p>As healthcare expands far beyond the walls of the hospital and physician’s office, administrative demands on clinicians begin to take their toll. MEDITECH Expanse - the first entirely web-based EHR - offers them a more mobile, personalized user experience, while connecting the complete patient story across all care settings and networks. </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="swiper-button-prev"></div>
      <div class="swiper-button-next"></div>
      <div class="swiper-pagination"></div>
    </div>
  </div>

  <script>
    var timelineSwiper = new Swiper('.timeline .swiper-container', {
      direction: 'vertical',
      loop: false,
      speed: 1000,
      pagination: '.swiper-pagination',
      paginationBulletRender: function(swiper, index, className) {
        var year = document.querySelectorAll('.swiper-slide')[index].getAttribute('data-year');
        return '<span class="' + className + '">' + year + '</span>';
      },
      paginationClickable: true,
      nextButton: '.swiper-button-next',
      prevButton: '.swiper-button-prev',
      breakpoints: {
        769: {
          direction: 'horizontal',
        }
      }
    });

  </script>
  <!-- End Block 3 -->


  <!-- Start Block 3 -->
  <div class="container background--cover border-none" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Employee-Bubbles-Block.jpg); background-position: top; padding-bottom: 8em;">

    <div class="container__centered auto-margins center text--white" style="padding-bottom:2em;">
      <h2>More than software.</h2>
      <p>Healthcare is all about people, and MEDITECH is no exception. We may be best known for our solutions, but our identity runs deeper than that. Meet some of the unique people behind your EHR.</p>
    </div>


    <div class="container__centered" style="padding-bottom:2em;">

      <div class="container__one-third">
        <a href="#Popup1" class="button">
          <div class="employee-bubble eb-1">
            <div class="eb-info">
              <h3 class="no-margin--bottom">Nhien Bui</h3>
              <p>Learn more</p>
            </div>
          </div>
        </a>

        <div id="Popup1" class="Modal">
          <div class="content">
            <div>

              <div class="container__one-third center employee-card">
                <div class="modal-employee eb-1"></div>
                <h3 class="no-margin--bottom">Nhien Bui</h3>
                <p class="no-margin--bottom">Product Manager</p>
                <p>Years at MEDITECH: <strong>2</strong></p>
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/meditech-logo.svg" style="width:175px;" alt="MEDITECH Logo">
              </div>
              <div class="container__two-thirds">
                <h4 class="text--meditech-green no-margin--bottom">MEDITECH is…</h4>
                <p>Ever-changing.</p>
                <h4 class="text--meditech-green no-margin--bottom">What does MEDITECH mean to you?</h4>
                <p>Collaboration.</p>
                <h4 class="text--meditech-green no-margin--bottom">What was your first car?</h4>
                <p>Toyota Cressida.</p>
                <h4 class="text--meditech-green no-margin--bottom">What is your favorite TV show to binge-watch?</h4>
                <p>Curb Your Enthusiasm, Gotham, and Veep.</p>
                <h4 class="text--meditech-green no-margin--bottom">What is the most interesting place you’ve traveled to?</h4>
                <p>Falmouth luminous lagoon in Jamaica.</p>
              </div>

            </div>
            <div class="close"></div>
          </div>
        </div>
      </div>


      <div class="container__one-third">

        <a href="#Popup2" class="button">
          <div class="employee-bubble eb-4">
            <div class="eb-info">
              <h3 class="no-margin--bottom">Samantha Kerley</h3>
              <p>Learn more</p>
            </div>
          </div>
        </a>

        <div id="Popup2" class="Modal">
          <div class="content">
            <div>
              <div class="container__one-third center employee-card">
                <div class="modal-employee eb-4"></div>
                <h3 class="no-margin--bottom">Samantha Kerley</h3>
                <p class="no-margin--bottom">Supervisor, Implementation <br>Ambulatory Clinical</p>
                <p>Years at MEDITECH: <strong>6</strong></p>
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/meditech-logo.svg" style="width:175px;" alt="MEDITECH Logo">
              </div>
              <div class="container__two-thirds">
                <h4 class="text--meditech-green no-margin--bottom">MEDITECH is…</h4>
                <p>A journey!</p>
                <h4 class="text--meditech-green no-margin--bottom">Describe your most impactful experience at MEDITECH.</h4>
                <p>My first site visit, as a specialist in our Home Health and Hospice service group. The site was a pediatric hospital and the visit was a system optimization. While your first site visit as a specialist is always memorable, traveling with the home health nurse to these young patients' homes and seeing the product utilized was truly a motivating experience.</p>
                <h4 class="text--meditech-green no-margin--bottom">What is your most prized possession?</h4>
                <p>My college degree (Go Gamecocks!)</p>
                <h4 class="text--meditech-green no-margin--bottom">What is your favorite TV show to binge-watch?</h4>
                <p>Prison Break.</p>
                <h4 class="text--meditech-green no-margin--bottom">What is the best advice you ever received?</h4>
                <p>"Be True to Yourself".</p>
              </div>

            </div>
            <div class="close"></div>
          </div>
        </div>
      </div>

      <div class="container__one-third">

        <a href="#Popup3" class="button">
          <div class="employee-bubble eb-2">
            <div class="eb-info">
              <h3 class="no-margin--bottom">Fred Mayer</h3>
              <p>Learn more</p>
            </div>
          </div>
        </a>

        <div id="Popup3" class="Modal">
          <div class="content">
            <div>
              <div class="container__one-third center employee-card-plus">
                <div class="modal-employee eb-2"></div>
                <h3 class="no-margin--bottom">Fred Mayer</h3>
                <p class="no-margin--bottom">Senior Promotional Writer</p>
                <p>Years at MEDITECH: <strong>22</strong></p>
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/meditech-logo.svg" style="width:175px;" alt="MEDITECH Logo">
              </div>
              <div class="container__two-thirds">
                <h4 class="text--meditech-green no-margin--bottom">MEDITECH is…</h4>
                <p>Like family -- both in terms of our relationships with our co-workers and our relationships with customers. It's the opposite of the cold, impersonal corporate feeling most of my friends tell me they experience in their own jobs.</p>
                <h4 class="text--meditech-green no-margin--bottom">What is the biggest change in healthcare you’ve seen in recent years?</h4>
                <p>Consumerism. We all know more as patients these days, and we expect more from our providers in terms of access, convenience, and cost.</p>
                <h4 class="text--meditech-green no-margin--bottom">Describe your most impactful experience at MEDITECH.</h4>
                <p>I get an opportunity to interview users of our software, and it's very meaningful to hear stories about how our work is benefitting providers and patients.</p>
                <h4 class="text--meditech-green no-margin--bottom">Where is the most interesting place you've visited?</h4>
                <p>I had an opportunity years ago to visit our colleagues at MEDITECH South Africa in Johannesburg and travel across the country to Cape Town. It was an experience I'll never forget.</p>
              </div>

            </div>
            <div class="close"></div>
          </div>
        </div>
      </div>

    </div>

    <div class="container__centered">

      <div class="container__one-third">
        <a href="#Popup4" class="button">
          <div class="employee-bubble eb-3">
            <div class="eb-info">
              <h3 class="no-margin--bottom">Michael Triggs</h3>
              <p>Learn more</p>
            </div>
          </div>
        </a>

        <div id="Popup4" class="Modal">
          <div class="content">
            <div>
              <div class="container__one-third center employee-card-plus">
                <div class="modal-employee eb-3"></div>
                <h3 class="no-margin--bottom">Michael Triggs</h3>
                <p class="no-margin--bottom">Director, Development</p>
                <p>Years at MEDITECH: <strong>21</strong></p>
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/meditech-logo.svg" style="width:175px;" alt="MEDITECH Logo">
              </div>
              <div class="container__two-thirds">
                <h4 class="text--meditech-green no-margin--bottom">MEDITECH is...</h4>
                <p>An extended family. I've made great friends, learned much about people, and shared life experiences, both good and bad. </p>
                <h4 class="text--meditech-green no-margin--bottom">What is the biggest change in healthcare you've seen in recent years?</h4>
                <p>Beyond the obvious that we've all seen with changes like Meaningful Use? Advancements in medication, especially around "orphan diseases." The advancements in treatment for illnesses such as Cystic Fibrosis over the past two decades is absolutely mind-boggling.</p>
                <h4 class="text--meditech-green no-margin--bottom">Where is the most interesting place you've traveled to?</h4>
                <p>The National Mustard Museum in Middleton, Wisconsin. Seriously, Americana of this nature is what makes traveling so interesting. </p>
                <h4 class="text--meditech-green no-margin--bottom">What was your first car?</h4>
                <p>A 1979 Dodge Aspen. It was only nine years old when I bought it, but wow, was it worn out.</p>
                <h4 class="text--meditech-green no-margin--bottom">What is the best advice you've ever received?</h4>
                <p>Find a hobby that differs radically from your work - not for the separation of the two, but because it adds a dimension to your character.</p>
              </div>

            </div>
            <div class="close"></div>
          </div>
        </div>
      </div>

      <div class="container__one-third">
        <a href="#Popup5" class="button">
          <div class="employee-bubble eb-5">
            <div class="eb-info">
              <h3 class="no-margin--bottom">Caryn Budd</h3>
              <p>Learn more</p>
            </div>
          </div>
        </a>

        <div id="Popup5" class="Modal">
          <div class="content">
            <div>
              <div class="container__one-third center employee-card-plus">
                <div class="modal-employee eb-5"></div>
                <h3 class="no-margin--bottom">Caryn Budd</h3>
                <p class="no-margin--bottom">Account Manager</p>
                <p>Years at MEDITECH: <strong>22</strong></p>
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/meditech-logo.svg" style="width:175px;" alt="MEDITECH Logo">
              </div>
              <div class="container__two-thirds">
                <h4 class="text--meditech-green no-margin--bottom">MEDITECH is…</h4>
                <p>So much more than a ‘job’, it’s been an exciting, challenging career and journey!</p>
                <h4 class="text--meditech-green no-margin--bottom">What is a surprising fact about yourself?</h4>
                <p>I was the first girl in Randolph (1978) to play Little League (opposed to softball), I just didn't want to play with the girls ( I was a complete tomboy).</p>
                <h4 class="text--meditech-green no-margin--bottom">Where is the most interesting place you've traveled to?</h4>
                <p>Switzerland...especially Zermatt and taking the tram to the top and staring out at the Matterhorn that stands 1500 ft. tall.</p>
                <h4 class="text--meditech-green no-margin--bottom">What is your most prized possession?</h4>
                <p>Well besides my son, I would have to say my grandmothers diamond originally purchased in the 1920s. Also my grandfather's desk purchased in the 1930s.</p>
                <h4 class="text--meditech-green no-margin--bottom">What is your favorite part of the MEDITECH picnic?</h4>
                <p>Seeing friends/coworkers from the many years I have worked here and no longer see on a regular basis. Especially seeing how everyone's kids have gone from strollers to now being all grown up!</p>
              </div>

            </div>
            <div class="close"></div>
          </div>
        </div>
      </div>

      <div class="container__one-third">
        <a href="#Popup6" class="button">
          <div class="employee-bubble eb-6">
            <div class="eb-info">
              <h3 class="no-margin--bottom">Javier Guzmán</h3>
              <p>Learn more</p>
            </div>
          </div>
        </a>

        <div id="Popup6" class="Modal">
          <div class="content">
            <div>
              <div class="container__one-third center employee-card">
                <div class="modal-employee eb-6"></div>
                <h3 class="no-margin--bottom">Javier Guzmán</h3>
                <p class="no-margin--bottom">Senior Marketing Consultant</p>
                <p>Years at MEDITECH: <strong>9</strong></p>
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/meditech-logo.svg" style="width:175px;" alt="MEDITECH Logo">
              </div>
              <div class="container__two-thirds">
                <h4 class="text--meditech-green no-margin--bottom">MEDITECH is…</h4>
                <p>A great company to work for.</p>
                <h4 class="text--meditech-green no-margin--bottom">What is the biggest change in healthcare you’ve seen in recent years?</h4>
                <p>Meaningful Use. </p>
                <h4 class="text--meditech-green no-margin--bottom">What is your favorite sports team?</h4>
                <p>New England Patriots.</p>
                <h4 class="text--meditech-green no-margin--bottom">What is your favorite part of the MEDITECH picnic?</h4>
                <p>Free food!</p>
                <h4 class="text--meditech-green no-margin--bottom">What is your favorite band/artist today?</h4>
                <p>Daddy Yankee and Bad Bunny.</p>
              </div>

            </div>
            <div class="close"></div>
          </div>
        </div>
      </div>

    </div>

  </div>
  <!-- End Block 3 -->

  <!-- Start Block 4 -->
  <div class="container background--cover border-none" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/quote-bg--starry-northern-lights.jpg); background-position: top;">
    <div class="slider text--white" style="padding-bottom:2em;">
      <div class="container">
        <div class="container__one-half transparent-overlay" style="padding: 1em 2em;">
          <div class="text--fresh-mint" style="text-align:center; padding-bottom:1em;"><i class="far fa-comments fa-3x"></i></div>
          <p class="italic">"We are proud to celebrate <a href="https://www.businesswire.com/news/home/20190806005090/en/MEDITECH-Celebrates-50-Years-Healthcare-Technology-Innovation" target="_blank">50 years in this industry</a>, and we continue to strive each day to maintain the spirit of innovation that we’ve established as a company. Our Expanse platform is the result of our long-standing commitment to identify what clinicians need so they can provide the best care for their patients and deliver high-quality service at a reasonable cost."</p>
          <p class="text--large no-margin--bottom"><span class="bold">Michelle O'Connor</span></p>
          <p>President and Chief Operating Officer, MEDITECH</p>
        </div>
        <div class="container__one-half transparent-overlay" style="padding: 1em 2em;">
          <div class="text--fresh-mint" style="text-align:center; padding-bottom:1em;"><i class="far fa-comments fa-3x"></i></div>
          <p class="italic">"From its beginnings, MEDITECH has embraced a family-oriented culture that makes caring for people a priority. It's reflected in the software we create, and it's something that we've continued to instill in our employees as our company has grown. Whether it's giving back to our local communities or advancing healthcare on the global scale, we feel lucky to be in the position to improve people's lives." </p>
          <p class="text--large no-margin--bottom"><span class="bold">Lawrence A. Polimeno</span></p>
          <p>Vice Chairman, MEDITECH</p>
        </div>
      </div>
      <div class="container">
        <div class="container__one-half transparent-overlay" style="padding: 1em 2em;">
          <div class="text--fresh-mint" style="text-align:center; padding-bottom:1em;"><i class="far fa-comments fa-3x"></i></div>
          <p class="italic">“Fifty years later, we are still just people with a desire <a href="https://ehr.meditech.com/about/community">to help care for others</a>. The possibilities of technology will continue to move far beyond what even the young teams of today can envision. But the goal remains the same. Giving caregivers back their home life. Helping patients take <a href="https://ehr.meditech.com/ehr-solutions/to-improve-patient-engagement-focus-on-the-patient">control of their own health</a>. Putting people first.”</p>
          <p class="text--large no-margin--bottom"><span class="bold">Howard Messing</span></p>
          <p>CEO, MEDITECH</p>
        </div>
        <div class="container__one-half transparent-overlay" style="padding: 1em 2em;">
          <div class="text--fresh-mint" style="text-align:center; padding-bottom:1em;"><i class="far fa-comments fa-3x"></i></div>
          <p class="italic">“Throughout our 25-year relationship, we’ve seen healthcare undergo massive shifts. MEDITECH has always found ways to evolve with us, support new strategic directions, and deliver results. As our two organizations have grown, the dedication to this partnership and achieving our common goals has only deepened. The foundation of such a relationship is trust.”</p>
          <p class="text--large no-margin--bottom"><span class="bold">P. Martin Paslick</span></p>
          <p>Senior Vice President and CIO, HCA</p>
        </div>
      </div>
      <div class="container">
        <div class="container__one-half transparent-overlay" style="padding: 1em 2em;">
          <div class="text--fresh-mint" style="text-align:center; padding-bottom:1em;"><i class="far fa-comments fa-3x"></i></div>
          <p class="italic">"In the beginning we had no great and far reaching plan. Each event, every decision, a success here, a mistake there, would take us another step forward."</p>
          <p class="text--large no-margin--bottom"><span class="bold">A. Neil Pappalardo</span></p>
          <p>Chairman &amp; Founder, MEDITECH</p>
        </div>
        <div class="container__one-half transparent-overlay" style="padding: 1em 2em;">
          <div class="text--fresh-mint" style="text-align:center; padding-bottom:1em;"><i class="far fa-comments fa-3x"></i></div>
          <p class="italic">“From MAGIC, to C/S, to Expanse, MEDITECH has always found ways to bring something new to the table. Over the course of our 30-year partnership, they’ve provided Avera Health with EHR solutions that have helped us to grow into a multi-state regional healthcare system. They understand what an organization needs from their EHR, and what providers and patients want from it.”</p>
          <p class="text--large no-margin--bottom"><span class="bold">Jim Veline</span></p>
          <p>Senior VP and CIO, Avera Health</p>
        </div>
      </div>
      <div class="container">
        <div class="container__one-half transparent-overlay" style="padding: 1em 2em;">
          <div class="text--fresh-mint" style="text-align:center; padding-bottom:1em;"><i class="far fa-comments fa-3x"></i></div>
          <p class="italic">“I always feel MEDITECH’s focus is on us, the customer, and the patients that we care for. It’s not about next quarter’s earnings. It’s about delivering a long-term investment, and building a long-term relationship with your partners that matters.”</p>
          <p class="text--large no-margin--bottom"><span class="bold">Denni McColm</span></p>
          <p>CIO, Citizens Memorial Hospital</p>
        </div>
        <div class="container__one-half transparent-overlay" style="padding: 1em 2em;">
          <div class="text--fresh-mint" style="text-align:center; padding-bottom:1em;"><i class="far fa-comments fa-3x"></i></div>
          <p class="italic">“I think MEDITECH listens to their customers and their needs. They recognize that physicians and nurses are experts too, and they’ve evolved their product in a way that makes customers say, ‘You know, they heard what we wanted, and found a creative way to build a system that works best for us.’ That’s something that sets MEDITECH apart from other vendors.”</p>
          <p class="text--large no-margin--bottom"><span class="bold">Bruce McNulty, MD</span></p>
          <p>VP and CMO, Swedish Covenant Hospital</p>
        </div>
      </div>
      <div class="container">
        <div class="container__one-half transparent-overlay" style="padding: 1em 2em;">
          <div class="text--fresh-mint" style="text-align:center; padding-bottom:1em;"><i class="far fa-comments fa-3x"></i></div>
          <p class="italic">“From the early days of our MIIS install (yes, I remember that), MEDITECH has always had a vision of the future. Technology has changed in ways we could never dream of, and yet MEDITECH has been a leader throughout this journey. Once again, they are on the cutting edge as they introduce Expanse and new ways to envision healthcare data. It is hard to believe it has been 50 years, or that I have had the pleasure of working with Neil, Howard and the team for over 34 years. Congratulations on 50 years of innovation and customer service!”</p>
          <p class="text--large no-margin--bottom"><span class="bold">Mark Farrow</span></p>
          <p>VP HITS, CIO, Hamilton Health Science</p>
        </div>
        <div class="container__one-half transparent-overlay" style="padding: 1em 2em;">
          <div class="text--fresh-mint" style="text-align:center; padding-bottom:1em;"><i class="far fa-comments fa-3x"></i></div>
          <p class="italic">“As a customer and consultant, there’s a family component to MEDITECH. It’s not just software. It’s not just delivering products. It’s the experience. Expanse is designed with a thoughtfulness that shows that MEDITECH is paying attention to their customers and how they interact and grow with the platform. It’s like family — you know that MEDITECH is going to take care of you.”</p>
          <p class="text--large no-margin--bottom"><span class="bold">William Gustin, MD</span></p>
          <p>MEDITECH</p>
        </div>
      </div>
    </div>
  </div>

  <link rel="stylesheet" href="<?php print $url; ?>/sites/all/themes/meditech/css/slick.css">
  <link rel="stylesheet" href="<?php print $url; ?>/sites/all/themes/meditech/css/slick-theme.css">
  <script src="<?php print $url; ?>/sites/all/themes/meditech/js/slick.min.js"></script>
  <script>
    jQuery(document).ready(function($) {
      $('.slider').slick({
        dots: true,
        prevArrow: '<a class="slick-prev">&#10092;</a>',
        nextArrow: '<a class="slick-next">&#10093;</a>',
      });
    });

  </script>
  <!-- End Block 4 -->

  <!-- Start Block 5 -->
  <div class="container background--cover border-none" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/50th-group-photo--centered.jpg);">
    <div class="container no-pad--top logo-position">
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/50th-anniversary-logo--white.png" style="width:175px; margin-bottom:1em;" alt="MEDITECH 50th Logo">
    </div>
    <div class="container__centered logo-box-height">&nbsp;</div>
    <div class="container__centered text--white">
      <div class="container transparent-overlay center" style="margin: 3em auto 0 auto; max-width: 57em;">
        <p class="text--large" style="margin: 0;">50 years in healthcare has given us so much to celebrate, and we want <span class="italic">you</span> to be part of the conversation. Use <span class="bold header-four">#MEDITECH50</span> on social media, and join the celebration!</p>

        <div style="margin-top:1em;">
          <?php print $share_link_buttons; ?>
        </div>

      </div>
    </div>
  </div>


</div>
<!-- end js__seo-tool__body-content -->

<!-- End Block 5 -->
<script>
  $jq.fn.expose = function(options) {
    var $modal = $jq(this),
      $trigger = $jq("a[href=" + this.selector + "]");
    $modal.on("expose:open", function() {
      $modal.addClass("is-visible");
      $modal.trigger("expose:opened");
    });
    $modal.on("expose:close", function() {
      $modal.removeClass("is-visible");
      $modal.trigger("expose:closed");
    });
    $trigger.on("click", function(e) {
      e.preventDefault();
      $modal.trigger("expose:open");
    });
    $modal.add($modal.find(".close")).on("click", function(e) {

      e.preventDefault();

      // if it isn't the background or close button, bail
      if (e.target !== this)
        return;

      $modal.trigger("expose:close");
    });
    return;
  }
  $jq("#Popup1").expose();
  $jq("#Popup2").expose();
  $jq("#Popup3").expose();
  $jq("#Popup4").expose();
  $jq("#Popup5").expose();
  $jq("#Popup6").expose();

  // Example Cancel Button
  $jq(".cancel").on("click", function(e) {

    e.preventDefault();
    $jq(this).trigger("expose:close");
  });

</script>

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>
<!-- END about--node-4215.php -->
