<!-- START campaign--node-3518.php -->
<?php // This template is set up to control the display of the NEW 2021 campaign Care Coordination content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');
$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>


<div class="js__seo-tool__body-content">

    <style>
        .button-vertical-adjustment {
            margin-top: 8em;
        }

        .fa-check-square:before {
            content: "\f14a";
            color: rgb(255, 255, 255);
        }

        .fa-arrow-alt-circle-right:before {
            content: "\f35a";
            color: rgb(37, 181, 112);
        }

        .fa-ul {
            list-style-type: none;
            margin-left: 2.5em;
            padding-left: 0;
        }

        .card__info {
            padding: 1em 2em;
        }

        @media all and (max-width: 50em) {
            .content__callout__content {
                padding-left: 6%;
                color: #3e4545;
                padding-top: 0em;
            }

            .video {
                max-width: 100%;
            }
        }

        .video--shadow {
            border-radius: 6px;
            box-shadow: 13px 13px 40px rgba(0, 0, 0, 0.3);
        }


        @media (max-width: 800px) {
            .button-vertical-adjustment {
                margin-top: 1em;
            }
        }

        .spacing {
            margin-bottom: 1em;
        }

        @media (max-width: 800px) {
            .spacing {
                margin-bottom: 0;
            }

            .transparent-overlay {
                margin-bottom: 1em;
            }
        }

    </style>


    <!-- Block 1 -->
    <div class="gl-container">
        <div class="container__one-half background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Coordinating-Your-Care.jpg); min-height: 23em;">

        </div>

        <div class="container__one-half gl-text-pad bg--green-gradient">

            <div class="text--white">
                <h1 class="text--white js__seo-tool__title" style="margin-top:0;">
                    MEDITECH's Care Coordination connects every point of patient care
                </h1>
                <h2 class="text--white">
                    Coordinate care with the power of one EHR.
                </h2>
                <p>Navigate all the complex twists and turns of healthcare with <a href="https://ehr.meditech.com/expanse">MEDITECH Expanse</a>. You’ll have one system so you can effectively focus on the whole patient, wherever they may be in their journey.</p>
            </div>

            <div>

                <div>
                    <?php hubspot_button($cta_code, "Read The Val Verde Patient Engagement Success Story"); ?>
                </div>
            </div>

        </div>

    </div>
    <!-- End of Block 1 -->


    <!--Block 2-->
    <div class="container bg--white">
        <div class="container__centered center text--black-coconut">

            <div style="margin-bottom:2em;">
                <h2>When the dots connect themselves, the whole picture becomes clear.</h2>
                <p>
                    Extend care beyond the hospital and practice. Share information across all settings to get a complete view of your patients' needs.
                </p>
            </div>

            <div>
                <div class="container__one-half mobile-padding" style="padding: 1em;">
                    <div class="container__one-fifth center">
                        <img style="max-width: 100px;" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/urgent-care.svg" alt="Urgent Care icon">
                    </div>
                    <div class="container__four-fifths" style="text-align: left;">
                        <h4>Urgent Care</h4>
                        <p>Keep track of patients' minor ailments and medication changes that occur 24/7, with MEDITECH’s Virtual On Demand Care.</p>
                    </div>
                </div>

                <div class="container__one-half mobile-padding" style="padding: 1em;">
                    <div class="container__one-fifth center">
                        <img style="max-width: 100px;" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/em-dept.svg" alt="Emergency Department icon">
                    </div>
                    <div class="container__four-fifths" style="text-align: left;">
                        <h4>Emergency Department</h4>
                        <p>Leverage information and documentation in one cohesive chart that facilitates care delivery inside and outside <a href="https://ehr.meditech.com/ehr-solutions/meditech-ed">the ED</a>.</p>
                    </div>
                </div>
            </div>



            <div>
                <div class="container__one-half mobile-padding" style="padding: 1em;">
                    <div class="container__one-fifth center">
                        <img style="max-width: 100px;" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/behavioral-health.svg" alt="Behavioral Health icon">
                    </div>
                    <div class="container__four-fifths" style="text-align: left;">
                        <h4>Behavioral Health</h4>
                        <p>Focus on patients' mental and physical health with tools to collaborate on intervention and support.</p>
                    </div>
                </div>

                <div class="container__one-half mobile-padding" style="padding: 1em;">
                    <div class="container__one-fifth center">
                        <img style="max-width: 100px;" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/rehab-icon.svg" alt="Rehab icon">
                    </div>
                    <div class="container__four-fifths" style="text-align: left;">
                        <h4>Rehabilitation</h4>
                        <p>Facilitate care team collaboration around patients' goals for positive patient outcomes. Embedded IRF-PAI documentation ensures accurate reimbursement.</p>
                    </div>
                </div>
            </div>



            <div>
                <div class="container__one-half mobile-padding" style="padding: 1em;">
                    <div class="container__one-fifth center">
                        <img style="max-width: 100px;" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/long-term-care.svg" alt="Long Term Care icon">
                    </div>
                    <div class="container__four-fifths" style="text-align: left;">
                        <h4>Long-Term Care</h4>
                        <p>Ensure smoother transitions of care for patients and residents using our single, integrated EHR.</p>
                    </div>
                </div>

                <div class="container__one-half mobile-padding" style="padding: 1em;">
                    <div class="container__one-fifth center">
                        <img style="max-width: 100px;" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/home-care-house.svg" alt="Home Care icon">
                    </div>
                    <div class="container__four-fifths" style="text-align: left;">
                        <h4>Home Care</h4>
                        <p>Support patients at every stage of life, in the comfort of home, with three integrated options — <a href="https://ehr.meditech.com/ehr-solutions/meditech-home-care">Home Health, Hospice, and Telehealth</a>.</p>
                    </div>
                </div>
            </div>


        </div>
    </div>

    <!--End of Block 2-->

    <!--Block 3-->
    <div class="container no-pad">
        <div class="gl-container">
            <div class="container__one-half gl-text-pad bg--green-gradient no-pad">

                <h2>
                    Keep patients on track.
                </h2>
                <p>
                    Case managers, advocate for your patients to receive the care they need. MEDITECH's Case Management offers a centralized solution for coordinating all patient activity from admissions through discharge. Keep your patients on a healthy path by tracking compliance, documenting progress towards their goals, managing care transitions, and planning for discharge.
                </p>
                <p class="no-pad--bottom">
                    Our Case Management solution includes:
                </p>
                <ul class="fa-ul">
                    <li><span class="fa-li"><i class="fas fa-check-square" aria-hidden="true"></i></span>
                        Easy access to multidisciplinary plans of care and clinically significant documentation
                    </li>

                    <li><span class="fa-li"><i class="fas fa-check-square" aria-hidden="true"></i></span>
                        Evidence-based care guidelines using MCG Indicia or McKesson Interqual to support utilization review
                    </li>
                    <li><span class="fa-li"><i class="fas fa-check-square" aria-hidden="true"></i></span>
                        Remote monitoring capabilities via at-home devices to track high-risk, high-cost populations
                    </li>
                    <li><span class="fa-li"><i class="fas fa-check-square" aria-hidden="true"></i></span>
                        A Community Care Transitions Portal that integrates case managers with post-acute facilities through a secure web portal for streamlined communication and discharge workflow planning.
                    </li>
                </ul>
            </div>


            <div class="container__one-half background--cover hide-on-tablets" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/coordination-care-dr-and-nurse-wheeling-man-out-of-hospital.jpg); min-height:25em; background-position: top;">

            </div>

        </div>
    </div>
    <!--End of Block 3-->

    <!--Block 4-->
    <div class="gl-container">
        <div class="container__one-half background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/connected-everywhere-across-networks.jpg); min-height: 23em;">

        </div>
        <div class="container__one-half gl-text-pad bg--green-gradient">
            <h2>Far and Wide.</h2>
            <p>
                Your patients don't always receive care in your network. Standards-based interoperability benefits everyone and helps you connect across the continuum of care, HIEs, and public health agencies with ease.
            </p>
            <p>
                <a href="https://ehr.meditech.com/ehr-solutions/meditech-interoperability">Traverse</a>, MEDITECH's interoperability solution included in Expanse, ensures you have all the components you need, in one bundle, to exchange information the way you want. From local and regional connections, to enrollment in nationwide data exchange services, Traverse can help you to securely share data between clinicians and patients throughout the entire healthcare landscape.

            </p>

            <p style="padding-top: 2em;"><strong>
                    <a href="https://ehr.meditech.com/news/beth-israel-lahey-health-connects-care-with-one-touch-video">Learn how Beth Israel Lahey Health</a> is improving both the clinician and patient experience with One Touch access.
                </strong></p>
        </div>

    </div>
    <!--End of Block 4-->

    <!-- Block 5 -->
    <!--
	<div id="modal1" class="modal">
		<a class="close-modal" href="javascript:void(0)">&times;</a>
		<div class="modal-content">
			<img src="< ?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-screenshot--patient-registries.png" alt="MEDITECH Care Coordination tablet screenshot">
		</div>
	</div>
	<div class="container bg--light-gray">
		<div class="container__centered">
			<div class="container__one-third">
				<h2>
					Stay ahead of your patients’ needs...
				</h2>
				<p>Know who you're accountable for, and help them to manage their health. Use MEDITECH's <a href="< ?php print $url; ?>/ehr-solutions/meditech-population-health">Patient Registries</a> as the basis of a care management program that enables you to keep healthy patients well, and intervene before minor conditions spiral out of control. These actionable registries populate with real-time patient data from across care settings, including remote monitoring devices and fitness trackers.</p>

			</div>
			<div class="container__two-thirds">
				<div class="open-modal" data-target="modal1">
					<div class="tablet--white">
						<img src="< ?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-screenshot--patient-registries.png" alt="MEDITECH Care Coordination tablet screenshot">
					</div>
					<div class="mag-bg">
						<i class="mag-icon fas fa-search-plus"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
-->

    <div id="modal1" class="modal">
        <a class="close-modal" href="javascript:void(0)">&times;</a>
        <div class="modal-content">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-screenshot--patient-registries.png" alt="MEDITECH Care Coordination tablet screenshot">
        </div>
    </div>
    <div class="container bg--light-gray">
        <div class="container__centered">
            <div class="container__one-third">
                <h2>
                    Stay ahead of your patients’ needs...
                </h2>
                <p>Know whom you are accountable for, and help them manage their health. Use the <a href="<?php print $url; ?>/ehr-solutions/care-compass">Expanse Care Compass Registries</a> as the basis of a care management program that enables you to keep healthy patients well and intervene before minor conditions spiral out of control.</p>
                <p>These actionable registries populate with real-time patient data from across care settings including remote monitoring devices and fitness trackers.</p>
                <p>When paired with Expanse Population Insight, clinicians are able to develop a more holistic view of their patients and capitalize on identified risk and open care gaps.
                </p>

            </div>
            <div class="container__two-thirds">
                <div class="open-modal" data-target="modal1">
                    <div class="tablet--white">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-screenshot--patient-registries.png" alt="MEDITECH Care Coordination tablet screenshot">
                    </div>
                    <div class="mag-bg">
                        <i class="mag-icon fas fa-search-plus"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Block 5 -->

    <!-- Block 6 -->
    <!--
    <div class="container background--cover" style="background-image:url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/bg--organic-shapes-3.svg);">
        <div class="container__centered center">
            <div class="auto-margins" style="text-align: center; margin-bottom:3em;">
                <h2>Get a complete picture of patient risk and utilization.</h2>
                <p>
                    Our collaboration with <a href="https://www.meditech.com/productbriefs/flyers/Arcadia_Flyer.pdf" target="_blank">Arcadia.io</a> enables us to embed Arcadia-supplied data elements into your care management workflow, allowing you to close clinical care gaps and extend your population health initiatives.
                </p>
            </div>

            <h4 class="center">Arcadia-Supplied Data + Advanced Analytics = Results</h4>

            <div style="padding-top: 1em; text-align: left;">

                <div class="container__one-third shadow-box">
                    <p><strong>Arcadia-Supplied Data
                        </strong></p>
                    <ul class="fa-ul">
                        <li><span class="fa-li"><i class="fas fa-arrow-alt-circle-right" aria-hidden="true"></i></span>Claims Data
                        </li>
                        <li><span class="fa-li"><i class="fas fa-arrow-alt-circle-right" aria-hidden="true"></i></span>Other-vendor EHR data
                        </li>
                        <li><span class="fa-li"><i class="fas fa-arrow-alt-circle-right" aria-hidden="true"></i></span>Clinical care gaps
                        </li>
                        <li><span class="fa-li"><i class="fas fa-arrow-alt-circle-right" aria-hidden="true"></i></span>Coding gaps
                        </li>
                        <li><span class="fa-li"><i class="fas fa-arrow-alt-circle-right" aria-hidden="true"></i></span>Provider attribution
                        </li>
                    </ul>
                </div>

                <div class="container__one-third shadow-box">
                    <p><strong>Advanced Analytics
                        </strong></p>
                    <ul class="fa-ul">
                        <li><span class="fa-li"><i class="fas fa-arrow-alt-circle-right" aria-hidden="true"></i></span>Risk stratification
                        </li>
                        <li><span class="fa-li"><i class="fas fa-arrow-alt-circle-right" aria-hidden="true"></i></span>Utilization of resources
                        </li>
                        <li><span class="fa-li"><i class="fas fa-arrow-alt-circle-right" aria-hidden="true"></i></span>Costs
                        </li>
                        <li><span class="fa-li"><i class="fas fa-arrow-alt-circle-right" aria-hidden="true"></i></span>Impact on achieving quality outcomes
                        </li>
                    </ul>
                </div>

                <div class="container__one-third shadow-box">
                    <p><strong>Results
                        </strong></p>
                    <p>
                        More effective management of quality care for patient populations
                        &mdash; the healthy, the chronically ill, and everyone in-between
                    </p>
                </div>

            </div>

        </div>

    </div>
-->
    <div class="container bg--green-gradient">
        <div class="container__centered">
            <div class="container__one-half"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Collage-image.png" alt="Collage of diverse people"></div>
            <div class="container__one-half">
                <h2>Manage your patient populations with Expanse tools</h2>
                <p>Clinicians benefit from a more holistic view of patient information with insights from multiple sources embedded throughout Expanse as part of their natural workflow.</p>
                <p>MEDITECH’s information-rich Population Health Management platform allows you to harnesses the comprehensive data set within Expanse Population Insight, the powerful analytics of <a href="<?php print $url; ?>/ehr-solutions/meditech-business-clinical-analytics">Business and Clinical Analytics</a>, and the centralized care management tools within <a href="<?php print $url; ?>/ehr-solutions/care-compass">Expanse Care Compass</a>, resulting in more effective management of your patient populations — who they are, where they've been, and where they're going.</p>
            </div>
        </div>
    </div>

    <!-- End Block 6 -->


    <!-- Start Block 7 -->
    <div class="container">
        <div class="container__centered center">

            <div class="auto-margins" style="max-width: 45em;">
                <h2>Empower the most important member of the care team
                    &mdash; the patient.</h2>
                <p style="margin-bottom: 2em;">No one plays a more crucial role in their health and well-being than patients themselves. <a href="https://ehr.meditech.com/ehr-solutions/patient-engagement">Keep them engaged</a> with MEDITECH's Patient and Consumer Health Portal, where they can view and trend their data from personal health devices to pursue individual wellness goals. Patients can also book appointments, update their information, and communicate securely with providers.</p>
            </div>

            <h3 style="padding-bottom: 1.5em;">Patient engagement solutions put patients at the center of their care.</h3>

            <div class="card__wrapper">

                <div class="container__one-third card">
                    <figure>
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/woman-taking-child-temp-over-virtual-visit.jpg" alt="woman taking child's temp over virtual visit">
                    </figure>
                    <div class="card__info">
                        <h4><a href="https://ehr.meditech.com/news/meditech-furthers-commitment-to-customers-virtual-care">Virtual Visits</a></h4>
                        <p>Offer patients a convenient, remote interaction with their providers &mdash; for both scheduled and <a href="https://home.meditech.com/en/d/mktcontent/otherfiles/virtualondemandcareinfographicweb.pdf" target="_blank">urgent care</a></p>
                    </div>
                </div>

                <div class="container__one-third card">
                    <figure>
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/atheltic-person-using-MHealth-app.jpg" alt="Talking with a patient about care gaps with ppe mask">
                    </figure>
                    <div class="card__info">
                        <h4><a href="https://ehr.meditech.com/news/mhealth-meditech-in-the-app-store">MHealth App</a></h4>
                        <p>Available through the Apple Store and Google Play, provides patients with secure, convenient access to their health information via their mobile device or tablet</p>
                    </div>
                </div>

                <div class="container__one-third card">
                    <figure>
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/man-using-iphone-at-desk.jpg" alt="Man using iphone at desk">
                    </figure>
                    <div class="card__info">
                        <h4><a href="https://ehr.meditech.com/news/health-records-on-iphone-available-on-all-meditech-platforms">Health Records on iPhone®</a></h4>
                        <p>Users can also access their own health data with Health Records on iPhone®, which includes all of their information in one easy-to-use interface.</p>
                    </div>
                </div>

            </div>

        </div>
    </div>
    <!-- End Block 7 -->


</div>
<!-- end js__seo-tool__body-content -->

<!-- Block 8 -->
<div class="container bg--green-gradient">
    <div class="container__centered center">

        <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
        <h2>
            <?php print $cta->field_header_1['und'][0]['value']; ?>
        </h2>
        <?php } ?>

        <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
        <div>
            <?php print $cta->field_long_text_1['und'][0]['value']; ?>
        </div>
        <?php } ?>

        <div class="center" style="margin-top:2em;">
            <?php hubspot_button($cta_code, "Sign Up For The Care Across The Continuum Webinar"); ?>
        </div>

        <div style="margin-top:1em;">
            <?php print $share_link_buttons; ?>
        </div>

    </div>
</div>
<!-- End Block 8 -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>

<!-- END campaign--node-3518.php -->
