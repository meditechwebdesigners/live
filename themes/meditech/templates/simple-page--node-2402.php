<!-- start simple-page--node-2402.tpl.php template -- COOKIE POLICY PAGE -->

  <section class="container__centered">
    <div class="container__two-thirds">
      <h1 class="page__title js__seo-tool__title"><?php print $title; ?></h1>
      
      <div class="container center" style="background-color:#e6e9ee; padding:2em 1em; margin-bottom:1em;">
        <div class="container__one-half">
          <a href="#" id="doNotTrack" class="btn--orange" style="margin-bottom:1em;">Stop Tracking Me</a>
          <p class="text--small" style="margin:0; line-height:1.25em;">Clicking this button will create a cookie in your browser that will tell Hubspot NOT to track your activity on this website.</p>
        </div>
        <div class="container__one-half">
          <a href="#" id="removeCookies" class="btn--orange" style="margin-bottom:1em;">Delete Hubspot Cookies</a>
          <p class="text--small" style="margin:0; line-height:1.25em;">Clicking this button will erase all Hubspot cookies from your browser. This action will cause the tracking opt-in/opt-out question to reappear at the top of these pages.</p>
        </div>
      </div>
      
      <div class="js__seo-tool__body-content">
        <?php print render($content['field_body']); ?>
      </div>
    </div>
  </section>
          
  <!-- Start of HubSpot Embed Code -->
  <script type='text/javascript'>
    document.getElementById('doNotTrack').onclick = function(){
      _hsq.push(['doNotTrack']);
    };
    document.getElementById('removeCookies').onclick = function(){
      _hsq.push(['revokeCookieConsent']);
    };
  </script>
  <!-- End of HubSpot Embed Code -->

<!-- end simple-page--node-2402.tpl.php template -->