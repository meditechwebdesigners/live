<!-- START campaign--node-3418.php -->

<?php // Coronavirus 2021

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');
$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>


<style>
    .bg-pattern--container {
        margin-top: 4em;
    }

    .bg--black-coconut-gradient {
        background: #3E4545;
        background: -webkit-linear-gradient(#4c5252, #181919);
        background: -o-linear-gradient(#4c5252, #181919);
        background: -moz-linear-gradient(#4c5252, #181919);
        background-image: linear-gradient(-150deg, #4c5252, #181919);
        color: #fff;
    }

    .card__info {
        padding: 1em 2em;
    }

    @media all and (max-width: 50em) {
        .bg-pattern--container {
            margin-top: inherit;
        }
    }

</style>

<h1 style="display:none;" class="js__seo-tool__title">MEDITECH COVID-19 Workflows</h1>

<div class="js__seo-tool__body-content">

    <!-- Block 1 -->
    <div class="container">
        <div class="container__centered">
            <div class="container__one-half">
                <h2 class="header-one">Guiding Customers through the COVID-19 Pandemic.</h2>
                <p>As healthcare organizations are racing to vaccinate staff and communities, they’re also confronting highly contagious new strains of COVID-19 that threaten to drive future surges. And yet, they continue to embrace innovative strategies to meet this historic challenge, inspiring all of us with their selflessness and determination.</p>
                <p>Arming customers with robust solutions for combatting the virus has been our priority throughout the pandemic. From virtual care and patient monitoring to contact tracing and rapid vaccine administration, here are some of the ways MEDITECH can support you.</p>
                <div style="margin-top:1em;">
                    <?php hubspot_button($cta_code, "Watch The COVID-19 Vaccine Administration Recorded Webinar"); ?>
                </div>
            </div>
            <div class="container__one-half">
                <div class="bg-pattern--container">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/relieved-nurse-removing-mask-outside.jpg" alt="Relieved nurse removes mask outside and takes a deep breath">
                    <div class="bg-pattern--blue-dots bg-pattern--right"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Block 1 -->

    <!-- Block 2 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-gradient--bg.jpg);">
        <div class="container__centered auto-margins">
            <div class="center text--white" style="margin-bottom:2em;">
                <h2>Accelerate the vaccine administration process.</h2>
            </div>
        </div>
        <div class="container no-pad">
            <div class="container__centered">
                <div class="gl-container" style="padding-bottom:2em;background-color: inherit;">
                    <div class="container__one-half bg--white">
                        <div class="center">
                            <img style="max-height: 115px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--quick.svg" alt="quick vaccine icon">
                            <h3>Quick Vaccination</h3>
                        </div>
                        <p>Reduce the burden of mass distribution with MEDITECH’s Quick Vaccination, a flexible, web-based solution that streamlines workflow. Integrated with the <a href="https://home.meditech.com/en/d/customerservicearticles/otherfiles/covid19vaccineupdate.pdf">MEDITECH Immunization Interface</a>, Quick Vaccination automatically sends vaccine data to your state's Immunization Information System.</p>
                    </div>
                    <div class="container__one-half bg--black-coconut-gradient">
                        <div class="center">
                            <img style="max-height: 115px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--three-star-certificate--white.svg" alt="three star certificate icon">
                            <h3>Vaccine certificates</h3>
                        </div>
                        <p>Quick Vaccination automatically generates certificates, a CDC requirement for patients to receive the second dose and validate vaccination. Accessible from the Patient and Consumer Health Portal, certificates include:</p>
                        <ul>
                            <li>Vaccine manufacturer.</li>
                            <li>Date of vaccination.</li>
                            <li>Care setting in which it was administered.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- End of Block 2 -->

    <!-- Block 3 -->
    <div class="container no-pad">
        <div class="gl-container">
            <div class="container__one-half bg--black-coconut-gradient gl-text-pad">
                <h2>Track vaccine deployment.</h2>
                <p>Meet the unique and shifting requirements of federal and state governments with patient registries that support the entire vaccine administration process.</p>
                <ul>
                    <li>Track employees and front-line workers</li>
                    <li>Identify eligible recipients by location, such as long-term care facilities</li>
                    <li>Monitor lists of patients based on age, occupation, comorbidities, and risk factors</li>
                    <li>Catch eligible patients not yet vaccinated and follow up through the patient portal</li>
                    <li>Identify high-risk patients to ensure they’re prioritized for vaccination, and verify they experienced no side effects after administration</li>
                    <li>Follow up with patients who have not yet completed their course of treatment or scheduled their second dose</li>
                </ul>
            </div>
            <div class="container__one-half background--cover flex-order--reverse" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/young-asian-woman-patient-getting-prepped-for-vaccine.jpg); min-height:28.125em;"></div>
        </div>
    </div>
    <!-- End of Block 3 -->


    <!-- Block 4 -->
    <!-- Start hidden modal box -->
    <div id="modal10" class="modal">
        <a class="close-modal" href="javascript:void(0)">&times;</a>
        <div class="modal-content">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/screenshot--Emanate-BCA-COVID19.png" alt="Screenshot of Emanate's BCA Covid-19 Dashboard">
        </div>
    </div>
    <!-- End hidden modal box -->
    <div class="container bg--white">
        <div class="container__centered">
            <div class="center auto-margins" style="margin-bottom:1.5em;">
                <h2>Trace COVID-19 exposure stemming from an inpatient stay.</h2>
                <p>Emanate Health enlisted MEDITECH’s Professional Services team for support in <a href="https://ehr.meditech.com/news/emanate-health-contact-tracing-dashboard-helps-track-covid-19-patients-to-prevent-staff">developing a contact tracing tool</a> that would identify staff and patients at high risk for COVID-19 exposure.</p>
            </div>
            <div class="container__one-third">
                <blockquote style="margin:1em 0 0 0;">
                    <p class="italic">“Emanate Health implemented plans to safely identify staff who have come into contact with a patient who has tested positive for the virus. The data helps clinical operations know where transmissions are happening within our three hospitals. This <a href="https://ehr.meditech.com/news/emanate-health-contact-tracing-dashboard-helps-track-covid-19-patients-to-prevent-staff">unique contract tracing program</a> was specifically developed for our EHR and how we work here at Emanate Health, helping us achieve some promising outcomes.”
                    </p>
                    <p>
                        <strong>Loucine Kasparian</strong>
                        <br>Infectious Disease Director<br>Emanate Health
                    </p>
                </blockquote>
            </div>
            <div class="container__two-thirds center">
                <div class="open-modal" data-target="modal10" style="margin:0.25em 0 2em 0;">
                    <div class="tablet--black">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/screenshot--Emanate-BCA-COVID19.png" alt="Screenshot of Emanate's BCA Covid-19 Dashboard">
                    </div>
                    <div class="mag-bg">
                        <i class="mag-icon fas fa-search-plus"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Close Block 4 -->


    <!-- Block 5 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-gradient--bg.jpg);">
        <div class="container__centered auto-margins">
            <div class="center text--white" style="margin-bottom:2em;">
                <h2>Monitor your patients’ COVID-19 status.</h2>
                <p>Identify and monitor inpatients’ COVID-19 status using Surveillance, MEDITECH’s predictive analytics solution. Surveillance generates electronic alerts to status boards and trackers, all of which reflect patients’ current status. These notifications help keep staff, and other patients, safer.</p>
            </div>
            <div class="gl-container" style="padding-bottom:2em;background-color: inherit;">
                <div class="container__one-half bg--white">
                    <div class="center">
                        <img style="max-height: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--case-study.svg" alt="case study icon">
                        <h3>Read the success story</h3>
                    </div>
                    <p>Find out how The Valley Hospital (Ridgewood, NJ) built <a href="https://f.hubspotusercontent10.net/hubfs/2897117/Case_Studies/Customer_Success_The_Valley_Hospital_Surveillance_COVID.pdf">three Surveillance profiles</a> to manage and monitor COVID-19.</p>
                </div>
                <div class="container__one-half bg--black-coconut-gradient text--white">
                    <div class="center">
                        <img style="max-height: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--webinar--white.svg" alt="webinar icon">
                        <h3>Watch the on-demand webinar</h3>
                    </div>
                    <p>This <a href="https://info.meditech.com/webinar-covid-19-surveillance">demonstration of Surveillance workflow</a> guidance is followed by a discussion on surveying patients with suspected COVID-19.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Block 5 -->


    <!-- Start Block 6 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/bg--blue-layered-shapes-2.svg);">
        <div class="container__centered">

            <div class="center auto-margins" style="margin-bottom:2em;">
                <h2>Use the power of analytics to stop the spread.</h2>
                <p>Leveraging the flexibility of MEDITECH’s Business and Clinical Analytics solution, healthcare organizations are able to gain the insight needed to make informed decisions and report COVID-19 data to governing bodies.</p>
            </div>

            <div class="card__wrapper">

                <div class="container__one-third card">
                    <figure>
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/hospital-image--Newton-Medical-Center-exterior.jpg" alt="Newton Medical Center Exterior">
                    </figure>
                    <div class="card__info">
                        <h3><strong>NMC Health</strong></h3>
                        <p>Worked with MEDITECH’s Professional Services to <a href="https://f.hubspotusercontent10.net/hubfs/2897117/Case_Studies/CustomerSuccess-NMC-COVID19.pdf">build COVID-19 dashboards</a> that:</p>
                        <ul>
                            <li>
                                Display real-time snapshots of inpatients, locations, testing status, and ventilator usage.
                            </li>
                            <li>
                                Leverage integration with Supply Chain to monitor high-demand items, such as PPE.
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="container__one-third card">
                    <figure>
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/hospital-image--Firelands-exterior.jpg" alt="Firelands Regional Medical Center Exterior">
                    </figure>
                    <div class="card__info">
                        <h3><strong>Firelands Regional Medical Center</strong></h3>
                        <p>Created <a href="https://f.hubspotusercontent10.net/hubfs/2897117/Case_Studies/Customer_Success_Firelands_COVID19.pdf">COVID-19 dashboards</a> to:</p>
                        <ul>
                            <li>
                                Provide hourly updates to the Command Center.
                            </li>
                            <li>
                                Monitor surge capacity, clusters, and bed occupancy.
                            </li>
                            <li>
                                Compare labs for fastest turnaround times.
                            </li>
                            <li>
                                Track mortalities and discharges.
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="container__one-third card">
                    <figure>
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/hospital-image--Calvert-Health-Medical-Center-exterior.jpg" alt="Calvert Health Medical Center Exterior">
                    </figure>
                    <div class="card__info">
                        <h3><strong>CalvertHeath</strong></h3>
                        <p>Built two dashboards — each in under 24 hours — to <a href="https://ehr.meditech.com/news/calverthealth-monitors-coronavirus-cases-using-meditechs-bca-solution">meet state daily reporting requirements</a>, including:</p>
                        <ul>
                            <li>
                                A COVID-19 reporting dashboard to track admissions, ICU patients, ventilator usage, discharges, and mortalities.
                            </li>
                            <li>
                                An Occupancy dashboard to track bed availability in acute, pediatric, and ICU settings.
                            </li>
                        </ul>
                    </div>
                </div>

            </div>

        </div>
    </div>
    <!-- End of Block 6 -->


    <!-- Block 7 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-gradient--bg.jpg);">
        <div class="container__centered">
            <div class="center text--white auto-margins" style="margin-bottom:2em;">
                <h2>Keep patients safer with Virtual Care.</h2>
                <p>Patients fearful of exposure to COVID-19 often delay care, which can have dire consequences. <a href="https://ehr.meditech.com/ehr-solutions/virtual-care">MEDITECH’s Virtual Care solution</a> provides them with access to physicians from the comfort of home, through both scheduled appointments and on-demand, urgent care visits.</p>
            </div>
            <div class="gl-container" style="padding-bottom:2em;background-color: inherit;">
                <div class="container__one-third bg--white">
                    <div class="center">
                        <img style="max-height: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/logo--Markham-Stouffville.svg" alt="Markham Stouffville logo">
                    </div>
                    <p>
                        <strong>Markham Stouffville</strong> was one of the first hospitals in Canada to leverage virtual care, significantly boosting patient portal enrollment.
                    </p>
                    <p><a href="https://blog.meditech.com/advancing-digital-health-solutions-beyond-the-pandemic">Read the blog</a>
                    </p>
                </div>
                <div class="container__one-third bg--black-coconut-gradient text--white">

                    <div class="center">
                        <img style="max-height: 100px; border-radius: 10px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/logo--Citizens-Memorial-Hospital.jpg" alt="Citizens Memorial logo">
                    </div>
                    <p>
                        <strong>Citizens Memorial</strong> turned to virtual visits early in the pandemic to pre-screen patients for COVID-19 and support a wide range of appointments.
                    </p>
                    <p><a href="https://blog.meditech.com/how-virtual-visits-are-keeping-providers-and-patients-safe-at-citizens-memorial-healthcare">Read the blog</a>
                    </p>
                </div>
                <div class="container__one-third bg--white">

                    <div class="center">
                        <img style="max-height: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/logo--Avera-Health.jpg" alt="Avera Health logo">
                    </div>
                    <p>
                        <strong>Avera Health</strong> expanded their comprehensive Avera@Home virtual care program to support COVID-19 volumes.
                    </p>
                    <p><a href="https://blog.meditech.com/virtual-care-in-the-age-of-covid-19-and-beyond">Listen to the podcast</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Block 7 -->


    <!-- Block 8 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/bg--blue-layered-shapes.svg);">
        <div class="container__centered center">

            <div style="margin-bottom:2em;">
                <h2>Minimize contact when hands-on care is necessary.</h2>
                <p>When hands-on care is the only option, here’s how MEDITECH helps your organization limit patients’ exposure:</p>
            </div>
            <div class="container__one-third shadow-box">
                <img style="height: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--mobile-check-in.svg" alt="mobile check-in icon">
                <h3><strong>Contactless Check-In</strong></h3>
                <p>
                    Reduce wait times with a <a href="https://ehr.meditech.com/news/patient-intake-made-easy-automated-paperless">pre-arrival solution</a> that allows patients to complete forms using their own devices.
                </p>
            </div>
            <div class="container__one-third shadow-box">
                <img style="height: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--mobile-questionnaires.svg" alt="mobile questionnaires icon">
                <h3><strong>Questionnaires</strong></h3>
                <p>
                    Enable patients to submit questionnaires online prior to appointments for faster processing and intake.
                </p>
            </div>
            <div class="container__one-third shadow-box">
                <img style="height: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--remote-monitoring.svg" alt="remote monitoring icon">
                <h3><strong>Remote Monitoring</strong></h3>
                <p>View and trend vital signs collected from home devices to help keep patients’ chronic conditions under control.</p>
            </div>

        </div>
    </div>
    <!-- End of Block 8 -->


    <!-- Start Block 9 -->
    <div class="container bg--white">
        <div class="container__centered">

            <div class="center auto-margins" style="margin-bottom: 2em;">
                <h2>Acknowledging everyday heroes.</h2>
                <p>The COVID-19 pandemic brought to light countless acts of heroism. Here’s a glimpse of everyday heroes across MEDITECH’s community:</p>
            </div>

            <div class="card__wrapper">

                <div class="container__one-half card">
                    <figure>
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/card-image--dr-lee-headshot.jpg" alt="Headshot of Dr. Terrance Lee">
                    </figure>
                    <div class="card__info">
                        <h3><strong>Terrance Lee, MD, MPH, Beth Israel Deaconess community hospitals</strong></h3>
                        <p>After preparing his own organization with a surge plan, Dr. Lee volunteered to serve on the frontlines in New York City — where he contracted the virus. While quarantining at home, he worked with a BID programming team to develop dashboards and reports, assisted with launching telehealth across three hospitals, and documented his experiences on Twitter.</p>
                        <p><a href="https://blog.meditech.com/reflections-from-the-frontlines-combating-covid19-in-boston-and-nyc-0">Read the blog</a></p>
                    </div>
                </div>

                <div class="container__one-half card">
                    <figure>
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/card-image--phoebe-putney-health-system.jpg" alt="Phoebe Putney Health System Exterior">
                    </figure>
                    <div class="card__info">
                        <h3><strong>Phoebe Putney Health System</strong></h3>
                        <p>Southwest Georgia was the unlikely location of the fourth-hottest hotspot in the world last April. But Phoebe Putney Memorial Hospital’s disaster preparedness helped them to marshal an extraordinary operational and technical counterattack, juggling competing priorities amid concerns for employee well-being and the health of its close-knit community.</p>
                        <p><a href="https://blog.meditech.com/anticipate-the-unimaginable-5-steps-to-mitigate-crises">Read the blog</a></p>
                    </div>
                </div>

                <div class="container__one-half card">
                    <figure>
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/card-image--lawrence-general-hospital.jpg" alt="Lawrence General Hospital Exterior">
                    </figure>
                    <div class="card__info">
                        <h3><strong>Lawrence General Hospital</strong></h3>
                        <p>Located in one of the hardest hit cities in Massachusetts, Lawrence General implemented drive-thru testing that now tests over 1,400 patients per day and provides a model for other organizations to follow. Through strong partnerships with the governor and mayor, they were identified as a high-volume testing site in their Stop the Spread initiative — a testing program that offers free, easily accessible testing to state residents. Despite a surge in testing, their frontline healthcare workers continue to step up their efforts to meet the increased demand.</p>
                        <p><a href="https://blog.meditech.com/managing-a-crisis-with-covid-19-drive-thru-testing-at-lawrence-general-hospital">Read the blog</a></p>
                    </div>
                </div>

                <div class="container__one-half card">
                    <figure>
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/card-image--cayuga-health.jpg" alt="Cayuga Health Exterior">
                    </figure>
                    <div class="card__info">
                        <h3><strong>Cayuga Health</strong></h3>
                        <p>Staff at Cayuga Medical Center and Schuyler Hospital rose to the challenge, both in Ithaca and across the state. First, they deployed their mobile team to Seneca View, Schuyler Hospital’s 120-bed skilled nursing facility— as well as a local assisted living facility in Ithaca— to proactively test high-risk residents for COVID-19. Then, 50 clinicians volunteered for the month-long New York City medical mission at the epicenter of the pandemic.</p>
                        <p><a href="https://ehr.meditech.com/news/cayuga-health-systems-covid-19-response-is-new-york-strong">Read their story</a></p>
                    </div>
                </div>

            </div>

        </div>
    </div>
    <!-- End Block 9 -->

</div><!-- end js__seo-tool__body-content -->


<!-- Block 9 CTA -->
<div class="container background--cover text--white" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-gradient--bg.jpg);">
    <div class="container__centered center">

        <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
        <h2>
            <?php print $cta->field_header_1['und'][0]['value']; ?>
        </h2>
        <?php } ?>

        <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
        <h3>
            <?php print $cta->field_long_text_1['und'][0]['value']; ?>
        </h3>
        <?php } ?>

        <div class="center" style="margin-top:2em;">
            <?php hubspot_button($cta_code, "Sign Up For The Expanse Patient Care Webinar"); ?>
        </div>

        <div style="margin-top:1em;">
            <?php print $share_link_buttons; ?>
        </div>

    </div>
</div>
<!-- End of Block 9 -->


<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>

<!-- End of Campaign Node 3418 -->
