<?php
  /*
  *
  * This template is set up to control the display of the 'hotel' content type 
  *
  */
  $url = $GLOBALS['base_url']; // grabs the site url

?>
<!-- start node--hotel.tpl.php template -->
    

  <section class="container__centered">
  
	<div class="container__two-thirds">

      <h1 class="page__title js__seo-tool__title"><?php print $title; ?></h1>
      
      <div class="js__seo-tool__body-content">

        <?php // INTRO =================================================================================
          if( !empty($content['field_intro_text']) ){
            print '<div>';
            print render($content['field_intro_text']);
            print '</div>';
          }
        ?>

        <address class="container no-pad">

          <?php // ADDRESS =============================================================================
            $stateTerms = field_view_field('node', $node, 'field_location_state'); 
            if(!empty($stateTerms)){
              foreach($stateTerms["#items"] as $sTerm){
                $locationState = $sTerm["taxonomy_term"]->description;
              }
            }
            else{
              $locationState = '';
            }        
          ?>

          <div class="container__one-fourth">
            <h3>Address</h3>
            <p><?php print render($content['field_location_address']); ?><br>
            <?php print render($content['field_location_city']); ?>, <?php print $locationState; ?> <?php print render($content['field_location_zip']); ?>
            <?php
            if( !empty($content['field_directions_url']) ){
              print '<br><a class="directions_gae" href="';
              print render($content['field_directions_url']);
              print '" target="_blank">Directions</a>';
            }
            ?></p>
          </div>

          <?php // PHONE =============================================================================== ?>
          <div class="container__one-fourth">
            <h3>Call</h3>
            <p><a class="phone_gae" href="tel:<?php print render($content['field_phone_number']); ?>"><?php print render($content['field_phone_number']); ?></a></p>
          </div>

          <?php // FAX ================================================================================== ?>
          <div class="container__one-fourth">
            <h3>Fax</h3>
            <p><?php print render($content['field_fax_number']); ?></p>
          </div>

          <?php // WEB ================================================================================== ?>
          <div class="container__one-fourth">
            <h3>Website</h3>
            <p><a class="website_gae" href="<?php print render($content['field_website_url']); ?>" target="_blank"><?php print $title; ?></a></p>
          </div>

        </address>


        <?php 
        // RESERVATIONS ================================================================================ 
        if( !empty($content['field_reservations']) ){
          print '<div>';
          print '<h3>Reservations</h3>';
          print render($content['field_reservations']);
          print '</div>';
        }


        // RATES ======================================================================================== 
        if( !empty($content['field_rates']) ){
          print '<div>';
          print '<h3>Rates</h3>';
          print render($content['field_rates']);
          print '</div>';
        }


        // AMENITIES =================================================================================== 
        if( !empty($content['field_amenities']) ){
          print '<div>';
          print '<h3>Amenities</h3>';
          print render($content['field_amenities']);
          print '</div>';
        }


        // SERVICES ==================================================================================== 
        if( !empty($content['field_services']) ){
          print '<div>';
          print '<h3>Services</h3>';
          print render($content['field_services']);
          print '</div>';
        }


        // DINING ======================================================================================= 
        if( !empty($content['field_dining']) ){
          print '<div>';
          print '<h3>Dining</h3>';
          print render($content['field_dining']);
          print '</div>';
        }    


        // RECREATION =================================================================================== 
        if( !empty($content['field_recreation']) ){
          print '<div>';
          print '<h3>Recreation</h3>';
          print render($content['field_recreation']);
          print '</div>';
        }  


        // SHUTTLE SERVICE =============================================================================== 
        if( !empty($content['field_shuttle_service']) ){
          print '<div>';
          print '<h3>Shuttle Service</h3>';
          print render($content['field_shuttle_service']);
          print '</div>';
        } 
        ?>  
      
      </div><!-- End .js__seo-tool__body-content -->
      
      <?php // SEO tool for internal use...
        if(node_access('update',$node)){
          print '<!-- SEO Tool is added to this div -->';
          print '<div class="container js__seo-tool"></div>';
        } 
      ?> 
   
    </div><!-- END container__two-thirds -->

   
    <?php // add ABOUT SIDE NAV =========================================================================== ?>
    <aside class="container__one-third panel">
      <div class="sidebar__nav">
        <?php
          $massnBlock = module_invoke('menu', 'block_view', 'menu-about-section-side-nav');
          print render($massnBlock['content']); 
        ?>
      </div>
    </aside>
  
  </section>
  
  
<!-- end node--hotel.tpl.php template -->