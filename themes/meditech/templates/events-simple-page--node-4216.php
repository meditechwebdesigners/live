<!-- START events-simple-page--node-4216.php --- Expanse Summer Showcase -->
<?php // This template is set up to control the display of the Expanse Summer Showcase Event

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');
$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>


<style>
    .btn--outline {
        background-color: transparent;
        color: #3E4545 !important;
        border-width: 4px !important;
        -webkit-transition: all 400ms ease-in-out;
    }

    .btn--orange:hover {
        -webkit-transition: all 400ms ease-in-out;

    }

    .track-one {
        background-color: #FFF0E0;
        padding: .5em 1em;
        border: 1px solid #333;
        border-radius: 7px;
    }

    .track-two {
        background-color: #FFDDDD;
        padding: .5em 1em;
        border: 1px solid #333;
        border-radius: 7px;
    }

    .track-three {
        background-color: #B3FFE0;
        padding: .5em 1em;
        border: 1px solid #333;
        border-radius: 7px;
    }

    .track-four {
        background-color: #D2F6FF;
        padding: .5em 1em;
        border: 1px solid #333;
        border-radius: 7px;
    }

    li.track-one,
    li.track-two,
    li.track-three,
    li.track-four {
        list-style: none;
    }

    .track-one.card,
    .track-two.card,
    .track-three.card,
    .track-four.card {
        padding: 2em;
        margin-bottom: 4em;
    }

    .new-margins {
        width: 60%;
        margin: 1em 20% 0
    }

    @media all and (max-width: 600px) {
        .new-margins {
            width: 80%;
            margin: 1em 10% 0
        }
    }

    #recordings a,
    #recordings a:link,
    #recordings a:visited,
    #recordings a:hover {
        color: #3E4545;
        display: block;
    }

    #recordings a.track-one,
    #recordings a.track-two,
    #recordings a.track-three,
    #recordings a.track-four {
        display: inline-block;
        margin-right: 1em;
        margin-bottom: 1em;
    }

    .card hr {
        border-bottom: 1px solid #3E4545;
    }

    #recordings .card__info {
        padding: 0 1em;
    }

    #recordings .card__info p {
        line-height: 1.25em;
    }

    .back {
        text-align: right;
    }

    .exec {
        border-radius: 7px;
    }

</style>

<div class="js__seo-tool__body-content" style="background: rgb(169,225,255); background: linear-gradient(0deg, rgba(255,255,255,1) 0%, RGB(197, 245, 255) 100%);">

    <!-- Start of Block 1 (Event Info) -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/events/Expanse-summer-showcase-event--umbrellas-top.png);">
        <div class="container__centered" style="padding-top:4em;">

            <div class="container__three-fourths">

                <h1 class="js__seo-tool__title" style="font-size: 3em;">Expanse Summer Showcase</h1>
                <h2>August 10th and 11th</h2>

                <div class="btn-holder--content__callout" style="margin-top:2em; text-align: left;">
                    <a href="#recordings" class="btn--orange btn--outline">View The On Demand Recordings</a>
                </div>
                <div style="margin-top:1em; margin-bottom:2em;">
                    <?php print $share_link_buttons; ?>
                </div>

                <p>The Expanse Summer Showcase is your best opportunity to learn about MEDITECH's extensive EHR offerings, get your questions answered by staff and customers, and meet one-on-one with a member of our executive team. This two-day online event will provide a snapshot of what a MEDITECH partnership could mean for your organization.</p>

                <p>Our Expanse EHR is easing physicians' cognitive burdens, giving nurses more agility, and alleviating the strains on caregivers and informaticists. In other words, making care better, safer, and people-centered. Come find out what big things are possible, no matter the size or specialty of your organization.</p>

            </div>

        </div>
    </div>
    <!-- End of Block 1 (Event Info) -->



   
    <!-- Start Block 4 (recordings) -->
    <div class="container container__centered" id="recordings">

        <h2 class="center">On Demand Recordings</h2>

        <div class="container container__centered">
            <div class="video-container auto-margins">
                <div class="video js__video" data-video-id="587887978">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/video-overlay--ESS--Opening-Panel.jpg" alt="Video covershot - Opening Panel">
                    </figure>
                    <a class="video__play-btn" href="https://player.vimeo.com/video/587887978"></a>
                    <div class="video__container"></div>
                </div>
            </div>
            <div style="margin-top:1em;">
                <div class="container__one-half">
                    <h3>Opening Panel</h3>
                    <p>Prioritizing your patients' care builds loyalty, trust, and satisfaction. And that equals better cash flow for your healthcare organization. But finding the balance between patient, payer, and staff demands is challenging. MEDITECH's Revenue Cycle can strike the balance you need to unlock your organization's maximum reimbursement potential. With a healthy and thriving Revenue Cycle, you can attract and retain patients by investing in technology, upgrading equipment, and hiring the staff to keep it all humming along. That's the bottom line.</p>
                </div>
                <div class="container__one-half">
                    <h2>Additional Resources</h2>
                    <ul>
                        <li><a href="https://ehr.meditech.com/ehr-solutions/meditechs-revenue-cycle">MEDITECH's Revenue Cycle</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="container container__centered">
            <div class="video-container auto-margins">
                <div class="video js__video" data-video-id="587888587">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/video-overlay--ESS--LL-Patient-Engagement.jpg" alt="Video covershot - Patient Engagement">
                    </figure>
                    <a class="video__play-btn" href="https://player.vimeo.com/video/587888587"></a>
                    <div class="video__container"></div>
                </div>
            </div>
            <div style="margin-top:1em;">
                <div class="container__one-half">
                    <h3>Lunch & Learn: Patient Engagement</h3>
                    <p>Consumers want to access care when and where it's convenient for them, using technology that they already know and like. Patients want attentive care, with clinicians that are in the know. With MEDITECH Expanse Patient Engagement solutions, you can deliver both.</p>
                </div>
                <div class="container__one-half">
                    <h2>Additional Resources</h2>
                    <ul>
                        <li><a href="https://f.hubspotusercontent10.net/hubfs/2897117/patientengagementbookletsinglepage.pdf?__hstc=188859636.be74c2991f5ff55a33d25f26dfb96452.1626187137358.1629294582414.1629303543976.89&__hssc=188859636.14.1629303543976&__hsfp=814967395&hsCtaTracking=08dfff58-199f-41c2-98e7-5770013efb65%7Cef01dea8-9be7-4403-a5e5-ae1957303c47">Service Excellence for the 21st Century Empowered Patient</a></li>
                        <li><a href="https://ehr.meditech.com/ehr-solutions/patient-engagement">MEDITECH's Patient Engagement</a></li>
                    </ul>
                </div>
            </div>
        </div>


        <div class="container container__centered">
            <div class="video-container auto-margins">
                <div class="video js__video" data-video-id="588529626">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/video-overlay--ESS--Professional-Services.jpg" alt="Video covershot - Professional Services">
                    </figure>
                    <a class="video__play-btn" href="https://player.vimeo.com/video/588529626"></a>
                    <div class="video__container"></div>
                </div>
            </div>
            <div style="margin-top:1em;">
                <div class="container__one-half">
                    <h3>MEDITECH's Professional Services</h3>
                    <p>Get predictable, positive outcomes from your Expanse EHR. Learn how your EHR can reach its fullest potential with MEDITECH's Professional Services. We offer a more personalized approach to your Expanse implementation, to maximize your ROI.</p>
                </div>
                <div class="container__one-half">
                    <h2>Additional Resources</h2>
                    <ul>
                        <li><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/professionalservicesmenu.pdf">Professional Services: List of Services</a></li>
                        <li><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/professionalservicesbcadashboards.pdf">Specialty Services: Business and Clinical Analytics</a></li>
                        <li><a href="https://info.meditech.com/en/emanate-health-advances-covid-19-contact-tracing-with-meditech-professional-services">Emanate Health Advances COVID-19 Contact Tracing with MEDITECH Professional Services</a></li>
                    </ul>
                </div>
            </div>
        </div>


        <div class="container container__centered">
            <div class="video-container auto-margins">
                <div class="video js__video" data-video-id="587925404">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/video-overlay--ESS--MaaS.jpg" alt="Video covershot - MaaS">
                    </figure>
                    <a class="video__play-btn" href="https://player.vimeo.com/video/587925404"></a>
                    <div class="video__container"></div>
                </div>
            </div>
            <div style="margin-top:1em;">
                <div class="container__one-half">
                    <h3>MEDITECH as a Service (MaaS)</h3>
                    <p>You shouldn't have to choose between cost and quality when selecting an EHR. MEDITECH as a Service (MaaS) is a cost-effective and scalable EHR solution for organizations of any size or specialty. MaaS is MEDITECH's cloud-hosted option for data storage, transfer, and recovery, and leverages the latest security measures to keep patient records safe.</p>
                </div>
                <div style="margin-top:1em;">
                    <div class="container__one-half">
                        <h2>Additional Resources</h2>
                        <ul>
                            <li><a href="https://f.hubspotusercontent10.net/hubfs/2897117/ebook_MaaS.pdf?__hstc=188859636.be74c2991f5ff55a33d25f26dfb96452.1626187137358.1629294582414.1629303543976.89&__hssc=188859636.5.1629303543976&__hsfp=814967395&hsCtaTracking=b6715975-a300-4d6a-acb9-395439dc8be7%7Cf5ebc3f5-4ac5-49be-b185-f58ae7762f59">Understanding the Value Proposition for MEDITECH as a Service (MaaS)</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="container container__centered">
            <div class="video-container auto-margins">
                <div class="video js__video" data-video-id="587923642">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/video-overlay--ESS--Full-Story-with-Traverse.jpg" alt="Video covershot - Traverse">
                    </figure>
                    <a class="video__play-btn" href="https://player.vimeo.com/video/587923642"></a>
                    <div class="video__container"></div>
                </div>
            </div>
            <div style="margin-top:1em;">
                <div class="container__one-half">
                    <h3>See the Full Patient Story, Wherever They Go, with Traverse (Canadian Session)</h3>
                    <p>Learn how Traverse is connecting Canadian communities by providing an EHR-to-EHR interoperability solution that puts the full patient story at clinicians' fingertips. Providers have all of the components they need to easily follow their patients across health systems and care environments, resulting in a better experience for all. Traverse supports provincial interoperability strategies, as well as the Ontario Health Team (OHT) initiative.</p>
                </div>
                <div class="container__one-half">
                    <h2>Additional Resources</h2>
                    <ul>
                        <li><a href="https://ehr.meditech.com/ehr-solutions/meditech-interoperability">MEDITECH Interoperability</a></li>
                        <li><a href="https://info.meditech.com/hubfs/WPs,%20Case%20Studies,%20Special%20Reports/CustomerSuccess_BethIsrael_One_Touch.pdf?hsCtaTracking=738a98fb-5c29-41b6-85ea-8461a5861781%7C71e12c8c-0c7d-469c-bd8c-ea146358856e">Beth Israel Deaconess Keeps Clinicians and Patients Connected with One Touch Feature in MEDITECH</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="container container__centered">
            <div class="video-container auto-margins">
                <div class="video js__video" data-video-id="587921217">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/video-overlay--ESS--International.jpg" alt="Video covershot - International">
                    </figure>
                    <a class="video__play-btn" href="https://player.vimeo.com/video/587921217"></a>
                    <div class="video__container"></div>
                </div>
            </div>
            <div style="margin-top:1em;">
                <div class="container__one-half">
                    <h3>Making a Global Impact and Innovating with MEDITECH Expanse (UK & Ireland, Australasia and MEA Session)</h3>
                    <p>Around the world, MEDITECH's Expanse EHR is easing physicians' cognitive burdens, giving nurses more agility, and alleviating the strains on caregivers and informaticists. This session will delve into MEDITECH's innovative development work in the areas of Genomics, Population Health, and Oncology.</p>
                </div>
                <div class="container__one-half">
                    <h2>Additional Resources</h2>
                    <ul>
                        <li><a href="https://ehr.meditech.com/global/meditech-uk-ireland">MEDITECH in the UK and Ireland</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="container container__centered">
            <div class="video-container auto-margins">
                <div class="video js__video" data-video-id="588313893">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/video-overlay--ESS--Genomics.jpg" alt="Video covershot - Genomics">
                    </figure>
                    <a class="video__play-btn" href="https://player.vimeo.com/video/588313893"></a>
                    <div class="video__container"></div>
                </div>
            </div>
            <div style="margin-top:1em;">
                <div class="container__one-half">
                    <h3>Decoding Genomics in the EHR</h3>
                    <p>Genomic sciences continue to provide an exciting vision for the future of healthcare. Without the proper tools, clinicians can only stand by and watch as new treatments and discoveries pave the way for enhanced treatments and better patient outcomes. Come hear about MEDITECH's groundbreaking LIS Genomics Solution and how it will help all clinicians utilize genetic data directly within their EHR workflow.</p>
                </div>
                <div class="container__one-half">
                    <h2>Additional Resources</h2>
                    <ul>
                        <li><a href="https://www.healthcareittoday.com/2021/04/21/meditech-integrates-genomics-into-their-ehr/">MEDITECH Integrates Genomics into their EHR</a></li>
                        <li><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/genomicinfographic.pdf">Genomics: Precision Medicine for All</a></li>
                        <li><a href="https://home.meditech.com/en/d/functionalitybriefs/otherfiles/genomics.pdf">Genomics & Precision Medicine: Functionality Brief</a></li>
                    </ul>
                </div>
            </div>
        </div>


        <div class="container container__centered">
            <div class="video-container auto-margins">
                <div class="video js__video" data-video-id="587878335">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/video-overlay--ESS--LL-Virtual-Assistant.jpg" alt="Video covershot - Virtual Assistant">
                    </figure>
                    <a class="video__play-btn" href="https://player.vimeo.com/video/587878335"></a>
                    <div class="video__container"></div>
                </div>
            </div>
            <div style="margin-top:1em;">
                <div class="container__one-half">
                    <h3>Lunch & Learn: Virtual Assistant</h3>
                    <p>Expanse Virtual Assistant responds to simple verbal commands by retrieving the information you're looking for — without you having to type, point, click, or even touch your device. Clinicians can experience new levels of efficiency and usability while keeping their focus squarely on patients, not their computer systems.</p>
                </div>
                <div class="container__one-half">
                    <h2>Additional Resources</h2>
                    <ul>
                        <li><a href="https://ehr.meditech.com/ehr-solutions/virtual-assistant">MEDITECH's Virtual Assistant</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="container container__centered">
            <div class="video-container auto-margins">
                <div class="video js__video" data-video-id="587927858">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/video-overlay--ESS--Cloud-Platform.jpg" alt="Video covershot - Cloud Platform">
                    </figure>
                    <a class="video__play-btn" href="https://player.vimeo.com/video/587927858"></a>
                    <div class="video__container"></div>
                </div>
            </div>
            <div style="margin-top:1em;">
                <div class="container__one-half">
                    <h3>MEDITECH Cloud Platform</h3>
                    <p>This demonstration highlights MEDITECH's new Cloud and Virtual solutions that can assist with today's challenges from both the patient and provider perspective. The audience will see how the patient benefits from the integration of the consumer health portal and the convenience of virtual care options and patient connect reminders. Additionally providers will see how virtual and mobile options such as Expanse Now and High Availability Snapshot will allow them to deliver care wherever they are.</p>
                </div>
                <div class="container__one-half">
                    <h2>Additional Resources</h2>
                    <ul>
                        <li><a href="https://f.hubspotusercontent10.net/hubfs/2897117/Cloud/Cloud-e-Book%20(2).pdf?__hstc=188859636.be74c2991f5ff55a33d25f26dfb96452.1626187137358.1629294582414.1629303543976.89&__hssc=188859636.2.1629303543976&__hsfp=814967395&hsCtaTracking=d7b365df-a9d7-4923-b15b-87d77611db35%7Ca1abd0fa-3587-4603-835b-4ec1eda990de">Expand Your Possibilities with MEDITECH Cloud Platform</a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <!-- End of Block 4 (recordings) -->



    <!-- Executives -->
    <div class="container container__centered center no-pad">

        <div class="auto-margins">
            <h2>Learn about the MEDITECH executives that were a part of this event.</h2>
        </div>

        <div class="container__one-fifth">
            <figure style="margin-bottom: 0;">
                <img class="exec" src="https://ehr.meditech.com/sites/default/files/images/people/Hoda-Sayed-Friel--MEDITECH--mobile-photo.jpg" alt="Hoda Sayed-Friel" />
                <figcaption class="center" style="margin-top: 1em;">
                    <h3 class="header-five no-margin"><a href="/about/executive/hoda-sayed-friel">Hoda Sayed-Friel</a></h3>
                    <p>Executive Vice President</p>
                </figcaption>
            </figure>
        </div>

        <div class="container__one-fifth">
            <figure style="margin-bottom: 0;">
                <img class="exec" src="https://ehr.meditech.com/sites/default/files/images/people/Helen-Waters--MEDITECH--mobile-photo.jpg" alt="Helen Waters" />
                <figcaption class="center" style="margin-top: 1em;">
                    <h3 class="header-five no-margin"><a href="/about/executive/helen-waters">Helen Waters</a></h3>
                    <p>Executive Vice President</p>
                </figcaption>
            </figure>
        </div>

        <div class="container__one-fifth">
            <figure style="margin-bottom: 0;">
                <img class="exec" src="https://ehr.meditech.com/sites/default/files/images/people/Leah-Farina--MEDITECH--mobile-photo.jpg" alt="Leah Farina" />
                <figcaption class="center" style="margin-top: 1em;">
                    <h3 class="header-five no-margin"><a href="/about/executive/leah-farina">Leah Farina</a></h3>
                    <p>Vice President of Client Services</p>
                </figcaption>
            </figure>
        </div>

        <div class="container__one-fifth">
            <figure style="margin-bottom: 0;">
                <img class="exec" src="https://ehr.meditech.com/sites/default/files/images/people/Carol-Labadini--MEDITECH--mobile-photo.jpg" alt="Carol Labadini" />
                <figcaption class="center" style="margin-top: 1em;">
                    <h3 class="header-five no-margin"><a href="/about/executive/carol-labadini">Carol Labadini</a></h3>
                    <p>Vice President of Client Services</p>
                </figcaption>
            </figure>
        </div>

        <div class="container__one-fifth">
            <figure style="margin-bottom: 0;">
                <img class="exec" src="https://ehr.meditech.com/sites/default/files/images/people/Scott-Radner--MEDITECH--mobile-photo.jpg" alt="Scott Radner" />
                <figcaption class="center" style="margin-top: 1em;">
                    <h3 class="header-five no-margin"><a href="/about/executive/scott-radner">Scott Radner</a></h3>
                    <p>Vice President of Advanced Technology</p>
                </figcaption>
            </figure>
        </div>

    </div>
    <!-- End Executives -->


</div>
<!-- end js__seo-tool__body-content -->


<!-- CTA-->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/events/Expanse-summer-showcase-event--sandcastle.png); padding:4em 0 20em 0;">
    <div class="container__centered center">


        <div class="btn-holder--content__callout" style="margin-top:2em;">
            <a href="https://home.meditech.com/webforms/contact.asp?rcpt=afilleti|meditech|com&rname=afilleti" class="btn--orange btn--outline">Contact Us With Any Questions</a>
        </div>

        <div style="margin-top:1em;">
            <?php print $share_link_buttons; ?>
        </div>

    </div>
</div>
<!-- End of CTA-->



<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>
<!-- END events-simple-page--node-4216.php -->
