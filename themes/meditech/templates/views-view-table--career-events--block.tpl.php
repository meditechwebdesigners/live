<?php
/**
 *
 *   This template is for EACH TABLE in the Views block: CAREER EVENTS .......................
 *
 *
 * @file
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $caption: The caption for this table. May be empty.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 * @ingroup views_templates
 */
?>
<!-- start views-view-table--career-events--block.tpl.php template -->

<?php if (!empty($title) || !empty($caption)) : ?>
   <h5><?php print $caption . $title; ?></h5>
<?php endif; ?>
<table class="table">
  <?php if (!empty($header)) : ?>
    <thead>
      <tr>
        <?php foreach ($header as $field => $label): ?>
          <?php if($label == 'Date'){ ?>
            <th class="table__careers__date"><?php print $label; ?></th>
          <?php } elseif($label == 'Event'){ ?>
            <th class="tables__careers__event"><?php print $label; ?></th>
          <?php } else { ?>
            <th class="tables__careers__event__location"><?php print $label; ?></th>
          <?php } ?>
        <?php endforeach; ?>
      </tr>
    </thead>
  <?php endif; ?>
  <tbody>
    <?php foreach ($rows as $row_count => $row): ?>
      <tr <?php if ($row_classes[$row_count]) { print 'class="' . implode(' ', $row_classes[$row_count]) .'"';  } ?>>
        <?php foreach ($row as $field => $content): ?>
          <td>
            <?php print $content; ?>
          </td>
        <?php endforeach; ?>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<!-- end views-view-table--career-events--block.tpl.php template -->