<?php if( user_is_logged_in() ){ ?>
<!-- START style-guide--node-2842.php Typography -->

<!--Javascript & CSS reference for Prism-->
<script type="text/javascript" src="<?php print $url; ?>/sites/all/themes/meditech/js/prism.js"></script>

<link href="<?php print $url; ?>/sites/all/themes/meditech/css/prism.css" rel="stylesheet" />

<style>
  .type-scale {
    padding: 0;
    margin: 0;
    list-style-type: none;
  }

  .type-scale li {
    display: flex;
    align-items: baseline;
    -webkit-align-items: baseline;
    padding: 0;
    overflow: auto;
    margin: .75em 0;
  }

  .type-scale li:last-child {
    padding-bottom: 0;
  }

</style>

<section class="container__centered">

  <h1 class="page__title">
    <?php print $title; ?>
  </h1>

  <div class="container__two-thirds">

    <h2>Fonts</h2>
    <p>Below are examples of our fonts and how they should be written within our CSS, complete with fallback fonts. These fonts can also be specified with the following classes: <code class="language-html">.montserrat</code> and <code class="language-html">.source-sans-pro</code>.</p>

    <div class="demo-ct">
      <h2>This is "Montserrat", our heading font.</h2>
      <p>This is "Source Sans Pro", our paragraph font.</p>
    </div>

    <!-- Start Montserrat CSS Code -->
    <pre><code class="language-css">font-family: "montserrat", Verdana, sans-serif;
font-family: "source-sans-pro", Helvetica, Arial, sans-serif;
</code></pre>
    <!-- End Montserrat CSS Code -->

    <h2>Headings</h2>
    <p>These are our headings with an example of how they should be written out in HTML. Beside each heading are the default font sizes in pixels and ems. In addition to that, we have heading classes which can be used to mimic the heading's font size without specifying it as a heading tag.</p> 
    <p>There is also an XL heading class, <code class="language-html">.header-xl</code> which can be used when the <code class="language-html">.header-one</code> size just isn't big enough. An example would be enlarging text (like numbers) for better visibility or making a prominent headline for our largest conference; HIMSS. Use sparingly.</p>
    <p><span class="italic"><strong>Note:</strong> The heading classes only change the font size and not the font family. If they are not used in conjunction with an H tag, the font will be Source Sans Pro by default.</span></p>

    <div class="demo-ct">

      <ul class="type-scale">

        <li>
          <div class="container__one-half">
            <span class="no-margin header-xl">Heading XL</span>
          </div>
          <div class="container__one-fourth">
            <p>2.5em / 40px</p>
          </div>
          <div class="container__one-fourth">
            <p><code class="language-html">.header-xl</code></p>
          </div>
        </li>
         <li>
          <div class="container__one-half">
            <h1 class="no-margin">Heading One</h1>
          </div>
          <div class="container__one-fourth">
            <p>1.8em / 32.4px</p>
          </div>
          <div class="container__one-fourth">
            <p><code class="language-html">.header-one</code></p>
          </div>
        </li>
        <li>
          <div class="container__one-half">
            <h2 class="no-margin">Heading Two</h2>
          </div>
          <div class="container__one-fourth">
            <p>1.5em / 27px</p>
          </div>
          <div class="container__one-fourth">
            <p><code class="language-html">.header-two</code></p>
          </div>
        </li>
        <li>
          <div class="container__one-half">
            <h3 class="no-margin">Heading Three</h3>
          </div>
          <div class="container__one-fourth">
            <p>1.2em / 21.6px</p>
          </div>
          <div class="container__one-fourth">
            <p><code class="language-html">.header-three</code></p>
          </div>
        </li>
        <li>
          <div class="container__one-half">
            <h4 class="no-margin">Heading Four</h4>
          </div>
          <div class="container__one-fourth">
            <p>1.1em / 19.8px</p>
          </div>
          <div class="container__one-fourth">
            <p><code class="language-html">.header-four</code></p>
          </div>
        </li>
        <li>
          <div class="container__one-half">
            <h5 class="no-margin">Heading Five</h5>
          </div>
          <div class="container__one-fourth">
            <p>.9em / 16.2px</p>
          </div>
          <div class="container__one-fourth">
            <p><code class="language-html">.header-five</code></p>
          </div>
        </li>
        <li>
          <div class="container__one-half">
            <h6 class="no-margin">Heading Six</h6>
          </div>
          <div class="container__one-fourth">
            <p>.88em / 15.84px</p>
          </div>
          <div class="container__one-fourth">
            <p><code class="language-html">.header-six</code></p>
          </div>
        </li>
        <li>
          <div class="container__one-half">
            <p class="no-margin header-micro">Heading Micro</p>
          </div>
          <div class="container__one-fourth">
            <p>.875em / 15.75px</p>
          </div>
          <div class="container__one-fourth">
            <p><code class="language-html">.header-micro</code></p>
          </div>
        </li>

      </ul>
    </div>

    <!-- Start Headings Code -->
    <pre><code class="language-html">&lt;h1>Heading One&lt;/h1>
&lt;h2>Heading Two&lt;/h2>
&lt;h3>Heading Three&lt;/h3>
&lt;h4>Heading Four&lt;/h4>
&lt;h5>Heading Five&lt;/h5>
&lt;h6>Heading Six&lt;/h6>
&lt;p class="header-micro">Heading Micro&lt;/p>
</code></pre>
    <!-- End Headings Code -->

    <h2>Font Styles</h2>
    <div class="demo-ct">
      <p><span class="italic">This text is italic.</span></p>
      <p><span class="bold">This text is bold.</span></p>
      <p><strong>This text is also bold.</strong></p>
    </div>

    <!-- Start Font Styles Code -->
    <pre style="margin-bottom:1em;"><code class="language-html">&lt;!-- Italic -->
&lt;span class="italic">This text is italic.&lt;/span>

&lt;!-- Bold -->
&lt;span class="bold">This text is bold.&lt;/span>

&lt;!-- Bold alternative -->
&lt;strong>This text is also bold.&lt;/strong>
</code></pre>
    <!-- End Font Styles Code -->

    <p><span class="italic"><strong>Note:</strong> Since there are multiple ways to write bold text, please refer to this <a href="https://developer.mozilla.org/en-US/docs/Web/HTML/Element/strong" target="_blank">Mozilla Developers article</a> for semantics and accessibility.</span></p>


    <h2>Text Sizing</h2>
    <div class="demo-ct">
      <p><span class="text--small">This text is small.</span></p>
      <p>This text is regular</p>
      <p><span class="text--large">This text is large.</span></p>
    </div>

    <!-- Start Text Sizing Code -->
    <pre><code class="language-html">&lt;!-- Small (.85em / 15.3px) -->
&lt;span class="text--small">This text is small.&lt;/span>

&lt;!-- Regular (1.125em / 18px) -->
&lt;p>This text is regular.&lt;/p>

&lt;!-- Large (1.2em / 21.6px) -->
&lt;span class="text--large">This text is large.&lt;/span>
</code></pre>
    <!-- End Text Sizing Code -->

    <h2>Text Shadow</h2>
    <div class="demo-ct" style="background-color:#e6e9ee;">      
      <h3 class="text-shadow--black text--white">This has a black text shadow.</h3>
      <h3 class="text-shadow--white">This has a white text shadow.</h3>
    </div>

    <!-- Start Font Styles Code -->
    <pre><code class="language-html">&lt;!-- Black text shadow -->
&lt;h3 class="text-shadow--black">This has a black text shadow.&lt;/h3>

&lt;!-- White text shadow -->
&lt;h3 class="text-shadow--white">This has a white text shadow.&lt;/h3>
</code></pre>
    <!-- End Font Styles Code -->


  </div>

  <!-- SIDEBAR -->
  <aside class="container__one-third">

    <div class="sidebar__nav panel">
      <?php
        $styleBlock = module_invoke('menu', 'block_view', 'menu-style-guide-side-nav');
        print render($styleBlock['content']); 
        ?>
    </div>

  </aside>
  <!-- END SIDEBAR -->

</section>

<!-- END style-guide--node-2842.php Typography -->
<?php } ?>