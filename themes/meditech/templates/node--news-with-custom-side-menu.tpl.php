<?php
// This template is set up to control the display of the 'news-with-custom-side-menu' content type 
// as well as the 'news-tags' taxonomy dynamic pages.
// 
// SECTION 1 describes the structure of the content when it's being displayed as an individual news article page.
// 
// SECTION 2 describes the structure of the content when it's being displayed in the main News page 
// OR as a list on a taxonomy page.


// get current URL info to determine which layout to render...
$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$currentURLarray = explode('/', $currentURL);
$urlArrayCountMinusThree = count($currentURLarray) -3;
$urlArrayCountMinusTwo = count($currentURLarray) -2;
$urlArrayCountMinusOne = count($currentURLarray) -1;
$lastURLsegment = $currentURLarray[$urlArrayCountMinusOne];
$secondToLastURLsegment = $currentURLarray[$urlArrayCountMinusTwo];
$thirdToLastURLsegment = $currentURLarray[$urlArrayCountMinusThree];

include('inc-share-buttons.php');

// [SECTION 1] if a ARTICLE node is being shown then render the following ========================================================================================================
// ex: ../news/article-name


if($secondToLastURLsegment == 'news' || $thirdToLastURLsegment == 'news' || $secondToLastURLsegment == "node" || $thirdToLastURLsegment == "node"){
?>

<!-- start node--news-with-custom-side-menu.tpl.php template SECTION 1 -->

  <section class="container__centered">

    <div class="container__two-thirds">
     
      <article class="no-pad--top">
      
      <?php 
        // get value for main image...
        $main_image = render($content['field_news_article_main_image']);
  
        if(!empty($main_image)){
        ?>
          <figure class="news__article__img">
            <?php print render($content['field_news_article_main_image']); ?>
          </figure>
        <?php
        }
        ?>
        
        <h1 class="page__title js__seo-tool__title"><?php print $title; ?></h1>

        <?php if(!empty($content['field_subtitle'])){ ?>
          <p class="italic js__seo-tool__body-content"><?php print render($content['field_subtitle']); ?></p>
        <?php } ?>
        
        
        <?php if($node->nid != 3241){ ?>
          <div class="inline__text__wrapper">
          <?php if($node->published_at){ ?>
            <p><span class="news__article__callout"><time datetime="<?php print date('Y-m-d', $node->published_at); ?>" itemprop="datePublished"><?php print date('F j, Y', $node->published_at); ?></time></span>
          <?php } else { // for previewing purposes... ?>
            <p><span class="news__article__callout"><em>Unpublished: No Date</em></span>
          <?php } ?>
            &nbsp;|&nbsp;</p>
            <ul class="news__article__filters tag_link_news_article_gae">
              <?php generate_news_tag_links($node); ?>
            </ul>
          </div>
        <?php } ?>
        

        <div class="js__seo-tool__body-content">
          <?php 
          if(!empty($content['field_body'])){
            print render($content['field_body']); // main article content 
          }
          else{
            print render($content['field_summary']); // summary content 
          } 
          ?>
          
          
          <?php
          $other_versions_covid_article = '
          <h3>Other Revisions</h3>
          <ul>
            <li><a href="https://ehr.meditech.com/news/meditech-releases-updated-decision-support-and-guidance-for-coronavirus-revised-070920">Revision 07/09/20</a></li>
            <li><a href="https://ehr.meditech.com/news/meditech-releases-updated-decision-support-and-guidance-for-coronavirus-revised-041620">Revision 04/16/20</a></li>
            <li><a href="https://ehr.meditech.com/news/meditech-releases-updated-decision-support-and-guidance-for-coronavirus-revised-040920">Revision 04/09/20</a></li>
          </ul>
          ';
          switch($node->nid){
            case 3288:
            case 3323:
            case 3389:
              print $other_versions_covid_article;
              break;
            default:
              print '';
          }
          ?>
        </div>
        
        <div>
          <?php print $share_link_buttons; ?>
        </div>
        
      </article>

      <?php // SEO Tool for internal use...
      if( user_is_logged_in() ){
        print '<!-- SEO Tool is added to this DIV -->';
        print '<div class="container js__seo-tool"></div>';
      } 
      ?>

    </div>

    <!-- SIDEBAR -->
    <aside class="container__one-third">
      
      <div class="panel">
        <div class="sidebar__nav news_sidebar_gae">
          <?php
            switch($node->nid){
              case 3241:
              case 3250:
              case 3251:
              case 3260:
              case 3261:
              case 3284:
              case 3288:
              case 3289:
              case 3290:
              case 3293:
              case 3323:
              case 3331:
              case 3332:
              case 3335:
              case 3337:
              case 3339:
              case 3340:
              case 3347:
              case 3348:
              case 3349:
              case 3351:
              case 3363:
              case 3365:
              case 3367:
              case 3372:
              case 3373:
              case 3374:
              case 3376:
              case 3389:
              case 3391:
                include('inc-covid19-sidebar.php');
                break;
              default:
                print render($content['field_long_text_1']); // custom sidebar content
            }
          ?>
        </div>
      </div>
      
    </aside>
    <!-- END SIDEBAR -->

  </section><!-- END SECTION -->
  
  <!-- Google Structured Data -->
  <?php 
    $image_url = file_create_url($node->field_news_article_main_image['und'][0]['uri']);
  ?>
  <script type="application/ld+json">
    {
      "@context" : "https://schema.org/",
      "@type" : "NewsArticle",
      "headline" : "<?php print $title; ?>",
      "datePublished" : "<?php print date('Y-m-d', $node->published_at); ?>",
      "dateModified" : "<?php print date('Y-m-d', $node->changed); ?>",
      "image" : [ "<?php print $image_url; ?>" ],
      "author" : {
        "@type" : "Organization",
        "name" : "MEDITECH"
      },
      "publisher" : {
        "@type" : "Organization",
        "name" : "MEDITECH",
        "logo" : {
          "@type" : "ImageObject",
          "name" : "MEDITECHlogo",
          "width" : "600",
          "height" : "60",
          "url" : "https://ehr.meditech.com/sites/all/themes/meditech/images/MEDITECH-logo--structured-data-version.jpg"
        }
      },
      "mainEntityOfPage" : "<?php print $currentURL; ?>"
    }
  </script> 
  
  <script>
    document.getElementsByTagName("h1")[0].innerHTML = 
      document.getElementsByTagName("h1")[0].innerHTML
      .replace(/((?!<sup>\s*))&reg;((?!\s*<\/sup>))/gi, '<sup>&reg;</sup>')
      .replace(/((?!<sup>\s*))®((?!\s*<\/sup>))/gi, '<sup>&reg;</sup>')
    ;
  </script>

<!-- END node--news-with-custom-side-menu.tpl.php template SECTION 1 -->


<?php
}
else{
  // [SECTION 2] if the NEWS-TAGS taxonomy page is being shown then render the following ========================================================================================================
  // ex: ../news-tags/physician
?>

  <!-- start node--news-with-custom-side-menu.tpl.php template SECTION 2 -->
  <?php 
  // get node URL from node ID...
  $nodeURL = url('node/'. $node->nid); 
  // check node's taxonomy news terms...
  $news_tags = field_view_field('node', $node, 'field_news_tags'); 
  // 'field_news_tags' is the machine name of the field in the content type that contains the taxonomy terms

  // list taxonomy news term associated with article...
  $tag_array = array();
  foreach($news_tags['#items'] as $news_tag){
    $term = taxonomy_term_load($news_tag['tid']);
    $term_name = $term->name;
    $tag_array[] = $term_name;
  } 
  ?>
    <div>

      <h3><a class="taxonomy_result_link_gae" href="<?php print $nodeURL; ?>"><?php print $title; ?></a></h3>

      <div class="inline__text__wrapper">
        <p><span class="snippet__card__text--callout"><time datetime="<?php print date('Y-m-d', $node->published_at); ?>" itemprop="datePublished"><?php print date("F j, Y", $node->published_at); ?></time></span>&nbsp;&mdash;&nbsp;</p>
        <?php
        $summary = field_view_field('node', $node, 'field_summary');
        print render($summary); 
        ?>
      </div>

      <ul class="news__article__filters tag_link_news_article_gae">
        <?php generate_news_tag_links($node); ?>
      </ul>

      <hr>

    </div>
  
<!-- END node--news-with-custom-side-menu.tpl.php template SECTION 2 -->
<?php
}
?>