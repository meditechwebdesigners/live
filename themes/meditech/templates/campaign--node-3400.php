<!-- START campaign--node-3400.php -->
<?php // This template is set up to control the display of the Expanse for Physician Campaign content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<script src="<?php print $url; ?>/sites/all/themes/meditech/js/splide.min.js"></script>

<style>
    /*	Splide Carousel Styles*/

    .splide__pagination {
        display: none;
    }

    .splide__slide {
        transition: transform 1s;
        transform: scale(.9);
    }

    .splide__slide:focus-visible {
        border: 1px solid #3e4545;
    }

    .splide__slide a:focus-visible {
        font-weight: bold;
    }

    .splide__slide.is-active {
        transform: scale(1);
        transition: transform .5s .4s;
    }

    .splide__arrow:hover {
        background: #dde0e7;
        transition: background .25s ease-in-out;
    }

    .splide__arrow:focus-visible {
        outline: 3px solid black;
    }

    /*	Secondary Slider Styles (Buttons)*/

    #secondary-slider {
        margin: 0 2em;
    }

    #secondary-slider .splide__track {
        padding-top: 1em;
    }

    #secondary-slider .splide__slide {
        transform: scale(1);
        cursor: pointer;
        color: #087E68;
        border: 1px solid #c8ced9;
        border-radius: 7px;
        padding: .5em 1em;
        font-size: 15px;
        font-weight: normal;
        transition: all .2s ease;
    }

    #secondary-slider .splide__slide:hover {
        color: #3e4545;
        border: 1px solid #3e4545;
    }

    #secondary-slider .is-active,
    #secondary-slider .splide__slide:active,
    #secondary-slider .splide__slide:focus {
        color: #3e4545;
        border: 1px solid #3e4545;
        transition: all .2s ease;
    }

    /*	Universal Styles*/
    .card {
        box-shadow: none;
        border: 1px solid #c8ced9;
    }

    .container-pad {
        padding: 4em 0;
    }

    .bg--light-blue {
        background-color: #f2f8fc;
    }

    .bg-pattern--blue-squares {
        z-index: 1;
    }

    .bg-pattern--container img {
        position: relative;
        z-index: 2;
    }

    .content__callout,
    .content__callout__content {
        background-color: transparent;
    }

    .circle-video-popup {
        width: 100%;
        border: 4px solid #e6e9ee;
        border-radius: 50%;
    }

    .vid-bg {
        width: 55px;
        height: 55px;
    }

    .video2.content__callout__content {
        padding-left: 8%;
        padding-right: 0;
        color: #fff;
    }

    .mag-icon {
        top: 52%;
        left: 51%;
    }



    /*	Splide affecting footer links (need fix)*/
    footer ul li {
        font-weight: 400;
    }

    /*	Media Queries*/
    @media all and (max-width: 900px) {
        .splide__slide {
            transform: scale(1);
        }

        .card figure img {
            height: 225px;
        }
    }

    @media all and (max-width: 50em) {
        .container-pad {
            padding: 2em 0;
        }

        .video2.content__callout__content {
            padding: 2em 6% 0 6%;
        }
    }

    @media all and (max-width: 600px) {
        #secondary-slider {
            display: none;
        }

        .splide__pagination {
            display: inline-flex;
            align-items: center;
            width: 100%;
            flex-wrap: wrap;
            justify-content: center;
            margin: 0
        }

        .splide__arrow {
            top: auto;
        }

        .splide__arrow--prev {
            left: 8%;
            bottom: -14px;
        }

        .splide__arrow--next {
            right: 8%;
            bottom: -14px;
        }
    }

    @media all and (max-width: 415px) {
        .splide__pagination li button {
            display: none;
        }

        .splide__arrow--prev {
            left: 40%;
        }

        .splide__arrow--next {
            right: 40%;
        }

        .card figure img {
            height: 150px;
        }
    }

    /*    YouTube specific classes */
    .video3 {
        position: relative;
        padding-bottom: 56.25%;
        /* 16:9 */
        height: 0;
    }

    .video3 iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }

    .border-none {
        border: none;
    }

    @media all and (max-width: 50em) {
        .content__callout__content {
            padding-left: 6%;
            color: #3e4545;
            padding-top: 0em;
        }

        .video {
            max-width: 100%;
        }
    }

    .video-iframe {
        /*Shrinks to match size of other video blocks*/
        max-width: 84.5%;
        position: relative;
        /* 16:9 */
        padding-bottom: 43%;
        overflow: hidden;
        margin: 0 auto;
        border-radius: 6px;
        box-shadow: 7px 8px 26px rgba(0, 0, 0, 0.1);
    }

    .video-iframe iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }

    @media all and (max-width: 50em) {
        .video-iframe {
            max-width: 100%;
        }
    }

</style>

<div class="js__seo-tool__body-content">

    <h1 style="display:none;" class="js__seo-tool__title">Expanse for Physicians</h1>

    <!-- Block 1 -->
    <div class="container container-pad">
        <div class="container__centered">
            <div class="container__one-half">
                <div class="bg-pattern--container">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/physician-sitting-with-patient-talking.jpg" alt="Physician talking with patient while using EHR">
                    <div class="bg-pattern--green-squares bg-pattern--left"></div>
                </div>
            </div>
            <div class="container__one-half content--pad-left">
                <p class="header-micro">EXPANSE</p>
                <h2>The Physician’s EHR</h2>
                <h3>With MEDITECH, physicians can focus on their patients, not their EHR.</h3>
                <p>Physicians face unpredictable situations every day, but their EHR shouldn’t be among them. MEDITECH Expanse gives doctors an EHR that suits them and the way they work so they can provide safe and efficient care, no matter what.</p>
                <div>
                    <?php hubspot_button($cta_code, "Register For The 2021 Physician And CIO Forum"); ?>
                </div>
            </div>
        </div>
    </div>
    <!-- END Block 1 -->

    <!-- Block 2 -->
    <div class="container container-pad bg--purple-gradient">
        <div class="container__centered">
            <div class="container__one-third">
                <p class="header-micro">PERSONALIZATION</p>
                <h2>It's your thing</h2>
                <p>Physicians are most satisfied with their EHR when they can personalize it. Expanse provides clinicians with a library of intuitive widgets and shortcuts, customizable order sets, and streamlined common tasks, so they can tailor it to their personal workflows.</p>
                <div id="modal1" class="modal">
                    <a class="close-modal" href="javascript:void(0)">&times;</a>
                    <div class="modal-content" style="z-index:10001;">
                        <iframe src="https://player.vimeo.com/video/239126049" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="open-modal" data-target="modal1">
                    <img class="circle-video-popup" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dr_m_surburg.jpg" alt="Matthew Surburg, MD Headshot" style="width: 150px;">
                    <div class="vid-bg">
                        <i class="mag-icon fas fa-play"></i>
                    </div>
                </div>
                <p>Learn how Expanse enables doctors to create their own workflows and improve efficiency.</p>
            </div>
            <div class="container__two-thirds" style="padding: 3em 0 0 0;">
                <div id="modal2" class="modal">
                    <a class="close-modal" href="javascript:void(0)">&times;</a>
                    <div class="modal-content">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Widgets--GIF.gif" alt="Expanse widgets showing vital signs walkthrough">
                    </div>
                </div>
                <figure>
                    <div class="open-modal tablet--white" data-target="modal2">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Widgets--GIF.gif" alt="Expanse widgets showing vital signs walkthrough (full size)">
                        <div class="mag-bg">
                            <i class="mag-icon fas fa-search-plus"></i>
                        </div>
                    </div>
                </figure>
            </div>
        </div>
    </div>
    <!-- END Block 2 -->

    <!-- Block 3 -->
    <div class="content__callout">
        <div class="content__callout__media">
            <div class="content__callout__image-wrapper">
                <div class="video js__video" data-video-id="413676623">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--interview-dr-doug-kanis-on-expanse.jpg" alt="Interview with Dr. Doug Kanis on Expanse - video covershot">
                    </figure>
                    <a class="video__play-btn" href="https://vimeo.com/413676623"></a>
                    <div class="video__container"></div>
                </div>
            </div>
        </div>
        <div class="content__callout__content">
            <div class="content__callout__body">
                <p class="header-micro">EFFICIENCY</p>
                <h2>Finish your work at work</h2>
                <p>With Expanse, improved efficiency comes naturally — from more intuitive, usable software. Physicians can finish their documentation at work and not take it home with them, so they can focus on caring for their patients. That makes everyone happier: Physicians. Patients. Communities.</p>
                <p class="no-margin--bottom" style="margin-top: 1.5em;"><strong>VIDEO: Getting home on time</strong></p>
                <p>Doug Kanis, DO, CMIO at Pella Regional Health Center, explains how Expanse makes it easier for him and his colleagues to provide the best care to their community.</p>
            </div>
        </div>
    </div>
    <!-- END Block 3 -->

    <!-- Block 4 -->
    <div class="bg--purple-gradient">
        <div class="content__callout">
            <div class="video2 content__callout__content">
                <div class="content__callout__body">
                    <p class="header-micro">Mobility</p>
                    <h2>Keep moving and stay connected</h2>
                    <p>Expanse doesn't tie physicians down, and never gets between them and their patients. Optimized for mobility, Expanse is the first natively web-based EHR, so doctors remain patient-facing as they tap and swipe through charts using techniques they already use with personal devices and apps.</p>
                    <p class="bold italic">“Thinks like a paper chart, works like an app”</p>
                    <p>Hear from six providers about how Expanse has improved mobility and changed their lives — at work and at home.</p>
                </div>
            </div>
            <div class="content__callout__media">
                <div class="content__callout__image-wrapper">
                    <div class="video js__video" data-video-id="451941860">
                        <figure class="video__overlay">
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay-Benefits-EHR-Mobility.jpg" alt="Physicians Reflect on the Benefits of EHR Mobility with Expanse">
                        </figure>
                        <a class="video__play-btn" href="https://vimeo.com/451941860"></a>
                        <div class="video__container"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Block 4 -->

    <!--	New Block 4.5-->
    <div class="content__callout border-none">

        <div class="content__callout__media">
            <div class="content__callout__image-wrapper">
                <div class="video js__video" data-video-id="371715414">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay-Halifax-Health-Improving-Efficiency-and-Reducing-Burnout.jpg" alt="Physicians Reflect on the Benefits of EHR Mobility with Expanse">
                    </figure>
                    <a class="video__play-btn" href="https://vimeo.com/371715414"></a>
                    <div class="video__container"></div>
                </div>
            </div>
        </div>

        <!--
        <div class="content__callout__media">
            <div class="content__callout__image-wrapper">
                <div class="js__video video-iframe">
                    <iframe width="500" height="315" src="https://www.youtube-nocookie.com/embed/vdu0vdOZG7Y" title="YouTube video player" frameborder="0" allow="encrypted-media;" allowfullscreen></iframe>
                </div>
            </div>
        </div>
-->


        <div class="content__callout__content">
            <div class="content__callout__body">
                <p class="header-micro">BALANCE</p>
                <h2>Defeat Burnout</h2>
                <p>At the heart of every physician’s day is their workload. Expanse helps <a href="https://home.meditech.com/en/d/mktcontent/otherfiles/expansenow.pdf">mitigate burnout</a> and lowers cognitive load with an intuitive, personalizable, mobile design that <a href="https://blog.meditech.com/how-ehrs-can-give-physicians-their-pajama-time-back-infographic-0">supports physicians’ work-life balance.</a></p>
            </div>
        </div>
    </div>



    <!--
    <div class="container container__centered">
        <div>
            <div class="container__one-half">
                <div class="video3"><iframe width="640" height="360" src="https://www.youtube-nocookie.com/embed/vdu0vdOZG7Y" title="YouTube video player" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true"></iframe></div>
            </div>
            <div class="container__one-half">
                <p class="header-micro">BALANCE</p>
                <h2>Defeat Burnout</h2>
                <p>At the heart of every physician’s day is their workload. Expanse helps <a href="https://home.meditech.com/en/d/mktcontent/otherfiles/expansenow.pdf">mitigate burnout</a> and lowers cognitive load with an intuitive, personalizable, mobile design that <a href="https://blog.meditech.com/how-ehrs-can-give-physicians-their-pajama-time-back-infographic-0">supports physicians’ work-life balance.</a></p>
            </div>
        </div>
    </div>
-->

    <!--	End of New Block 4.5-->

    <!-- Block 5 -->
    <div class="container container-pad bg--light-blue">
        <div class="container__centered">
            <div class="flex-order--container">
                <div class="container__one-half content--pad-right">
                    <p class="header-micro">CONVENIENCE</p>
                    <h2>Reach your patients where they are</h2>
                    <p>The use of virtual visits increased rapidly with the COVID-19 crisis, and their impact looks to be long-lasting. Using virtual care, clinicians and patients alike are benefiting from the convenience and safety of these visits, while reducing the risk of infections in crowded waiting rooms.</p>
                </div>
                <div class="container__one-half flex-order--reverse">
                    <div class="bg-pattern--container">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/physician-using-telehealth-with-patient.jpg" alt="Physician using telehealth with patient checking blood pressure">
                        <div class="bg-pattern--blue-squares bg-pattern--right"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Block 5 -->

    <!-- Block 6 -->
    <div class="container">
        <div class="container__centered">

            <div class="center">
                <p class="header-micro" style="display: inline;">FUNCTIONALITY</p>
                <h2>The right tools for the job</h2>
            </div>

            <div id="primary-slider" class="splide">
                <div class="splide__track">
                    <div class="splide__list">

                        <li class="splide__slide card" tabindex="0">
                            <figure>
                                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Expanse-Now--Carousel.jpg" alt="Woman checking Expanse Now on mobile device">
                            </figure>
                            <div class="card__info">
                                <h3>Expanse Now</h3>
                                <p>MEDITECH’s physician app lets doctors remotely manage routine tasks from their smartphone, effectively communicating and coordinating care from the palm of their hands.</p>
                                <p><a href="<?php print $url; ?>/ehr-solutions/ehr-mobility#GtyEwya">Read more about Expanse Now</a></p>
                            </div>
                        </li>

                        <li class="splide__slide card" tabindex="0">
                            <figure>
                                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Virtual-Care--Carousel.jpg" alt="Man using virtual care to diagnose daughter">
                            </figure>
                            <div class="card__info">
                                <h3>Virtual Care</h3>
                                <p>MEDITECH’s Virtual Care solutions help patients stay connected to their care providers via convenient video visits no matter the circumstances, with options for scheduled and on-demand visits for both new and existing patients.</p>
                                <p><a href="<?php print $url; ?>/ehr-solutions/virtual-care">Read more about Virtual Care</a></p>
                            </div>
                        </li>

                        <li class="splide__slide card" tabindex="0">
                            <figure>
                                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/oncology-patient--treatment.jpg" alt="Dr speaking with oncology patient">
                            </figure>
                            <div class="card__info">
                                <h3>Expanse Oncology</h3>
                                <p>All the benefits of Expanse's modern, web-based user interface, tailored to the needs of oncologists. Expanse Oncology optimizes cancer care team communication with evidence-based treatment plans that support multidisciplinary ordering.</p>
                                <p><a href="<?php print $url; ?>/ehr-solutions/meditech-oncology">Read more about Expanse Oncology</a></p>
                            </div>
                        </li>

                        <li class="splide__slide card" tabindex="0">
                            <figure>
                                <img src="<?php print $url; ?>/sites/default/files/styles/main-article-image-breakpoints_theme_meditech_big-medium_2x/public/images/news/High-Availability-SnapShot.jpg" alt="Female doctor using tablet">
                            </figure>
                            <div class="card__info">
                                <h3>High Availability SnapShot</h3>
                                <p>High Availability SnapShot equips organizations with uninterrupted access to EHR patient data during unplanned downtime events, ensuring patient safety and enabling clinicians to provide continuous quality care.</p>
                                <p><a href="<?php print $url; ?>/ehr-solutions/digital-transformation#HAS">Read more about High Availability SnapShot</a></p>
                            </div>
                        </li>

                        <li class="splide__slide card" tabindex="0">
                            <figure>
                                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/internist-speaking-with-virtual-assistant.jpg" alt="Physician using verbal commands to control Expanse software">
                            </figure>
                            <div class="card__info">
                                <h3>Expanse Virtual Assistant</h3>
                                <p>Expanse Virtual Assistant responds to verbal commands, retrieving the information doctors are looking for — without the need to type, point, click, or even touch a device, so they can focus on their patients, not their computer systems.</p>
                                <p><a href="<?php print $url; ?>/ehr-solutions/virtual-assistant">Read more about Expanse Virtual Assistant</a></p>
                            </div>
                        </li>

                        <li class="splide__slide card" tabindex="0">
                            <figure>
                                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/home/nurse-forum-2020-recap--video-overlay.jpg" alt="Nurses using Expanse Efficiency Dashboard">
                            </figure>
                            <div class="card__info">
                                <h3>Efficiency Dashboard</h3>
                                <p>Expanse Efficiency Dashboard leverages user metrics to identify and close training and knowledge gaps, adjust provider workflows, improve EHR proficiency and personalization, and deliver a better overall physician experience.</p>
                                <p><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/physicianefficiencydashboardinfographic.pdf" target="_blank">Read more about Efficiency Dashboard</a></p>
                            </div>
                        </li>

                        <li class="splide__slide card" tabindex="0">
                            <figure>
                                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-Traverse.jpg" alt="Meditech Traverse interoperability suite">
                            </figure>
                            <div class="card__info">
                                <h3>Traverse</h3>
                                <p>Traverse, MEDITECH’s powerful interoperability suite, puts patients back at the center of healthcare by bridging the gaps that can occur during transitions between providers, care settings, and organizations.</p>
                                <p><a href="<?php print $url; ?>/ehr-solutions/meditech-interoperability#Wimttthl">Read more about MEDITECH Traverse</a></p>
                            </div>
                        </li>

                        <li class="splide__slide card" tabindex="0">
                            <figure>
                                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/rotating-dna-glowing-molecule.jpg" alt="Expanse genomics showing glowing molecule">
                            </figure>
                            <div class="card__info">
                                <h3>Expanse Genomics</h3>
                                <p>Expanse Genomics is an integrated EHR-based solution that receives, stores, and presents complex genomic information to clinicians at the point of care — with clinical decision support and guidance to help them confidently counsel their patients.</p>
                                <p><a href="<?php print $url; ?>/ehr-solutions/meditech-genomics">Read more about Expanse Genomics</a></p>
                            </div>
                        </li>

                    </div>
                </div>
            </div>

            <div id="secondary-slider" class="splide" aria-hidden="true">
                <div class="splide__track">
                    <ul class="splide__list">
                        <li class="splide__slide" tabindex="-1">
                            Expanse Now
                        </li>
                        <li class="splide__slide" tabindex="-1">
                            Virtual Care
                        </li>
                        <li class="splide__slide" tabindex="-1">
                            Expanse Oncology
                        </li>
                        <li class="splide__slide" tabindex="-1">
                            High Availability SnapShot
                        </li>
                        <li class="splide__slide" tabindex="-1">
                            Expanse Virtual Assistant
                        </li>
                        <li class="splide__slide" tabindex="-1">
                            Efficiency Dashboard
                        </li>
                        <li class="splide__slide" tabindex="-1">
                            Traverse
                        </li>
                        <li class="splide__slide" tabindex="-1">
                            Expanse Genomics
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </div>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var secondarySlider = new Splide('#secondary-slider', {
                autoWidth: true,
                autoHeight: true,
                gap: "1.5em",
                arrows: false,
                isNavigation: true,
                accessibility: false,
                focus: 'center',
                breakpoints: {
                    600: {
                        isNavigation: false,
                    }
                }
            }).mount();

            var primarySlider = new Splide('#primary-slider', {
                type: "loop",
                perPage: 3,
                gap: "1em",
                padding: {
                    right: "39px"
                },
                speed: "400",
                pagination: "false",
                keyboard: "true",
                updateOnMove: "true",
                focus: "center",
                breakpoints: {
                    900: {
                        perPage: 1,
                        gap: "2em",
                        padding: {
                            right: "2em",
                            left: "2em",
                        }
                    }
                }
            });

            primarySlider.sync(secondarySlider).mount();
        });

    </script>
    <!-- END Block 6 -->

    <!-- Block 7 -->
    <div class="container container-pad bg--purple-gradient">
        <div class="container__centered center">

            <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
            <h2>
                <?php print $cta->field_header_1['und'][0]['value']; ?>
            </h2>
            <?php } ?>

            <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
            <div>
                <?php print $cta->field_long_text_1['und'][0]['value']; ?>
            </div>
            <?php } ?>

            <div class="center" style="margin-top:2em;">
                <?php hubspot_button($cta_code, "Register For The 2021 Physician And CIO Forum"); ?>
            </div>

            <div style="margin-top:1em;">
                <?php print $share_link_buttons; ?>
            </div>

        </div>
    </div>
    <!-- END Block 7 -->

</div>
<!-- end js__seo-tool__body-content -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>

<!-- END campaign--node-3400.php Expanse for Physician CAMPAIGN -->
