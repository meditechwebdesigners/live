<?php // This template is for each row of the Views block: CASE STUDIES 3-COLUMN VENDORS ....................... ?>
<!-- start views-view-fields--case-studies-3-column-vendors--block.tpl.php template -->
<?php    
// get node ID...
$nid = $fields['nid']->content;
$node = node_load($nid);

// display state if there is one...
$topicTerms = field_view_field('node', $node, 'field_case_study_topics'); 
if(!empty($topicTerms)){
  foreach($topicTerms["#items"] as $topicTerm){
    $topicAbbr = $topicTerm["taxonomy_term"]->description;
    $topicTerm = $topicTerm["taxonomy_term"]->name;
  }
}
else{
  $topicAbbr = '';
  $topicTerm = '';
}
?>
<div class="container__thirds <?php print strip_tags(trim($topicAbbr)); ?> bg--light-gray">
   <div class="container__content">
    <h3 style="margin-bottom:0;"><?php print $fields['title']->content; ?></h3>
    <p style="font-size:.8em; margin:0 0 .6em 0; font-style:italic;">Published on <?php print $fields['published_at']->content; ?>
    <?php // add Edit Video link...
      if( user_is_logged_in() ){ 
        print ' <span style="font-size:12px;">'; print l( t('Edit This'),'node/'. $fields['nid']->content .'/edit' ); print "</span>"; 
      } 
    ?></p>
    <a class="btn--orange cta_link_case_studies_page_gae" href="<?php print $fields['field_text_1']->content; ?>" style="margin:1em;">Learn more</a>
  </div>
  <div class="bottom-text"><?php print $topicTerm; ?></div>
</div>
<!-- end views-view-fields--case-studies-3-column-vendors--block.tpl.php template -->