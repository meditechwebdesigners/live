<?php // This template is set up to control the display of the 'Careers' content type 
  $url = $GLOBALS['base_url']; // grabs the site url
?>
<!-- start node--careers.tpl.php template -->

<?php
$node_id = $node->nid;
$special_pages = array(307, 310, 314, 3377, 4168, 4170, 4171, 4222);
// if current node ID (page) is in the above array, then use a special template...
if( in_array($node_id, $special_pages) === true ){
  include('careers--node-'.$node_id.'.php');
}
// otherwise use the following code...
else{
?>

<section class="container__centered">

  <div class="container__two-thirds">

    <h1 class="page__title js__seo-tool__title"><?php print $title; ?></h1>

    <div class="js__seo-tool__body-content">
      <?php print render($content['field_body']); ?>
    </div>

     <?php if($node_id == 308){ ?>
      <div>
        <div class="container__two-thirds">
        <h3>Perks</h3>
          <p>Being part of the MEDITECH community comes with its own special perks. Join us and enjoy:</p>
            <ul>
              <li>Volunteering with co-workers and finding many opportunities to give back</li>
              <li>Making social connections in our Workplace Communities, designed to support inclusion and belonging</li>
              <li>Embracing flexibility in your work schedule to ensure in-office time fosters meaningful interactions</li>
              <li>Getting creative with our hands-on Art Liaison Program, and participating in our annual staff art shows</li>
              <li>Visiting our onsite cafeterias for delicious (and discounted) breakfast, lunch, and snacks</li>
              <li>Skipping the gym to work out in our well-maintained 24/7 on-site fitness centers.</li>
            </ul>
        </div>
        <div class="container__one-third">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/careers/Icons--Perks-careers.svg">
        </div>
      </div>
      <?php } ?>
    
  </div>
  <!-- END container__two-thirds -->

  <!-- CAREERS SIDEBAR -->
  <aside class="container__one-third panel">
    <div class="sidebar__nav careers_sidebar_gae">
      <?php
            $massnBlock = module_invoke('menu', 'block_view', 'menu-careers-section-side-nav');
            print render($massnBlock['content']); 
          ?>
    </div>
  </aside>
  <!-- END CAREERS SIDEBAR -->

</section>
<!-- END SECTION container__centered -->

<?php // VIDEO SECTION =================================================================
      // get value from field_video to pass to View (if video exists)...
      $video = render($content['field_video']);
      if(!empty($video)){ // if the page has a video...
        print '<!-- VIDEO -->';
        print '<div id="video" class="js__seo-tool__body-content">';
        // remove apostrophes from titles to prevent View from breaking...
        $video_filtered = str_replace("&#039;", "'", $video);
        // adds 'video' Views block...
        print views_embed_view('video_boxed', 'block', $video_filtered);
        print '</div>';
        print '<!-- END VIDEO -->';
      }
    ?>


<?php if($node_id == 4172){ ?>
<script type='text/javascript' src='https://click.appcast.io/pixels/generic3-15067.js?ent=84'></script>
<?php } ?>


<?php // SEO tool for internal use...
      if(node_access('update',$node)){
        print '<!-- SEO Tool is added to this div -->';
        print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
      } 
    ?>

<?php 
}
?>
<!-- end node--careers.tpl.php template -->
