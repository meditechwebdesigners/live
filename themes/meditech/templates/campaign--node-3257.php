<!-- START campaign--node-3257.php -->

<?php // This template is set up to control the display of the campaign: Critical Care

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');
$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>


<style>
  .quote-text {
    width: 100%;
    padding: 1em 3em .5em 3em;
    font-style: italic;
    font-size: 1.4em;
    color: white;
    text-align: left;
  }

  .quote-text p {
    line-height: 1.35em;
  }

  .quote-element {
    padding: 0 2em;
  }

  .quote-person {
    margin-left: 50%;
    padding-top: 1em;
  }

  .quote-person p {
    line-height: 1.35em;
  }

  .angled-bg--solid-top-left {
    background-image: linear-gradient(240deg, transparent 48.9%, rgba(08, 126, 104, .88) 59%);
  }

</style>

<div class="js__seo-tool__body-content">

  <h1 style="display:none;" class="js__seo-tool__title">Critical Care</h1>

  <!-- Block 1 Hero Block-->
  <div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/two-nurses-in-hallway-blurred-01.jpg);background-color:#3e4545;">
    <div class="container__centered">
      <div class="container__one-half transparent-overlay--xp text--white text-shadow--black">
        <h1 class="js__seo-tool__title">True integration speaks to safer critical care.</h1>
        <p>Combine MEDITECH Expanse with our new Critical Care software and your entire organization will speak the same language. This smart, interactive solution completes the patient narrative for smoother handoffs and more effective transitions.</p>

        <div class="btn-holder--content__callout">
          <?php hubspot_button($cta_code, ""); ?>
        </div>

      </div>
    </div>
  </div>
  <!-- End of Block 1 -->



  <!-- Block 2 -->
  <!-- Start hidden modal box -->
  <div id="modal11" class="modal">
    <a class="close-modal" href="javascript:void(0)">&times;</a>
    <div class="modal-content">
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/XPC-flowsheet--large.jpg">
    </div>
  </div>
  <!-- End hidden modal box -->

  <div class="container bg--white" style="padding: 4em 0;">
    <div class="container__centered">
      <div class="container__one-half">
        <h2>Make every second count.</h2>
        <div>
          <p>Nurses live in our Critical Care flowsheet. Everything they need is right there — no more navigating from screen to screen.</p>
          <ul>
            <li>Document and view patient information simultaneously, on a single screen</li>
            <li>Pull values from monitors and ventilators into the flowsheet</li>
            <li>Capture cardiac waveforms and annotate directly on them</li>
            <li>Manage IVs and titrations from the flowsheet, without exiting to the MAR</li>
            <li>View and trend patient information such as results, reports, and physician documentation.</li>
          </ul>
        </div>
      </div>
      <div class="container__one-half center">
        <!-- Start modal trigger -->
        <div class="open-modal" data-target="modal11">
          <div class="tablet--white">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/XPC-flowsheet--small.jpg">
          </div><!-- Add modal trigger here -->
          <div class="mag-bg">
            <!-- Include if using image trigger -->
            <i class="mag-icon fas fa-search-plus"></i>
          </div>
        </div>
        <!-- End modal trigger -->
      </div>
    </div>
  </div>
  <!-- End of Block 2 -->


  <!-- Block 3 -->
  <!-- Start hidden modal box -->
  <div id="modal12" class="modal">
    <a class="close-modal" href="javascript:void(0)">&times;</a>
    <div class="modal-content">
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/XPC-icu-desktop--large.jpg">
    </div>
  </div>
  <!-- End hidden modal box -->

  <div class="container background--cover" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dark-grey-background-with-faint-lines.jpg); padding: 4em 0;">
    <div class="container__centered">

      <div class="container__one-half center">
        <!-- Start modal trigger -->
        <div class="open-modal" data-target="modal12">
          <div class="tablet--white">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/XPC-icu-desktop--small.jpg">
          </div><!-- Add modal trigger here -->
          <div class="mag-bg">
            <!-- Include if using image trigger -->
            <i class="mag-icon fas fa-search-plus"></i>
          </div>
        </div>
        <!-- End modal trigger -->
      </div>

      <div class="container__one-half text--white">
        <h2>Go with the flow sheet.</h2>
        <div>
          <p>Go ahead, document on our interactive flowsheets. They’re designed to handle the fluctuations of critical care delivery.</p>
          <ul>
            <li>Customize by location, by clinician, and on the fly</li>
            <li>Expand and contract menus with the click of a button</li>
            <li>Personalize workflows with widgets (e.g., Bedside Bulletin and Orders Notification) that appear above the flowsheet, minimizing navigation from screen to screen
            </li>
            <li>Customize snapshots wherever you need summary information.</li>
          </ul>
        </div>
      </div>

    </div>
  </div>
  <!-- End of Block 3 -->


  <!-- Block 4 -->
  <!-- Start hidden modal box -->
  <div id="modal13" class="modal">
    <a class="close-modal" href="javascript:void(0)">&times;</a>
    <div class="modal-content">
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/XPC-summary--large.jpg">
    </div>
  </div>
  <!-- End hidden modal box -->

  <div class="container bg--light-gray" style="padding: 4em 0;">
    <div class="container__centered">
      <div class="container__one-third">
        <h2>Nurses, get a head start.</h2>
        <div>
          <p>With a chart that instantly puts CCU/ICU nurses on the same page as their colleagues, your entire care team can move forward with the same information. True mobility means nurses are spending less time with screens, and more time providing hands-on, human care.</p>
        </div>
      </div>
      <div class="container__two-thirds center">
        <!-- Start modal trigger -->
        <div class="open-modal" data-target="modal13">
          <div class="tablet--white">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/XPC-summary--small.jpg">
          </div><!-- Add modal trigger here -->
          <div class="mag-bg">
            <!-- Include if using image trigger -->
            <i class="mag-icon fas fa-search-plus"></i>
          </div>
        </div>
        <!-- End modal trigger -->
      </div>
    </div>
  </div>
  <!-- End of Block 4 -->



  <!-- BLOCK 5 -->
  <div class="container">
    <div class="container__centered">
      <div class="container no-pad">
        <div class="container__two-thirds">
          <figure>
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/critical-care-monitors-talking.png" alt="hospital monitors communicating with MEDITECH software">
          </figure>
        </div>
        <div class="container__one-third">
          <h2>Now you’re talking.</h2>
          <div>
            <p>Save time and eliminate potential transcription errors. Our Critical Care solution “talks” to hemodynamic monitors, mechanical ventilators, and ECG machines,
              so nurses can pull in data from multiple monitors, all at once.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- BLOCK 5 -->


  <!-- Block 6 - video -->
  <div class="content__callout">
    <div class="content__callout__media">
      <div class="content__callout__image-wrapper">
        <div class="video js__video" data-video-id="143025811">
          <figure class="video__overlay">
            <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/video-overlay--clinically-crafted-for-substantial-savings.jpg" alt="Expanse Patient Care - Video Covershot">
          </figure>
          <a class="video__play-btn" href="https://vimeo.com/143025811?&autoplay=1"></a>
          <div class="video__container"></div>
        </div>
      </div>
    </div>
    <div class="content__callout__content">
      <div class="content__callout__body">
        <div class="content__callout__body__text">
          <h2 class="header-one">Clinically crafted for substantial savings.</h2>
          <p>MEDITECH collaborated with dozens of customers to develop an effective tool for intensive care delivery. Alicia Brubaker, RN, BSN, CCRN-CMC, of Valley Hospital (Ridgewood, NJ), shares how her experiences led to significant enhancements.</p>
          <p>Valley Hospital is realizing a substantial ROI with our Critical Care solution.</p>
          <ul>
            <li>$85,872 annual maintenance savings</li>
            <li>Projected $429,360 annual maintenance savings over five years</li>
            <li>0.25 FTE reduction for application support and 0.15 FTE for interface support</li>
            <li>80 percent improvement in capturing charges for “special orders."</li>
          </ul>

        </div>
      </div>
    </div>
  </div>
  <!-- End of Block 6 -->


  <!-- Block 7 -->

  <div class="container">
    <div class="container__centered center">
      <h2>It’s all here, in MEDITECH Expanse.</h2>
      <p>Critical Care is the perfect in-road to <a href="https://ehr.meditech.com/expanse">MEDITECH’s patient‐centric EHR.</a> Our groundbreaking technology is blazing trails in the new healthcare landscape.</p>
    </div>
  </div>
  <!-- End of Block 7 -->


  <?php if( user_is_logged_in() ){ ?>

  <div class="container background--cover no-pad" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Critical_care2.jpg);">
    <div class="angled-bg--solid-top-left" style="padding:2em 0;">
      <div class="container__centered">
        <div class="container__one-third text--white" style="padding: 2em;">
          <h2>It’s all here, in MEDITECH Expanse.</h2>
          <p>Critical Care is the perfect in-road to <a href="https://ehr.meditech.com/expanse">MEDITECH’s patient‐centric EHR.</a> Our groundbreaking technology is blazing trails in the new healthcare landscape.</p>
        </div>
        <div class="container__two-thirds">
          &nbsp;
        </div>
      </div>
    </div>
  </div>

  <?php } ?>


  <!-- Block 8 -->
  <div class="container bg--blue-gradient no-pad">

    <div class="container__centered" style="padding:3em;">
      <div class="quote-text">
        <p>"In the Critical Care flow sheet, providers can document with less steps than the current process. Not only that, they see the progress of the patient in real-time and the documentation sequence. Even though we’re starting with critical care, every unit in the hospital wants this functionality, and we’re going to roll it out to them — it’s a very powerful way to view and document."</p>
      </div>
      <div class="quote-element">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/quote_element_right--white.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/quote_element_right--white.png';this.onerror=null;" alt="quote graphic">
      </div>
      <div class="quote-person">
        <p>John Brown<br />
          CIO, Pacific Alliance Medical Center<br />
          (Los Angeles, CA)</p>
      </div>
    </div>

  </div>
  <!-- Close Block 8 -->


</div>
<!-- end js__seo-tool__body-content -->

<!-- LAST Block -->
<div class="container bg--light-gray" style="padding:5.5em;">
  <div class="container__centered center">

    <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
    <h2>
      <?php print $cta->field_header_1['und'][0]['value']; ?>
    </h2>
    <?php } ?>

    <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
    <div>
      <?php print $cta->field_long_text_1['und'][0]['value']; ?>
    </div>
    <?php } ?>

    <div class="center" style="margin-top:2em;">
      <?php hubspot_button($cta_code, "View Enrollment Details"); ?>
    </div>

    <div style="margin-top:1em;">
      <?php print $share_link_buttons; ?>
    </div>

  </div>
</div>
<!-- End of LAST Block -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>

<!-- END campaign--node-3145.php -->
