<!-- START campaign--node-3447.php -->

<?php // This template is set up to control the display of the campaign: CFO

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');
$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
	/* Universal */
	.shadow-box {
		color: #3e4545;
		min-height: 15em;
	}

	.fa-check-circle:before {
		content: "\f058";
	}

	.fa-ul li a {
		border-bottom: 1px solid #c8ced9;
	}

	.fa-ul li a:hover {
		color: #3e4545;
		border-bottom: 1px solid #3e4545;
	}

	/* Block 1 */
	.shadow-box--var {
		padding: 2em;
		color: #3e4545;
		background-color: #fff;
		border-radius: 7px;
		box-shadow: 0px 0px 20px 1px rgba(120, 120, 120, .2);
		max-width: 450px;
		position: absolute;
		right: 18px;
		top: 50%;
		transform: translateY(-50%);
	}

	.container--bg-img {
		max-width: 900px;
		border-radius: 7px;
	}

	.container--rel {
		position: relative;
	}

	/* Block 3 */
	.gl-container>div {
		padding: 0;
	}

	.fw-pad,
	.fw-pad2 {
		padding: 2em 5em;
		max-width: 1800px;
		margin: 0 auto;
	}

	.fw-pad .gl-container {
		background-color: #f0fafb;
		border-radius: 7px;
	}

	.box-pad1 {
		padding: 0 4em 0 0 !important;
	}

	.box-pad2 {
		padding: 0 0 0 4em !important;
	}

	.pad-adjust1 {
		padding: 3em 4em 2em 4em;
	}

	.pad-adjust2 {
		padding: 3em 4em 4em 4em;
	}

	.video {
		padding-bottom: 57%;
		overflow: visible;
		margin: 0;
		box-shadow: none;
		max-width: 100%;
	}

	.content__callout__image-wrapper {
		padding: 0;
	}

	.card__info {
		padding: 1em 2em;
	}

	@media all and (max-width: 1150px) {
		.fw-pad {
			padding: 0 1em;
		}

		.fw-pad2 {
			padding: 2em 1em;
		}

		.gl-container .container__one-half,
		.gl-container .container__one-third,
		.gl-container .container__two-thirds {
			width: 100%;
		}

		.flex-order--reverse {
			order: -1;
		}

		.box-pad1,
		.box-pad2 {
			padding: 1em 0 0 0 !important;
		}

		.pad-adjust1 {
			padding: 3em;
		}

		.pad-adjust2 {
			padding: 0 3em 3em 3em;
		}

		.gl-text-pad {
			padding: 3em !important;
		}
	}

	@media all and (max-width: 950px) {
		.shadow-box--var {
			max-width: 100%;
			position: relative;
			margin: 0 auto;
			width: 90%;
			right: 0;
		}

		.container--bg-img {
			max-width: 100%;
			border-radius: 7px;
		}

		.container--rel {
			margin-bottom: -8em;
		}
	}

	@media all and (max-width: 500px) {
		.shadow-box--var {
			transform: translateY(0%);
			width: 100%;
			margin-top: 2.35765%;
		}

		.container--rel {
			margin-bottom: 0;
		}
	}

</style>

<div class="js__seo-tool__body-content">

	<h1 style="display:none;" class="js__seo-tool__title">CFO</h1>

	<!-- Start Background Image -->
	<div class="background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/organic-shapes--bg.jpg);">

		<!-- Block 1 -->
		<div class="container">
			<div class="container__centered container--rel">
				<img class="container--bg-img" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/cfo-using-tablet-smiling.jpg" alt="Woman CFO smiling">
				<div class="shadow-box--var">
					<h2 class="header-one">Let’s Get Down to Business</h2>
					<p>As a CFO, you like to think in numbers and bottom lines. Your ability to make tough decisions — along with your ability to lead an organization focused on the welfare of both patients and staff — got you where you are today. MEDITECH’s integrated <a href="https://ehr.meditech.com/ehr-solutions/meditechs-revenue-cycle">Revenue Cycle</a>, <a href="https://ehr.meditech.com/ehr-solutions/ehr-value-and-sustainability">Financial</a>, and <a href="https://ehr.meditech.com/ehr-solutions/meditech-business-clinical-analytics">Analytics</a> solutions can help to organize your data, and make those decisions a little easier.</p>
				</div>
			</div>
		</div>
		<!-- Block 1 -->


		<!-- Block 2 UPDATED KLAS BLOCK -->
		<div class="container">
			<div class="container__centered center">
				<div class="auto-margins">
					<h2>Your revenue cycle is the bottom line</h2>
					<p style="margin-bottom: 2em;">Invest in the best. There's good reason our <a href="https://ehr.meditech.com/ehr-solutions/meditechs-revenue-cycle">Revenue Cycle solution</a> won Best in KLAS for Patient Accounting and Patient Management (Community Hospital) for three consecutive years. It will help you to monitor the financial well-being of your organization, while continually identifying areas to increase your bottom line.</p>
				</div>
				<div class="container__one-half shadow-box">
					<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--data-visualization.png" alt="Data visualization icon" style="height: 80px; margin-bottom:1em;">
					<p class="no-margin">Keep tabs on your Revenue Cycle with the Financial Status Desktop: A one-stop-shop to view receivables, compare payers, trend revenue, drill down on denials, and monitor the productivity of your account representatives.</p>
				</div>
				<div class="container__one-half shadow-box">
					<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--improve-revenue.png" alt="Improve revenue icon" style="height: 80px; margin-bottom:1em;">
					<p class="no-margin">Get fully reimbursed for the care you provide. Fight costly denials with embedded, front-end denial prevention workflows, a comprehensive appeals process, and the data to monitor trends.</p>
				</div>
			</div>
		</div>
		<!-- Block 2 UPDATED KLAS BLOCK -->


	</div><!-- End Background Image -->

	<!-- Block 3 -->
	<div class="container fw-pad">
		<div class="gl-container pad-adjust1">
			<div class="container__one-half box-pad1">
				<h2>Analyze this</h2>
				<p>The answers are in the data. Achieve your long-term financial goals by focusing on data and analytics. With powerful visualization tools you can drive organizational change and see measurable results. Trusted, transparent data will help uncover opportunities to improve revenue and reduce costs.</p>
				<p>Take a look at how <a href="https://info.meditech.com/summit-pacific-increases-reimbursement-clinic-volumes-with-meditech-analytics-solution-0">Summit Pacific</a> has increased clinic volumes and reimbursement using <a href="https://ehr.meditech.com/ehr-solutions/meditech-business-clinical-analytics">MEDITECH’s Business and Clinical Analytics (BCA) solution</a>.</p>
			</div>
			<div class="container__one-half background--cover flex-order--reverse" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/cfo-using-tablet-speaking-with-colleague.jpg); background-position: right; border-radius:7px; min-height:19em;"></div>
		</div>
		<div class="gl-container pad-adjust2">
			<div class="container__one-half">
				<div class="content__callout__image-wrapper">
					<div class="video js__video" data-video-id="457732917">
						<figure class="video__overlay">
							<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Innovator-in-Action-Newton-Medical-Center.jpg" style="border-radius: 7px;" alt="Newton Medical video thumbnail">
						</figure>
						<a class="video__play-btn video_gae" href="https://vimeo.com/457732917"></a>
						<div class="video__container">
						</div>
					</div>
				</div>
			</div>
			<div class="container__one-half box-pad2">
				<h2>See Our Real-world Results</h2>
				<p>How do you stay on top of what’s going on, even during a crisis? Senior administration officials and Incident Command teams <a href="https://blog.meditech.com/3-stories-on-using-business-and-clinical-analytics-to-increase-efficiency-and-patient-safety">are using BCA</a> as their go-to resource for monitoring the state of their facilities in real-time during the COVID-19 pandemic.</p>
				<ul class="fa-ul">
					<li><span class="fa-li"><i class="fas fa-check-circle text--meditech-green"></i></span><a href="https://ehr.meditech.com/news/newton-medical-center-uncovers-covid-19-insights-with-meditechs-bca-solution">Newton Medical Center</a> is tracking COVID-19 cases in real-time and monitoring critical supplies such as PPE through a burn rate calculator, thanks to BCA.</li>
					<li><span class="fa-li"><i class="fas fa-check-circle text--meditech-green"></i></span><a href="https://ehr.meditech.com/news/calverthealth-monitors-coronavirus-cases-using-meditechs-bca-solution">CalvertHealth</a> set up a BCA dashboard in just 24 hours to monitor COVID-19 cases and send data to state health department officials.</li>
					<li><span class="fa-li"><i class="fas fa-check-circle text--meditech-green"></i></span><a href="https://ehr.meditech.com/news/firelands-regional-health-system-monitors-covid-19-patients-with-meditechs-bca-solution">Firelands Regional Health System</a> endeavored to stay ahead of the surge by using analytics to identify cases, trends, and resources.</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- Block 3 -->

	<!-- Block 4 -->
	<div class="container">
		<div class="container__centered center">
			<div class="auto-margins">
				<h2>Keep an eye on your supplies</h2>
				<p style="margin-bottom: 2em;">It all adds up. Keep a watchful eye over your supply chain by integrating with your EHR, and benefit from improved patient safety, reduced costs, increased revenue, and enhanced decision support. Our centralized electronic system can keep you competitive and financially fit.</p>
			</div>
			<div class="container__one-third shadow-box">
				<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--real-time-tracking.png" alt="Real time tracking icon" style="height: 80px; margin-bottom:1em;">
				<p class="no-margin">Real-time tracking of inventory for just-in-time purchasing to save you money.</p>
			</div>
			<div class="container__one-third shadow-box">
				<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--caring-for-patients.png" alt="Caring for patients icon" style="height: 80px; margin-bottom:1em;">
				<p class="no-margin">Tight integration with surgical preference cards so nurses can spend less time on supply chain tasks (such as locating supplies) and more time caring for patients.</p>
			</div>
			<div class="container__one-third shadow-box">
				<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--critical-supplies.png" alt="Critical supplies icon" style="height: 80px; margin-bottom:1em;">
				<p class="no-margin">Analytics and reports to help reduce standing inventory, prevent wastage, and ensure critical supplies stay in stock.</p>
			</div>
		</div>
	</div>
	<!-- Block 4 -->

	<!-- Block 5 -->
	<div class="container fw-pad2">
		<div class="gl-container" style="border-radius: 7px;">
			<div class="container__two-thirds gl-text-pad" style="background-color: #f0fafb;">
				<h2>An easier way to measure your regulatory performance</h2>
				<p>The cost of monitoring your organization’s regulatory performance can be staggering. In 2017 the <a href="https://www.aha.org/guidesreports/2017-11-03-regulatory-overload-report" target="_blank">American Hospital Association</a> reported that hospitals, health systems, and post-acute care providers annually spend nearly $39 billion on administrative tasks alone related to regulatory compliance. </p>
				<p>Our Quality Vantage dashboards enable your staff to <a href="https://ehr.meditech.com/ehr-solutions/macra">monitor regulatory performance measures</a> well before the reporting period, to determine potential impacts on reimbursement. With drill-down capabilities and color-coding, it’s simple to see a straightforward, visual representation of at-risk and below-threshold metrics.</p>
			</div>
			<div class="container__one-third background--cover flex-order--reverse" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/young-businessman-addressing-regulatory-performance.jpg); min-height:17em;"></div>
		</div>
	</div>
	<div class="container fw-pad2">
		<div class="gl-container" style="border-radius: 7px;">
			<div class="container__one-third background--cover flex-order--reverse" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/female-doctor-discussing-sustainable-solutions.jpg); min-height:17em; background-position: top;"></div>
			<div class="container__two-thirds gl-text-pad" style="background-color: #f0fafb;">
				<h2>Consider the total cost of ownership</h2>
				<p>Your EHR should be a long-term, <a href="https://ehr.meditech.com/ehr-solutions/ehr-value-and-sustainability">sustainable solution</a> for your organization. Scalable, and with the ability to grow along with you. Eighty-five percent of our customers have been with MEDITECH for ten years or more. Choosing the right EHR now will save you in the future.</p>
				<p class="bold">Did you know...</p>
				<ul class="fa-ul">
					<li><span class="fa-li"><i class="fas fa-check-circle text--meditech-green"></i></span>We’ll identify third-party add-ons and services that can be eliminated with our integrated EHR solutions. What are you paying for your current EHR? What are you actually getting for the price?</li>
					<li><span class="fa-li"><i class="fas fa-check-circle text--meditech-green"></i></span>Ninety-eight percent of Expanse customers use our <a href="https://ehr.meditech.com/ehr-solutions/meditechs-revenue-cycle">Revenue Cycle</a> solution to streamline their financial workflow. Your reputation is on the line with every patient billing statement you send. Produce a single, accurate, patient-friendly statement for all visits across your network of care so patients can confidently pay their bills on time.</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- Block 5 -->

	<!-- Start Background Image -->
	<div class="background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/organic-shapes-2--bg.jpg);">

		<!-- Block 6 -->
		<div class="container">
			<div class="container__centered center">

				<h2>Go at risk with confidence</h2>
				<p style="margin-bottom: 2em;">Elevate your <a href="https://ehr.meditech.com/ehr-solutions/meditech-population-health">Population Health</a> initiatives with the tools and information you need to confidently take on value-based contracts.</p>

				<div class="card__wrapper">
					<div class="container__one-third card">
						<figure>
							<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/discussing-patient-registries-on-tablet.jpg" alt="Discussing patient registries on tablet with a group of colleagues">
						</figure>
						<div class="card__info">
							<h4>Patient registries</h4>
							<p>Your EHR data, now strengthened with <a href="https://ehr.meditech.com/news/meditech-continues-to-optimize-arcadiaio-collaboration-with-expanse-population-health">Arcadia-supplied</a> external EHR and claims data, drives an informed approach to care coordination.</p>
						</div>
					</div>

					<div class="container__one-third card">
						<figure>
							<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/talking-with-a-patient-about-care-gaps-ppe.jpg" alt="Talking with a patient about care gaps with ppe mask">
						</figure>
						<div class="card__info">
							<h4>Identifying care gaps</h4>
							<p>With customizable widgets, physicians can be prompted to close patient care gaps at the point of care in a smoother, simpler, paperless, and gap-free workflow.</p>
						</div>
					</div>

					<div class="container__one-third card">
						<figure>
							<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/woman-cfo-finding-patterns-in-population-analytics.jpg" alt="Female CFO finding trends in population analytics">
						</figure>
						<div class="card__info">
							<h4>Population analytics</h4>
							<p>Find trends across your patient population, including outcomes and utilization patterns, and target your programs to deliver high-quality care at the lowest price point.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Block 6 -->


		<!-- Block 7 - CTA Block -->
		<div class="container">
			<div class="container__centered" style="text-align: center;">

				<?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
				<h2>
					<?php print $cta->field_header_1['und'][0]['value']; ?>
				</h2>
				<?php } ?>

				<?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
				<div>
					<?php print $cta->field_long_text_1['und'][0]['value']; ?>
				</div>
				<?php } ?>

				<div class="center" style="margin-top:2em;">
					<?php hubspot_button($cta_code, "Sign up for the Ambulatory Practice Management Webinar"); ?>
				</div>

				<div style="margin-top:1em;">
					<?php print $share_link_buttons; ?>
				</div>

			</div>
		</div>
		<!-- End Block 7 -->

	</div><!-- End Background Image -->


	<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>

	<!-- END campaign--node-3447.php -->
