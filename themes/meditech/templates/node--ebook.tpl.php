<?php // This template is set up to control the display of the EBOOK content type

$url = $GLOBALS['base_url']; // grabs the site url

$button_1 = field_collection_data($node, 'field_fc_button_1');

include('inc-share-buttons.php');
?>

<!-- start node--ebook.tpl.php template --> 

<?php if($node->nid == 2775){ // display main eBook page... ?>
  
   
  <section class="container__centered">
  
	  <div class="container__two-thirds">

      <h1><?php print $title; ?></h1>
          
      <?php print views_embed_view('ebooks', 'block'); // adds 'ebooks' Views block... ?>

    </div><!-- END container__two-thirds -->
    
    <?php include('inc-news-sidebar.php'); ?>
    
  </section>
     
   
<?php } else { // display eBook individual pages... ?>  
          
  <style>
    .ebook-img { float: right; margin: 0 0em 1em 1.5em; }
    .ebook-img img { height: 100%; max-width: 300px; }
    .ebook-img .cta_button { width: 100% !important; }
    @media screen and (max-width: 450px) {
    .ebook-img, .ebook-img img { max-width: 100%; margin-left: 0; }
  }
  </style>        
          
  <section class="container__centered">
  
	  <div class="container__two-thirds">

      <h1>eBook: <?php print $title; ?></h1>
          
      <div>
        <div class="ebook-img">
          <?php print render($content['field_image']); ?>
          <?php 
          // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
          if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
            $button_1_code = trim($button_1->field_hubspot_embed_code_1['und'][0]['value']);
          ?>
            <div class="button--hubspot">
              <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
            </div>
          <?php 
          }else{ 
            if( $button_1->field_button_url_1['und'][0]['value'] != '' ){
          ?>
              <div style="clear:both;"><a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange button-vertical-adjustment campaign_button_gae"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a></div>
          <?php 
            }
          } 
          ?>
        </div>
        <?php print render($content['field_body']); ?>
      </div>
      
      <div style="margin-top:1em; clear:left;">
        <?php print $share_link_buttons; ?>
      </div>  

    </div><!-- END container__two-thirds -->
    
    <?php include('inc-news-sidebar.php'); ?>
    
  </section>
  
<?php } ?>   
<!-- end node--ebook.tpl.php template -->