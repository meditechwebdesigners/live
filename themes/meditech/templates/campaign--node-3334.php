<!-- START campaign--node-3334.php -->
<?php // This template is set up to control the display of the Long Term Partnerships campaign

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');
$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>


<div class="js__seo-tool__body-content">

    <style>
        .tranparent-with-p p {
            margin: 0;
        }

        .spacing {
            margin-bottom: 1em;
        }

        .video--shadow {
            border-radius: 6px;
            box-shadow: 13px 13px 40px rgba(0, 0, 0, 0.3);
        }

        @media (max-width: 50em) {
            .spacing {
                margin-bottom: 0;
            }
            .transparent-overlay {
                margin-bottom: 1em;
            }
            .video {
                max-width: 100%;
            }
        }

    </style>

    <!-- Block 1 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/customer-partnerships.jpg);">
        <div class="container__centered">
            <div class="container__one-half transparent-overlay text--white">
                <h1 class="text--white js__seo-tool__title">
                    Time after time, our customers transform with us.
                </h1>
                <p><em>84% of our customers have been with us for over a decade</em>. That kind of statistic doesn’t result from your typical EHR vendor relationship. Nor does it come from some fleeting fad. MEDITECH’s strong customer longevity is due to genuine long term partnerships with those who continue to trust bold innovation.</p>
                <p>We are especially proud of how these relationships have led to our customers’ success today. Here’s a sampling of their recent transformations.</p>

                <div class="btn-holder--content__callout">
                    <?php hubspot_button($cta_code, "Download The Innovators Booklet"); ?>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Block 1 -->

    <!-- Block 2 - Video Block -->
    <div class="content__callout border-none" style="background-color: #E6E9EE !important;">
        <div class="content__callout__media">
            <div class="content__callout__image-wrapper" style="padding:3em !important;">
                <div class="video js__video video--shadow" data-video-id="214024425">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--oppurtunity-to-grow.jpg" alt="Opportunity to grow video image">
                    </figure>
                    <a class="video__play-btn" href="https://vimeo.com/214024425"></a>
                    <div class="video__container"></div>
                </div>
            </div>
        </div>
        <div class="content__callout__content">
            <div class="content__callout__body">
                <div class="content__callout__body__text">
                    <h2>Opportunity to grow.</h2>
                    <p>Commonwealth Health Corporation originally became a MEDITECH customer in&nbsp;<strong>1984</strong>&nbsp;for their Bowling Green, KY, facility. Today, Emergency Department physician William Moss, MD, shares how a decades-long relationship with MEDITECH led to a confident choice to move forward with MEDITECH Expanse.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- End Block 2 -->

    <!-- Block 3 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dark-blurred-elderly-man-in-wheelchair-being-escorted-by-wife-nurse-and-doctor-holding-tablet.jpg);">
        <article class="container__centered text--white center auto-margins">
            <figure>
                <img class="quote__content__img" src="https://ehr.meditech.com/sites/all/themes/meditech/images/quote.svg" onerror="this.src='https://ehr.meditech.com/sites/all/themes/meditech/images/quote.png';this.onerror=null;" style="width: 150px;" alt="Quote bubble">
            </figure>
            <div class="text--large italic" style="font-size:1.4em; padding-top: .75em;">
                <p>“The MEDITECH system creates the opportunity for a shared patient record . . . It’s safer. It’s faster. It’s more effective. It’s better patient care.”</p>
            </div>
            <p class="no-margin--bottom text--large bold text--meditech-green">Jo-anne Marr, President and CEO</p>
            <p>Markham Stouffville Hospital (customer since 1988).</p>
            <br>
            <p>Markham Stouffville is currently partnering with Southlake Regional Health Centre and Stevenson Memorial Hospital, connecting all three facilities through a single MEDITECH EHR.</p>
        </article>
    </div>
    <!-- End Block 3 -->

    <!-- Block 4 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Nurse-Navigator-Program.jpg);">
        <div class="container__centered">
            <div class="container__two-thirds transparent-overlay text--white">
                <h2>
                    Proof positive.
                </h2>
                <p>With their Nurse Navigator Program, <a href="https://www.avera.org/locations/mckennan/" target="_blank">Avera McKennan Hospital &amp; University Center</a> showcased just what this customer since <strong>1989</strong> could do with their EHR.</p>
                <ul>
                    <li>$475,000 in annual savings</li>
                    <li>13.7% decrease in non-emergent ED visits</li>
                    <li>78% drop in ED visits by program participants</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- End Block 4 -->

    <!-- Block 5 - Video Block -->
    <div class="content__callout border-none" style="background-color: #E6E9EE !important;">
        <div class="content__callout__media">
            <div class="content__callout__image-wrapper" style="padding:3em !important;">
                <div class="video js__video video--shadow" data-video-id="215063769">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--at-your-service.jpg" alt="Opportunity to grow video image">
                    </figure>
                    <a class="video__play-btn" href="https://vimeo.com/215063769"></a>
                    <div class="video__container"></div>
                </div>
            </div>
        </div>
        <div class="content__callout__content">
            <div class="content__callout__body">
                <div class="content__callout__body__text">
                    <h2>At your service.</h2>
                    <p>Personal interactions have always been the cornerstone of MEDITECH’s partnerships with customers. For years, satisfied customers have been submitting “kudos” to individual staff members for going above and beyond in their customer service. Give this video a watch to see what our customers really think of us...</p>
                </div>
            </div>
        </div>
    </div>
    <!-- End Block 5 -->

    <!-- Block 6 -->
    <div class="container bg--white">
        <div class="container__centered">
            <figure class="container__one-fourth center">
                <img class="quote__content__img" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Alicia-Brubaker.png" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Alicia-Brubaker.png';this.onerror=null;" alt="Partnership Profile - Alicia Brubaker">

            </figure>
            <div class="container__three-fourths">
                <h2>
                    Shoulder to shoulder.
                </h2>
                <h4>
                    Partnership Profile
                </h4>
                <h4>
                    Alicia Brubaker, RN, BSN, CCRN-CMC
                </h4>
                <div>
                    <p>You couldn’t ask for a better example of MEDITECH’s customer partnerships than with Alicia Brubaker. Alicia is both a CCU nurse at <strong>20+ year</strong> customer Valley Hospital (Ridgewood, NJ) and a MEDITECH nurse liaison. She brings her clinical experience to our development team, and expertly helps us hone products that other nurses will eventually use. Read all about her special role as a MEDITECH collaborator <a href="https://ehr.meditech.com/news/meditech-develops-critical-relationship-with-valley-hospital-nurse">here</a>.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- End Block 6 -->

    <!-- Block 7 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/first-customer-in-ireland.jpg);">
        <div class="container__centered">

            <div class="page__title--center text--white">
                <h2>
                    First and foremost.
                </h2>
                <p>Galway Clinic is no stranger to leading the pack.</p>

                <div class="page__title__ribbon" style="background-color: #00BC6F;"></div>
            </div>

            <div class="container center no-pad--bottom">

                <div class="container__one-fourth text--white">
                    <div class="transparent-overlay">
                        <h1 class="text--large text--meditech-green" style="text-align:center;">2003</h1>
                        <p>First MEDITECH customer in Ireland</p>
                    </div>
                </div>

                <div class="container__one-fourth text--white">
                    <div class="transparent-overlay">
                        <h1 class="text--large text--meditech-green" style="text-align:center;">2004</h1>
                        <p>First independently funded hospital to be opened in Ireland in twenty years</p>
                    </div>
                </div>

                <div class="container__one-fourth text--white">
                    <div class="transparent-overlay">
                        <h1 class="text--large text--meditech-green" style="text-align:center;">2014</h1>
                        <p>First hospital in Ireland to achieve Stage 6 HIMSS designation</p>
                    </div>
                </div>

                <div class="container__one-fourth text--white">
                    <div class="transparent-overlay">
                        <h1 class="text--large text--meditech-green" style="text-align:center;">2017</h1>
                        <p>MEDITECH’s First Expanse customer in Europe</p>
                    </div>
                </div>

            </div>

            <div class="container center no-pad--top" style="margin-top: 2.35765%;">

                <div class="transparent-overlay text--white">
                    <h1 class="text--large text--meditech-green" style="text-align:center;">Now</h1>
                    <p>First guest on MEDITECH’s Thought Leader Podcast, featuring <a href="http://blog.meditech.com/thought-leader-podcast-series-big-data">Big Data</a>. Listen to Raphael Jaffrezic, CIO, in his podcast <a href="http://blog.meditech.com/thought-leader-podcast-series-big-data?hs_preview=XutsebxQ-4964070438">interview</a> about big data, organizational buy-in, and their impressive plans for Expanse.</p>
                </div>

            </div>

        </div>
    </div>
    <!-- End Block 7 -->

    <!-- Block 8 -->
    <div class="container bg--black-coconut">
        <div class="container__centered center">

            <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
            <h2>
                <?php print $cta->field_header_1['und'][0]['value']; ?>
            </h2>
            <?php } ?>

            <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
            <div>
                <?php print $cta->field_long_text_1['und'][0]['value']; ?>
            </div>
            <?php } ?>

            <div class="center" style="margin-top:2em;">
                <?php hubspot_button($cta_code, "Download The Innovators Booklet"); ?>
            </div>

            <div style="margin-top:1em;">
                <?php print $share_link_buttons; ?>
            </div>

        </div>
    </div>
    <!-- End Block 8 -->


</div>
<!-- end js__seo-tool__body-content -->


<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>

<!-- end node--campaign_long_term_partnerships.tpl.php template -->
