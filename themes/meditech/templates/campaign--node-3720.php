<!-- START campaign--node-3720.php -- Greenfield Tiers -->
<?php
$url = $GLOBALS['base_url']; // grabs the site url
  
$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<?php
if( !isset($_GET['mtid']) || $_GET['mtid'] != 'review' ){
  print '<script type="text/javascript">';
  print 'window.location.replace("https://ehr.meditech.com/not-meant-for-your-eyes");';
  print '</script>';
}
else{
?>

<style>
    .shadow-box--var {
        padding: 2em;
        color: #3e4545;
        background-color: #fff;
        border-radius: 7px;
        box-shadow: 0px 0px 20px 1px rgba(120, 120, 120, .2);
        max-width: 500px;
        position: absolute;
        right: 18px;
        top: 50%;
        transform: translateY(-50%);
    }

    .container--bg-img {
        max-width: 900px;
        border-radius: 7px;
    }

    .container--rel {
        position: relative;
    }

    @media all and (max-width: 950px) {
        .shadow-box--var {
            max-width: 100%;
            position: relative;
            margin: 0 auto;
            width: 90%;
            right: 0;
        }

        .container--bg-img {
            max-width: 100%;
            border-radius: 7px;
        }

        .container--rel {
            margin-bottom: -8em;
        }
    }

    @media all and (max-width: 500px) {
        .shadow-box--var {
            transform: translateY(0%);
            width: 100%;
            margin-top: 2.35765%;
        }

        .container--rel {
            margin-bottom: 0;
        }
    }
    
    .btn--orange {
          border: 4px solid #ff8300 !important;
    }

    .btn--outline {
            background-color: unset;
            color: #3E4545 !important;
            border-width: 4px !important;
        }
            
         .btn--orange:hover {
            -webkit-transition: all 600ms ease-in-out;
            }
    
    .icons img { width:40%; height:40%; margin-top:1.5em; }

</style>

<div class="js__seo-tool__body-content">


    <!-- Block 1 -->
    <div class="container container__centered">
            <h1><?php print $title; ?></h1>
            <p>Every developer registered within Greenfield will fall into one of the following categories: casual developers, certified developers, or co-developers.</p>
        </div>
    </div>
    <!-- Block 1 -->


                    
    <div class="container container__centered">
        <div class="card__wrapper">

            <div class="container__one-third card bg--emerald">
                <div class="card__info">
                   <h2>Casual developers</h2>
                    <p>Most developers will be casual developers because they will have tested within the Greenfield sandbox, but their solution may not be officially validated. It is expected that they will pursue promoting their app individually without engaging in joint marketing efforts with MEDITECH.</p>
                </div>
            </div>
            <div class="container__one-third card bg--dark-blue">
                <div class="card__info">
                    <h2>Certified developers</h2>
                    <p>Developers who have in-depth conversations about their solution with MEDITECH in order to pursue joint promotion and marketing are certified developers. This relationship between the developer and MEDITECH does not include shared revenue or contracting.</p>
                </div>
            </div>
            <div class="container__one-third card bg--purple">
                <div class="card__info">
                    <h2>Co-developers</h2>
                    <p>Developers who engage in official partnership solutions with MEDITECH to create white labeled solutions for co-development, joint marketing and contracting, and shared revenue.</p>
                </div>
            </div>
            
        </div>
    </div>



 


    <!-- START Buttons -->
    <div class="container container__centered" style="padding-bottom: 6em;">
        <div class="center">
            <a href="https://info.meditech.com/welcome-to-meditech-greenfield-rfi" class="btn--orange btn--outline">Register</a>&nbsp;&nbsp;&nbsp;<a href="https://greenfield.meditech.com/" class="btn--orange">Log In</a>
        </div>
    </div>
    <!-- START Buttons -->



    <?php // SEO tool for internal use...
  //if(node_access('update',$node)){
  //  print '<!-- SEO Tool is added to this div -->';
  //  print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  //} 
?>

    <!-- END campaign--node-3720.php -->
    <?php } ?>
