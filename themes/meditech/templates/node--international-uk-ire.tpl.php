<?php 
// This template is set up to control the display of the 'INTERNATIONAL - UK and IRELAND' content type 
$url = $GLOBALS['base_url']; // grabs the site url
$node_id = $node->nid;
?>
<!-- start node--international-uk-ire.tpl.php template -->

<?php
// set region variables for UK/IRE...
$home_page = 'meditech-uk-ireland';
$international_region = 'UK & Ireland';
$side_nav = 'menu-side-menu---uk-ireland';
$country_graphic = 'uk-ireland/uk-ire-graphic.png';
$sidebar_class = 'uk_ire_sidebar_gae';

$customer_link = 'https://www.meditech.com/supportbeta/';

// set hero variables based on node id...
// ignore if page does not use the hero...

switch($node_id){
  case 2384: // MEDITECH UK IRE home page
    $hero = 'yes-main';
    $hero_background_image = 'uk-ireland/london-modern-skyline.jpg'; 
    $text_class = '';
    break;
}

?>

<?php if( isset($hero) && $hero == 'yes-main' ){ // if hero belongs on page AND this is a "main" page, display the following... ?>
<style>
  .country-logo-text {
    padding-top: 3.5em;
    margin-bottom: 0;
    text-align: center;
    font-size: 1.5em;
  }

  @media all and (max-width: 1050px) {
    .country-logo-text {
      padding-top: 1em;
    }
  }

  @media all and (max-width: 900px) {
    .country-logo-text {
      padding-top: 0;
    }
  }

  .anniversary-logo {
    position: absolute;
    right: 50px;
    width: 250px;
    height: 300px;
    background-color: RGBA(255, 255, 255, 0.8);
    z-index: 2;
    top: -3em;
    border-radius: 0 0 48% 48%;
  }

  .anniversary-logo img {
    height: auto;
    padding: 1em .75em;
    position: relative;
    top: 2.5em;
  }

  @media (max-width: 800px) {
    .anniversary-logo {
      position: absolute;
      right: 50px;
      width: 165px;
      height: 200px;
      background-color: white;
      z-index: 2;
      top: -4.75em;
    }

    .anniversary-logo img {
      height: auto;
      padding: 1em;
      position: relative;
      top: 3em;
    }
  }

</style>
<!-- HERO section -->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/international/<?php print $hero_background_image; ?>); min-height:20em;">
  <div class="container__centered">
    <div class="container__one-half">&nbsp;</div>
    <div class="container__one-half container__centered" style="position:relative;">
     <div class="anniversary-logo">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/international/uk-ireland/UK30thlogo.png" alt="MEDITECH UK 30th Anniversary logo" width="300" height="300">
      </div>
    </div>
  </div>
</div>
<!-- End of HERO section -->
<?php } ?>


<?php if( isset($hero) && $hero == 'yes' ){ // if hero belongs on page AND this is NOT a "main" page, display the following... ?>
<!-- HERO section -->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/international/<?php print $hero_background_image; ?>);">
  <div class="container__centered">
    <h1 class="<?php print $text_class; ?>"><?php print $title; ?></h1>
  </div>
</div>
<!-- End of HERO section -->
<?php } ?>


<section class="container__centered">

  <div class="container__two-thirds">

    <?php if( isset($hero) && $hero == 'yes-main' ){  ?>
    <h1><?php print $title; ?></h1>
    <?php } ?>

    <?php
      // if image exists for node, then proceed to render DIV...
      if(!empty($content['field_main_image_international'])){
      ?>
    <figure class="news__article__img">
      <?php print render($content['field_main_image_international']); ?>
    </figure>
    <?php
      }
      ?>

    <?php if( !isset($hero) ){ // if no hero on page, display title here... ?>
    <h1 class="page__title js__seo-tool__title"><?php print $title; ?></h1>
    <?php } ?>


    <div class="js__seo-tool__body-content">

      <?php print render($content['field_body']); // main content if any ?>

      <?php
        // specific content for specific pages ======================================================================
        switch($node_id){
          
          case 2386: // UK IRE NEWS page ======================================================================
            print views_embed_view('news_united_kingdom_and_ireland', 'block'); // adds 'News - United Kingdom and Ireland' Views block... 
            break;
            
            
          case 2399: // UK IRE CONTACT page ========================================================================== 
            ?>
      <h2 class="page__title">Are you an existing customer?</h2>
      <p>If you're a MEDITECH customer in the UK or Ireland in need of assistance, please log in to our <a href="https://www.meditech.com/supportbeta/" target="_blank">Customer Service area</a> and enter a task, or call us anytime at one of our locations listed below.</p>

      <address class="container no-pad">
        <h2>Medical Information Technology United Kingdom PLC trading as MEDITECH UK</h2>
        <p>Registered in England and Wales<br />
          Registration Number: 02220263<br />
          VAT: 627 8674 94</p>
        <div class="container__one-third">
          <h3><i class="fas fa-map-marker-alt meditech-green"></i> Address</h3>
          <p>One Northumberland Avenue<br />
            London<br />
            WC2N 5BW</p>
        </div>
        <div class="container__one-third">
          <h3><i class="fas fa-phone meditech-green"></i> Call</h3>
          <p><a class="phone-number" href="tel:+ 44 (0) 207 872 5583">+ 44 (0) 207 872 5583</a></p>
        </div>
        <div class="container__one-third">
          <h3><i class="fas fa-fax meditech-green"></i> Fax</h3>
          <p>+44 (0) 207 117 1562</p>
        </div>
      </address>

      <p>Or send us an email through <a href="https://home.meditech.com/webforms/contact.asp?rcpt=salesint%7Cmeditech%7Ccom%20&rname=salesint">this form</a>.</p>

      <hr>

      <?php
            break;
            
            
          case 2669: // UK ON DEMAND WEBINARS page ==========================================================================
            print views_embed_view('on_demand_webinars_page_uk', 'block'); // adds 'On Demand Webinars page - UK' Views block
            break;
                      
            
        } // END node ID switch
        ?>


    </div><!-- End .js__seo-tool__body-content -->
    <?php // SEO tool for internal use...
        if(node_access('update',$node)){
          print '<!-- SEO Tool is added to this div -->';
          print '<div class="container no-pad--top js__seo-tool"></div>';
        } 
      ?>

  </div>

  <!-- RIGHT SIDE NAV MENU -->

  <style>
    .sidebar__nav ul.menu ul.menu {
      padding-left: 1.5em;
      margin-top: 0;
    }

    .sidebar__nav ul.menu ul.menu li {
      padding: .4em 0 .5em 0;
      font-size: .9em;
    }

    .sidebar__nav ul.menu ul.menu li.last {
      padding-bottom: 0;
    }

    @media (max-width: 800px) {
      .sidebar__nav ul.menu ul.menu li {
        font-size: 1em;
      }
    }

  </style>

  <aside class="container__one-third panel">

    <?php if($node_id != 2384){ // if not MEDITECH UK/IRE home page, display menu header... ?>
    <div>
      <div class="center">
        <h3><a href="<?php print $url; ?>/global/<?php print $home_page; ?>" title="Go to the <?php print $international_region; ?> home page">MEDITECH UK Serving the UK & Ireland</a></h3>
      </div>
    </div>
    <?php } ?>

    <div class="sidebar__nav <?php print $sidebar_class; ?>" style="float:left; width:100%;">
      <?php
            // adds appropriate international menu block...
            $international_menu = module_invoke('menu', 'block_view', $side_nav);
            print render($international_menu['content']); 
          ?>
    </div>

    <p style="text-align:center;"><a href="<?php print $customer_link; ?>" target="_blank" class="btn--orange uk_ire_customers_gae">MEDITECH <?php print $international_region; ?><br />Customer Service</a></p>




    <?php if( !empty( views_get_view_result('next_big_event_uk_ire', 'block') ) || !empty( views_get_view_result('upcoming_uk_ire_trade_shows', 'block') ) ){ ?>
    <div class="panel <?php print $sidebar_class; ?> no-margin--bottom">

      <?php print views_embed_view('next_big_event_uk_ire', 'block'); // adds 'Next Big US Event - International' Views block... ?>

      <?php if( !empty( views_get_view_result('upcoming_uk_ire_trade_shows', 'block') ) ){ ?>
      <h3>Upcoming Trade Shows & Conferences</h3>
      <?php print views_embed_view('upcoming_uk_ire_trade_shows', 'block'); // adds 'Upcoming UK IRE Trade Shows' Views block... ?>
      <?php } ?>

    </div>
    <?php } ?>

    <?php if($node_id == '2384'){ ?>
    <div style="padding-top:2em;">
      <a class="twitter-timeline" data-lang="en" data-width="360" data-height="600" data-dnt="true" data-theme="light" data-link-color="#ff610a" href="https://twitter.com/MEDITECH_UK">Tweets by MEDITECH UK/Ireland</a>
      <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
    </div>
    <?php } ?>

  </aside>

</section>

<!-- end node--international-uk-ire.tpl.php template -->
