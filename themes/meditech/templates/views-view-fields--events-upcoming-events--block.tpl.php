<?php // This template is for each row of the Views block:  UPCOMING EVENTS ....................... 

  // get node ID...
  $nid = $fields['nid']->content;
  $node = node_load($nid);

// check to see if the do not allow clicking check box was checked, if so then remove link...
if($fields['field_do_not_allow_clicking']->content){
  $not_clickable = field_view_field('node', $node, 'field_do_not_allow_clicking'); 
  if($not_clickable["#items"][0]["value"] === 1){
    $link_html_1 = '';
    $link_html_2 = '';
  }
}
else{
  $link_html_1 = '<a class="events_main_link_gae" href="'.$fields['path']->content.'">';
  $link_html_2 = '</a>';
}
?>
<!-- start views-view-fields--events-upcoming-events--block.tpl.php template -->
<figure class="container no-pad">
  <div class="container__one-third">
    <?php
      // get crop orientation option to set proper class name for image...
      if($fields['field_image_1_crop_orientation']->content == 'Show Left Side of Image'){
        $crop = 'crop-left';
      }
      elseif($fields['field_image_1_crop_orientation']->content == 'Show Right Side of Image'){
        $crop = 'crop-right';
      }
      else{
        $crop = 'crop-center';
      }
    ?>
    <div class="square-img-cropper <?php print $crop; ?>">
      <?php print $link_html_1.$fields['field_event_image']->content.$link_html_2; ?>
    </div>
  </div>
  <figcaption class="container__two-thirds">
    <h3 class="header-four no-margin"><?php print $link_html_1.$fields['title']->content.$link_html_2; ?></h3>
    
    <?php
    $dateTBD = field_get_items('node', $node, 'field_date_tbd');
    // if Date TBD is not checked...
    if($dateTBD[0]['value'] != 1){
    ?>
      <h5 class="no-margin--bottom"><?php print $fields['field_event_date']->content; ?></h5>
    <?php
    }
    else{
      print '<h5 class="no-margin--bottom">Date: Coming Soon</h5>';
    }
    ?>
    
     <h5 class="no-margin--top">
      <?php 
        print $fields['field_location_city']->content; 
        if($fields['field_location_state']->content){
          $stateTerms = field_view_field('node', $node, 'field_location_state'); 
          if(!empty($stateTerms)){
            foreach($stateTerms["#items"] as $sTerm){
              $eventState = ', '.$sTerm["taxonomy_term"]->description;
            }
          }
          else{
            $eventState = '';
          }
          print $eventState; 
        }
        if($fields['field_location_country']->content){
          if($fields['field_location_country']->content != 'United States' && $fields['field_location_country']->content != 'Canada'){
            print ', '.$fields['field_location_country']->content;
          }
        }
      ?>
    </h5>
    <p><?php print $fields['field_summary']->content; ?></p>
  </figcaption>
</figure>

<?php 
if( user_is_logged_in() ){ 
  print '<p style="text-align:right; font-size:12px;"><a href="https://ehr.meditech.com/node/'.$nid.'/edit">Edit this content</a></p>';
}
?>
<hr>
<!-- end views-view-fields--events-upcoming-events--block.tpl.php template -->