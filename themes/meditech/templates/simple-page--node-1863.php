<!-- start simple-page--node-1863.tpl.php template -- Case Studies page -->

<style>
  .container__thirds {
    float: left;
    display: block;
    margin: 2% 1.5%;
    padding: 2%;
    width: 30%;
    height: 300px;
    text-align: center;
    overflow: hidden;
    -moz-border-radius: 10px;
    -webkit-border-radius: 10px;
    -webkit-font-smoothing: antialiased;
    border-radius: 10px;
    position: relative;
    background-position: bottom left;
    background-repeat: no-repeat;
    background-size: 125px 125px;
  }

  .container__thirds h3 {
    font-size: 1em;
  }

  fieldset#topics {
    padding: 1em;
  }

  .topic {
    width: auto;
    display: block;
    margin-right: 1%;
  }

  .topic:nth-child(3n) {
    margin-right: 0;
  }

  @media all and (max-width: 65em) {
    .container__thirds {
      float: left;
      display: block;
      margin: 2% 1.5%;
      padding: 2%;
      width: 30%;
      height: 400px;
      text-align: center;
      overflow: hidden;
    }
  }

  @media all and (max-width: 50em) {
    .topic {
      display: block;
      width: 100%;
      margin-right: 0;
    }

    .container__thirds {
      float: left;
      display: block;
      margin: 2% 1.5%;
      padding: 2%;
      width: 94%;
      height: 200px;
      text-align: center;
      overflow: hidden;
    }
  }

  .container__thirds.bca {
    border-color: #087E68;
    /*background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/news/case-studies-icon--ed-specialty--colored.png);*/
  }

  .container__thirds.ce {
    border-color: #D7D1C4;
    /*background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/news/case-studies-icon--fiscal-leader--colored.png);*/
  }

  .container__thirds.fr {
    border-color: #3E4545;
    /*background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/news/case-studies-icon--quality-safety--colored.png);*/
  }

  .container__thirds.ia {
    border-color: #FF610A;
    /*background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/news/case-studies-icon--interoperability--colored.png);*/
  }

  .container__thirds.int {
    border-color: #A73F06;
  }

  .container__thirds.pee {
    border-color: #0075A8;
    /*background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/news/case-studies-icon--physician-nurse--colored.png);*/
  }

  .container__thirds.ph {
    border-color: #4C0E8B;
    /*background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/news/case-studies-icon--pop-health--colored.png);*/
  }

  .container__thirds.qo {
    border-color: #00BC6F;
  }

  .container__thirds.pacs {
    border-color: #8B0303;
  }

  .container__thirds.iqs {
    border-color: #BA5DA1;
  }

  .container__thirds .topic-text {
    font-size: .9em;
    color: #999;
    line-height: 1.25em;
    margin-bottom: 0.75em;
  }

</style>


<!-- Block 1 -->
<div class="container" style="padding-top:1em; padding-bottom:0;">
  <div class="container__centered">
    <h1 class="js__seo-tool__title"><?php print $title; ?></h1>
    <div class="container__one-third">
      <fieldset id="topics">
        <legend>Filter by Topic:</legend>
        <?php print views_embed_view('case_study_topics', 'block'); // adds 'case study topics' Views block... ?>
      </fieldset>
    </div>
    <div class="container__two-thirds content--pad-left">
      <div>
        <div class="container__one-half">
          <p class="bold"><span class="text--large">Innovators:</span><br>MEDITECH Customers In Action</p>
          <p>Download our booklet of case study summaries and success stories to find out how customers are improving outcomes.</p>
          <div class="center" style="margin-top:2em;">
            <?php hubspot_button('5230d784-296e-47df-96e2-88aab7a25cab', "Download The Innovators Booklet"); ?>
          </div>
        </div>
        <div class="container__one-half">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/innovators-booklet-cover--2022.jpg" alt="Innovator's Booklet thumbnail" class="shadow-box" style="width:250px; height:auto; padding:0;">
        </div>
      </div>
      <p>Download individual success stories and case studies below.</p>
    </div>
  </div>
</div>
<!-- END Block 1 -->


<section class="container__centered">

  <div class="js__seo-tool__body-content">
    <?php print views_embed_view('case_studies_3_column', 'block'); // adds 'Case Studies - 3-Column' Views block... ?>
  </div>

  <div class="container auto-margins" style="clear:both;">
    <p>Read our <a href="https://ehr.meditech.com/collaborative-solution-case-studies">collaborative solution case studies</a> to learn how MEDITECH works with the industry's leading developers, consultants, and system integrators to explore new functionality and optimized workflows for our EHR.</p>
  </div>

</section>



<script>
  // table filter search function...
  // When document is ready: this gets fired before body onload...
  var $jq = jQuery.noConflict();

  $jq(document).ready(function() {
    // FILTER FUNCTION...

    // filter by checkbox selections...
    $jq("fieldset#topics input").click(function() {
      // get checked values using functions below...
      var selectedTopics = getTopicValues();

      // start fresh every time by displaying all DIVs first...
      $jq("div.container__thirds").show();

      if (selectedTopics && selectedTopics.length > 0) {
        // hide all DIVs...
        $jq("div.container__thirds").hide();

        for (var t = 0; t < selectedTopics.length; t++) {

          $jq("div.container__thirds").each(function() {
            var classList = (this).className.split(/\s+/);
            for (var i = 0; i < classList.length; i++) {
              if (classList[i] == selectedTopics[t]) {
                $jq(this).show();
              }
            }
          });

        }

      }
    });

    function getTopicValues() {
      // create an array to hold the values...
      var topics = [];
      // look for all location check boxes that are checked and add value to array...
      $jq("fieldset#topics input:checkbox:checked").each(function() {
        topics.push($jq(this).val());
      });
      return topics;
    }

    $jq("div.container__thirds").each(function() {
      // get height of container (could be different by device)...
      var containerH = $jq(this).height();
      // get height of inner content...
      var contentH = $jq(this).find("div.container__content").height();
      // get height difference...
      var hDiff = containerH - contentH;
      // set top and bottom margins of inner DIV...
      var newMargin = (hDiff / 2).toFixed(2);
      console.log(containerH + ' | ' + contentH + ' | ' + hDiff + ' | ' + newMargin);
      $jq("div.container__content", this).css('margin', newMargin + 'px 0');
    });

  });

</script>

<!-- end simple-page--node-1863.tpl.php template -->
