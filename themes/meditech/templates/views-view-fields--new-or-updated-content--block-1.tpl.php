<!-- start views-view-fields--new-or-updated-content--block-1.tpl.php template -->
<?php 
// This template is for each row of the Views block: NEW-OR-UPDATED-CONTENT--PAGE ....................... 
$websiteURL = $GLOBALS['base_url']; // grabs the site url
?>
<div>

  <h2 style="margin-bottom:0;"><a href="<?php print $websiteURL.$fields['path']->content; ?>"><?php print $fields['title']->content; ?></a></h2>
  <div class="inline__text__wrapper">
    <p><span class="snippet__card__text--callout"><strong>Type of Content:</strong> <?php print $fields['type']->content; ?><br />
    <strong>Published or Updated on:</strong> <?php print $fields['published_at']->content; ?></span></p>
  </div>
  
  <hr>

</div>
<!-- end views-view-fields--new-or-updated-content--block-1.tpl.php template -->