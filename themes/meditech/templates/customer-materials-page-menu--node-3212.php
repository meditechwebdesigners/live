<!-- start customer-materials-page-menu--node-3212.php Functionality Briefs template -->
<?php
$url = $GLOBALS['base_url']; // grabs the site url  

$content_sections = multi_field_collection_data_unrestricted($node, 'field_fc_head_ltext_1');
$buttons = multi_field_collection_data_unrestricted($node, 'field_fc_button_unl_1');
$side_menu = multi_field_collection_data_unrestricted($node, 'field_fc_text_link_unl_1');
?>

<style> 
  a.anchor { display: block; position: relative; top: -195px; visibility: hidden;	} 
  .transparent-overlay { margin-bottom: 2em; }
  .transparent-overlay > div.center { color:#ffffff; padding-bottom:.5em; }
</style>

<!-- Hero -->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/public-materials/busy-hospital-hallway.jpg);">
  <div class="container__centered">
    <h1 class="text--white text-shadow--black"><?php print $title; ?></h1>
    <?php if( !empty($content['field_sub_header_1']) ){ ?>
      <h2 class="text--white"><?php print render($content['field_sub_header_1']); ?></h2>
    <?php } ?>
  </div>
</div>
<!-- End of Hero -->

<!-- Main Content -->
<div class="container__centered" style="padding-top:2em; margin-bottom:1em;">

  <div class="container__two-thirds">
    <div>
      <?php
      if( !empty($content_sections[0]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[0]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      ?>
      <img style="float:right; width:50%; margin:0 0 1em 1em;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-WebAmb--homescreen_small.png" alt="Meditech Web Ambulatory screenshot on a tablet">
      <?php print $content_sections[0]->field_long_text_1['und'][0]['value']; ?>
    </div>
  </div>
  	
  <aside class="container__one-third panel">
    <?php
    if( isset($side_menu) && !empty($side_menu) ){
      print '<div class="sidebar__nav solutions_sidebar_gae">';
      print '<ul class="menu">';
      $number_of_menu_links = count($side_menu);
      for($ml=0; $ml<$number_of_menu_links; $ml++){
        print '<li><a href="';
        print $side_menu[$ml]->field_link_url_1['und'][0]['value']; 
        print '">';
        print $side_menu[$ml]->field_link_text_1['und'][0]['value']; 
        print '</a></li>';
      }
      print '</ul>';
      print '</div>';
    }
    ?>
  </aside>

</div> <!-- END container__centered -->


<div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/public-materials/green-medical-pattern.png); border-bottom: 8px solid #36B673;">
  <div class="container__centered">
      
    <div class="container__one-third text--white">
     
      <div class="transparent-overlay">
        <div class="center"><i class="far fa-hospital" aria-hidden="true" style="font-size:2em;"></i></div>
        <a class="anchor" id="CareCoordination"></a>
        <h3 class="center"><?php print $content_sections[1]->field_header_1['und'][0]['value']; ?></h3>
        <div>
          <?php print $content_sections[1]->field_long_text_1['und'][0]['value']; ?>
        </div>
      </div>
      
      <div class="transparent-overlay">
        <div class="center"><i class="fas fa-home" aria-hidden="true" style="font-size:2em;"></i></div>
        <a class="anchor" id="PostAcuteCare"></a>
        <h3 class="center"><?php print $content_sections[2]->field_header_1['und'][0]['value']; ?></h3>
        <div>
          <?php print $content_sections[2]->field_long_text_1['und'][0]['value']; ?>
        </div>
      </div>
      
    </div> <!-- END first column -->
    
    <div class="container__one-third text--white">
     
      <div class="transparent-overlay">
        <div class="center"><i class="fas fa-dollar-sign" aria-hidden="true" style="font-size:2em;"></i></div>
        <a class="anchor" id="PatientAccessandRevenueCycle"></a>
        <h3 class="center"><?php print $content_sections[3]->field_header_1['und'][0]['value']; ?></h3>
        <div>
          <?php print $content_sections[3]->field_long_text_1['und'][0]['value']; ?>
        </div>
      </div>
      
      <div class="transparent-overlay">
        <div class="center"><i class="fas fa-stethoscope" aria-hidden="true" style="font-size:2em;"></i></div>
        <a class="anchor" id="DiagnosticServices"></a>
        <h3 class="center"><?php print $content_sections[4]->field_header_1['und'][0]['value']; ?></h3>
        <div>
          <?php print $content_sections[4]->field_long_text_1['und'][0]['value']; ?>
        </div>
      </div>
      
      <div class="transparent-overlay">
        <div class="center"><i class="fas fa-users" aria-hidden="true" style="font-size:2em;"></i></div>
        <a class="anchor" id="PatientEngagement"></a>
        <h3 class="center"><?php print $content_sections[5]->field_header_1['und'][0]['value']; ?></h3>
        <div>
          <?php print $content_sections[5]->field_long_text_1['und'][0]['value']; ?>
        </div>
      </div>
      <div class="transparent-overlay">
        <div class="center"><i class="fab fa-connectdevelop" aria-hidden="true" style="font-size:2em;"></i></div>
        <a class="anchor" id="Interoperability"></a>
        <h3 class="center"><?php print $content_sections[9]->field_header_1['und'][0]['value']; ?></h3>
        <div>
          <?php print $content_sections[9]->field_long_text_1['und'][0]['value']; ?>
        </div>
      </div>
      
    </div> <!-- END second column -->
    
    <div class="container__one-third text--white">
     
      <div class="transparent-overlay">
        <div class="center"><i class="fas fa-user-md" aria-hidden="true" style="font-size:2em;"></i></div>
        <a class="anchor" id="PhysicianPractices"></a>
        <h3 class="center"><?php print $content_sections[6]->field_header_1['und'][0]['value']; ?></h3>
        <div>
          <?php print $content_sections[6]->field_long_text_1['und'][0]['value']; ?>
        </div>
      </div>
      
      <div class="transparent-overlay">
        <div class="center"><i class="fas fa-balance-scale" aria-hidden="true" style="font-size:2em;"></i></div>
        <a class="anchor" id="FinancialandDecisionSupport"></a>
        <h3 class="center"><?php print $content_sections[7]->field_header_1['und'][0]['value']; ?></h3>
        <div>
          <?php print $content_sections[7]->field_long_text_1['und'][0]['value']; ?>
        </div>
      </div>
      
      <div class="transparent-overlay">
        <div class="center"><i class="fas fa-spa" aria-hidden="true" style="font-size:2em;"></i></div>
        <a class="anchor" id="CommunityWellness"></a>
        <h3 class="center"><?php print $content_sections[8]->field_header_1['und'][0]['value']; ?></h3>
        <div>
          <?php print $content_sections[8]->field_long_text_1['und'][0]['value']; ?>
        </div>
      </div>
      
    </div> <!-- END third column -->
  
  </div>
</div>
<!-- end customer-materials-page-menu--node-3212.php Functionality Briefs template -->