<?php // This template is set up to control the display of the SALES PAGES content type *********
$url = $GLOBALS['base_url']; // grabs the site url
?>
<!-- start node--sales-pages.tpl.php template -->

<div class="container container__centered">

    <h1 class="page__title"><?php print $title; ?></h1>
    
    <?php 
    if(!empty($content['field_body'])){
      print render($content['field_body']); // main article content 
    }
    ?>

    <div>

        <div class="container__one-half">

            <h2>Business Operations Demonstrations</h2>

            <ul>
                <?php
        $bod = array(
            array('Business and Clinical Analytics', '398934433'),
            array('Population Health', '429612196'),
            array('General Ledger/Fixed Assets', '351621516'),
            array('Staff Management', '406305863'),
            array('Supply Chain Management', '405456156'),
            array('Payroll', '438167285'),
            array('Cost Accounting', '438166497'),
            array('Budgeting and Forecasting', '524911627')
            );

        for($n=0; $n < count($bod); $n++){
            print '<li><a href="javascript:void(0)" class="open-modal" data-target="modal'.($n+100).'">'.$bod[$n][0].'</a> </li>';
            print '<div class="modal" id="modal'.($n+100).'"><a class="close-modal" href="javascript:void(0)">×</a>';
            print '<div class="modal-content modal-content--vid"><iframe src="https://player.vimeo.com/video/'.$bod[$n][1].'" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe></div>';
            print '</div>';
        }
        ?>
            </ul>

            <h2>Care Coordination Demonstrations</h2>

            <ul><?php
        $ccd = array(
            array('Expanse Nurse Walkthrough', '474710179'),
            array('Care Across the Continuum', '646161382'),
            array('Monitoring Risk and Achieving Quality Outcomes (Surv)', '648738843'),
            array('Acute Care Nursing Experience (Includes CC)', '577627061'),
            array('Emergency Department for Non-Physicians', '408849979'),
            array('Perinatal (tag team with PEX)', '410839374'),
            array('Labor &amp; Delivery', '648715607'),
            array('Critical Care', '443364068'),
            array('Point of Care', '405043879')
            );

        for($n=0; $n < count($ccd); $n++){
            print '<li><a href="javascript:void(0)" class="open-modal" data-target="modal'.($n+200).'">'.$ccd[$n][0].'</a> </li>';
            print '<div class="modal" id="modal'.($n+200).'"><a class="close-modal" href="javascript:void(0)">×</a>';
            print '<div class="modal-content modal-content--vid"><iframe src="https://player.vimeo.com/video/'.$ccd[$n][1].'" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe></div>';
            print '</div>';
        }
        ?>
            </ul>

            <h2>Clinical Specialties Demonstrations</h2>

            <ul><?php
        $csd = array(
        array('Oncology Management', '646003123'),
        array('Pharmacy &amp; Medication Management', '591683186'),
        array('Surgical Services', '423217953')
            );

        for($n=0; $n < count($csd); $n++){
            print '<li><a href="javascript:void(0)" class="open-modal" data-target="modal'.($n+300).'">'.$csd[$n][0].'</a> </li>';
            print '<div class="modal" id="modal'.($n+300).'"><a class="close-modal" href="javascript:void(0)">×</a>';
            print '<div class="modal-content modal-content--vid"><iframe src="https://player.vimeo.com/video/'.$csd[$n][1].'" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe></div>';
            print '</div>';
        }
        ?>
            </ul>

            <h2>Diagnostic Services Demonstrations</h2>

            <ul><?php
        $dsd = array(
        array('LIMS Overview (high level all LIS)', '412805560'),
        array('General Laboratory', '412858407'),
        array('Imaging and Documentation Management', '412938013'),
        array('Infection Control', '696609838'),
        array('Microbiology', '413566864'),
        array('Outreach Laboratory', '412743964'),
        array('Anatomical Pathology/Cytology', '691807905'),
        array('Transfusion Services', '413127759')
            );

        for($n=0; $n < count($dsd); $n++){
            print '<li><a href="javascript:void(0)" class="open-modal" data-target="modal'.($n+400).'">'.$dsd[$n][0].'</a> </li>';
            print '<div class="modal" id="modal'.($n+400).'"><a class="close-modal" href="javascript:void(0)">×</a>';
            print '<div class="modal-content modal-content--vid"><iframe src="https://player.vimeo.com/video/'.$dsd[$n][1].'" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe></div>';
            print '</div>';
        }
        ?>
            </ul>

            <h2>Extended Care Demonstrations</h2>

            <ul><?php
        $ecd = array(
        array('Rehab Therapies (PT, OT, Speech)', '422970128'),
        array('Respiratory Therapies', '426251902'),
        array('Nursing Solutions for Inpatient Rehabilitation Facilities', '454736849'),
        array('Nursing Solutions for Long Term Care Hospitals', '458997010'),
        array('Solutions for Nurses providing Inpatient BH', '406068916'),
        array('Nursing Solutions for Skilled Nursing Facilities', '405932622'),
        array('Dietary', '576777842')
            );

        for($n=0; $n < count($ecd); $n++){
            print '<li><a href="javascript:void(0)" class="open-modal" data-target="modal'.($n+500).'">'.$ecd[$n][0].'</a> </li>';
            print '<div class="modal" id="modal'.($n+500).'"><a class="close-modal" href="javascript:void(0)">×</a>';
            print '<div class="modal-content modal-content--vid"><iframe src="https://player.vimeo.com/video/'.$ecd[$n][1].'" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe></div>';
            print '</div>';
        }
        ?>
            </ul>

            <h2>Home Care Demonstrations</h2>

            <ul><?php
        $hcd = array(
        array('Home Health Coordination of Care', '526280448'),
        array('Hospice Integration', '406241558'),
        array('Home Health Integration', '405996008'),
        array('Home Care Mobile', '515738967')
            );

        for($n=0; $n < count($hcd); $n++){
            print '<li><a href="javascript:void(0)" class="open-modal" data-target="modal'.($n+600).'">'.$hcd[$n][0].'</a> </li>';
            print '<div class="modal" id="modal'.($n+600).'"><a class="close-modal" href="javascript:void(0)">×</a>';
            print '<div class="modal-content modal-content--vid"><iframe src="https://player.vimeo.com/video/'.$hcd[$n][1].'" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe></div>';
            print '</div>';
        }
        ?>
            </ul>

        </div>

        <div class="container__one-half">

            <h2>Patient Access Demonstrations</h2>

            <ul><?php
        $pad = array(
        array('Patient Engagement', '400644290'),
        array('Complete Health Record Management', '405907958'),
        array('Long Term Care Admissions/Registration', '328204207'),
        array('Patient and Consumer Health Portal', '442367708'),
        array('Universal Patient Access', '407139092'),
        array('Transitions of Care and Case Management', '496978979'),
        array('Virtual Visits', '407133052'),
        array('Expanse Patient Connect', '561801470'),
        array('Expanse Patient Security', '686266726')
            );

        for($n=0; $n < count($pad); $n++){
            print '<li><a href="javascript:void(0)" class="open-modal" data-target="modal'.($n+700).'">'.$pad[$n][0].'</a> </li>';
            print '<div class="modal" id="modal'.($n+700).'"><a class="close-modal" href="javascript:void(0)">×</a>';
            print '<div class="modal-content modal-content--vid"><iframe src="https://player.vimeo.com/video/'.$pad[$n][1].'" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe></div>';
            print '</div>';
        }
        ?>
            </ul>

            <h2>Physician Experience Demonstrations</h2>

            <ul><?php
        $ped = array(
        array('Expanse Physician Walkthrough', '574883744'),
        array('Perinatal (tag team with CC)', '410839374'),
        array('Ambulatory for Physicians', '574881649'),
        array('Ambulatory for Behaviorial Health', '408213371'),
        array('Ambulatory for Nurses', '403269040'),
        array('Inpatient Physicians', '574881498'),
        array('OB (Physician stand-alone)', '383344403'),
        array('ED Physicians', '574881112'),
        array('Virtual Assistant (Inpatient - Recording only)', '672105534'),
        array('Expanse Now (Recording only)', '427348586'),
        array('Efficiency Dashboard (Recording Only)', '437127884'),
        array('Virtual Care', '473148782'),
        array('Expanse Traverse', '583359274'),
        array('High Availibility Snapshot', '474683079'),
        array('Improving the Healthcare Experience with MT Cloud Offerings', '587927858'),
        array('Dr. Fearing\'s Decoding Genomics Overivew', '658675130'),
        array('Expanse Cam', '670343019')
            );

        for($n=0; $n < count($ped); $n++){
            print '<li><a href="javascript:void(0)" class="open-modal" data-target="modal'.($n+800).'">'.$ped[$n][0].'</a> </li>';
            print '<div class="modal" id="modal'.($n+800).'"><a class="close-modal" href="javascript:void(0)">×</a>';
            print '<div class="modal-content modal-content--vid"><iframe src="https://player.vimeo.com/video/'.$ped[$n][1].'" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe></div>';
            print '</div>';
        }
        ?>
            </ul>

            <h2>Revenue Cycle Management Demonstrations</h2>

            <ul><?php
        $rcmd = array(
        array('Maximize your Revenue Cycle with MEDITECH', '398837311'),
        array('Ambulatory Front Office Management', '405985779'),
        array('Long Term Care Billing', '405958822'),
        array('Patient Accounting', '406248657'),
        array('Practice Management', '398524694'),
        array('Revenue Workflow Management', '405831569')
            );

        for($n=0; $n < count($rcmd); $n++){
            print '<li><a href="javascript:void(0)" class="open-modal" data-target="modal'.($n+900).'">'.$rcmd[$n][0].'</a> </li>';
            print '<div class="modal" id="modal'.($n+900).'"><a class="close-modal" href="javascript:void(0)">×</a>';
            print '<div class="modal-content modal-content--vid"><iframe src="https://player.vimeo.com/video/'.$rcmd[$n][1].'" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe></div>';
            print '</div>';
        }
        ?>
            </ul>

            <h2>Canada</h2>

            <ul><?php
        $can = array(
        array('Care Across the Continuum', '448609072'),
        array('Patient Portal', '585776728'),
        array('Ambulatory for Physicians', '646674130'),
        array('Expanse Overview (French) - PEX', '646182457'),
        array('Canadian Analytics', '630073154'),
        array('Expanse Traverse - PEX', '624570129')
            );

        for($n=0; $n < count($can); $n++){
            print '<li><a href="javascript:void(0)" class="open-modal" data-target="modal'.($n+1000).'">'.$can[$n][0].'</a> </li>';
            print '<div class="modal" id="modal'.($n+1000).'"><a class="close-modal" href="javascript:void(0)">×</a>';
            print '<div class="modal-content modal-content--vid"><iframe src="https://player.vimeo.com/video/'.$can[$n][1].'" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe></div>';
            print '</div>';
        }
        ?>
            </ul>

        </div>

    </div>
    <!-- end node--sales-pages.tpl.php template -->
