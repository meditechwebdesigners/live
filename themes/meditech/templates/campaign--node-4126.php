<!-- START campaign--node-4126.php -- GREENFIELD ALLIANCE -->
<?php
$url = $GLOBALS['base_url']; // grabs the site url
  
$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
  .shadow-box--var {
    padding: 2em;
    color: #3e4545;
    background-color: #fff;
    border-radius: 7px;
    box-shadow: 0px 0px 20px 1px rgba(120, 120, 120, .2);
    max-width: 500px;
    position: absolute;
    right: 18px;
    top: 50%;
    transform: translateY(-50%);
  }

  .container--bg-img {
    max-width: 900px;
    border-radius: 7px;
  }

  .container--rel {
    position: relative;
  }

  ul.numbers {
    padding: 0;
  }

  ul.numbers li {
    list-style: none;
    text-align: center;
  }

  ul.numbers li span {
    font-size: 2em;
    font-weight: bold;
    margin-right: 0.15em;
    position: relative;
    top: 0.15em;
  }

  @media all and (max-width: 950px) {
    .shadow-box--var {
      max-width: 100%;
      position: relative;
      margin: 0 auto;
      width: 90%;
      right: 0;
    }

    .container--bg-img {
      max-width: 100%;
      border-radius: 7px;
    }

    .container--rel {
      margin-bottom: -8em;
    }
  }

  @media all and (max-width: 500px) {
    .shadow-box--var {
      transform: translateY(0%);
      width: 100%;
      margin-top: 2.35765%;
    }

    .container--rel {
      margin-bottom: 0;
    }
  }

  .btn--orange {
    border: 4px solid #ff8300 !important;
  }

  .btn--outline {
    background-color: unset;
    color: #3E4545 !important;
    border-width: 4px !important;
  }

  .btn--orange:hover {
    -webkit-transition: all 600ms ease-in-out;
  }

  .icons img {
    width: 40%;
    height: 40%;
    margin-top: 1.5em;
  }

  .pipe-margin {
    margin: 0 0.5em;
  }

  .gl-container .container__one-third:last-child {
    margin: 0 auto;
  }

</style>

<div class="js__seo-tool__body-content">


  <!-- Block 1 -->
  <div class="container container__centered container--rel">
    <img class="container--bg-img" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/meditech-greenfield-alliance-logo--over-network-of-people.jpg" alt="people networking">
    <div class="shadow-box--var">
      <h1>Discover the MEDITECH Greenfield Alliance</h1>
      <p>MEDITECH is creating an ecosystem of partner organizations with proven, successful, and interoperable solutions. Members can connect with a growing global network of participating partners, while leveraging the innovation and proven results of MEDITECH Expanse.</p>
      <div class="center" style="margin-top:2em;">
        <a href="https://info.meditech.com/meditech_greenfield_postcard" class="btn--orange">Apply for MEDITECH Greenfield Alliance</a>
      </div>
    </div>
  </div>
  <!-- Block 1 -->


  <!-- Block 3 -->
  <div class="container bg--green-gradient">
    <div class="container__centered">
      <div class="auto-margins text--white" style="padding-bottom:1em;">
        <h2>Innovate, accelerate, and collaborate with us</h2>
        <p>MEDITECH’s Greenfield Alliance is specifically designed for companies that want to collaborate with us, and offer solutions which complement, enhance, or extend MEDITECH Expanse. Flexibility is key, so we have three distinct programs for aligning with us:</p>
      </div>
      <div class="card__wrapper text--black-coconut">
        <div class="container__one-third card">
          <div class="center" style="padding-top:1em;">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/meditech-greenfield-alliance-badge--innovator.png" alt="MEDITECH Greenfield Alliance Innovator badge" width="250" height="250">
          </div>
          <div class="card__info">
            <h3 class="center" style="font-size:2em;">Innovator</h3>
            <p>Provide embedded solutions and form an integrated relationship with MEDITECH’s Business and Technology teams.</p>
          </div>
        </div>
        <div class="container__one-third card">
          <div class="center" style="padding-top:1em;">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/meditech-greenfield-alliance-badge--accelerator.png" alt="MEDITECH Greenfield Alliance Accelerator badge" width="250" height="250">
          </div>
          <div class="card__info">
            <h3 class="center" style="font-size:2em;">Accelerator</h3>
            <p>Maximize your sales opportunities by offering validated solutions with proven use cases for Expanse. Connect with MEDITECH’s Sales and Implementation teams for referral opportunities and streamlined customer implementations.</p>
          </div>
        </div>
        <div class="container__one-third card">
          <div class="center" style="padding-top:1em;">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/meditech-greenfield-alliance-badge--collaborator.png" alt="MEDITECH Greenfield Alliance Collaborator badge" width="250" height="250">
          </div>
          <div class="card__info">
            <h3 class="center" style="font-size:2em;">Collaborator</h3>
            <p>Build turnkey solutions leveraging out of the box integrations with Expanse. Access our interoperability specification library and work closely with MEDITECH’s Business teams.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Block 3 -->


  <div class="container container__centered">
    <h2 class="center">Get to know some of our Greenfield Alliance members</h2>
    <?php print views_embed_view('vendor_logos_selection', 'block', '3436,1180,1083,1082,4133,4132,1188'); ?>
    <div class="center">
      <p><a href="https://home.meditech.com/en/d/newmeditech/pages/collaborativesolutions.htm">Collaborative Solutions</a>
    </div>
  </div>


  <div class="container bg--light-brown">
    <div class="container__centered center">
      <h2>MEDITECH recognized by KLAS for the 8th consecutive year</h2>
      <div class="container" style="padding: 1em 0;">
        <div class="container__one-third">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-KLAS-Award-2022--Acute-Care-EMR.png" alt="Best in KLAS 2022 award - Acute Care EMR (community hospital) for MEDITECH Expanse" width="250" height="250" style="width:70%; height:auto;">
          <p class="bold no-margin--bottom">Acute Care EMR</p>
          <p>Community Hospital</p>
        </div>
        <div class="container__one-third">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-KLAS-Award-2022--Patient-Accounting-and-Patient-Management.png" alt="Best in KLAS 2021 award - Patient Accounting &amp; Patient Management (community hospital) for MEDITECH Expanse" width="250" height="250" style="width:70%; height:auto;">
          <p class="bold no-margin--bottom">Patient Accounting &amp;
            Patient Management</p>
          <p>Community Hospital</p>
        </div>
        <div class="container__one-third">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-KLAS-Award-2022--Home-Health.png" alt="Best in KLAS award - Home Health (small) for MEDITECH Expanse" width="250" height="250" style="width:70%; height:auto;">
          <p class="bold no-margin--bottom">Home Health EHR</p>
          <p>1-200 average daily census</p>
        </div>
      </div>
      <p class="italic" style="margin-bottom: 2em;">This was also the 2nd consecutive year that Expanse was rated a Top Performer (#2 overall) for: <br>
        <span class="bold">Overall Software Suite <span class="pipe-margin">|</span> Acute Care EMR (Large) <span class="pipe-margin">|</span> Ambulatory EMR (Over 75 physicians)</span>
      </p>
    </div>
  </div>


  <!-- Block 2 -->
  <style>
    .circle {
      border-radius: 50%;
      display: flex;
      align-items: center;
      justify-content: center;
    }

    .circle p span {
      font-size: 2em;
      font-weight: bold;
      display: block;
      margin-bottom: .15em;
      margin-top: .25em;
    }

    .donut {
      width: 200px;
      height: 200px;
      margin: 0 auto;
    }

    .donut-hole {
      width: 65%;
      height: 65%;
      border: 4px solid #fff;
      background: #32325D;
      color: #FF610A;
      font-weight: bold;
      font-size: 2.5em;
    }

    .chart-1 {
      background: conic-gradient(#FF610A 0deg 83deg, #ffffff 83deg 360deg);
    }

    .chart-2 {
      background: conic-gradient(#FF610A 0deg 169deg, #ffffff 169deg 360deg);
    }

    .chart-3 {
      background: conic-gradient(#FF610A 0deg 302deg, #ffffff 302deg 360deg);
    }
    
    .bubbles {
       position:relative; width: 100%; height: 350px;
    }

    .bubble-1 {
      width: 250px;
      height: 250px;
      background-color: RGBa(255, 97, 10, .8);
      position: absolute;
      top: 5%;
      left: 60%;
      padding: 1em;
    }

    .bubble-2 {
      width: 225px;
      height: 225px;
      background-color: rgba(255, 255, 255, .8);
      position: absolute;
      top: 25%;
      left: 75%;
      padding: 1em;
    }

    .bubble-3 {
      width: 300px;
      height: 300px;
      background-color: RGBa(255, 97, 10, .8);
      position: absolute;
      top: 0;
      left: 20%;
      padding: 1em;
    }

    .bubble-4 {
      width: 150px;
      height: 150px;
      background-color: rgba(255, 255, 255, .8);
      position: absolute;
      top: 35%;
      left: 44%;
      padding: 1em;
    }

    .bubble-5 {
      width: 50px;
      height: 50px;
      background-color: RGBa(255, 97, 10, .8);
      position: absolute;
      top: 17%;
      left: 55%;
    }

    .bubble-6 {
      width: 80px;
      height: 80px;
      background-color: rgba(255, 255, 255, .8);
      position: absolute;
      top: 66%;
      left: 60%;
    }

    .bubble-7 {
      width: 40px;
      height: 40px;
      background-color: rgba(255, 255, 255, .8);
      position: absolute;
      top: 15%;
      left: 12%;
    }

    .bubble-8 {
      width: 30px;
      height: 30px;
      background-color: RGBa(255, 97, 10, .8);
      position: absolute;
      top: 60%;
      left: 85%;
    }

    @media (max-width: 1000px) {
      .bubbles { height: unset; }
      .bubbles .circle {
        border-radius: 0;
        width: 100%;
        top: unset;
        left: unset;
        display: block;
        position: unset;
        height: unset;
      }
    }

  </style>

  <div class="container bg--purple-gradient">

    <div class="container__centered">

      <div class="container__one-third">
        <div class="circle donut chart-1">
          <div class="circle donut-hole">23%</div>
        </div>
        <div class="center text--large" style="padding:1em 2em;">
          <p>US Market Share</p>
        </div>
      </div>

      <div class="container__one-third">
        <div class="circle donut chart-2">
          <div class="circle donut-hole">47%</div>
        </div>
        <div class="center text--large" style="padding:1em 2em;">
          <p>Canadian Market Share</p>
        </div>
      </div>

      <div class="container__one-third">
        <div class="circle donut chart-3">
          <div class="circle donut-hole">84%</div>
        </div>
        <div class="center text--large" style="padding:1em 2em;">
          <p>of our customers have been with us for over 10 years</p>
        </div>
      </div>

    </div>

    <div class="container__centered center bubbles">

      
      <div class="circle bubble-3 text--large">
        <p class="no-margin--bottom"><span>300 Billion</span> data transactions per year</p>
      </div>
      <div class="circle bubble-4 text--large">
        <p class="text--black-coconut no-margin--bottom"><span>50+</span> APIs</p>
      </div>
      <div class="circle bubble-1">
        <p class="no-margin--bottom"><span>500,000+</span> interoperability interfaces delivered</p>
      </div>
      <!--
      <div class="circle bubble-2">
        <p class="text--black-coconut no-margin--bottom"><span>200,000+</span> interfaces successfully exchange MEDITECH data</p>
      </div>
      -->
      <div class="circle bubble-5"></div>
      <div class="circle bubble-6"></div>
      <div class="circle bubble-7"></div>
      <div class="circle bubble-8"></div>

    </div>

  </div>
  <!-- End Block 2 -->


  <div class="container">
    <div class="container__centered auto-margins">
      <h2>Become a Greenfield Alliance member</h2>
      <p>If you have an app focused on using USCDI R4 patient access APIs and/or FHIR Scheduling APIs you would like to test, please register for access to the <a href="https://ehr.meditech.com/ehr-solutions/greenfield-workspace">Greenfield Workspace</a>, our app development environment.</p>
      <p>Please refer to our <a href="https://ehr.meditech.com/privacy-policy">Privacy Policy</a> for more details on how MEDITECH stores and processes information.</p>
      <div class="center" style="margin-top:2em;">
        <a href="https://info.meditech.com/meditech_greenfield_postcard" class="btn--orange">Apply for MEDITECH Greenfield Alliance</a>
      </div>
    </div>
  </div>


</div>
<!-- end js__seo-tool__body-content -->


<div class="container container__centered center">
  <div>
    <?php print $share_link_buttons; ?>
  </div>
</div>



<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>
<!-- END campaign--node-4126.php -->
