<!-- start views-view-fields--import-news-article--block.tpl.php template -->
<?php 
  // This template is for each row of the Views block: IMPORT NEWS ARTICLE ....................... 
?>
   
<h2><?php print $fields['title']->content; ?></h2>
<div><?php print $fields['field_body']->content; ?></div>

<!-- end views-view-fields--import-news-article--block.tpl.php template -->