<?php

/**
 * @file
 * Default template for feed displays that use the RSS style.
 *
 * @ingroup views_templates
    <description><?php print $description; ?></description>
 */
?>
<?php print '<?xml version="1.0" encoding="utf-8" ?>'; ?>
<rss version="2.0" xml:base="<?php print $link; ?>"<?php print $namespaces; ?>>
<channel>
<title><?php print $title; ?></title>
<link><?php print $link; ?></link>
<language><?php print $langcode; ?></language>
<?php print $channel_elements; ?>
<jobs>
<?php print $items; ?>
</jobs>
</channel>
</rss>
