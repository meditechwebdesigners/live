<!-- START campaign--node-4209.php -->

<?php // This template is set up to control the display of the MEDITECH Circle Campaign

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
    .extra-padding {
        padding: 3em 0 3em 4em;
    }

    .bg--light-blue {
        background-color: #f2f8fc;
    }

    .headshot-container {
        display: flex;
        align-items: center;
        margin-top: 1.5em;
    }

    .headshot {
        max-width: 100px;
        max-height: 100px;
        flex-grow: 1;
        margin-right: 1.5em;
        float: left;
    }

    .headshot img {
        border-radius: 50%;
    }

    .quote-graphic {
        position: absolute;
        top: -50px;
        left: 50%;
        transform: translate(-50%);
        z-index: 3;
    }

    .quote-graphic img {
        width: 100px;
        height: 100px;
        border-radius: 50%;
    }

    .quote-box {
        padding: 3em;
        background-color: rgba(255, 255, 255, 1);
        border-left: 5px solid #00bc6f;
        margin-bottom: 2.35765%;
        border-radius: 7px;
        box-shadow: 0px 0px 20px 1px rgb(0 0 0 / 20%);
        position: relative;
    }

    @media all and (max-width: 65em) {
        .extra-padding {
            padding: inherit;
        }
    }

</style>

<div class="js__seo-tool__body-content">

    <!-- Start Block 1 -->

    <div class="content__callout background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/background--circles-1.png); background-position:top left; padding: 2em 0;">
        <div class="content__callout__content" style="background:none; padding-right: 0; padding-left: 6%;">
            <div class="content__callout__body">
                <div class="content__callout__body__text text-shadow--white">
                    <h1 class="js__seo-tool__title" style="font-family: Nexa, montserrat, Verdana, sans-serif; font-size:2.75em"><span class="text--meditech-green bold">MEDITECH</span> <span style="font-weight:normal;">Circle</span></h1>
                    <h2>The Most Connected, Transparent Support Around</h2>
                    <p>We are proud to announce MEDITECH Circle, an intuitive customer support platform built to simplify the customer experience, save time, and connect all the pieces of your organizational story. It is the culmination of years of user feedback, workflow research, and investments to unite everything we do to support our customers.</p>
                </div>
            </div>
        </div>
        <div class="content__callout__media">
            <div class="content__callout__image-wrapper">
                <div class="video js__video" data-video-id="733946668">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--introducing-meditech-circle.jpg" alt="MEDITECH Circle - Video Covershot">
                    </figure>
                    <a class="video__play-btn" href="https://vimeo.com/733946668?&amp;autoplay=1"></a>
                    <div class="video__container"></div>
                </div>
            </div>
        </div>
    </div>


    <!--
    <div class="content__callout background--cover extra-padding" style="background-image: url(< ?php print $url; ?>/sites/all/themes/meditech/images/campaigns/purple-neon-circle--centered-offscreen-darkened.jpg); background-position: center;">
        <div class="content__callout__content" style="background:none; padding-right: 0; padding-left: 6%;">
            <div class="content__callout__body">
                <div class="content__callout__body__text text-shadow--black text--white">
                    <h1 class="js__seo-tool__title"><span style="font-weight:bold;" class="text--meditech-green">MEDITECH</span> <span style="font-weight:lighter;">Circle</span></h1>
                    <h2>The Most Connected, Transparent Support Around</h2>
                    <p>We are proud to announce MEDITECH Circle, an intuitive customer support platform built to simplify the customer experience, save time, and connect all the pieces of your organizational story. It is the culmination of years of user feedback, workflow research, and investments to unite everything we do to support our customers.</p>
                </div>
            </div>
        </div>
        <div class="content__callout__media">
            <div class="content__callout__image-wrapper">
                <div class="video js__video" data-video-id="733946668">
                    <figure class="video__overlay">
                        <img src="< ?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--introducing-meditech-circle.jpg" alt="MEDITECH Circle - Video Covershot">
                    </figure>
                    <a class="video__play-btn" href="https://vimeo.com/733946668?&amp;autoplay=1"></a>
                    <div class="video__container"></div>
                </div>
            </div>
        </div>
    </div>
-->
    <!-- End Block 1 -->


    <!-- Start Block 2 -->
    <div class="container no-pad">
        <div class="gl-container">
            <div class="container__one-half background--cover flex-order--reverse" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/business-casual-man-working-help-desk.jpg); min-height:20em; background-position:center top;"></div>
            <div class="container__one-half bg--purple-gradient gl-text-pad">
                <h2>Strong, versatile knowledge at your fingertips</h2>
                <p>MEDITECH Circle empowers your staff to get all the answers to their EHR questions through three unique pathways:</p>
                <ul class="fa-ul">
                    <li><span class="fa-li text--meditech-green"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span><b>A simple question</b> usually has a simple answer in our Knowledge Base.</li>
                    <li><span class="fa-li text--meditech-green"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span><b>A more complicated query</b> puts you in touch with one of our experts, to help support you and reach a resolution.</li>
                    <li><span class="fa-li text--meditech-green"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span><b>If your question doesn’t have a quick answer</b>, you can monitor progress through our transparent case system.</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- End Block 2 -->


    <!-- Start Block 3 -->
    <div id="modal1" class="modal">
        <a class="close-modal" href="javascript:void(0)">&times;</a>
        <div class="modal-content">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/screenshot--meditech-circle--large.png"> <!-- Add modal content here -->
        </div>
    </div>
    <!-- End hidden modal box -->

    <!-- Start modal trigger -->


    <div class="container bg--light-blue">
        <div class="container__centered">

            <div class="container__one-half">
                <h2>Open options for questions and support</h2>
                <p>MEDITECH Circle includes a sophisticated new support portal that directs every possible need.</p>
                <ul class="fa-ul">
                    <li><span class="fa-li text--dark-blue"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span>Intuitive search features and interfaces will empower more intelligent self-service</li>
                    <li><span class="fa-li text--dark-blue"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span>Rapid and transparent communication on each request means staff always are kept in the loop.</li>
                </ul>
            </div>
            <div class="container__one-half">
                <div class="open-modal" data-target="modal1">
                    <div class="tablet--black">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/screenshot--meditech-circle--small.png">
                    </div>
                    <div class="mag-bg">
                        <!-- Include if using image trigger -->
                        <i class="mag-icon fas fa-search-plus"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Block 3 -->


    <!-- Start Block 4 -->
    <div class="container bg--purple-gradient">
        <div class="container__centered">
            <h2>Addressing everyone’s concerns, on every level</h2>

            <div class="card__wrapper text--black-coconut" style="margin-top:2em;">

                <div class="container__one-third card">
                    <figure>
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/card-image--overhead-spiral-stairs.jpg" alt="">
                    </figure>
                    <div class="card__info">
                        <h3>C-Level</h3>
                        <p>From corner to corner, a CIO can tell how all of their organization’s needs are being met. You can also identify common areas and trends that could use more attention.</p>
                    </div>
                </div>

                <div class="container__one-third card">
                    <figure>
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/card-image--circular-abstract-data-alt-1.png" alt="">
                    </figure>
                    <div class="card__info">
                        <h3>Informaticist</h3>
                        <p>An informaticist with a more in-depth question can get a status update in moments through the intuitive portal. There will always be a dedicated MEDITECH staff member or team available to help you.</p>
                    </div>
                </div>

                <div class="container__one-third card">
                    <figure>
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/card-image--clinical-team-forming-hand-circle.jpg" alt="">
                    </figure>
                    <div class="card__info">
                        <h3>Clinician</h3>
                        <p>A clinician can refer a quick question to the robust Knowledge Base for an answer, without holding up their workflow.</p>
                    </div>
                </div>

            </div>

        </div>
    </div>
    <!-- End Block 4 -->


    <!-- Start Block 5 -->
    <div class="container bg--light-blue" style="padding:4em 0 3em 0;">
        <div class="container__centered">

            <div class="container no-pad--bottom">
                <div class="quote-box">
                    <div class="quote-graphic">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/quote-outline.svg" alt="Quote graphic">
                    </div>

                    <p class="text--large italic">“MEDITECH Circle is our shift to a more complete, connected, and open support platform. This solid foundation will advance our ability to support customers far into the future.”</p>
                    <img class="headshot" src="<?php print $url; ?>/sites/all/themes/meditech/images/people/Michael-Malone.png" alt="Michael Malone headshot">
                    <p class="bold no-margin--bottom">Michael Malone, Associate Vice President,</p>
                    <p>Client Services, MEDITECH</p>
                </div>
            </div>
        </div>
    </div>
    <!-- End Block 5 -->



</div>
<!-- end js__seo-tool__body-content -->


<!-- Start Block 6 -->
<div class="container bg--purple-gradient">
    <div class="container__centered center auto-margins">

        <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
        <h2>
            <?php print $cta->field_header_1['und'][0]['value']; ?>
        </h2>
        <?php } ?>

        <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
        <div>
            <?php print $cta->field_long_text_1['und'][0]['value']; ?>
        </div>
        <?php } ?>

        <div class="btn-holder--content__callout center" style="margin-bottom:1.5em;">
            <div class="center" style="margin-top:2em;">
                <?php hubspot_button($cta_code, "Register for our Clinical Informatics Symposium"); ?>
            </div>
        </div>

        <div style="margin-top:1em;">
            <?php print $share_link_buttons; ?>
        </div>

    </div>
</div>
<!-- End Block 12 -->


<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>
<!-- END campaign--node-2735.php -->
