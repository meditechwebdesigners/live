<!-- START campaign--node-3016.php -->
<?php // This template is set up to control the display of the Expanse Practice Management content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
	.angled-bg--custom-1-white {
		background-image: linear-gradient(-70deg, transparent 45.9%, rgba(255, 255, 255, .8) 46%);
		padding: 3em 0;
	}

	.angled-bg--custom-2-white {
		background-image: linear-gradient(-70deg, transparent 40.9%, rgba(255, 255, 255, .8) 41%);
		padding: 3em 0;
	}


	@media all and (max-width: 50em) {

		.angled-bg--custom-1-white {
			background-image: linear-gradient(70deg, rgba(255, 255, 255, .85) 49.9%, rgba(255, 255, 255, .85) 50%);
		}
	}

	@media all and (max-width: 50em) {

		.angled-bg--custom-2-white {
			background-image: linear-gradient(70deg, rgba(255, 255, 255, .8) 49.9%, rgba(255, 255, 255, .8) 50%);
		}
	}

	.small_popup_screenshot {
		border: 4px solid #e6e9ee;
		border-radius: 6%;
	}

	.packery-grid-item {
		float: left;
		width: 33.333%;
		height: 200px;
		padding: 0.5em 0 0 0;
		border-right: 5px solid #FFFFFF;
		border-bottom: 5px solid #FFFFFF;
	}

	@media screen and (max-width: 600px) {

		.packery-grid-item,
		.packery-grid-item--width2 {
			width: 100%;
		}
	}

	.packery-grid-item--height2 {
		height: 400px;
	}

	.packery-grid-item--bg {
		position: relative;
		padding: 0;
		margin: 0;
		height: 100%;
		overflow: hidden;
	}

	.packery-grid-item--info {
		position: absolute;
		top: auto;
		bottom: 0;
		left: 0;
		width: 100%;
		padding: 1em;
		background: rgba(0, 0, 0, 0.65);
	}

	.packery-grid-item--info--title {
		font-weight: bold;
		line-height: 1.25em;
		margin-bottom: 0;
		color: white;
	}

</style>


<div class="js__seo-tool__body-content">

	<!-- Block 1 -->
	<div class="container background--cover no-pad" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/male-patient-signing-waiver-at-check-in-at-doctors-office.jpg); background-position: right;">
		<div class="angled-bg--custom-1-white">
			<div class="container__centered">
				<div class="container__one-half">
					<p class="header-micro">EXPANSE AMBULATORY</p>
					<h1 class="js__seo-tool__title">Practice made perfect.</h1>
					<p>Rising patient expectations. Increased competition. Tight margins. Your practice needs a solution that drives revenue while simultaneously improving the patient experience and reducing the stress of a busy practice environment. Expanse Ambulatory’s Practice Management solution does all of those things through more efficient office workflows and more intuitive software.</p>
					<div style="margin-top:2em;">
						<?php hubspot_button($cta_code, "Sign Up for the Practice Management Webinar"); ?>
					</div>
				</div>
				<div class="container__one-half">&nbsp;</div>
			</div>
		</div>
	</div>
	<!-- End of Block 1 -->


	<!-- Block 2 -->
	<div class="container bg--purple-gradient">
		<div class="container__centered text--white">

			<div class="center auto-margins">
				<p class="header-micro" style="display: inline;">PATIENT-CENTERED</p>
				<h2>Give your patients convenience <span class="italic">and</span> safety.</h2>
				<p style="margin-bottom: 2em;"><a href="https://ehr.meditech.com/ehr-solutions/patient-engagement">Enhance the patient experience</a> to grow your practice and keep patients coming back.</p>
			</div>

			<div class="container center no-pad--top">
				<div class="gl-container bg--meditech-green">
					<div class="container__one-third bg--dark-blue">
						<h4>Access and Engagement</h4>
						<ul class="left fa-ul">
							<li><span class="fa-li"><i class="fas fa-check-square text--white" style="padding-right: 0.5em;"></i></span>A unified patient portal across all care settings</li>
							<li><span class="fa-li"><i class="fas fa-check-square text--white" style="padding-right: 0.5em;"></i></span>24/7 access to book appointments</li>
							<li><span class="fa-li"><i class="fas fa-check-square text--white" style="padding-right: 0.5em;"></i></span>Care management resources to keep patients engaged</li>
						</ul>
					</div>
					<div class="container__one-third bg--white">
						<h4>Contactless Check-In</h4>
						<ul class="left fa-ul">
							<li><span class="fa-li"><i class="fas fa-check-square text--black-coconut" style="padding-right: 0.5em;"></i></span>Forms can be completed prior to arrival for shorter wait times</li>
							<li><span class="fa-li"><i class="fas fa-check-square text--black-coconut" style="padding-right: 0.5em;"></i></span>Patients can bypass the front desk with self-check-in via the MHealth app</li>
						</ul>
					</div>
					<div class="container__one-third bg--meditech-green">
						<h4>Virtual Visits</h4>
						<ul class="left fa-ul">
							<li>
								<span class="fa-li"><i class="fas fa-check-square text--white" style="padding-right: 0.5em;"></i></span>See their provider from the comfort of their home
							</li>
							<li>
								<span class="fa-li"><i class="fas fa-check-square text--white" style="padding-right: 0.5em;"></i></span>Experience greater convenience and safety
							</li>
							<li>
								<span class="fa-li"><i class="fas fa-check-square text--white" style="padding-right: 0.5em;"></i></span>Save time traveling back and forth to the practice
							</li>
						</ul>
					</div>

				</div>
			</div>

		</div>
	</div>
	<!-- End of Block 2 -->


	<!-- Block 3 -->
	<div class="container background--cover no-pad" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/mom-and-daughter-checking-in-at-doctors-office-with-insurance-card.jpg);">
		<div class="angled-bg--custom-2-white">
			<div class="container__centered">
				<div class="container__one-half">
					<p class="header-micro">REIMBURSEMENT</p>
					<h2>Maximize opportunities to boost revenue.</h2>
					<p>Build a foundation for long-term financial sustainability and operational efficiency. Expanse Ambulatory Practice Management provides staff with the tools to:</p>
					<ul>
						<li>Identify and collect copays, deductibles, and outstanding balances when patients check in and check out.</li>
						<li>Receive automated alerts that minimize claim denials for missing information.</li>
						<li>Diminish lost revenue by capturing charges at the point of care.</li>
						<li>Accelerate eligibility checking, authorizations and referrals, claim submissions, and remittances through electronic transactions.</li>
						<li>Manage daily billing, collections, and denial management follow-up, all from MEDITECH’s Patient Accounting Desktop.</li>
					</ul>
				</div>
				<div class="container__one-half">
					&nbsp;
				</div>
			</div>
		</div>
	</div>
	<!-- End of Block 3 -->


	<!-- Block 4 -->
	<div class="container bg--purple-gradient" style="padding:4em 0">
		<div class="container__centered">
			<div class="container__one-half no-padding">
				<p class="header-micro">QUALITY</p>
				<h2 style="margin-top:1em; font-size: 1.47em;">Your partner for patient-centered care.</h2>
				<p>Our Expanse EHR lowers your burden for achieving Patient-Centered Medical Home (PCMH) recognition and helps your providers earn PCMH status. That’s because we’ve worked with the NCQA to <a href="https://www.ncqa.org/programs/health-care-providers-practices/patient-centered-medical-home-pcmh/" target="_blank">prevalidate Expanse</a>. See how easy it is to earn improvement activity credit toward MIPS requirements from CMS, and watch your MIPS payments rise.</p>
			</div>
			<div class="container__one-half no-padding center"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Patient-Centered-Medical-Home-Prevalidated.png" alt="" style="width:350px;"></div>
		</div>
	</div>
	<!-- End of Block 4 -->


	<!-- Block 5 -->
	<div id="modal2" class="modal">
		<a class="close-modal" href="javascript:void(0)">&times;</a>
		<div class="modal-content">
			<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-Quality-Vantage-EP-Dashboard.png">
		</div>
	</div>

	<div class="container">
		<div class="container__centered">
			<div class="container__two-thirds">
				<div class="center">
					<div class="open-modal center" data-target="modal2">
						<div class="tablet--black">
							<img style="width: 100%;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-Quality-Vantage-EP-Dashboard.png" alt="MEDITECH Ambulatory home screen">
						</div>
						<div class="mag-bg">
							<i class="mag-icon fas fa-search-plus"></i>
						</div>
					</div>
				</div>
			</div>
			<div class="container__one-third">
				<p class="header-micro" style="display: inline;">VALUE-BASED</p>
				<h2>Track performance with Quality Vantage.</h2>
				<p>Assess <a href="https://ehr.meditech.com/ehr-solutions/macra">regulatory performance</a> at the provider, practice, and group level to determine potential impacts on reimbursement. Quality Vantage dashboards are interactive and user-friendly, providing performance snapshots that help practices identify and analyze measures to meet their goals.</p>
			</div>
		</div>
		<div class="container no-pad--bottom">
			<div class="container__centered">
				<figure class="container__one-fourth center">
					<img class="quote__content__img" style="padding-top: 2em;" src="<?php print $url; ?>/sites/all/themes/meditech/images/quote.svg" alt="Quote bubble">
				</figure>
				<div class="container__three-fourths">
					<div class="quote__content__text text--large" style="margin-top:1.5em;">
						<p class="italic">“Quality Vantage is a powerful tool for providers and organizations to view quality metrics in near-real time. Its simple and sleek design makes it easy to navigate for all staff. QV is accessible from the web, which allows staff to see the high-level performance rates, drill down to the details, and launch directly to the patient's chart. Having quality metrics within MEDITECH is an integral part of identifying and improving patients’ health easily.”</p>
					</div>
					<p class="text--large no-margin--bottom">Scott Stone, Application Specialist, CAPM</p>
					<p>Logan Health</p>
				</div>
			</div>
		</div>
	</div>
	<!-- End of Block 5 -->


	<!-- Block 6 -->
	<div class="container bg--purple-gradient">
		<div class="container__centered text--white">

			<div class="center auto-margins">
				<p class="header-micro" style="display: inline;">EXPANSE EVERYWHERE</p>
				<h2>For those who integrate.</h2>
				<p>Is your practice associated with a health system using Expanse across the enterprise? If so, you’ll reap the additional benefits of MEDITECH’s industry-leading integration.</p>
			</div>

			<div class="container center no-pad--top">
				<div class="gl-container bg--emerald">

					<div class="container__one-third bg--meditech-green">
						<img style="width: 40%;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Complete-Picture--Icon.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Complete-Picture--Icon.png';this.onerror=null;" alt="Complete Picture icon">
						<h3>One Record</h3>
						<h4>across the enterprise</h4>
						<p>One shared EHR across care settings means your practice has a complete picture of health for each patient, facilitating seamless referrals and better care coordination.</p>
					</div>
					<div class="container__one-third bg--white">
						<img style="width: 40%;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/schedule--icon.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/schedule--icon.png';this.onerror=null;" alt="Schedule on Computer icon">
						<h3>One Schedule</h3>
						<h4>across the enterprise</h4>
						<p>A single schedule includes provider appointments, rounding schedules, and meetings.</p>
					</div>
					<div class="container__one-third">
						<img style="width: 40%;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/statement--icon.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/statement--icon.png';this.onerror=null;" alt="Complete Picture icon">
						<h3>One Bill</h3>
						<h4>for patient care</h4>
						<p>Acute, ambulatory, and long-term care charges are combined on one patient-friendly statement, easily reviewed and paid through the patient portal.</p>
					</div>

				</div>
			</div>

		</div>
	</div>
	<!-- End of Block 6 -->

	<!--Block 7-->
	<div id="modal1" class="modal">
		<a class="close-modal" href="javascript:void(0)">&times;</a>
		<div class="modal-content">
			<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Ambulatory-Operations-BCA--screenshot-full-size.png">
		</div>
	</div>

	<div class="container">
		<div class="container__centered">
			<div class="auto-margins center" style="margin-bottom:2em;">
				<p class="header-micro" style="display: inline;">ANALYTICS</p>
				<h2>Data to drive your practice forward.</h2>
				<p>Put your data to use with an integrated <a href="https://ehr.meditech.com/ehr-solutions/meditech-business-clinical-analytics">business intelligence tool</a> that boosts operational efficiency, maximizes financial performance, and improves patient outcomes.</p>
			</div>
			<div class="container no-pad--top">
				<div class="container__one-third">
					<ul>
						<h3>MEDITECH’s Business and Clinical Analytics solution</h3>
						<li>Monitor and evaluate key performance indicators with comprehensive, standard datasets and dashboards.</li>
						<li>Customize dashboards to track unique organizational goals.</li>
						<li>Use our growing library of standard dashboards to make informed decisions that drive success.</li>
						<li><a href="https://info.meditech.com/summit-pacific-increases-reimbursement-clinic-volumes-with-meditech-analytics-solution-0">Learn how one organization</a> drove down A/R days and drove up clinic volumes using BCA.</li>
					</ul>
				</div>
				<div class="container__two-thirds">
					<div class="open-modal center" data-target="modal1">
						<div class="tablet--black">
							<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Ambulatory-Operations-BCA--screenshot-full-size.png" alt="MEDITECH Ambulatory Operations BCA screen">
						</div>
						<div class="mag-bg">
							<i class="mag-icon fas fa-search-plus"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--End of Block 7-->

</div>
<!-- end js__seo-tool__body-content -->

<!-- Call to Action -->
<div class="container  bg--purple-gradient">
	<div class="container__centered center">

		<?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
		<h2>
			<?php print $cta->field_header_1['und'][0]['value']; ?>
		</h2>
		<?php } ?>

		<?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
		<div>
			<?php print $cta->field_long_text_1['und'][0]['value']; ?>
		</div>
		<?php } ?>

		<div class="center" style="margin-top:2em;">
			<?php hubspot_button($cta_code, "Sign Up for the Ambulatory Webinar"); ?>
		</div>

		<div style="margin-top:1em;">
			<?php print $share_link_buttons; ?>
		</div>

	</div>
</div>
<!-- End Call to Action -->


<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>
<!-- END campaign--node-3016.php -->
