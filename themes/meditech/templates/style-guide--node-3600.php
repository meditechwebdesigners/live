<?php if( user_is_logged_in() ){ ?>
<!-- START style-guide--node-3600.php Images -->

<!--Javascript & CSS reference for Prism-->
<script type="text/javascript" src="<?php print $url; ?>/sites/all/themes/meditech/js/prism.js"></script>

<link href="<?php print $url; ?>/sites/all/themes/meditech/css/prism.css" rel="stylesheet" />

<style>
    .sg-section {
        margin-bottom: 2em;
    }

    .sg-section ul li {
        margin-bottom: .5em;
    }

</style>

<section class="container__centered">

    <h1 class="page__title">
        <?php print $title; ?>
    </h1>

    <div class="container__two-thirds">

        <div class="sg-section">
            <h2>Advertising Specifications</h2>
            <p><a href="https://www.himssmedia.com/advertising-specifications" target="_blank"><strong>HIMSS Ad Specs</strong></a></p>
            <p><a href="https://histalk2.com/advertise/" target="_blank"><strong>HIStalk Ad Specs</strong></a></p>
        </div>

        <div class="sg-section">
            <h2>Compression &amp; Quality</h2>
            <p><a href="https://tinypng.com/" target="_blank"><strong>Tiny PNG</strong></a>: Image compression software for JPGs and PNGs. Always compress large images before uploading to the server. Ideally we want image size to stay under 200kb.</p>
            <p><strong><a href="https://docs.google.com/presentation/d/1-44VC0PX7TfYRiJ2UGktDwy2OprJweNycG_OFy-B120/edit#slide=id.p" target="_blank">Website Images</a></strong>: Save as JPG, "high/very high" quality, 72DPI, and then compress if over 200kb.</p>
        </div>

        <div class="sg-section">
            <h2>Image Galleries/Libraries</h2>
            <p><a href="<?php print $url; ?>/news-article-images"><strong>News Article Image Gallery</strong></a>: A quick view of all news article images created recently.</p>
            <p><strong><a href="https://staff.meditech.com/en/d/salesandmarketing/pages/screencapturelibrary.htm" target="_blank">Screen Capture Library</a></strong>: A collection of MEDITECH software screen caps. All screen caps are Expanse unless noted otherwise.</p>
            <p><strong><a href="https://drive.google.com/drive/folders/0B4ABcB2BUDAlbFlMR052eklnQ2c">Stock Image Library</a></strong>: A library of Getty stock photos, MEDITECH buildings, executive headshots, illustrations, icons, logos and more.</p>
        </div>

        <div class="sg-section">
            <h2>Social Media Sizes</h2>
            <p>Save images at "maximum" and 72DPI. We want highest quality since all the platforms will automatically compress the images when uploaded.</p>
            <p>Please use a 16:9 aspect ratio unless otherwise specified below.</p>
            <ul style="margin-bottom: 1em;">
                <li>
                    <strong>Facebook, LinkedIn, Twitter</strong><br>
                    Dimensions: 1200 x 627px<br>
                    Filename: article-name--fb-li-tw
                </li>
                <li>
                    <strong>Google Doc Banner</strong><br>
                    Dimensions: 1000 x 180px<br>
                    Filename: article-name--google
                </li>
                <li>
                    <strong>Hubspot Email</strong><br>
                    Dimensions: 640 x 350px<br>
                    Filename: article-name--hubspot-email
                </li>
                <li>
                    <strong>Hubspot Blog</strong><br>
                    Dimensions: 800 x 450px<br>
                    Filename: article-name--hubspot-blog
                </li>
                <li>
                    <strong>Instagram</strong><br>
                    Dimensions: 1080 x 1080px<br>
                    Filename: article-name--instagram
                </li>
                <li>
                    <strong>News Article</strong><br>
                    Dimensions: 1444 x 640px<br>
                    Filename: article-name
                </li>
                <li>
                    <strong>Salesforce</strong><br>
                    Dimensions: 650 x 350px<br>
                    Filename: article-name--salesforce
                </li>
                <li>
                    <strong>Success Stories (Home Page)</strong><br>
                    Dimensions: 800 x 450px<br>
                    Filename: article-name--success-stories
                </li>
                <li>
                    <strong>Video Overlay</strong><br>
                    Dimensions: 960 x 540px<br>
                    Filename: video-name--video-overlay
                </li>
            </ul>
            <p><a href="https://docs.google.com/spreadsheets/d/1iPcwrbdxmy6uVPTVLrjnme150UDJ8La9Won26ibOGWg/edit?usp=sharing" target="_blank"><strong>Social Media Testing Accounts</strong></a></p>
            <p><a href="https://docs.google.com/spreadsheets/d/16zR402-MriVnvXcPk-1JpOUwwfAJvYGF16VBpNlkVH0/edit#gid=476246499" target="_blank"><strong>Sprout Social Image Size Cheat Sheet</strong></a></p>
        </div>

    </div>

    <!-- SIDEBAR -->
    <aside class="container__one-third">

        <div class="sidebar__nav panel">
            <?php
        $styleBlock = module_invoke('menu', 'block_view', 'menu-style-guide-side-nav');
        print render($styleBlock['content']); 
        ?>
        </div>

    </aside>
    <!-- END SIDEBAR -->

</section>

<!-- END style-guide--node-3600.php Images -->
<?php } ?>
