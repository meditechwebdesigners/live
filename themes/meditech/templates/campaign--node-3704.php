<!-- START campaign--node-3704.php -- Greenfield Resources -->
<?php
$url = $GLOBALS['base_url']; // grabs the site url
  
$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
  .shadow-box--var {
    padding: 2em;
    color: #3e4545;
    background-color: #fff;
    border-radius: 7px;
    box-shadow: 0px 0px 20px 1px rgba(120, 120, 120, .2);
    max-width: 500px;
    position: absolute;
    right: 18px;
    top: 50%;
    transform: translateY(-50%);
  }

  .container--bg-img {
    max-width: 900px;
    border-radius: 7px;
  }

  .container--rel {
    position: relative;
  }

  @media all and (max-width: 950px) {
    .shadow-box--var {
      max-width: 100%;
      position: relative;
      margin: 0 auto;
      width: 90%;
      right: 0;
    }

    .container--bg-img {
      max-width: 100%;
      border-radius: 7px;
    }

    .container--rel {
      margin-bottom: -8em;
    }
  }

  @media all and (max-width: 500px) {
    .shadow-box--var {
      transform: translateY(0%);
      width: 100%;
      margin-top: 2.35765%;
    }

    .container--rel {
      margin-bottom: 0;
    }
  }

  .btn--orange {
    border: 4px solid #ff8300 !important;
  }

  .btn--outline {
    background-color: unset;
    color: #3E4545 !important;
    border-width: 4px !important;
  }

  .btn--orange:hover {
    -webkit-transition: all 600ms ease-in-out;
  }

  .icons img {
    width: 40%;
    height: 40%;
    margin-top: 1.5em;
  }

</style>

<div class="js__seo-tool__body-content">


  <!-- Block 1 -->
  <div class="container container__centered" style="padding-bottom:0;">
    <div class="container__two-thirds">
      <h1><?php print $title; ?></h1>
      <p>Welcome to the Greenfield Workspace Resource Hub, where you can explore information about interoperability features available within the Greenfield ecosystem. You may receive technical support from MEDITECH developers as well as reference our Greenfield Workspace web application for technical API specifications.</p>
    </div>
    <div class="container__one-third center" style="padding-top:3em;">
      <a href="https://info.meditech.com/welcome-to-meditech-greenfield-rfi" class="btn--orange btn--outline" style="margin-bottom:1em;">Register </a>&nbsp;&nbsp;&nbsp;<a href="https://greenfield.meditech.com/" class="btn--orange" style="margin-bottom:1em;">Log in</a>
    </div>
  </div>
  <!-- Block 1 -->


  <!-- START block -->
  <div class="container container__centered">
    <div class="shadow-box" style="background-color: #f0fafb;">

      <h2>View Greenfield Workspace specifications</h2>
      <div class="container no-pad">
        <div class="container__one-half">
          <ul>
            <li>FHIR Patient Access APIs (<a href="https://greenfield.meditech.com/explorer/topic/patient-health-data">DSTU2</a>/<a href="https://greenfield.meditech.com/explorer/topic/USCore-patient-health-data">R4</a>)</li>
            <li><a href="https://ehr.meditech.com/hl7-outbound-list-for-greenfield">HL7v2 Interfaces</a></li>
            <li>FHIR Scheduling APIs <em>(coming soon)</em></li>
          </ul>
        </div>
      </div>

    </div>
  </div>
  <!-- END block -->


  <!-- START block -->
  <div class="container container__centered">
    <div class="card__wrapper">
      <div class="container__one-half card bg--emerald">
        <div class="card__info">
          <h2>Learn about standards</h2>
          <ul>
            <li><a href="http://www.hl7.org/fhir/dstu2/index.html">FHIR DSTU2</a></li>
            <li><a href="http://hl7.org/fhir/index.html">FHIR R4</a></li>
            <li><a href="http://www.fhir.org/guides/argonaut/r2/">Argonaut Data Query Imp Guide</a></li>
          </ul>
        </div>
      </div>
      <div class="container__one-half card bg--dark-blue">
        <div class="card__info">
          <h2>Get help with Greenfield Workspace</h2>
          <ul>
            <li><a href="https://home.meditech.com/en/d/restapiresources/pages/apiterms.htm">Terms of Use</a></li>
            <li><a href="https://www.ftc.gov/tips-advice/business-center/guidance/mobile-health-apps-interactive-tool">FTC: Mobile Health Apps Interactive Tool</a></li>
            <li><a href="https://info.meditech.com/subscribe-for-greenfield-updates">Subscribe for Greenfield Updates</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- END block -->



  <!-- START faqs -->
  <div id="faqs" class="container container__centered">
    <h2>FAQs</h2>

    <div class="accordion">
      <ul class="accordion__list">

        <li id="item" class="accordion__list__item">
          <a class="accordion__link" href="#">What is the value in API integration?<div class="accordion__list__control"></div></a>
          <div class="accordion__dropdown">
            <div class="js__seo-tool__body-content">
              <p>Standards-based APIs are game changers for interoperable healthcare. By giving providers, patients, and consumers direct access to specific data points through convenient apps, they can see and use information in ways they never could before. Apps give providers more freedom and flexibility to work the way they want to. In addition, consumers will have new vehicles for accessing and interacting with their own health data. The result is healthcare innovation that leads to more satisfying user experiences for all.</p>
            </div>
            <a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close</a>
          </div>
        </li>

        <li id="item" class="accordion__list__item">
          <a class="accordion__link" href="#">Which MEDITECH releases and platforms include these APIs?<div class="accordion__list__control"></div></a>
          <div class="accordion__dropdown">
            <div class="js__seo-tool__body-content">
              <p>Any US MEDITECH customer using our certified 2015 edition software can access APIs that provide patients with access to their own data. USCDI-related APIs are available to all customers. Future API development within MEDITECH Greenfield Workspace — including FHIR Scheduling — is available to Expanse customers.</p>
            </div>
            <a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close</a>
          </div>
        </li>

        <li id="item" class="accordion__list__item">
          <a class="accordion__link" href="#">What use cases does Greenfield currently support?<div class="accordion__list__control"></div></a>
          <div class="accordion__dropdown">
            <div class="js__seo-tool__body-content">
              <p>Greenfield currently supports the Common Clinical Data Set for patient-facing applications, FHIR Scheduling, USCDI R4 (augmenting what is currently available with the Common Clinical Data Set), and documentation for Outbound HL7 V2 Interfaces.</p>
            </div>
            <a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close</a>
          </div>
        </li>

        <li id="item" class="accordion__list__item">
          <a class="accordion__link" href="#">Can MEDITECH customers request access to Greenfield Workspace or is it only for vendors developing apps?<div class="accordion__list__control"></div></a>
          <div class="accordion__dropdown">
            <div class="js__seo-tool__body-content">
              <p>Customers who are interested in developing their own apps are welcome to request access to Greenfield Workspace.</p>
            </div>
            <a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close</a>
          </div>
        </li>

        <li id="item" class="accordion__list__item">
          <a class="accordion__link" href="#">As a developer, how can I see what APIs are available?<div class="accordion__list__control"></div></a>
          <div class="accordion__dropdown">
            <div class="js__seo-tool__body-content">
              <p>Our testing environment includes a list of available <a href="https://home.meditech.com/restapiresources/index.html">Common Clinical Data Set APIs</a> and associated documentation. <a href="https://info.meditech.com/welcome-to-meditech-greenfield-rfi">Register now</a> to get started.</p>
            </div>
            <a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close</a>
          </div>
        </li>

        <li id="item" class="accordion__list__item">
          <a class="accordion__link" href="#">What API do I use for USCDI data?<div class="accordion__list__control"></div></a>
          <div class="accordion__dropdown">
            <div class="js__seo-tool__body-content">
              <p>US Core FHIR R4 should be used for USCDI data. This version of the patient access FHIR APIs will conform to the FHIR US Core Implementation Guide and contain the USCDI data elements.</p>
            </div>
            <a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close</a>
          </div>
        </li>

        <li id="item" class="accordion__list__item">
          <a class="accordion__link" href="#">Does connecting to Greenfield Workspace enable my application to connect to all healthcare organizations using MEDITECH?<div class="accordion__list__control"></div></a>
          <div class="accordion__dropdown">
            <div class="js__seo-tool__body-content">
              <p>No, Greenfield Workspace is MEDITECH's developer environment. Upon receiving access to Greenfield Workspace, you'll be given credentials and endpoints that enable you to connect to the Workspace specifically. Greenfield Workspace is not connected to any customer environments, therefore access to Greenfield Workspace doesn't give you access to MEDITECH's customers.</p>
              <p>Each MEDITECH customer will have their own policies for connecting applications to their system. You can expect a process similar to registering for access to Greenfield to be required for each MEDITECH customer. MEDITECH doesn't grant access to specific applications, but rather helps to facilitate a connection between your application and our customers per our customers' request</p>
            </div>
            <a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close</a>
          </div>
        </li>

        <li id="item" class="accordion__list__item">
          <a class="accordion__link" href="#">Should developers still use HL7 interfaces when developing API solutions?<div class="accordion__list__control"></div></a>
          <div class="accordion__dropdown">
            <div class="js__seo-tool__body-content">
              <p>Some integrations and desired workflows will require the use of HL7 v2 interfaces. You can use the Greenfield service desk to discuss your use cases and what HL7 interfaces may help support your solution. The following common HL7 specifications are listed below and the technical specifications can be viewed here:</p>
              <ul>
                <li>Administered Medications Outbound</li>
                <li>ADT OUT</li>
                <li>Assessment Outbound</li>
                <li>Charges Outbound</li>
                <li>Home Meds Outbound</li>
                <li>Immunizations Interface</li>
                <li>OM Diet Orders</li>
                <li>OM Orders Outbound</li>
                <li>Reports Out (PTH) </li>
                <li>Result Out (LAB)</li>
                <li>SCH Appt Out</li>
                <li>Syndromic Surveillance</li>
                <li>Telephony Bed Status</li>
              </ul>

              <p>This list of HL7 interfaces is not exhaustive and other HL7 interfaces may be available. Customers with questions about an HL7 interface, should contact their MEDITECH Interoperability specialist. </p>

              <p>Vendors or developers with additional questions regarding outbound HL7 interface documentation can reach out to our <a href="https://home.meditech.com/webforms/contact.asp?rcpt=greenfieldinfo|meditech|com&rname=greenfieldinfo">MEDITECH Greenfield support team</a>.</p>
            </div>
            <a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close</a>
          </div>
        </li>

      </ul>
    </div>

  </div>
  <!-- END faqs -->



  <!-- Block -->
  <div class="container container__centered center icons">
    <h2 style="margin-bottom:2em;">Get started in the Greenfield Workspace</h2>
    <div class="card__wrapper">

      <div class="container__one-fourth card bg--emerald">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/MEDITECH-Greenfield-Workspace-logo--white-knockout.svg" width="50" height="50" style="width:60%; height:auto; margin-top:2.5em; margin-bottom:0.5em;">
        <div class="card__info">
          <p><a href="https://ehr.meditech.com/ehr-solutions/greenfield-workspace">About Greenfield Workspace</a></p>
        </div>
      </div>
      <div class="container__one-fourth card bg--emerald">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--learning.svg" width="50" height="50">
        <div class="card__info">
          <p><a href="https://ehr.meditech.com/ehr-solutions/how-to-work-in-the-greenfield-workspace">How to work in the Greenfield Workspace</a></p>
        </div>
      </div>
      <div class="container__one-fourth card bg--dark-blue">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--construction.svg" width="50" height="50">
        <div class="card__info">
          <p><a href="https://greenfield.meditech.com/">Enter the Greenfield Workspace</a></p>
        </div>
      </div>
      <div class="container__one-fourth card bg--dark-blue">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--email.svg" width="50" height="50">
        <div class="card__info">
          <p><a href="https://jira.meditech.com/servicedesk/customer/portal/1">Contact our Service Desk for help</a></p>
        </div>
      </div>

    </div>
  </div>
  <!-- End Block -->


  <!-- Block -->
  <div class="container">
    <div class="container__centered center">
      <h2>MEDITECH Greenfield Alliance</h2>
      <p>MEDITECH is creating an ecosystem of partner organizations with proven, successful, and interoperable solutions.</p>
      <a href="https://ehr.meditech.com/ehr-solutions/greenfield-alliance" class="btn--orange btn--outline">Learn more about Greenfield Alliance</a>

      <div style="margin-top:2em;">
        <?php print $share_link_buttons; ?>
      </div>
    </div>
  </div>
  <!-- End Block -->



  <?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>

  <!-- END campaign--node-3704.php -->
