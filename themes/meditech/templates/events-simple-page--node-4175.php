<!-- start events-simple-page--node-4175.php -->
<?php // IF is UK webinar page ============================================================================= ?>

  <section class="container__centered">
    <div class="container__two-thirds">

      <h1 class="page__title js__seo-tool__title"><?php print $title; ?></h1>

      <div class="js__seo-tool__body-content">
        <?php print render($content['field_body']); ?>
      </div>
        
    </div><!-- END container__two-thirds -->

    <!-- SIDEBAR -->
    <aside class="container__one-third">
      &nbsp;
    </aside>
    <!-- END SIDEBAR -->
  </section>
<!-- end events-simple-page--node-4175.php -->