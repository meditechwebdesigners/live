<!-- START campaign--node-2956.php Physician CAMPAIGN -->

<?php
$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
  .small_popup_video {
    width: 100%;
    border: 4px solid #e6e9ee;
    border-radius: 50%;
  }

  .vid-bg {
    width: 55px;
    height: 55px;
  }

  .mag-icon {
    top: 52%;
    left: 51%;
  }

  @media (max-width: 40em) {
    .vid-bg {
      width: 40px;
      height: 40px;
    }

    .open-modal:hover .vid-bg {
      height: 45px;
      width: 45px;
    }

    .open-modal:hover .mag-icon {
      font-size: 18px;
    }
  }

</style>


<div class="js__seo-tool__body-content">

  <!-- Hero -->
  <div class="container no-pad background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dr_smiling_at_patient2.jpg);">
    <div class="container__centered bg-overlay--white">

      <div class="container__one-half center text-align left" style="padding: 4em 0;">
        <h1 class="js__seo-tool__title">Expanse for Physicians.</h1>
        <h3>With MEDITECH, physicians can focus on their patients, not their EHR.</h3>
        <p>The user-friendly design of Expanse helps physicians provide safe, sound and efficient care, no matter what setting they're in.</p>

        <div class="center" style="margin-top:2em;">
          <?php hubspot_button($cta_code, "Register for MEDITECH's 2019 Physician and CIO Forum"); ?>
        </div>
      </div>

    </div>
  </div>
  <!-- Hero Block -->


  <!-- Block 2 -->
  <div class="container no-pad background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dr_smiling_at_tablet1.jpg);">
    <div class="container__centered bg-overlay--black">

      <div class="container__one-third">
        &nbsp;
      </div>

      <div class="container__two-thirds text--white" style="padding: 2em 0;">
       
        <h2 class="js__seo-tool__title">Increase your efficiency, naturally.</h2>
        <p>We know you're already efficient. We think your EHR should be efficient, too.
          With Expanse, improved efficiency comes naturally — from more intuitive, <a href="https://blog.meditech.com/how-meditech-fulfills-amas-8-ehr-usability-principles">usable</a> software. The result is <a href="https://ehr.meditech.com/search-results?as_q=%23physician+burnout">happier physicians</a> who can finish their work at work and not take it home with them. Physicians who are more satisfied with their jobs because they can focus on what they enjoy doing: caring for their patients. And that makes everyone happier: Physicians. Patients. Communities.</p>

        <article class="container__centered">
          <figure class="container__one-fourth center">

            <!-- Start hidden modal box -->
            <div id="modal1" class="modal">
              <a class="close-modal" href="javascript:void(0)">&times;</a>
              <div class="modal-content" style="z-index:10001;">
                <iframe src="https://player.vimeo.com/video/274880053" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
              </div>
            </div>
            <!-- End hidden modal box -->
            <!-- Start modal trigger -->
            <div class="open-modal" data-target="modal1">
              <img class="small_popup_video" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/dr_doug_kanis.jpg" onerror="this.src='https://ehr.meditech.com/sites/all/themes/meditech/images/quote.svg';this.onerror=null;" alt="quote bubble graphic">
              <div class="vid-bg">
                <!-- Include if using image trigger -->
                <i class="mag-icon fas fa-play"></i>
              </div>
            </div>
            <!-- End modal trigger -->

          </figure>

          <div class="container__three-fourths">
            <div>
              <h4>How I Get Home on Time [VIDEO]</h4>
              <p style="font-size: 1em;">
                Before Expanse, Doug Kanis spent a lot of time after hours finishing his documentation. Today, he finishes his work at work. Hear how he gets out of work on time, even on his busiest days.
              </p>
              <h6> <i>Doug Kanis, DO</i> </h6>
            </div>
          </div>
        </article>
        
      </div>

    </div>
  </div>
  <!-- End Block 2 -->



  <!-- Block 3 -->
  <div class="container bg--green-gradient">

    <div class="container__centered text--white auto-margins">
      <h2>It's <u>Your</u> Thing.</h2>
      <p>Physicians can personalize their Expanse EHR, so it works for them, not against them.
        They choose from a <a href="https://ehr.meditech.com/ehr-solutions/meditech-acute">library of intuitive widgets and shortcuts</a>, create their own customized order sets, and streamline their most common tasks so that the EHR is tailored to their personal workflows — not the other way around. Why shouldn't your EHR be as easy and enjoyable to use as your smartphone and favorite apps? </p>
    </div>

    <!-- Start hidden modal box -->
    <div id="modal3" class="modal">
      <a class="close-modal" href="javascript:void(0)">&times;</a>
      <div class="modal-content">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/my_widget_vital_signs.jpg" alt="my widget--vital signs"> <!-- Add modal content here -->
      </div>
    </div>
    <!-- End hidden modal box -->
    <div class="container__centered">
     
      <div class="container__two-thirds">
        <figure style="text-align:center; padding-top: 2em; padding-bottom: 2em;">
          <!-- Start modal trigger -->
          <div class="open-modal" data-target="modal3">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/vital_signs_no_callout.jpg" alt="my widget--vital signs" style="width:100%;">
            <div class="mag-bg">
              <!-- Include if using image trigger -->
              <i class="mag-icon fas fa-search-plus"></i>
            </div>
          </div>
          <!-- End modal trigger -->
        </figure>
      </div>

      <article class="container__one-third">
       
        <figure class="container__centered">
          <!-- Start hidden modal box -->
          <div id="modal2" class="modal">
            <a class="close-modal" href="javascript:void(0)">&times;</a>
            <div class="modal-content" style="z-index:10001;">
              <iframe src="https://player.vimeo.com/video/239126049" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
            </div>
          </div>
          <!-- End hidden modal box -->
          <!-- Start modal trigger -->
          <div class="open-modal" data-target="modal2">
            <div class="container__centered">
              <img class="small_popup_video" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/dr_m_surburg.jpg" onerror="this.src='https://ehr.meditech.com/sites/all/themes/meditech/images/quote.svg';this.onerror=null;" alt="quote bubble graphic" style="max-width: 162px;">
            </div>
            <div class="vid-bg">
              <!-- Include if using image trigger -->
              <i class="mag-icon fas fa-play"></i>
            </div>
          </div>
          <!-- End modal trigger -->
        </figure>

        <div class="container__centered">
          <div>
            <h4>How I Make My EHR My Own [VIDEO]</h4>
            <p style="font-size: 1em;">Learn how physicians personalize their Expanse EHR and make it their own.</p>
            <h6><i>Jeffrey Schleich, MD
              <br>Matthew Surburg, MD
              <br>John Tollerson, DO</i></h6>
          </div>
        </div>
        
      </article>
      
    </div>
    
  </div>
  <!-- End Block 3 -->



  <!-- Block 4 -->
  <div class="container no-pad background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dr_using_mobile1.jpg);">
    <div class="container__centered bg-overlay--white">

      <div class="container__two-thirds" style="padding: 2em 0;">
       
        <h2 class="js__seo-tool__title">Physician unbound.</h2>
        <p>Cut the ties that chain you to your desk, with an EHR that goes anywhere, and everywhere, you do. Expanse is the first natively web-based EHR, which means it's <a href="https://ehr.meditech.com/news/meditech-enhances-mobility-for-nurses-with-expanse-point-of-care">optimized for mobility</a>. Stay patient-facing as you tap and swipe through charts using techniques you already know and use with your personal devices and apps. Expanse is an EHR that doesn't tie you down and never gets between you and your patients.</p>

        <article class="container__centered">
         
          <figure class="container__one-fourth center">
            <!-- Start hidden modal box -->
            <div id="modal4" class="modal">
              <a class="close-modal" href="javascript:void(0)">&times;</a>
              <div class="modal-content" style="z-index:10001;">
                <iframe src="https://player.vimeo.com/video/261876988" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
              </div>
            </div>
            <!-- End hidden modal box -->
            <!-- Start modal trigger -->
            <div class="open-modal" data-target="modal4">
              <img class="small_popup_video" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/dr_d_harrigan.jpg" onerror="this.src='https://ehr.meditech.com/sites/all/themes/meditech/images/quote.svg';this.onerror=null;" alt="quote bubble graphic">
              <div class="vid-bg">
                <!-- Include if using image trigger -->
                <i class="mag-icon fas fa-play"></i>
              </div>
            </div>
            <!-- End modal trigger -->
          </figure>

          <div class="container__three-fourths">
            <div>
              <h4>How Mobility Helps Me Provide Better, More Efficient Care [VIDEO]</h4>
              <p style="font-size: 1em;">
                Hear how a mobile EHR helps Dr. Harrigan stay focused on her patients and provide more efficient care.
              </p>
              <h6> <i>Deborah Harrigan, MD
                <br>John Tollerson, DO</i> </h6>
            </div>
          </div>
          
        </article>
        
      </div>

    </div>
  </div>
  <!-- Block 4 -->

  <!-- New Expanse Now Block -->

          <div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Light-gray-gradient-triangle--bg.png);padding: 6em 0;">
            <div class="container__centered">
                <div class="container__one-third">
                  <div class="expanse-logo">
                      <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/expanse-now-logo.png" alt="MEDITECH Expanse logo" style="margin-top: 1em;">
                  </div>
                </div>
                <div class="container__two-thirds">
                    <h2>When you need it, you need it <u>now</u>.</h2>
                    <p>That’s why we created Expanse Now, our physician mobility app for Android and iOS smartphones. Securely access your Expanse EHR wherever you are, using intuitive mobile device conventions and voice commands. Remotely manage routine tasks from the palm of your hand, and ensure the highest levels of care coordination and communication with your patients and care teams. <a href="https://ehr.meditech.com/ehr-solutions/ehr-mobility">Learn more</a>.</p>

                </div>
            </div>
        </div>
  <!-- End of Expanse Now Block -->

  <!-- Block 5 -->
  <div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/quote_bg.jpg);">
    <div class="container__centered">
      <div class="container__one-half">
        <div class="transparent-overlay">
          <div class="text--fresh-mint" style="text-align:center; padding-bottom:1em;"><i class="far fa-comments fa-3x"></i></div>
          <div class="text--white">
            <p>"Using a tablet is a game changer. Going from room-to-room with it, placing orders at the bedside, reviewing them with patients, discharging, reviewing patient charts in the rooms with them has been a major positive change. It's a big win for doctors and the patient experience."</p>
            <p class="text--large no-margin--bottom">Daniel Peterson, MD</p>
            <p>Halifax Health Daytona, FL</p>
          </div>
        </div>
      </div>
      <div class="container__one-half">
        <div class="transparent-overlay">
          <div class="text--fresh-mint" style="text-align:center; padding-bottom:1em;"><i class="far fa-comments fa-3x"></i></div>
          <div class="text--white">
            <p>"The ability to stay mobile at work is one of my favorite MEDITECH features. It makes me faster, more efficient, and able to stay close to my critical patients."</p>
            <p class="text--large no-margin--bottom">Elisabeth Moore, MD</p>
            <p>Androscoggin Valley Hospital Berlin, NH</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Block 5 -->

  <!-- New Efficiency Dashboard Block -->
    
      <div class="container background--cover"style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/back-angle-runner-on-road4.png);">
        <div class="container__centered">
          <div class="container__one-half transparent-overlay text--white">
            <h2>Full speed ahead</h2>
                <p>Empower your physicians to get the most from their EHR with the Expanse Efficiency Dashboard. With detailed user metrics and real-time reporting, the dashboard offers actionable insights for adjusting workflows and personalizing the EHR experience to each provider’s needs. The result is happier physicians who get their work done on time and focus on what’s most important to them &mdash; their patients. Identify opportunities for targeted training, EHR personalization, and individualized support, so you can ensure confident, proficient, satisfied users. </p>
            </div>
            <div class="container__one-half">
                 &nbsp;
            </div>
            
        </div>
      </div>

    <!-- End Efficiency Dashboard Block -->

  <!-- Block 6 -->
  <div class="container background">
    <div class="container__centered center">
      <div>
        <h2>Tear down these walls.</h2>
        <p>
          True to its name, Expanse knows no boundaries. It's a single EHR spanning <a href="https://ehr.meditech.com/news/meditech-s-web-ehr-improves-continuity-of-care-at-kalispell-regional">all care settings.</a> One schedule, with appointments across facilities. One set of allergies, medications, and problems. All data immediately available everywhere — with the specific tools, data views, and workflows you need in your own setting. That's the power of one EHR everywhere. That's the power of Expanse.
        </p>
      </div>
      <div class="page__title--center">
        <div class="container__one-third" style="margin-top: 2em; margin-bottom: 2em;">
          <img style="width: 100px;" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/EHR-icon-Hospital.svg" onerror="this.src='https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/EHR-icon-Hospital.png';this.onerror=null;" alt="Hospital Icon">
          <h4><a href="https://ehr.meditech.com/ehr-solutions/meditech-acute" rel="noreferrer noopener">Expanse in<br>the Hospital</a></h4>
        </div>
        <div class="container__one-third" style="margin-top: 2em; margin-bottom:2em;">
          <img style="width: 100px;" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/EHR-icon-Practice.svg" onerror="this.src='https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/EHR-icon-Practice.png';this.onerror=null;" alt="Medical Bag Icon">
          <h4><a href="https://ehr.meditech.com/ehr-solutions/meditech-ambulatory" rel="noreferrer noopener">Expanse in
              <br>the Practice</a></h4>
        </div>
        <div class="container__one-third" style="margin-top: 2em; margin-bottom:2em;">
          <img style="width: 100px;" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/EHR-icon-ED.svg" onerror="this.src='https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/EHR-icon-ED.png';this.onerror=null;" alt="EM Vehicle Icon">
          <h4><a href="https://ehr.meditech.com/ehr-solutions/meditech-ed" rel="noreferrer noopener">Expanse
              <br>the ED</a></h4>
        </div>

      </div>
    </div>
  </div>
  <!-- End of Block 6 -->


  <!-- Block 7 -->
  <div class="container no-pad background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/hikers_helping1.jpg);">

    <div class="container__centered bg-overlay--white">

      <div class="container__one-third">
        &nbsp;
      </div>

      <div class="container__two-thirds" style="padding: 4em 0;">
       
        <h2 class="js__seo-tool__title">All together now.</h2>
        <p>Physicians know that more data is not always better. They need the right data, presented the right way, at the right time, to make the right decisions. Expanse brings together relevant data from every care setting, both inside and outside the organization. With its robust <a href="https://ehr.meditech.com/ehr-solutions/meditech-interoperability">interoperability</a> features, Expanse can gather data from other healthcare organizations, Health Information Exchanges, public health registries, Prescription Drug Monitoring Programs, and beyond &mdash; and embed it in existing workflows so you don't have to go outside to find it. <a href="https://www.youtube.com/watch?v=M4XliT60kuI">Traverse</a> the healthcare expanse with MEDITECH.</p>

        <article class="container__centered transparent-overlay" style="background-color: rgba(255, 255, 255, 0.7);">

          <div class="container__three-fourths">
            <div>
              <h4>How Interoperability Benefits Physicians and Patients [VIDEO]</h4>
              <p style="font-size: 1em;">
                Learn how Expanse's real-time interoperability helps physicians provide better, safer, and more efficient care.
              </p>
              <h6><i>Michael Burke, MS, RN
                <br>Gerald Greeley, CHCIO
                <br>Louis B. Harris, MD </i></h6>
            </div>
          </div>

          <figure class="container__one-fourth center">
            <!-- Start hidden modal box -->
            <div id="modal5" class="modal">
              <a class="close-modal" href="javascript:void(0)">&times;</a>
              <div class="modal-content" style="z-index:10001;">
                <iframe src="https://player.vimeo.com/video/311922713" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
              </div>
            </div>
            <!-- End hidden modal box -->
            <!-- Start modal trigger -->
            <div class="open-modal" data-target="modal5">
              <img class="small_popup_video" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/g_greely.jpg" onerror="this.src='https://ehr.meditech.com/sites/all/themes/meditech/images/quote.svg';this.onerror=null;" alt="quote bubble graphic">
              <div class="vid-bg">
                <!-- Include if using image trigger -->
                <i class="mag-icon fas fa-play"></i>
              </div>
            </div>
            <!-- End modal trigger -->
          </figure>

        </article>
        
      </div>

    </div>
  </div>
  <!-- Block 7 -->


  <!-- Block 8 -->
  <div class="container no-pad background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/nurse_helping_woman_with_walker.jpg);">
    <div class="container__centered bg-overlay--white">

      <div class="container__two-thirds" style="padding: 4em 0;">
       
        <h2 class="js__seo-tool__title">We've got your back.</h2>
        <p>No one supports physicians like MEDITECH. We believe that<a href="https://ehr.meditech.com/ehr-solutions/clinical-decision-support"> Clinical Decision Support</a> should do just that: support and guide you — never disrupt, distract, or dictate your decision making. That's why we've loaded Expanse with pre-built content, curated by experts, and provided at the point of care. Evidence-based content, including hundreds of specialty templates developed by practicing physicians, and robust <a href="https://ehr.meditech.com/ehr-solutions/meditech-ehr-excellence-toolkits">toolkits</a> for CAUTI, sepsis, fall risk, diabetes management, and more. Everything needed to support sound clinical decision making, with nothing to get in your way.</p>

        <article class="container__centered">

          <figure class="container__one-fourth center">
            <!-- Start hidden modal box -->
            <div id="modal6" class="modal">
              <a class="close-modal" href="javascript:void(0)">&times;</a>
              <div class="modal-content" style="z-index:10001;">
                <iframe src="https://player.vimeo.com/video/313253834" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
              </div>
            </div>
            <!-- End hidden modal box -->
            <!-- Start modal trigger -->
            <div class="open-modal" data-target="modal6">
              <img class="small_popup_video" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/dr_b_bagdasian.jpg" onerror="this.src='https://ehr.meditech.com/sites/all/themes/meditech/images/quote.svg';this.onerror=null;" alt="quote bubble graphic">
              <div class="vid-bg">
                <!-- Include if using image trigger -->
                <i class="mag-icon fas fa-play"></i>
              </div>
            </div>
            <!-- End modal trigger -->
          </figure>

          <div class="container__three-fourths">
            <div>
              <h4>Expanse Toolkits: Clinical Decision Support the Right Way [VIDEO]</h4>
              <p style="font-size: 1em;">
                Dr. Bagdasian explains how MEDITECH's toolkits support sound clinical decision-making and make best practices second nature.
              </p>
              <h6> <i>Bryan Bagdasian, MD</i> </h6>
            </div>
          </div>

        </article>
        
      </div>

    </div>
  </div>
  <!-- Block 8 -->

</div>
<!-- end js__seo-tool__body-content -->

<!-- Block 9 - CTA Block -->
<div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-boxes-pattern.jpg);">
  <div class="container__centered auto-margins text--white" style="text-align: center;">

    <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
      <h2 class="text--white">
        <?php print $cta->field_header_1['und'][0]['value']; ?>
      </h2>
    <?php } ?>

    <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
      <div>
        <?php print $cta->field_long_text_1['und'][0]['value']; ?>
      </div>
    <?php } ?>

    <div class="center" style="margin-top:2em;">
      <?php hubspot_button($cta_code, "Sign up for the Mastering the Physician Mindset Webinar"); ?>
    </div>

    <div style="margin-top:1em;">
      <?php print $share_link_buttons; ?>
    </div>

  </div>
</div>
<!-- End Block 9 -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>

<!-- END campaign--node-2956.php Physician CAMPAIGN -->