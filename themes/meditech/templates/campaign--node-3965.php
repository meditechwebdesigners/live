<!-- start campaign--node-3965.php template -- Global page -->
<?php 
$url = $GLOBALS['base_url']; // grabs the site url
include('inc-share-buttons.php');
$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<div class="js__seo-tool__body-content">

    <script src="<?php print $url; ?>/sites/all/themes/meditech/js/splide.min.js"></script>

    <style>
        /* Quotes */
        .quote-box {
            padding: 3em;
            background-color: rgba(3, 3, 30, .5);
            border-left: 5px solid #00bc6f;
            margin-bottom: 2.35765%;
            border-radius: 7px;
            box-shadow: 0px 0px 20px 1px rgba(0, 0, 0, .2);
        }

        /*	Universal Styles*/
        .card {
            box-shadow: none;
            border: 1px solid #c8ced9;
        }

        .content__callout,
        .content__callout__content {
            background-color: transparent;
        }

        /* VIDEO  */
        .video--loop-container {
            min-height: 600px;
            max-height: 650px;
        }

        @media (max-width: 800px) {
            .video--loop-container {
                min-height: 480px;
            }
        }

        .video--loop-container video {
            /* Make video to at least 100% wide and tall */
            min-width: 100%;
            min-height: 100%;
            /* Setting width & height to auto prevents the browser from stretching or squishing the video */
            width: auto;
            height: auto;
            /* Center the video */
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }

        .video--title-3 {
            width: 100%;
            margin-left: auto;
            margin-right: auto;
            margin-top: 4em;
            margin-bottom: 1em;
            position: relative;
            color: #fff;
            z-index: 3;
        }

        .hero-image-container {
            position: relative;
        }

        .hero-image-container img {
            border-radius: 7px;
            margin: 2em 0 0 0;
            max-width: 100%;
            z-index: 1;
            position: relative;
        }

        @media all and (max-width: 50em) {
            .flex-order--reverse {
                order: -1;
            }

            .hero-image-container img {
                margin: 0;
            }
        }

        .container.card figure img,
        .container__one-half.card figure img {
            height: 150px;
        }

        /*	Splide Carousel Styles*/
        #primary-slider .card {
            box-shadow: none;
            border: 1px solid #c8ced9;
            background-color: transparent;
            align-self: flex-start;
        }

        #primary-slider .card__info {
            padding: 2em;
        }

        .splide__pagination {
            text-align: center;
        }

        .splide__slide:focus-visible {
            border: 2px solid #fff !important;
        }

        .headshot-container {
            display: flex;
            align-items: center;
            margin-top: 1.5em;
        }

        .headshot {
            max-width: 90px;
            max-height: 90px;
            flex-grow: 1;
            margin-right: 1.5em;
            border-radius: 50%;
        }

        .headshot-img {
            border-radius: 50%;
        }

        .quote-name {
            flex-grow: 2;
        }

        .quote-name p {
            margin-bottom: 0;
        }

        /* <!-- Outlined Button --> */
        .btn--orange:hover {
            -webkit-transition: all 400ms ease-in-out;
        }

        /* MEDIA */
        @media all and (max-width: 56.250em) {
            .card {
                width: 100%;
                margin-left: 0;
                margin-right: 0;
            }
        }

    </style>


    <!--Block 1-->
    <div class="video--loop-container">
        <div class="video--overlay" style="background:none;"></div>
        <video class="video--loop" preload="auto" autoplay loop muted playsinline>
            <source src="https://player.vimeo.com/external/267652677.hd.mp4?s=f6bd1afcfe2c8f517cdb239f9ab8b73220de7642&profile_id=174">
        </video>
        <div class="container__centered video--title-3">
            <div class="container__two-thirds">
                <div class="transparent-overlay--xp text--white">
                    <h1>
                        MEDITECH's Global Impact
                    </h1>
                    <h2>
                        We're making a world of difference.
                    </h2>
                    <p>
                        Healthcare technology is all about possibilities. Those possibilities are limitless with <a href="https://ehr.meditech.com/ehr-solutions/meditech-expanse">MEDITECH Expanse</a>, our mobile, web-based EHR. Organizations around the world are transforming healthcare using our innovative technology, which provides them with the data they need to treat the whole patient, no matter where they're receiving care.
                    </p>
                </div>
            </div>
            <div class="container__one-third">&nbsp;</div>
        </div>
    </div>
    <!--End of Block 1-->


    <!--NEW Block 2 -->
    <div class="content__callout" style="background-color: #fff;">
        <div class="content__callout__media">

            <div class="content__callout__image-wrapper">
                <div class="video js__video" data-video-id="585444917">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-Podcast--video-overlay.jpg" alt="Video Covershot - MEDITECH Podcast">
                    </figure>
                    <a class="video__play-btn" href="https://vimeo.com/585444917"></a>
                    <div class="video__container"></div>
                </div>
            </div>
        </div>
        <div class="content__callout__content" style="background-color: #fff;">
            <div class="content__callout__body">
                <div class="content__callout__body__text">
                    <h2>MEDITECH's Podcast series is growing!</h2>
                    <p>MEDITECH Associate Vice President Christine Parent has hosted many great conversations with MEDITECH customers and health IT thought leaders. Give a listen as she chats with our guests about their real-world challenges and topics that are important to our industry, such as healthcare equity with Jennifer Zelmer, PhD, President and CEO of Healthcare Excellence Canada, Dr. K. Nadeem Ahmed Chief Medical Information Officer at Aga Khan University Hospital, and Dr. Boniface Mativa, Acting Chief Medical Information Officer at Aga Khan University Hospital Nairobi.</p>
                    <p>On the MEDITECH <a href="https://ehr.meditech.com/podcast-index">podcasts</a> you may learn something personal about our hosts and guests you never knew. Find us wherever you like to listen to podcasts, including <a href="https://podcasts.apple.com/us/podcast/meditech-podcast/id1566866668">Apple</a>, <a href="https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy5zaW1wbGVjYXN0LmNvbS9RTFYxZE93Ng==">Google</a>, <a href="https://open.spotify.com/show/7q7wbf2Q6J1y5IGItYrBxR">Spotify</a>, and <a href="https://meditech-podcast.simplecast.com/">Simplecast</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Block 2 -->


    <!-- End of Block 3: Canada -->
    <div class="container no-pad">
        <div class="gl-container">
            <div class="container__one-half gl-text-pad bg--purple-gradient" style="padding: 5em !important;">
                <h2 class="header-one">Connecting Canada with MEDITECH's EHR</h2>
                <p>As the leading supplier of multi-site and clustered environments in Canada and with 47% of the hospital market share, our customers represent geographic regions, health authorities, and hospitals of all types and sizes. Find out how we're connecting them with more efficient EHR tools and integration that spans all settings.</p>
                <div style="margin-top:2em;">
                    <?php hubspot_button('9dcbc4c4-bfcb-41ab-a4a3-4bfa592ed9b2', "Learn more about MEDITECH in Canada"); ?>
                </div>
            </div>
            <div class="container__one-half background--cover flex-order--reverse" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/international/canada/canadian-flag-flying-over-quebec-city.jpg); min-height:400px; background-position: left;"></div>
        </div>
    </div>
    <!-- End of Block 3: Canada -->


    <!-- Block 4 -->
    <div class="container bg--white">
        <div class="container__centered">

            <div class="center auto-margins" style="margin-bottom: 2em;">
                <h2>Extending Our Reach</h2>
                <p>MEDITECH has a long history of shaping healthcare around the globe. Offices in the <a href="<?php print $url; ?>/global/meditech-uk-ireland"><b>UK</b></a>, <a href="<?php print $url; ?>/global/meditech-south-africa"><b>South Africa</b></a> (covering Africa, Middle East, and South Asia), <a href="<?php print $url; ?>/global/meditech-asia-pacific"><b>Australia, and Singapore</b></a> now extend our reach throughout the following regions:</p>
            </div>

            <div class="card__wrapper">
                <div class="container__one-third card">
                    <figure>
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/international-card-background--uk-ireland.jpg">
                    </figure>
                    <div class="card__info">
                        <div>
                            <h4 style="text-decoration: underline;"><a href="https://ehr.meditech.com/global/meditech-uk-ireland">MEDITECH in the United Kingdom &amp; Ireland:</a></h4>
                        </div>
                        <ul>
                            <li>20+ customers in the UK</li>
                            <li>5 customers in Ireland</li>
                            <li>They represent numerous NHS trusts and public and private sector hospitals and clinics.</li>
                        </ul>
                    </div>
                </div>

                <div class="container__one-third card">
                    <figure>
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/international-card-background--middle-east-south-asia.jpg">
                    </figure>
                    <div class="card__info">
                        <div>
                            <h4 style="text-decoration: underline;"><a href="https://ehr.meditech.com/global/meditech-south-africa">MEDITECH in the <br>Middle East &amp; South Asia:</a></h4>
                        </div>
                        <ul>
                            <li>1 private hospital in Dubai</li>
                            <li>1 hospital in Kuwait</li>
                            <li>1 private hospital in Pakistan</li>
                        </ul>
                    </div>
                </div>

                <div class="container__one-third card">
                    <figure>
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/international-card-background--asia-pacific.jpg">
                    </figure>
                    <div class="card__info">
                        <div>
                            <h4 style="text-decoration: underline;"><a href="https://ehr.meditech.com/global/meditech-asia-pacific">MEDITECH in Asia Pacific:</a></h4>
                        </div>
                        <ul>
                            <li>68 private hospitals in <a href="">Australia</a></li>
                            <li>2 private hospitals and 3 labs in <a href="">Singapore</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="card__wrapper">
                <div class="container no-pad card">
                    <figure>
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/international-card-background--africa.jpg">
                    </figure>
                    <div class="card__info">
                        <div class="container no-pad">
                            <h4 style="text-decoration: underline;"><a href="https://ehr.meditech.com/global/meditech-south-africa">MEDITECH in Africa and South Africa:</a></h4>
                        </div>
                        <div class="container__two-thirds">
                            <p><b>Africa:</b></p>
                            <div class="container__one-half">
                                <ul>
                                    <li>100% public hospitals in Botswana</li>
                                    <li>1 University Hospital in Botswana</li>
                                    <li>100% public and private sector labs in Namibia</li>
                                </ul>
                            </div>
                            <div class="container__one-half">
                                <ul>
                                    <li>100% blood transfusion services Botswana</li>
                                    <li>5 hospitals in Nigeria</li>
                                    <li>1 Private Hospital in Kenya</li>
                                </ul>
                            </div>
                        </div>
                        <div class="container__one-third">
                            <p><b>South Africa:</b></p>
                            <ul>
                                <li>24 hospitals</li>
                                <li>95% commercial labs (760 sites)</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Block 4 -->


    <!-- Start Block 5 -->
    <div class="container bg--purple-gradient text--white">
        <div class="container__centered">
            <div id="primary-slider" class="splide">
                <div class="splide__track">
                    <div class="splide__list">

                        <!-- Connie Dejak Quote -->
                        <li class="splide__slide card" tabindex="0">
                            <div class="card__info">
                                <p class="italic text--large">"By acquiring the Expanse technology, we are also now able to improve our care coordination and internal processes, as well as work more closely with Humber for the betterment of the community."
                                </p>
                                <div class="headshot-container">
                                    <div class="headshot">
                                        <img class="headshot-img" src="<?php print $url; ?>/sites/all/themes/meditech/images/international/canada/headshot--connie-dejak.png" style="max-width:90px; max-height:90px;" alt="Connie Dejak head shot">
                                    </div>
                                    <div class="quote-name">
                                        <p><span class="bold text--meditech-green" style="line-height: 1.4;">
                                                Connie Dejak</span><br>
                                            President and Chief Executive Officer<br>
                                            Runnymede Healthcare Centre</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <!-- End Connie Dejak Quote -->

                        <!-- James Rawlinson Quote -->
                        <li class="splide__slide card" tabindex="0">
                            <div class="card__info">
                                <p class="italic text--large">"As a valued partner, MEDITECH has a straightforward and efficient approach to meeting our evolving needs. They've been able to address our interoperability initiatives throughout our Integrated Care system, region and the NHS. MEDITECH's use of HL7 and FHIR industry standards provides us with a tremendous amount of flexibility, so we can customise and mould our clinical and business workflows to better serve our patients, community, and the Trust."
                                </p>
                                <div class="headshot-container">
                                    <div class="headshot">
                                        <img class="headshot-img" src="<?php print $url; ?>/sites/all/themes/meditech/images/people/james-rawlinson.png" style="max-width:90px; max-height:90px;" alt="James Rawlinson head shot">
                                    </div>
                                    <div class="quote-name">
                                        <p><span class="bold text--meditech-green" style="line-height: 1.4;">
                                                James Rawlinson MBCS</span><br>
                                            Director of Health Informatics<br>
                                            Rotherham Hospital, The Rotherham NHS Foundation Trust
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <!-- End of James Rawlinson Quote -->

                        <!-- David Reilly Quote -->
                        <li class="splide__slide card" tabindex="0">
                            <div class="card__info">
                                <p class="italic text--large">"We wanted to do something based on IHE standards and didn't want to rely on complex integration. We quickly selected MEDITECH for their use of formatted data using XDS standards, and within only six weeks we had developed a CDA feed into the shared record. It was really easy to achieve and we now have a rich set of information."
                                </p>
                                <div class="headshot-container">
                                    <div class="headshot">
                                        <img class="headshot-img" src="<?php print $url; ?>/sites/all/themes/meditech/images/people/David-Reilly.png" style="max-width:90px; max-height:90px;" alt="David Reilly headshot">
                                    </div>
                                    <div class="quote-name">
                                        <p><span class="bold text--meditech-green" style="line-height: 1.4;">David Reilly</span><br>
                                            Head of Interoperability<br>
                                            Alder Hey Children's NHS Foundation Trust
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <!-- End of David Reilly Quote -->

                        <!-- Carl Amrhein Quote -->
                        <li class="splide__slide card" tabindex="0">
                            <div class="card__info">
                                <p class="italic text--large">"The Aga Khan University is committed to providing the communities it serves with quality care. The organization laid out a rigorous selection process for international vendors and after careful deliberation, we are pleased to have chosen MEDITECH as our Electronic Health Record (EHR) system provider. Our EHR implementation is mission-critical for many reasons, but the most important being to enhance the delivery of evidence-based, patient-centered care."</p>
                                <div class="headshot-container">
                                    <div class="headshot">
                                        <img class="headshot-img" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Carl-Amrhein--AKU.jpg" alt="Carl Amrhein head shot">
                                    </div>
                                    <div class="quote-name">
                                        <p><span class="bold text--meditech-green" style="line-height: 1.4;">Carl Amrhein</span><br>
                                            Provost and Vice President Academics<br>
                                            Aga Khan University & Hospitals
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <!-- End of Carl Amrhein Quote -->

                        <!-- Dr. Shawn Bolouki Quote -->
                        <li class="splide__slide card" tabindex="0">
                            <div class="card__info">
                                <p class="italic text--large">"The Aga Khan University & Hospitals are fully committed to the implementation of an Electronic Health Record system that will provide our clinicians with a single source of information for coordination of patient care. This solution will enhance the University Hospital's delivery of evidence-based patient care and facilitate availability of data that will contribute to research and knowledge sharing."</p>
                                <div class="headshot-container">
                                    <div class="headshot">
                                        <img class="headshot-img" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dr-sean-bolouki--headshot.jpg" alt="Shawn Bolouki head shot">
                                    </div>
                                    <div class="quote-name">
                                        <p><span class="bold text--meditech-green" style="line-height: 1.4;">Dr. Shawn Bolouki</span><br>
                                            Vice President, Health Services & Chief Executive Officer<br>
                                            Aga Khan University Hospital, Nairobi
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <!-- End of Dr. Shawn Bolouki Quote -->

                        <!-- Pr. Michael Boyer Quote -->
                        <li class="splide__slide card" tabindex="0">
                            <div class="card__info">
                                <p class="italic text--large">"The implementation of Expanse has created substantial benefits from both an administrative as well as a clinical perspective. From an administrative perspective, ambulatory bookings are more efficient. We've also witnessed a decrease in waiting times in our chemotherapy suite — thanks to changes in administrative processes underpinned by Expanse."</p>
                                <div class="headshot-container">
                                    <div class="headshot">
                                        <img class="headshot-img" src="https://blog.meditech.com/hubfs/Prof%20Boyer_head%20shot.jpg" alt="Professor Michael Boyer headshot">
                                    </div>
                                    <div class="quote-name">
                                        <p><span class="bold text--meditech-green" style="line-height: 1.4;">Professor Michael Boyer</span><br>
                                            Chief Clinical Officer and Medical Oncologist<br>
                                            Chris O'Brien Lifehouse
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <!-- End of Pr. Michael Boyer Quote -->

                        <!-- Kevin Fernandes Quote -->
                        <li class="splide__slide card" tabindex="0">
                            <div class="card__info">
                                <p class="italic text--large">"MEDITECH was instrumental in working with our technology team to successfully integrate more than 15 directly connected interfaces, which enables patient data to be transmitted in real time. Today, we're providing better coordinated care and enabling clinicians to use all of their resources to make more informed decisions."
                                </p>
                                <div class="headshot-container">
                                    <div class="headshot">
                                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/quote.svg" style="max-width:90px; max-height:90px;" alt="Quote graphic">
                                    </div>
                                    <div class="quote-name">
                                        <p><span class="bold text--meditech-green" style="line-height: 1.4;">Kevin Fernandes</span><br>
                                            Chief Technology Officer<br>
                                            Humber River Hospital
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <!-- End of Kevin Fernandes Quote -->

                        <!-- Andy Hart Quote -->
                        <li class="splide__slide card" tabindex="0">
                            <div class="card__info">
                                <p class="italic text--large">"MEDITECH has played an integral role in our journey to becoming an NHS healthcare leader. By leveraging their EPR to provide safe and high quality care, we've seen our Trust become the first within the NHS to achieve all requirements of the national Global Digital Exemplar (GDE) programme, and the first hospital in the North of England to be awarded HIMSS Stage 7. We look forward to continuing our longstanding partnership with MEDITECH and to further advancing care as we move forward with Expanse."
                                </p>
                                <div class="headshot-container">
                                    <div class="headshot">
                                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/quote.svg" style="width:90px; height:90px;" alt="Quote graphic">
                                    </div>
                                    <div class="quote-name">
                                        <p><span class="bold text--meditech-green" style="line-height: 1.4;">Andy Hart</span><br>
                                            Director of Information Management and Technology<br>
                                            South Tyneside and Sunderland NHS Foundation Trust
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <!-- End of Andy Hart Quote -->

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            new Splide('#primary-slider', {
                type: "loop",
                perPage: 2,
                gap: "2em",
                padding: {
                    right: "2em",
                    left: "2em",
                },
                speed: "400",
                pagination: "true",
                keyboard: "true",
                updateOnMove: "true",
                breakpoints: {
                    900: {
                        perPage: 1,
                        gap: "2em",
                        padding: {
                            right: "2em",
                            left: "2em",
                        }
                    }
                }
            }).mount();
        });

    </script>
    <!-- End Block 5 -->


    <!-- START Block 6 -->
    <div class="container bg--white">
        <div class="container__centered text--black-coconut">

            <div class="center" style="margin-bottom: 2em;">
                <h2>Uniting Care in the United Kingdom and Ireland</h2>
                <p>Leading the charge to safer, more holistic care with MEDITECH's integrated solutions also leads to achieving firsts from NHS England and the analytics unit of HIMSS Europe.</p>
            </div>
            <div class="card__wrapper">
                <div class="container__one-half card">
                    <figure>
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/sunderland-uk--global.jpg" alt="Doctor speaking with patient while using tablet">
                    </figure>
                    <div class="card__info">
                        <p class="no-margin--bottom">Two of just four Trusts in the UK that have achieved HIMSS Level 7 inpatient certification are MEDITECH customers.
                        </p>
                        <ul class="fa-ul" style="margin-top:1em;">
                            <li><span class="fa-li"><i class="fas fa-angle-double-right" style="color:darkpurple;" aria-hidden="true"></i></span><a href="https://ehr.meditech.com/news/sunderland-royal-hospital-honoured-with-prestigious-himss-stage-7-recognition">Sunderland Royal Hospital</a> was the first hospital in the North of England to be awarded HIMSS Stage 7.</li>
                            <li><span class="fa-li"><i class="fas fa-angle-double-right" style="color:darkpurple;" aria-hidden="true"></i></span><a href="https://www.digitalhealth.net/2021/11/alder-hey-emram-stage-7-himss">Alder Hey Children's Hospital</a> is the first paediatric hospital in Europe to reach HIMSS Stage 7 inpatient certification.</li>
                        </ul>
                    </div>
                </div>
                <div class="container__one-half card">
                    <figure>
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/galway-city-ireland--global.jpg" alt="Doctor speaking with patient while using tablet">
                    </figure>
                    <div class="card__info">
                        <p class="no-margin--bottom"><a href="https://www.galwayclinic.com/">Galway Clinic</a> (Galway City, Ireland) was the first hospital in Ireland to achieve HIMSS Stage 6 designation, as well as the first to launch Expanse throughout the UK and Ireland.</p>
                    </div>
                </div>
                <div class="container no-pad center" style="margin-top: 1em;">
                    <a href="<?php print $url; ?>/international/meditech-uk-ireland" class="btn--orange">Learn More About MEDITECH UK &amp; Ireland</a>
                </div>
            </div>

        </div>
    </div>
    <!-- END Block 6 -->


    <!-- Block 7 -->
    <div class="gl-container">

        <div class="container__one-half background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/africa-global-page-light.jpg);">

            <div style="padding-top: 6em;">
                <div class="container__centered text--white transparent-overlay--xp" style="background-color: rgba(0, 0, 0, 0.7);">
                    <h2>MEDITECH South Africa</h2>
                    <p>For four decades, <a href="https://ehr.meditech.com/global/meditech-south-africa">MEDITECH South Africa</a> has provided integrated software solutions to healthcare organizations across the globe.</p>
                    <p>We're proud to be partnering with Aga Khan University, an international institution of distinction providing health services to millions of people in the developing world. The Expanse deployment will create the first integrated EHR in East Africa, with a subsequent deployment in Pakistan.</p>

                    <div style="margin-top: 2em;">
                        <a href="https://ehr.meditech.com/international/meditech-south-africa" class="btn--orange">Learn More About MEDITECH South Africa</a>
                    </div>
                </div>
            </div>

        </div>

        <div class="container__one-half gl-text-pad bg--purple-gradient">
            <div class="container__centered">
                <div class="hero-image-container">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Aga-Khan-University-Signing-Group-Shot--edited.jpg" alt="Aga-Khan-University-Signing-Group-Photo" title="" style="">
                </div>
            </div>
        </div>

    </div>
    <!-- END Block 7 -->


    <!--Block 8-->
    <div class="content__callout">
        <div class="content__callout__media">

            <div class="content__callout__image-wrapper">
                <div class="video js__video" data-video-id="174224144">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay-Botswana-The-Power-and-Impact-of-Serving-a-Global-Healthcare-Mission.jpg" alt="Video Covershot">
                    </figure>
                    <a class="video__play-btn" href="https://vimeo.com/174224144"></a>
                    <div class="video__container">
                    </div>
                </div>
            </div>

        </div>
        <div class="content__callout__content">

            <div class="content__callout__body">
                <div class="content__callout__body__text">
                    <h2>
                        One Patient, One Record in Botswana
                    </h2>
                    <p>
                        From sophisticated urban hospitals in the UK and Canada to remote desert health posts in the heart of Africa, we're working with local communities and governments to improve people's health no matter where they live, and no matter the level of IT connectivity.
                    </p>
                </div>
            </div>

        </div>
    </div>
    <!--End of Block 8-->


    <!-- Block 9 (Quote Timothy Low) -->
    <div class="gl-container">

        <div class="container__one-half gl-text-pad bg--purple-gradient">
            <div class="container__centered">
                <h2>MEDITECH Asia Pacific</h2>
                <p>Together, MEDITECH Australia and MEDITECH Singapore are delivering effective and sustainable healthcare solutions to the Asia Pacific region. Our world-class partnerships include <a href="https://www.ramsayhealth.com/Our-Businesses/Ramsay-Australia">Ramsay Health Care</a>, <a href="https://ehr.meditech.com/news/australias-chris-obrien-lifehouse-launches-expanse">Chris O'Brien Lifehouse</a>, and Farrer Park Hospital.</p>

                <div class="quote-box" style="padding:2em; margin-top: 2em;">
                    <p class="italic">"This hospital was planned to be technologically relevant for the next 20 years, because the future of healthcare is going to be digitally-focused and technology-based."</p>
                    <div class="headshot-container">
                        <div class="headshot">
                            <img class="headshot-img" src="<?php print $url; ?>/sites/all/themes/meditech/images/people/dr-timothy-low.png" style="max-width:90px; max-height:90px;" alt="dr. Timothy Low headshot">
                        </div>
                        <p><b>Dr. Timothy Low</b><br>
                            CEO Farrer Park Hospital
                        </p>
                    </div>
                </div>
            </div>

            <div class="center" style="margin-top: 2em;">
                <a href="https://ehr.meditech.com/international/meditech-asia-pacific" class="btn--orange" style="margin-top:0.5em;">Learn More About MEDITECH Asia Pacific</a>
            </div>
        </div>

        <div class="container__one-half background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/polynesian-islands--global.jpg);">
        </div>

    </div>
    <!-- END Block 9 -->


    <style>
        .shadow-box.w-icon {
            padding: 4em 2em 2em 2em;
            position: relative;
            margin-top: 50px;
        }

        .shadow-box.w-icon-outline {
            padding: 4em 2em 2em 2em;
            position: relative;
            margin-top: 50px;
            box-shadow: none;
            border: 1px solid #c8ced9;
            background-color: transparent;
        }

        .shadow-box.outline {
            box-shadow: none;
            border: 1px solid #c8ced9;
            background-color: transparent;
        }

        .icon {
            position: absolute;
            top: -50px;
            left: 50%;
            transform: translate(-50%);
        }

        .icon img {
            width: 100px;
            height: 100px;
            border-radius: 50%;
        }

    </style>

    <!-- START Block 10 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/bg--organic-shapes-green-blue.svg);">
        <div class="container__centered center">
            <div class="auto-margins" style="margin-bottom:2em;">
                <h2>Riding Waves of Change in the Caribbean</h2>
                <p>
                    As digital health records gain momentum in the Caribbean, MEDITECH is expanding its presence with customers throughout the US Virgin Islands, Puerto Rico, the British Virgin Islands, and the Bahamas.
                </p>
                <p>Customers continue to realize the benefits of moving forward with Expanse. MEDITECH is the EHR leader in Puerto Rico with nearly 50% of EHR market share.
                </p>
            </div>
            <div class="container__one-third shadow-box sb-min">
                <img style="width:125px; margin-bottom:1em;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--twenty-percent-increase-global.svg" alt="2020 Black Book Highest Client Satisfaction Award for Inpatient EHR icon">
                <p> Since implementing MEDITECH,<b>Wilma N. Vázquez Medical Center (Vega Baja, PR)</b> has documented a 20% increase in their cash flow and 30% reduction in ED wait times.</p>
            </div>
            <div class="container__one-third shadow-box sb-min">
                <img style="width:125px; margin-bottom:1em;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--connected-nationwide-global.svg" alt="2020 Black Book #1 EHR in Usability for Nurses icon">
                <p><b>The British Virgin Islands Health Authority</b> will soon connect patients and care providers nationwide and across the islands using MEDITECH Expanse. Wherever patients receive care — Peeble's Hospital, outpatient clinics, urgent care centers —&nbsp;their records will follow.</p>
            </div>
            <div class="container__one-third shadow-box sb-min">
                <img style="width:125px; margin-bottom:1em;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--ninetieth-percentile-global.svg" alt="2020 Black Book #1 EHR in Usability for Nurses icon">
                <p>Patients at <b>Doctors Hospital (Nassau, Bahamas)</b> rate the hospital above the 90th percentile. Their MEDITECH solution helps them keep their patients engaged and healthier over the long term.</p>
            </div>
        </div>
    </div>
    <!-- END Block 10 -->


    <!--Block 11-->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/sun-to-the-left-of-earth-with-glowing-cities.jpg); background-position-x:right;">
        <div class="container__centered">
            <div class="container__one-fourth">
                &nbsp;
            </div>
            <div class="container__three-fourths">
                <h2 class="text--white center">
                    Our Global Market Presence
                </h2>
                <div class="container__centered list-style--none">

                    <div class="container__one-fourth text--white">
                        <ul>
                            <li>Australia</li>
                            <li>Bahamas</li>
                            <li>British Virgin Islands</li>
                            <li>Botswana</li>
                            <li>Canada</li>
                            <li>Ghana</li>
                            <li>Honduras</li>
                        </ul>
                    </div>

                    <div class="container__one-fourth text--white">
                        <ul>
                            <li>Ireland</li>
                            <li>Kenya</li>
                            <li>Kuwait</li>
                            <li>Lesotho</li>
                            <li>Mozambique</li>
                            <li>Namibia</li>
                        </ul>
                    </div>

                    <div class="container__one-fourth text--white">
                        <ul>
                            <li>Nigeria</li>
                            <li>Pakistan</li>
                            <li>Puerto Rico*</li>
                            <li>Singapore</li>
                            <li>South Africa</li>
                            <li>Swaziland</li>
                            <li>Uganda</li>
                        </ul>
                    </div>

                    <div class="container__one-fourth text--white">
                        <ul>
                            <li>United Arab Emirates</li>
                            <li>United Kingdom</li>
                            <li>United States</li>
                            <li>Virgin Islands*</li>
                            <li>Zambia</li>
                            <li>Zimbabwe</li>
                        </ul>
                    </div>

                </div>
                <div class="container__centered no-pad text--white">
                    <p>
                        * United States territory
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!--End of Block 11-->


    <!--Block 12 - CTA-->
    <div class="container bg--white">
        <div class="container__centered" style="text-align: center;">

            <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
            <h2>
                <?php print $cta->field_header_1['und'][0]['value']; ?>
            </h2>
            <?php } ?>

            <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
            <div>
                <?php print $cta->field_long_text_1['und'][0]['value']; ?>
            </div>
            <?php } ?>

            <div class="center" style="margin-top:2em;">
                <?php hubspot_button($cta_code, "Download the Innovators Booklet"); ?>
            </div>

            <div style="margin-top:1em;">
                <?php print $share_link_buttons; ?>
            </div>

        </div>
    </div>
    <!--End of Block 12-->


</div>
<!-- end js__seo-tool__body-content -->


<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>
<!-- end campaign--node-3965.php template -- Global page -->
