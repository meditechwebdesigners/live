<!-- start customer-materials-page-menu--node-3012.php template Aga Khan Announcement page -->
<?php
$content_sections = multi_field_collection_data_unrestricted($node, 'field_fc_head_ltext_1');
$buttons = multi_field_collection_data_unrestricted($node, 'field_fc_button_unl_1');
$side_menu = multi_field_collection_data_unrestricted($node, 'field_fc_text_link_unl_1');
?>

  <!-- HERO section -->
  <div class="container no-pad background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/customer/business-men-hand-shake.jpg);">
    <div class="container__centered bg-overlay--black">
      <div class="container__one-half" style="padding:6em 0em;">
        <div class="transparent-overlay text--white">
          <h1>
            <?php print $title; ?>
          </h1>
          <?php if( !empty($content['field_sub_header_1']) ){ ?>
          <h2>
            <?php print render($content['field_sub_header_1']); ?>
          </h2>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
  <!-- End of HERO section -->

  <section class="container__centered">

    <div class="container__two-thirds">

    <?php

    if( isset($content_sections) && !empty($content_sections) ){
      $number_of_content_sections = count($content_sections);
      for($cs=0; $cs<$number_of_content_sections; $cs++){
        print '<h2>';
        print $content_sections[$cs]->field_header_1['und'][0]['value']; 
        print '</h2>';
        print '<div>';
        print $content_sections[$cs]->field_long_text_1['und'][0]['value']; 
        print '</div>';
      }
    }
 
    /*
    if( isset($buttons) && !empty($buttons) ){
      $number_of_buttons = count($buttons);
      for($b=0; $b<$number_of_buttons; $b++){
        if( !empty($buttons[$b]->field_hubspot_embed_code_1['und'][0]['value']) && $buttons[$b]->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
          $button_code = $buttons[$b]->field_hubspot_embed_code_1['und'][0]['value']; 
          print '<div class="center" style="margin-top:2em;">';
          hubspot_button($button_code, "button");
          print '</div>';
        }
        else{
          print '<a href="';
          print $buttons[$b]->field_button_url_1['und'][0]['value'];
          print '" class="btn--orange">';
          print $buttons[$b]->field_button_text_1['und'][0]['value'];
          print '</a>';
        }
      }
    }
    */    
    ?>
    
      <!-- Start hidden modal box -->
      <div id="modal7" class="modal">
        <a class="close-modal" href="javascript:void(0)">&times;</a>
        <div class="modal-content">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/customer/Aga-Khan-University-Signing-0588.jpg"> <!-- Add modal content here -->
        </div>
      </div>
      <!-- End hidden modal box -->
      <!-- Start modal trigger -->
      <div class="open-modal" data-target="modal7">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/customer/Aga-Khan-University-Signing-0588.jpg"> <!-- Add modal trigger here -->
        <div class="mag-bg"> <!-- Include if using image trigger -->
          <i class="mag-icon fas fa-search-plus"></i>
        </div>
      </div>
      <!-- End modal trigger -->
      
      <div>
       
        <div class="container__one-half">
          <!-- Start hidden modal box -->
          <div id="modal1" class="modal">
            <a class="close-modal" href="javascript:void(0)">&times;</a>
            <div class="modal-content">
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/customer/Aga-Khan-University-Signing-9962.jpg"> <!-- Add modal content here -->
            </div>
          </div>
          <!-- End hidden modal box -->
          <!-- Start modal trigger -->
          <div class="open-modal" data-target="modal1">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/customer/Aga-Khan-University-Signing-9962.jpg"> <!-- Add modal trigger here -->
            <div class="mag-bg"> <!-- Include if using image trigger -->
              <i class="mag-icon fas fa-search-plus"></i>
            </div>
          </div>
          <!-- End modal trigger -->

          <div id="modal2" class="modal">
            <a class="close-modal" href="javascript:void(0)">&times;</a>
            <div class="modal-content">
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/customer/Aga-Khan-University-Signing-0322.jpg"> <!-- Add modal content here -->
            </div>
          </div>
          <!-- End hidden modal box -->
          <!-- Start modal trigger -->
          <div class="open-modal" data-target="modal2">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/customer/Aga-Khan-University-Signing-0322.jpg"> <!-- Add modal trigger here -->
            <div class="mag-bg"> <!-- Include if using image trigger -->
              <i class="mag-icon fas fa-search-plus"></i>
            </div>
          </div>
          <!-- End modal trigger -->

          <div id="modal3" class="modal">
            <a class="close-modal" href="javascript:void(0)">&times;</a>
            <div class="modal-content">
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/customer/Aga-Khan-University-Signing-0100.jpg"> <!-- Add modal content here -->
            </div>
          </div>
          <!-- End hidden modal box -->
          <!-- Start modal trigger -->
          <div class="open-modal" data-target="modal3">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/customer/Aga-Khan-University-Signing-0100.jpg"> <!-- Add modal trigger here -->
            <div class="mag-bg"> <!-- Include if using image trigger -->
              <i class="mag-icon fas fa-search-plus"></i>
            </div>
          </div>
          <!-- End modal trigger -->
        </div>
        
        <div class="container__one-half">
          <div id="modal4" class="modal">
            <a class="close-modal" href="javascript:void(0)">&times;</a>
            <div class="modal-content">
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/customer/Aga-Khan-University-Signing-0204.jpg"> <!-- Add modal content here -->
            </div>
          </div>
          <!-- End hidden modal box -->
          <!-- Start modal trigger -->
          <div class="open-modal" data-target="modal4">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/customer/Aga-Khan-University-Signing-0204.jpg"> <!-- Add modal trigger here -->
            <div class="mag-bg"> <!-- Include if using image trigger -->
              <i class="mag-icon fas fa-search-plus"></i>
            </div>
          </div>
          <!-- End modal trigger -->

          <div id="modal5" class="modal">
            <a class="close-modal" href="javascript:void(0)">&times;</a>
            <div class="modal-content">
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/customer/Aga-Khan-University-Signing-0539.jpg"> <!-- Add modal content here -->
            </div>
          </div>
          <!-- End hidden modal box -->
          <!-- Start modal trigger -->
          <div class="open-modal" data-target="modal5">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/customer/Aga-Khan-University-Signing-0539.jpg"> <!-- Add modal trigger here -->
            <div class="mag-bg"> <!-- Include if using image trigger -->
              <i class="mag-icon fas fa-search-plus"></i>
            </div>
          </div>
          <!-- End modal trigger -->

          <div id="modal6" class="modal">
            <a class="close-modal" href="javascript:void(0)">&times;</a>
            <div class="modal-content">
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/customer/Aga-Khan-University-Signing-0570.jpg"> <!-- Add modal content here -->
            </div>
          </div>
          <!-- End hidden modal box -->
          <!-- Start modal trigger -->
          <div class="open-modal" data-target="modal6">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/customer/Aga-Khan-University-Signing-0570.jpg"> <!-- Add modal trigger here -->
            <div class="mag-bg"> <!-- Include if using image trigger -->
              <i class="mag-icon fas fa-search-plus"></i>
            </div>
          </div>
          <!-- End modal trigger -->
        </div>
        
      </div>

    </div>

    <aside class="container__one-third <?php 
    if( isset($side_menu) && $side_menu[0] != '' ){ 
      print 'panel';
    } 
    ?>">

      <?php
      if( isset($side_menu) && $side_menu[0] != '' ){
        print '<div class="sidebar__nav solutions_sidebar_gae">';
        print '<ul class="menu">';
        $number_of_menu_links = count($side_menu);
        for($ml=0; $ml<$number_of_menu_links; $ml++){
          print '<li><a href="';
          print $side_menu[$ml]->field_link_url_1['und'][0]['value'];
          print '"';
          if($side_menu[$ml]->field_external_link['und'][0]['value'] == 1){
            print ' target="_blank"';
          }
          print '>';
          print $side_menu[$ml]->field_link_text_1['und'][0]['value']; 
          print '</a></li>';
        }
        print '</ul>';
        print '</div>';
      }
      ?>

    </aside>

  </section>
  <!-- end customer-materials-page-menu--node-3012.php template -->
