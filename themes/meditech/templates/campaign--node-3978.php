<!-- START campaign--node-3978.php -->

<?php // This template is set up to control the display of the MEDITECH Care Compass Campaign

$url = $GLOBALS['base_url']; // grabs the site url
	
$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
    .content__callout,
    .content__callout__content {
        background-color: transparent;
        color: #fff;
    }

    hr.dotted {
        width: 50%;
        border-bottom: 5px dotted #8797dc;
        height: 1px;
        margin: 1.6875em auto;
    }

    .stats-container {
        display: flex;
        align-items: center;
        margin-bottom: 3em;
    }

    .stats-img {
        width: 18%;
        margin-right: 2em;
        text-align: center;
    }

    .stats-text {
        width: 80%;
        padding-right: 2em;
    }

    .stats-text p {
        margin: 0;
    }

    @media (max-width: 37.5em) {
        .container__one-fourth {
            width: 100%;
        }
    }

    @media (max-width: 50em) {
        .stats-container {
            margin-bottom: 1em;
        }
    }

</style>

<div class="js__seo-tool__body-content">


    <!-- START Block 1 -->
    <div class="container bg--purple-gradient" style="padding: 1em 0;">
        <div class="content__callout">
            <div class="content__callout__media">
                <div class="content__callout__image-wrapper">
                    <div class="video js__video" data-video-id="674851582">
                        <figure class="video__overlay">
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--meditech-care-compass.jpg" alt="Expanse Care Compass - video covershot">
                        </figure>
                        <a class="video__play-btn" href="https://vimeo.com/674851582"></a>
                        <div class="video__container"></div>
                    </div>
                </div>
            </div>
            <div class="content__callout__content">
                <div class="content__callout__body">
                    <h1 class="js__seo-tool__title">Help guide patients between visits with Expanse Care Compass</h1>
                    <p>Delivering high quality, efficient care means knowing who your patients are and what they need when they’re not in your office or at the hospital. That requires keeping your care managers in the loop, so they can easily work in concert with patients and across care teams to coordinate the right care between visits.</p>
                    <!--					<hr class="dotted">-->
                    <p>Identify and meet the needs of your populations while closing care gaps with Expanse Care Compass — our latest integrated, web-based solution built specifically for care managers. Whether patients are managing chronic conditions or working to maintain wellness, you can help them to reach their goals while also improving outcomes and reducing costs for your organization.</p>
                    <div class="center" style="margin-top: 1em;">
                        <?php hubspot_button($cta_code, "Join Us At MUSE"); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Block 1 -->


    <!-- START Block 2-->
    <div class="container background--cover center" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/about/bg--organic-shapes-green-full-width.svg);">
        <div class="container__centered" style="margin-bottom: 2em;">
            <div class="auto-margins" style="margin-bottom: 2em;">
                <h2>Improve the care manager experience</h2>
                <p>Provide care managers with the <a href="https://home.meditech.com/en/d/mktcontent/otherfiles/carecompass.pdf" target="_blank">integrated set of tools</a> they need to proactively manage different patient populations. With Expanse Care Compass, they can:</p>
            </div>
            <div class="container__one-fourth shadow-box">
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--patient-registries.jpg" alt="Icon of a tablet showing a patient dashboard">
                <p>Coordinate and prioritize care activities as well as patient outreach through one intuitive, central home screen.</p>
            </div>
            <div class="container__one-fourth shadow-box">
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--patient-lists.jpg" alt="Icon showing a list of patient registries">
                <p>Use patient registries to compile patient lists based upon selected criteria.</p>
            </div>
            <div class="container__one-fourth shadow-box">
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--information-exchange.jpg" alt="Icon showing two arrows depicting information exchange">
                <p>Document information exchanged between the patient and among care team members to ensure care decisions are made with most up-to-date information.</p>
            </div>
            <div class="container__one-fourth shadow-box">
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--management-roles.jpg" alt="Icon showing a group of people with a magnifying glass">
                <p>Tailor responsibilities according to care management roles — the flexibility of Expanse Care Compass helps organizations adapt to any care management model.</p>
            </div>
        </div>
    </div>
    <!-- END Block 2 -->


    <!-- START Block 3 -->
    <div class="container no-pad">
        <div class="gl-container">
            <div class="container__one-half background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/african-american-woman-using-glucometer.jpg); min-height:400px;"></div>
            <div class="container__one-half gl-text-pad bg--purple-gradient">
                <h2>Stay one step ahead to keep patients on track</h2>
                <p>Anticipate and plan for the next steps in each patient's care. Patients are more apt to engage in their own care when they’re supported by effective care coordination and collaboration.</p>
                <p>Dynamic, enterprise-wide patient registries enable care managers to:</p>
                <ul class="fa-ul">
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Identify and monitor those cohorts of patients with the most immediate needs.</li>
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Ensure patients are following the appropriate protocols for their conditions/wellness.</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END Block 3 -->


    <!-- START Block 4 -->
    <div class="container no-pad">
        <div class="gl-container">
            <div class="container__one-half gl-text-pad bg--purple-gradient">
                <h2>Deliver a more holistic approach to care</h2>
                <p>Stay connected to your patients, their families, and care team members to develop a deeper understanding of each individual’s situation while:</p>
                <ul class="fa-ul">
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Building trusting and lasting patient relationships.</li>
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Identifying <a href="https://blog.meditech.com/lives-in-the-balance-the-pursuit-of-health-equity">Social Determinants of Health</a> and other factors that may impact a patient’s health.</li>
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Providing valuable services for the most vulnerable patients in your community.</li>
                </ul>
            </div>
            <div class="container__one-half background--cover flex-order--reverse" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/senior-man-using-tablet-to-video-chat-with-doctor.jpg); min-height:400px;"></div>
        </div>
    </div>
    <!-- END Block 4 -->


    <!-- START Block 5-->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/about/bg--organic-shapes-green-right-2.svg);">
        <div class="container__centered">
            <div class="center" style="margin-bottom: 2em;">
                <h2>Ensure timely reimbursement for Chronic Care Management</h2>
            </div>
            <div class="container__one-half" style="margin-top: 2em;">
                <div class="stats-container">
                    <div class="stats-img">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--one-in-three.jpg" alt="Icon showing one out of three people">
                    </div>
                    <div class="stats-text">
                        <p>Worldwide, more than <strong>1 in 3 individuals</strong> are impacted.<sup>1</sup></p>
                    </div>
                </div>
                <div class="stats-container">
                    <div class="stats-img">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--117-million.jpg" alt="Icon showing people in the shape of the United States">
                    </div>
                    <div class="stats-text">
                        <p>In the U.S. alone, an estimated <strong>117 million adults</strong> have one or more chronic health conditions.</p>
                    </div>
                </div>
                <div class="stats-container">
                    <div class="stats-img">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--one-in-four.jpg" alt="Icon showing one out of four people">
                    </div>
                    <div class="stats-text">
                        <p><strong>One in four</strong> adults have two or more chronic health conditions. <sup>2</sup></p>
                    </div>
                </div>
            </div>
            <div class="container__one-half shadow-box">
                <p>With Expanse Care Compass, care managers in the US can control their chronic disease costs and improve outcomes by:</p>
                <ul class="fa-ul">
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Enrolling patients in Chronic Care Management (CCM) services.</li>
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Accurately and efficiently bill patient activity through CCM tracking.</li>
                </ul>
                <p>Save valuable time, money, and resources by intervening before unnecessary high acuity utilization occurs. As you consider entering into value-based care agreements, Expanse Care Compass will help you align your patient population and payer initiatives for maximum benefits.</p>
                <p class="no-margin--bottom"><sup>1</sup> <a href="https://www.weforum.org/agenda/2017/12/healthcare-future-multiple-chronic-disease-ncd/" target="_blank"><span class="text--small">World Economic Forum</span></a></p>
                <p class="no-margin--bottom"><sup>2</sup> <a href="https://www.cms.gov/About-CMS/Agency-Information/OMH/equity-initiatives/chronic-care-management" target="_blank"><span class="text--small">CMS.gov</span></a></p>
            </div>
        </div>
    </div>
    <!-- END Block 5 -->


    <!-- START Block 6 -->
    <div class="container bg--purple-gradient">
        <div class="container__centered center">

            <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
            <h2>
                <?php print $cta->field_header_1['und'][0]['value']; ?>
            </h2>
            <?php } ?>

            <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
            <div>
                <?php print $cta->field_long_text_1['und'][0]['value']; ?>
            </div>
            <?php } ?>
            <div style="margin-top:1.5em;">
                <?php hubspot_button($cta_code, "Join Us At MUSE"); ?>
            </div>
            <div style="margin-top:1em;">
                <?php print $share_link_buttons; ?>
            </div>

        </div>
    </div>
    <!-- END Block 6 -->


</div>
<!-- end js__seo-tool__body-content -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>

<!-- END campaign--node-3978.php MEDITECH Care Compass -->
