<?php
// create share links (social media) ===========================================

  $tw = '<i class="fab fa-twitter-square"></i>';
  $li = '<i class="fab fa-linkedin"></i>';
  $fb = '<i class="fab fa-facebook-square"></i>';
  $em = '<i class="fas fa-envelope-square"></i>';

$share_link_buttons = '<!-- inc-share-buttons.php -->
<div class="share-icons no-target-icon"><p>';

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$remove = array("“", "”", "‘", "’", "&#039;", "039");
$replacements = array("", "", "", "", "'", "'");
$removeApostrophy = str_replace($remove, $replacements, $title);
$decodeTitle = html_entity_decode($removeApostrophy, ENT_COMPAT, 'UTF-8');
$titleStrippedOfPunctuation = preg_replace("[^A-Za-z0-9 \:\-\']", '', $decodeTitle);
$raw_encoded_title = rawurlencode($titleStrippedOfPunctuation);
$encoded_url = urlencode($currentURL);

$type = $node->type;

switch($type){
  case 'news_article':
  case 'news_with_custom_side_menu':
  case 'event':
  case 'event_international':
  case 'ebook':
    $summary = render($content['field_summary']);
    $stripped_summary = strip_tags($summary);
    $shorten_summary = substr($stripped_summary, 0, 50);
    $raw_encoded_summary = rawurlencode($shorten_summary);
    $raw_encoded_summary_full = rawurlencode($stripped_summary);
    break;
  case 'job_listing':
    $summary = render($content['field_job_description']);
    $stripped_summary = strip_tags($summary);
    $shorten_summary = substr($stripped_summary, 0, 50);
    $raw_encoded_summary = rawurlencode($shorten_summary);
    $raw_encoded_summary_full = rawurlencode($stripped_summary);
    break;
  case 'event_himss':
    $summary = $raw_encoded_title;
    $stripped_summary = strip_tags($summary);
    $shorten_summary = substr($stripped_summary, 0, 50);
    $raw_encoded_summary = rawurlencode($shorten_summary);
    $raw_encoded_summary_full = rawurlencode($stripped_summary);
    break;
  case 'campaign':
    $summary = render($content['field_meta_description']);
    $stripped_summary = strip_tags($summary);
    $shorten_summary = substr($stripped_summary, 0, 50);
    $raw_encoded_summary = rawurlencode($shorten_summary);
    $raw_encoded_summary_full = rawurlencode($stripped_summary);
    break;
  default: // campaigns
    $summary = 'Meet Expanse, our next-generation web platform. Expanse is defined as, "an uninterrupted space or area; a great reach or extent of command. The complete and total view." That\'s exactly what we\'re bringing to the table with our web-based EHR platform — an open workspace spanning every care setting, where clinicians can work unfettered by technological constraints. Expanse is designed to bring care to new places — where will it take you?';
    $stripped_summary = strip_tags($summary);
    $shorten_summary = substr($stripped_summary, 0, 50);
    $raw_encoded_summary = rawurlencode($shorten_summary);
    $raw_encoded_summary_full = rawurlencode($stripped_summary);
    break;
}

$share_link_buttons .= '<a class="Twitter_share_gae" href="https://twitter.com/intent/tweet?text='.$raw_encoded_title;
$share_link_buttons .= '&via=MEDITECH&url='.$encoded_url; //&source=sharethiscom&related=sharethis
$share_link_buttons .= '" target="_blank" title="Share this link on Twitter" data-dnt="true">'.$tw.'</a> ';
$share_link_buttons .= '<a class="LinkedIn_share_gae" href="https://www.linkedin.com/sharing/share-offsite/?url='.$encoded_url;
$share_link_buttons .= '" target="_blank" title="Share this link on LinkedIn">'.$li.'</a> ';
$share_link_buttons .= '<a class="Facebook_share_gae" href="https://www.facebook.com/sharer/sharer.php?u='.$encoded_url.'" target="_blank" title="Share this link on Facebook">'.$fb.'</a> ';
$share_link_buttons .= '<a class="Email_share_gae" href="mailto:?&subject='.$raw_encoded_title.'&body='.$encoded_url.'%0A%0A'.$raw_encoded_summary_full.'" title="Share this link via email software">'.$em.'</a>';

$share_link_buttons .= '</p></div>
<!-- inc-share-buttons.php -->';

/*
if( user_is_logged_in() === true ){ 
  print '<script src="https://platform.linkedin.com/in.js" type="text/javascript">lang: en_US</script>';
  print '<script type="IN/Share" data-url="https://www.linkedin.com"></script>';
}
*/
?>
