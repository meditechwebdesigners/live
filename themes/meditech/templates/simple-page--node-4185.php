<!-- start simple-page--node-4185.tpl.php template -- NEWS PAGE -->

<h1 style="display:none;">MEDITECH News</h1>

<!-- Hero -->
<?php print views_embed_view('news_hero', 'block'); // adds 'News Hero' Views block... ?>
<!-- End Hero -->

<section class="container__centered">
  <div class="container__two-thirds">
    <?php print views_embed_view('sticky_us_news', 'block'); // adds 'Sticky - US News' Views block... ?>
    <?php print views_embed_view('news_main_page_filter', 'block_1'); // adds 'US News' Views block... ?>
  </div>
  <!-- END container__two-thirds -->

  <?php include('inc-news-sidebar.php'); ?>

</section>
    
<!-- END simple-page--node-4185.tpl.php template -->    