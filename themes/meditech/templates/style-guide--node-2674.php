<?php if( user_is_logged_in() ){ ?>
<!-- START style-guide--node-2674.php GIT / COMMAND PROMPT -->

<style>
  .cmd {
    background-color: #3e4545;
    color: #eaeaea;
    font-family: Consolas, Arial, sans-serif;
    padding: 0.5em;
    word-wrap: break-word;
    border-radius: 4px;
  }

  .large-marg {
    margin-top: 1em;
  }
</style>

<section class="container__centered">

  <h1 class="page__title">
    <?php print $title; ?>
  </h1>

  <div class="container__two-thirds">

    <p><a href="https://git-scm.com/" target="_blank">Git</a> is an open source version control system that helps teams manage changes to source code over time. The software keeps track of every modification to the code in a special kind of database or "repository" (we use <a href="https://bitbucket.org/" target="_blank">BitBucket</a>). If a mistake is made, developers can turn back the clock and compare earlier versions of the code to help fix the mistake while minimizing disruption to all team members. We use Git in conjunction with the <a href="https://sourceforge.net/projects/console/files/" target="_blank">Console 2 Command Prompt</a>.</p>

    <h2 class="large-marg">Download &amp; Installation</h2>
    <p>Navigate to the <a href="https://git-scm.com/" target="_blank">Git website</a> and download the "Latest Source Release" for your operating system. Proceed through the Git setup tool and be sure to select the option for "<strong>Git from the command line and also from 3rd-party software</strong>" as seen below.</p>
    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/styleguide/Screenshot--git-setup.jpg" alt="Git installation setup option for 3rd-party software">
    <p>Next, navigate to the download site for the <a href="https://sourceforge.net/projects/console/files/" target="_blank">Console 2 Command Prompt</a> and download the latest version. If you'd like to make your version of command prompt more user friendly, follow the instructions on this page: <a href="https://www.hanselman.com/blog/Console2ABetterWindowsCommandPrompt.aspx" target="_blank">Console 2 - A Better Windows Command Prompt</a>.</p>

    <h2 class="large-marg">Initial Git Setup</h2>
    <p>To start, you will need to navigate into the folder where you want your repo folder created. You do not need to create the folder that the repo is named after (ex: LIVE, TEST, DEV), that will be created automatically.</p>

    <p>To change the current working directory, you can use the <strong>cd ..</strong> command (where "cd" stands for "change directory"). This allows you to move one directory upwards (into the current folder's parent folder).</p>
    <p class="cmd">cd ..</p>

    <p>To navigate into subfolders, you would use the <strong>cd [folder name]</strong> command (without the brackets).</p>
    <p class="cmd">cd folder name</p>

    <p><span class="italic"><strong>Note:</strong> You can press tab after entering a portion of the name and it will autofill the rest.</span></p>

    <p>You will then need to create an account at <a href="https://bitbucket.org/" target="_blank">BitBucket</a> or sign into your existing account. Then, navigate to the MEDITECH repo you'd like to clone into (in our example, LIVE). You will find the link to copy when clicking on the "Clone" button on the "Source" page of the BitBucket repo. It will look something like this:</p>
    <div id="modal1" class="modal">
      <a class="close-modal" href="javascript:void(0)">×</a>
      <div class="modal-content">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/styleguide/Screenshot--clone-repository.jpg" alt="Clone repository screenshot in Bitbucket">
      </div>
    </div>

    <div class="open-modal" data-target="modal1">
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/styleguide/Screenshot--clone-repository.jpg" alt="Clone repository screenshot in Bitbucket">
      <div class="mag-bg">
        <i class="mag-icon fas fa-search-plus"></i>
      </div>
    </div>

    <p>Finally, copy and paste the highlighted link into the command prompt and press enter to start the cloning process.</p>
    <p class="cmd">git clone https://username@bitbucket.org/meditechwebdesigners/live.git</p>

    <h2 class="large-marg">Syncing</h2>
    <p>Like a good cup of coffee, it is necessary to start off your day by syncing your work with the repo. This will ensure that you have the most up to date version of the code on your local machine and will prevent merge conflicts. We do this using the "git fetch" and "git pull" commands.</p>

    <p>The <strong>git fetch</strong> command downloads commits, files, and refs from the remote repo into your local repo. Fetching is what you do when you want to see what everybody else has been working on. Git isolates fetched content from existing local content, it has absolutely no effect on your local development work. This makes fetching a safe way to review commits before integrating them with your local repo.</p>
    <p class="cmd">git fetch</p>

    <p>The <strong>git pull</strong> command is used to fetch and download content from the remote repo and immediately update the local repo to match that content. The git pull command is actually a combination of two other commands, git fetch followed by git merge. If you are sure you want to sync your local work with the repo, then skip the git fetch command and just go straight to a git pull.</p>
    <p class="cmd">git pull</p>

    <h2 class="large-marg">Pushing Changes to the Repo</h2>
    <p>Once you've finished editing your files, you will need to push the changes to the repo to update it with your work.</p>

    <p>To begin the process, we will use the <strong>git add --all</strong> command which stages all changes in your local files for the next commit.</p>

    <p><span class="italic"><strong>Note:</strong> the alternate command <strong>git add .</strong> can also be used. The difference is that "git add --all" also stages files in higher directories that still belong to the same git repo.</span></p>

    <p class="cmd">git add --all</p>

    <p>Next, we want to commit our files to the repo and add a description. The <strong>git commit -m "a commit message in the present tense"</strong> command allows you to describe which files were changed and what the changes are. It is considered best practice to write your message in the tense that this will do something to the code. Git will also tag this commit with a unique ID which can be used to revert back to if needed.</p>
    <p class="cmd">git commit -m "a commit message in the present tense"</p>

    <p>Finally, the <strong>git push</strong> command is used to transfer commits from your local repo to the remote repo. Pushing has the potential to overwrite changes, so be certain you are ready to merge all files beforehand.</p>
    <p class="cmd">git push</p>

    <h2 class="large-marg">Other Commands &amp; Tips</h2>

    <p>The <strong>git status</strong> command will give you a list of all the updated, added and deleted files. It also shows which branch you're working on.</p>
    <p class="cmd">git status</p>

    <p>It can save time to combine two commands if you are sure they'll be used properly and in order. For example, <strong>git fetch &amp;&amp; git pull</strong> would execute both commands in order.</p>
    <p class="cmd">git fetch &amp;&amp; git pull</p>

    <p><strong>Arrow Keys</strong>: The command prompt keeps a history of the most recent commands you executed. By pressing the ARROW UP key, you can step through the last commands you called (starting with the most recently used). ARROW DOWN will move forward in history towards the most recent call.</p>

    <p><strong>Clear Command Prompt Screen</strong>: If your command prompt screen gets too messy, you can simply clear it by typing in the <strong>cls</strong> command. In addition, pressing the <strong>Esc</strong> key will clear the input line only.</p>
    
    <p><strong>GitHub Fatal Error (HttpRequestException)</strong>: If you happen encounter this error, reference this solution: <a href="https://codeshare.co.uk/blog/how-to-solve-the-github-error-fatal-httprequestexception-encountered/">Codeshare Article</a>.</p>

    <h2 class="large-marg">Further Reading</h2>
    <p>In the Git world, there are hundreds of commands so we're not going to list them all here. We've included the basics to get you started but if you want to go down the Git rabbit hole, here are some more advanced resources:</p>
    <ul>
      <li><a href="https://www.atlassian.com/git/tutorials" target="_blank">Atlassian Git Tutorials</a> - These are among the most detailed and easy to understand tutorials we've found so far. (Bonus: <a href="https://www.atlassian.com/git/tutorials/atlassian-git-cheatsheet" target="_blank">Cheatsheet</a>).</li>
      <li><a href="https://git-scm.com/docs" target="_blank">Git Documentation</a> - Very detailed but also very technical. (Bonus: <a href="https://services.github.com/on-demand/downloads/github-git-cheat-sheet.pdf" target="_blank">Cheatsheet</a>).</li>
    </ul>

  </div>

  <!-- SIDEBAR -->
  <aside class="container__one-third">

    <div class="sidebar__nav panel">
      <?php
        $styleBlock = module_invoke('menu', 'block_view', 'menu-style-guide-side-nav');
        print render($styleBlock['content']); 
        ?>
    </div>

  </aside>
  <!-- END SIDEBAR -->

</section>

<!-- END style-guide--node-2674.php GIT / COMMAND LINE -->
<?php } ?>