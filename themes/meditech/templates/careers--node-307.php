<?php $url = $GLOBALS['base_url']; // grabs the site url ?>
<!-- START careers--node-307.php template JOB LISTINGS page -->

<!-- CAREERS FILTER TEST -->
<style>
  #jobListings tbody:last-child tr:last-child>td:first-child,
  #jobListings tbody:last-child tr:last-child>td:last-child {
    padding: .5em;
  }

  fieldset {
    padding: 1em 1.5em;
  }

  legend {
    border: 0;
    padding: 0 .5em;
    font-weight: bold;
  }

  @media all and (max-width: 50em) {
    .container__one-fourth {
      width: 100%;
      margin-right: 0;
    }

    .container__three-fourths {
      width: 100%;
      margin-right: 0;
    }
  }

  div.expander {
    margin-bottom: 1em;
  }

  div.expander a {
    border-bottom: 1px solid #087E68;
  }

  /*
    div.expander span:before {
    content: "+ ";
    font-weight: bold;
    font-size: 1.25em;
    position: relative;
    top: .05em;
  }
  div.expander span.expanded:before { content: "\2013 "; top: .02em; letter-spacing: .25em; }
*/
  .collapsible {
    display: none;
    width: 100%;
    transition: display 0.5s ease-out;
    padding: 1em;
    margin-bottom: 1em;
    border-radius: 0.389em;
    border: solid 1px #bbb;
    box-shadow: 0px 0px 20px 1px rgb(120 120 120 / 20%);
  }

</style>

<!-- tablesorter js script -->
<script type="text/javascript" src="<?php print $url; ?>/sites/all/themes/meditech/js/jquery.tablesorter.min.js"></script>
<script>
  var $jq = jQuery.noConflict();
  // tablesorter - - - https://github.com/christianbach/tablesorter
  $jq(".tablesorter").tablesorter();

  $jq(document).ready(function() {
    // [1,0] sets first column to sort in ascending order by default
    // [0,0] leaves 2nd column alone
    $jq("#jobListings").tablesorter({
      sortList: [
        [1, 0],
        [0, 0]
      ]
    });
  });

</script>

<div class="container container__centered no-pad--bottom">

  <div class="container__three-fourths">

    <h1 id="jobs">MEDITECH Job Opportunities</h1>

    <?php print render($content['field_body']); ?>

  </div>

  <!-- CAREERS SIDEBAR -->
  <aside class="container__one-fourth panel">
    <div class="sidebar__nav careers_sidebar_gae">
      <?php
          $massnBlock = module_invoke('menu', 'block_view', 'menu-careers-section-side-nav');
          print render($massnBlock['content']); 
        ?>
    </div>
  </aside>
  <!-- END CAREERS SIDEBAR -->

</div>

<div class="container container__centered no-pad--top">

  <div class="container__one-fourth">
    <fieldset id="locations">
      <legend>Filter by Locations:</legend>
      <?php print views_embed_view('job_locations', 'block'); // adds 'job locations' Views block... ?>
    </fieldset>
    <fieldset id="categories">
      <legend>Filter by Categories:</legend>
      <?php print views_embed_view('job_categories', 'block'); // adds 'job categories' Views block... ?>
    </fieldset>
    <div class="bg--light-brown" style="width:100%; padding:1em; margin-top:1em;">
      <h3>Not Finding What You're Looking For?</h3>
      <p>Share your information and we will contact you if new opportunities fitting your qualifications become available.</p>
      <a class="btn--orange" href="https://recruiting.paylocity.com/Recruiting/PublicLeads/New/7791d608-f43a-4be3-8c56-90cab1a0447c">Share Your Information</a>
    </div>

    <div class="sidebar__images" style="margin-top:1em;">
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/careers/proudlyhires.png" alt="red, white and blue ribbon graphic">
    </div>
  </div>

  <div class="container__three-fourths">

    <?php
        // ADD your feed titles and URLs within this array...
        $feed_url = "https://recruiting.paylocity.com/recruiting/v2/api/feed/jobs/6eb73bf0-3811-4dfa-b26f-766c39530183";

        function get_feed($feed_url){

          // get RSS feed data...
          $json = file_get_contents($feed_url);
          $feed = json_decode($json, true);

          if( !empty($feed) ){
            echo "<!-- Valid RSS feed URL. (1) -->";
          }else{
            echo "<!-- Invalid RSS feed URL. (1) -->";
          }

          if( !empty($feed) ){

            if($feed['jobs']){ 

              print '<table class="table tablesorter" id="jobListings">';
                print '<thead>';
                  print '<tr>';
                    print '<th>Position</th>';
                    print '<th>Department</th>';
                    print '<th>Location</th>';
                  print '</tr>';
                print '</thead>';
                print '<tbody>';

                $number_of_posts = count($feed['jobs']);

                for($i = 0; $i < $number_of_posts; $i++){

                  $job = $feed['jobs'][$i];

                  $position = $job['title'];
                  $department = $job['hiringDepartment'];
                  $link = $job['displayUrl'];
                  $postDate = $job['publishedDate'];
                  $location = $job['jobLocation']['state']; 
                  $location_choices = array(
                    array('MA', 'Massachusetts, US'),
                    array('GA', 'Georgia, US'),
                    array('MN', 'Minnesota, US'),
                    array('FL', 'Florida, US'),
                    array('TX', 'Texas, US'),
                    array('NC', 'North Carolina, US'),
                    array('SC', 'South Carolina, US'),
                  );
                  foreach($location_choices as $l){
                    if($location == $l[0]){
                      $location_display = $l[1];
                    }
                  }
                  // format the publication date (Ex: 01-01-22)
                  $postDate = date('m-d-y',strtotime($postDate));

                  print '<tr><td><a href="'.$link.'#LayoutLogoSection">'.$position.'</a><!-- '.$postDate.' --></td><td>'.$department.'</td><td>'.$location_display.'</td></tr>';

                }

                print '</tbody>';
              print '</table>';

            }
            else{

              print '<p>There are currently no job opportunities available.</p>';

            }

            }
            else{

              print '<p>There are currently no job opportunities available.</p>';

            }

        }
        ?>

    <div class="container__centered">

      <?php get_feed($feed_url); ?>

    </div>

  </div>

</div>

</div>

<script>
  // table filter search function...
  // When document is ready: this gets fired before body onload...
  $jq(document).ready(function() {

    // filter by checkbox selections...
    $jq("fieldset#locations input, fieldset#categories input").click(function() {
      // get checked values using functions below...
      var selectedLocations = getLocationValues();
      var selectedCategories = getCategoryValues();

      // start fresh every time by displaying all rows first...
      $jq("table#jobListings tbody tr").show();

      if (selectedLocations && selectedLocations.length > 0 || selectedCategories && selectedCategories.length > 0) {
        // hide all rows...
        $jq("table#jobListings tbody tr").hide();

        $jq("table#jobListings tbody tr").each(function() {
          var cell3Content = $jq.trim($jq(this).find('td').eq(2).text());
          var cell2Content = $jq.trim($jq(this).find('td').eq(1).text());

          // location(s) is selected but not categories...
          if (selectedLocations.indexOf(cell3Content) > -1 && selectedCategories.length === 0) {
            $jq(this).show();
          }
          // category(s) is selected but not locations...
          if (selectedCategories.indexOf(cell2Content) > -1 && selectedLocations.length === 0) {
            $jq(this).show();
          }
          // if both are selected...
          if (selectedLocations.indexOf(cell3Content) > -1 && selectedCategories.indexOf(cell2Content) > -1) {
            $jq(this).show();
          }
        });

      }
    });

    function getLocationValues() {
      // create an array to hold the values...
      var locations = [];
      // look for all location check boxes that are checked and add value to array...
      $jq("fieldset#locations input:checkbox:checked").each(function() {
        locations.push($jq(this).val());
      });
      return locations;
    }

    function getCategoryValues() {
      // create an array to hold the values...
      var categories = [];
      // look for all category check boxes that are checked and add value to array...
      $jq("fieldset#categories input:checkbox:checked").each(function() {
        categories.push($jq(this).val());
      });
      return categories;
    }

  });

</script>

<!-- END careers--node-307.php template -->
