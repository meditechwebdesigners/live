<!-- START campaign--node-3626.php -->
<?php // This template is set up to control the display of the 2021 Virtual Care content type

$url = $GLOBALS['base_url']; // grabs the site url

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<div class="js__seo-tool__body-content">

  <style>

    .shadow-box {
      padding: 2em;
      background-color: #fff;
      border-radius: 7px;
      box-shadow: 0px 0px 20px 1px rgba(120, 120, 120, .2);
      margin-bottom: 1em;
    }

    .fa-ul {
      list-style-type: none;
      margin-left: 2.5em;
      padding-left: 0;
    }

    .fa-angle-double-right:before {
      content: "\f101";
      color: rgb(39, 183, 246);
    }

    .quote-element {
      padding: 0 2em;
    }

    .quote-person {
      margin-left: 50%;
      padding-top: 1em;
    }


    .headshot_video {
      width: 100%;
      border: 4px solid #e6e9ee;
      border-radius: 50%;
      margin: 0 auto;
      max-height: 262px;
      width: 100%;
      border-radius: 50%;
    }

    .vid-bg {
      width: 55px;
      height: 55px;
    }

    .mag-icon {
      top: 52%;
      left: 51%;
    }

    @media (max-width: 40em) {
      .vid-bg {
        width: 40px;
        height: 40px;
      }
    }

    .open-modal:hover .vid-bg {
      height: 45px;
      width: 45px;
    }

    .open-modal:hover .mag-icon {
      font-size: 18px;
    }


    @media all and (max-width: 50em) {
      .angled-bg--white {
        background-image: linear-gradient(70deg, rgba(255, 255, 255, .85) 49.9%, rgba(255, 255, 255, .85) 50%);
      }
    }

      .angled-bg--meditech-green {
        background-image: linear-gradient(70deg, rgba(8, 126, 104, .85) 49.9%, rgba(8, 126, 104, .85) 50%);
      }

      @media (max-width: 1150px){
    .gl-container .container__one-half {
    width: 100%;
    }
  }

  </style>


  <!-- Block 1 -->

  <div class="container no-pad">
    <div class="gl-container">

      <div class="container__one-half gl-text-pad text--white" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-gradient--bg.jpg);">
        <h1 class="js__seo-tool__title">Stay connected no matter what, through virtual care</h1>
        <p>The COVID-19 pandemic introduced patients to the benefits and convenience of virtual care. Now they expect it to be a standard of care moving forward, with <a href="https://www.hcinnovationgroup.com/population-health-management/mobile-health-mhealth/news/21160977/unitedhealthcare-survey-most-americans-bullish-on-virtual-care-for-medical-services">more than half of Americans</a> saying they were likely to use virtual care for medical services. MEDITECH’s Virtual Care solution enables patients to stay connected to their care providers (and your organization) via convenient video visits, no matter the circumstances. And with Virtual On Demand Care, healthcare organizations can extend their virtual care offering by providing convenient urgent care appointments to both new and existing patients.</p>

        <div class="center" style="margin-top:2em;">
          <?php hubspot_button($cta_code, "SWatch The Virtual Care Roundtable Webinar Recording"); ?>
        </div>
      </div>


      <div class="container__one-half background--cover flex-order--reverse hide-on-tablets" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Father-caring-for-his-daughter-consulting-with-physician.jpg); min-height: 17em;">

      </div>

    </div>
  </div>

  <!-- End of Block 1 -->

  <!-- Start of new block 2 -->

  <div class="container">
    <div class="container__centered">

      <h2>Why integration matters</h2>
      <div style="margin-bottom:2em;">Expanse Virtual Care fills the need for telehealth visits by offering a suite of tools that can be used across the continuum. Patients can schedule their visits, verify demographics, pay any necessary fees, respond to pre-visit questionnaires, and launch the visits, all from their patient portal or MHealth app.
      </div>

      <div class="card__wrapper">
  <div class="container__one-fourth card">
    <figure>
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/virtual-visit-interacting-with-patient-chart.jpg">
    </figure>
    <div class="card__info">
      <h4>Embedded in your workflow</h4>
      <p>Virtual Care is integrated into existing ambulatory workflows, allowing providers to conduct scheduled virtual visits while interacting with the patient’s chart.</p>
    </div>    
  </div>

  <div class="container__one-fourth card">
    <figure>
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/virtual-visit-patient-with-doctor.jpg">
    </figure>
    <div class="card__info">
      <h4>Connect outside the practice setting</h4>
      <p>Hospital-based specialists can also conduct virtual visits outside of MEDITECH’s Ambulatory solution, and join from their schedule when it’s time for the visit.</p>
    </div>    
  </div>

  <div class="container__one-fourth card">
    <figure>
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/virtual-visit-new-patient.jpg">
    </figure>
    <div class="card__info">
      <h4>Extend virtual care across the community</h4>
      <p>Virtual On Demand Care allows immediate access to your organization’s provider network, even if the consumer is not currently a patient.</p>
    </div>    
  </div>

  <div class="container__one-fourth card">
    <figure>
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/virtual-visit-physician-interacting-with-other-physicians.jpg">
    </figure>
    <div class="card__info">
      <h4>Improve the physician-to-physician connection</h4>
      <p>Physicians have the ability to interact with their peers to virtually consult on a patient.</p>
    </div>    
  </div>
</div>

    </div>
  </div>

  <!-- End of new block 2 -->
  
  <!-- Block 3 -->

  <div class="container no-pad">
    <div class="gl-container">

      <div class="container__one-half background--cover flex-order--reverse hide-on-tablets" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Nurse-giving-virtual-guidance.jpg); min-height: 17em;">

      </div>

      <div class="container__one-half background--cover text--white" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-gradient--bg.jpg); padding: 4em;">

        <h2>The welcome mat to your digital front door</h2>
        
    <div style="padding-bottom: 2em">

        <p>Give healthcare consumers an easy path to becoming new patients. With new patient onboarding, they can quickly connect with your organization as well as accurately manage their own information. Patient-entered information will populate across all areas of the EHR and reduce your administrative burdens. </p>

         <ul class="fa-ul">
          <li><span class="fa-li"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span>Increase completion rates, reduce transcription errors, and improve PHI security by eliminating paper registration forms</a></li>

          <li><span class="fa-li"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/selfservicepatientintake.pdf" target="_blank">Improve the patient experience by enabling new patients to enroll themselves at their convenience </a></li>

          <li><span class="fa-li"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span>Reduce the time it takes to register new patients to keep your practice on schedule</li>

          <li><span class="fa-li"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span>Customize questionnaires and other intake forms to best fit the needs of your practice</li>

          <li><span class="fa-li"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span>Capture patient consent electronically through online signature forms</li>

          <li><span class="fa-li"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span>Automatically enroll new patients in the Patient and Consumer Health Portal</li>
        </ul>
      </div>
        
      </div>
    </div>
  </div>
  <!-- End of Block 3 -->

  <!-- Block 4 -->

  <div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/blue-layered-background.jpg);">
    <div class="container__centered center">
      <div class="auto-margins" style="margin-bottom:2em;">
        <h2>Anytime, anywhere access to care with Virtual On Demand Care</h2>
        <p>With the ongoing demands of everyday life — balancing work, caring for family, and taking care of themselves — people with urgent medical needs often don’t have the time to wait for an appointment to see their PCP. With <a href="https://home.meditech.com/en/d/mktcontent/otherfiles/virtualondemandcareinfographicweb.pdf">Virtual On Demand Care</a>, they don’t have to. And you can build on the momentum of patients embracing telemedicine, <a href="https://ehr.meditech.com/ehr-solutions/patient-engagement">improve the consumer experience</a>, and give your patients new care options that will grow your business and build loyalty.</p>
      </div>
      <div class="container__one-third shadow-box">
        <div class="center">
          <img style="max-width: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Travel-ban-icon.svg" alt="care team icon">
        </div>
        <h3><strong>No Travel</strong></h3>
        <p>
          Patients receive immediate care without having to spend time (and money) visiting an ED or retail urgent care facility.
        </p>
      </div>
      <div class="container__one-third shadow-box">
        <div class="center">
          <img style="max-width: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Patient-story-icon.svg" alt="patient check in icon">
        </div>
        <h3><strong>Full Patient Story</strong></h3>
        <p>
          Functionality is embedded throughout Expanse and across multiple care settings. Care teams have full access to the patient’s entire care journey, enabling better diagnosis and treatment.
        </p>
      </div>
      <div class="container__one-third shadow-box">
        <div class="center">
          <img style="max-width: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Consumer-access-icon.svg" alt="holding mobile">
        </div>
        <h3><strong>Easy Consumer Access</strong></h3>
        <p>
          By offering a convenient alternative to ED or urgent care, organizations can attract consumers seeking care and welcome them as patients.
        </p>

      </div>
    </div>
  </div>

  <!-- End Of Block 4 -->


  <!-- Block 5 -->

  <div class="container bg--white">
    <div class="container__centered text--black-coconut">
      <div class="auto-margins text--black-coconut" style="margin-bottom:2em;">
        <h2>Three ways Virtual Care keeps you connected</h2>
        <p>Virtual Care extends and reinforces the relationship between patients, their providers, and their healthcare organization. Here are the benefits you’ll see:</p>
      </div>
      <div class="gl-container">
       
        <div class="container__one-third bg--blue-gradient">
          <div class="center">
            <img style="max-width: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/care-team--virtual-care.svg" alt="Physician icon">
          </div>
          <p class="text--large">For the care team…</p>
          <ul class="fa-ul">
            <li>
              <span class="fa-li"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span>
              Maintain contact with patients <a href="https://www.ajmc.com/view/patient-and-clinician-experiences-with-telehealth-for-patient-followup-care" targget="_blank">between in-person visits,</a> driving continuity of care</li>
            <li>
              <span class="fa-li"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span>
              Deliver care using their current Expanse workflow —no new tools to learn or systems to maintain
            </li>
          </ul>
        </div>
        
        <div class="container__one-third bg--light-gray">
          <div class="center">
            <img style="max-width: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Patients--virtual-care.svg" alt="Patient icon">
          </div>
          <p class="text--large">For the patient...</p>
          <ul class="fa-ul">
            <li>
              <span class="fa-li"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span>Patients can meet with their desired clinician at the most convenient time and location
            </li>
            <li>
              <span class="fa-li"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span>New patients can register themselves with a healthcare organization for access to virtual care
            </li>
            <li>
              <span class="fa-li"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span>Use the same virtual and mobile solutions they use every day
            </li>
            <li>
              <span class="fa-li"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span>Avoid risk of delaying or forgoing necessary care for chronic conditions
            </li>
          </ul>
        </div>
        
        <div class="container__one-third bg--blue-gradient">
          <div class="center">
            <img style="max-width: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Healthcare-Organization--virtual-care.svg" alt="Hospital Icon">
          </div>
          <p class="text--large">For the healthcare organization...</p>
          <ul class="fa-ul">
            <li>
              <span class="fa-li"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span>
              Maintain a healthy revenue stream by offering patients new ways to receive care virtually
            </li>
            <li>
              <span class="fa-li"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span>Expand your reach to new consumers seeking a medical home with streamlined enrollment and self-scheduling
            </li>
            <li>
              <span class="fa-li"><i class="fas fa-angle-double-right" aria-hidden="true"></i></span><a href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7279631/" target="_blank">Reduce appointment no-show rates</a>
            </li>
          </ul>
        </div>
        
      </div>
    </div>
  </div>
  <!-- End Block 5 -->

  <!-- Start of Quotes Block 6 -->
<div class="container no-pad">
    <div class="gl-container">
    
    <div class="container__one-half bg--blue-gradient" style="padding:3em;">
      <article class="container__centered center auto-margins">
      <figure>

        <!-- Start hidden modal box -->
            <div id="modal1" class="modal">
              <a class="close-modal" href="javascript:void(0)">&times;</a>
              <div class="modal-content" style="z-index:10001;">
                <iframe width="1080" height="608" src="https://www.youtube.com/embed/-TfjEzXAly8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
            <!-- End hidden modal box -->
            <!-- Start modal trigger -->
            <div class="open-modal" data-target="modal1">
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Doug-Kanis--headshot.png" alt="Doug-Kanis-DO" style="width:150px;">
              <div class="vid-bg">
                <!-- Include if using image trigger -->
                <i class="mag-icon fas fa-play"></i>
              </div>
            </div>
            <!-- End modal trigger -->

      </figure>
      <div class="text--large italic" style="font-size:1.4em; padding-top: .75em;">
        <p>“We recently fast-tracked the Virtual Visit functionality. In literally a matter of days, I was doing Virtual Visits on a cardiac transplant patient and a kidney transplant patient recovering from prostate cancer surgery. We also had <a href="https://blog.meditech.com/how-virtual-visits-are-keeping-providers-and-patients-safe-at-citizens-memorial-healthcare">great support</a> and guidance from Citizens Memorial in Bolivar, Missouri.”</p>
      </div>
      <p class="no-margin--bottom text--large bold">Doug Kanis, DO</p>
      <p>Pella Regional Health Center</p>
    </article>
    </div>

    <div class="container__one-half bg--light-gray" style="padding:3em;">
      <article class="container__centered center auto-margins">
      <figure>
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/William-Dailey--headshot.png" alt="William-Dailey-MD" style="width:150px;">
      </figure>
      <div class="text--large italic" style="font-size:1.4em; padding-top: .75em;">
        <p>“Now we have integrated ambulatory <a href="https://ehr.meditech.com/news/meditech-virtual-visits-connect-providers-patients-at-med-center-health">video visits</a> up in Expanse. The EMR does instantaneous registration, spawns the visit documentation, and establishes the bidirectional video link. The remarkable thing is, it only took a week to set up.”</p>
      </div>
      <p class="no-margin--bottom text--large bold">William Dailey, MD</p>
      <p>MS, MSMI, CMIO, Golden Valley Memorial Healthcare</p>
    </article>
    </div>

  </div>
</div>
<!-- End of Quotes Block 6 -->

<!-- Block 7 -->

  <div class="container bg--white">
    <div class="container__centered">

      <div class="container__one-fourth">

        <!-- Hidden Modal -->

        <div id="modal24" class="modal">
          <a class="close-modal" href="javascript:void(0)">×</a>
          <div class="modal-content">
            <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Scheduled-Appt--iphone.png" alt="Confirmation Screencap">
          </div>
        </div>

        <!-- End of Hidden Modal -->

        <!-- Start modal trigger -->

        <div class="open-modal" data-target="modal24">
          <div class="phone--black">
            <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Scheduled-Appt--iphone.png" alt="Confirmation Screencap">
          </div>
          <!-- Add modal trigger here -->
          <div class="mag-bg">
            <!-- Include if using image trigger -->
            <i class="mag-icon fas fa-search-plus"></i>
          </div>
        </div>

        <!-- End modal trigger -->
      </div>

      <div class="container__three-fourths text--black-coconut">
        <div class="container__centered">
          <div class="container no-pad auto-margins" style="padding-bottom: 1em;">
            <h2 class="center">Your appointment is confirmed</h2>
            <p>Give time back to your providers and put patients in the driver’s seat, with MEDITECH’s Patient and Consumer Health Portal &mdash; also available in French and Spanish. Scheduling and pre-registering has never been easier.
            </p>
          </div>
          <div class="container__centered">
            <div class="container no-pad">
              <div class="container__centered">
                <div class="container__one-half">
                  <h3>Patients</h3>
                  <ul class="fa-ul">
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--white" aria-hidden="true"></i></span>Fill out questionnaires; update medication, allergy and insurance information; and complete patient, family, social history prior to the visit — whether it’s virtual or in-person.</li>
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--white" aria-hidden="true"></i></span>Know the expected copay before seeing your provider.</li>
                  </ul>
                </div>
                <div class="container__one-half">
                  <h3>Healthcare Organization</h3>
                  <ul class="fa-ul">
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--white" aria-hidden="true"></i></span>Care teams can spend more time with the patient instead of chasing down background information.</li>
                    <li><span class="fa-li"><i class="fas fa-angle-double-right text--white" aria-hidden="true"></i></span>Office staff can minimize the cumbersome task of scanning traditional paper forms, and eliminate transcription errors.</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

  <!-- End of Block 7 -->
  
  <!-- Block 8 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-gradient--bg.jpg);">
    <div class="container__centered">

      <div class="container__two-thirds text--white">
        <div style="margin-bottom: 1em;">
          <h2>Connect the dots through remote patient monitoring</h2>
          <p>Patient visits — both in-office and virtual — are just part of the patient care journey. Fortunately, remote patient monitoring can <a href="https://ehr.meditech.com/ehr-solutions/bridging-the-gaps-in-care">fill the gaps</a> in between. <a href="https://home.meditech.com/en/d/mktcontent/otherfiles/telehealthapproach.pdf">By encouraging their patients to upload data</a> from personal health devices and medical device kits into MEDITECH’s Patient and Consumer Health Portal, care teams can <a href="https://ehr.meditech.com/ehr-solutions/meditech-population-health">better manage their patient populations</a>, especially those with chronic or pre-existing conditions.
            Learn how a remote patient monitoring (RPM) program in conjunction with MEDITECH’s Patient Portal and Raziel Health kept Val Verde Regional Medical Center's elderly and chronically ill patients <a href="https://f.hubspotusercontent10.net/hubfs/2897117/Case_Studies/CustomerSuccess_ValVerde_Regional_RPM.pdf">on track while staying safe</a> at home during COVID-19.</p>
        </div>

      </div>

      <div class="container__one-third">

        <!-- Hidden Modal -->

        <div id="modal23" class="modal">
          <a class="close-modal" href="javascript:void(0)">×</a>
          <div class="modal-content">
            <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/HealthTracker.jpg" alt="AMB Regulatory Graphical Clinical Quality Analysis">
          </div>
        </div>

        <!-- End of Hidden Modal -->

        <!-- Start modal trigger -->

        <div class="open-modal" data-target="modal23">
          <div class="phone--black">
            <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/HealthTracker--small.jpg" alt="AMB Regulatory Graphical Clinical Quality Analysis">
          </div>
          <!-- Add modal trigger here -->
          <div class="mag-bg">
            <!-- Include if using image trigger -->
            <i class="mag-icon fas fa-search-plus"></i>
          </div>
        </div>

        <!-- End modal trigger -->
      </div>

    </div>
  </div>

  <!-- End of Block 8 -->

  <!-- Block 9 - CTA Block -->
  <div class="container bg--white text--black-coconut">
    <div class="container__centered" style="text-align: center;">

      <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
      <h2>
        <?php print $cta->field_header_1['und'][0]['value']; ?>
      </h2>
      <?php } ?>

      <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
      <div>
        <?php print $cta->field_long_text_1['und'][0]['value']; ?>
      </div>
      <?php } ?>

      <div class="center" style="margin-top:2em;">
        <?php hubspot_button($cta_code, "Watch The Virtual Care Roundtable Webinar Recording"); ?>
      </div>

      <div style="margin-top:1em;">
        <?php print $share_link_buttons; ?>
      </div>

    </div>
  </div>
  <!-- End Block 9 -->

</div>
<!-- end js__seo-tool__body-content -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>

<style>
  @media (min-width: 941px) {
    .hide--desktop {
      display: none;
    }
  }

</style>

<!-- END campaign--node-3626.php -->
