<!-- START simple-page--greenfield-docs.php template -->
<style>
  #greenfield-docs {
    font-size: .9em;
  }

  code {
    display: inline-block;
  }

  div.expander {
    margin-bottom: 1em;
  }

  div.expander a {
    border-bottom: 1px solid #087E68;
  }

  div.expander span:before {
    content: "+ ";
    font-weight: bold;
    font-size: 1.25em;
    position: relative;
    top: .05em;
  }

  div.expander span.expanded:before {
    content: "\2013 ";
    top: .02em;
    letter-spacing: .25em;
  }

  .collapsible {
    display: none;
    width: 100%;
    transition: display 0.5s ease-out;
    padding: 1em;
    margin-bottom: 1em;
    border-radius: 0.389em;
    border: solid 1px #bbb;
    box-shadow: 0px 0px 20px 1px rgb(120 120 120 / 20%);
  }

  .panel.sticky {
    position: -webkit-sticky;
    position: sticky;
    top: 2em;
  }

  .btn--orange {
    border: 4px solid #ff8300 !important;
  }

  .btn--outline {
    background-color: unset;
    color: #3E4545 !important;
    border-width: 4px !important;
  }

  .btn--orange:hover {
    -webkit-transition: all 600ms ease-in-out;
  }

</style>


<section class="container__centered">
  <div class="container__two-thirds">
    <h1 class="page__title js__seo-tool__title"><?php print $title; ?></h1>
    <div class="js__seo-tool__body-content">
      <?php print render($content['field_body']); ?>
    </div>
  </div>
  <div class="container__one-third">
    <div class="panel sticky">

      <div class="sidebar__nav" style="float:left; width:100%;">
        <ul class="menu">
          <li class="first"><a href="https://ehr.meditech.com/ehr-solutions/greenfield-workspace">Greenfield Workspace</a></li>
          <li class=""><a href="https://ehr.meditech.com/ehr-solutions/greenfield-alliance">Greenfield Alliance</a></li>
          <li class=""><a href="https://ehr.meditech.com/ehr-solutions/greenfield-resources">Greenfield resources</a></li>
          <li class=""><a href="https://ehr.meditech.com/ehr-solutions/how-to-work-in-the-greenfield-workspace">How to work in Greenfield Workspace</a></li>
          <li class=""><a href="https://ehr.meditech.com/hl7-outbound-list-for-greenfield">HL7 Outbound List</a></li>
          <li class="last"><a href="https://jira.meditech.com/servicedesk/customer/portal/1">Contact our Service Desk for help</a></li>
        </ul>
      </div>

      <div class="center" style="margin-top:2em;">
        <a href="https://info.meditech.com/welcome-to-meditech-greenfield-rfi" class="btn--orange btn--outline" style="margin:1em 0; width:100%;">Register for Greenfield Workspace</a>
        <a href="https://greenfield.meditech.com/" class="btn--orange" style="margin:0; width:100%;">Log in to Greenfield Workspace</a>
      </div>

    </div>
  </div>
</section>
<!-- end simple-page--greenfield-docs.php template -->
