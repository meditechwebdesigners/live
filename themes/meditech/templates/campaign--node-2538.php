<!-- START campaign--node-2538.php -->
<?php // This template is set up to control the display of the MACRA content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>
<style>
    .pie-chart--key-color-square {
        float: left;
        width: 20px;
        height: 20px;
        margin: 5px;
    }

    .pie-chart-container {
        padding-left: 9em;
    }

    .pie-chart-numbers {
        margin-top: 5em;
    }

    .pie-chart-numbers li {
        list-style: none;
    }

    .transparent-overlay--white {
        padding: 1em;
        background-color: rgba(255, 255, 255, 0.8);
        min-height: 5em;
    }

    .fa-check-square,
    .fa-genderless {
        color: #00bc6f;
    }

    @media all and (max-width: 50em) {
        .pie-chart-container {
            padding-left: 0em;
        }

        .pie-chart-numbers {
            margin-top: 1em;
        }

    }

</style>

<div class="js__seo-tool__body-content">


    <!-- Block 1 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/businessman-moving-up-escalator.jpg); padding: 5em 0;">
        <div class="container__centered">
            <div class="container__one-half">
                &nbsp;
            </div>
            <div class="container__one-half transparent-overlay--xp text--white">
                <h1 class="js__seo-tool__title">Guiding You on Your MACRA Journey.</h1>
                <p>You've charted your course for MACRA. But with each update to the ruling, the map is being redrawn. Do you stay the course or rethink your route? Do you stay on the MIPS path, or are you ready to give Advanced APMs a try? The answer is not always simple. We're here to help.</p>
            </div>
        </div>
    </div>

    <!-- Block 2 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/aerial-view-of-Estaiada-bridge.jpg);">
        <div class="container__centered text--white center">
            <h2>Choosing Your Route.</h2>
            <p>You begin your journey faced with two options, each with its own set of rules. Choose your adventure:</p>
            <div class="container" style="padding: 2em 0;">
                <div class="container__one-half transparent-overlay">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/ribbon-line-icon.png" alt="Ribbon Icon">
                    <h3>Merit-based Incentive Payment System (MIPS) Path</h3>
                    <strong>Proceed if:</strong>
                    <ul class="fa-ul left">
                        <li>
                            <span class="fa-li"><i class="fas fa-check-square"></i></span>You are not a qualified participant in an APM
                        </li>
                        <li>
                            <span class="fa-li"><i class="fas fa-check-square"></i></span>You bill for Medicare B
                        </li>
                        <li>
                            <span class="fa-li"><i class="fas fa-check-square"></i></span>Your billed charges > $90,000
                        </li>
                        <li>
                            <span class="fa-li"><i class="fas fa-check-square"></i></span>Your beneficiaries > 200
                        </li>
                        <li>
                            <span class="fa-li"><i class="fas fa-check-square"></i></span>You hold an eligible role
                        </li>
                        <p>If you haven’t determined your eligibility yet, use the <a href="https://qpp.cms.gov/participation-lookup" target="_blank">QPP Lookup Tool</a>.</p>
                    </ul>
                </div>
                <div class="container__one-half transparent-overlay">
                    <div>
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/hospital-line-icon.png" alt="Hospital Icon">
                    </div>
                    <h3>Advanced Payment Models (APM) Options</h3>
                    <ul class="fa-ul left">
                        <li>
                            <span class="fa-li"><i class="fas fa-genderless"></i></span>Bundled Payments for Care Improvement Advanced Model
                        </li>
                        <li>
                            <span class="fa-li"><i class="fas fa-genderless"></i></span>Medicare Shared Savings Program Track 2 &amp; 3
                        </li>
                        <li>
                            <span class="fa-li"><i class="fas fa-genderless"></i></span>Next Generation ACO Model
                        </li>
                        <li>
                            <span class="fa-li"><i class="fas fa-genderless"></i></span>Oncology Care Model Two-Sided Risk Arrangement
                        </li>
                        <li>
                            <span class="fa-li"><i class="fas fa-genderless"></i></span>Comprehensive ESRD Care Model
                        </li>
                        <li>
                            <span class="fa-li"><i class="fas fa-genderless"></i></span>Comprehensive Primary Care Plus (CPC+)
                        </li>
                        <li>
                            <span class="fa-li"><i class="fas fa-genderless"></i></span>Comprehensive Care for Joint Replacement
                        </li>
                        <li>
                            <span class="fa-li"><i class="fas fa-genderless"></i></span>APM Performance Pathway (APP)
                        </li>
                        <li>
                            <span class="fa-li"><i class="fas fa-genderless"></i></span>All Payer Option
                        </li>
                    </ul>
                </div>
            </div>
            <p>It's important to make sure you qualify before you take those first steps.</p>
        </div>
    </div>
    <!-- End Block 2 -->

    <!-- Block 3 -->
    <div class="container bg--green-gradient">
        <div class="container__centered text--white center">
            <h2>Your MIPS Compass.</h2>
            <div>
                <div class="container__one-third">
                    &nbsp;
                </div>
                <div class="container__one-third">
                    <h3>Quality</h3>
                    <p>Submit collected data for at least 6 measures, or a complete specialty measure set, with bonus points available.</p>
                </div>
                <div class="container__one-third">
                    &nbsp;
                </div>
            </div>
            <div>
                <div class="container__one-third">
                    <h3>Promoting Interoperability</h3>
                    <p>Points for attesting to performance category measures organized under 4 objectives (2015 CEHRT), with bonuses available.</p>
                </div>
                <div class="container__one-third">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/white-compass-icon.png" alt="Compass Icon">
                </div>
                <div class="container__one-third">
                    <h3>Improvement Activities</h3>
                    <p>Choose from high or medium value activities that promote care coordination, beneficiary engagement, and patient safety.</p>
                </div>
            </div>
            <div>
                <div class="container__one-third">
                    &nbsp;
                </div>
                <div class="container__one-third">
                    <h3>Cost</h3>
                    <p>Cost is calculated on Medicare Spending Per Beneficiary (MSPB) and total per capita cost measures.</p>
                </div>
                <div class="container__one-third">
                    &nbsp;
                </div>
            </div>
        </div>
    </div>
    <!-- End Block 3 -->

    <!-- Block 4 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/two-backpackers-hiking-along-sea-side-mountains.jpg);">
        <div class="container__centered">
            <div class="container no-pad center" style="margin-bottom: 1em;">
                <h2>MEDITECH Supplies for a Successful Trip</h2>
                <p class="auto-margins">Every traveler needs supplies along the way; <a href="<?php print $url; ?>/ebooks/5-ehr-tools-for-thriving-under-value-based-care"> we've got you covered.</a> From embedded predictive analytics and quality reports to seamless connectivity with patients and outside providers, you'll be equipped with the tools you need to make the transition to value-based care models.</p>
            </div>
            <div class="container no-pad--top left">
                <div class="container__one-fourth transparent-overlay--white">
                    <h3 class="center">Improvement Activities</h3>
                    <p>Telehealth: integration of remote monitors and activity trackers</p>
                    <p>Population Health/Patient Registries</p>
                    <p>Behavioral Health</p>
                    <p>Patient Portal, including direct booking (beneficiary empowerment)</p>
                    <p>Virtual visits (expanded practice access)</p>
                    <p>Management of social determinants of health (achieving health equity)</p>
                </div>
                <div class="container__one-fourth transparent-overlay--white">
                    <h3 class="center">Quality</h3>
                    <p>Surveillance (predictive analytics)</p>
                    <p>Quality Vantage dashboards</p>
                    <p>Business and Clinical Analytics</p>
                </div>
                <div class="container__one-fourth transparent-overlay--white">
                    <h3 class="center">Promoting Interoperability</h3>
                    <p>Immunization registry</p>
                    <p>Robust security*</p>
                    <p>e-Prescribing* and medication reconciliation</p>
                    <p>Interoperability with HIEs* and member of Commonwell Health Alliance®</p>
                    <p>Transmission of patient summary</p>
                    <p>Patient and family access to records* and education</p>
                    <p>Secure patient messaging</p>
                    <p class="text--small">* indicates a requirement</p>
                </div>
                <div class="container__one-fourth transparent-overlay--white">
                    <h3 class="center">Cost</h3>
                    <p>Clinically driven charge capture</p>
                    <p>Integrated supply chain</p>
                    <p>Automated claims processing</p>
                </div>
            </div>
        </div>
    </div>
    <!-- End Block 4 -->

    <!-- Block 5 -->
    <div class="container bg--white">
        <div class="container__centered">
            <h2 class="center" style="margin-bottom: 1em;">
                Measuring Your Progress: How MIPS Calculates Scores.
            </h2>
            <div class="container__one-half center">
                <div class="pie-chart-container">
                    <h3>2021</h3>
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MACRA--pie-chart-3.png" alt="MIPS Pie Chart">
                </div>
            </div>
            <div class="container__one-half">
                <ul class="pie-chart-numbers">
                    <li>
                        <div class="pie-chart--key-color-square" style="background-color:#00bc6f;"></div>
                        <p>40% Quality</p>
                    </li>
                    <li>
                        <div class="pie-chart--key-color-square" style="background-color:#00bbb3;"></div>
                        <p>25% Promoting Interoperability (PI)</p>
                    </li>
                    <li>
                        <div class="pie-chart--key-color-square" style="background-color:#ff8300;"></div>
                        <p>20% Cost</p>
                    </li>
                    <li>
                        <div class="pie-chart--key-color-square" style="background-color:#b24ec4;"></div>
                        <p>15% Improvement Activities (IA)</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!-- End Block 5 -->

    <!-- Block 6 -->
    <div class="container bg--blue-gradient">
        <div class="container__centered text--white">
            <div class="center auto-margins">
                <h2>It IS a Competition: Tracking Penalties and Percentages.</h2>
                <p>No matter how well you perform, you are always being measured against your peers — which means the bar is always being raised. How do you stay positive?</p>
                <img style="width: 575px; margin: 1em 0 3em 0;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MACRA--timeline.png" alt="MACRA Timeline">
            </div>
            <div class="container__one-third">
                <p>Stay ahead of the competition by keeping a close eye on your measures. MEDITECH's Quality Vantage dashboards can help. Quality Vantage dashboards provide an interactive view of regulatory measure performance, giving you a "point-in-time" view of measures well before the reporting period and attestation. Color coding indicates where performance is not meeting thresholds, so you can easily see the areas that need extra attention. View by practice, group, hospital, or down to the clinician level — even drill down into a patient list to view patients, encounters, medications, and orders that are counted on the provider/measures.</p>
            </div>
            <div class="container__two-thirds">
                <div id="modal1" class="modal">
                    <a class="close-modal" href="javascript:void(0)">&times;</a>
                    <div class="modal-content">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-ambulatory_quality-vantage-2020.jpg" alt="MEDITECH ambulatory quality vantage Screenshot">
                    </div>
                </div>
                <div class="open-modal" data-target="modal1">
                    <div class="tablet--white">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-ambulatory_quality-vantage-2020.jpg" alt="MEDITECH ambulatory quality vantage Screenshot">
                    </div>
                    <div class="mag-bg">
                        <i class="mag-icon fas fa-search-plus"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="container__centered">
            <article class="center auto-margins">
                <div class="text--large italic" style="font-size:1.4em;">
                    <p>"Quality Vantage is a powerful tool for providers and organizations to view quality metrics in near-real time. Its simple and sleek design makes it easy to navigate for all staff. Having quality metrics within MEDITECH is an integral part of identifying and improving patients’ health easily."</p>
                </div>
                <p class="no-margin--bottom text--large bold">Scott Stone, Application Specialist, CAPM</p>
                <p>Kalispell Regional Healthcare</p>
            </article>
        </div>
    </div>
    <!-- End Block 6 -->

</div>
<!-- end js__seo-tool__body-content -->

<!--Block 7 -->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/red-and-white-lighthouse--dark.jpg);">
    <div class="container__centered text--white">
        <div class="container__one-third">
            &nbsp;
        </div>
        <div class="container__two-thirds">
            <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
            <h2>
                <?php print $cta->field_header_1['und'][0]['value']; ?>
            </h2>
            <?php } ?>

            <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
            <div>
                <?php print $cta->field_long_text_1['und'][0]['value']; ?>
            </div>
            <?php } ?>

            <div class="center" style="margin-top:2em;">
                <?php hubspot_button($cta_code, "Contact Our Regulatory Team"); ?>
            </div>

            <div style="margin-top:1em;" class="center">
                <?php print $share_link_buttons; ?>
            </div>

        </div>
    </div>
</div>
<!--End of Block 7 -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>
<!-- END campaign--node-2538.php -->
