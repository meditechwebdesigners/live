<!-- START about--node-4166.php -->

<?php // This template is set up to control the display of the MEDITECH Customer Awards Campaign

$url = $GLOBALS['base_url']; // grabs the site url
include('inc-share-buttons.php');
?>

<style>
	/*	Universal styles */
	.container.pad-adjust {
		padding: 5em 0;
	}

	.vert-adjust {
		padding-top: 5%;
	}

	.shadow-box.w-headshot {
		padding: 4.5em 2em 2em 2em;
		position: relative;
		margin-top: 60px;
	}

	.shadow-box.w-headshot-outline {
		padding: 4.5em 2em 2em 2em;
		position: relative;
		margin-top: 60px;
		box-shadow: none;
		border: 1px solid #c8ced9;
		background-color: transparent;
	}

	.shadow-box.outline {
		box-shadow: none;
		border: 1px solid #c8ced9;
		background-color: transparent;
	}

	.headshot {
		position: absolute;
		top: -60px;
		left: 50%;
		transform: translate(-50%);
	}

	.headshot img {
		width: 120px;
		height: 120px;
		border-radius: 50%;
	}

	.headshot-border {
		width: 175px;
		margin-bottom: .5em;
		border: 6px solid #e6e9ee;
		border-radius: 50%;
	}

	.card__info {
		padding: 1em 2em;
	}

	@media all and (max-width: 50em) {
		.container.pad-adjust {
			padding: 3em 0;
		}

		.vert-adjust {
			padding-top: 0;
		}

		.shadow-box.w-headshot,
		.shadow-box.w-headshot-outline {
			padding: 6em 2em 2em 2em;
			margin-top: 5em;
		}
	}

</style>

<div class="js__seo-tool__body-content">

	<h1 style="display:none;" class="js__seo-tool__title">MEDITECH Customer Awards</h1>

	<!-- START Block 1 -->
	<div class="container pad-adjust container__centered">
		<div class="container__centered">
			<div class="container__one-half">
				<div class="bg-pattern--container">
					<img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/female-medical-rep-meeting-with-hospital-staff.jpg" alt="Female medical rep meeting with hospital staff">
					<div class="bg-pattern--green-squares bg-pattern--left"></div>
				</div>
			</div>
			<div class="container__one-half content--pad-left">
				<p class="header-micro" style="margin-top: 3em;">ACHIEVING RESULTS, TOGETHER</p>
				<h2>Connection, Partnership, Success</h2>
				<p>A partnership with MEDITECH can open up new possibilities for your organization. Working together, we can make sure that our solutions address your unique needs.</p>
				<p>See how a shared mission and trust in your EHR vendor can lead to great results.</p>
				<div class="button--hubspot" style="margin-top:1em;">
          <a id="cta_button_2897117_9686be0a-4826-49da-9782-165c8ac10ab1" class="cta_button " href="https://info.meditech.com/cs/c/?cta_guid=9686be0a-4826-49da-9782-165c8ac10ab1&amp;signature=AAH58kF-IzbE1PUg0EN9imnTcCg0ZcbTuw&amp;placement_guid=6d388ce1-7f67-4de4-90ba-c65086aa2453&amp;click=407db0b1-ddc8-428d-abd3-c29acfe118e1&amp;hsutk=791af6c68cd6c39c492e0ba9e8ee9b91&amp;canon=https%3A%2F%2Fehr.meditech.com%2Fabout%2Fmeditech-customer-awards&amp;utm_referrer=https%3A%2F%2Fehr.meditech.com%2Fadmin%2Fcontent&amp;portal_id=2897117&amp;redirect_url=APefjpGq7Ffne3eFdlVfTUOfoPmwTrOAxY5EhMyTkdDiC3QrHIJT2YR50SV4KpqTinx8ypiTdgxWQeIOLgTm9-P20MtizGEFlqiDHqH6y97WBa4TAiQmU4p8eUC9JzhGEw97Tm7As6otslkyFqUnyB3h61wszkgwmb9oCXQvUEpDE60JojNyjKhQU7b4QX4V60lwoSySSfWFV8YsYn-n9xwf7o1JR6QdquJ1PRYFaqRJdkRJH0r1w0kGMmZYDHvNH2_ud23ayoVUDZABYxbxBMOvSk9oIkx2Ew" style="" cta_dest_link="https://ehr.meditech.com/ehr-solutions/customer-engagement" title="Learn More And Never Miss A Story">Learn More And Never Miss A Story</a>
			</div>
			</div>
		</div>
	</div>
	<!-- END Block 1 -->


	<!-- START Block 2 -->
	<div class="container bg--purple-gradient">
		<div class="container__centered">
			<div class="container__one-half content--pad-right vert-adjust">
				<p class="header-micro">INNOVATION</p>
				<h2>Reaching New Heights</h2>
				<p>Several <a href="<?php print $url; ?>/news/meditech-customers-named-to-chimes-most-wired-2021">MEDITECH customers</a> were named to <strong>CHIME's 2021 Digital Health Most Wired list</strong>.</p>
				<p>Avera Health was one of only seven organizations nationwide to achieve Level 10 status in both the acute and ambulatory settings.</p>
			</div>
			<div class="container__one-half">
				<div class="shadow-box w-headshot-outline">
					<div class="headshot">
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/Bruk-Kammerman.png" alt="Bruk Kammerman head shot">
					</div>
					<p class="text--large italic">“Avera’s history of Most Wired awards as well as our latest designation as Level 10 demonstrates that we are an industry leader. Avera fosters a strong culture of innovation, combining the latest technology with medical expertise to benefit our patients.”</p>
					<p class="bold no-margin--bottom">Bruk Kammerman</p>
					<p>Avera Chief Information Officer, Avera Health</p>
				</div>
			</div>
		</div>
	</div>
	<!-- END Block 2 -->


	<!-- START Block 3 -->
	<div class="container background--cover pad-adjust hide__bg-image--mobile" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/about/bg--organic-shapes-green-left.svg);">
		<div class="container__centered">
			<div class="container__one-half">
				<div class="shadow-box w-headshot">
					<div class="headshot">
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/P-Martin-Paslick.png" alt="P. Martin Paslick head shot">
					</div>
					<p class="text--large italic">"MEDITECH is a longtime partner of HCA Healthcare, and we are excited to continue working together through their next-generation software capabilities designed to better support our hospital teams and continue to improve care."</p>
					<p class="bold no-margin--bottom">P. Martin Paslick</p>
					<p>Senior Vice President and CIO, HCA Healthcare</p>
				</div>
			</div>
			<div class="container__one-half content--pad-left vert-adjust" style="margin-top: 1em;">
				<p class="header-micro">COLLABORATION</p>
				<h2>Working Together</h2>
				<p>HCA Healthcare and MEDITECH were recently honored with the <a href="<?php print $url; ?>/news/hca-healthcare-and-meditech-honored-with-chime-collaboration-award">CHIME Collaboration Award</a>.</p>
				<img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/CHIME-Collaboration-MEDITECH-HCA-Healthcare.png" alt="CHIME Collaboration Award for MEDITECH & HCA Healthcare">
			</div>
		</div>
	</div>
	<!-- END Block 3 -->


	<!-- START Block 4 -->
	<div class="container pad-adjust bg--purple-gradient">
		<div class="container__centered">
			<div class="container__one-half content--pad-right">
				<p class="header-micro">PIONEERS</p>
				<h2>Women In Health IT</h2>
				<p>Recognized as pioneers in the health IT field, MEDITECH customers are named as <a href="<?php print $url; ?>/news/meditech-executive-and-several-customers-named-women-in-health-it-to-watch-by-beckers-hospital">'Women in Health IT to Watch'</a> by Becker's Hospital Review.</p>
			</div>
			<div class="container__one-half center" style="margin-top: 1em;">
				<div class="container__one-half" style="margin-bottom: 1em;">
					<img class="headshot-border" src="<?php print $url; ?>/sites/all/themes/meditech/images/people/Renee-Fosberg.png" alt="Renee Fosberg head shot">
					<p class="bold no-margin--bottom">Renee Fosberg, VP &amp; CIO</p>
					<p>Emerson Hospital</p>
				</div>
				<div class="container__one-half">
					<img class="headshot-border" src="<?php print $url; ?>/sites/all/themes/meditech/images/people/Sheree-McFarland.png" alt="Sheree McFarland head shot">
					<p class="bold no-margin--bottom">Sheree McFarland, CIO</p>
					<p>West Florida at HCA Healthcare</p>
				</div>
			</div>
		</div>
	</div>
	<!-- END Block 4 -->


	<!-- START Block 5 -->
	<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/bg--organic-shapes-green-blue.svg);">
		<div class="container__centered">
			<div class="center">
				<p class="header-micro" style="display:inline-block;">GLOBAL IMPACT</p>
				<h2 style="margin-bottom: 1.5em;">Accolades For Our Customers Around The World</h2>
			</div>
			<div class="card__wrapper">
				<div class="container__one-half card">
					<figure>
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/alder-hey-paediatric-hospital.jpg" alt="Alder Hey paediatric hospital">
					</figure>
					<div class="card__info">
						<p><a href="<?php print $url; ?>/news/first-paediatric-hospital-in-europe-awarded-himss-emram-stage-7">Alder Hey is the first paediatric hospital in Europe awarded HIMSS EMRAM Stage 7</a></p>
					</div>
				</div>
				<div class="container__one-half card">
					<figure>
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/Julia-Hanigsberg-Holland-Bloorview-President-CEO.jpg" alt="Julia Hanigsberg, President and CEO of Holland Bloorview">
					</figure>
					<div class="card__info">
						<p><a href="<?php print $url; ?>/news/holland-bloorview-president-ceo-named-hitmc-healthcare-it-trailblazer-of-the-year">Julia Hanigsberg, President &amp; CEO of Holland Bloorview Kids Rehabilitation Hospital, named HITMC Healthcare IT Trailblazer of the Year</a></p>
					</div>
				</div>
			</div>
			<div class="center" style="margin-top:1.5em;">
				<?php hubspot_button('99dbdc30-9ed4-49dd-b0de-4b8f151e2da7', "Learn More About MEDITECH's Global Impact"); ?>
			</div>
		</div>
	</div>
	<!-- END Block 5 -->


	<!-- START Block 6 -->
	<div class="container pad-adjust bg--purple-gradient">
		<div class="container__centered">
			<div class="container__one-half">
				<p class="header-micro" style="margin-top: 0;">CUSTOMER SUCCESS</p>
				<h2>Stay up-to-date on MEDITECH customers leading in innovation</h2>
				<div style="margin:1.5em 0;">
					<?php hubspot_button('5230d784-296e-47df-96e2-88aab7a25cab', "Download The Innovator’s Booklet"); ?>
				</div>
			</div>
			<div class="container__one-half">
				<ul class="fa-ul">
					<li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span><a href="<?php print $url; ?>/news/meditech-customers-top-100-rural-and-community-hospitals-and-critical-access-lists">MEDITECH customers named as Chartis Group top 100 Rural and Community Hospitals and Critical Access Hospitals</a></li>
					<li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span><a href="<?php print $url; ?>/news/kingman-regional-wins-health-it-innovation-award">Kingman Regional wins Health IT Innovation Award</a></li>
					<li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span><a href="<?php print $url; ?>/news/meditech-customers-recognized-as-2021-healthgrades-award-recipients">Over 100 MEDITECH customers are 2021 Healthgrades Patient Safety Excellence Award™ and/or Outstanding Patient Experience Award™ recipients</a></li>
				</ul>
			</div>
		</div>
	</div>
	<!-- END Block 6 -->


	<!-- START Block 7 -->
	<div class="container background--cover pad-adjust" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/about/bg--organic-shapes-green-right.svg);">
		<div class="container__centered">
			<div class="container__one-half vert-adjust hide--mobile" style="margin-bottom: 1.5em;">
				<div class="bg-pattern--container">
					<img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/multiracial-business-team-meeting-in-lobby.jpg" alt="Multiracial business team meeting in lobby">
					<div class="bg-pattern--green-squares bg-pattern--left"></div>
				</div>
			</div>
			<div class="container__one-half content--pad-left">
				<div class="shadow-box w-headshot">
					<div class="headshot">
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/Helen-Waters.png" alt="Helen Waters head shot">
					</div>
					<p class="text--large italic">"Our customers are progressive leaders that have made a point of embracing advanced technologies to open up new possibilities for transformational change across their organizations. It's been an honor partnering with them as they continue to show the benefits a modern EHR can bring to their communities."</p>
					<p class="bold no-margin--bottom">Helen Waters</p>
					<p>CHIME Foundation Board Member, MEDITECH Executive Vice President and Chief Operating Officer</p>
				</div>
			</div>
		</div>
	</div>
	<!-- END Block 7 -->


	<!-- Block 8 -->
	<div class="container bg--purple-gradient">
		<div class="container__centered center">
		
        <h2>Join our community of customer leaders and share your story with us.</h2>
			<div class="center button--hubspot" style="margin-top:2em;">
          <a id="cta_button_2897117_9686be0a-4826-49da-9782-165c8ac10ab1" class="cta_button " href="https://info.meditech.com/cs/c/?cta_guid=9686be0a-4826-49da-9782-165c8ac10ab1&amp;signature=AAH58kF-IzbE1PUg0EN9imnTcCg0ZcbTuw&amp;placement_guid=6d388ce1-7f67-4de4-90ba-c65086aa2453&amp;click=407db0b1-ddc8-428d-abd3-c29acfe118e1&amp;hsutk=791af6c68cd6c39c492e0ba9e8ee9b91&amp;canon=https%3A%2F%2Fehr.meditech.com%2Fabout%2Fmeditech-customer-awards&amp;utm_referrer=https%3A%2F%2Fehr.meditech.com%2Fadmin%2Fcontent&amp;portal_id=2897117&amp;redirect_url=APefjpGq7Ffne3eFdlVfTUOfoPmwTrOAxY5EhMyTkdDiC3QrHIJT2YR50SV4KpqTinx8ypiTdgxWQeIOLgTm9-P20MtizGEFlqiDHqH6y97WBa4TAiQmU4p8eUC9JzhGEw97Tm7As6otslkyFqUnyB3h61wszkgwmb9oCXQvUEpDE60JojNyjKhQU7b4QX4V60lwoSySSfWFV8YsYn-n9xwf7o1JR6QdquJ1PRYFaqRJdkRJH0r1w0kGMmZYDHvNH2_ud23ayoVUDZABYxbxBMOvSk9oIkx2Ew" style="" cta_dest_link="https://ehr.meditech.com/ehr-solutions/customer-engagement" title="Learn More And Never Miss A Story">Learn More And Never Miss A Story</a>
			</div>
			<div>
				<?php print $share_link_buttons; ?>
			</div>
		</div>
	</div>
	<!-- END Block 8 -->

</div>
<!-- end js__seo-tool__body-content -->


<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>

<!-- end about--node-4166.php template -->
