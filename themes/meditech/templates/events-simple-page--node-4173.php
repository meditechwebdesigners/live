<!-- start events-simple-page--node-4173.php -->

<?php // This template is set up to control the display of the MEDITECH LIVE 22 Customer Event
$url = $GLOBALS['base_url']; // grabs the site url
include('inc-share-buttons.php');
?>

<style>
	/*	Block 1 - Event Banner */
	.event-banner {
		background-image: url(<?php print $websiteURL; ?>/sites/all/themes/meditech/images/events/MEDITECH-LIVE-22--coworkers-chatting-at-event--reversed.jpg);
		background-position: top;
	}

	.event-banner .container__centered {
		padding-top: 2em;
		padding-bottom: 5em;
	}

	h2.event-header {
		display: inline-block;
		line-height: 1;
		font-weight: 600;
		font-family: "montserrat", Verdana, sans-serif;
		color: #D2479D;
		/*non-webkit fallback*/
		font-size: 3em;
		text-transform: uppercase;
		background: -webkit-linear-gradient(135deg, #e65b25, #cd4699, #af1e4b);
		-webkit-background-clip: text;
		-webkit-text-fill-color: transparent;
		margin-top: 0;
	}

	/*	Block 1.5 - Event Registration Bar */
	.event-register-bar.shadow-box {
		position: relative;
		z-index: 2;
		display: flex;
		align-items: center;
		background-color: #fff;
		text-align: center;
		padding: 1.25em;
		box-shadow: 0px 0px 20px 1px rgb(120 120 120 / 20%);
		margin-top: -100px;
		margin-bottom: 0;
	}

	.event-register-bar.shadow-box p {
		margin-bottom: 0;
	}

	.event-logo img {
		width: 275px;
		margin-top: 0.5em;
	}

	div.button--hubspot {
		margin-bottom: 0;
	}

	/* Block 2 - Event Topics */
	.bg--light-blue {
		background-color: #f2f8fc;
	}

	.margin-adjust {
		margin-top: -100px;
		padding-top: 10em;
	}

	.event-topics {
		display: flex;
		justify-content: center;
		margin-bottom: 2.35765%;
	}

	.event-topics .first-topic {
		text-align: left;
		background-color: transparent;
		box-shadow: none;
	}

	.event-topics p {
		margin-bottom: 0;
	}

	.event-topics div {
		border-radius: 7px;
		padding: 1em;
		background-color: #fff;
		box-shadow: 0px 0px 20px 1px rgb(120 120 120 / 10%);
	}

	.event-topics .container__one-third {
		display: flex;
		align-items: center;
	}

	.event-topics .container__one-third img {
		padding: 0.75em;
		width: 100px;
		height: 100px;
	}

	/*	Block 3 - Featured Speakers */
	.ft--container {
		border: 1px solid #7877b3;
		padding: 2em;
		border-radius: 7px;
		margin-bottom: 1.75em;
	}

	.ft--flex {
		display: flex;
		align-items: center;
		margin-bottom: 1em;
	}

	.ft--headshot {
		width: 28%;
		margin-right: 1em;
	}

	.ft--headshot img {
		min-width: 75px;
	}

	.ft--title {
		width: 72%;
	}

	.ft--container p {
		margin-bottom: 0;
	}

	/* Block 4 - Agenda */
	.agenda-btns div {
		margin-right: 1em;
		display: inline-block;
	}

	.agenda-btns div:last-child {
		margin-right: 0;
	}

	.accordion__agenda {
		position: relative;
		margin-bottom: .5em;
	}

	.accordion__agenda ul li {
		margin-bottom: 0;
	}

	.accordion__agenda .header-micro {
		margin: .5em 0;
	}

	.accordion__agenda hr {
		margin: 1.25em 0;
	}

	.accordion__agenda:last-child {
		margin-bottom: 0;
	}

	.accordion__agenda .accordion__link {
		font-weight: bold;
		padding: 0 0 0 1.75em;
		font-family: "source-sans-pro", Helvetica, Arial, sans-serif;
	}

	.accordion__agenda .accordion__list__control {
		position: absolute;
		left: 0px;
		top: 0px;
	}

	.accordion__agenda .accordion__list__control {
		position: absolute;
		left: 0px;
		top: 0px;
	}

	.accordion__agenda .accordion__dropdown {
		padding: .75em 0 0 1.75em;
	}

	/*	Block 5 - Video */
	.content__callout,
	.content__callout__content {
		background-color: transparent;
	}

	.content__callout__image-wrapper {
		padding: 4em 5em;
	}

	.video-iframe {
		position: relative;
		padding-bottom: 56.25%;
		overflow: hidden;
		margin: 0 auto;
		border-radius: 6px;
		box-shadow: 7px 8px 26px rgba(0, 0, 0, 0.1);
	}

	.video-iframe iframe {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
	}

	/* Block 6 - FAQs */
	fieldset {
		background: none;
		border: 1px solid #bfc5d1;
		margin: 0 0 2em 0;
		border-radius: 7px;
		padding: 2em;
	}

	legend {
		text-align: center;
		padding: 0 1em;
		font-size: .875em;
		text-transform: uppercase;
		font-family: "montserrat", Verdana, sans-serif;
		font-weight: bold;
	}

	.accordion {
		margin-top: 0;
	}

	.accordion__list__item {
		border-radius: 7px;
		background: #fff;
		margin-bottom: 1.25em;
		position: relative;
		box-shadow: 0px 0px 20px 1px rgb(120 120 120 / 7%);
	}

	.accordion__link {
		padding: 1.2em 3em 1.2em 1.2em;
	}

	.accordion__list__control {
		position: absolute;
		right: 20px;
		top: 22px;
	}

	.accordion__dropdown {
		border-radius: 0 0 7px 7px;
		background: #fff;
		border: none;
	}

	/* Block 7 - Helen Quote */
	.event-quote {
		padding: 5em 0;
	}

	.event-headshot {
		width: 275px;
	}

	/*	Block 8 - Attendees */
	.shadow-box.w-headshot {
		padding: 5em 2em 2em 2em;
		position: relative;
		margin-top: 50px;
		margin-bottom: 3em;
	}

	.headshot {
		position: absolute;
		top: -63px;
		left: 50%;
		transform: translate(-50%);
	}

	.headshot img {
		width: 125px;
		height: 125px;
		border-radius: 50%;
	}

	/*	Block 9 - CTA */
	.event-cta {
		padding: 5em 1em;
		overflow: hidden;
	}

	.event-cta .shadow-box {
		position: relative;
		z-index: 2;
	}

	.glass-effect {
		background-color: rgba(255, 255, 255, .1);
		backdrop-filter: blur(10px);
		box-shadow: none;
	}

	.bounding-box {
		position: relative;
		z-index: 0;
	}

	.rotate-svg img {
		position: absolute;
		z-index: 1;
		width: 1050px;
		left: 43%;
		top: -375px;
	}

	.inner-arches img {
		-webkit-animation: spin 50s linear infinite;
		-moz-animation: spin 50s linear infinite;
		animation: spin 50s linear infinite;
	}

	.middle-arches img {
		-webkit-animation: spin 60s linear infinite;
		-moz-animation: spin 60s linear infinite;
		animation: spin 60s linear infinite;
	}

	.outer-arches img {
		-webkit-animation: spin 70s linear infinite;
		-moz-animation: spin 70s linear infinite;
		animation: spin 70s linear infinite;
	}

	@-moz-keyframes spin {
		100% {
			-moz-transform: rotate(360deg);
		}
	}

	@-webkit-keyframes spin {
		100% {
			-webkit-transform: rotate(360deg);
		}
	}

	@keyframes spin {
		100% {
			-webkit-transform: rotate(360deg);
			transform: rotate(360deg);
		}
	}

	/* Media Queries */
	@media all and (max-width: 1200px) {
		.rotate-svg img {
			left: 37%;
		}
	}

	@media all and (max-width: 900px) {
		h2.event-header {
			font-size: 2.5em;
		}

		.event-topics {
			flex-wrap: wrap;
			margin-bottom: 0;
		}

		.event-topics .first-topic {
			justify-content: center;
			text-align: center;
		}

		.event-topics .container__one-third {
			width: 100%;
			margin-right: 0;
			margin-bottom: 2.35765%;
		}

		.rotate-svg img {
			display: none;
		}
	}

	@media all and (max-width: 800px) {
		.event-banner {
			background: #32325d;
			background: -webkit-linear-gradient(#32325d, #25253b);
			background: -o-linear-gradient(#32325d, #25253b);
			background: -moz-linear-gradient(#32325d, #25253b);
			background-image: linear-gradient(-150deg, #32325d, #25253b);
		}

		.event-banner .container__centered {
			padding-top: 1em;
		}

		.event-headshot {
			margin-bottom: 1.5em;
		}

		.event-quote {
			padding: 5em;
			text-align: center;
		}

		.accordion__list__control {
			top: 17px;
		}

		.headshot {
			top: -50px;
		}

		.headshot img {
			width: 100px;
			height: 100px;
		}

		.content__callout__image-wrapper {
			padding-left: 6%;
			padding-right: 6%;
			padding-bottom: 1em;
		}

		.content__callout__content {
			text-align: center;
			padding-top: 1em;
			padding-bottom: 3em;
		}
	}

	@media (max-width: 650px) {
		.event-register-bar.shadow-box {
			flex-wrap: wrap;
		}

		.event-register-bar .container__one-third {
			margin-right: 0;
		}

		.margin-adjust {
			margin-top: -265px;
			padding-top: 22em;
		}

		.event-banner {
			text-align: center;
		}

		div.button--hubspot {
			margin: 1em;
		}

		.event-quote {
			padding: 2em;
		}
	}

	@media all and (max-width: 575px) {
		.agenda-btns div {
			display: block;
			margin-right: 0;
			margin-bottom: 1em;
		}
	}

	@media all and (max-width: 400px) {
		.ft--container {
			text-align: center;
		}

		.ft--flex {
			flex-wrap: wrap;
		}

		.ft--title {
			width: 100%;
		}

		.ft--headshot {
			width: 100%;
			margin-right: 0;
		}

		.ft--headshot img {
			width: 150px;
		}
	}

	@media (max-width: 340px) {
		h2.event-header {
			font-size: 2em;
		}
	}

	/* Turn Off Animation Preference */
	@media (prefers-reduced-motion) {

		.inner-arches img,
		.middle-arches img,
		.outer-arches img {
			animation: none;
		}
	}

</style>

<div class="js__seo-tool__body-content">

	<h1 style="display:none;" class="js__seo-tool__title">MEDITECH LIVE 2022 Customer Event</h1>

	<!-- START Block 1 -->
	<div class="container background--cover event-banner bg--purple-gradient">
		<div class="container__centered">
			<div class="container__one-half">
				<p class="bold montserrat no-margin--bottom" style="font-size: 1.5em;">CALLING ALL</p>
				<h2 class="event-header"><span style="font-weight:800;">CHANGE</span>MAKERS</h2>
				<p>MEDITECH invites healthcare leaders to a new, cross-disciplinary, immersive learning experience. Connect with innovative disrupters. Have authentic, focused discussions on the most substantive challenges we face as a community. And set a plan in motion for your organization to renew and recommit to its vision for the future.</p>
				<p><a href="<?php print $url; ?>/events/meditech-live-22#Agenda-Top">View the agenda</a> to see what we have planned.</p>
				<p>Follow and post using the hashtag <span class="bold">#MEDITECHLive2022</span> on <a href="https://www.linkedin.com/company/meditech" target="_blank">LinkedIn</a>, <a href="https://twitter.com/MEDITECH" target="_blank">Twitter</a>, and <a href="https://www.facebook.com/MeditechEHR" target="_blank">Facebook</a>.</p>
			</div>
		</div>
	</div>
	<!-- END Block 1 -->


	<!-- START Block 1.5 -->
	<div class="container__centered">
		<div class="shadow-box event-register-bar">
			<div class="container__one-third">
				<div class="event-logo">
					<img src="<?php print $websiteURL; ?>/sites/all/themes/meditech/images/events/MEDITECH-Live22-logo.svg" alt="MEDITECH LIVE 2022 logo">
				</div>
			</div>
			<div class="container__one-third">
				<p class="bold text--large">September 20 - 22</p>
				<p><a href="<?php print $websiteURL; ?>/about-meditech/directions-to-meditech">MEDITECH Conference Center</a></p>
				<p>Foxborough, MA</p>
				<p><a href="<?php print $websiteURL; ?>/about-meditech/area-hotels#Foxborough">Where to Stay</a> | <a href="<?php print $websiteURL; ?>/meditech-covid-19-policies-for-visitors">COVID Policy</a></p>
			</div>
			<div class="container__one-third">
				<?php hubspot_button('aedffd1d-31db-4186-960c-e01a048c9b66', "Register For MEDITECH LIVE"); ?>
			</div>
		</div>
	</div>
	<!-- END Block 1.5 -->


	<!-- START Block 2 -->
	<div class="container bg--light-blue margin-adjust">
		<div class="container__centered">
			<div class="agenda-btns center" style="margin-bottom: 4em;">
				<div><a href="<?php print $url; ?>/events/meditech-live-22#speakers" class="btn--emerald">Speakers</a></div>
				<div><a href="<?php print $url; ?>/events/meditech-live-22#Agenda-Top" class="btn--emerald">Agenda</a></div>
				<div><a href="<?php print $url; ?>/events/meditech-live-22#faq" class="btn--emerald">FAQ</a></div>
				<div><a href="<?php print $url; ?>/events/meditech-live-22#events" class="btn--emerald">Events</a></div>
			</div>
			<div class="auto-margins center">
				<h2>Review, rethink, and revise together.</h2>
				<p>The healthcare industry is at a critical juncture. The demands of digital transformation, value-based care, and consumer engagement are merging. Our success as leaders depends on our ability to connect with others and collaborate across disciplines. This event will gather diverse decision makers together for a rare opportunity of focused idea generation and creative problem solving.</p>
			</div>
			<div class="container event-topics no-pad--bottom">
				<div class="container__one-third first-topic">
					<h3>We will explore relevant, timely topics such as:</h3>
				</div>
				<div class="container__one-third">
					<img src="<?php print $websiteURL; ?>/sites/all/themes/meditech/images/events/icon--innovative-technologies.svg" alt="Light bulb icon">
					<p>Accelerating the adoption of innovative technologies</p>
				</div>
				<div class="container__one-third">
					<img src="<?php print $websiteURL; ?>/sites/all/themes/meditech/images/events/icon--personalized-care.svg" alt="Hand holding a heart icon">
					<p>Improving the patient experience through personalized care</p>
				</div>
			</div>
			<div class="container no-pad--top event-topics">
				<div class="container__one-third">
					<img src="<?php print $websiteURL; ?>/sites/all/themes/meditech/images/events/icon--clinician-burnout.svg" alt="Bright person icon">
					<p>Reducing staff turnover and clinician burnout</p>
				</div>
				<div class="container__one-third">
					<img src="<?php print $websiteURL; ?>/sites/all/themes/meditech/images/events/icon--health-equity.svg" alt="Smart watch icon">
					<p>Addressing health equity and social determinants</p>
				</div>
				<div class="container__one-third">
					<img src="<?php print $websiteURL; ?>/sites/all/themes/meditech/images/events/icon--interconnected-world.svg" alt="Focused globe icon">
					<p>Maintaining your organization's independence in an interconnected world</p>
				</div>
			</div>
		</div>
	</div>
	<!-- END Block 2 -->


	<!-- Block 3 -->
	<div id="speakers" class="container bg--purple-gradient">
		<div class="container__centered">

			<div class="center" style="margin-bottom: 2.5em;">
				<h2>Featured Speakers</h2>
			</div>

			<div class="container__one-half">
				<div id="Michael-Cuffe" class="ft--container">
					<div class="ft--flex">
						<div class="ft--headshot">
							<img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/Michael-Cuffe--headshot.png" alt="Michael Cuffe, MD, MBA">
						</div>
						<div class="ft--title">
							<p class="bold">Michael Cuffe, MD, MBA</p>
							<p>Executive Vice President and Chief Clinical Officer of HCA Healthcare</p>
							<p><a href="<?php print $url; ?>/events/meditech-live-22#Session-Cuffe">Session Information</a></p>
						</div>
					</div>
					<p>In his role at HCA — one of the nation's leading providers of healthcare services — Dr. Cuffe is responsible for clinical quality, nursing, clinical informatics, care transformation, urgent care operations, graduate medical education, laboratory services, and for overseeing more than 13,000 employed and managed physicians.</p>
				</div>

				<div id="Vivian-Lee" class="ft--container">
					<div class="ft--flex">
						<div class="ft--headshot">
							<img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/Vivian-Lee--headshot.png" alt="Vivian Lee, MD, PhD, MBA">
						</div>
						<div class="ft--title">
							<p class="bold">Vivian Lee, MD, PhD, MBA</p>
							<p>President of Verily Health</p>
							<p><a href="<?php print $url; ?>/events/meditech-live-22#Session-Lee">Session Information</a></p>
						</div>
					</div>
					<p>Dr. Lee is the president of Health Platforms at Verily Life Sciences, whose mission is to apply digital solutions that enable people to enjoy healthier lives. A passionate champion of improving health, she works closely with Verily's clinical and engineering teams to develop products and platforms that support the successful transformation of health systems as well as providers, patients, and communities. Dr. Lee is also the author of the acclaimed book, <span class="italic">The Long Fix: Solving America's Health Care Crisis with Strategies that Work for Everyone (Norton, 2020)</span>.</p>
				</div>

				<div id="Ryan-Terry" class="ft--container">
					<div class="ft--flex">
						<div class="ft--headshot">
							<img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/Ryan-Terry--headshot.png" alt="Ryan A. Terry">
						</div>
						<div class="ft--title">
							<p class="bold">Ryan A. Terry</p>
							<p>Digital Transformation Officer, Managing Director, Healthcare &amp; Life Sciences, Google Cloud</p>
						</div>
					</div>
					<p>Ryan leads Healthcare and Life Sciences for the Cloud Customer Experience organization. He and his teams work with strategic customers and partners to guide them through the process of adopting cloud and Alphabet solutions. He also serves as an advisor to MEDITECH's delivery teams, focusing on driving long-term value to both our customers and Google.</p>
				</div>
			</div>

			<div class="container__one-half">
				<div id="John-Halamka" class="ft--container">
					<div class="ft--flex">
						<div class="ft--headshot">
							<img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/John-Halamka--headshot.png" alt="John Halamka, MD, MS">
						</div>
						<div class="ft--title">
							<p class="bold">John Halamka, MD, MS</p>
							<p>President of Mayo Clinic Platform</p>
							<p><a href="<?php print $url; ?>/events/meditech-live-22#Session-Halamka">Session Information</a></p>
						</div>
					</div>
					<p>As president of the Mayo Clinic Platform, Dr. Halamka leads a portfolio of platform businesses focused on transforming health care by leveraging artificial intelligence, connected healthcare devices, and a network of trusted partners. An active emergency room physician trainer, Dr. Halamka has been developing and implementing health care information strategy and policy for more than 25 years.</p>
				</div>

				<div id="Stacy-Palmer" class="ft--container">
					<div class="ft--flex">
						<div class="ft--headshot">
							<img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/Stacy-Palmer--headshot.png" alt="Stacy Palmer, VP, COO">
						</div>
						<div class="ft--title">
							<p class="bold">Stacy Palmer</p>
							<p>Senior Vice President and COO of The Beryl Institute</p>
							<p><a href="<?php print $url; ?>/events/meditech-live-22#Session-Palmer">Session Information</a></p>
						</div>
					</div>
					<p>As Senior Vice President and COO of The Beryl Institute, Ms. Palmer has helped lead the expansion of patient experience as a central conversation in healthcare. A visionary thinker and pragmatic strategist, she joined the organization in 2010 and assisted in establishing the Institute as a membership organization and global thought leader.</p>
				</div>

				<div id="Micky-Tripathi" class="ft--container">
					<div class="ft--flex">
						<div class="ft--headshot">
							<img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/Micky-Tripathi--headshot.png" alt="Micky Tripathi, PhD, MPP">
						</div>
						<div class="ft--title">
							<p class="bold">Micky Tripathi, PhD, MPP</p>
							<p>National Coordinator for Health IT</p>
						</div>
					</div>
					<p>Dr. Tripathi is the National Coordinator for Health Information Technology at the U.S. Department of Health and Human Services, where he leads the formulation of our federal health IT strategy and coordinates federal health IT policies, standards, programs, and investments. He has over 20 years of experience across the health IT landscape in areas ranging from population health management and value-based care to interoperability and clinical data analytics.</p>
				</div>
			</div>

		</div>
	</div>
	<!-- End Block 3 -->


	<!-- Block 4 -->
	<div id="Agenda-Top" class="container bg--white" style="padding: 5em 0;">
		<div class="container__centered">
			<h2 class="center" style="margin-bottom: 1.5em;">Agenda</h2>

			<div class="agenda-btns center" style="margin-bottom: 2em;">
				<div><a href="<?php print $url; ?>/events/meditech-live-22#Day1" class="btn--emerald">Day 1 (Tuesday 9/20)</a></div>
				<div><a href="<?php print $url; ?>/events/meditech-live-22#Day2" class="btn--emerald">Day 2 (Wednesday 9/21)</a></div>
				<div><a href="<?php print $url; ?>/events/meditech-live-22#Day3" class="btn--emerald">Day 3 (Thursday 9/22)</a></div>
			</div>

			<div id="Day1" class="container auto-margins" style="padding: 0 0 1.5em 0;">
				<fieldset>
					<legend>Day 1 &nbsp;|&nbsp; Tuesday, September 20</legend>
					<div class="accordion__dropdown__container">
						<div class="accordion__dropdown__container__left" style="margin-top:0;">
							<p class="bold">11:30 a.m.</p>
						</div>
						<div class="accordion__dropdown__container__right" style="margin-top:0;">
							<p class="bold">Registration and Lunch</p>
						</div>
					</div>
					<div class="accordion__dropdown__container">
						<div class="accordion__dropdown__container__left">
							<p class="bold">12:30 p.m.</p>
						</div>
						<div class="accordion__dropdown__container__right">
							<p class="bold">MEDITECH Welcome</p>
							<p class="italic">Helen Waters, Executive Vice President &amp; COO, MEDITECH</p>
						</div>
					</div>
					<div id="Session-Cuffe" class="accordion__dropdown__container">
						<div class="accordion__dropdown__container__left">
							<p class="bold">1 p.m.</p>
						</div>
						<div class="accordion__dropdown__container__right">
							<p class="bold no-margin--bottom">Leadership Through Change and Disruption</p>
							<p class="italic no-margin--bottom"><a href="<?php print $url; ?>/events/meditech-live-22#Michael-Cuffe">Michael Cuffe, MD, MBA, Executive Vice President &amp; Chief Clinical Officer, HCA Healthcare</a></p>
							<p>Dr. Cuffe will discuss healthcare initiatives that are transforming the way we deliver care today — including digital health innovations, patient engagement, workforce challenges, health equity, and value-based reimbursement models — while providing strategies for organizational leadership.</p>
						</div>
					</div>
					<div class="accordion__dropdown__container">
						<div class="accordion__dropdown__container__left">
							<p class="bold">2 p.m.</p>
						</div>
						<div class="accordion__dropdown__container__right">
							<p class="bold">Break/Visit the Exhibitors</p>
						</div>
					</div>
					<div class="accordion__dropdown__container">
						<div class="accordion__dropdown__container__left">
							<p class="bold">2:30 p.m.</p>
						</div>
						<div class="accordion__dropdown__container__right">
							<div class="accordion accordion__agenda">
								<ul class="accordion__list">
									<li id="item">
										<a class="accordion__link" href="#">Track Sessions<div class="accordion__list__control"></div></a>
										<div class="accordion__dropdown">

											<p class="header-micro">Health Equity</p>
											<p class="no-margin--bottom bold">Social Determinants of Health and Health Equity Townhall</p>
											<p class="italic">Moderator: Kate Jenkins-Brown, Product Manager, Population Health &amp; Care Management, MEDITECH<br>
												Panelists: Heather Kirby, Vice President, Integrated Care &amp; Chief Population Health Officer, Frederick Health<br>
												Mitchell Izower, MD, Hospitalist and Clinical Informaticist, Beth Israel Lahey Health and MEDITECH<br>
												Christina Wolf, MSN, RN, CNL, Executive Director, Population Health &amp; Care Continuum, Lawrence General Hospital</p>

											<hr>

											<p class="header-micro">Patient Experience</p>
											<p class="no-margin--bottom bold">Innovating Virtual Care</p>
											<p class="no-margin--bottom italic">Marsha Fearing, MD, Account Executive, Canadian Physician Specialist, MEDITECH<br>
												Jessica Haller, Senior Supervisor, Client Services, MEDITECH</p>
											<p>The SARS CoV-2 public health crisis has driven digital health innovation and virtual care in ways unparalleled in modern history. Join us for a discussion on how virtual care can optimize (or limit) the scope of the medical care you deliver in your environment.</p>

											<hr>

											<p class="header-micro">Digital Health</p>
											<p class="no-margin--bottom bold">Mobility</p>
											<p class="italic no-margin--bottom">David Dean, CEO, ACS MediHealth<br>
												Lee Howard, Vice President, Client Services, Forward Advantage<br>
												Rebecca Lancaster, CPM, Director, Product Management, MEDITECH</p>
											<p>Apps and mobility are cornerstones for adopting technology in today's world. As EHRs evolve, clinicians need to maximize their device time with solutions focused on streamlining workflows and accomplishing tasks. During this session, we will discuss the efficiencies mobility can bring to clinician workflows and how they can help address the challenge of cognitive burden. </p>

											<hr>

											<p class="header-micro">Leadership</p>
											<p class="no-margin--bottom bold">Reducing Staff Turnover and Clinician Burnout</p>
											<p class="italic">Moderator: Cathy Turner, BSN, RN, Associate Vice President, MEDITECH<br>
												Panelists: Diane Drexler, Vice President of Patient Care Services/Chief Nursing Officer, Community Memorial Health System<br>
												Diane Cornell, Director, Critical Care and Cardiovascular Services, Community Memorial Health System<br>
												Colin Banas, MD, MHA, Chief Medical Officer, DrFirst</p>

											<hr>

											<p class="header-micro">Changemaker</p>
											<p class="no-margin--bottom bold">Transforming Care with the Cloud: A MaaS Discussion</p>
											<p class="italic no-margin--bottom">Randy Brandt, PA-C, Mile Bluff Medical Center<br>
												Travis Boucher, CFO, Speare Memorial Hospital<br>
												Jonathan Hatfield, Director of IS, Klickitat Valley Health</p>
											<p>Come and hear from existing MAAS sites on their journey to the cloud.</p>

											<hr>

											<p class="header-micro">Canada</p>
											<p class="no-margin--bottom bold">What's On the Horizon: An Overview of the Canadian Landscape</p>
											<p class="italic no-margin--bottom">Robert Molloy, Director, Canadian Market and Product Strategy, MEDITECH</p>
											<p>Attend this session for an overview of the Canadian landscape and development activities shaping the roadmap for our customers.</p>

										</div>
									</li>
								</ul>
							</div>

						</div>
					</div>
					<div class="accordion__dropdown__container">
						<div class="accordion__dropdown__container__left">
							<p class="bold">3:30 p.m.</p>
						</div>
						<div class="accordion__dropdown__container__right">
							<p class="bold">Break</p>
						</div>
					</div>
					<div class="accordion__dropdown__container">
						<div class="accordion__dropdown__container__left">
							<p class="bold">3:45 p.m.</p>
						</div>
						<div class="accordion__dropdown__container__right">
							<div class="accordion accordion__agenda">
								<ul class="accordion__list">
									<li id="item">
										<a class="accordion__link" href="#">Track Sessions<div class="accordion__list__control"></div></a>
										<div class="accordion__dropdown">

											<p class="header-micro">Health Equity</p>
											<p class="no-margin--bottom bold">Addressing Health Literacy</p>
											<p class="italic no-margin--bottom">Charles Lee, Senior Director, Clinical Knowledge - Meducation, First Databank</p>
											<p>Hear from First Databank Senior Director Dr. Charles Lee as he provides an overview of health literacy - including its impact on health outcomes as well as practical approaches to reduce medication errors, improve adherence, and hone techniques for complex devices.</p>

											<hr>

											<p class="header-micro">Patient Experience</p>
											<p class="no-margin--bottom bold">Patients Want an Amazon Experience: How to Build a Consumer-Focused &amp; Branded Patient Experience Across the Health System</p>
											<p class="italic no-margin--bottom">Audrey Brislin, Vice President, Marketing &amp; Product, Forward Advantage<br>
												Brian Davis, CIO, Magnolia Regional Health Center</p>
											<p>In today's consumer-driven environment, health systems are tasked with creating a comprehensive digital strategy that aligns branding and patient experience. How do you leverage your MEDITECH EHR while bringing multiple patient engagement strategies and tools under one umbrella while keeping the patient at the center? Hear how Magnolia Regional Health Center is advancing its digital strategy across the health system operationally with an integrated approach, so consumers have a consistent experience with the brand throughout the continuum of care.</p>

											<hr>

											<p class="header-micro">Digital Health</p>
											<p class="no-margin--bottom bold">New Opportunities, Existing Priorities: Apps, Data, and Quality Care Delivery</p>
											<p class="no-margin--bottom italic">Jim Jirjis, MD, Chief Health Information Officer, HCA Healthcare<br>
												John Valutkevich, Director of Programs, Drummond Group, LLC<br>
												Mike Cordeiro, Director, Interoperability Market &amp; Product Strategy, MEDITECH<br>
												Jason Vogt, Senior Supervisor, Interoperability API &amp; Structured Documents, Product Development, MEDITECH</p>
											<p>The intent of the 21st Century Cures Act is to "advance interoperability and support the access, exchange and use of electronic health information," as well as to support patient access to their EHI in a form most convenient to the patient. Data security remains the most important priority for clinical interoperability as more patients begin to leverage technology to access and share their health data. This session will review the ONC Cures Act final rule, explore solutions to support patient access, and discuss factors organizations need to consider in an app onboarding strategy.</p>

											<hr>

											<p class="header-micro">Leadership</p>
											<p class="no-margin--bottom bold">Revenue Cycle Management: What's Trending</p>
											<p class="italic no-margin--bottom">Roger Lutz, CIO, Butler Health System<br>
												Melissa Hall, CIO and AVP, Information Services, Calvert Health Medical Center<br>
												Christy Wright, Chief Marketing Officer, SSI Group<br>
												Lori Brocato, Senior Vice President, Product Management, SSI Group</p>
											<p>As revenue cycle leaders continue to navigate the lingering impact of the pandemic and the ever-changing regulatory/payer landscape, they are also preparing for whatever may come next. Attend this session for a discussion on the current trends in revenue cycle management, including those with the most impact on healthcare leaders.</p>

											<hr>

											<p class="header-micro">Changemaker</p>
											<p class="bold no-margin--bottom">The Moving Healthcare Landscape</p>
											<p class="italic no-margin--bottom">John McDaniels, Former CIO, Kingman Regional Medical Center</p>
											<p>During this session, Former Kingman Regional Medical Center CIO John McDaniels will explore the moving landscape of healthcare, how it will dictate new delivery solutions and services, as well as the role IT will play in facilitating healthcare transformation.</p>

											<hr>

											<p class="header-micro">Canada</p>
											<p class="bold no-margin--bottom">Canadian Interoperability Initiatives </p>
											<p class="no-margin--bottom italic">Allie Marks, Canadian Regulatory Program Manager</p>
											<p>Learn about the latest innovative technologies and standards on the horizon that will help to connect communities across Canada.</p>

										</div>
									</li>
								</ul>
							</div>

						</div>
					</div>
					<div class="accordion__dropdown__container">
						<div class="accordion__dropdown__container__left">
							<p class="bold">4:45 p.m.</p>
						</div>
						<div class="accordion__dropdown__container__right">
							<p class="bold">Networking Reception</p>
						</div>
					</div>
				</fieldset>
			</div>

			<div id="Day2" class="container no-pad auto-margins">
				<fieldset>
					<legend>Day 2 &nbsp;|&nbsp; Wednesday, September 21</legend>
					<div id="Session-Lee" class="accordion__dropdown__container">
						<div class="accordion__dropdown__container__left" style="margin-top:0;">
							<p class="bold">8 a.m.</p>
						</div>
						<div class="accordion__dropdown__container__right" style="margin-top:0;">
							<p class="bold no-margin--bottom">Featured Speaker</p>
							<p class="italic no-margin--bottom"><a href="<?php print $url; ?>/events/meditech-live-22#Vivian-Lee">Vivian Lee, MD, PhD, MBA, President of Verily Health</a></p>
							<p>Verily Health Platforms President and author of <a href="https://wwnorton.com/books/9781324006671" target="_blank">The Long Fix</a>, Vivian S. Lee, MD, MBA will share her perspective on what it means to advance precision health and achieve interoperability. Dr. Lee is widely recognized as a leader in health technology, payer, and health delivery systems. Throughout her discussion she will provide insights on why the best way to improve a fragmented ecosystem is to use technology to align incentives among all players to deliver better outcomes, improve experiences, and lower costs. Dr. Lee will also share how Verily is reimagining healthcare with a user-centric design that helps elevate digital health platforms and better engage patients, their caregivers, and clinicians; as well as the role transformative risk and payment models play in supporting these efforts.</p>
						</div>
					</div>
					<div class="accordion__dropdown__container">
						<div class="accordion__dropdown__container__left">
							<p class="bold">9 a.m.</p>
						</div>
						<div class="accordion__dropdown__container__right">
							<p class="bold no-margin--bottom">Exhibitor Time and Vivian Lee Book Signing</p>
							<p class="italic">MEDITECH Professional Services Booth</p>
						</div>
					</div>
					<div class="accordion__dropdown__container">
						<div class="accordion__dropdown__container__left">
							<p class="bold">9:30 a.m.</p>
						</div>
						<div class="accordion__dropdown__container__right">
							<p class="bold no-margin--bottom">Digital Health Strategy Panel</p>
							<p class="italic no-margin--bottom">Moderator: Christine Silva, Senior Director, Product Management, MEDITECH</p>
							<p class="italic">Panelists: Mary K. Moscato, FACHE, President, Hebrew SeniorLife Health Care Services and Hebrew Rehabilitation Center<br>
								Mike Burke, Assistant Vice President, Information Systems, Valley Health System<br>
								Dan Nash, Chief Information Officer, Emanate Health
							</p>
						</div>
					</div>
					<div class="accordion__dropdown__container">
						<div class="accordion__dropdown__container__left">
							<p class="bold">10:30 a.m.</p>
						</div>
						<div class="accordion__dropdown__container__right">
							<p class="bold no-margin--bottom">Break/Visit the Exhibitors</p>
						</div>
					</div>
					<div id="Session-Palmer" class="accordion__dropdown__container">
						<div class="accordion__dropdown__container__left">
							<p class="bold">11 a.m.</p>
						</div>
						<div class="accordion__dropdown__container__right">
							<div class="accordion accordion__agenda">
								<ul class="accordion__list">
									<li id="item">
										<a class="accordion__link" href="#">Track Sessions<div class="accordion__list__control"></div></a>
										<div class="accordion__dropdown">

											<p class="header-micro">Health Equity</p>
											<p class="no-margin--bottom bold">Health Equity: Achieving Your Community's Full Potential</p>
											<p class="italic no-margin--bottom">Kim Maples, RN, BSN, IT Applications Manager, Carteret Health Care</p>
											<p>During this discussion on social determinants of health, Kim Maples will share how Carteret is working with their HIE to use data in a more meaningful way for their ACO. Attendees will learn how the organization signed up to participate with the EQIC (Eastern Quality Improvement Collaborative) to work through their Health Equity toolkit. </p>

											<hr>

											<p class="header-micro">Patient Experience</p>
											<p class="no-margin--bottom bold">Guiding Principles for Experience Excellence</p>
											<p class="no-margin--bottom italic"><a href="<?php print $url; ?>/events/meditech-live-22#Stacy-Palmer">Stacy Palmer, Senior Vice President and COO, The Beryl Institute</a></p>
											<p>This presentation outlines eight actions foundational to a comprehensive experience strategy. Organizations focused on achieving experience excellence will commit to these fundamental building blocks, laying the groundwork to get started or to reset the foundation on which their initiatives are built. </p>

											<hr>

											<p class="header-micro">Digital Health</p>
											<p class="no-margin--bottom bold">The Path to Successful Precision Medicine</p>
											<p class="italic no-margin--bottom">Jackie Rice, BSN, RN, Vice President for Information Technology and CIO, Frederick Health<br>
												Jennifer Ford, Product Manager, Strategy, MEDITECH</p>
											<p>Genomics continues its astounding growth in the healthcare industry. Attend this session for a look at genomics and precision medicine in the community setting, and how to best leverage the data. Key topics of discussion include the benefits of including DNA data in daily care for more positive outcomes, and how to influence organizational engagement. We'll also share strategies on how to overcome some key challenges such as physician buy-in, integrating with laboratories, and starting a Pharmacogenomic program from scratch.</p>

											<hr>

											<p class="header-micro">Leadership</p>
											<p class="no-margin--bottom bold">Change Management: The Key to Thriving Amid Constant Change</p>
											<p class="no-margin--bottom italic">Desiree Paoli, Director, Product Marketing, Interlace Health<br>
												Sameer Brahmavar, Chief Product Officer, Interlace Health<br>
												Michael Burke, Assistant Vice President, Information Systems, Valley Health System<br>
												Bob Gronberg, Senior Director, CereCore</p>
											<p>While technology can ease the path for clinicians, staff, and patients, organizations are often reluctant to fully adopt it. When processes are deeply ingrained and there are high-stakes outcomes, it’s difficult to imagine overhauling operations in any way. Join us for a panel discussion on how hospital and health system leadership can build a change management strategy. We will focus on the people who are critical to implementing and supporting the digital transformations that are essential in 2022 and beyond.</p>

											<hr>

											<p class="header-micro">Changemaker</p>
											<p class="no-margin--bottom bold">CMIO 3.0: The Evolving Role</p>
											<p class="no-margin--bottom italic">Howard Landa, MD, Vice President of Clinical Informatics and EHR, Sutter Health<br>
												Louis Harris, CMIO, MD, Citizens Memorial Healthcare</p>
											<p>A discussion around the role of the CMIO, how it has evolved over time, and where we see it going in the future.</p>

											<hr>

											<p class="header-micro">Canada</p>
											<p class="no-margin--bottom bold">Improving Clinical Outcomes with Digital Patient Engagement and Remote Patient Monitoring Programs</p>
											<p class="no-margin--bottom italic">James Chan, PhD, Director of Innovation, Sault Area Hospital<br>
												Joshua Liu, MD, Co-Founder and Chief Executive Officer, SeamlessMD</p>
											<p>Sault Area Hospital has worked with SeamlessMD for remote patient monitoring to provide a scalable way to engage and monitor patients outside the hospital. Come hear how this program can leverage digital-savvy patients to improve the health consumer experience and outcomes.</p>

										</div>
									</li>
								</ul>
							</div>

						</div>
					</div>
					<div class="accordion__dropdown__container">
						<div class="accordion__dropdown__container__left">
							<p class="bold">12 p.m.</p>
						</div>
						<div class="accordion__dropdown__container__right">
							<p class="bold">Lunch</p>
						</div>
					</div>
					<div class="accordion__dropdown__container">
						<div class="accordion__dropdown__container__left">
							<p class="bold">1 p.m.</p>
						</div>
						<div class="accordion__dropdown__container__right">
							<div class="accordion accordion__agenda">
								<ul class="accordion__list">
									<li id="item">
										<a class="accordion__link" href="#">Track Sessions<div class="accordion__list__control"></div></a>
										<div class="accordion__dropdown">

											<p class="header-micro">Health Equity</p>
											<p class="no-margin--bottom bold">Health Equity: Strategies, Considerations &amp; Barriers to Collecting and Addressing SDOH</p>
											<p class="italic no-margin--bottom">Janet Desroche, Associate Vice President, Professional Services, MEDITECH<br>
												Barbara Hobbs, Senior Government Affairs Manager, Meaningful Use and Government Initiatives, MEDITECH<br>
												Mitchell Izower, MD, Hospitalist and Clinical Informaticist, Beth Israel Lahey Health and MEDITECH</p>
											<p>CMS defines health equity as the attainment of the highest level of health for all people, where everyone has a fair and just opportunity to attain optimal health regardless of race, ethnicity, disability, sexual orientation, gender identity, socioeconomic status, geography, preferred language, or other factors that affect access to care and health outcomes. These factors or Social Determinants of Health (SDOH) are widely recognized as important predictors in clinical care, as patients with social risk factors often experience worse outcomes and increased costs. During this session, we will explore the benefits, barriers, considerations, and strategies to developing programs that screen for, collect, and intervene on SDOH factors in your community, while leveraging data analytics and the EHR. </p>

											<hr>

											<p class="header-micro">Patient Experience</p>
											<p class="no-margin--bottom bold">Revolutionizing the Revenue Cycle for the Patient Experience</p>
											<p class="italic no-margin--bottom">Betsey O'Brien, Director, EHR, Revenue Cycle &amp; Access Applications, BILH IT System Services, EHR-MEDITECH, Beth Israel Lahey Clinic</p>
											<p>It's time for a revolution! There are countless factors that pressure the financial health of a healthcare organization, such as price transparency, forced cancellations of elective procedures, and ongoing clinician burnout. These can result in increased costs while institutions see decreased reimbursement. So how can healthcare leaders employ technology to ease the burdens that these and other factors are placing on their organizations? Attend this session to learn how automation, artificial intelligence, and machine learning can be applied to the revenue cycle process to evolve RCM capabilities and improve an organization's financial health while also enhancing the patient experience. </p>

											<hr>

											<p class="header-micro">Digital Health</p>
											<p class="no-margin--bottom bold">Disease Management: Getting Started with Remote Patient Monitoring</p>
											<p class="no-margin--bottom italic">Stephen Tingley, MD, CMIO of Mount Nittany Medical Center<br>
												Lucienne Ide, MD, PhD, Founder and CEO, Rimidi</p>
											<p>As disease management programs and technology evolve, they are becoming more effective in supporting people with multiple chronic conditions in their day-to-day lives. Hear from Dr. Stephen Tingley from Mount Nittany as he shares his experience in managing patients with diabetes. We will also explore the evolution of care via remote patient monitoring devices.</p>

											<hr>

											<p class="header-micro">Leadership</p>
											<p class="no-margin--bottom bold">Community Hospital of the Future: What Does it Mean to You?</p>
											<p class="italic no-margin--bottom">Mark Gridley, MBA, FACHE, President and Chief Executive Officer, FHN Memorial Hospital</p>
											<p>This interactive session will be a facilitated discussion for community hospital leaders to share their top concerns and current initiatives underway for the next three to five years. In order to maximize the time value of this networking conversation for all participants, please submit any questions you would like to add to the discussion to <a href="https://home.meditech.com/webforms/contact.asp?rcpt=lrobblee|meditech|com&rname=lrobblee">Lynn Robblee</a> prior to the event.</p>

											<hr>

											<p class="header-micro">Changemaker</p>
											<p class="no-margin--bottom bold">Pharmacogenomics: It's More than Genes</p>
											<p class="italic no-margin--bottom">Donald Levick, MD, MBA, CPE, FHIMSS, Pediatrician, Former Chief Medical Information Officer, Lehigh Valley Health Network<br>
												Anna Dover, PharmD, BCPS, Director, Product Management, First Databank</p>
											<p>Precision medicine and pharmacogenomics are emerging as powerful tools that will impact all aspects of healthcare at the point of care. Implementing a pharmacogenomics program requires a vision of the benefits and values, identifying appropriate targets for change that are accepting and excited for the change, communication and education of all impacted stakeholders (including patients), and the ability to measure results and provide feedback. During this session, Donald Levick details Lehigh Valley Health Network's participation in a pilot program through the ONC - <em>The Sync for Genes Phase 2</em>.</p>

											<hr>

											<p class="header-micro">Canada</p>
											<p class="no-margin--bottom bold">PrescribeIT: Canada's Nationwide e-Prescribing Network</p>
											<p class="italic no-margin--bottom">Mary St. Pierre, Supervisor, Ordering/e-Prescribing Implementation, MEDITECH</p>
											<p>e-Prescribing stands to streamline and improve communication between prescribers and retail pharmacies in many ways. Attend this session to learn more about the e-Prescribing features incorporated into Expanse.</p>

										</div>
									</li>
								</ul>
							</div>

						</div>
					</div>
					<div class="accordion__dropdown__container">
						<div class="accordion__dropdown__container__left">
							<p class="bold">2 p.m.</p>
						</div>
						<div class="accordion__dropdown__container__right">
							<p class="bold">Break</p>
						</div>
					</div>
					<div id="Session-Halamka" class="accordion__dropdown__container">
						<div class="accordion__dropdown__container__left">
							<p class="bold">2:15 p.m.</p>
						</div>
						<div class="accordion__dropdown__container__right">
							<p class="bold no-margin--bottom">Transforming Health Care From Within Health Care</p>
							<p class="italic no-margin--bottom"><a href="<?php print $url; ?>/events/meditech-live-22#John-Halamka">John Halamka, MD, MS, President of Mayo Clinic Platform</a></p>
							<p>True transformation of the health care sector requires us to move from pipeline thinking to a platform approach. To do this, we must capture diverse data sources at scale, constantly derive new insights from that data, and create closed-loop solutions in a highly repeatable model. This presentation will focus on the work of Mayo Clinic Platform to create a model that protects both data and intellectual property as well as the partnerships required to enable the greatest impact.</p>
						</div>
					</div>
				</fieldset>
			</div>

			<div id="Day3" class="container auto-margins" style="padding: 1.5em 0 0 0;">
				<fieldset>
					<legend>Day 3 &nbsp;|&nbsp; Thursday, September 22</legend>
					<div class="accordion__dropdown__container">
						<div class="accordion__dropdown__container__left" style="margin-top:0;">
							<p class="bold">7 a.m.</p>
						</div>
						<div class="accordion__dropdown__container__right" style="margin-top:0;">
							<p class="bold no-margin--bottom">Breakfast</p>
						</div>
					</div>
					<div class="accordion__dropdown__container">
						<div class="accordion__dropdown__container__left">
							<p class="bold">8 a.m.</p>
						</div>
						<div class="accordion__dropdown__container__right">
							<p class="bold no-margin--bottom">Becoming a Healthcare Data Platform Through Interoperability</p>
							<p class="italic">Mike Cordeiro, Senior Director, Interoperability Market &amp; Product Strategy, MEDITECH</p>
						</div>
					</div>
					<div class="accordion__dropdown__container">
						<div class="accordion__dropdown__container__left">
							<p class="bold">8:30 a.m.</p>
						</div>
						<div class="accordion__dropdown__container__right">
							<p class="bold no-margin--bottom">Update from the Office of the National Coordinator for Health IT</p>
							<p class="italic no-margin--bottom"><a href="<?php print $url; ?>/events/meditech-live-22#Micky-Tripathi">Micky Tripathi, PhD, MPP, National Coordinator for Health IT, US Department of Health and Human Services</a></p>
							<p>National Coordinator for Health Information Technology Micky Tripathi, PhD, MPP, will provide updates on current ONC priorities, including implementing the Cures Act information sharing provisions. He will also discuss the importance of launching the Trusted Exchange Framework and Common Agreement, improving health equity, and aligning health IT-related activities across HHS in support of the Department's health IT and interoperability goals.</p>
						</div>
					</div>
					<div class="accordion__dropdown__container">
						<div class="accordion__dropdown__container__left">
							<p class="bold">9:30 a.m.</p>
						</div>
						<div class="accordion__dropdown__container__right">
							<p class="bold">Break</p>
						</div>
					</div>
					<div class="accordion__dropdown__container">
						<div class="accordion__dropdown__container__left">
							<p class="bold">9:45 a.m.</p>
						</div>
						<div class="accordion__dropdown__container__right">
							<p class="bold no-margin--bottom">The Importance of Having a Native Cloud Solution as Part of Your Digital Strategy</p>
							<p class="italic">Scott Radner, Vice President, Advanced Technology, MEDITECH<br>
								<a href="<?php print $url; ?>/events/meditech-live-22#Ryan-Terry">Ryan Terry, Digital Transformation Officer, Managing Director, Healthcare &amp; Life Sciences, Google Cloud</a>
							</p>
						</div>
					</div>
					<div class="accordion__dropdown__container">
						<div class="accordion__dropdown__container__left">
							<p class="bold">10:45 a.m.</p>
						</div>
						<div class="accordion__dropdown__container__right">
							<p class="bold">Break</p>
						</div>
					</div>
					<div class="accordion__dropdown__container">
						<div class="accordion__dropdown__container__left">
							<p class="bold">11 a.m.</p>
						</div>
						<div class="accordion__dropdown__container__right">
							<p class="bold no-margin--bottom">Designing a Secure EHR System</p>
							<p class="italic">Michael Hale, Chief Technology Officer, Information Services, Steward Health Care System<br>
								Nassim Abouzeid, Director, System Technology Technical Support, MEDITECH<br>
								CJ Fay, Manager, System Technology Technical Support, MEDITECH<br>
								Thomas Moriarity, Manager, Information Security, MEDITECH</p>
						</div>
					</div>
					<div class="accordion__dropdown__container">
						<div class="accordion__dropdown__container__left">
							<p class="bold">12 p.m.</p>
						</div>
						<div class="accordion__dropdown__container__right">
							<p class="bold no-margin--bottom">What's the Greenfield Alliance Program?</p>
							<p class="italic">Jeffrey Kimball, Associate Vice President, MEDITECH</p>
						</div>
					</div>
					<div class="accordion__dropdown__container">
						<div class="accordion__dropdown__container__left">
							<p class="bold">12:30 p.m.</p>
						</div>
						<div class="accordion__dropdown__container__right">
							<p class="bold">Lunch</p>
						</div>
					</div>
				</fieldset>
			</div>
		</div>
	</div>
	<!-- END Block 4 -->


	<!-- Block 5 -->
	<div class="bg--purple-gradient">
		<div class="content__callout">
			<div class="content__callout__media">
				<div class="content__callout__image-wrapper">
					<div class="js__video video-iframe">
						<iframe width="560" height="315" src="https://www.youtube.com/embed/93cXWnQcrdA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</div>
				</div>
			</div>
			<div class="content__callout__content">
				<div class="content__callout__body text--white">
					<h2>Get ready to connect, learn, and build new goals...at MEDITECH LIVE.</h2>
				</div>
			</div>
		</div>
	</div>
	<!-- END Block 5 -->


	<!-- START Block 6 -->
	<div id="faq" class="container bg--light-blue">
		<div class="container__centered">
			<h2 class="center" style="margin-bottom: 1.5em;">FAQs</h2>
			<fieldset>
				<legend>When is MEDITECH LIVE?</legend>
				<div class="container no-pad center">
					<div class="container__one-third">
						<p class="bold no-margin--bottom">Tuesday, September 20</p>
						<p class="no-margin--bottom">Conference: 12 p.m. to 5 p.m.</p>
						<p>Cocktail Reception: 5 p.m. - 6 p.m.</p>
					</div>
					<div class="container__one-third">
						<p class="bold no-margin--bottom">Wednesday, September 21</p>
						<p class=" no-margin--bottom">Conference: 8 a.m. to 2 p.m. </p>
						<p>Post-Conference Activities: Start at 3 p.m.</p>
					</div>
					<div class="container__one-third">
						<p class="bold no-margin--bottom">Thursday, September 22 <br>(Technology Day)</p>
						<p>Conference: 8 a.m. to 12 p.m.</p>
					</div>
				</div>
			</fieldset>

			<div class="container__one-half">
				<div class="accordion">
					<ul class="accordion__list">
						<li id="item" class="accordion__list__item">
							<a class="accordion__link" href="#">What is MEDITECH LIVE?<div class="accordion__list__control"></div></a>
							<div class="accordion__dropdown">
								<p>MEDITECH LIVE is a new, cross-disciplinary in-person event that will provide attendees with an immersive learning experience. Attendees will engage in authentic, focused discussions on the most substantive challenges facing the industry so they can gather insight from others and work to establish their own organization's path forward.</p>
								<a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close</a>
							</div>
						</li>
						<li id="item" class="accordion__list__item">
							<a class="accordion__link" href="#">Can I sign up if I am not an executive?<div class="accordion__list__control"></div></a>
							<div class="accordion__dropdown">
								<p>Yes, but please note the format of this event, as there will not be the same level of product discussion as other MEDITECH events.</p>
								<a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close</a>
							</div>
						</li>
						<li id="item" class="accordion__list__item">
							<a class="accordion__link" href="#">How does content differ from other events?<div class="accordion__list__control"></div></a>
							<div class="accordion__dropdown">
								<p>Content for MEDITECH LIVE is more strategic. We will be focusing on industry trends and shared strategies on how to succeed in the new healthcare landscape, as opposed to product optimization.</p>
								<a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close</a>
							</div>
						</li>
						<li id="item" class="accordion__list__item">
							<a class="accordion__link" href="#">Is this replacing the Physician/CIO Forum?<div class="accordion__list__control"></div></a>
							<div class="accordion__dropdown">
								<p>This year we are trying a new approach to customer engagement with leaders across our customer base and holding this leadership event, instead of a Physician/CIO forum. We are excited to engage with an interdisciplinary executive audience to discuss innovation in the rapidly evolving digital health arena and strategies to meet the new demands in healthcare. We will also leverage feedback from this event to help shape our future event strategy.</p>
								<a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close</a>
							</div>
						</li>
						<li id="item" class="accordion__list__item">
							<a class="accordion__link" href="#">What is the deadline for registration?<div class="accordion__list__control"></div></a>
							<div class="accordion__dropdown">
								<p>Attendees can register up until the date of the event.</p>
								<a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close</a>
							</div>
						</li>
						<li id="item" class="accordion__list__item">
							<a class="accordion__link" href="#">Since this is an in-person event, what is MEDITECH's COVID policy?<div class="accordion__list__control"></div></a>
							<div class="accordion__dropdown">
								<p>Please see <a href="https://ehr.meditech.com/meditech-covid-19-policies-for-visitors">MEDITECH's COVID policy</a> for visitors.</p>
								<a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close</a>
							</div>
						</li>
					</ul>
				</div>
			</div>

			<div class="container__one-half">
				<div class="accordion">
					<ul class="accordion__list">
						<li id="item" class="accordion__list__item">
							<a class="accordion__link" href="#">Who is invited to MEDITECH LIVE?<div class="accordion__list__control"></div></a>
							<div class="accordion__dropdown">
								<p>MEDITECH LIVE is focused on MEDITECH customers who are health leaders, both current and emerging. This includes C-Level staff, such as CEOs, CIOs, CFOs, CNOs, CMIOs, CTOs, COOs, etc., but also those who serve in key leadership roles or have similar titles and job functions.but may not have C-level titles, such as IT Directors.</p>
								<a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close</a>
							</div>
						</li>
						<li id="item" class="accordion__list__item">
							<a class="accordion__link" href="#">Can I register if my facility is not on Expanse?<div class="accordion__list__control"></div></a>
							<div class="accordion__dropdown">
								<p>Definitely. This will be a great opportunity for you to network and learn from many, including some of our Expanse customers. Topics will also be more focused on high level strategies.</p>
								<a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close</a>
							</div>
						</li>
						<li id="item" class="accordion__list__item">
							<a class="accordion__link" href="#">How does the format differ?<div class="accordion__list__control"></div></a>
							<div class="accordion__dropdown">
								<p>MEDITECH LIVE is less focused on speaker presentations and product discussions and more focused on interactive groups, round tables, and panel discussions. Attendees are not just listening, but actively participating.</p>
								<a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close</a>
							</div>
						</li>
						<li id="item" class="accordion__list__item">
							<a class="accordion__link" href="#">Will there be any big name speakers?<div class="accordion__list__control"></div></a>
							<div class="accordion__dropdown">
								<p>Yes. Featured speakers include:</p>
								<ul>
									<li>Michael Cuffe, MD, Executive Vice President and Chief Clinical Officer, HCA Healthcare</li>
									<li>John Halamka, MD, MS, President, Mayo Clinic Platform</li>
									<li>Vivian Lee, MD, PhD, MBA, President, Verily Health</li>
									<li>Stacy Palmer, Senior Vice President and COO of The Beryl Institute</li>
									<li>Ryan A. Terry, Digital Transformation Officer, Managing Director, Healthcare &amp; Life Sciences, Google Cloud</li>
									<li>Micky Tripathi, PhD, MPP, National Coordinator for Health IT</li>
								</ul>
								<p>Please check back for updates on additional speakers, sessions, and other information.</p>
								<a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close</a>
							</div>
						</li>
						<li id="item" class="accordion__list__item">
							<a class="accordion__link" href="#">I am unable to attend in person. Will the event be recorded and made available?<div class="accordion__list__control"></div></a>
							<div class="accordion__dropdown">
								<p>Given the interactive format of MEDITECH LIVE, it is highly encouraged that you attend the event in person in order to gain the full benefit of the sessions. If you are unable to attend, a select number of sessions will be available at the conclusion of the event.</p>
								<a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close</a>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- END Block 6 -->


	<!-- START Block 7 -->
	<div class="container bg--purple-gradient event-quote">
		<div class="container__centered" style="padding: 0 4em;">
			<div class="container__one-third center">
				<figure>
					<img class="event-headshot" src="<?php print $url; ?>/sites/all/themes/meditech/images/events/Helen-Waters--headshot-alt.png" alt="Helen Waters Headshot">
				</figure>
			</div>
			<div class="container__two-thirds">
				<p class="text--large italic">"MEDITECH LIVE is all about amplifying your voice and your experience, and combining it with the expertise of your peers as well as industry thought leaders. Together, we have a chance to build upon our collective momentum, embrace the opportunities of this next transition in digital heath, and drive healthcare's evolution."</p>
				<p><span class="text--large bold text--meditech-green"> Helen Waters</span><br>
					Executive Vice President &amp; COO, MEDITECH</p>
			</div>
		</div>
	</div>
	<!-- END Block 7 -->


	<!-- Block 8 -->
	<div class="container bg--light-blue" style="padding: 5em 0 3em 0;">
		<div class="container__centered">
			<div class="auto-margins center" style="margin-bottom: 3em;">
				<h2>Attendees are excited for MEDITECH LIVE!</h2>
			</div>
			<div class="container__one-third">
				<div class="shadow-box w-headshot">
					<div class="headshot">
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/gerald-greeley.png" alt="Gerald Greeley, CIO, Lawrence General Hospital head shot">
					</div>
					<p class="italic">"MEDITECH LIVE is a great opportunity to hear from my physician and IT peers, as well as connect with other executives who are shaping strategy at their organizations. There's a lot we can learn from one another."</p>
					<p class="bold no-margin--bottom">Gerald Greeley, CIO</p>
					<p>Lawrence General Hospital</p>
				</div>
			</div>
			<div class="container__one-third">
				<div class="shadow-box w-headshot">
					<div class="headshot">
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/William-Sewell.png" alt="William Sewell, MD, MBA, CMIO head shot">
					</div>
					<p class="italic">"For most healthcare organizations, developing cohesive strategies to optimize AI, increase mobility, improve health equity, extend natural language processing, and promote personalized medicine can be overwhelming. MEDITECH LIVE is a unique opportunity for leaders to tackle these concerns together."</p>
					<p class="bold no-margin--bottom">William Sewell, MD, MBA, Chief Medical Information Officer</p>
					<p>Phoebe Putney Health System</p>
				</div>
			</div>
			<div class="container__one-third">
				<div class="shadow-box w-headshot">
					<div class="headshot">
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/kim-hoover.png" alt="Kim Hoover, RN, BSN, CMSRN, Clinical Informatics Coordinator, Mount Nittany Health head shot">
					</div>
					<p class="italic">“We are always looking for ways to optimize the EHR, improve workflows and satisfaction for our team, and explore the latest innovative technologies. The MEDITECH LIVE event offers the opportunity to gain insights in all of these areas.”</p>
					<p class="bold no-margin--bottom">Kim Hoover, RN, BSN, CMSRN, Clinical Informatics Coordinator</p>
					<p>Mount Nittany Health</p>
				</div>
			</div>
		</div>
	</div>
	<!-- END Block 8 -->


	<!-- Block 9 -->
	<div id="events" class="container container__centered">
		<div class="auto-margins center" style="margin-bottom: 1em;">
			<h2>Coming to MEDITECH LIVE and looking for something fun to do Monday night? Choose from these vendor-sponsored events!</h2>
		</div>
		<div class="container">
			<div class="container__one-half">
				<div class="bg-pattern--container">
					<img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/friends-meeting-at-cocktail-reception.jpg" alt="Friends meeting at a reception">
					<div class="bg-pattern--blue-gradient bg-pattern--left" style="background-image: linear-gradient(-150deg, #e65b25, #cd4699, #af1e4b);"></div>
				</div>
			</div>
			<div class="container__one-half content--pad-left">
				<h3>MEDITECH LIVE Welcome Reception</h3>
				<p><span class="bold">Sponsored by:</span> CloudWave, Forward Advantage, and Experis</p>
				<p><span class="bold">When:</span> Monday, September 19, 6:00 p.m. - 9:30 p.m.</p>
				<p><span class="bold">Where:</span> <a href="https://www.cbssportingclub.com/" target="_blank">CBS Sporting Club</a>, Patriot Place, Foxboro, MA</p>
				<p><span class="bold">Registration:</span> <a href="https://cloud.news.forwardadvantage.com/meditech-live-2022" target="_blank">Click here to register</a> (separate registration required)</p>
				<p><span class="bold">Transportation:</span> Free transportation to/from area hotels through Uber (Uber event code will be provided prior to the event).</p>
				<p><span class="bold">For more information:</span> Robin Gordon, rgordon@gocloudwave.com or 508-251-8906</p>
			</div>
		</div>
		<div class="flex-order--container">
			<div class="container__one-half content--pad-right">
				<h3>MEDITECH LIVE Happy Hour</h3>
				<p><span class="bold">Sponsored by:</span> BridgeHead, CereCore, Google Cloud, Interlace Health, and Pure Storage</p>
				<p><span class="bold">When:</span> Monday, September 19, 6:00 p.m. - 9:30 p.m.</p>
				<p><span class="bold">Where:</span> <a href="https://www.howlsplitsville.com/foxborough-ma/" target="_blank">Howl Splitsville Topgolf Foxborough</a>, Patriot Place, Foxborough, MA</p>
				<p><span class="bold">Registration:</span> <a href="https://cerecore.net/meditechlive22-event-rsvp" target="_blank">Click here to register</a> (separate registration required)</p>
			</div>
			<div class="container__one-half flex-order--reverse">
				<div class="bg-pattern--container">
					<img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/friends-cheers-at-restaurant.jpg" alt="Friends cheers at a restaurant">
					<div class="bg-pattern--green-gradient bg-pattern--right" style="background-image: linear-gradient(-150deg, #00ace4, #1dbbb5, #12dd81);"></div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="container__one-half">
				<div class="bg-pattern--container">
					<img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/boston-harbor-skyline-early-evening.jpg" alt="Friends meeting at a reception">
					<div class="bg-pattern--blue-gradient bg-pattern--left" style="background-image: linear-gradient(-150deg, #e65b25, #CD6546, #DE883F);"></div>
				</div>
			</div>
			<div class="container__one-half content--pad-left">
				<h3>Let's go cruisin'!</h3>
				<p>Join us as we take in the picturesque skyline during a Boston Harbor sunset cruise on <strong>Wednesday, September 21st</strong>, from <strong>5:00 p.m. - 8:00 p.m</strong>. Plenty of great food and drinks will be served and bus transportation will be provided from Foxborough and back.</p>
				<p>Please complete <a href="https://docs.google.com/forms/d/e/1FAIpQLSeyE77xhiKXpoOVWzesrZ3TMqvgPXBPJwHrq0AObs49BKiXMQ/viewform?usp=sf_link">this form</a> to let us know if you would be interested in attending the cruise. We will send you an email once you're confirmed to set sail!</p>
				<p><em><strong>Note:</strong> Space is limited and this form is for informational purposes only – it is not a registration. We will send an email confirming your spot on the boat.</em></p>
			</div>
		</div>
		<div class="container no-pad auto-margins center">
			<h4>Many thanks to our cruise sponsors:</h4>
			<?php
        $vendor_ids = array(
          array('ACS MediHealth', '1157'),
          array('Forward Advantage', '1082'),
          array('Medisolv', '1181'),
          array('SIS', '3439')
        );
        print '<div class="container" style="padding: 1em 0;">';
        foreach($vendor_ids as $vID){ // loop through vendors
          print '<div class="container__one-fourth" style="display: flex; align-items: center; height: 100px;">';
          print views_embed_view('vendor_logos', 'block', $vID[1]); // 2nd item in each array
          print '</div>';
        }
        print '</div>';
      ?>
			<div class="container no-pad center">
				<p>And to our bus transportation sponsor:</p>
				<p><img typeof="foaf:Image" src="https://ehr.meditech.com/sites/default/files/vendors/logo--interbit.jpg" width="320" height="80" alt="Interbit" style="height: 40px; width: auto;"></p>
			</div>
		</div>
	</div>
	<!-- END Block 9 -->


	<!-- Block 10 -->
	<div class="container bg--purple-gradient event-cta">
		<div class="bounding-box">
			<div class="rotate-svg inner-arches">
				<img src="<?php print $websiteURL; ?>/sites/all/themes/meditech/images/events/arch-shapes--inner-dark.svg" alt="Inner arch shapes">
			</div>
			<div class="rotate-svg middle-arches">
				<img src="<?php print $websiteURL; ?>/sites/all/themes/meditech/images/events/arch-shapes--middle.svg" alt="Middle arch shapes">
			</div>
			<div class="rotate-svg outer-arches">
				<img src="<?php print $websiteURL; ?>/sites/all/themes/meditech/images/events/arch-shapes--outer-dark.svg" alt="Outer arch shapes">
			</div>
		</div>
		<div class="container__centered shadow-box glass-effect">
			<div class="container__two-thirds">
				<h2>Be different. Be distinguished. Be a success story.</h2>
				<div>
					<p>Join the EHR vendor that was <a href="<?php print $websiteURL; ?>/about/meditech-awards">ranked #1 by KLAS in three key categories</a>. Together, we will resolve to be transformative changemakers in healthcare's ever-changing world.</p>
					<p>Register for MEDITECH LIVE today. More details on the agenda and speakers coming soon.</p>
				</div>
			</div>
			<div class="container__one-third center" style="padding-top:2em;">
				<div style="margin-bottom: 1.5em;">
					<?php hubspot_button('aedffd1d-31db-4186-960c-e01a048c9b66', "Register For MEDITECH LIVE"); ?>
				</div>
				<div>
					<?php print $share_link_buttons; ?>
				</div>
			</div>
		</div>
	</div>
	<!-- END Block 10 -->

</div>
<!-- end js__seo-tool__body-content -->

<!-- start events-simple-page--node-4173.php -->
