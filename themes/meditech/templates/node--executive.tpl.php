<!-- start node--executive.tpl.php template -->

<?php // This template is set up to control the display of the 'executive' individual pages... 

  $url = $GLOBALS['base_url']; // grabs the site url
?>

<?php 
if( !empty($content['field_image']) ){ 
  // get image data...
  $image_data = field_get_items('node', $node, 'field_image');
  // get image uri...
  $image_uri = $image_data[0]['uri']; // grab image URI
  // remove "public://" portion at the beginning...
  $image_filename = str_replace('public://', '', $image_uri);
}  
?>

<style>
	.exec-banner {
		padding: 4em 0 0 0;
		margin-bottom: 9.5em;
		height: 14em;
	}

	.exec-headshot {
		border-radius: 7px;
		height: 350px;
		background-size: cover;
		background-position: center;
		background-repeat: no-repeat;
	}

	.exec-titles {
		margin-top: 3em;
	}

	.exec-body {
		margin-top: -10em;
	}

	.flex-order--container {
		display: block;
	}

	@media all and (max-width: 50em) {
		.exec-banner {
			padding: 5em 0;
			height: 18em;
		}

		.exec-headshot {
			width: 350px;
			margin: 0 auto;
		}

		.exec-titles {
			color: #3e4545;
			margin: 2em 0 0 0;
		}

		.exec-body {
			margin-top: 0;
		}

		.flex-order--container {
			display: flex;
			padding-top: 0;
		}
	}

	@media all and (max-width: 450px) {
		.exec-headshot {
			width: 100%;
		}
	}

</style>

<!-- Start Banner Section -->
<div class="bg--purple-gradient exec-banner">
	<div class="container__centered">

		<div class="container__one-third">
			<figure>
				<a href="<?php print $fields['path']->content; ?>">
					<div class="exec-headshot" style="background-image: url(<?php print $url; ?>/sites/default/files/<?php print $image_filename; ?>);">&nbsp;</div>
				</a>
			</figure>
		</div>

		<div class="container__two-thirds">
			<div class="exec-titles text--white">
				<h1 class="js__seo-tool__title" style="margin-bottom:0;"><?php print $title; ?></h1>
				<h2 class="header-three" style="font-weight: normal;"><?php print render($content['field_job_title']); ?></h2>
			</div>
		</div>

	</div>
</div>
<!-- End Banner Section -->

<section class="container__centered flex-order--container">

	<!-- Start About Side Nav -->
	<aside class="container__one-third panel">
		<div class="sidebar__nav">
			<?php
        $massnBlock = module_invoke('menu', 'block_view', 'menu-about-section-side-nav');
        print render($massnBlock['content']); 
      ?>
		</div>
	</aside>
	<!-- End About Side Nav -->

	<!-- Start Executive Bio -->
	<div class="container__two-thirds flex-order--reverse">
		<div class="exec-body js__seo-tool__body-content">
			<?php print render($content['field_body']); ?>
		</div>

		<?php // SEO tool for internal use...
      if(node_access('update',$node)){
        print '<!-- SEO Tool is added to this div -->';
        print '<div class="container js__seo-tool"></div>';
      } 
    ?>
	</div>
	<!-- End Executive Bio -->

</section>
<!-- end node--executive.tpl.php template -->