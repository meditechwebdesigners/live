<!-- start simple-page--node-4177.tpl.php template -- Search Results page -->

<?php
// get Action from URL...
if( isset($_GET['action']) ){
  $action = $_GET['action'];
}
else{
 $action = '';
}

// get page URL...
$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
// break up URL by equal signs...
$currentURLarray = explode('=', $currentURL);

if($action == 'pnf'){
  // PAGE NOT FOUND version...
  // break up segment by ampersand...
  $arrayendOfURL = count($currentURLarray) -2;
  $search_variables = explode('&', $currentURLarray[$arrayendOfURL]);
  // isolate the search term...
  $search_term = $search_variables[0];
}
else{
  // SEARCH PAGE version...
  // break up segment by ampersand...
  $arrayendOfURL = count($currentURLarray) -1;
  // isolate the search term...
  $search_term = $currentURLarray[$arrayendOfURL];
}

$url = $GLOBALS['base_url']; // grabs the site url
?>


<style>
    .gsc-control-cse,
    .gsc-control-cse .gsc-table-result {
        margin: 0;
        padding: 0;
        font-size: 1.125em;
        font-family: inherit;
        font-weight: normal;
        line-height: 1.6875em;
    }

    .gsc-above-wrapper-area-container {
        margin: 0;
    }

    .gsc-above-wrapper-area,
    .gsc-result-info-container,
    .gsc-orderby-container,
    .gsc-result-info {
        padding: 0;
    }

    .gsc-result-info,
    .gsc-orderby-label.gsc-inline-block,
    .gs-bidi-start-align.gs-visibleUrl.gs-visibleUrl-short,
    .gs-bidi-start-align.gs-visibleUrl.gs-visibleUrl-long {
        font-size: .8em;
        line-height: 1.25em;
    }

    .gsc-selected-option-container {
        max-width: 100%;
        font-size: .8em;
    }

    .gsc-option-menu-item {
        padding: 0 15px;
    }

    .gsc-adBlock,
    .gcsc-branding,
    .gsc-resultsHeader {
        display: none;
    }

    .gsc-above-wrapper-area {
        border-bottom: 0;
    }

    .gsc-above-wrapper-area-container {
        border: 0;
    }

    .gsc-results {
        width: 100%;
    }

    .gsc-thumbnail-inside,
    .gsc-url-top {
        padding-left: 0;
        padding-right: 0;
    }

    .gs-bidi-start-align.gs-snippet {
        font-size: .8em;
        line-height: 1.5em;
    }

    table,
    tbody,
    tr,
    td {
        background-color: #fff;
        border: 0;
    }

    tr:hover>td,
    tr:hover>th {
        background: #fff;
    }

    .gsc-result .gs-title {
        height: auto;
    }

    .gsc-control-cse .gs-result .gs-title,
    .gsc-control-cse .gs-result .gs-title * {
        font-family: "montserrat", Verdana, sans-serif;
        font-size: 1em;
    }

    .gs-result .gs-title,
    .gs-result .gs-title *,
    .gs-webResult.gs-result a.gs-title:link,
    .gs-webResult.gs-result a.gs-title:link b,
    .gs-webResult.gs-result a.gs-title:visited,
    .gs-webResult.gs-result a.gs-title:visited b,
    .gs-imageResult a.gs-title:visited,
    .gs-imageResult a.gs-title:visited b {
        color: #109372;
    }

    .gs-webResult.gs-result a.gs-title:hover,
    .gs-webResult.gs-result a.gs-title:hover b,
    .gs-webResult div.gs-visibleUrl {
        color: #3e4545;
    }

    .gs-webResult div.gs-visibleUrl {
        font-style: italic;
    }

    .gs-result .gs-title,
    .gs-result .gs-title * {
        text-decoration: none;
    }

    .gsc-table-result {
        margin: .5em 0;
    }

    .gsc-cursor-box.gs-bidi-start-align {
        margin: 10px 0;
    }

    .gsc-results .gsc-cursor-box .gsc-cursor-page {
        color: #087E68;
        margin-right: 1em;
    }

    .gsc-results .gsc-cursor-box .gsc-cursor-page.gsc-cursor-current-page,
    .gsc-results .gsc-cursor-box .gsc-cursor-page:hover {
        color: #3e4545;
    }

    .gs-fileFormat {
        display: none;
    }

</style>


<div class="container__centered" style="margin-top:2em; margin-bottom:2em;">

    <div class="container__two-thirds">

        <?php 
        if($action == 'pnf'){ // "page not found" results
          print '<div>';
            print '<div class="container__two-thirds">';
              print '<h1>Oops, it looks like something is wrong.</h1>';
              print '<p>We hate extra taps and clicks too, but does one of the links below seem close to what you were looking for?</p>';
            print '</div>';
            print '<div class="container__one-third center">';
              print '<img src="'.$url.'/sites/all/themes/meditech/images/404-hands-grey.png" />';
            print '</div>';
          print '</div>';
        }
        else{
          print '<h1>'.$title.'</h1>';
        }
        ?>

        <div class="search-results">

            <script>
                (function() {
                    var cx = '004459460329414904570:jebrei6tb4w';
                    var gcse = document.createElement('script');
                    gcse.type = 'text/javascript';
                    gcse.async = true;
                    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(gcse, s);
                })();

            </script>
            <gcse:searchresults-only></gcse:searchresults-only>

        </div>

    </div>

    <aside class="container__one-third panel">
        <div class="sidebar__nav">
            <h4>Looking for customer-specific content? <br><br>Please <a href="https://home.meditech.com/en/d/newsroom/pages/searchresults.htm?as_q=<?php print $search_term; ?>"> search Customer Service</a>.</h4>
        </div>
    </aside>

</div>
<!-- end simple-page--node-4177.tpl.php template -->
