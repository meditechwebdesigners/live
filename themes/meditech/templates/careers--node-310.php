<?php $url = $GLOBALS['base_url']; // grabs the site url ?>
<!-- START careers--node-310.php template LIFE AT MEDITECH page -->
    <section class="container__centered">
      <div class="container__two-thirds">

        <h1 class="page__title js__seo-tool__title"><?php print $title; ?></h1>

        <div class="js__seo-tool__body-content">
          <?php print render($content['field_body']); ?>
        </div>

          <script type="text/javascript" src="<?php print $url; ?>/sites/all/themes/meditech/js/instafeed.min.js"></script>
          <script type="text/javascript">
            // source: http://instafeedjs.com
            var feed = new Instafeed({
              get: 'tagged',
              tagName: 'meditech45',
              clientId: 'c8d0a953b0a34ad595d366dac5485485',
              limit: 9,
              resolution: 'low_resolution',
              template: '<a href="{{link}}"><img src="{{image}}" style="width:33%;" /></a>'
            });
            feed.run();
          </script>
          <div id="instafeed"></div>
      </div><!-- END container__two-thirds -->
      
      <!-- CAREERS SIDEBAR -->
      <aside class="container__one-third panel">
	      <div class="sidebar__nav careers_sidebar_gae">
          <?php
            $massnBlock = module_invoke('menu', 'block_view', 'menu-careers-section-side-nav');
            print render($massnBlock['content']); 
          ?>
        </div>
      </aside>
      <!-- END CAREERS SIDEBAR -->
    
    </section>
    <!-- END SECTION container__centered -->
    
    <?php // VIDEO SECTION =================================================================
      // get value from field_video to pass to View (if video exists)...
      $video = render($content['field_video']);
      if(!empty($video)){ // if the page has a video...
        print '<!-- VIDEO -->';
        print '<div class="js__seo-tool__body-content">';
        // remove apostrophes from titles to prevent View from breaking...
        $video_filtered = str_replace("&#039;", "'", $video);
        // adds 'video' Views block...
        print views_embed_view('video_boxed', 'block', $video_filtered);
        print '</div>';
        print '<!-- END VIDEO -->';
      }
    ?>

    <?php // SEO tool for internal use...
      if(node_access('update',$node)){
        print '<!-- SEO Tool is added to this div -->';
        print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
      } 
    ?> 
<!-- END careers--node-310.php template -->