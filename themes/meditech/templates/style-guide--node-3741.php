<?php if( user_is_logged_in() ){ ?>
<!-- START style-guide--node-3741.php GIFs & Videos  -->

<!--Javascript & CSS reference for Prism-->
<script type="text/javascript" src="<?php print $url; ?>/sites/all/themes/meditech/js/prism.js"></script>

<link href="<?php print $url; ?>/sites/all/themes/meditech/css/prism.css" rel="stylesheet" />

<style>
    video {
        max-width: 100%;
    }

    .demo {
        border-radius: 0.389em;
        box-shadow: 0px 0px 20px 1px rgb(120 120 120 / 20%);
    }

</style>
<section class="container__centered">

    <h1 class="page__title">
        <?php print $title; ?>
    </h1>

    <div class="container__two-thirds">

        <h2>Exporting GIFs &amp; Videos</h2>
        <video controls muted playsinline class="demo">
            <source src="<?php print $url; ?>/sites/all/themes/meditech/videos/styleguide/Exporting-Video-GIF--tutorial.mp4" type="video/mp4">
        </video>
        <p class="italic"><strong>Note:</strong> This is tailored for After Effects but the principal is the same for Premiere. The difference is to export with Media Encoder, you click queue (in the render menu Shortcut: Crtl + M) instead of export to bring into Media Encoder. But you can export with either Premiere or Media Encoder with the same H.264 settings.</p>

        <h4>Step 1:</h4>
        <ul>
            <li>When ready to export, make sure your current composition is selected and go to "<i>Composition</i>" then to "<i>Add to Adobe Media Encoder Queue</i>".</li>
            <li><strong>Tip:</strong> Keyboard Shortcut: Crtl + Alt + M</li>
        </ul>
        <h4>Step 2:</h4>
        <ul>
            <li>It may take a few minutes for both Adobe Media Encoder to open and for it to find your requested export. <strong>Be patient</strong>.</li>
            <li>Once it does, make sure the "<i>H.264</i>" format is selected. </li>
            <li>From there update "<i>Output Name</i>" and location.</li>
            <li>Uncheck "<i>Export Audio</i>" if audio is not needed.</li>
            <li>Check "<i>Use maximum Render Quality</i>"</li>
            <li>Lastly, click "<i>OK</i>".</li>
        </ul>
        <h4>Step 3:</h4>
        <ul>
            <li>Check new file size. If GIF is not needed, you are done.</li>
            <li>If GIF is needed open your browser and go to <a href="https://ezgif.com/video-to-gif">ezgif.com/video-to-gif</a></li>
        </ul>
        <h4>Step 4:</h4>
        <ul>
            <li>Upload video file. If above steps are followed it should easily be under the 100MB max file size.</li>
            <li>Once uploaded make sure the "<i>size</i>" is set to original and "<i>frame rate</i>" is set appropriately.</li>
            <li>Click "<i>Convert to GIF!</i>"</li>
        </ul>
        <h4>Step 5:</h4>
        <ul>
            <li>If file size is small enough, click "<i>save</i>". If not click "<i>optmizie the GIF</i>".</li>
        </ul>
        <h4>Step 6:</h4>
        <ul>
            <li>Majority of the time, the default "<i>Optimization method</i>"of "<i>Lossy GIF</i>" is best but play around the options if you don't like the results. And always feel free to reach out to Charles with questions.</li>
            <li>The "<i>Compression level</i>" will entirely depend on your file but I suggest starting low, around 30 and work your way up till you're happy with both the file size and quality.</li>
            <li>Lastly, click "<i>save</i>" to download new compressed GIF.</li>
        </ul>





    </div>

    <!-- SIDEBAR -->
    <aside class="container__one-third">

        <div class="sidebar__nav panel">
            <?php
        $styleBlock = module_invoke('menu', 'block_view', 'menu-style-guide-side-nav');
        print render($styleBlock['content']); 
        ?>
        </div>

    </aside>
    <!-- END SIDEBAR -->

</section>

<!-- END style-guide--node-2858.php Accordion -->
<?php } ?>
