<!-- START campaign--node-3584.php -->
<?php // Genomics campaign

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');
$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>


<style>
    .headshot-container {
        display: flex;
        align-items: center;
        margin-top: 1.5em;
    }

    .headshot {
        max-width: 100px;
        max-height: 100px;
        flex-grow: 1;
        margin-right: 1.5em;
    }

    .headshot img {
        border-radius: 50%;
    }

    .shadow-box.w-headshot-outline-b7 {
        padding: 4em 2em 2em 2em;
        position: relative;
        margin-top: 50px;
        box-shadow: none;
        border: 1px solid #c8ced9;
        background-color: transparent;
    }

    .shadow-box.outline {
        box-shadow: none;
        border: 1px solid #c8ced9;
        background-color: transparent;
    }

    .headshot-b7 {
        position: absolute;
        top: -50px;
        left: 50%;
        transform: translate(-50%);
    }

    .headshot-b7 img {
        width: 100px;
        height: 100px;
        border-radius: 50%;
    }

    .floating-container ul li {
        margin-bottom: 0.5em;
    }

    .numbered-circle {
        font-family: 'source-sans-pro', Helvetica, Arial, sans-serif;
        border-radius: 50%;
        width: 1.3em;
        height: 1.3em;
        color: #fff;
        box-shadow: 0px 0px 20px 1px rgb(120 120 120 / 20%);
        margin: auto;
        display: flex;
        justify-content: center;
        align-items: center;
        border-radius: 50%;
        font-size: 2.5em;
        font-weight: 600;
    }

    .circle-4-6 {
        margin-top: 1em;
    }

    .circle-2 {
        margin-top: 4em;
    }

    .floating-container {
        padding: 4em 0;
        position: relative;
    }

    .floating {
        position: absolute;
        top: 10px;
        width: 100%;
    }

    .floating img {
        height: 1250px;
    }

    .card__info {
        padding: 1em 2em;
    }

    @media all and (max-width: 58.75em) {

        .one-half--tablet {
            width: 48%
        }
    }

    @media all and (max-width: 50em) {

        /* 800px */
        .floating-container {
            padding: 2em 0;
            position: inherit;
        }

        .floating {
            position: inherit;
            top: auto;
            width: 100%;
        }

        .floating img {
            max-width: 300px;
            position: relative;
            top: 0;
            left: 0;
        }

        .one-half--tablet {
            width: inherit;
        }
    }

    .content__callout__content {
        padding-left: 6%;
        padding-right: 6%;
        padding-bottom: inherit;
    }


    @media all and (max-width: 50em) {
        .circle-4-6 {
            margin-top: inherit;
        }

        .circle-2 {
            margin-top: inherit;
        }
    }

</style>

<h1 style="display:none;" class="js__seo-tool__title">MEDITECH Genomics</h1>

<div class="js__seo-tool__body-content">

    <!-- Block 1 -->
    <div class="content__callout background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/bg--purple-DNA-illustration.png); background-position:right center; padding: 2em 0;">
        <div class="content__callout__content" style="background:none; padding-right: 0;">
            <div class="content__callout__body">
                <div class="content__callout__body__text text--white text-shadow--black">
                    <h2 class="header-one">Expanse Genomics</h2>
                    <h2>Precision Medicine for Every Community</h2>
                    <p>For health systems of all sizes, the path to precision medicine is here. Our integrated, end-to-end solution gives providers access to patients’ genetic data — and the clinical decision support tools they need to apply it.</p>
                    <p>With Expanse Genomics, physicians can now order genetic tests just like any other test, receive and store genetic results in their patient’s chart, display results in meaningful and actionable ways, and perform pharmacogenomic drug-gene checking. Most importantly, providers receive the interpretation and guidance they need to steer their patients toward the most effective treatment for their specific genetics profiles.</p>
                    <div style="margin-top:1.5em;">
                        <?php hubspot_button($cta_code, "Genomics Demo"); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="content__callout__media">
            <div class="content__callout__image-wrapper">
                <div class="video js__video" data-video-id="509763548">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--expanse-genomics-2.jpg" alt=" - Video Covershot">
                    </figure>
                    <a class="video__play-btn" href="https://vimeo.com/509763548?&amp;autoplay=1"></a>
                    <div class="video__container"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Block 1 -->


    <!-- Block 2 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/background--genomics-dna-mapped-3.png); background-position:top;">
        <div class="container__centered">

            <div class="card__wrapper">

                <div class="container__one-third card">
                    <figure>
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/card-image--medical-professional-using-tablet-while-walking.jpg" alt="Medical Professional using tablet while while walking">
                    </figure>
                    <div class="card__info">
                        <h3>Empower physicians</h3>
                        <h4>with genetic data at their fingertips</h4>
                        <p>Work smarter, not harder. Expanse Genomics imports test results from genetic labs and parses them into discrete data displayed in an uncomplicated, intuitive chart. What’s more, the solution provides clinical decision support and guidance where physicians need it most — at the point of care.</p>
                    </div>
                </div>

                <div class="container__one-third card">
                    <figure>
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/card-image--impromptu-meeting-in-hospital-hallway.jpg" alt="Impromptu meeting between medical professionals and a businesswoman in hospital hallway">
                    </figure>
                    <div class="card__info">
                        <h3>Empower organizations</h3>
                        <h4> with care at the cutting edge</h4>
                        <p>Now, organizations of all sizes and budgets can add precision medicine to their continuum of care. Early adoption of this emerging field keeps your organization ahead of the curve, creates new lines of revenue, and empowers your clinicians to provide the best care possible.</p>
                    </div>
                </div>

                <div class="container__one-third card">
                    <figure>
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/card-image--masked-mother-and-daughter-walking-along-paved-trail.jpg" alt="masked mother and daughter walking along paved trail in park">
                    </figure>
                    <div class="card__info">
                        <h3>Empower patients</h3>
                        <h4> with personalized care, right next door</h4>
                        <p>Give your patients access to the latest technologies, in the convenience of their own communities — with providers they know and trust.</p>
                        <p>Precision medicine enables providers to tailor care plans to individual genetic profiles, which can help your patients pay less for more accurate treatments.</p>
                    </div>
                </div>

            </div>

        </div>
    </div>
    <!-- End of Block 2 -->


    <!-- Block 3 -->
    <div class="container bg--purple-gradient">
        <div class="container__centered">

            <div class="container__one-third">
                <div class="shadow-box w-headshot-outline-b7">
                    <div class="headshot-b7">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/william-dailey.png" alt="Dr. William Dailey head shot">
                    </div>
                    <p class="italic">“The solution is going to expand our possibilities for delivering precision medicine at our organization and enhance our connectivity with commercial labs. It’s also going to elevate our ability to analyze and parse results into discrete, actionable data displayed directly in the patient charts.”</p>
                    <p><span class="bold no-margin--bottom text--meditech-green">
                            Dr. William Dailey, CMIO</span><br>
                        Golden Valley Memorial Hospital</p>
                </div>
            </div>

            <div class="container__one-third">
                <div class="shadow-box w-headshot-outline-b7">
                    <div class="headshot-b7">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/jackie-rice.png" alt="Jackie Rice headshot">
                    </div>
                    <p class="italic">“MEDITECH’s genomics solution is helping Frederick Health extend the use of genomics beyond the specialties that have traditionally used it, like oncology, cardiology, neurology, and maternal and fetal medicine. We’re now able to put the power of genomic medicine into the hands of physicians across all specialties and areas of medicine, including primary care and internal medicine.”</p>
                    <p><span class="bold no-margin--bottom text--meditech-green">Jackie Rice, CIO</span><br>
                        Frederick Health</p>
                </div>
            </div>

            <div class="container__one-third">
                <div class="shadow-box w-headshot-outline-b7">
                    <div class="headshot-b7">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/Anna-Dover.png" alt="Anna Dover head shot">
                    </div>
                    <p class="italic">“What MEDITECH and FDB have done is develop a way to translate genetic information into clinical action — by transmitting genetic lab results as discrete data, and then providing clinical decision support within clinical workflow in a way that's easy to interpret and take action.”</p>
                    <p><span class="bold no-margin--bottom text--meditech-green">Anna Dover, PharmD, BCPS</span><br>
                        Director Product Management<br>
                        First Databank</p>
                </div>
            </div>



        </div>
    </div>
    <!-- End of Block 3 -->


    <!-- Block 4 -->
    <div class="container bg--white no-pad--bottom">
        <div class="container__centered auto-margins">
            <h2>The entire genomics workflow inside your EHR</h2>
            <p>To access genetic data in most EHRs, physicians launch separate applications, search PDF files, copy and paste data back into the patient record, and consult with genetic specialists — all before making their clinical decisions. No wonder they find accessing this information so frustrating and inefficient.</p>
            <p>Expanse Genomics allows physicians to perform all aspects of their genetic workflow within the EHR. When needed, it isolates the relevant genetic data and provides clinical decision support, with clear, concise, and actionable guidance that physicians can easily interpret. </p>
        </div>
    </div>
    <!-- Close Block 4 -->


    <!-- Block 5 -->
    <div class="container floating-container">
        <div class="container__centered">

            <div class="container no-pad">
                <div class="container__one-third one-half--tablet">
                    <div style="padding-bottom:3em;">
                        <p class="numbered-circle" style="background-color: #00BC6F;">1</p>
                        <h3 class="center">Physician orders genetic test in Expanse.</h3>
                        <p class="header-three center" style="color:#32325D;"><strong>No More:</strong></p>
                        <ul>
                            <li>launching separate systems</li>
                            <li>manually entering genetic test orders</li>
                            <li>returning to the EHR to document test orders</li>
                        </ul>
                    </div>
                    <div style="padding-bottom:3em;">
                        <p class="numbered-circle" style="background-color: #32325D;">3</p>
                        <h3 class="center">Reference lab conducts test and returns results.</h3>
                        <p class="header-three center" style="color:#32325D;"><strong>No More:</strong></p>
                        <ul>
                            <li>results returned in flat files (typically PDFs) and encoded as free text</li>
                        </ul>
                    </div>
                    <div class="">
                        <p class="numbered-circle" style="background-color: #087E68;">5</p>
                        <h3 class="center">Results are displayed intuitively within patient chart.</h3>
                        <p class="header-three center" style="color:#32325D;"><strong>No More:</strong></p>
                        <ul>
                            <li>searching for relevant results
                            </li>
                            <li>test results displayed in disparate formats based on test vendor and test type</li>
                        </ul>
                    </div>
                </div>

                <div class="container__one-third center hide--tablet">
                    &nbsp;
                </div>


                <div class="container__one-third one-half--tablet circle-2">
                    <div style="padding-bottom:3em;">
                        <p class="numbered-circle" style="background-color: #00BC6F;">2</p>
                        <h3 class="center">Specimen is collected and sent to reference lab.</h3>
                        <p class="header-three center" style="color:#32325D;"><strong>No More:</strong></p>
                        <ul>
                            <li>manually tying specimens to genetic tests ordered</li>
                        </ul>

                    </div>
                    <div class="circle-4-6" style="padding-bottom:3em;">
                        <p class="numbered-circle" style="background-color: #32325D;">4</p>
                        <h3 class="center">Expanse receives and stores genetic data in a codified format along with full PDF reports.</h3>
                        <p class="header-three center" style="color:#32325D;"><strong>No More:</strong></p>
                        <ul>
                            <li>results stored as document links that launch in a separate viewer</li>
                        </ul>
                    </div>
                    <div class="circle-4-6">
                        <p class="numbered-circle" style="background-color: #087E68;">6</p>
                        <h3 class="center">Embedded clinical decision support with pharmocogenomics.</h3>
                        <p class="header-three center" style="color:#32325D;"><strong>No More:</strong></p>
                        <ul>
                            <li>searching through documents for analysis</li>
                            <li>translating genetic results into patient-friendly language</li>
                            <li>trial and error prescribing — drug and dosing recommendations based on patients’ unique genetic profiles</li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
        <div class="floating center hide--tablet">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/illustration--dna-02.svg" alt="Large DNA illustration">
        </div>
    </div>
    <!-- End of Block 5 -->


    <div class="container bg--purple-gradient">
        <div class="container__centered text--white">
            <div class="container__one-third center no-target-icon" style="padding-top:2em;">
                <a href="https://interactive.fdbhealth.com/pharmacogenomics-landing-meditech" target="_blank">
                    <img style="max-width: 100%; max-height: 180px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/logo--fdb--white.svg" alt="First Databank Logo">
                </a>
            </div>
            <div class="container__two-thirds">
                <h2>Empower your providers</h2>
                <h3>with pharmacogenomics at the point of care</h3>
                <p>Through our collaboration with First Databank, Expanse Genomics includes integrated pharmacogenomic decision support at the point of care to guide clinicians with clear and concise recommendations. Referencing an individual’s genetic profile when creating pharmaceutical therapies optimizes drug efficacy while mitigating the risk of adverse reactions or dosing errors.</p>
            </div>

        </div>
    </div>
    <!-- End of Block 6 -->


    <!-- CTA -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/bg--accent-dna-team-illustrations.png); background-position: bottom center;">
        <div class="container__centered center auto-margins">

            <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
            <h2>
                <?php print $cta->field_header_1['und'][0]['value']; ?>
            </h2>
            <?php } ?>

            <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
            <h3>
                <?php print $cta->field_long_text_1['und'][0]['value']; ?>
            </h3>
            <?php } ?>

            <div class="center" style="margin-top:2em;">
                <?php hubspot_button($cta_code, "Sign Up For The Expanse Patient Care Webinar"); ?>
            </div>

            <div style="margin-top:1.5em;">
                <?php print $share_link_buttons; ?>
            </div>

        </div>
    </div>
    <!-- End CTA -->

</div>
<!-- end js__seo-tool__body-content -->


<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>
<!-- End of Campaign Node 3584 -->
