<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>
<body class="<?php print $classes; ?>" style="background-color:#000;">
 
  <div id="page">

    <div id="container" class="clearfix">

      <div id="main" class="column">
        <div id="main-squeeze">

          <!-- Expanse Block -->
          <div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/expanse-starry-winter-sky--maintenance-version.jpg);">
            <div class="container__centered">
             
              <div class="container">
                <img style="padding-left:2em; padding-right:2em;" src="https://ehr.meditech.com/sites/all/themes/meditech/images/MEDITECH-expanse-logo--green-white.svg" alt="MEDITECH Expanse Logo">
              </div>
              <div class="container transparent-overlay center no-pad--top text--white text-shadow--black" style="background-color: rgba(21, 19, 74, 0.5); width: 70%; margin: 0 auto; padding: 1em 2em;">
                <p class="text--large">The MEDITECH marketing website is currently undergoing updates and will be back online shortly. In the meantime, you may still access the following MEDITECH sites:</p>
              </div>

              <div class="container center">
                <a style="margin:1em 2em;" href="https://blog.meditech.com/" class="btn--orange">MEDITECH's Blog</a>
                <a style="margin:1em 2em;" href="https://home.meditech.com/en/d/customer/" class="btn--orange">Customer Service</a>
              </div>

            </div>
          </div>
          <!-- End Expanse Block -->


        </div>
      </div> <!-- /main-squeeze /main -->

    </div> <!-- /container -->

  </div> <!-- /page -->

</body>
</html>