<!-- START careers--node-4170.php -- Get to Know the MEDITECH Staff page -->

<?php
$url = $GLOBALS['base_url']; // grabs the site url
?>

<style>
  .modal-content { 
    max-width: 1050px;
    width: 100%;
    height: unset;
    padding: 3em 2em 3em 2em;
    border-radius: 6px;
    background: #fff;
  }
  .modal-employee {
    width: 200px;
    height: 200px;
    border: 10px solid #e6e9ee;
    border-radius: 50%;
    margin: auto;
    margin-top: 1em;
    background-size: 100%;
  }
  
  /*  Employee Bubbles */
  .eb-1 { background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/people/Jay-Frails.jpg); }
  .eb-2 { background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/people/MaryLuz-Bravo-Axline.jpg); }
  .eb-3 { background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/people/Mike-Dewberry.jpg); }
  .eb-4 { background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/people/Kimberly-Makepeace.jpg); }
  .eb-5 { background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/people/Shweta-Patel.jpg); }
  .eb-6 { background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/people/Justin-Demers.jpg); }
  .eb-7 { background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/people/Patricia-Rios.jpg); }
  .eb-8 { background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/people/Andres-Rivera.jpg); }
  .eb-9 { background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/people/Craig-Raposa.jpg); }
  .eb-10 { background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/people/Stephanie-Boeckers.jpg); }
  .eb-11 { background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/people/Jen-Lindquist.jpg); }
  .eb-12 { background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/people/Jonathan-Hallman.jpg); }

  .employee-bubble {
    width: 250px;
    height: 250px;
    border: 10px solid #e6e9ee;
    border-radius: 50%;
    /*    position: relative;*/
    cursor: pointer;
    box-shadow: inset 0 0 0 0 rgba(0, 188, 111, 0.95);
    -webkit-transition: all 0.3s ease-in-out;
    -moz-transition: all 0.3s ease-in-out;
    -o-transition: all 0.3s ease-in-out;
    -ms-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
    background-position: center top;
  }

  .employee-bubble:hover .eb-info {
    opacity: 1;
    -webkit-transform: scale(1);
    -moz-transform: scale(1);
    -o-transform: scale(1);
    -ms-transform: scale(1);
    transform: scale(1);
  }

  .employee-bubble:hover {
    box-shadow: inset 0 0 0 135px rgba(0, 188, 111, 0.95);
  }

  .employee-card {
    padding-bottom: 1.5em;
  }
  .employee-card div { text-align: left; }

  .employee-card-plus {
    padding-top: 3em;
    padding-bottom: 1.5em;
  }

  .eb-info {
    position: absolute;
    width: 250px;
    height: 250px;
    border-radius: 50%;
    opacity: 0;
    -webkit-transition: all 0.4s ease-in-out;
    -moz-transition: all 0.4s ease-in-out;
    -o-transition: all 0.4s ease-in-out;
    -ms-transition: all 0.4s ease-in-out;
    transition: all 0.4s ease-in-out;
    -webkit-transform: scale(0);
    -moz-transform: scale(0);
    -o-transform: scale(0);
    -ms-transform: scale(0);
    transform: scale(0);
    -webkit-backface-visibility: hidden;
  }

  .eb-info h3 {
    color: #fff;
    position: relative;
    padding: 80px 19px 5px 0;
    text-align: center;
  }

  .eb-info p {
    color: #fff;
    position: relative;
    padding: 0px 19px 0 0;
    text-align: center;
  }

  @media all and (max-width: 50em) {
    .employee-card {
      border-right: none;
      border-bottom: 1px solid #e6e9ee;
      margin-bottom: 1em;
    }

    .employee-card-plus {
      border-right: none;
      border-bottom: 1px solid #e6e9ee;
      padding-top: 0;
      margin-bottom: 1em;
    }

    .employee-bubble {
      margin: auto;
      margin-bottom: 1em;
    }

    .modal-content .container__two-thirds { padding-top: 3em; }
</style>


<!-- Start staff circles -->
<div class="container no-pad--bottom">

  <div class="container__centered auto-margins" style="padding-bottom:2em;">
    <h1 class="page__title js__seo-tool__title"><?php print $title; ?></h1>
    <p>MEDITECH's unique work environment includes an open atmosphere, small company feel, beautiful facilities, and sense of camaraderie which makes the company an inviting and welcoming place to work. We value each of our staff members and are committed to their career success.</p>
    <p>Our staff have an opportunity to make a significant impact on today's healthcare industry and grow with an innovative company. We invite you to meet some of the staff who are contributing to our mission.</p>
  </div>

</div>

<div class="container" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/careers/large-dots-background.png); background-position: top; padding-bottom: 8em;">
 
  <div class="container__centered">

    <div class="container__one-third center">
      
      <!-- Start hidden modal box -->
      <div id="modal1" class="modal">
        <a class="close-modal" href="javascript:void(0)">&times;</a>
        <div class="modal-content">
          <!-- Add modal content here -->
          <div class="gl-container">
            <div class="container__one-third center employee-card">
              <div class="modal-employee eb-1"></div>
              <h3 class="no-margin--bottom">Jay Frails</h3>
              <p class="no-margin--bottom text--dark-blue"><strong>Strategy</strong></p>
              <p>At MEDITECH since <strong>2006</strong></p>
            </div>
            <div class="container__two-thirds" style="text-align:left;">
              <p>Jay applied to MEDITECH for a position in our Service division, but during the interview, the recruiter asked if she'd be interested in Marketing and she decided to take the challenge and step outside of her comfort zone. Jay spent eight years in Marketing before working on certification for our software. While demonstrating our software solutions in Marketing, Jay's nursing background helped her to connect with our end-users and to understand the customers needs. Her latest position has her building Standard Content into our software which is: evidence- and experience-based content to support optimal workflow across the continuum of care.</p>
              <p>Jay's advice to candidates: "Don't be afraid to step out of your comfort zone. Be flexible. Always be willing to go in a different direction. You will find you'll never get bored. Be open to learning new things. It opens you to new adventures. Share your knowledge, don't hoard it. Remember you are part of a team."</p>
            </div>
          </div>
        </div>
      </div>
      <!-- End hidden modal box -->

      <!-- Start modal trigger -->
      <div class="open-modal" data-target="modal1">
        <div class="employee-bubble eb-1">
          <div class="eb-info">
            <h3 class="no-margin--bottom">Jay Frails</h3>
            <p>Learn more</p>
          </div>
        </div>
      </div>
      <!-- End modal trigger -->
      
    </div>
    
    <div class="container__one-third center">
      
      <!-- Start hidden modal box -->
      <div id="modal2" class="modal">
        <a class="close-modal" href="javascript:void(0)">&times;</a>
        <div class="modal-content">
          <!-- Add modal content here -->
          <div class="gl-container">
            <div class="container__one-third center employee-card">
              <div class="modal-employee eb-2"></div>
              <h3 class="no-margin--bottom">MaryLuz Bravo-Axline</h3>
              <p class="no-margin--bottom text--dark-blue"><strong>Marketing</strong></p>
              <p>At MEDITECH since <strong>2007</strong></p>
            </div>
            <div class="container__two-thirds" style="text-align:left;">
              <p>MaryLuz says her pharmacy degree and being a registered pharmacist has helped her form great relationships at MEDITECH. Her background enables her to offer her knowledge of the profession to her peers, who come to her with questions and ideas.</p>
              <p>Her Spanish language skills have also helped her grow MEDITECH's scope. As a marketing solutions specialist she has traveled to Puerto Rico to give her presentations in Spanish and has also assisted Implementation groups in Go-LIVEs at two hospitals there.</p>
            </div>
          </div>
        </div>
      </div>
      <!-- End hidden modal box -->

      <!-- Start modal trigger -->
      <div class="open-modal" data-target="modal2">
        <div class="employee-bubble eb-2">
          <div class="eb-info">
            <h3 class="no-margin--bottom">MaryLuz Bravo-Axline</h3>
            <p>Learn more</p>
          </div>
        </div>
      </div>
      <!-- End modal trigger -->
      
    </div>
    
    <div class="container__one-third center">
      
      <!-- Start hidden modal box -->
      <div id="modal3" class="modal">
        <a class="close-modal" href="javascript:void(0)">&times;</a>
        <div class="modal-content">
          <!-- Add modal content here -->
          <div class="gl-container">
            <div class="container__one-third center employee-card">
              <div class="modal-employee eb-3"></div>
              <h3 class="no-margin--bottom">Mike Dewberry</h3>
              <p class="no-margin--bottom text--dark-blue"><strong>Product Development</strong></p>
              <p>At MEDITECH since <strong>1997</strong></p>
            </div>
            <div class="container__two-thirds" style="text-align:left;">
              <p>"I have been very fortunate that I never felt the need to make a career move because I didn't like what I was doing. Exciting opportunities were available to me and I was able to try something new."</p>
              <p>Mike's career, which started as an implementation specialist, has spanned the Implementation, Sales, and Development divisions. "Don't be afraid to try out new things. The more you learn, the more well-rounded you will be for something else."</p>
            </div>
          </div>
        </div>
      </div>
      <!-- End hidden modal box -->

      <!-- Start modal trigger -->
      <div class="open-modal" data-target="modal3">
        <div class="employee-bubble eb-3">
          <div class="eb-info">
            <h3 class="no-margin--bottom">Mike Dewberry</h3>
            <p>Learn more</p>
          </div>
        </div>
      </div>
      <!-- End modal trigger -->
      
    </div>
    
  </div>
   
  <div class="container__centered" style="padding-bottom:2em;">
    
    <div class="container__one-third center">
      
      <!-- Start hidden modal box -->
      <div id="modal4" class="modal">
        <a class="close-modal" href="javascript:void(0)">&times;</a>
        <div class="modal-content">
          <!-- Add modal content here -->
          <div class="gl-container">
            <div class="container__one-third center employee-card">
              <div class="modal-employee eb-4"></div>
              <h3 class="no-margin--bottom">Kimberly Makepeace</h3>
              <p class="no-margin--bottom text--dark-blue"><strong>Product Development</strong></p>
              <p>At MEDITECH since <strong>1998</strong></p>
            </div>
            <div class="container__two-thirds" style="text-align:left;">
              <p>Kimberly joined the company as a programmer for one of our newest applications and only a handful of customers were going through an implementation.</p>
              <p>She says she enjoyed the unique challenges the new product offered and was able to hone her understanding of the financial side of healthcare. "Look for areas in your own application or the company where there is a gap in expertise," says Kimberly. "This is a great way to make a name for yourself."</p>
            </div>
          </div>
        </div>
      </div>
      <!-- End hidden modal box -->

      <!-- Start modal trigger -->
      <div class="open-modal" data-target="modal4">
        <div class="employee-bubble eb-4">
          <div class="eb-info">
            <h3 class="no-margin--bottom">Kimberly Makepeace</h3>
            <p>Learn more</p>
          </div>
        </div>
      </div>
      <!-- End modal trigger -->
      
    </div>
    
    <div class="container__one-third center">
      
      <!-- Start hidden modal box -->
      <div id="modal5" class="modal">
        <a class="close-modal" href="javascript:void(0)">&times;</a>
        <div class="modal-content">
          <!-- Add modal content here -->
          <div class="gl-container">
            <div class="container__one-third center employee-card">
              <div class="modal-employee eb-5"></div>
              <h3 class="no-margin--bottom">Shweta Patel</h3>
              <p class="no-margin--bottom text--dark-blue"><strong>Advanced Technology</strong></p>
              <p>At MEDITECH since <strong>1996</strong></p>
            </div>
            <div class="container__two-thirds" style="text-align:left;">
              <p>Shweta Patel was introduced to MEDITECH by her friend, a MEDITECH staff member, who spoke highly of our open, friendly environment, opportunities for advancement, and meaningful work. Since she wanted a challenging environment with an opportunity for personal growth and advancement, she applied and began her MEDITECH career as a programmer. Although she had received an electrical engineering degree, she had taken many computer programming classes. This got her interested in coding and software engineering. She loved the sense of accomplishment when a coding job was finished and it worked as expected.</p>
              <p>Shweta credits her hard work, dedication, and efficient and supportable coding for her career advancement at MEDITECH. She says, "It's a great company to work for where your hard work and dedication get recognized and rewarded. You will have a lot of learning opportunities to improve your skills and advance your career here."</p>
            </div>
          </div>
        </div>
      </div>
      <!-- End hidden modal box -->

      <!-- Start modal trigger -->
      <div class="open-modal" data-target="modal5">
        <div class="employee-bubble eb-5">
          <div class="eb-info">
            <h3 class="no-margin--bottom">Shweta Patel</h3>
            <p>Learn more</p>
          </div>
        </div>
      </div>
      <!-- End modal trigger -->
      
    </div>
    
    <div class="container__one-third center">
      
      <!-- Start hidden modal box -->
      <div id="modal6" class="modal">
        <a class="close-modal" href="javascript:void(0)">&times;</a>
        <div class="modal-content">
          <!-- Add modal content here -->
          <div class="gl-container">
            <div class="container__one-third center employee-card">
              <div class="modal-employee eb-6"></div>
              <h3 class="no-margin--bottom">Justin Demers</h3>
              <p class="no-margin--bottom text--dark-blue"><strong>Sales</strong></p>
              <p>At MEDITECH since <strong>2005</strong></p>
            </div>
            <div class="container__two-thirds" style="text-align:left;">
              <p>Justin knew he wanted to work at a company that did great things for people. He knew MEDITECH was a "do good" company that allowed him to work in an industry that is making healthcare delivery more safe, efficient, and cost-effective.</p>
              <p>Justin also wanted the opportunity to travel. While studying English at Stonehill College, he traveled abroad for two semesters. "I have been able to fulfill my passion for travel and find a niche in the healthcare industry. Life is good."</p>
            </div>
          </div>
        </div>
      </div>
      <!-- End hidden modal box -->

      <!-- Start modal trigger -->
      <div class="open-modal" data-target="modal6">
        <div class="employee-bubble eb-6">
          <div class="eb-info">
            <h3 class="no-margin--bottom">Justin Demers</h3>
            <p>Learn more</p>
          </div>
        </div>
      </div>
      <!-- End modal trigger -->
      
    </div>
    
  </div>
   
  <div class="container__centered" style="padding-bottom:2em;">
    
    <div class="container__one-third center">
      
      <!-- Start hidden modal box -->
      <div id="modal7" class="modal">
        <a class="close-modal" href="javascript:void(0)">&times;</a>
        <div class="modal-content">
          <!-- Add modal content here -->
          <div class="gl-container">
            <div class="container__one-third center employee-card">
              <div class="modal-employee eb-7"></div>
              <h3 class="no-margin--bottom">Patricia Rios</h3>
              <p class="no-margin--bottom text--dark-blue"><strong>Client Support</strong></p>
              <p>At MEDITECH since <strong>2005</strong></p>
            </div>
            <div class="container__two-thirds" style="text-align:left;">
              <p>Patricia was very interested to join a company that complimented her two degrees in Information Technology and Business Administration. She knew she wanted to work in management, so she began her career as a specialist in our financial application and worked her way to a supervisor position. As our customer base grows, she increasingly has chances to use her fluency in both English and Spanish to work with a wide range of customers.</p>
              <p>Patricia says of MEDITECH, "We are such a diverse community and we welcome your individualism. Your professional expertise is only part of what we look for in a candidate, you as a whole make up that other part we want to get to know."</p>
            </div>
          </div>
        </div>
      </div>
      <!-- End hidden modal box -->

      <!-- Start modal trigger -->
      <div class="open-modal" data-target="modal7">
        <div class="employee-bubble eb-7">
          <div class="eb-info">
            <h3 class="no-margin--bottom">Patricia Rios</h3>
            <p>Learn more</p>
          </div>
        </div>
      </div>
      <!-- End modal trigger -->
      
    </div>
    
    <div class="container__one-third center">
      
      <!-- Start hidden modal box -->
      <div id="modal8" class="modal">
        <a class="close-modal" href="javascript:void(0)">&times;</a>
        <div class="modal-content">
          <!-- Add modal content here -->
          <div class="gl-container">
            <div class="container__one-third center employee-card">
              <div class="modal-employee eb-8"></div>
              <h3 class="no-margin--bottom">Andres Rivera</h3>
              <p class="no-margin--bottom text--dark-blue"><strong>Product Development</strong></p>
              <p>At MEDITECH since <strong>1995</strong></p>
            </div>
            <div class="container__two-thirds" style="text-align:left;">
              <p>Andres' entire career in the Development division allows him the opportunity to work on cutting edge initiatives such as telehealth. With an original interest in pursuing pre-medicine, Andres is thankful for the opportunity to fulfill his inner desire to help people.</p>
              <p>"I get to be a part of exciting technology and software that directly impacts patient care." Not only does he thrive on his daily work, but he enjoys his work environment as well. "The family-friendly culture that upper management fosters has enabled me to build long-lasting friendships."</p>
            </div>
          </div>
        </div>
      </div>
      <!-- End hidden modal box -->

      <!-- Start modal trigger -->
      <div class="open-modal" data-target="modal8">
        <div class="employee-bubble eb-8">
          <div class="eb-info">
            <h3 class="no-margin--bottom">Andres Rivera</h3>
            <p>Learn more</p>
          </div>
        </div>
      </div>
      <!-- End modal trigger -->
      
    </div>
    
    <div class="container__one-third center">
      
      <!-- Start hidden modal box -->
      <div id="modal9" class="modal">
        <a class="close-modal" href="javascript:void(0)">&times;</a>
        <div class="modal-content">
          <!-- Add modal content here -->
          <div class="gl-container">
            <div class="container__one-third center employee-card">
              <div class="modal-employee eb-9"></div>
              <h3 class="no-margin--bottom">Craig Raposa</h3>
              <p class="no-margin--bottom text--dark-blue"><strong>Regulatory Compliance</strong></p>
              <p>At MEDITECH since <strong>1998</strong></p>
            </div>
            <div class="container__two-thirds" style="text-align:left;">
              <p>Craig knew he wanted to find a company that would allow him to use his technical and analytical skills, but also give him an opportunity to have a variety of projects and challenges.</p>
              <p>He never saw himself as "just a programmer." He likes how MEDITECH offers the ability to work within different departments and divisions throughout the company. "If you do a good job and are well respected, there is a possibility that something could potentially be created specifically for you," he says.</p>
            </div>
          </div>
        </div>
      </div>
      <!-- End hidden modal box -->

      <!-- Start modal trigger -->
      <div class="open-modal" data-target="modal9">
        <div class="employee-bubble eb-9">
          <div class="eb-info">
            <h3 class="no-margin--bottom">Craig Raposa</h3>
            <p>Learn more</p>
          </div>
        </div>
      </div>
      <!-- End modal trigger -->
      
    </div>
    
  </div>
   
  <div class="container__centered" style="padding-bottom:2em;">
    
    <div class="container__one-third center">
      
      <!-- Start hidden modal box -->
      <div id="modal10" class="modal">
        <a class="close-modal" href="javascript:void(0)">&times;</a>
        <div class="modal-content">
          <!-- Add modal content here -->
          <div class="gl-container">
            <div class="container__one-third center employee-card">
              <div class="modal-employee eb-10"></div>
              <h3 class="no-margin--bottom">Stephanie Boeckers</h3>
              <p class="no-margin--bottom text--dark-blue"><strong>Client Services</strong></p>
              <p>At MEDITECH since <strong>2001</strong></p>
            </div>
            <div class="container__two-thirds" style="text-align:left;">
              <p>Stephanie was introduced to the company at a college career fair in Minnesota where she was intrigued by the idea of being part of a growing industry in a company that promoted from within. She began her career in the (then) combined Client Services/Implementation division.</p>
              <p>After taking on a position that included informal new hire training, Stephanie felt that there was a need for dedicated training staff. "It was evident that there was a long term need for dedicated new hire training staff, so I submitted a proposal detailing the need, and benefits, to the company. As a result, the role of Training Coordinator was created. So be persistent and look for opportunities - the possibilities are endless!" says Stephanie.</p>
            </div>
          </div>
        </div>
      </div>
      <!-- End hidden modal box -->

      <!-- Start modal trigger -->
      <div class="open-modal" data-target="modal10">
        <div class="employee-bubble eb-10">
          <div class="eb-info">
            <h3 class="no-margin--bottom">Stephanie Boeckers</h3>
            <p>Learn more</p>
          </div>
        </div>
      </div>
      <!-- End modal trigger -->
      
    </div>
    
    <div class="container__one-third center">
      
      <!-- Start hidden modal box -->
      <div id="modal11" class="modal">
        <a class="close-modal" href="javascript:void(0)">&times;</a>
        <div class="modal-content">
          <!-- Add modal content here -->
          <div class="gl-container">
            <div class="container__one-third center employee-card">
              <div class="modal-employee eb-11"></div>
              <h3 class="no-margin--bottom">Jen Lindquist</h3>
              <p class="no-margin--bottom text--dark-blue"><strong>Product Development</strong></p>
              <p>At MEDITECH since <strong>2004</strong></p>
            </div>
            <div class="container__two-thirds" style="text-align:left;">
              <p>After graduating with an English major and a computer science minor and teaching English in Japan for a year, Jen's first position in Implementation fit her very well. The position combined all of her interests and her education. She moved her way up and is now a manager in Development.</p>
              <p>Jen's favorite thing about MEDITECH is that management takes the time to get to know their staff and advocate for their success. Her supervisor was very supportive of her furthering her career. "They take an interest in employees for the long term, so they take a greater interest in their personal lives." Jen wants you to remember to be yourself. "Whenever you interview, it's a relationship you are building, not just a job."</p>
            </div>
          </div>
        </div>
      </div>
      <!-- End hidden modal box -->

      <!-- Start modal trigger -->
      <div class="open-modal" data-target="modal11">
        <div class="employee-bubble eb-11">
          <div class="eb-info">
            <h3 class="no-margin--bottom">Jen Lindquist</h3>
            <p>Learn more</p>
          </div>
        </div>
      </div>
      <!-- End modal trigger -->
      
    </div>
    
    <div class="container__one-third center">
      
      <!-- Start hidden modal box -->
      <div id="modal12" class="modal">
        <a class="close-modal" href="javascript:void(0)">&times;</a>
        <div class="modal-content">
          <!-- Add modal content here -->
          <div class="gl-container">
            <div class="container__one-third center employee-card">
              <div class="modal-employee eb-12"></div>
              <h3 class="no-margin--bottom">Jonathan Hallman</h3>
              <p class="no-margin--bottom text--dark-blue"><strong>Client Support</strong></p>
              <p>At MEDITECH since <strong>1995</strong></p>
            </div>
            <div class="container__two-thirds" style="text-align:left;">
              <p>Having spent his whole career in Client Services, Jonathan is very excited about his group's mission to energize customer relationships and help with industry challenges and changes.</p>
              <p>"I never felt the need to move to another division. Each division has overlapping agendas in order to help meet the needs of our customers. I have been able to gain different skills in Client Services through the positions I've held and have been able to work on projects that filled my interests and professional goals," he says.</p>
            </div>
          </div>
        </div>
      </div>
      <!-- End hidden modal box -->

      <!-- Start modal trigger -->
      <div class="open-modal" data-target="modal12">
        <div class="employee-bubble eb-12">
          <div class="eb-info">
            <h3 class="no-margin--bottom">Jonathan Hallman</h3>
            <p>Learn more</p>
          </div>
        </div>
      </div>
      <!-- End modal trigger -->
      
    </div>

  </div>
  
</div>

<!-- End staff circles -->

<!-- END careers--node-4170.php -->
