<!-- START campaign--node-4038.php -->

<?php // This template is set up to control the display of the MEDITECH Expanse Ambulatory Campaign

$url = $GLOBALS['base_url']; // grabs the site url
	
$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<script src="<?php print $url; ?>/sites/all/themes/meditech/js/splide.min.js"></script>


<style>
  /*	Splide Carousel Styles*/
  #primary-slider .splide__pagination {
    display: none;
  }

  #nav-slider .splide__pagination {
    display: none;
  }

  #quote-slider .card {
    box-shadow: none;
    border: 1px solid #c8ced9;
    background-color: #fff;
    padding-bottom: 3px;
    align-self: flex-start;
  }

  #quote-slider .card__info {
    padding: 2em;
  }

  #quote-slider .splide__arrow--prev {
    left: -1em;
  }

  #quote-slider .splide__slide {
    transition: transform 1s;
    transform: scale(1);
  }

  #quote-slider .splide__pagination {
    text-align: center;
    padding-top: 2em;
  }

  .headshot-container {
    display: flex;
    align-items: center;
    margin-top: 1.5em;
  }

  .headshot {
    max-width: 100px;
    max-height: 100px;
    flex-grow: 1;
    margin-right: 1.5em;
  }

  .headshot img {
    border-radius: 50%;
  }

  .quote-name {
    flex-grow: 2;
  }

  .quote-name p {
    margin-bottom: 0;
  }

  .splide__slide {
    transition: transform 1s;
    transform: scale(.9);
  }

  .splide__slide:focus-visible {
    border: 1px solid #3e4545;
  }

  .splide__slide a:focus-visible {
    font-weight: bold;
  }

  .splide__slide.is-active {
    transform: scale(1);
    transition: transform .5s .4s;
  }

  .splide__arrow:hover {
    background: #dde0e7;
    transition: background .25s ease-in-out;
  }

  .splide__arrow:focus-visible {
    outline: 3px solid black;
  }

  #nav-slider {
    margin: 0 2em;
  }

  #nav-slider .splide__track {
    padding-top: 1em;
  }

  #nav-slider .splide__slide {
    transform: scale(1);
    cursor: pointer;
    color: #087E68;
    border: 1px solid #c8ced9;
    border-radius: 7px;
    padding: .5em 1em;
    font-size: 15px;
    font-weight: normal;
    transition: all .2s ease;
    background-color: #fff;
  }

  #nav-slider .splide__slide:hover {
    color: #3e4545;
    border: 1px solid #3e4545;
  }

  #nav-slider .is-active,
  #nav-slider .splide__slide:active,
  #nav-slider .splide__slide:focus {
    color: #3e4545;
    border: 1px solid #3e4545;
    transition: all .2s ease;
  }

  /*	Universal Styles*/
  .content__callout {
    background-color: #fff;
  }

  .content__callout__content {
    background-color: #fff;
  }

  .btn--outline {
    background-color: unset;
    color: #3E4545 !important;
    border-width: 4px !important;
  }

  .caption {
    padding-top: 1em;
    text-align: center;
  }

  .phone-pad {
    padding: 4em 0 0 0;
    max-width: 60%;
    margin: auto;
  }

  .card {
    box-shadow: none;
    border: 1px solid #c8ced9;
  }

  .container-pad {
    padding: 4em 0;
  }

  .bg--light-blue {
    background-color: #f2f8fc;
  }

  .bg-pattern--container img {
    position: relative;
    z-index: 2;
  }

  /*	Splide affecting footer links (need fix)*/
  footer ul li {
    font-weight: 400;
  }

  /*	Media Queries*/
  @media all and (max-width: 900px) {
    .splide__slide {
      transform: scale(1);
    }

    .card figure img {
      height: 225px;
    }
  }

  @media all and (max-width: 50em) {
    .container-pad {
      padding: 2em 0;
    }
  }

  @media all and (max-width: 600px) {
    #nav-slider {
      display: none;
    }

    .phone-pad {
      padding: 1em 0 0 0;
    }

    .splide__pagination {
      display: inline-flex;
      align-items: center;
      width: 100%;
      flex-wrap: wrap;
      justify-content: center;
      margin: 0
    }

    .splide__arrow {
      top: auto;
    }

    .splide__arrow--prev {
      left: 8%;
      bottom: -14px;
    }

    .splide__arrow--next {
      right: 8%;
      bottom: -14px;
    }
  }

  @media all and (max-width: 415px) {
    .splide__pagination li button {
      display: none;
    }

    .splide__arrow--prev {
      left: 40%;
    }

    .splide__arrow--next {
      right: 40%;
    }

    .card figure img {
      height: 150px;
    }
  }

</style>

<div class="js__seo-tool__body-content">


  <h1 style="display:none;" class="js__seo-tool__title">Expanse Ambulatory</h1>

  <!-- Block 1 -->
  <div class="content__callout">
    <div class="content__callout__media">
      <div class="content__callout__image-wrapper">
        <div class="video js__video" data-video-id="678830645">
          <figure class="video__overlay">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--smiling-ambulatory-doctor.jpg" alt="Expanse Ambulatory - video covershot">
          </figure>
          <a class="video__play-btn" href="https://vimeo.com/678830645"></a>
          <div class="video__container"></div>
        </div>
      </div>
    </div>
    <div class="content__callout__content">
      <div class="content__callout__body">
        <p class="header-micro">PRACTICE-CENTERED</p>
        <h2 class="header-one">Built for the practice</h2>
        <p>Physician practices need a comprehensive EHR designed to meet the unique needs of the ambulatory environment. Expanse Ambulatory is a complete solution, built from the ground up with input from practice administrators, nurses, and physicians across a wide range of specialties.</p>
        <p>Whether you’re an independent physician practice or part of an organization using Expanse in other care settings, <span class="bold">Expanse Ambulatory is the most intuitive and efficient EHR and practice management solution available.</span></p>
        <div class="center" style="margin-top: 1em;">
          <?php hubspot_button($cta_code, "Register For The 2021 Physician And CIO Forum"); ?>
        </div>
      </div>
    </div>
  </div>
  <!-- END Block 1 -->

  <!-- Block 2 -->
  <div class="container container-pad bg--purple-gradient">
    <div class="container__centered">
      <p class="header-micro" style="display: inline;">OUR FOCUS</p>
      <h2>Software that’s better for clinicians, patients, and practices</h2>
      <div class="card__wrapper text--black-coconut" style="margin-top:3em;">

        <div class="container__one-third card" style="border:none;">
          <figure>
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/black-doctor-using-tablet-while-standing-in-office.jpg" alt="black doctor using tablet while standing in his office" class="corner-border">
          </figure>
          <div class="card__info">
            <h3>Intuitive for Clinicians</h3>
            <h4>Personalized, flexible, and familiar tools</h4>
            <ul>
              <li>Touch-based navigation</li>
              <li>Customizable chart views</li>
              <li>Specialty-specific templates</li>
            </ul>
          </div>
        </div>

        <div class="container__one-third card" style="border:none;">
          <figure>
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/black-woman-checking-tablet-at-home-in-big-chair.jpg" alt="black woman checking tablet at home in big chair" class="corner-border">
          </figure>
          <div class="card__info">
            <h3>Convenient for Patients</h3>
            <h4>Technology that connects patients to their care</h4>
            <ul>
              <li>Patient portal &amp; mobile app</li>
              <li>Contactless check-in</li>
              <li>Virtual care on-demand</li>
            </ul>
          </div>
        </div>
        <div class="container__one-third card" style="border:none;">
          <figure>
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/white-man-checking-out-at-doctor-office.jpg" alt="Older white man checking out at doctor's office" class="corner-border">
          </figure>
          <div class="card__info">
            <h3>Profitable for Practices</h3>
            <h4>Workflows focused on efficiency and value</h4>
            <ul>
              <li>Efficient workflows</li>
              <li>Advanced revenue cycle tools</li>
              <li>Powerful analytics</li>
            </ul>
          </div>
        </div>

      </div>

    </div>
  </div>
  <!-- END Block 2 -->

  <!-- Block 3 -->
  <!-- START Quote Slider -->
  <div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/about/bg--organic-shapes-green-right-2.svg);">
    <div class="container__centered">
      <div id="quote-slider" class="splide">
        <div class="splide__track">
          <div class="splide__list">

            <li class="splide__slide quote card" tabindex="0">
              <div class="card__info">
                <p class="italic text--large">“I love the MEDITECH platform. I use a mobile device for pretty much everything I do, and I really like the layout. It’s appealing to the eye and it makes sense. The touch screen makes it more natural documenting and interacting with the software.”
                </p>
                <div class="headshot-container">
                  <div class="headshot">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/headshot--jeffery-schleich.jpg" style="max-width:90px; height:90px;" alt="Jeffrey Schleich head shot">
                  </div>
                  <div class="quote-name">
                    <p><span class="bold text--meditech-green" style="line-height: 1.4;">Jeffrey Schleich, MD</span><br>
                      Family Practice Physician<br>
                      FHN</p>
                  </div>
                </div>
              </div>
            </li>

            <li class="splide__slide quote card" tabindex="0">
              <div class="card__info">
                <p class="italic text--large">“I’m so impressed with the MEDITECH system. The patients I see have a ton of chronic illnesses. I’m able to review my notes very easily, look at my previous documentation, pull forward my exam, my review of systems, my previous diagnoses. Everything goes very smoothly. I’m getting a lot more work done, very quickly, and my work is staying at work.”
                </p>
                <div class="headshot-container">
                  <div class="headshot">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/headshot--doug-kanis.jpg" style="max-width:90px; height:90px;" alt="Doug Kanis head shot">
                  </div>
                  <div class="quote-name">
                    <p><span class="bold text--meditech-green" style="line-height: 1.4;">
                        Doug Kanis, DO</span><br>
                      Internal Medicine Physician<br>
                      Pella Regional Health Center</p>
                  </div>
                </div>
              </div>
            </li>

            <li class="splide__slide quote card" tabindex="0">
              <div class="card__info">
                <p class="italic text--large">“Using MEDITECH Expanse on a tablet has enabled me to save up to an hour of time while documenting and placing orders.”
                </p>
                <div class="headshot-container">
                  <div class="headshot">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/headshot--samuel-miller.jpg" style="max-width:90px; height:90px;" alt="Sam Miller head shot">
                  </div>
                  <div class="quote-name">
                    <p><span class="bold text--meditech-green" style="line-height: 1.4;">Sam Miller, MD</span><br>
                      Family Medicine Physician<br>
                      Halifax Health</p>
                  </div>
                </div>
              </div>
            </li>

            <li class="splide__slide quote card" tabindex="0">
              <div class="card__info">
                <p class="italic text--large">“Almost everything I need to write my note, to write my orders is there, and in those cases where something isn’t there I’m about one click or maybe two away. I don’t have to close what I’m doing. I can go look at something and come right back to where I was… I think that really shows that physicians have been greatly involved in the development of this product.”
                </p>
                <div class="headshot-container">
                  <div class="headshot">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/headshot--stephen-tingley.jpg" style="max-width:90px; height:90px;" alt="Stephen Tingley head shot">
                  </div>
                  <div class="quote-name">
                    <p><span class="bold text--meditech-green" style="line-height: 1.4;">Stephen Tingley, MD</span><br>
                      Chief Medical Information Officer<br>
                      Mount Nittany Health
                    </p>
                  </div>
                </div>
              </div>
            </li>

            <li class="splide__slide quote card" tabindex="0">
              <div class="card__info">
                <p class="italic text--large">“We consider MEDITECH Expanse to be the flagship EHR application among the suite of American Health Information Management Association VLab applications that we provide to students.”</p>
                <div class="headshot-container">
                  <div class="headshot">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/headshot--john-richey.jpg" style="max-width:90px; height:90px;" alt="John Richey head shot">
                  </div>
                  <div class="quote-name">
                    <p><span class="bold text--meditech-green" style="line-height: 1.4;">John Richey, MBA, RHIA, FAHIMA</span><br>
                      American Health Information Management Association
                    </p>
                  </div>
                </div>
              </div>
            </li>

          </div>
        </div>
      </div>
    </div>
  </div>

  <script>
    document.addEventListener('DOMContentLoaded', function() {
      var primarySlider = new Splide('#quote-slider', {
        type: "loop",
        perPage: 2,
        gap: "2em",
        padding: {
          right: "2em",
          left: "2em",
        },
        speed: "400",
        pagination: "true",
        keyboard: "true",
        updateOnMove: "true",
        breakpoints: {
          900: {
            perPage: 1,
            gap: "2em",
            padding: {
              right: "2em",
              left: "2em",
            }
          }
        }
      }).mount();
    });

  </script>

  <!-- END Quote slider -->

  <!-- END Block 3 -->

  <!-- Block 4 -->
  <div id="modal1" class="modal">
    <a class="close-modal" href="javascript:void(0)">&times;</a>
    <div class="modal-content">
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/screenshot--animated-chart.gif" alt="Expanse Ambulatory Chart Demo">
    </div>
  </div>

  <div class="container container-pad bg--purple-gradient">
    <div class="container__centered">
      <div class="container__one-third">
        <p class="header-micro" style="display: inline;">CLINICIANS</p>
        <h2>Practice <span class="italic">your</span> way</h2>
        <p>Expanse Ambulatory was designed to be the most intuitive, mobile EHR available, and it’s as easy to personalize as your favorite apps. Clinicians tailor the software to their individual practice styles and workflows — not the other way around — with specialty-specific templates and evidence-based content. The result is less time interacting with their EHR and more time with their patients, which is better for everyone.</p>
      </div>

      <div class="container__two-thirds">
        <figure style="padding: 1.5em 0 0 0;">
          <div class="open-modal tablet--white" data-target="modal1">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/screenshot--animated-chart.gif" alt="Expanse Ambulatory Chart Demo">
            <div class="mag-bg">
              <i class="mag-icon fas fa-search-plus"></i>
            </div>
          </div>
          <p class="caption">Expanse Ambulatory's intuitive, mobile-friendly patient chart</p>
        </figure>
      </div>
    </div>
  </div>
  <!-- END Block 4 -->

  <!-- Block 5 -->
  <!--
    <div id="modal2" class="modal">
        <a class="close-modal" href="javascript:void(0)">&times;</a>
        <div class="modal-content">
            <img src="< ?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Home-Screen_MHealth_Portal--PE.jpg" alt="MHealth portal screenshot" style="width:18em;">>
        </div>
    </div>
-->
  <div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/about/bg--organic-shapes-green-full-width.svg);">
    <div class="container__centered">
      <div class="container__one-half">
        <p class="header-micro">PATIENTS</p>
        <h2>Convenience + engagement = loyalty</h2>
        <p>Patients today expect more from their care experience — and they have more options. Keep your patients engaged and loyal by offering the most convenient, high-quality services possible, like fully integrated virtual visits (including virtual care on-demand), contactless check-in, bidirectional communications, and our powerful patient portal and mobile app, which support <a href="<?php print $url; ?>/ehr-solutions/health-records-on-iphone">Health Records on iPhone®</a>.</p>

        <img style="border-radius:7px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/asian-man-using-phone-to-do-contactless-checkin.jpg" alt="Asain man doing a contactless checkin with phone">

      </div>
      <div class="container__one-half">
        <figure class="phone-pad">
          <div class="phone--black">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Home-Screen_MHealth_Portal--PE.jpg" alt="MHealth portal screenshot (full size)">
          </div>
        </figure>
      </div>
    </div>
  </div>

  <!-- END Block 5 -->
  <!-- Block 6 -->
  <div id="modal3" class="modal">
    <a class="close-modal" href="javascript:void(0)">&times;</a>
    <div class="modal-content">
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/screenshot--amb-scheduling-grid--large.png" alt="Expanse  (full size)">
    </div>
  </div>

  <div class="container container-pad bg--purple-gradient">
    <div class="container__centered">
      <div class="container__one-third">
        <p class="header-micro" style="display: inline;">PRACTICE</p>
        <h2>Go team!</h2>
        <p>Expanse Ambulatory promotes efficient workflows for your entire team — reducing friction, lowering stress, and contributing to a healthier work environment (and bottom line). Like their clinical counterparts, office staff benefit from intuitive, user-centered design — like drag-and-drop scheduling, automated patient communications, and interactive reporting dashboards. The result is an efficient and productive practice environment that’s also peaceful and satisfying for clinicians, staff, and patients.</p>
      </div>

      <div class="container__two-thirds">
        <figure style="padding: 1.5em 0 0 0;">
          <div class="open-modal tablet--white" data-target="modal3">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/screenshot--amb-scheduling-grid-small.png" alt="Expanse ">
            <div class="mag-bg">
              <i class="mag-icon fas fa-search-plus"></i>
            </div>
          </div>
          <p class="caption">Expanse Ambulatory’s intuitive drag-and-drop scheduling grid</p>
        </figure>
      </div>
    </div>
  </div>


  <!--
    <div class="container no-pad">
        <div class="gl-container">
            <div class="container__one-half gl-text-pad bg--purple-gradient">
                <p class="header-micro">PRACTICE</p>
                <h2>Go team!</h2>
                <p>Expanse Ambulatory promotes efficient workflows for your entire team — reducing friction, lowering stress, and contributing to a healthier work environment (and bottom line). Like their clinical counterparts, office staff benefit from intuitive, user-centered design — like drag-and-drop scheduling, automated patient communications, and interactive reporting dashboards. The result is an efficient and productive practice environment that’s also peaceful and satisfying for clinicians, staff, and patients.</p>
                <figure style="max-width: 80%; margin:auto; padding-top:1em;">
                    <div class="open-modal tablet--white" data-target="modal3">
                        <img src="< ?php print $url; ?>/sites/all/themes/meditech/images/campaigns/screenshot--amb-scheduling-grid-small.png" alt="Expanse ">
                        <div class="mag-bg">
                            <i class="mag-icon fas fa-search-plus"></i>
                        </div>
                    </div>
                    <p class="caption">Expanse Ambulatory’s intuitive drag-and-drop scheduling grid</p>
                </figure>
            </div>
            <div class="container__one-half background--cover flex-order--reverse" style="background-image: url(< ?php print $url; ?>/sites/all/themes/meditech/images/campaigns/doctor-hugging-patient-in-office.jpg); background-position:right; min-height:15em;">
            </div>
        </div>
    </div>
-->
  <!-- END Block 6 -->
  <!-- Block 7 -->
  <div class="container container-pad  bg--light-blue">
    <div class="container__centered">

      <div class="center">
        <p class="header-micro" style="display: inline;">FUNCTIONALITY</p>
        <h2>The right tools for the job</h2>
      </div>

      <div id="primary-slider" class="splide">
        <div class="splide__track">
          <div class="splide__list">

            <li class="splide__slide card" tabindex="0">
              <figure>
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-Traverse.jpg" alt="Meditech Traverse interoperability suite">
              </figure>
              <div class="card__info">
                <h3>Traverse</h3>
                <p>Traverse, MEDITECH’s powerful interoperability suite, puts patients back at the center of healthcare by bridging the gaps that can occur during transitions between providers, care settings, and organizations.</p>
                <p><a href="<?php print $url; ?>/ehr-solutions/meditech-interoperability">Read more about MEDITECH Traverse</a></p>
              </div>
            </li>

            <li class="splide__slide card" tabindex="0">
              <figure>
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/expanse-care-compass-hero-image--resized.jpg" alt="Woman Holding Compass in a Desert">
              </figure>
              <div class="card__info">
                <h3>Expanse Care Compass</h3>
                <p>Improve outcomes and reduce costs by enabling care managers to identify and monitor patients with the most pressing needs.</p>
                <p><a href="<?php print $url; ?>/ehr-solutions/care-compass">Read more about Expanse Care Compass</a></p>
              </div>
            </li>

            <li class="splide__slide card" tabindex="0">
              <figure>
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Virtual-Care--Carousel.jpg" alt="Man using virtual care to diagnose daughter">
              </figure>
              <div class="card__info">
                <h3>Virtual Care</h3>
                <p>MEDITECH’s Virtual Care solutions help patients stay connected to their care providers via convenient video visits no matter the circumstances, with options for scheduled and on-demand visits for both new and existing patients.</p>
                <p><a href="<?php print $url; ?>/ehr-solutions/virtual-care">Read more about Virtual Care</a></p>
              </div>
            </li>

            <li class="splide__slide card" tabindex="0">
              <figure>
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/busy-park-with-senior-man-messaging.jpg" alt="Senior man checking phone in park">
              </figure>
              <div class="card__info">
                <h3>Expanse Patient Connect</h3>
                <p>Automate patient communications with conversational, bidirectional messaging via the patient's preferred channel (text message, email, and phone call).</p>
                <p><a href="<?php print $url; ?>/ehr-solutions/expanse-patient-connect">Read more about Expanse Patient Connect</a></p>
              </div>
            </li>

            <li class="splide__slide card" tabindex="0">
              <figure>
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Consumers_Choice--Patient-Engagement--resized.jpg" alt="Man checking mobile device">
              </figure>
              <div class="card__info">
                <h3>Patient &amp; Consumer Health Portal</h3>
                <!--                                <p>Improve access and foster loyalty with tools that inform and empower patients.</p>-->
                <p>MEDITECH's Patient and Consumer Health Portal and mHealth app put patients in control of their health and healthcare with convenient, easy to use tools that resemble the consumer technologies they already know well.</p>
                <p><a href="<?php print $url; ?>/ehr-solutions/patient-engagement">Read more about Patient Engagement</a></p>
              </div>
            </li>

            <li class="splide__slide card" tabindex="0">
              <figure>
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Expanse-Now--Carousel.jpg" alt="Woman checking Expanse Now on mobile device">
              </figure>
              <div class="card__info">
                <h3>Expanse Now</h3>
                <p>MEDITECH’s physician app lets doctors remotely manage routine tasks from their smartphone, effectively communicating and coordinating care from the palm of their hands.</p>
                <p><a href="<?php print $url; ?>/ehr-solutions/ehr-mobility#GtyEwya">Read more about Expanse Now</a></p>
              </div>
            </li>

            <li class="splide__slide card" tabindex="0">
              <figure>
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/internist-speaking-with-virtual-assistant.jpg" alt="Physician using verbal commands to control Expanse software">
              </figure>
              <div class="card__info">
                <h3>Expanse Virtual Assistant</h3>
                <p>Expanse Virtual Assistant responds to verbal commands, retrieving the information doctors are looking for — without the need to type, point, click, or even touch a device, so they can focus on their patients, not their computer systems.</p>
                <p><a href="<?php print $url; ?>/ehr-solutions/virtual-assistant">Read more about Expanse Virtual Assistant</a></p>
              </div>
            </li>

            <li class="splide__slide card" tabindex="0">
              <figure>
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/home/nurse-forum-2020-recap--video-overlay.jpg" alt="Nurses using Expanse Efficiency Dashboard">
              </figure>
              <div class="card__info">
                <h3>Efficiency Dashboard</h3>
                <p>Expanse Efficiency Dashboard leverages user metrics to identify and close training and knowledge gaps, adjust provider workflows, improve EHR proficiency and personalization, and deliver a better overall physician experience.</p>
                <p><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/physicianefficiencydashboardinfographic.pdf" target="_blank">Read more about Efficiency Dashboard</a></p>
              </div>
            </li>

          </div>
        </div>
      </div>

      <div id="nav-slider" class="splide" aria-hidden="true">
        <div class="splide__track">
          <ul class="splide__list">
            <li class="splide__slide" tabindex="-1">
              Traverse
            </li>
            <li class="splide__slide" tabindex="-1">
              Expanse Care Compass
            </li>
            <li class="splide__slide" tabindex="-1">
              Virtual Care
            </li>
            <li class="splide__slide" tabindex="-1">
              Expanse Patient Connect
            </li>
            <li class="splide__slide" tabindex="-1">
              Patient &amp; Consumer Health Portal
            </li>
            <li class="splide__slide" tabindex="-1">
              Expanse Now
            </li>
            <li class="splide__slide" tabindex="-1">
              Expanse Virtual Assistant
            </li>
            <li class="splide__slide" tabindex="-1">
              Efficiency Dashboard
            </li>
          </ul>
        </div>
      </div>

    </div>
  </div>

  <script>
    document.addEventListener('DOMContentLoaded', function() {
      var secondarySlider = new Splide('#nav-slider', {
        autoWidth: true,
        autoHeight: true,
        arrows: false,
        isNavigation: true,
        gap: "1.5em",
        accessibility: false,
        pagination: "false",
        focus: 'center',
        breakpoints: {
          600: {
            isNavigation: false,
          }
        }
      }).mount();

      var primarySlider = new Splide('#primary-slider', {
        type: "loop",
        perPage: 3,
        gap: "1em",
        padding: {
          right: "39px"
        },
        speed: "400",
        pagination: "false",
        keyboard: "true",
        updateOnMove: "true",
        focus: "center",
        breakpoints: {
          900: {
            perPage: 1,
            gap: "2em",
            padding: {
              right: "2em",
              left: "2em",
            }
          }
        }
      });

      primarySlider.sync(secondarySlider).mount();
    });

  </script>
  <!-- END Block 7 -->
  <!-- Block 8 -->
  <div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/about/bg--organic-shapes-green-right-2.svg);">
    <div class="container__centered center">
      <div class="container__one-half">
        <p class="header-micro" style="display: inline;">LEARN MORE</p>
        <h2 style="margin-bottom: 1.5em;">Explore MEDITECH’s top-rated Expanse Ambulatory solution:</h2>
        <a href="<?php print $url; ?>/ehr-solutions/meditech-ambulatory" class="btn--orange btn--outline">Ambulatory EHR</a>
        <p style="margin-bottom: 1.5em;">Clinical Features</p>
        <a href="<?php print $url; ?>/ehr-solutions/expanse-practice-management" class="btn--orange btn--outline">Practice Management</a>
        <p style="margin-bottom: 1.5em;">Administrative/Financial/Reporting</p>
      </div>
      <div class="container__one-half">
        <figure><img class="shadow-box" style="max-width:90%;" src="<?php print $url; ?>/sites/all/themes/meditech/images/about/klas-provider-satisfaction-with-emr-personalization--bold.jpg" alt="KLAS Performance Data - Satisfaction with the level of EMR personalization available for providers."></figure>
      </div>
    </div>
  </div>
  <!-- END Block 8 -->


  <!-- Block 9 CTA -->
  <div class="container bg--purple-gradient">
    <div class="container__centered center">

      <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
      <h2>
        <?php print $cta->field_header_1['und'][0]['value']; ?>
      </h2>
      <?php } ?>

      <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
      <h3>
        <?php print $cta->field_long_text_1['und'][0]['value']; ?>
      </h3>
      <?php } ?>

      <div class="center" style="margin-top:2em;">
        <?php hubspot_button($cta_code, "Watch The Ambulatory Care With Expanse Recorded Webinar"); ?>
      </div>

      <div style="margin-top:1em;">
        <?php print $share_link_buttons; ?>
      </div>

    </div>
  </div>
  <!-- End of Block 9 -->


</div>
<!-- end js__seo-tool__body-content -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>

<!-- END campaign--node-4038.php MEDITECH Expanse Ambulatory -->
