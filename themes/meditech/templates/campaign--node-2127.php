<!-- START campaign--node-2127.php EHR VALUE & SUSTAINABILITY CAMPAIGN -->
<?php
$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
	.circle-icon {
		height: 75px;
		width: 75px;
		color: #fff;
		display: flex;
		justify-content: center;
		align-items: center;
		border-radius: 50%;
		margin: 0 auto;
		margin-bottom: 1em;
	}

	.bg--black-coconut-gradient {
		background: #3E4545;
		background: -webkit-linear-gradient(#4c5252, #181919);
		background: -o-linear-gradient(#4c5252, #181919);
		background: -moz-linear-gradient(#4c5252, #181919);
		background-image: linear-gradient(-150deg, #4c5252, #181919);
		color: #fff;
	}

	.bg--black-coconut-gradient a {
		color: #fff
	}

	.horizontal-bar {
		width: 100%;
		float: left;
		margin-bottom: 1em;
	}

	.horizontal-bar-label {
		float: left;
		text-align: right;
		width: 15%;
		margin-right: 3%;
		font-weight: bold;
	}

	.horizontal-bar-container {
		float: left;
		width: 70%;
	}

	.horizontal-bar-graph--meditech {
		float: left;
		background: linear-gradient(225deg, transparent 10px, #00BC6F 0) top right,
			linear-gradient(315deg, transparent 10px, #00BC6F 0) bottom right;
		background-size: 100% 50%;
		background-repeat: no-repeat;
	}

	.horizontal-bar-graph--cerner {
		float: left;
		background: linear-gradient(225deg, transparent 10px, #00baff 0) top right,
			linear-gradient(315deg, transparent 10px, #00baff 0) bottom right;
		background-size: 100% 50%;
		background-repeat: no-repeat;
	}

	.horizontal-bar-graph--epic {
		float: left;
		background: linear-gradient(225deg, transparent 10px, #bf2525 0) top right,
			linear-gradient(315deg, transparent 10px, #bf2525 0) bottom right;
		background-size: 100% 50%;
		background-repeat: no-repeat;
	}

	.horizontal-bar-value {
		float: left;
		width: 5%;
		margin-left: 2%;
	}

	@media all and (max-width: 1050px) {
		.container__one-half.half-graph {
			width: 100%;
			margin-bottom: 2em;
		}
	}

	.meditech-value {
		font-size: 2em;
		line-height: .8em;
		font-weight: bold;
	}

	/* inner-shadow is actually inner highlight */

	.inner-shadow {
		-moz-box-shadow: inset 0px .8em 0px 0px rgba(255, 255, 255, 0.35);
		-webkit-box-shadow: inset 0px .8em 0px 0px rgba(255, 255, 255, 0.35);
		box-shadow: inset 0px .8em 0px 0px rgba(255, 255, 255, 0.35);
	}

	/* inner-shadow is actually inner highlight */

	.inner-shadow--vertical {
		-moz-box-shadow: inset 20px 0px 0px 0px rgba(255, 255, 255, 0.35);
		-webkit-box-shadow: inset 20px 0px 0px 0px rgba(255, 255, 255, 0.35);
		box-shadow: inset 20px 0px 0px 0px rgba(255, 255, 255, 0.35);
	}

</style>

<div class="js__seo-tool__body-content">

	<!-- Block 1 -->
	<div class="container no-pad">
		<div class="gl-container">
			<div class="container__one-half background--cover" style="background-image: url( <?php print $url; ?>/sites/all/themes/meditech/images/campaigns/hand-stacking-coins-on-dirt-mound.jpg); min-height:550px; background-position: center;">
			</div>
			<div class="container__one-half gl-text-pad bg--green-gradient">
				<h1 class="js__seo-tool__title">A clinically sophisticated EHR at a reasonable cost</h1>
				<p>A solid EHR shouldn’t have to break the bank. Providing care that’s affordable and keeps the cost of healthcare manageable for payers and institutions isn’t just a financial issue, it’s also a matter of morals. MEDITECH’s EHR is a scalable solution that will grow with your organization. Let us help you navigate a challenging economy as you remain committed to providing the best care, at a reasonable price point.</p>
				<div class="btn-holder--content__callout">
					<?php hubspot_button($cta_code, "Explore our Value and Sustainability eBook"); ?>
				</div>
			</div>
		</div>
	</div>
	<!-- End of Block 1 -->

	<!-- Block 2 -->
	<!--        <div class="container background--cover" style="background-image: url(< ?php print $url; ?>/sites/all/themes/meditech/images/campaigns/bg--organic-shapes--rotated-180.svg); padding-bottom: 0;">-->
	<div class="container bg--white">
		<div class="container__centered">
			<div class="container no-pad">
				<div class="auto-margins center">
					<h2>MEDITECH outperforms the competition</h2>
					<p>Why pay extra so your EHR vendor can develop an extravagant employee campus? With Expanse, you get all the functionality with none of the hidden costs. The data speaks for itself. MEDITECH beats the competition in value-based outcomes, with low HAC and readmission penalties, according to a recent <a href="https://home.meditech.com/en/d/mktcontent/otherfiles/navinhafftyqualitystudy.pdf">Navin, Haffty &amp; Associates survey</a>.</p>
				</div>
			</div>
			<div class="container no-pad--bottom">
				<div class="container__one-half half-graph">

					<h3 class="center" style="margin-bottom:1em;">Percent of Hospitals with Readmission Penalty</h3>

					<div class="horizontal-bar">
						<div class="horizontal-bar-label" style="line-height:.9em;">MEDITECH Expanse</div>
						<div class="horizontal-bar-container">
							<div class="horizontal-bar-graph--meditech inner-shadow corner--remove" style="width:84.2%;">&nbsp;</div>
							<div class="horizontal-bar-value meditech-value">84.2%</div>
						</div>
					</div>

					<div class="horizontal-bar">
						<div class="horizontal-bar-label">Cerner</div>
						<div class="horizontal-bar-container">
							<div class="horizontal-bar-graph--cerner inner-shadow corner--remove" style="width:85.2%;">&nbsp;</div>
							<div class="horizontal-bar-value">85.2%</div>
						</div>
					</div>

					<div class="horizontal-bar">
						<div class="horizontal-bar-label">Epic</div>
						<div class="horizontal-bar-container">
							<div class="horizontal-bar-graph--epic inner-shadow corner--remove" style="width:87.1%;">&nbsp;</div>
							<div class="horizontal-bar-value">87.1%</div>
						</div>
					</div>

				</div>

				<div class="container__one-half half-graph">

					<h3 class="center" style="margin-bottom:1em;">Percent of Hospitals with Hospital-Acquired Conditions Penalty</h3>

					<div class="horizontal-bar">
						<div class="horizontal-bar-label" style="line-height:.9em;">MEDITECH Expanse</div>
						<div class="horizontal-bar-container">
							<div class="horizontal-bar-graph--meditech inner-shadow corner--remove" style="width:20.9%;">&nbsp;</div>
							<div class="horizontal-bar-value meditech-value">20.9%</div>
						</div>
					</div>

					<div class="horizontal-bar">
						<div class="horizontal-bar-label">Cerner</div>
						<div class="horizontal-bar-container">
							<div class="horizontal-bar-graph--cerner inner-shadow corner--remove" style="width:24.6%;">&nbsp;</div>
							<div class="horizontal-bar-value">24.6%</div>
						</div>
					</div>

					<div class="horizontal-bar">
						<div class="horizontal-bar-label">Epic</div>
						<div class="horizontal-bar-container">
							<div class="horizontal-bar-graph--epic inner-shadow corner--remove" style="width:24.5%;">&nbsp;</div>
							<div class="horizontal-bar-value">24.5%</div>
						</div>
					</div>

				</div>
			</div>
			<div class="container no-pad">
				<div class="container__one-half half-graph">

					<h3 class="center" style="margin-bottom:1em;">Percent of Hospitals with Positive Value-Based Purchasing Adjustment</h3>

					<div class="horizontal-bar">
						<div class="horizontal-bar-label" style="line-height:.9em;">MEDITECH Expanse</div>
						<div class="horizontal-bar-container">
							<div class="horizontal-bar-graph--meditech inner-shadow corner--remove" style="width:66.1%;">&nbsp;</div>
							<div class="horizontal-bar-value meditech-value">66.1%</div>
						</div>
					</div>

					<div class="horizontal-bar">
						<div class="horizontal-bar-label">Cerner</div>
						<div class="horizontal-bar-container">
							<div class="horizontal-bar-graph--cerner inner-shadow corner--remove" style="width:52.4%;">&nbsp;</div>
							<div class="horizontal-bar-value">52.4%</div>
						</div>
					</div>

					<div class="horizontal-bar">
						<div class="horizontal-bar-label">Epic</div>
						<div class="horizontal-bar-container">
							<div class="horizontal-bar-graph--epic inner-shadow corner--remove" style="width:58.7%;">&nbsp;</div>
							<div class="horizontal-bar-value">58.7%</div>
						</div>
					</div>

				</div>

				<div class="container__one-half half-graph">

					<h3 class="center" style="margin-bottom:1em;">Percent of Hospitals with Negative Value-Based Purchasing Adjustment</h3>

					<div class="horizontal-bar">
						<div class="horizontal-bar-label" style="line-height:.9em;">MEDITECH Expanse</div>
						<div class="horizontal-bar-container">
							<div class="horizontal-bar-graph--meditech inner-shadow corner--remove" style="width:33%;">&nbsp;</div>
							<div class="horizontal-bar-value meditech-value">33%</div>
						</div>
					</div>

					<div class="horizontal-bar">
						<div class="horizontal-bar-label">Cerner</div>
						<div class="horizontal-bar-container">
							<div class="horizontal-bar-graph--cerner inner-shadow corner--remove" style="width:46.8%;">&nbsp;</div>
							<div class="horizontal-bar-value">46.8%</div>
						</div>
					</div>

					<div class="horizontal-bar">
						<div class="horizontal-bar-label">Epic</div>
						<div class="horizontal-bar-container">
							<div class="horizontal-bar-graph--epic inner-shadow corner--remove" style="width:40.3%;">&nbsp;</div>
							<div class="horizontal-bar-value">40.3%</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>


	<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/bg--organic-shapes.svg);">
		<div class="container__centered">
			<div class="center" style="margin-bottom:2em;">
				<h2>A better value, a better partnership</h2>
				<h3>How else does MEDITECH set itself apart from other EHR vendors?</h3>
			</div>
			<div class="container__one-third shadow-box">
				<div class="circle-icon bg--meditech-green">
					<i class="fas fa-hand-holding-usd fa-2x"></i>
				</div>
				<h3 class="center">Value</h3>
				<ul class="fa-ul text--large">
					<span class="fa-li"><i class="fas fa-check-square"></i></span>
					<li class="header-four"><span class="fa-li"><i class="fas fa-genderless text--meditech-green"></i></span>No surcharge for active patient portal charts</li>
					<li class="header-four"><span class="fa-li"><i class="fas fa-genderless text--meditech-green"></i></span>No nickel and diming on support fees</li>
					<li class="header-four"><span class="fa-li"><i class="fas fa-genderless text--meditech-green"></i></span>Fewer IT staff needed for support vs comparable vendors</li>
				</ul>
			</div>
			<div class="container__one-third shadow-box">
				<div class="circle-icon bg--meditech-green">
					<i class="fas fa-handshake fa-2x"></i>
					<!--                        <i class="fas fa-hand-holding-heart fa-2x"></i>-->
				</div>
				<h3 class="center">Partnerships</h3>
				<ul class="fa-ul text--large">
					<li class="header-four"><span class="fa-li"><i class="fas fa-genderless text--meditech-green"></i></span>No bait and switch contracts</li>
					<li class="header-four"><span class="fa-li"><i class="fas fa-genderless text--meditech-green"></i></span>Our customers provide a valued voice in software direction</li>
					<li class="header-four"><span class="fa-li"><i class="fas fa-genderless text--meditech-green"></i></span>Dedicated Professional Services</li>
				</ul>
			</div>
			<div class="container__one-third shadow-box">
				<div class="circle-icon bg--meditech-green">
					<i class="fas fa-user-shield fa-2x"></i>
				</div>
				<h3 class="center">Dependability</h3>
				<ul class="fa-ul text--large">
					<li class="header-four"><span class="fa-li"><i class="fas fa-genderless text--meditech-green"></i></span>Over 50 years of EHR experience</li>
					<li class="header-four"><span class="fa-li"><i class="fas fa-genderless text--meditech-green"></i></span>Eighty-five percent of our customers have been with MEDITECH for ten years or more</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- End Block 2 -->


	<!-- Block 3 UPDATED KLAS BLOCK -->
	<div class="container no-pad">
		<div class="gl-container">
			<div class="container__one-half gl-text-pad bg--green-gradient">
				<h2>Make dollars and sense out of your revenue cycle</h2>
				<p>Get reimbursed for the care you provide. Every time. With MEDITECH’s <a href="https://ehr.meditech.com/ehr-solutions/meditechs-revenue-cycle">Revenue Cycle solution</a> you can minimize lost charges, reduce claim rejections, improve employee productivity, and see a more complete patient story with every claim. Unlike the competition, even within MEDITECH Ambulatory, you can access the easy-to-navigate, integrated Centralized Business Office, where Denial Management begins at the first point of patient contact.</p>
				<div class="center">
					<img style="max-width:250px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-KLAS-Award-2022--Patient-Accounting-and-Patient-Management.png" alt="KLAS 2022 Award for Patient Accounting logo">
					<img style="max-width:250px; margin:0 1em;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-KLAS-Award-2022--Acute-Care-EMR.png" alt="KLAS 2022 Award for Acute Care logo">
				</div>
			</div>
			<div class="container__one-half background--cover flex-order--reverse" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/father-and-young-son-checking-in-at-reception-desk.jpg); min-height:550px; background-position: right;">
			</div>
		</div>
	</div>
	<!-- End Block 3 UPDATED KLAS BLOCK -->


	<!-- Block 4 -->
	<div class="container bg--light-gray">
		<div class="container__centered">
			<h2 class="center">Become a success story</h2>
			<div class="gl-container">
				<div class="container__one-third bg--meditech-green">
					<p><span style="font-weight: bold;" class="header-one">$10 Million Cost Savings</span><br> and 45% Mortality Rate Reduction through sepsis screening at <a href="https://ehr.meditech.com/news/meditech-helps-avera-health-reduce-sepsis-mortality-by-45-save-10-million-0">Avera Health</a> (Sioux Falls, SD)</p>
				</div>
				<div class="container__one-third">
					<p><span style="font-weight: bold;" class="header-one">$2.1 million increase in charge revenue</span><br> via integrated documentation and billing at <a href="https://ehr.meditech.com/news/davies-winner-hilo-medical-center-to-adopt-meditech%E2%80%99s-web-ambulatory">Hilo Medical Center</a> (Hilo, HI)</p>
				</div>
				<div class="container__one-third bg--meditech-green">
					<p><span style="font-weight: bold;" class="header-one">$500,000 savings on interface costs</span><br> through MEDITECH integration at <a href="https://ehr.meditech.com/news/meditech-cuts-costs-improves-productivity-at-phoebe-putney">Phoebe Putney Memorial Hospital</a> (Albany, GA)</p>
				</div>
			</div>
			<div class="gl-container">
				<div class="container__one-third">
					<p><span style="font-weight: bold;" class="header-one">20% increase in cash flow</span><br> and 30% reduction in ED wait times at <a href="https://ehr.meditech.com/news/meditech-s-web-ehr-brings-integration-value-to-wilma-n-v-zquez-medical-center">Wilma N. Vázquez Medical Center</a> (Vega Baja, PR)</p>
				</div>
				<div class="container__one-third bg--meditech-green">
					<p><span style="font-weight: bold;" class="header-one">13 Fewer A/R Days</span><br> and an 8 percent increase in daily clinic volumes at <a href="https://info.meditech.com/summit-pacific-increases-reimbursement-clinic-volumes-with-meditech-analytics-solution-0">Summit Pacific Medical Center</a></p>
				</div>
				<div class="container__one-third">
					<p><span style="font-weight: bold;" class="header-one">$475,000 annual cost savings</span>,<br> 13.7 percent decrease in non-emergent ED visits at <a href="https://ehr.meditech.com/news/meditech-s-ehr-steers-avera-mckennan-ed-nurse-navigator-program-to-475000-annual-savings">Avera McKennan &amp; University Medical Center</a> (Sioux Falls, SD)</p>
				</div>
			</div>
		</div>
	</div>
	<!-- End Block 4 -->


	<!-- Block 5 -->
	<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/bg--organic-shapes-3.svg);">
		<div class="container__centered">
			<div class="container__one-third center">
				<!-- style="max-width:220px;" -->
				<img style="max-height: 430px;" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/icon-cost-down-value-up.png" alt="Icon illustrating lowered cost but improved quality">
				<!--                onerror="this.src='https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/icon-cost-down-value-up.png"-->
			</div>
			<div class="container__two-thirds">
				<h2>Keep the cost of ownership low, but quality high</h2>
				<p>How will you invest the money you save? Take advantage of cost opportunity savings by expanding staffing or investing in technology, social programs, and resources to provide the quality care and welcoming environment your patients deserve. Choose from perpetual license or subscription models with minimal capital outlay.</p>
				<h3>How does MEDITECH do it?</h3>
				<ul class="fa-ul">
					<li><span class="fa-li"><i class="far fa-check-square" aria-hidden="true"></i></span>Using inherent integration</li>
					<li><span class="fa-li"><i class="far fa-check-square" aria-hidden="true"></i></span>Implementing a cost-conscious implementation approach</li>
					<li><span class="fa-li"><i class="far fa-check-square" aria-hidden="true"></i></span>Offering an all-inclusive support fee</li>
					<li><span class="fa-li"><i class="far fa-check-square" aria-hidden="true"></i></span>Utilizing a native web infrastructure</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- End Block 5 -->


	<!-- Block 6 -->
	<div class="background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/maas-cloud-services.jpg); background-position: top;">
		<div class="container__centered bg-overlay--black">
			<div class="container__one-half text--white" style="padding: 3em 0;">
				<h2>Try a subscription-based service on for size</h2>
				<p>Would a subscription-based service model better suit your organization’s needs? <a href="https://ehr.meditech.com/ehr-solutions/meditech-as-a-service-maas">MEDITECH-as-a-Service (MaaS)</a> is a cloud-hosted option for data storage, transfer, and recovery that leverages the latest security measures to keep patient records safe.</p>
				<ul>
					<li>
						<!--<span class="fa-li"><i class="fas fa-genderless"></i></span>-->You decide which option works best for your organization’s needs and finances: the perpetual license model or a subscription-based service.
					</li>
					<li>
						<!--<span class="fa-li"><i class="fas fa-genderless"></i></span>-->For any size, any specialty, MEDITECH as a Service (MAAS) is a cost-effective and scalable EHR solution.
					</li>
					<li>
						<!--<span class="fa-li"><i class="fas fa-genderless"></i></span>-->With MEDITECH’s MaaS offering, customers have autonomy over their EHR. Unlike our competitors’ models, decisions are not dictated by other customers.
					</li>
				</ul>
			</div>
			<div class="container__one-half">&nbsp;</div>
		</div>
	</div>
	<!-- End Block 6 -->

	<!-- Block 7 -->
	<div class="container no-pad">
		<div class="gl-container">
			<div class="container__one-half background--cover" style="background-image: url( <?php print $url; ?>/sites/all/themes/meditech/images/campaigns/family-talks-to-doctor-using-virtual-visit.jpg); min-height:550px; background-position: left;">
			</div>
			<div class="container__one-half gl-text-pad">
				<h2>Virtual care saves lives and money</h2>
				<p>During the COVID-19 pandemic and beyond, <a href="https://www.meditech.com/productbriefs/flyers/virtual_visits_Flyer.pdf">Virtual Visits</a> make it possible to maintain your organization’s revenue stream, or recruit new consumers, when in-person visits and elective procedures are not possible. When at-risk and chronically ill patients can avoid busy public settings or crowded public transportation, they are less susceptible to disease spread that can lead to stressful and costly hospital readmissions.</p>
				<p>Convenient for both patients and providers, this technology also helps you to maintain a personal connection with vulnerable populations who are most at risk for no-show appointments.</p>
			</div>
		</div>
	</div>
	<!-- End Block 7 -->


	<!-- Block 8 -->
	<div class="container no-pad">
		<div class="gl-container">
			<div class="container__one-half gl-text-pad">
				<h2>Achieve positive outcomes with population health</h2>
				<p>Patients aren’t data and statistics — they’re families and relationships, names and faces. See your <a href="https://ehr.meditech.com/ehr-solutions/meditech-population-health">patient population</a> in a new light with the supported functionality to help manage health risks through all stages of life and support value-based care.</p>
				<ul>
					<li>
						<p>With <strong>patient registries</strong>, providers can examine entire groups of patients, break them out into manageable cohorts, and decide on appropriate interventions.</p>
					</li>
					<li>
						<p>Care teams can quickly and easily close <strong>care gaps</strong> right at the point of care while performing their normal ordering, documenting, and scheduling routines.</p>
					</li>
					<li>
						<p>Patients can be connected with vital social services and community resources through <strong>social determinants of health</strong>.</p>
					</li>
				</ul>
			</div>
			<div class="container__one-half background--cover flex-order--reverse" style="background-image: url( <?php print $url; ?>/sites/all/themes/meditech/images/campaigns/crowd-of-people-on-pedestrian-crosswalk.jpg); min-height:550px; background-position: left;">
			</div>
		</div>
	</div>
	<!-- End Block 8 -->


	<!-- Block 9 -->
	<!-- Start hidden modal box -->
	<div id="modal1" class="modal">
		<a class="close-modal" href="javascript:void(0)">&times;</a>
		<div class="modal-content">
			<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/screenshot--BCA-service-line-margin-analysis-large.png">
		</div>
	</div>
	<!-- End hidden modal box -->

	<div class="container bg--green-gradient">
		<div class="container__centered">
			<h2 class="text--white">See how you’re doing, at a glance</h2>
			<div class="container__one-half text--white">
				<div>
					<p>Analyzing, interpreting, and understanding data is key to making informed decisions that touch every area of your organization. And you can’t measure your clinical financial progress without actionable data. MEDITECH’s <a href="https://ehr.meditech.com/ehr-solutions/meditech-business-clinical-analytics">Business and Clinical Analytics (BCA)</a> offers web-based “at a glance” data visualization from across the EHR to increase efficiency, measure progress, and improve performance.</p>
					<ul>
						<li>Standard, regularly refreshed dashboards will help get you up and running with big data initiatives.</li>
						<li>Self-service analytics allows you to create personalized, intuitive dashboards that require no coding expertise.</li>
						<li>Drill down into your Revenue Cycle or choose to view combined Revenue Cycle data with clinical and operational insights for a broader perspective.</li>
					</ul>
				</div>
			</div>
			<div class="container__one-half">
				<!-- Start modal trigger -->
				<div class="open-modal" data-target="modal1">
					<div class="tablet--white">
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/screenshot--BCA-service-line-margin-analysis-small.png">
					</div>
					<!-- Add modal trigger here -->
					<div class="mag-bg">
						<!-- Include if using image trigger -->
						<i class="mag-icon fas fa-search-plus"></i>
					</div>
				</div>
				<!-- End modal trigger -->
			</div>
		</div>
	</div>
	<!-- End Block 9 -->

</div>
<!-- end js__seo-tool__body-content -->

<!-- Block 10 - CTA Block -->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/bg--organic-shapes-2.svg);">
	<div class="container__centered center">

		<?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
		<h2>
			<?php print $cta->field_header_1['und'][0]['value']; ?>
		</h2>
		<?php } ?>

		<?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
		<div>
			<?php print $cta->field_long_text_1['und'][0]['value']; ?>
		</div>
		<?php } ?>

		<div class="center" style="margin-top:2em;">
			<?php hubspot_button($cta_code, "Explore our Value and Sustainability eBook"); ?>
		</div>

		<div style="margin-top:1em;">
			<?php print $share_link_buttons; ?>
		</div>

	</div>
</div>
<!-- End Block 11 -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>
<!-- END campaign--node-2127.php -->
