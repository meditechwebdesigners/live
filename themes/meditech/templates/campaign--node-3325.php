<!-- START campaign--node-3325.php -->


<?php // This template is set up to control the display of the Expanse Oncology campaign updated 2022

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');
$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>


<div class="js__seo-tool__body-content">

  <style>

    .bg--light-blue {
    background-color: #f2f8fc;
    }

    .bg--dark-blue {
    background-color: #222741;
    }

    .content__callout,
    .content__callout__content {
        background-color: transparent;
        color: #fff;
    }

    .show-less {
      height: 500px;
    }

    .show-more-container {
      background-image: linear-gradient(180deg, rgba(255, 255, 255, 0) 0%, RGB(34, 39, 65, 1) 40%);
    }

     .headshot {
        position: absolute;
        top: -50px;
        left: 50%;
        transform: translate(-50%);
        z-index: 3;
    }

    .headshot img {
        width: 100px;
        height: 100px;
        border-radius: 50%;
    }

    .quote-box {
        padding: 3em;
        background-color: rgba(3, 3, 30, .5);
        border-left: 5px solid #00bc6f;
        margin-bottom: 2.35765%;
        border-radius: 7px;
        box-shadow: 0px 0px 20px 1px rgba(0, 0, 0, .2);
        position: relative;
    }

    .image--left img {
      width: 100px;
      height: 100px;
      float: left;
      margin: 0 1em 1em 0;
    }



    @media all and (max-width: 1040px) {
      .image--left img {
        width: 50px;
        height: 50px;
        float: left;
        margin: 0 1em 1em 0;
      }
    }

  </style>

  <!-- START Block 1 -->
    <div class="bg--white" style="padding: 2em 0;">
        <div class="content__callout">
            <div class="content__callout__media">
                <div class="content__callout__image-wrapper">
                    <div class="video js__video" data-video-id="477743790">
                        <figure class="video__overlay">
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--MEDITECH-Oncology--patient-celebrating.jpg" alt=" - Video Covershot">
                        </figure>
                        <a class="video__play-btn" href="https://vimeo.com/477743790"></a>
                        <div class="video__container"></div>
                    </div>
                </div>
            </div>
            <div class="content__callout__content">
                <div class="content__callout__body text--black-coconut">
                  <h1 class="js__seo-tool__title " style="margin-bottom: 0;">MEDITECH Expanse Oncology</h1>
                    <p class="header-two">Keep complex care connected.</p>
                        <p>From diagnosis to survivorship, cancer patients need more than oncology-specific care — they need whole person care. That’s why it’s even more important for their history to be captured in one integrated record, accessible to oncologists in a modern, web-based interface.</p>
                        <p>Expanse Oncology is a complete outpatient solution. Automated practice, scheduling, and infusion workflows optimize efficiency, so you can meet high patient volumes <span class="italic">and</span> provide a positive patient experience.</p>
                         <div>
                          <?php hubspot_button($cta_code, ""); ?>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Block 1 -->


  <!-- BLOCK 2 -->
  <div id="modal1" class="modal">
    <a class="close-modal" href="javascript:void(0)">&times;</a>
    <div class="modal-content">
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Oncology-screencap-edited--block2.png" alt="MEDITECH Oncology software screen shot">
    </div>
  </div>
  <div class="container bg--purple-gradient">
    <div class="container__centered">

      <div class="container__one-half">
        <h2>Care for more patients.</h2>

        <p>Cancer incidence rates are on the rise, challenging oncologists and cancer centers to care for more patients. With Expanse Oncology, they can use any device to see the information they need in the way they want to see it.</p>

        <ul class="fa-ul">
          <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>View the patient’s whole story in personalized chart displays and mobile, web-based specialty workflows.</li>
          <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Use customizable widgets, shortcuts, preferences, and order sets to schedule treatments.</li>
          <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>Document care easily. Options include voice recognition and typicals, which allow physicians to determine preset responses used regularly, or considered “normal” for a patient to be experiencing.</li>
        </ul>
      </div>

      <div class="container__one-half">
        <figure class="center" style="margin-top: 2em;">
          <div class="open-modal tablet--white" data-target="modal1">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Oncology-screencap-edited--block2.png" alt="MEDITECH Oncology software screen shot">
            <div class="mag-bg">
              <i class="mag-icon fas fa-search-plus"></i>
            </div>
          </div>
        </figure>
      </div>

    </div>
  </div>
  <!-- BLOCK 2 -->


 <!-- BLOCK 3 -->
  <div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/about/bg--organic-shapes-green-full-width-2.svg);">
  <div class="container" style="padding-bottom: 1em;">
    <div class="container__centered center">
      <div class="auto-margins">
        <h2>Empower oncologists with specialty-driven content.</h2>

        <p>Everyone benefits from instant access to embedded decision support as well as expert-based templates and protocols from industry-standard resources. Expanse Oncology optimizes care team communication with evidence-based treatment plans that support multidisciplinary ordering.</p>
      </div>

      <div style="padding-top: 1.5em;">
        <div class="container__one-fourth shadow-box" style="padding-top:0;">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--graph-oncology.svg" alt="" style="padding-top:.75em; width:150px;">
          <p>Integrated AJCC 8th & 9th edition TNM staging protocols</p>
        </div>
        <div class="container__one-fourth shadow-box" style="padding-top:0;">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--dr-with-shield-oncology.svg" alt="" style="padding-top:.75em;width:150px;">
          <p>425+ peer-reviewed, embedded NCCN<sup>®</sup> chemotherapy templates</p>
        </div>
        <div class="container__one-fourth shadow-box" style="padding-top:0;">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--touchpoint-oncology.svg" alt="" style="padding-top:.75em; width:150px:">
          <p>Assessment and documentation templates</p>
        </div>
        <div class="container__one-fourth shadow-box" style="padding-top:0;">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--dr-talking-with-patient-oncology.svg" alt="" style="padding-top:.75em; width:150px;">
          <p>NCCN Clinical Practice Guidelines<sup>®</sup> - Survivorship templates</p>
        </div>
      </div>

    </div>
  </div>
</div>
  <!-- BLOCK 3 -->


  <!-- BLOCK 4 -->
  <div class="container bg--dark-blue">
    <div class="container__centered">

      <div class="auto-margins center">
        <h2>The whole patient journey, from diagnosis through survivorship.</h2>

        <p>The patient story doesn’t begin with a cancer diagnosis. Expanse Oncology helps oncologists deliver essential, whole person care. They can see everything that has led up to their patients’ diagnosis and coordinate care effectively during treatments and beyond.</p>
      </div>

      <div class="show-less" data-height="500">
        <div class="gl-container" style="background-color:unset;">
          <div class="container__one-third">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/oncology-pre-screen--block4.png" alt="" style="width:100%;">
            <h3>Start with the big picture. Meet Peter . . . </h3>
            <p>After his diagnosis, Peter arrives at the cancer center, where the oncologist accesses his demographic and health data. Dr. Shelby confirms Peter’s identity, accepts the referral, and reviews everything she needs to know in the EHR. She engages with Peter while staging the cancer electronically and deciding on his treatment.</p>
          </div>
          <div class="container__one-third">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/oncology-cancer-treatment--block4.png" alt="" style="width:100%;">
            <h3>Keep everyone on the same page.</h3>
            <p>Wherever Peter is treated, his care teams are in the know. Standard workflows for reviewing medications, chemotherapy cycles, and treatment plans keep everyone on the same page, and multidisciplinary documentation ensures consistent communication. Plus, centralized ordering enables medications, tests, and assessments to be paired, supporting more efficient treatment planning.</p>
          </div>
          <div class="container__one-third">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/oncology-cancer-free--block4.png" alt="" style="width:100%;">
            <h3>Celebrate with Peter as he rings the bell.</h3>
            <p>Peter’s Survivorship Care Plan keeps him informed about his cancer, treatment, and follow-up care, while his centralized treatment history summarizes the care he received across all settings. Leveraging patient registries and health maintenance protocols, Peter’s care team ensures he meets his milestones during remission.</p>
          </div>
        </div>
      </div>
      <button class="show-more">Show More</button>

    </div>
  </div>
  <!-- BLOCK 4 -->

    <!-- START Block 5 -->
  <div class="container no-pad">
    <div class="gl-container">
      <div class="container__one-half background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/patient-going-over-records-with-doctor-oncology-block5.jpg); min-height:400px;"></div>
      <div class="container__one-half gl-text-pad bg--light-gray">
        <h2>Simplify the patient experience to support a healthier outcome.</h2>
        <p>Your patients often navigate a maze of specialists involved with cancer care, leaving them  overwhelmed. Help them focus on their recovery by using an EHR that keeps all information centralized and coordinates appointments.</p>
       <ul class="fa-ul">
          <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>
            <p>Complete health records follow your patients from setting to setting, ensuring their care is better coordinated. Plus, they don’t have to repeat their medical history over and over again.</p>
          </li>
          <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>
            <p>Centralized scheduling guides care teams so they can coordinate patient appointments, freeing them from multiple health system visits and excessive travel.</p>
          </li>
          <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>
            <p>Easy access to MEDITECH’s Patient and Consumer Health Portal enables your patients to view their consolidated appointments scheduled with various specialists, avoiding confusion.</p>
          </li>
          <li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>
            <p>A centralized business office simplifies billing, so your patients can understand their financial obligations.</p>
          </li>
          
        </ul>
      </div>
    </div>
  </div>
  <!-- END Block 5 -->


  <!-- BLOCK 6 -->
   <div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/about/bg--organic-shapes-green-full-width-2.svg);">
    <div class="container__centered">

      <div class="center" style="padding-bottom: 2em;">
        <h2>Coordinate care and maximize efficiency within the infusion center and throughout the practice.</h2>
      </div>

      <div>
        <div class="container__one-third shadow-box">
          <div class="center">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon-scheduling-oncology.svg" alt="" style="width:150px;">
        </div>
          <h4 class="center bold" style="padding-bottom: 1em;">Scheduling staff</h4>
            <p class="bold">Adjust to changing demands</p>
            <p>Modify appointment schedules on the fly with common web conventions such as drag and drop.</p>
            <p class="bold">Save clicks</p>
            <p>Access patients’ regimens from inside the appointment block, providing context for moving resources on the grid.</p>
        </div>
        <div class="container__one-third shadow-box">
           <div class="center">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon-portal-oncology.svg" alt="" style="width:150px;">
        </div>
           <h4 class="center bold" style="padding-bottom: 1em;">Care team</h4>
            <p class="bold">Experience even greater efficiency</p>
            <p>Integrate Expanse Patient Care with Expanse Oncology to compile status boards of pertinent information from shared system data fields.</p>
            <p class="bold">Improve communication</p>
            <p>Communicate treatment plan changes and modifications quickly to all members of the care team with embedded workload features.</p>
        </div>
        <div class="container__one-third shadow-box">
           <div class="center">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon-finances-oncology.svg" alt="" style="width:150px;">
        </div>
           <h4 class="center bold" style="padding-bottom: 1em;">Practice managers</h4>
            <p class="bold">Track performance</p>
            <p>Access interactive Quality Vantage dashboards to help identify and analyze measures, meet goals, and determine potential impacts on reimbursement.</p> 
        </div>
      </div>
    </div>
    <p class="bold center" style="padding-top:1.5em;"><a href="https://blog.meditech.com/supporting-complex-oncology-care-close-to-home">Find out</a> how Conway Regional Health System is supporting cancer care close to home with Expanse Oncology.</p>
  </div>
  <!-- BLOCK 6 -->

  <!-- Start of Block 7 for when they want TWO quotes, make quote box a one half -->
    <div class="container bg--purple-gradient" style="padding:2em 0 2em 0;">
        <div class="container__centered">

            <div class="container no-pad--bottom">
                <div class="quote-box">
                    <div class="headshot">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/quote-outline.svg" alt="Quote graphic">
                    </div>

                    <p class="text--large italic">“Expanse Oncology has been quite the game changer. We were able to improve functionality across the board while streamlining communications with the rest of the care teams. Our oncologist has become proficient in a short amount of time, resulting in overall better documentation and care.”</p>
                    <p class="bold no-margin--bottom">Travis Reeves, Oncology Director</p>
                    <p>Major Health Partners<br>Shelbyville, IN</p>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Block 7 -->



</div>
<!-- end js__seo-tool__body-content -->


<!-- Block 8 -->
<div class="container">
  <div class="container__centered center">
    

    <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
    <h2>
      <?php print $cta->field_header_1['und'][0]['value']; ?>
    </h2>
    <?php } ?>

    <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
    <div>
      <?php print $cta->field_long_text_1['und'][0]['value']; ?>
    </div>
    <?php } ?>

    <div class="center" style="margin-top:2em;">
      <?php hubspot_button($cta_code, ""); ?>
    </div>

    <div style="margin-top:1em;">
      <?php print $share_link_buttons; ?>
    </div>

  </div>
</div>
<!-- End Block 8 -->


<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>
<!-- END campaign--node-3325.php -->