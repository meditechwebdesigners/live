<!-- START campaign--node-3469.php -->
<?php // This template is set up to control the display of the Cloud Platform content type

$url = $GLOBALS['base_url']; // grabs the site url

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<div class="js__seo-tool__body-content">

    <style>
        .card__wrapper {
            justify-content: center;
        }

        .add-hover {
            border-bottom: 1px solid #c8ced9;
            color: #087E68;
        }

        .add-hover:hover {
            color: #3e4545;
            border-bottom: 1px solid #3e4545;
        }

        .bg--black-coconut-gradient {
            background: #3E4545;
            background: -webkit-linear-gradient(#4c5252, #181919);
            background: -o-linear-gradient(#4c5252, #181919);
            background: -moz-linear-gradient(#4c5252, #181919);
            background-image: linear-gradient(-150deg, #4c5252, #181919);
            color: #fff;
        }

        .headshot-circular {
            margin: 0 auto;
            max-height: 262px;
            width: 100%;
            border-radius: 50%;
        }

        .fa-ul {
            list-style-type: none;
            margin-left: 2.5em;
            padding-left: 0;
        }

        .fa-check-double:before {
            content: "\f560";
            color: rgb(39, 183, 246);
        }

        .card {
            padding: 1em 1.5em;
        }

        @media all and (max-width: 480px) {
            .center-mobile {
                text-align: center;
            }
        }

        @media (max-width: 1150px) {
            .gl-container .container__one-half {
                width: 100%;
            }
        }

    </style>

    <div class="js__seo-tool__body-content">

        <!-- Block 1 -->
        <div class="container background--cover" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/woman-looking-confident-with-clouds-in-foreground--block-one.jpg);">
            <div class="container__centered">

                <div class="container__one-third">&nbsp;</div>

                <div class="container__two-thirds transparent-overlay--xp" style="background-color:rgba(255,255,255,.8);">
                    <h1 style="display:none;" class="js__seo-tool__title">MEDITECH Cloud Platform</h1>
                    <h2 class="header-one no-margin--bottom">MEDITECH Cloud Platform</h2>

                    <h2>Better together: EHR and the cloud</h2>

                    <p>Give your clinicians and patients a better, more mobile healthcare experience while ensuring your organization's long-term sustainability with MEDITECH's Expanse EHR and Cloud Platform.</p>

                    <p>MEDITECH's scalable, secure, cloud-native solutions are built on Google Cloud, representing the latest step in our journey to deliver innovative, cost-effective healthcare technology.</p>

                    <p>Our cloud services include:</p>
                    <ul>
                        <li><strong>Expanse Patient Connect</strong>, which facilitates automated, proactive communication with patients through their preferred channel, including text, email, and phone
                        </li>
                        <li><strong>Virtual Care</strong>, our platform for remote visits, whether routine or urgent
                        </li>
                        <li><strong>Expanse NOW</strong>, our mobile app that allows providers to manage routine tasks and coordinate care, wherever they are
                        </li>
                        <li><strong>High Availability SnapShot</strong>, which augments your downtime solution by providing mobile access to patient data for uninterrupted care
                        </li>
                        <li><strong>Expanse Transport</strong>, which provides newly integrated and automated EHR workflows associated with transporting patients, equipment, and other items within an organization
                        </li>
                    </ul>

                    <div class="center" style="margin-top:1em;">
                        <?php hubspot_button($cta_code, "Download Our Cloud eBook Now (No sign up required)"); ?>
                    </div>
                </div>

            </div>
        </div>
        <!-- Block 1 -->

        <!--Block 2-->
        <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-gradient--bg.jpg">
            <div class="container__centered text--white">

                <div class="center" style="margin-bottom:2em;">
                    <h2>Reap the rewards of cloud transformation</h2>
                </div>

                <div>
                    <div class="container__one-half center-mobile" style="padding: 2em;">
                        <div class="container__one-fifth">
                            <img style="width: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/cloud-security--cloud-care.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/cloud-security--cloud-care.png';this.onerror=null;" alt="security icon">
                        </div>
                        <div class="container__four-fifths">
                            <h4>Security</h4>
                            <p>Safe access to information for patients and providers</p>
                        </div>
                    </div>

                    <div class="container__one-half center-mobile" style="padding: 2em;">
                        <div class="container__one-fifth">
                            <img style="width: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/flexibility--cloud-care.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/flexibility--cloud-care.png';this.onerror=null;" alt="flexibility icon">
                        </div>
                        <div class="container__four-fifths">
                            <h4>Flexibility</h4>
                            <p>Cloud solutions that fit your organization's unique needs</p>
                        </div>
                    </div>
                </div>

                <div>
                    <div class="container__one-half center-mobile" style="padding: 2em;">
                        <div class="container__one-fifth">
                            <img style="width: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/universal-access--cloud-care.svg" alt="universal access icon">
                        </div>
                        <div class="container__four-fifths">
                            <h4>Universal Access</h4>
                            <p>Full control over user credentials and permissions</p>
                        </div>
                    </div>

                    <div class="container__one-half center-mobile" style="padding: 2em;">
                        <div class="container__one-fifth">
                            <img style="width: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/affordability--cloud-care.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/affordability--cloud-care.png';this.onerror=null;" alt="affordability icon">
                        </div>
                        <div class="container__four-fifths">
                            <h4>Affordability</h4>
                            <p>Single contract, subscription payment model</p>
                        </div>
                    </div>
                </div>

                <div>
                    <div class="container__one-half center-mobile" style="padding: 2em;">
                        <div class="container__one-fifth">
                            <img style="width: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/agility--cloud-care.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/agility--cloud-care.png';this.onerror=null;" alt="agility icon">
                        </div>
                        <div class="container__four-fifths">
                            <h4>Agility</h4>
                            <p>Click-and-go integration reduces time lost to installations and upgrades</p>
                        </div>
                    </div>

                    <div class="container__one-half center-mobile" style="padding: 2em;">
                        <div class="container__one-fifth">
                            <img style="width: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Availability--cloud-care.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Availability--cloud-care.png';this.onerror=null;" alt="Availability icon">
                        </div>
                        <div class="container__four-fifths">
                            <h4>Availability</h4>
                            <p>Uninterrupted, seamless access to current and past patient data</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!--Block 2-->


        <!-- Block 3 -->
        <div class="container background--cover" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-layered-background.jpg);">
            <div class="container__centered">

                <div class="center" style="margin-bottom:2em;">
                    <h2>Your toolset just got bigger</h2>
                </div>

                <div class="card__wrapper">

                    <div class="container__one-half card">
                        <div class="center">
                            <img style="width: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--Expanse-Patient-Connect.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/mobile-icon--virtual-care.png';this.onerror=null;" alt="patient connecting with their doctor icon">
                            <h3 class="bold" style="margin-bottom: .25em;">Expanse Patient Connect</h3>
                            <p>Communicate at the speed of life</p>
                        </div>
                        <ul class="fa-ul">
                            <li>
                                <span class="fa-li"><i class="fas fa-check-double" aria-hidden="true"></i></span>
                                Connect with patients on their preferred channel across 19 languages
                            </li>
                            <li>
                                <span class="fa-li"><i class="fas fa-check-double" aria-hidden="true"></i></span>
                                Automate routine appointment-related communications, with patient confirmations and cancelations updating the EHR in real time
                            </li>
                            <li><span class="fa-li"><i class="fas fa-check-double" aria-hidden="true"></i></span>
                                Close care gaps, manage chronic diseases, and promote wellness with actionable messages to pre-selected patients
                            </li>
                        </ul>
                        <div id="modal1" class="modal">
                            <a class="close-modal" href="javascript:void(0)">&times;</a>
                            <div class="modal-content modal-content--vid">
                                <iframe src="https://player.vimeo.com/video/544527596" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="open-modal center" data-target="modal1" style="display: block;">
                            <p><span class="add-hover">Communicate at the Speed of Life [VIDEO]</span></p>
                        </div>
                    </div>

                    <div class="container__one-half card">
                        <div class="center">
                            <img style="width: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/mobile-icon--virtual-care.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/mobile-icon--virtual-care.png';this.onerror=null;" alt="patient using virtual care icon">
                            <h3 class="bold" style="margin-bottom: .25em;">Virtual Care</h3>
                            <p>Care wherever you are</p>
                        </div>
                        <ul class="fa-ul">
                            <li>
                                <span class="fa-li"><i class="fas fa-check-double" aria-hidden="true"></i></span>
                                Mobile access via MEDITECH Patient Portal and MHealth App
                            </li>
                            <li>
                                <span class="fa-li"><i class="fas fa-check-double" aria-hidden="true"></i></span>
                                Virtual Visits for routine care, Virtual On Demand Care for urgent cases
                            </li>
                            <li><span class="fa-li"><i class="fas fa-check-double" aria-hidden="true"></i></span>
                                Reduces personal contact, but not personal care
                            </li>
                        </ul>
                        <p class="center"><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/virtualcareflyer.pdf" target="blank">Stay Connected, No Matter What</a></p>
                    </div>

                    <div class="container__one-half card">
                        <div class="center">
                            <img style="width: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/high-availability-snapshot.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/high-availability-snapshot.png';this.onerror=null;" alt="Computer cloud icon">
                            <h3 class="bold" style="margin-bottom: .25em;">High Availability SnapShot</h3>
                            <p>Uninterrupted care</p>
                        </div>
                        <ul class="fa-ul">
                            <li>
                                <span class="fa-li"><i class="fas fa-check-double" aria-hidden="true"></i></span>
                                Keeps your organization running during unplanned downtime
                            </li>
                            <li>
                                <span class="fa-li"><i class="fas fa-check-double" aria-hidden="true"></i></span>
                                Patient data hosted securely on Google Cloud Platform
                            </li>
                            <li>
                                <span class="fa-li"><i class="fas fa-check-double" aria-hidden="true"></i></span>
                                Clinicians can view health records from any internet-capable device
                            </li>
                        </ul>
                        <div id="modal2" class="modal">
                            <a class="close-modal" href="javascript:void(0)">&times;</a>
                            <div class="modal-content modal-content--vid">
                                <iframe src="https://www.youtube.com/embed/C4q7SdagzN4" title="YouTube video player" width="100%" height="100%" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="open-modal center" data-target="modal2" style="display: block;">
                            <p><span class="add-hover">Because Care Can't Stop [VIDEO]</span></p>
                        </div>
                    </div>

                    <div class="container__one-half card">
                        <div class="center">
                            <img style="width: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/mobile-icon--expanse-now.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/mobile-icon--expanse-now.png';this.onerror=null;" alt="Flexibility icon">
                            <h3 class="bold" style="margin-bottom: .25em;">Expanse NOW</h3>
                            <p>Mobility app for physicians</p>
                        </div>
                        <ul class="fa-ul">
                            <li>
                                <span class="fa-li"><i class="fas fa-check-double" aria-hidden="true"></i></span>
                                Access to high priority workload via smartphone
                            </li>
                            <li>
                                <span class="fa-li"><i class="fas fa-check-double" aria-hidden="true"></i></span>
                                Voice navigation for hands-free convenience
                            </li>
                            <li>
                                <span class="fa-li"><i class="fas fa-check-double" aria-hidden="true"></i></span>
                                Designed with team-based care and care coordination in mind
                            </li>
                        </ul>
                        <p class="center"><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/expansenow.pdf" target="blank">Balance Amid Burnout [Infographic]</a></p>
                    </div>
                    <div class="container__one-half card">
                        <div class="center">
                            <img style="width: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/mobile-icon--expanse-transport.svg" alt="Expanse Transport icon">
                            <h3 class="bold" style="margin-bottom: .25em;">Expanse Transport</h3>
                            <p>Reliable and always at your service</p>
                        </div>
                        <ul class="fa-ul">
                            <li>
                                <span class="fa-li"><i class="fas fa-check-double" aria-hidden="true"></i></span>
                                Increases efficiency of transport staff and dispatchers with a modern solution for transporting patients and equipment
                            </li>
                            <li>
                                <span class="fa-li"><i class="fas fa-check-double" aria-hidden="true"></i></span>
                                Provides patients with smooth, timely transitions that reduce wait times as well as increase satisfaction and quality of care
                            </li>
                            <li>
                                <span class="fa-li"><i class="fas fa-check-double" aria-hidden="true"></i></span>
                                Eliminates the need for third party systems or reliance on manual processes via integration with application/user workflows
                            </li>
                        </ul>
                        <div id="modal3" class="modal">
                            <a class="close-modal" href="javascript:void(0)">&times;</a>
                            <div class="modal-content modal-content--vid">
                                <iframe src="https://www.youtube.com/watch?v=_DOlbsmIVGg" title="YouTube video player" width="100%" height="100%" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="open-modal center" data-target="modal3" style="display: block;">
                            <p><span class="add-hover">Reliable and Always at Your Service [VIDEO]</span></p>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <!--Block 3 -->

        <!-- Block 4 -->
        <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-gradient--bg.jpg">
            <div class="container__centered">

                <div class="container no-pad">
                    <div class="gl-container bg--dark-blue" style="padding-bottom:2em; background-color: inherit;">
                        <div class="container__one-half bg--white">
                            <h3>Security</h3>
                            <ul class="fa-ul">
                                <li>
                                    <span class="fa-li"><i class="fas fa-check-square" aria-hidden="true"></i></span>
                                    Leverages cloud platform built-in security measures
                                </li>
                                <li>
                                    <span class="fa-li"><i class="fas fa-check-square" aria-hidden="true"></i></span>
                                    Enhanced protection against ransomware attacks, phishing, and other cyberthreats
                                </li>
                                <li>
                                    <span class="fa-li"><i class="fas fa-check-square" aria-hidden="true"></i></span>
                                    HIPAA-compliant access to patient data
                                </li>
                            </ul>
                        </div>
                        <div class="container__one-half bg--black-coconut-gradient">
                            <h3>Cost-effectiveness</h3>
                            <ul class="fa-ul">
                                <li>
                                    <span class="fa-li"><i class="fas fa-check-square" aria-hidden="true"></i></span>
                                    Subscription model spreads out implementation costs
                                </li>
                                <li>
                                    <span class="fa-li"><i class="fas fa-check-square" aria-hidden="true"></i></span>
                                    Lower overhead for data storage
                                </li>
                                <li>
                                    <span class="fa-li"><i class="fas fa-check-square" aria-hidden="true"></i></span>
                                    Frees you to focus on your core business: Healthcare
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- Block 4 -->

        <!-- Block 5 -->
        <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/bright-cloud-background--cloud-care.jpg);">
            <div class="container__centered center">

                <div class="auto-margins" style="margin-bottom: 2em;">

                    <figure style="padding-bottom: 1em;">
                        <img class="headshot-circular" src="<?php print $url; ?>/sites/all/themes/meditech/images/people/Scott-Radner--headshot.jpg" style="width:150px;">
                    </figure>
                    <div class="text--large italic">
                        <p>"At MEDITECH, we immediately saw the benefits of moving to the cloud as a replacement for existing systems and as a new way to collaborate with our development teams so that we can achieve our core mission: Supporting care delivery with the best available technology. Freeing our customers to focus on patients and services means that they can succeed in the cloud environment just as MEDITECH has."</p>
                    </div>
                    <div>
                        <p><span class="text--large bold text--meditech-green">Scott Radner</span><br>
                            MEDITECH VP</p>
                    </div>

                </div>

                <p><strong>MEDITECH VP Scott Radner <a href=" https://cloudonair.withgoogle.com/events/hcls-caregiver-agility-webinars?talk=talk2">explains how our cloud-based EHR solutions</a> provide the foundation for healthcare organizations to evolve in today's changing healthcare landscape.</strong></p>

            </div>
        </div>
        <!-- Block 5 -->

        <!-- Block 6 -->
        <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-gradient--bg.jpg">
            <div class="container__centered center" style="padding-bottom:3em; padding-top:0;">
                <div class="slider text--white">

                    <!-- Slider -->
                    <div>
                        <div class="auto-margins">
                            <figure style="padding-bottom: 1em;">
                                <img class="headshot-circular" src="<?php print $url; ?>/sites/all/themes/meditech/images/people/David-Nixdorf.jpg" style="width:150px;">
                            </figure>
                            <div class="text--large italic">
                                <p>"Delivering healthcare throughout remote areas can be challenging, and we strive to support our clinicians with the tools and information they need to provide exceptional healthcare. We believe MEDITECH's High Availability SnapShot is a <b>cost-effective and valuable tool</b> that will give us the ability to access important patient information during unexpected downtimes."</p>
                            </div>
                            <div>
                                <p><span class="text--large bold text--meditech-green">Dave Nixdorf, Director of Support Services</span><br>
                                    Frances Mahon Deaconess Hospital</p>
                            </div>
                        </div>
                    </div>

                    <!-- Slider -->
                    <div>
                        <div class="auto-margins">
                            <figure style="padding-bottom: 1em;">
                                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/louis-harris.png" style="width:150px;">
                            </figure>
                            <div class="text--large italic">
                                <p>"I encourage all organizations using MEDITECH to <b>seriously consider implementing Virtual Visits</b>. It's easier than you think. Our providers picked it up very quickly and patients really appreciate it."</p>
                            </div>
                            <div>
                                <p><span class="text--large bold text--meditech-green">Louis Harris, MD, CMIO</span><br>
                                    Citizens Memorial Healthcare</p>
                            </div>
                        </div>
                    </div>

                    <!-- Slider -->
                    <div>
                        <div class="auto-margins">
                            <figure style="padding-bottom: 1em;">
                                <img class="headshot-circular" src="<?php print $url; ?>/sites/all/themes/meditech/images/people/Mark-Brookman.jpg" style="width:150px;">
                            </figure>
                            <div class="text--large italic">
                                <p>"We're seeing already that a <b>physician can perform a telehealth visit in less time</b> than an in-person visit. It also saves our patients a lot of time and travel to the office."</p>
                            </div>
                            <div>
                                <p><span class="text--large bold text--meditech-green">Mark Brookman, Vice President & CIO</span><br>
                                    Med Center Health</p>
                            </div>
                        </div>
                    </div>

                    <!-- Slider -->
                    <div>
                        <div class="auto-margins">
                            <figure style="padding-bottom: 1em;">
                                <img class="headshot-circular" src="<?php print $url; ?>/sites/all/themes/meditech/images/people/Clark-Averill.jpg" style="width:150px;">
                            </figure>
                            <div class="text--large italic">
                                <p>"<b>Expanse NOW</b> enables our physicians to monitor and respond to workload messages <b>from wherever they are</b>."</p>
                            </div>
                            <div>
                                <p><span class="text--large bold text--meditech-green">Clark Averill, CIO</span><br>
                                    St. Luke's Health</p>
                            </div>
                        </div>
                    </div>

                    <!-- Slider -->
                    <div>
                        <div class="auto-margins">
                            <figure style="padding-bottom: 1em;">
                                <img class="headshot-circular" src="<?php print $url; ?>/sites/all/themes/meditech/images/people/Andy-Burchett-DO.jpg" style="width:150px;">
                            </figure>
                            <div class="text--large italic">
                                <p>"Having an emergency strategy to Avera has always been essential, especially being located in rural areas when our systems may experience issues because of unpredictable weather interrupting technology. This addition to our downtime strategy strengthens our ability to support our community and care teams in any situation, and allows us to use technology to help provide patients with <b>superior care</b>."</p>
                            </div>
                            <div>
                                <p><span class="text--large bold text--meditech-green">Dr. Andy Burchett, CMIO</span><br>
                                    Avera Health</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <link rel="stylesheet" href="<?php print $url; ?>/sites/all/themes/meditech/css/slick.css">
        <link rel="stylesheet" href="<?php print $url; ?>/sites/all/themes/meditech/css/slick-theme.css">
        <script src="<?php print $url; ?>/sites/all/themes/meditech/js/slick.min.js"></script>
        <script>
            jQuery(document).ready(function($) {
                $('.slider').slick({
                    dots: true,
                    prevArrow: '<a class="slick-prev">&#10092;</a>',
                    nextArrow: '<a class="slick-next">&#10093;</a>',
                });
            });

        </script>
        <!-- Block 6 -->

        <!-- Block 7 - CTA Block -->
        <div class="container bg--white text--black-coconut">
            <div class="container__centered" style="text-align: center;">

                <div>
                    <div class="container__two-thirds">

                        <div style="margin:3em 0;">
                            <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
                            <h2>
                                <?php print $cta->field_header_1['und'][0]['value']; ?>
                            </h2>
                            <?php } ?>

                            <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
                            <div>
                                <?php print $cta->field_long_text_1['und'][0]['value']; ?>
                            </div>
                            <?php } ?>

                            <div class="center" style="margin-top:2em;">
                                <?php hubspot_button($cta_code, "Download ebook Now"); ?>
                            </div>
                        </div>

                    </div>

                    <div class="container__one-third">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/cloud-care-e-book-thumbnail.jpg" alt="cloud care ebook thumbnail">
                    </div>
                </div>

                <div style="margin-top:1em;">
                    <?php print $share_link_buttons; ?>
                </div>

            </div>
        </div>
        <!-- Block 7 - CTA Block -->

    </div>
    <!-- end js__seo-tool__body-content -->

    <?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>

    <!-- END campaign--node-3469.php -->
