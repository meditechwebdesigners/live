<!-- start simple-page--node-358.tpl.php template -- CONTACT PAGE -->

    <section class="container__centered">

	    <div class="container__two-thirds">

        <div class="js__seo-tool__body-content">
          <h1 class="js__seo-tool__title">Contact</h1>
          <h2 class="page__title">Are you an existing customer?</h2>
          <p>If you’re a customer in need of assistance, please log in to our <a href="https://www.meditech.com/Programs/ams.exe?TYPE=OnLine.html" target="_blank">Customer Service area</a> and enter a task, or call us anytime at <a class="phone-number" href="tel:1-781-821-3000">781-821-3000</a>.</p>

          <h2>Are you from the media?</h2>
          <p>We’re here to help. Media inquiries should be directed to our <a href="<?php print $url; ?>/meditech-communications-team">Communications team</a>.</p>

          <address class="container no-pad">
            <div class="container__one-third">
              <h3><i class="fas fa-map-marker-alt meditech-green"></i> Address</h3>
              <p>7 Blue Hill River Road,
              <br>Canton, MA 02021</p>
            </div>
            <div class="container__one-third">
              <h3><i class="fas fa-phone meditech-green"></i> Call</h3>
              <p><a class="phone-number" href="tel:1-781-821-3000">781-821-3000</a></p>
            </div>
            <div class="container__one-third">
              <h3><i class="fas fa-fax meditech-green"></i> Fax</h3>
              <p>781-821-2199</p>
            </div>
          </address>
        </div>

        <!-- CONTACT US FORM =============================== -->
        <h3><i class="fas fa-envelope meditech-green"></i>&nbsp; Send us a message</h3>
        
        <?php
          $contactForm = module_invoke('webform', 'block_view', 'client-block-1071');
          print render($contactForm['content']);
        ?>
        <!-- End of Contact Form -->

      </div>

      <aside class="container__one-third panel">
        <div class="sidebar__nav">
          <h2 class="sidebar__nav__title">Facilities</h2>
          <p>Click a location for directions.</p>
          <p>Please review <a href="https://ehr.meditech.com/meditechs-visitor-wellness-policy">MEDITECH's visitor wellness policy</a> before entering our buildings.</p>
          <?php print views_embed_view('locations_contact_page', 'block'); // adds 'locations contact page' Views block... ?>
          <a class="sidebar__nav__link--special" href="about/directions-to-meditech" style="border-top:0;">See directions to all our facilities.</a>
        </div>
      </aside>

    </section>
    
<!-- END simple-page--node-358.tpl.php template -->    