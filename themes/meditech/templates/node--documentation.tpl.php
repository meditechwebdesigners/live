<?php
  // This template is set up to control the display of the 'documentation' content type 

  $url = $GLOBALS['base_url']; // grabs the site url
?>

<!-- start node--documentation.tpl.php template -->
 
<style>
  table, .small { font-size: .8em; }
  table td, table th { padding:.25em 1em; }
  table th { text-align: left; }

  .shortcodes li { margin:10px 0; }
  .shortcodes li ul { list-style-type: circle; }
  .beige { background:#e8d8cc; border:1px solid #999; padding: .5em 2em; }
  .white { background:#ffffff; border:1px solid #999; padding: .5em 2em; } 
  .page-example { 
    overflow: hidden; 
    border: 1px solid #ccc; 
    padding: 1em; margin: 0 auto;
    background-color: beige;
    -moz-box-shadow: 1px 1px 2px 1px #ccc;
    -webkit-box-shadow: 1px 1px 2px 1px #ccc;
    box-shadow: 1px 1px 2px 1px #ccc;
    margin-bottom: 1em;
  }
  .page-small { font-size: 0.8em; height: auto; line-height: 1.15em; min-height: 150px; width: 90%; }
  .page-large { width: 90%; height: auto; margin-top: 2em; font-size: .8em; line-height: 1.15em; }
  hr { margin: 2em 0; }
  .half-hr { width: 50%; margin: 2em auto; float: none; }
  
  .sidebar__nav ul.menu ul.menu { padding-left: 1.5em; margin-top: 0; }
  .sidebar__nav ul.menu ul.menu li { padding: .4em 0 .5em 0; font-size: .8em; }
  .sidebar__nav ul.menu ul.menu li.last { padding-bottom: 0; }

  @media (max-width: 800px){
    .sidebar__nav ul.menu ul.menu li { font-size: 1em; }
  }
</style> 
  
<section class="container__centered">
 
  <div class="container__two-thirds">
    
    <?php if( user_is_logged_in() ){ ?>

      <h1 class="page__title"><?php print $title; ?></h1>

      <?php print render($content['field_body']); // main content if any ?>
    
    <?php } else { ?>
    
      <h1 class="page__title">Access Denied</h1>

      <p>The content of this page is meant for MEDITECH employees only.</p>
    
    <?php } ?>
    
  </div>
  
  <!-- SIDEBAR -->
  <aside class="container__one-third">
    
    <?php if( user_is_logged_in() ){ ?>
    
      <div class="sidebar__nav panel documentation_sidebar_gae">
        <?php
        $docuBlock = module_invoke('menu', 'block_view', 'menu-documentation-side-nav');
        print render($docuBlock['content']); 
        ?>
      </div>
    
    <?php } ?>
    
  </aside>
  <!-- END SIDEBAR -->
  
</section>

<section class="container__centered">
 
  <div class="container__two-thirds">
         
    <?php if( user_is_logged_in() ){ ?>
         
      <?php echo '<!-- <pre>'; echo print_r($node); echo '</pre> -->'; ?>
         
      <?php $date = date('M j, Y G:i', $node->revision_timestamp); ?>
          
      <hr />
      
      <p><strong>Page Identification Information</strong></p>
      <table>
        <?php if($node->nid == 1989){ ?>
        <tr>
          <td style="background-color:#70d0d0; font-size:1.5em;"><strong>Guidelines</strong></td><td style="background-color:white;">&nbsp;</td>
        </tr>
        <?php } else { ?>
        <tr>
          <td style="background-color:#71bcea; font-size:1.5em;"><strong>Work Instruction</strong></td><td style="background-color:white;">&nbsp;</td>
        </tr>
        <?php } ?>
        <tr>
          <td width="30%">Document Owner</td><td>Sean Collins</td>
        </tr>
        <tr>
          <td width="30%">Document Status</td><td>Approved</td>
        </tr>
        <tr>
          <td width="30%">Approved By</td><td>Sean Collins</td>
        </tr>
        <tr>
          <td width="30%">Approval Date/Time</td><td><?php print $date; ?></td>
        </tr>
        <tr>
          <td width="30%">Approval Comment</td><td></td>
        </tr>
        <tr>
          <td width="30%">Approved Version</td><td>1</td>
        </tr>
        <tr>
          <td width="30%">Effective Date</td><td><?php print $date; ?></td>
        </tr>
        <tr>
          <td width="30%">Review Date</td><td>Jul 29, 2022</td>
        </tr>
      </table>

      <p>This documentation is a work-in-progress. Any questions or problems you have while using Drupal may help us to improve this document and reduce the amount of questions and problems in the future.</p>
    
    <?php } ?>
    
  </div>
  
</section>

<!-- end node--documentation.tpl.php template -->