<!-- START campaign--node-2838.php -->
<?php // This template is set up to control the display of the Labor and Delivery content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
	.circle-icon {
		height: 75px;
		width: 75px;
		color: #fff;
		display: flex;
		justify-content: center;
		align-items: center;
		border-radius: 50%;
		margin: 0 auto;
		margin-bottom: 1em;
	}

	.bg--white a {
		color: #087E68;
	}

	.bg--white a:hover {
		color: #3e4545;
	}

</style>

<div class="js__seo-tool__body-content">

	<!-- Hero -->
	<div class="container background--cover no-pad" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Mother-feeding-newborn-with-nurse.jpg);">
		<div class="container__centered bg-overlay--white" style="padding-top:3em; padding-bottom:3em;">
			<div class="container__one-half">
				<h1 class="js__seo-tool__title">The Labor and Delivery solution nurses need to welcome their newest patients.</h1>
				<p>Life's most important transition of care occurs the moment a baby enters the world. With MEDITECH's Expanse Labor and Delivery solution, nurses can keep this transition safe and seamless, from the start of labor through postpartum care.</p>
				<div class="center" style="margin-top:2em;">
					<?php hubspot_button($cta_code, "Sign up for the Labor and Delivery Webinar"); ?>
				</div>
			</div>
			<div class="container__one-half">
				&nbsp;
			</div>
		</div>
	</div>
	<!-- End of Hero -->


	<!-- Block 2 -->
	<div class="container bg--black-coconut">
		<div class="container__centered text--white">

			<div class="auto-margins center">
				<h2>Easing the nurse workflow.</h2>
				<p>When the maternity unit gets busy, nursing priorities can shift in a heartbeat. Expanse Labor and Delivery has tools that allow <a href="https://ehr.meditech.com/ehr-solutions/meditech-nursing">nurses</a> to easily see and trend data to prioritize their workloads and determine where they are most needed at any given time.</p>
			</div>

			<div class="container center no-pad--top">
				<div class="gl-container bg--emerald">

					<div class="container__one-third bg--meditech-green">
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--teamwork.png" alt="Schedule icon">
						<h3>Labor and Delivery Status Board</h3>
						<p>Nurses can manage their workflows, receive alerts, and seamlessly hand off patients when a labor extends beyond their shift.</p>
					</div>
					<div class="container__one-third bg--light-gray">
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--inspect-data.png" alt="Care alerts icon">
						<h3>Labor and Delivery Flowsheet</h3>
						<p>This flexible flowsheet brings all clinical information to the point of documentation, and gives nurses the full perinatal story to guide decisions.</p>
					</div>
					<div class="container__one-third">
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--data-on-phone.png" alt="Patient Portal icon">
						<h3>Expanse Point of Care</h3>
						<p>With this software, nurses gain mobility to complete their daily tasks while minimizing the EHR navigation that keeps them away from the bedside.</p>
					</div>

				</div>
			</div>

		</div>
	</div>
	<!-- End of Block 2 -->


	<!-- Block 3 -->
	<div class="container background--cover bg--light-gray no-pad" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/nurse-using-smart-phone-hallway.jpg);">
		<div class="container__centered bg-overlay--white" style="padding:3em;">
			<div class="container__one-half">
				&nbsp;
			</div>
			<div class="container__one-half">
				<h2>Keep mother and baby closely connected.</h2>
				<p>The close connection between mother and baby shouldn't end after delivery. Expanse Labor and Delivery keeps mother and baby information linked throughout the entire episode, providing a clear view of their stories even as they <a href="https://ehr.meditech.com/ehr-solutions/meditechs-care-coordination">transition between care settings</a>. Through single sign-on to the electronic fetal monitor strip, nurses can evaluate trends between mother and baby in one location to provide complementary care for them both. Following birth, nurses can recall information from the mother's record onto the baby's, eliminating duplicate documentation and keeping them connected.</p>
			</div>
		</div>
	</div>
	<!-- End of Block 3 -->


	<!-- Block 4 -->
	<div class="container background--cover no-pad" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/sleeping-newborn-and-mother.jpg);">
		<div class="container__centered bg-overlay--white" style="padding:3em;">
			<div class="container__one-half">
				<h2>Nurse mobility helps lessen disturbances to new mothers.</h2>
				<p>Postpartum care is an important time for new families to truly begin to bond. <a href="https://ehr.meditech.com/ehr-solutions/meditech-nursing">Expanse Point of Care</a> allows Labor and Delivery nurses to quickly check in on both mother and baby in an unobtrusive way. From a smartphone or handheld mobile device, nurses can take vitals, conduct feeding assessments and safely administer medications without even turning on the lights, letting new mothers get some much-needed rest.</p>
			</div>
			<div class="container__one-half">&nbsp;</div>
		</div>
	</div>
	<!-- End of Block 4 -->


	<!-- Block 5 -->
	<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blurred-busy-hospital-lobby.jpg);">
		<article class="container__centered text--white center auto-margins">
			<figure>
				<img class="quote__content__img" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Donna-Laferriere--headshot.png" style="width: 150px;" alt="Donna Laferriere Headshot">
			</figure>
			<div class="text--large italic" style="font-size:1.4em; padding-top: .75em;">
				<p>“The continuity of care is great. From mom’s prenatal visits, all the way to the pediatrician, this whole multi-day care experience is seeing benefits.”</p>
			</div>
			<p class="no-margin--bottom text--large bold text--meditech-green">Donna Laferriere, RN</p>
			<p>Clinical Informaticist at Northeastern Vermont Regional Hospital (St. Johnsbury, Vermont)</p>
		</article>
	</div>
	<!-- Block 5 -->


	<!-- Block 6 -->
	<div class="container bg--emerald">
		<div class="container__centered">
			<div class="center">
				<h2>Integration tells the complete patient story.</h2>
				<p>From prenatal to postpartum care, the entire episode stays connected within MEDITECH's integrated EHR.</p>
			</div>
			<div class="container center no-pad--top">
				<div class="gl-container bg--white">
					<div class="container__one-third bg--white">
						<h3>Prenatal Care</h3>
						<p>Maria visits an Ob/Gyn for prenatal care in the months leading up to delivery. Her Ob/Gyn keeps a detailed record of her treatments within <a href="https://ehr.meditech.com/ehr-solutions/meditech-ambulatory">Expanse Ambulatory</a>.</p>
					</div>
					<div class="container__one-third bg--light-gray">
						<h3>Labor</h3>
						<p>When she goes into labor, integration with Expanse Ambulatory bridges the gap between prenatal care and delivery. Nurses have a complete record of Maria's perinatal visits to guide their decisions at the point of delivery and beyond.</p>
					</div>
					<div class="container__one-third bg--white">
						<h3>C-Section Delivery</h3>
						<p>Maria's labor takes an unexpected turn and a C-section is required. Her nurses use a <a href="https://ehr.meditech.com/ehr-solutions/meditech-surgical-services">Surgical Services</a> link to quickly document her delivery.</p>
					</div>
				</div>
				<div class="container fw-gl-container bg--blue-gradient">
					<div>
						<h3>Postpartum Care</h3>
						<p>After delivery, Maria reports for her first postpartum checkup. Maria's pediatrician has a complete record of the 10-month perinatal episode to provide informed postpartum care. The focus then starts to shift from Maria onto her healthy newborn son. Via integration with Expanse Ambulatory and Labor and Delivery, pediatricians will gain access to a complete history of the perinatal episode.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End of Block 6 -->


	<!-- Block 7 -->
	<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/doctor-checking-on-newborn-and-mother.jpg);">
		<div class="container__centered text--white">
			<div class="container__two-thirds transparent-overlay">
				<h2>Healthcare organizations can see far-reaching benefits.</h2>
				<ul>
					<li>
						<h4>Improve hand-off communication:</h4>
						<p>Securely share information and seamlessly transition patients among nurses as shifts change.</p>
					</li>
					<li>
						<h4>Enhance continuity of care:</h4>
						<p>Access prenatal information from MEDITECH Ambulatory, or access other-vendor prenatal solutions through a variety of interoperability options.</p>
					</li>
					<li>
						<h4>Identify at-risk patients:</h4>
						<p>Quickly identify high-risk pregnancies and enhance patient safety with <a href="https://ehr.meditech.com/ehr-solutions/meditech-surveillance">MEDITECH's surveillance indicators</a>.</p>
					</li>
					<li>
						<h4>Reduce readmissions:</h4>
						<p>Prepare patients for their return home using Patient Discharge Instructions, Medication Management, the Patient Portal, and follow-up calls.</p>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- End of Block 7 -->


</div>
<!-- end js__seo-tool__body-content -->

<!-- Block 8 - CTA Block -->
<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/abstract-x-background-green.jpg);">
	<div class="container__centered center text--white">

		<?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
		<h2>
			<?php print $cta->field_header_1['und'][0]['value']; ?>
		</h2>
		<?php } ?>

		<?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
		<div>
			<?php print $cta->field_long_text_1['und'][0]['value']; ?>
		</div>
		<?php } ?>

		<div class="center" style="margin-top:2em;">
			<?php hubspot_button($cta_code, "Sign up for the Labor and Delivery Webinar"); ?>
		</div>

		<div style="margin-top:1em;">
			<?php print $share_link_buttons; ?>
		</div>

	</div>
</div>
<!-- End Block 8 -->


<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>
<!-- END campaign--node-2838.php -->
