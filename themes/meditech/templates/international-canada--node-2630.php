<!-- START international-canada--node-2630.php Canadian Page -->

<?php
$url = $GLOBALS['base_url']; // grabs the site url
  
$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
?>

<script src="<?php print $url; ?>/sites/all/themes/meditech/js/splide.min.js"></script>

<style>
	/*	General Styles */
	.btn--outline {
		background-color: transparent;
		color: #fff !important;
		border: 4px solid #087E68 !important;
		-webkit-transition: all 400ms ease-in-out;
	}

	.btn--outline:hover {
		background-color: #087E68;
		-webkit-transition: all 400ms ease-in-out;
	}

	a#cta_button_2897117_79886e8b-4197-43a3-a2de-75edfb615370 {
		padding: 1.17em 1.5em !important;
	}

	.red-border {
		border-left: 7px solid #ff0000;
		padding-left: 12px;
		padding-top: 2px;
		display: block;
	}

	.flag-bg {
		padding: 0 !important;
		background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/international/canada/canadian-flag-flying-over-quebec-city.jpg);
		min-height: 400px;
		background-position: left;
		position: relative;
		z-index: 1;
	}

	.forty-years-logo {
		position: absolute;
		right: 50px;
		width: 165px;
		height: 324px;
		background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/international/canada/40-Years-Canadian-Healthcare-Partnership-Logo.png);
		z-index: 2;
	}

	.card__date {
		bottom: 6px;
	}

	.headshot-container {
		display: flex;
		align-items: center;
		margin-top: 1.5em;
	}

	.headshot {
		max-width: 90px;
		max-height: 90px;
		flex-grow: 1;
		margin-right: 1.5em;
	}

	.quote-name {
		flex-grow: 2;
	}

	.quote-name p {
		margin-bottom: 0;
	}

	/*	Video Styles */
	.content__callout,
	.content__callout__content {
		background-color: transparent;
	}

	.content__callout__content {
		padding-top: 1em;
		padding-bottom: 1em;
	}

	.content__callout__image-wrapper {
		padding: 3em 5em;
	}

	.video-iframe {
		position: relative;
		padding-bottom: 56.25%;
		overflow: hidden;
		margin: 0 auto;
		border-radius: 6px;
		box-shadow: 7px 8px 26px rgba(0, 0, 0, 0.1);
	}

	.video-iframe iframe {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
	}

	/*	Stats Bubbles */
	.stats-container {
		padding: 8em 0 2em 0;
	}

	.stats-bubbles {
		position: relative;
		float: right;
		width: 540px;
		height: 440px;
	}

	.stats-text1 {
		line-height: 1;
		margin: 0;
		font-weight: 600;
		font-size: 3.25em;
	}

	.stats-text2 {
		line-height: 1;
		margin: 0;
		font-weight: 600;
		font-size: 6em;
	}

	.stats-text3 {
		line-height: 1;
		margin: 0;
		font-weight: 600;
		font-size: 4em;
	}

	.sm-text {
		font-size: 1em;
		line-height: 1.25;
	}

	.percent {
		font-size: 3.5em;
		font-weight: 600;
		vertical-align: text-bottom;
	}

	.cr1 {
		width: 250px;
		height: 250px;
		position: absolute;
		background: rgba(50, 50, 93, .75);
		padding: 83px 0;
		z-index: 2;
		border-radius: 50%;
		top: 22px;
		left: -20px;
	}

	.cr2 {
		width: 325px;
		height: 325px;
		position: absolute;
		background: rgba(8, 126, 104, .75);
		padding: 88px 0;
		border-radius: 50%;
		top: 111px;
		left: 30%;
		z-index: 1;
	}

	.cr3 {
		width: 240px;
		height: 240px;
		position: absolute;
		background: rgba(255, 131, 0, .9);
		padding: 64px 0;
		left: 65%;
		border-radius: 50%;
		top: -47px;
	}

	.cr4 {
		width: 25px;
		height: 25px;
		position: absolute;
		background: #00bc6f;
		left: 50%;
		border-radius: 50%;
		top: 10%;
	}

	.cr5 {
		width: 40px;
		height: 40px;
		position: absolute;
		background: rgba(255, 0, 0, .5);
		left: 16%;
		border-radius: 50%;
		top: 72%;
	}

	.cr6 {
		width: 20px;
		height: 20px;
		position: absolute;
		background: rgba(50, 50, 93, .75);
		right: -5%;
		border-radius: 50%;
		top: 50%;
	}

	.fa-li {
		left: -2.25em;
		font-size: 1.25em;
		padding-top: .25em;
	}

	.fa-ul li h3 {
		line-height: 1.5;
	}

	/*	Splide Carousel Styles*/
	#primary-slider .card {
		box-shadow: none;
		border: 1px solid #c8ced9;
		background-color: transparent;
		align-self: flex-start;
	}

	#primary-slider .card__info {
		padding: 2em;
	}

	.splide__pagination {
		text-align: center;
	}

	.splide__slide:focus-visible {
		border: 2px solid #fff !important;
	}

	/*	Calendar Styles */
	.cal-left {
		text-align: center;
		border-radius: 7px 0 0 7px;
	}

	.cal-left p {
		margin: .5em 0;
	}

	.cal-right {
		border-radius: 0 7px 7px 0;
		border-top: 1px solid #E6E9EE;
		border-right: 1px solid #E6E9EE;
		border-bottom: 1px solid #E6E9EE;
		background: rgba(248, 249, 253, 1);
	}

	.cal-right h4 {
		margin: .5em 0;
		padding-left: 2em;
	}

	.cal-right a {
		border-bottom: 1px solid #c8ced9;
	}

	.calendar .gl-container>div {
		padding: 1.25em;
		margin-bottom: 1.5em;
	}

	/*	Contact Us */
	textarea {
		height: 100px;
	}

	select {
		margin-bottom: 1em;
	}

	/*	Media Queries*/
	@media all and (max-width: 1050px) {
		.pad-right {
			padding-right: 3em;
		}

		.stats-container {
			padding: 6em 0 0 0;
		}
	}

	@media all and (max-width: 1000px) {
		.content__callout__image-wrapper {
			padding: 3em;
		}

		.stats-container .container__one-half {
			width: 100%;
		}

		.stats-container {
			padding: 3em 0 0 0;
		}

		.stats-bubbles {
			float: none;
			margin: 3em auto 0 auto;
		}

		.pad-right {
			padding-right: 0;
		}
	}

	@media all and (max-width: 50em) {
		.stats-text1 {
			line-height: 1;
			margin: 0;
			font-weight: 600;
			font-size: 4.5em;
		}

		.stats-text2 {
			line-height: 1;
			margin: 0;
			font-weight: 600;
			font-size: 8.25em;
		}

		.stats-text3 {
			line-height: 1;
			margin: 0;
			font-weight: 600;
			font-size: 5.15em;
		}

		.sm-text {
			font-size: 1.35em;
		}

		.percent {
			font-size: 4.5em;
		}

		.content__callout__image-wrapper {
			padding-left: 6%;
			padding-right: 6%;
			padding-bottom: 1em;
		}

		.content__callout__content p a {
			color: #fff;
			border-bottom: 1px solid #fff;
		}

		.content__callout__content p a:hover {
			color: #00bbb3;
			border-bottom: 1px solid #00bbb3;
		}

		.calendar .gl-container>div:first-child {
			margin-bottom: 0;
		}

		.cal-left {
			border-radius: 7px 7px 0 0;
		}

		.cal-right {
			text-align: center;
			border-radius: 0 0 7px 7px;
			margin-bottom: 1.5em;
			border-top: none;
			border-left: 1px solid #E6E9EE;
		}

		.cal-right h4 {
			padding-left: 0;
		}
	}

	@media all and (max-width: 600px) {

		.splide__pagination {
			display: inline-flex;
			align-items: center;
			width: 95%;
			flex-wrap: wrap;
			justify-content: center;
			margin: 0
		}

		.splide__arrow {
			top: auto;
		}

		.splide__arrow--prev {
			left: 8%;
			bottom: -14px;
		}

		.splide__arrow--next {
			right: 8%;
			bottom: -14px;
		}
	}

	@media all and (max-width: 560px) {
		.stats-bubbles {
			margin: 1em auto 0 auto;
			height: auto;
			width: 100%;
		}

		.cr1 {
			width: 215px;
			height: 215px;
			position: relative;
			padding: 64px 0;
			top: 0;
			left: 0;
			margin: 0 auto;
		}

		.cr2 {
			width: 215px;
			height: 215px;
			position: relative;
			padding: 40px 40px;
			top: 0;
			left: 0;
			margin: -1.5em auto -1.5em auto;
		}

		.cr3 {
			width: 215px;
			height: 215px;
			position: relative;
			padding: 49px 0;
			left: 0;
			top: 0;
			margin: 0 auto;
		}

		.cr4,
		.cr5,
		.cr6 {
			display: none;
		}

		.stats-text2 {
			font-size: 5.15em;
		}

		.percent {
			font-size: 3.5em;
			vertical-align: bottom;
		}
	}

	@media all and (max-width: 415px) {
		.splide__pagination li button {
			display: none;
		}

		.splide__arrow--prev {
			left: 40%;
		}

		.splide__arrow--next {
			right: 40%;
		}
	}

</style>

<div class="js__seo-tool__body-content">

	<h1 style="display:none;" class="js__seo-tool__title">MEDITECH Canada</h1>

	<!-- Block 1 -->
	<div class="container no-pad">
		<div class="gl-container">
			<div class="container__one-half gl-text-pad bg--purple-gradient">
				<h2 class="header-one"><span class="red-border">Connecting Canada with MEDITECH’s EHR</span></h2>
				<p>Across Canada, MEDITECH customers have a rich history of leading the way in delivering the highest quality healthcare. Find out how we’re connecting clinicians, hospitals, and health authorities with more efficient EHR tools and integration that spans all settings.</p>
				<div style="margin:1.5em 0;">
					<?php hubspot_button('6fc5f8e5-6c38-4e87-b9d7-cf6321fca1b7', "Listen To The Children's Wellness Podcast"); ?>
				</div>
				<div>
					<a href="https://customer.meditech.com/en/d/customercanada/homepage.htm" class="btn--emerald btn--outline">Canadian Customer Support</a>
				</div>
			</div>
			<div class="container__one-half background--cover flex-order--reverse flag-bg">
				<div class="forty-years-logo"></div>
			</div>
		</div>
	</div>
	<!-- End Block 1 -->


	<?php if( user_is_logged_in() ){ ?>
	<!-- Block 2 -->
	<div class="container no-pad">
		<div class="gl-container">

			<div class="container__one-half gl-text-pad bg--light-gray">
				<div class="content__callout__image-wrapper" style="padding: 0;">
					<div class="video js__video video--shadow" style="max-width:100%; padding-bottom: 56.25%;" data-video-id="730078197">
						<figure class="video__overlay">
							<img src="<?php print $url; ?>/sites/all/themes/meditech/images/international/canada/MEDITECH-Traverse-Exchange-Canada--Video-Overlay.jpg" alt="MEDITECH Traverse Exchange Canada video image">
						</figure>
						<a class="video__play-btn" href="https://vimeo.com/730078197"></a>
						<div class="video__container"></div>
					</div>
				</div>
			</div>

			<div class="container__one-half gl-text-pad bg--purple-gradient">
				<h2 style="padding-top: 3%;"><span class="red-border">Traverse Exchange Canada bridges communication across communities</span></h2>
				<p>Traverse Exchange Canada is a new cloud-based interoperability solution that enables the free flow of health information from multiple participating organizations and sends it to MEDITECH’s EHR and other EHRs that comply with interoperability standards. The solution provides clinicians with a more holistic view of their patients’ care. It will be available via a software as a service subscription model beginning in Ontario, with the intent to expand throughout Canada. Read the full press release to learn more.</p>
			</div>

		</div>
	</div>
	<!-- END Block 2 -->
	<?php } ?>


	<!-- New Block 2 -->
	<div class="container">
		<div class="container__centered">
			<h2 class="center" style="margin-bottom: 1.5em;"><span class="red-border" style="display:inline-block;">Insights from the Field</span></h2>

			<div class="card__wrapper">
				<div class="container__one-half card">
					<figure>
						<div class="card__date text--small">News</div>
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/international/canada/Holland-Bloorview-Colin-Hung-Julia-Hanigsberg.jpg" alt="Colin Hung and Julia Hanigsberg head shots">
					</figure>
					<div class="card__info">
						<h4 class="no-margin--top no-margin--bottom"><a href="https://www.healthcareittoday.com/2022/09/07/innovation-is-for-kids-at-holland-bloorview/" target="_blank">Innovation is for Kids at Holland Bloorview</a></h4>
						<p class="italic">by Colin Hung</p>
						<p>Holland Bloorview President &amp; CEO, Julia Hanigsberg, recently received the <a href="<?php print $url; ?>/news/holland-bloorview-president-ceo-named-hitmc-healthcare-it-trailblazer-of-the-year">2022 Medigy HITMC Award</a>. In a followup <a href="https://www.healthcareittoday.com/2022/09/07/innovation-is-for-kids-at-holland-bloorview/" target="_blank">interview</a> for <span class="italic">Healthcare IT Today</span>, CMO and editor Colin Hung spoke with Hanigsberg about her approach to innovation. <a href="https://www.youtube.com/watch?v=bmyW09pOfQw">Watch</a> the full interview as Hanigsberg discusses the risks of not innovating in healthcare as well the hospital’s future projects.</p>
					</div>
				</div>
				<div class="container__one-half card">
					<figure>
						<div class="card__date text--small">News</div>
						<img src="<?php print $url; ?>/sites/default/files/styles/main-article-image-breakpoints_theme_meditech_big-medium_2x/public/images/news/Canada-news--pronouns-two-people-forest-green-expanse--Article_0.jpg" alt="Young couple smiling">
					</figure>
					<div class="card__info">
						<h4 class="no-margin--top"><a href="<?php print $url; ?>/news/southlake-regional-health-centre-unveils-use-of-pronouns-within-its-expanse-ehr">Southlake Regional Health Centre improves the patient experience by including gender-affirming pronouns within the EHR</a></h4>
						<p>As part of its commitment to creating more inclusive and equitable care environments, Southlake Regional Health Centre announced that patients can now include gender-affirming pronouns within their electronic health records. Read the full <a href="<?php print $url; ?>/news/southlake-regional-health-centre-improves-the-patient-experience-by-including-gender-affirming">article</a> to learn more about how Expanse can assist with inclusive care.</p>
					</div>
				</div>
			</div>

			<div class="card__wrapper">
			<div class="container__one-third card">
					<figure>
						<div class="card__date text--small">News (Press Release)</div>
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/international/canada/ottowa-canda.jpg" alt="Doctor with child listening to heart">
					</figure>
					<div class="card__info">
						<h4 class="no-margin--top"><a href="https://www.qch.on.ca/newsroom?newsid=87">Ottawa area hospitals among the first in Canada to offer Health Records on iPhone</a></h4>
						<p>Hospitals in the Ottawa region now offer Health Records on iPhone, which brings together hospitals, clinics and the existing Apple Health app to make it easy for patients to see their available medical data from multiple providers.</p>
					</div>
				</div>
				<div class="container__one-third card">
					<figure>
						<div class="card__date text--small">Podcast</div>
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/podcasts/MEDITECH-Podcast--Apple--Resized-Web.png" alt="Julia Hanigsberg President & CEO awarded HITMC">
					</figure>
					<div class="card__info">
						<h4 class="no-margin--top"><a href="https://meditech-podcast.simplecast.com/episodes/why-elevating-childrens-wellness-is-a-social-justice-issue?__hstc=&__hssc=&hsCtaTracking=6fc5f8e5-6c38-4e87-b9d7-cf6321fca1b7%7C71cfba3b-c029-43a5-810c-4646cfd55ab1" target="_blank">Why Elevating Children’s Wellness is a Social Justice Issue</a></h4>
						<p>Julia Hanigsberg, President &amp; CEO of Holland Bloorview Kids Rehabilitation Hospital, explains how advocating for patients with disabilities and other marginalized groups informs her leadership at the largest children’s rehab hospital in Canada.</p>
					</div>
				</div>
				<div class="container__one-third card">
					<figure>
						<div class="card__date text--small">News</div>
						<img src="<?php print $url; ?>/sites/default/files/styles/main-article-image-breakpoints_theme_meditech_big-medium_2x/public/images/news/Fraser-Health--Article.jpg" alt="Doctor with child listening to heart">
					</figure>
					<div class="card__info">
						<h4 class="no-margin--top"><a href="https://www.businesswire.com/news/home/20220413005143/en/Fraser-Health-signs-for-MEDITECH-Expanse-to-support-32-facilities-across-British-Columbia%E2%80%94organization-to-leverage-mobile-technology-and-advanced-clinical-functionality-to-enhance-care">Fraser Health signs for MEDITECH Expanse to support 32 facilities across British Columbia</a></h4>
						<p>Fraser Health (Surrey, B.C.) has selected MEDITECH Expanse to transform care and promote better health outcomes for the more than 1.9 million people it serves throughout 20 diverse communities.</p>
					</div>
				</div>
			</div>

			<div class="center" style="margin-top:1.5em;">
				<p>Additional <a href="<?php print $url; ?>/news-tags/canada">News</a>, <a href="https://blog.meditech.com/">Blogs</a>, &amp; <a href="https://meditech-podcast.simplecast.com/" target="_blank">Podcasts</a></p>
			</div>
		</div>
	</div>
	<!-- New Block 2 -->


	<!-- Start Block 3 -->
	<div class="bg--purple-gradient">
		<div class="content__callout">
			<div class="content__callout__media">
				<div class="content__callout__image-wrapper">
					<div class="js__video video-iframe">
						<iframe width="560" height="315" src="https://www.youtube.com/embed/XOnj4sTruws" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</div>
				</div>
			</div>
			<div class="content__callout__content">
				<div class="content__callout__body text--white">
					<h2><span class="red-border">High Availability Snapshot</span></h2>
					<p>When every minute counts, providers need access to the latest patient data — no matter what happens. With MEDITECH High Availability SnapShot, your patient data is hosted securely on Google Cloud Platform so you can augment your existing disaster recovery and business continuance strategies.</p>
					<p><a href="https://www.youtube.com/channel/UCyj0QFHbuw4kViZWe309pYg" target="_blank">Additional Videos</a></p>
				</div>
			</div>
		</div>
	</div>
	<!-- End Block 3 -->


	<!-- Start Block 4 -->
	<div class="container stats-container">
		<div class="container__centered" style="padding-bottom: 3em;">
			<div class="container__one-half pad-right" style="position: relative;">
				<h2 style="margin-bottom: 1.5em;">Over four decades of uniting communities throughout Canada</h2>
				<ul class="fa-ul" style="padding-right: 2em;">
					<li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>
						<h3>Providing solutions throughout seven provinces &amp; two territories</h3>
					</li>
					<li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>
						<h3>Leading supplier of multi-site and clustered environments in Canada</h3>
					</li>
					<li><span class="fa-li"><i class="fas fa-angle-double-right text--meditech-green" aria-hidden="true"></i></span>
						<h3>MEDITECH customers were the first Canadian hospitals to achieve EMRAM Stage 6 &amp; 7</h3>
					</li>
				</ul>
			</div>
			<div class="container__one-half text--white center">
				<div class="stats-bubbles">
					<div class="cr1">
						<span class="stats-text1">500+</span>
						<p class="sm-text bold">Inpatient facilities</p>
					</div>
					<div class="cr2">
						<span class="stats-text2">47</span><span class="percent">%</span>
						<p class="sm-text bold">of Canadian hospital market</p>
					</div>
					<div class="cr3">
						<span class="stats-text3">50+</span>
						<p class="sm-text bold">LIVE regional<br>installations</p>
					</div>
					<div class="cr4"></div>
					<div class="cr5"></div>
					<div class="cr6"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Block 4 -->


	<!-- Start Block 5 -->
	<div class="container bg--purple-gradient text--white">
		<div class="container__centered">
			<div id="primary-slider" class="splide">
				<div class="splide__track">
					<div class="splide__list">
						<li class="splide__slide card" tabindex="0">
							<div class="card__info">
								<p class="italic text--large">“As a longtime customer, we've experienced the many benefits of utilizing MEDITECH's EHR. These benefits don't only cover patient care but they span across the whole organization. We're fortunate to have wisely invested in a solution that's been sustainable for the last two decades and that we foresee will continue to be well into the future.”</p>
								<div class="headshot-container">
									<div class="headshot">
										<img src="<?php print $url; ?>/sites/all/themes/meditech/images/international/canada/headshot--barbara-collins.png" style="max-width:90px; max-height:90px;" alt="Barbara Collins head shot">
									</div>
									<div class="quote-name">
										<p class="bold text--meditech-green" style="line-height: 1.4;">Barbara Collins, CEO</p>
										<p>Humber River Hospital</p>
									</div>
								</div>
							</div>
						</li>
						<li class="splide__slide card" tabindex="0">
							<div class="card__info">
								<p class="italic text--large">“Our relationship with Humber provided us the foundation to build an EHR that would take our organization to the next level. By acquiring the Expanse technology, we are also now able to improve our care coordination and internal processes, as well as work more closely with Humber for the betterment of the community.”</p>
								<div class="headshot-container">
									<div class="headshot">
										<img src="<?php print $url; ?>/sites/all/themes/meditech/images/international/canada/headshot--connie-dejak.png" style="max-width:90px; max-height:90px;" alt="Connie Dejak head shot">
									</div>
									<div class="quote-name">
										<p class="bold text--meditech-green" style="line-height: 1.4;">Connie Dejak, President and Chief Executive Officer</p>
										<p>Runnymede Healthcare Centre</p>
									</div>
								</div>
							</div>
						</li>
						<li class="splide__slide card" tabindex="0">
							<div class="card__info">
								<p class="italic text--large">“The patient portal is a valuable tool that empowers patients to be active participants in their own care.”</p>
								<div class="headshot-container">
									<div class="headshot">
										<img src="<?php print $url; ?>/sites/all/themes/meditech/images/international/canada/headshot--sanaz-riahi.png" style="max-width:90px; max-height:90px;" alt="Sanaz Riahi head shot">
									</div>
									<div class="quote-name">
										<p class="bold text--meditech-green" style="line-height: 1.4;">Sanaz Riahi RN, MSN, PhD, Vice-President, Practice, Academics and Chief Nursing Executive</p>
										<p>Ontario Shores Centre for Mental Health Sciences</p>
									</div>
								</div>
							</div>
						</li>
						<li class="splide__slide card" tabindex="0">
							<div class="card__info">
								<p class="italic text--large">“Even though the content of the Electronic Health Record is sometimes in English – for continuity of care with our community partners – offering our patients a French interface was non-negotiable. By collaborating with MEDITECH, we were able to make this option available.”</p>
								<div class="headshot-container">
									<div class="headshot">
										<img src="<?php print $url; ?>/sites/all/themes/meditech/images/international/canada/headshot--bernard-leduc.png" style="max-width:90px; max-height:90px;" alt="Bernard Leduc head shot">
									</div>
									<div class="quote-name">
										<p class="bold text--meditech-green" style="line-height: 1.4;">Dr. Bernard Leduc, CEO</p>
										<p>Hôpital Montfort</p>
									</div>
								</div>
							</div>
						</li>
						<li class="splide__slide card" tabindex="0">
							<div class="card__info">
								<p class="italic text--large">“One way we’re leveraging Expanse to assist us in some of our health equity work is that we recently turned on the feature for pronouns. And that means that the individual’s pronouns are right there in the health record for the clinician to see and that person doesn't have to explain their gender identity, every time. That's a great example of when you change something in the system it's just changed for life, the patient doesn't have to keep coming back and telling you over and over again.”</p>
								<div class="headshot-container">
									<div class="headshot">
										<img src="<?php print $url; ?>/sites/all/themes/meditech/images/international/canada/headshot--julia-hanigsberg.png" style="max-width:90px; max-height:90px;" alt="Julia Hanigsberg head shot">
									</div>
									<div class="quote-name">
										<p class="bold text--meditech-green" style="line-height: 1.4;">Julia Hanigsberg President & CEO</p>
										<p>Holland Bloorview Kids Rehabilitation Hospital</p>
									</div>
								</div>
							</div>
						</li>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script>
		document.addEventListener('DOMContentLoaded', function() {
			new Splide('#primary-slider', {
				type: "loop",
				perPage: 2,
				gap: "2em",
				padding: {
					right: "2em",
					left: "2em",
				},
				speed: "400",
				pagination: "true",
				keyboard: "true",
				updateOnMove: "true",
				breakpoints: {
					900: {
						perPage: 1,
						gap: "2em",
						padding: {
							right: "2em",
							left: "2em",
						}
					}
				}
			}).mount();
		});

	</script>
	<!-- End Block 5 -->


	<!-- Start Block 6 -->
	<div class="container calendar">
		<div class="container__centered">
			<h2 class="center" style="margin-bottom: 1.5em;"><span class="red-border" style="display:inline-block">Calendar of Events</span></h2>

			<div class="gl-container">
				<div class="container__one-fourth cal-left bg--purple-gradient text--white">
					<p class="header-four bold">Sept 20-22, 2022</p>
				</div>
				<div class="container__three-fourths cal-right">
					<h4><a href="https://ehr.meditech.com/events/meditech-live">MEDITECH LIVE: Building Connections that Drive Change</a> (In-person)</h4>
				</div>
			</div>

		</div>
	</div>
	<!-- End Block 6 -->


	<!-- Start Block 7 -->
	<div id="contact-form" class="container bg--purple-gradient">
		<div class="container__centered">
			<div class="container__one-half">
				<h2>Contact Us</h2>
				<p style="margin-bottom: 1.5em;">Have a question? We’re here to help. Please send us a message through the contact form.</p>
				<div class="container no-pad--top" style="padding-bottom:1em;">
					<div class="headshot-container">
						<div class="headshot">
							<img src="<?php print $url; ?>/sites/all/themes/meditech/images/international/canada/headshot--robert-molloy.png" alt="Robert Molloy Headshot">
						</div>
						<div class="quote-name">
							<h3 style="margin-bottom:.25em;">Robert Molloy</h3>
							<p>Director Canadian Market and Product Strategy</p>
						</div>
					</div>
				</div>
				<div class="container no-pad--top" style="padding-bottom:1em;">
					<div class="headshot-container">
						<div class="headshot">
							<img src="<?php print $url; ?>/sites/all/themes/meditech/images/international/canada/headshot--patricia-pacewicz.png" alt="Patricia Pacewicz Headshot">
						</div>
						<div class="quote-name">
							<h3 style="margin-bottom:.25em;">Patricia Pacewicz</h3>
							<p>Director Client Support Canada</p>
						</div>
					</div>
				</div>
				<div class="container no-pad--top">
					<div class="headshot-container">
						<div class="headshot">
							<img src="<?php print $url; ?>/sites/all/themes/meditech/images/international/canada/headshot--nick-palmieri.png" alt="Nick Palmieri Headshot">
						</div>
						<div class="quote-name">
							<h3 style="margin-bottom:.25em;">Nick Palmieri</h3>
							<p>Director Regional Sales Canada</p>
						</div>
					</div>
				</div>
				<div style="margin-bottom: 1.5em;">
					<a href="https://customer.meditech.com/en/d/customercanada/homepage.htm" class="btn--emerald btn--outline">Canadian Customer Support</a>
				</div>
			</div>
			<div class="container__one-half text--white" style="padding: 1em 2em; border: 1px solid #c8ced9; border-radius: 7px;">
				<?php $canadaForm = module_invoke('webform', 'block_view', 'client-block-3831'); print render($canadaForm['content']); ?>
			</div>
		</div>
	</div>
	<!-- End Block 7 -->

	<!-- Block 8 -->
	<div class="container">
		<div class="container__centered" style="text-align: center;">
			<div class="auto-margins">
				<h2>Why Elevating Children’s Wellness is a Social Justice Issue</h2>
				<p>Hear from Julia Hanigsberg, President & CEO of Holland Bloorview, as she explains how advocating for patients with disabilities and other marginalized groups informs her leadership at the largest children’s rehab hospital in Canada.</p>
			</div>

			<div class="center" style="margin-top:2em;">
				<?php hubspot_button('6fc5f8e5-6c38-4e87-b9d7-cf6321fca1b7', "Listen To The Children's Wellness Podcast"); ?>
			</div>

			<div style="margin-top:1em;">
				<?php print $share_link_buttons; ?>
			</div>
		</div>
	</div>

</div>
<!-- End Block 8 -->

<!-- END international-canada--node-2630.php Canadian Page -->
