<!-- START page.tpl.php of the SEVEN theme -->
 
  <div id="branding" class="clearfix">
    <?php print $breadcrumb; ?>
    <?php print render($title_prefix); ?>
    <?php if ($title): ?>
      <h1 class="page-title"><?php print $title; ?></h1>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
    <?php print render($primary_local_tasks); ?>
  </div>

  <div id="page">
    <?php if ($secondary_local_tasks): ?>
      <div class="tabs-secondary clearfix"><?php print render($secondary_local_tasks); ?></div>
    <?php endif; ?>

    <div id="content" class="clearfix">
     
      <div class="element-invisible"><a id="main-content"></a></div>
      <?php if ($messages): ?>
        <div id="console" class="clearfix"><?php print $messages; ?></div>
      <?php endif; ?>
      <?php if ($page['help']): ?>
        <div id="help">
          <?php print render($page['help']); ?>
        </div>
      <?php endif; ?>
      <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
      
      
      <?php      
      $remove_needsQC_and_QCcomplete_options = '<script>
        var $jq = jQuery.noConflict();
        $jq(document).ready(function(){
          $jq(\'#edit-workbench-moderation-state-new option[value="needs_review"]\').each(function(){
            $jq(this).remove();
          });
          $jq(\'#edit-workbench-moderation-state-new option[value="qc_complete"]\').each(function(){
            $jq(this).remove();
          });
        });
        </script>';
      
      $remove_qc_options_and_save_buttons = '<script>
        var $jq = jQuery.noConflict();
        $jq(document).ready(function(){
          $jq(\'#edit-workbench-moderation-state-new\').each(function(){
            $jq(this).remove();
          });
          $jq(\'#edit-actions\').each(function(){
            $jq(this).remove();
          });
        });
        </script>';
      
      function get_news_tags($node){
        // list taxonomy News-tags terms associated with article...
        $news_tags = field_view_field('node', $node, 'field_news_tags'); 
        // 'field_news_tags' is the machine name of the field in the content type that contains the taxonomy
        $news_terms = array();
        foreach($news_tags['#items'] as $news_tag){
          $news_term = taxonomy_term_load($news_tag['tid']);
          $news_terms[] = $news_term->name;
        }
        return $news_terms;
      }
      
      function get_international_region($node){
        // list taxonomy International Region terms associated with article...
        $international_region_tags = field_view_field('node', $node, 'field_international_region');                  
        // 'field_international_region' is the machine name of the field in the content type that contains the taxonomy...
        $international_region_term = $international_region_tags['#items'][0]['taxonomy_term'];
        $international_region_name = $international_region_term->name;
        return $international_region_name;
      }
      
      // list current user's roles...
      $user_roles = $user->roles;
      
      $not_south_africa_warning = '<p class="country-alert">You are NOT logged in as a <strong>South Africa User</strong> and this page is designated as South Africa content! <strong>Please DO NOT edit this page!</strong><br><span style="font-size:.7em;">If you accidentally do, please inform a member of the US Web Design team so it can be corrected.</span></p>';
      
      $not_asia_pacific_warning = '<p class="country-alert">You are NOT logged in as an <strong>Asia Pacific User</strong> and this page is designated as Asia Pacific content! <strong>Please DO NOT edit this page!</strong><br><span style="font-size:.7em;">If you accidentally do, please inform a member of the US Web Design team so it can be corrected.</span></p>';
      
      $not_proper_region_user_warning = '<p class="country-alert">You <strong>DO NOT</strong> have the proper permissions to edit this content! <strong>Please DO NOT edit this page!</strong><br><span style="font-size:.7em;">If you accidentally do, please inform a member of the US Web Design team so it can be corrected.</span></p>';

      ?>
      
      
      
      
      <?php 
        if( isset($page['content']['system_main']['#node']->type) ){ 
        // if a node edit/create page, use 2-column layout with SEO TOOL 2.0 on the right AND country content warning... 
      ?>
        
        <style> .country-alert { padding: 1em; background-color: #f00; color: #fff; font-size: 1.5em; line-height: 1.5em; } </style>

        <div style="float:left; width:78%; margin-right:2%;">
        
      <?php } else { ?>
      
        <div>
      
      <?php } ?>
      
      
      
     
      <?php 
      // if looking at the back end of a node page...
      if( isset($node->nid) ){
        // display node id to user...
        print '<p><strong>Node ID:</strong> '.$node->nid;
        // get the node type...
        $node_type = $node->type;
        // replace any underscores in the type name with hyphens...
        $type_link = str_replace('_', '-', $node_type);
        // display link to content type documentation page...
        print '<a style="margin-left:2em; color:black; background-color:#ffd17c; padding:.25em .75em;" href="https://ehr.meditech.com/drupal-documentation/';
          if( strpos($type_link, 'campaign') !== false ){
            print 'campaign-content-types';
          }
          else{
            print $type_link.'-content-type';
          }
        print '" target="_blank">Instructions using this form</a>';
        // display link to SEO best practice page...
        print '<a style="margin-left:2em; color:black; background-color:#ffd17c; padding:.25em .75em;" href="https://ehr.meditech.com/drupal-documentation/seo-best-practices" target="_blank">SEO Best Practices</a></p>';
        
        
        
        
echo '<!--<pre>'; echo print_r($user_roles); echo '</pre>-->';
        
        // check to see if user is accessing an International - South Africa page...
        if( $node->type == 'international_south_africa' ){
          // if the user does not have South Africa as one of their roles, display warning...
          if( !in_array('South Africa', $user_roles) && !in_array('administrator', $user_roles) && !in_array('International', $user_roles) ){
            print $not_south_africa_warning;
          }
        } // END if South Africa page
        
        // check to see if user is accessing an International - Asia Pacific page...
        elseif( $node->type == 'international_asia_pacific' ){
          // if the user does not have Asia Pacific as one of their roles, display warning...
          if( !in_array('Asia Pacific', $user_roles) && !in_array('administrator', $user_roles) && !in_array('International', $user_roles) ){
            print $not_asia_pacific_warning;
          }
        } // END if Asia Pacific page
        
        
        
        
        // check to see if user is accessing a News Article page...
        elseif( $node->type == 'news_article' ){
          $news_terms = get_news_tags($node);
          
          // check to see if the News Article is tagged with South Africa...
          if( in_array('South Africa', $news_terms) ){
            // if the user does not have one of the following roles, display warning...
            if( !in_array('South Africa', $user_roles) && !in_array('administrator', $user_roles) && !in_array('International', $user_roles) ){
              print $not_south_africa_warning;
            }
          }
          
          // check to see if the News Article is tagged with Asia Pacific...
          if( in_array('Asia Pacific', $news_terms) ){
            // if the user does not have one of the following roles, display warning...
            if( !in_array('Asia Pacific', $user_roles) && !in_array('administrator', $user_roles) && !in_array('International', $user_roles) ){
              print $not_asia_pacific_warning;
            }
          }
          
          // if News Article is NOT tagged with Asia Pacific or South Africa...
          if( !in_array('Asia Pacific', $news_terms) && !in_array('South Africa', $news_terms) ){
            // if the user does not have one of the following roles, display warning...
            if( in_array('Asia Pacific', $user_roles) || in_array('South Africa', $user_roles) ){
              print $not_proper_region_user_warning;
            }
          }
          
        } // END if News Article
        
        
        // check to see if user is accessing a page designated by an International Region (Events, Executives, Video, Locations)...
        else{
          
          if( property_exists($node, 'field_international_region') === true ){
            $international_region_name = get_international_region($node);
            
            // check to see if the page's region is South Africa...
            if($international_region_name == 'South Africa' ){
              // if the user does not have one of the following roles, display warning...
              if( !in_array('South Africa', $user_roles) && !in_array('administrator', $user_roles) && !in_array('International', $user_roles) ){
                print $not_south_africa_warning;
              }
            }
            // check to see if the page's region is Asia Pacific...
            if($international_region_name == 'Asia Pacific' ){
              // if the user does not have one of the following roles, display warning...
              if( !in_array('Asia Pacific', $user_roles) && !in_array('administrator', $user_roles) && !in_array('International', $user_roles) ){
                print $not_asia_pacific_warning;
              }
            }
            if($international_region_name != 'Asia Pacific' && $international_region_name != 'South Africa'){
              $a = 0;
              if( in_array('South Africa', $user_roles)){
                $a = $a + 1;
              }
              if( in_array('Asia Pacific', $user_roles)){
                $a = $a + 1;
              }
echo $a;
              if($a > 0){
                print $not_proper_region_user_warning;
              }
            }
            
          }
          
        } // END if has international region
        
      } // END if node id exsits


      print render($page['content']); // displays Drupal's backend content form


      // if creating a new node...
      if( !isset($node->nid) ){
        // check to see if current user has the South Africa or Asia Pacific role assigned to them...
        if( in_array('South Africa', $user_roles) || in_array('Asia Pacific', $user_roles)){
          // remove all but Draft and Publish from publishing drop down...
          print $remove_needsQC_and_QCcomplete_options;
        }
      } // END if creating a new node
          
      // if editing an existing node...
      else {
        // check if current user has edit permissions for this node type...
        if(node_access('update',$node)){
          
          // if International - South Africa content type...
          if( $node->type == 'international_south_africa' ){
            // if the user has South Africa as one of their roles, remove some options...
            if( in_array('South Africa', $user_roles) ){
              // remove all but Draft and Published options for other content types...
              print $remove_needsQC_and_QCcomplete_options;
            }
          }
          
          // if International - Asia Pacific content type...
          if( $node->type == 'international_asia_pacific' ){
            // if the user has Asia Pacific as one of their roles, remove some options...
            if( in_array('Asia Pacific', $user_roles) ){
              // remove all but Draft and Published options for other content types...
              print $remove_needsQC_and_QCcomplete_options;
            }
          }

          // if News Article content type...
          if( $node->type == 'news_article' ){
            $news_terms = get_news_tags($node);
            
            
            // if the South Africa tag is set for the current node's news-tags...
            if( in_array('South Africa', $news_terms) ){
              // if the user has South Africa, International or administrator as one of their roles...
              if( in_array('International', $user_roles) || in_array('administrator', $user_roles) || in_array('South Africa', $user_roles) ){
                print $remove_needsQC_and_QCcomplete_options;
              }
              else{
                // otherwise remove all options...
                print $remove_qc_options_and_save_buttons;
              }
            }
            
            // if the Asia Pacific tag is set for the current node's news-tags...
            if( in_array('Asia Pacific', $news_terms) ){
              // if the user has Asia Pacific, International or administrator as one of their roles...
              if( in_array('International', $user_roles) || in_array('administrator', $user_roles) || in_array('Asia Pacific', $user_roles) ){
                print $remove_needsQC_and_QCcomplete_options;
              }
              else{
                // otherwise remove all options...
                print $remove_qc_options_and_save_buttons;
              }
            }
            
            // if not designated as South Africa or Asia Pacific news article but user is South Africa or Asia Pacific...
            if( !in_array('South Africa', $news_terms) && !in_array('Asia Pacific', $news_terms) ){
              if( in_array('South Africa', $user_roles) || in_array('Asia Pacific', $user_roles) ){
                // remove all options...
                print $remove_qc_options_and_save_buttons;
              }
            }
            
          } // END if news article


          // not News Article or International content types...
          else{

            // check if international region field is set/exists...
            if( property_exists($node, 'field_international_region') === true ){
              $international_region_name = get_international_region($node);
              
              // if the South Africa tag is NOT set as the current node's international region...
              if($international_region_name == 'South Africa'){
                // if the user has South Africa, International or administrator as one of their roles...
                if( in_array('International', $user_roles) || in_array('administrator', $user_roles) || in_array('South Africa', $user_roles) ){
                  // remove all but Draft and Publish options...
                  print $remove_needsQC_and_QCcomplete_options;
                }
                else{
                  // remove all options...
                  print $remove_qc_options_and_save_buttons;
                }
              }
              
              // if the Asia Pacific tag is NOT set as the current node's international region...
              if($international_region_name == 'Asia Pacific'){
                // if the user has Asia Pacific, International or administrator as one of their roles...
                if( in_array('International', $user_roles) || in_array('administrator', $user_roles) || in_array('Asia Pacific', $user_roles) ){
                  // remove all but Draft and Publish options...
                  print $remove_needsQC_and_QCcomplete_options;
                }
                else{
                  // remove all options...
                  print $remove_qc_options_and_save_buttons;
                }
              }
              
              // if not designated as South Africa or Asia Pacific region but user is South Africa or Asia Pacific...
              if( $international_region_name != 'South Africa' && $international_region_name != 'Asia Pacific' ){
                if( in_array('South Africa', $user_roles) || in_array('Asia Pacific', $user_roles) ){
                  // remove all options...
                  print $remove_qc_options_and_save_buttons;
                }
              }
              
            } // END if international region exists

          } // END if NOT News or South Africa

        } // END if have node update access

      } // END if editing existing node
      ?>
        
      <?php if( isset($page['content']['system_main']['#node']->type) ){ // if a node edit/create page, display SEO TOOL 2.0... ?>

        </div>
        
        <div style="float:left; width:20%;">
          <div style="width:80%; margin:5%; padding:5%; background:#e6e6e6;">
            <?php 
            if($node->nid == 2390){ // this node needs to use SEO Tool 2.0 - for some reason the 1.0 doesn't show up ???...
              include('inc-seo-tool-2.0.php'); 
            }
            ?>
          </div>
        </div>
                
        <script>
        var $jq = jQuery.noConflict();
        // replace curly quote with straight quotes within the Title field to prevent losing them in social media sharing...
        $jq(document).ready(function(){
          // replace single/double curly quotes with straight quotes when editing...
          var title = $jq('#edit-title');
          var titleValue = title.val();
          var newTitle = titleValue.replace(/[\u2018\u2019]/g, "'").replace(/[\u201C\u201D]/g, '"');
          // replace single/double curly quotes with straight quotes when creating...
          $jq(title).val(newTitle);
          $jq(title).focusout(function(){
            var titleValue = title.val();
            var newTitle = titleValue.replace(/[\u2018\u2019]/g, "'").replace(/[\u201C\u201D]/g, '"');
            $jq(title).val(newTitle);
          });
        });
        </script>  
        
      <?php } else { ?>
      
        </div>
      
      <?php } ?>
      
                              
    </div>    
    

    <div id="footer">
      <?php print $feed_icons; ?>
    </div>

  </div>
  
<?php // if current page is a news article edit or create page, get news tags and add their descriptions to the form labels...
$path = current_path();
if( $path == 'node/add/news-article' || ( isset($node->nid) && $node->type = 'news_article' ) ){
  
  $vocabulary = taxonomy_vocabulary_machine_name_load('news_tags');
  $terms = entity_load('taxonomy_term', FALSE, array('vid' => $vocabulary->vid));
  $news_tags = array();
  foreach($terms as $term){
    $news_tags[] = array('name' => $term->name, 'description' => $term->description);
  }
  ?>
  
  <script>
    var $jq = jQuery.noConflict();
    $jq(document).ready(function(){
      $jq('label').each(function(){
        var labelText = $jq(this).text();

        <?php 
        foreach($news_tags as $tag){ 
          $d = trim($tag['description']);
          $d = str_replace('<p>', '<i>( ', $d);
          $d = str_replace('</p>', ' )</i>', $d);
        ?>
          if(labelText.trim() == '<?php print $tag['name']; ?>'){
            $jq(this).append('<span style="margin-left:2em;"><?php print $d ?></span>');
          }
        <?php } ?>

      });        
    });
  </script>
  
  <?php
}
?>  

<!-- END page.tpl.php of the SEVEN theme -->